function bestAlpha = selectByFisherInfo(xhat,alpha)


for k1=1:length(alpha);
  q = 1/(1+exp(-xhat - alpha(k1)));
  J(k1) = (q*(1-q))^2;
end

[bestJ, I] = max(J);
bestAlpha=alpha(I); 
