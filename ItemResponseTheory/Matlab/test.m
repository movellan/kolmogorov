clear
trueX =0;
dx=0.01
x=-4:dx:4;
xHat=0;
alpha =[-2:0.1:2];

ntrials =100
for trial=1:ntrials
  bestAlpha(trial)= selectByFisherInfo(xHat,alpha);
  q = 1/(1+exp(-trueX-bestAlpha(trial)));
  y(trial) = rand(1)<q;
  for k=1:length(x)
    logLike(k) = 0;
    for k2=1:trial
      q = 1/(1+exp(-x(k)-bestAlpha(k2)));
      logLike(k) = logLike(k)+y(k2)*log(q);
      logLike(k) = logLike(k)+ (1-y(k2))*log(1- q);
    end
  end
  [lL,k3] = max(logLike);
  bestX(trial) = x(k3);

  if(mean(y) >0 && mean(y) < 1)
    H = logLike(k3+2) - logLike(k3+1);
    H = H - logLike(k3+1) + logLike(k3);
    H = H/dx/dx;
    Var(trial) = -1/H;
  else
    Var(trial) = 4/trial;
  end
 
end

%plot(bestX)


