clear
x =[-5:0.1:5]';
mu=-2;
px = exp(-0.5*(x-mu).^2);
px = px/sum(px);
alpha =[-5:0.1:5];

py=0;
mx = sum(px.*x);
for k1=1:length(alpha);
  q = 1/(1+exp(-mx - alpha(k1)));
  J(k1) = (q*(1-q))^2;
end

[bestJ, I] = max(J);
bestAlpha=alpha(I) 
