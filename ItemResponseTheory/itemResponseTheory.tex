\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}
\usepackage{listings}
\usepackage{boxedminipage}

 \title{Item Response Theory}
\author{Copyright \copyright Javier R. Movellan}



\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}





\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}



\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{lem}{Lemma}[section]
\theoremstyle{definition}
\newtheorem{rem}{Remark}[section]
\theoremstyle{definition}
\newtheorem{ex}{Example}[section]






\begin{document}
\maketitle


Please cite as

{ Movellan J. R. (2012) {\em Item Response Theory.} MPLab Tutorials,
University of California San Diego }

    
\newpage

Item Response Theory (IRT) is a formalism to make inferences about a
latent variable $X$ representing the skill level of a subject, based
on responses to questions of different difficulty levels.

A popular IRT model takes the following form
\begin{align}
p(Y= 1 \given x,k) = \frac{1}{1+e^{-a_k(x- b_k)}}
\end{align}
$Y=1$ stands for a correct response to a question, $x$ is the person's
skill level, $b_k$ is the difficulty of questions of type $k$, and
$a_k$ the discrimination power of questions of type $k$.  Here we will
focus on different approaches to the problem of how to choose
questions to make inferences about $X$ as efficiently as possible.




\section{Classical (Frequentist) approach}
The classical approach to statistics is characterized by an
unwillingness to treat parameter as random variables. Under this view
parameters has a fixed, yet unknown, value. Parameter estimation is
typically done via approaches such as maximum likelihood, and variable
selection is performed using the Fisher information criterion.  The
Fisher information about $x$ of answer $Y$ to question $k$ the is
defined as follows
\begin{align}
J_k(x) = \sum_{y=0}^1 p(y\given x,k) \Big( \frac{\partial \log p(y\given x,k)}{\partial x}\Big)^2 = -\sum_{y=0}^1 \frac{\partial^2 \log p(y\given x,k)}{\partial x^2}
\end{align}
If $T = f(Y)$ is an unbiased estimate of $X$, i.e., $E[T \given
  x,k]=x$ then, it can be shown that 
\begin{align}
Var[T\given x,k] \leq \frac{1}{J_{k}(x)}
\end{align}
Thus, the inverse of the Fisher information lower bounds the
efficiency of unbiased estimates of $X$ based on $Y$. It is
reasonable to choose questions which have the largest Fisher
information. 

Let's check how the Fisher information looks like for the IRT
model. Let $\pi_k(x) = p(Y=1\given x,k)$ and note
\begin{align}
\frac{\partial \pi_k(x)}{\partial a_k x} = \frac{e^{-a_k(x-b_k)}}{(1+e^{-a_k(x-b_k)})^2}  = \pi_k(x) (1-\pi_k(x))
\end{align}
Thus
\begin{align}
\frac{\partial \pi_k(x)}{\partial  x} = 
\frac{\partial \pi_k(x)}{\partial a_k x}\frac{\partial a_k x}{\partial x} = 
a_k \pi_k(x) (1-\pi_k(x)) 
\end{align}
Moreover
\begin{align}
\log p(y\given x,k) = y \log \pi_k(x) + (1 - y) \log (1- \pi_k(x))
\end{align}
Thus
\begin{align}
\frac{\partial \log p(y \given x,k)}{\partial \pi_k(x)} =
\frac{y}{\pi_k(x)} - \frac{1- y}{1- \pi_k(x)} = \frac{y - \pi_k(x)}{\pi_k(x)(1- \pi_k(x))}
\end{align}
and
\begin{align}
\frac{\partial \log p(y \given x,k)}{\partial x} =
\frac{\partial \log p(y \given x,k)}{\partial \pi_k(x)}\frac{\partial \pi_k(x)}{\partial x} = a_k(y - \pi_k(x)) 
\end{align}
The Fisher information follows
\begin{align}
J_k(x) &= \pi_k(x) a_k^2 (1-\pi_k(x))^2+ (1- \pi_k(x)) a_k^2 ( - \pi_k(x))^2
\nonumber\\
&= a_k^2 \pi_k(x) (1- \pi_k(x))
\end{align}
We now confirm that the Fisher information can also be obtained using the second order derivatives.Note
\begin{align}
\frac{\partial ^2 \log p(y\given x,k)}{\partial x^2} = \frac{\partial a_k (y - \pi_k(x))}{\partial x}= - a_k^2 \pi_k(x) (1-\pi_k(x))
\end{align}
and
\begin{align}
&-\sum_{y_k=0}^1 \frac{\partial^2 \log p(y\given x,k)}{\partial x^2} =  \pi_k(x) a_k^2 \pi_k(x) (1-\pi_k(x)) + (1- \pi_k(x))a_k^2 \pi_k(x) (1-\pi_k(x)) \nonumber\\
&= a_k^2 \pi_k(x) (1-\pi_k(x))= J_k(x)
\end{align}
\subsection{Parameter Estimation}
The classical approach calls for choosing question $i$ that maximizes the Fisher information, i,e,
\begin{align}
J_k(x) &=a_k^2 \pi_k(x) (1-\pi_k(x))
\end{align}
Unfortunately computing the Fisher information of question $k$
requires knowing the value of $x$, which is an unknown
parameter. Thus, we estimate the Fisher information by using an
estimate of $x$ instead of $x$, which is unknown.  Maximum likelihood
estimates can be seen assymptotically efficient and thus are quite
popular within the classical approach. Suppose we have a serios of
questions $k_1, k_2, \cdots$ and variables indicating whether the
answers to those questions were correct $Y_1, Y_2, \cdots$.  The log
likelihood of the observed answers given a parameter $x$ takes the following form
\begin{align}
L_t(x)=  \sum_{s=1}^t \log p(y_t  \given x,k_s) \nonumber\\
&=
 \sum_{s=1}^t \Big( y_s \log \pi_{k_s}(x) + (1- y_s )\log (1- \pi_{k_s}(x))\Big)
\end{align}
The maximimum likelihood estimate $x_t$ is the value of $x$ that maximizes the likelihood function 
\begin{align}
\hat x_t = \argmax_x L_t(x)
\end{align}
We then choose questions based on the estimate of the Fisher information 
\begin{align}
J_k(\hat x_t)
\end{align}

\section{Bayesian Approaches}
Bayesian approaches treat parameters as random variables, thus
representing uncertainty about these parameters as probability
distributions.  Given a prior distribution $p_x$ for the parameter,
Bayesian approaches measure the desirability of a question of type $k$
in terms of its expected effect on the posterior distribution of $X$
after observing the answer to the question. Below we see some examples

\subsection{Maximum Information Gain Criterion}
This criterion prescribes to ask the question that is expected to provide the maximum amount of information about $X$. 
\begin{align}
I_k = H(X) - H(X \given Y,k)
\end{align}
where $I_k$ is the Shannon information gain, $H(X)$ the prior entropy of $X$
\begin{align}
H(X) = -\int p(x) \log p(x)
\end{align}
and $H(X\given Y,k)$ the expected entropy of $X$ after getting the answer $Y$ to question $k$
\begin{align}
&H(X\given Y,k) = \sum_y p(y\given k) H(X\given y,k)\\
&p(y\given x) = \int p(x) p(y\given x,k)\\
&H(X\given y,k) =  \int p(x \given y,k) \log p(x\given y,k)
\end{align}
\subsection{Minimum Risk Criterion}
In this approach we define a cost function $c(x,\hat x)$ that represents the cost of guessing parameter $\hat x$ when the true parameter is $x$. We choose a question that would minimize the expected cost
\begin{align}
\rho_k = E[ c(X,\hat X)\given Y,k]
\end{align}
where
\begin{align}
\hat X = \argmin_U E[c(X,U) \given Y,k]
\end{align}
where $U$ is a function of $Y$. 
\paragraph{ Squared Error Cost}
In this case 
\begin{align}
&c(x,\hat x) = (x - \hat  x)^2\\
&\hat x = \argmin_U E[(X -U)^2 \given Y,k]
\end{align}
Note
\begin{align}
E[(X-U)^2 \given Y,k] = \sum_y p(y\given k) \int p(x \given y,k) (x - f(y) )^2 dx
\end{align}
Taking the derivative with respect to $f(y)$ and setting it to zero
\begin{align}
2\int p(x\given y,k) (x - f(y)) dx =0\\
\end{align}
Thus minimum cost is achieved for 
\begin{align}
\hat x = E[X \given y ,k]
\end{align}
and the least squares error criterion for choosing  questions is as follows
\begin{align}
\rho_k &= \sum_y p(y\given k) \int p(x\given y,k) (x - E[X\given y,k])^2 dx\nonumber\\
&= \sum_y p(y\given k) Var[X\given y,k]
\end{align}
Thus this approach chooses the question that is expected to provide
the posterior distribution of $X$ with smallest variance. In the
language of statistics, this is known as the most efficient question,
because it is the one that provides the most efficient (least squared
error) estimate of $X$.


\end{document}

