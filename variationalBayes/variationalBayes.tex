\documentclass{article}
\usepackage[square,numbers]{natbib}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
\usepackage{fancybox}





\newcommand\argmin{\operatornamewithlimits{argmin}}

\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand\given{\medspace|\medspace}
\newcommand\argmax{\operatornamewithlimits{argmax}}


 \title{ Variational Bayes}
\author{ Javier R. Movellan\\ 7/28/2022\\1/7/2023}





\newenvironment{namelist}[1]{%
\begin{list}{}
	{ 		\let\makelabel\namelistlabel
\skettowidth{\labelwidth}{#1}
\setlength{\leftmargin}{1.1\labelwidth} 	} }{%
\end{list}}

\empty

% Replace with the directory where the graphs have been placed
\newcommand{\graphdir}{./graphs}
\newcommand{\centerfig}[2]{\centerline{\psfig{figure=#1,width=#2}}}




\begin{document}
\maketitle
Goal of this document is to explore the essence of Variational Bayes approaches and provide examples of how it is used in ML. The example we focuson is   "DropConnect is Effective in Modeling Uncertainty of Bayesian Deep Networks  \cite{vdc}. 

The document  is informal and fluid. Focus is on exploring ideas. 


\paragraph{Notation}
We use the standard notation in mathematical probability theory, a special case of measure theory. However we will use short-cut notation and gloss over the measure theory details. We represent random vectors with capital letters and specific samples of these variables with small letters, and when possible we identify probability distributions by their arguments. For example $p_Y(u)$ the probability or probability density of the random variable $Y$ evaluated at $u$ will be represented as $p(y)$. 

\section{Problem Statement}


 Let $X,Y,W$ be random vectors representing the input, output and parameters (e.g., deep network weights) of a model. For example $X$ could be an image, $Y$ a pixel by pixel segmentation of the image into foreground and background.  Let $p(y\given x,w ) = p(y\given f(x,w))$ where $f$ is the model parameterized by $W$.  For example, if $Y$ is continuous we may model $p(y\given x,w)$ as a Gaussian random vector with mean $f(x,w)$ and diagonal covariance matrix $\sigma^2 I$, i.e., 
 \begin{align}
\log p(y \given x,w) =  -\frac{1}{2\sigma^2} \| y - f(x,w)\|^2 + K
\end{align}
where $K$ is a constant independent of $x,w$. Similarly if  $Y$ is a binary vector we may model $p(y\given x,w)$ as a Bernoulli random vector
\begin{align}
\log p(y \given x,w) = \sum_i y_i f_i(x,w)  +  (1-y_i) (1- f_i(x,w))
\end{align}
where $y_i,f_i$ are the $i^{th}$ element of the vector $y$ and the vector function $f$. 


\section{MAP, MLE Training}
We are given a training set $d=\{(x_i,y_i) \}_{i=1}^n$ consisting of pairs of $(X,Y)$ samples. Our goal is to predict $Y$ for new samples of $X$. 

The standard Maximum Posterior/Maximum Likelihood  approach consists of finding a value of $w$ that is a local maximum to  the posterior probability of $p(w\given d)$ (MAP)  or the likelihood $p(d\given w)$. Using Bayes rule:
\begin{align}
\hat w = \argmax_w  \log p(w| d) &= \argmax_q \log {p(d\given w) p(w)}{p(d)} \nonumber\\
&= \argmax_w \log p(d|w) + \log p(w)\nonumber\\
&= \sum_i \log p(y_i\given w_i, w)  + \log p(w)
\end{align}
where we used the fact that $p(d)$ does not depend on $w$.  Maximum Likelihood approaches can be seen as a special  case of MAP with uniform priors, i.e., the $\log p(w)$ term is ignored.  Training a model refers to the process of finding a local maximum $\hat w$. In deep networks the optimization process is typically done using Stochastic Gradient Ascent on $\log p(w\given d)$.
'
\section{Bayesian Learning}

 Given the problem formalization above probability theory tells us how to make optimal predictions using the information in the training set:  We need to computer the  posterior probability $p(w\given d)$, which can be derived using Bayes rule
\begin{align}
p(w\given d) = \frac{p(w) p(d\given w)}{p(d)}
\end{align}
Predictions for new inputs $x$ can be obtained by using the Law of Total Probability
\begin{align}
p(y\given x,d) = \int p(y,w\given x,d) \;dw = \int p(w\given d) p(y\given x, w)\;dw
\end{align}
Note that in MAP/ML approaches the result of training is a single model $\hat w$. However in Bayesian approaches  the result of Bayes rule is an entire probability distribution over models $p(w\given d)$. We can think of this  as an ensemble of models $w$ each with a different  prediction $p(y\given x, w)$ for the correct response to the input $x$. Prior to observing the training dataset the opinions of each model are weighted by a prior distribution $p(w)$. After training the weights of each model are weighted by the posterior distribution  $p(w\given d)$. Learning, thus is a form of inference which consists of modifying $p(w)$ into $p(w\given d)$. Bayesian methods tend to be more resistant to overfitting than MAP/ML methods, they are better at knowing when they don't know.   Unfortunately it's not always possible to find exactly the posterior probability distribution, analytically or computationally. 


\section{Variational Bayes: Bayes Rule as an optimization problem}
ML and MAP approaches are casted as an optimization problem, i.e., find parameter values that are local maxima of the lop posterior objective function. This allows the use of optimization methods like Stochastic Gradient Descent that can be used with very complex models, like deep neural networks. 

Traditional Bayesian approaches focus on applying Bayes rule as the optimal way to update model beliefs and synthesize all the information available in the data. One issue is that in many cases of interest applying the Bayes rule is computationally intractable. Variational Bayesian approaches frame Bayes rule as the solution to an optimization problem. This allows to use optimization and approximation tools like gradient descent. 

 \subsection{Optimality of Bayes Rule}
 Here we show that Bayes rule is in fact the solution to an optimization problem. In ML/MAP the optimization problem is in the space of parameters, i.e. we search for a parameter value that maximizes the log posterior objective function. In the Bayesian case the optimization is in the space of distributions and the objective function $V$ that combines how well this distribution fits the training data and how much it deviates from the prior distribution $p(w)$
  \begin{align}
 V(q,p) &= L(q,p)  - D[q, p] 
 \end{align}
 where
 \begin{align}
 L(q,p)&=\int q(w) \log p(d \given w) dw \\
D[q,p]&=  \int q(w) \log \frac{q(w)}{p(w)} dw
 \end{align}
 The  objective function $V$ evaluates a candidate probability distribution $q(w)$ as a sun of two  terms: The first term $L$ is the Expected Log Likelihood and it evaluates how well $q$ fits the data. It is maximized if $q(w)$ is concentrated on the values of $w$ with maximum $p(d\given w)$. The second term $D$ is  the Prior Divergence. It is minimized when the candidate distribution $q$ equals the prior distribution. Our goal is to find a probability distribution $\hat q$ that strikes an optimal balance between these two terms.  We will show that the solution of this optimization problem is precisely the posterior distribution $p(w\given d)$.   
 
 \paragraph{Proposition} 
 $V(q,p)$ is maximized for $q(w) = p(w\given d)$. 
 \paragraph{Proof}
 \begin{align}
D[q,p_{w|d} ] &= \int q(w) \log\frac{ q(w)}{p(w)p(d \given w)/p(d)} dw \nonumber\\
&= \log p(d) - L(q) + D[q(W),p(W)] = \log p(d) - V(q)
\end{align}
Thus 
 \begin{align}
V(q)  = \log p(d) - D[q(W), p(W\given d) ]  \leq \log p(d)
\end{align}
Since $\log p(d)$ is constant with respect to $q$ then $V(q)$ is maximized for  $q(W) = p(W\given d)$, at which point the KL Divergence is 0 and $L(q) = \log p(d)$.  The term $\log p(d)$ is known as the Evidence and since $V$ lower bounds it, it is often known as the Evidence Lower Bound (ELBO). 



\subsection{Free Energy Interpretation}
Note the Variational objective function can be decomposed into three terms
\begin{align}
V(q,p) &=  P[q,p]+L[q,p] + H[q]
\end{align}
where $P$ is a prior term
\begin{align}
P[q,p] = \int q(w) \log p(w) dw
\end{align}
$L$ is a likelihood term
\begin{align}
L[q,p] = \int q(w) \log p(d\given w) dw
\end{align}
and  $H[q]$ is an entropy term
\begin{align}
H[q] = - \int q(w) \log q(w) dw
\end{align}
We note that  $F[q,p] = - V[q,p]$ has the form of a Free Energy, the combination of an average energy minus an entropy. Here the average energy is
\begin{align}
E[q,p] = - P[q,p] - L[q,p] = \int q(w) \log p(w,d) dw
\end{align}
It is well known that negative Free Energies are maximized by the Boltzmann distribution (see Appendix)
\begin{align}
q(w) \propto e^{- E[q,p]} = e^{\log p(w,d)} = p(w,d)
\end{align}
Thus, as expected 
\begin{align}
q(w) = p(w|d)
\end{align}
\paragraph{Proposition}
Consider the following modification of the likelihood and prior distributions
\begin{align}
\bar p(d\given w) &\propto p(d\given w)^{\alpha}\\
\bar p(w) &\propto p(w)^{\alpha}
\end{align}
\begin{align}
 V(q,\bar p) &=L[q,\bar p] + P[q,\bar p] +  H[q] \\
                  &= \alpha \big(L[q, p] + P[q, p] \big)+  H[q] 
\end{align}  
Thus weighting the Energy term $E=-L-P$ by a factor of $\alpha$ is equivalent to using a modified version of the prior and the likelihood models. The modified prior and likelihoods equal the original ones to the power of $\alpha$.  This is also equivalent to weighting the entropy term by a factor of $1/\alpha$. 

\paragraph{Proposition}
Consider the following modification of the likelihood function 
\begin{align}
\bar p(d\given w) &\propto p(d\given w)^{\alpha}\\
\end{align}
\begin{align}
 V(q,\bar p) &=  L[q,\bar p]  - D[q, p] \\
                  &= - \alpha   L[q,p] - D[q,p ]
\end{align}  
Thus weighting the Likelihood term $L$ by a factor of $\alpha$ is equivalent to using a modified version of the likelihood model. The modified likelihood equal the original to the power of $\alpha$.  This is also equivalent to weighting the Divergence term by a factor of $1/\alpha$. 



\subsection{Three  Equivalent Interpretations}

\begin{itemize}
\item Maximize $V$,  which combines data fit and distance from prior. 
\item Minimize Free Energy $F=-V$ a combination of entropy and expected loss
\item Minimize Divergence between $q$ and posterior $p_{w|d}$
\end{itemize}

\section{Numerical Methods}
The same way we can use numerical optimization to find parameters that maximize the likelihood function we can now use numerical optimization to find distributions that maximize the $V$ function, thus effectively approximating the Bayesian posterior distribution.  To do some  use  a family of  approximating distributions  $q(w\given \phi)$ parameterized by $\phi$.  Our goal is to find values of $\phi$  such that $q(w\given \phi)$ approximates $p(w\given d)$.  Note here we have a hyperparameter vector $\phi$ that maps into a probability distribution over parameters $q(w \given \phi)$ which is evaluated by the function $V$. Our goal is to find values of $\phi$ that optimize $V$. In ML/MAP the goal is to directly find values of $w$ that optimize the log posterior objective function. 

\subsection{Probability Gradient Method}
This method is also known as the Score method. Since the set of candidate distributions $q$ is parameterized by $\phi$ we will change our notation so that $V,F,L,D$ are also a function of $\phi$. The most basic form of stochastic gradient descent would use the fact that 
\begin{align}
\nabla_\phi V(\phi) &= \nabla_\phi \int q(w\given \phi) \Big(\log p(d, w) - \log q(w\given \phi)\Big) dw\nonumber\\
&= \int q(w\given \phi) \nabla_\phi \log q(w\given \phi) \Big( \log p(d, w) - \log q(w\given \phi) \Big)dw
\end{align} 
and the stochastic gradient descent version would use gradient estimates  based on samples from $q(w\given \phi)$. While this approach provides unbiased gradient estimates, they also have high variance, making the optimization process slow. Variance reduction methods like Rao-Blackwelization and Control-Variates can be used to reduce the variance. This is the approach used in approaches such as "Blackbox Variational Inference"

\subsection{ Reparameterization Method}
This method is also known as the Pathwise Derivative method, Change of Variables Method, and Reparameterization Trick. This  approach tends to provide gradient estimates with lower variance than the Probability Gradient method. It is based on the Change of Variables Rule for Expected Values: f a random variable $Y$ is a function of another random variable $X$, i.e., $Y = f(X)$ then
\begin{align}
E[Y] = \int p(y) y \; dy = \int p(x) f(x) dx
\end{align}
Suppose there is a random variable  $Z$  such that $W = f(Z,\phi)$. Then
\begin{align}
P[\phi]&=\int q(w\given \phi)  \log p(w)  dw \nonumber\\
&=-\int q(z)  \log p(w) dz
\end{align}
\begin{align}
L[\phi]&=\int q(w\given \phi)  \log p(d\given w)  dw \nonumber\\
&=-\int q(z)  \log p(d\given w) dz
\end{align}
and the entropy 
\begin{align}
H[\phi] &= -\int q(w\given \phi)  \log q(w\given \phi)   dw\nonumber\\
&=-\int p(z)  \log q( w \given \phi) dz
\end{align}
where $w = f(z,\phi)$
Taking gradients 
\begin{align}
\nabla_\phi  P[\phi] &= \int p(z) \nabla_\phi \log p(w) dz \\
\nabla_\phi  L[\phi] &= \int p(z) \nabla_\phi \log p(d\given w) dz 
\end{align}
Regarding the gradient of the Entropy we have two options.
\paragraph{Jacobian Method}
 If the entropy has no analytical form and $W=f(Z,\phi)$ is invertible then 
\begin{align}
q(w\given \phi) = \frac{p(z)}{| J(z,\phi)|}
\end{align}
where $w= f(z,\phi)$ and $| J(z,\phi)|$ is the absolute value of the determinant of the Jacobian of $w$ wrt $z$. 
\begin{align}
J(z,\phi) = \nabla_z f(z,\phi)' 
\end{align}

Thus
\begin{align}
H[\phi] &= - \int p(z) \Big(\log p(z) -  \log | J(z,\phi)| \Big) dz \\
\nabla_\phi H[\phi] &= \int p(z) \nabla_\phi \log |J(z,\phi ) |\; dz
\end{align}

\paragraph{Explicit Entropy Method}
In some cases the entropy has analytical form $g$ for which we can directly compute the gradient
\begin{align}
H[\phi] &= - \int q(w\given \phi) \log q(w\given \phi) dw = g(\phi)\\
\nabla_\phi H[\phi] &= \nabla_\phi g(\phi)
\end{align}
\paragraph{Explicit Divergence Formula}
In some cases the prior divergence has an analytical expression that can be differentiated directly
\begin{align}
D(\phi) = \int q_\phi(w) \log \frac{q_\phi(w)}{p(w)} dw
\end{align}
In which case
\begin{align}
V(\phi) &= \int q_\phi(w) \log p(d\given w) dw - D(\phi)\nonumber\\
&= \int p(z) \log p(d\given f(z,\phi) dz - D(\phi)
\end{align}
and
\begin{align}
\nabla_\phi V(\phi) &= \int p(z) \nabla_\phi \log p(d\given f(z,\phi) dz - \nabla_\phi D(\phi)
\end{align}

\section{Example: Simple Gaussian Case}
Let $p(x\given w) = g(x\given w,1)$ where $g(\cdot \given a,b)$ is a Gaussian pdf with mean $a$ and variance $b$. Let $p(w) = g(w\given  \mu_0, \sigma_0^2)$. Let $q(w\given \phi) = g(w\given \phi_1,\phi_2^2)$. 
We observe a random sample $d=\{x_1,\cdots x_n\}$ of $X$ and want to find the values of $\phi$ that maximize the Variational Bayes Objective $V$.


\subsection{Prior Term}
\begin{align}
P[\phi] &= \int q(w \given \phi) \log p(w) dw \nonumber\\
           &= \int p(z) \log p(w) dz\\
\end{align}
where 
\begin{align}
w = \phi_1 + \phi_2 z
\end{align}
Taking gradients

\begin{align}
\nabla_w  P[\phi] &= - \int q(z) \frac{w - \mu_0}{\sigma^2_0}\\
\nabla_{\phi_1} w &= 1\\
\nabla_{\phi_2} w &= z 
\end{align}
Thus
\begin{align}
\nabla_{\phi_1} P[\phi] &= \nabla_{\phi_1}  w \nabla_w P[\phi] =-  \int p(z) \frac{w- \mu_0}{\sigma^2_0} dz 
\end{align}
and
\begin{align}
\nabla_{\phi_2} P[\phi] &= \nabla_{\phi_2}  w \nabla_w P[\phi] =-  \int p(z) z  \frac{w - \mu_0}{\sigma^2_0} dz \\
\end{align}
We can get stochastic estimates of the gradient by sampling from $Z$, setting $W= \phi_1+\phi_2 Z$ and 
\begin{align}
\nabla_{\phi_1}\hat P[\phi] &= -  \frac{W- \mu_0}{\sigma^2_0} \\
\nabla_{\phi_2}\hat P[\phi] &=  -Z \frac{W- \mu_0}{\sigma^2_0} 
\end{align}
Taking Expected Values
\begin{align}
\nabla_{\phi_1} P[\phi] &=-  E[ \frac{W- \mu_0}{\sigma^2_0} ] =  -\frac{\phi_1 - \mu_0}{\sigma^2_0}\\
\nabla_{\phi_2} P[\phi] &= - E[Z   \frac{W- \mu_0}{\sigma^2_0} ] = - \frac{\phi_2}{\sigma^2_0}
\end{align}
\subsection{Likelihood Term}
\begin{align}
L[\phi]  &=   \int q(w\given \phi) \log p( x \given w) dw \nonumber\\
&= \int p(z)  \log p(x\given w) dz 
\end{align}
where 
\begin{align}
w = \phi_1 + \phi_2 z
\end{align}
Taking gradients
\begin{align}
\nabla_w L[\phi] &= \int p(z) \log p(x\given w) dz = - \int p(z) \sum_i (w-x_i) dz\\
\nabla_{\phi_1} L[\phi] &= \nabla_{\phi_1} w \nabla_w L[\phi] =- \int p(z) \sum_i (w-x_i) dz\\
\nabla_{\phi_2} L[\phi] &= \nabla_{\phi_2} w \nabla_w L[\phi] = - \int p(z) z \;\sum_i  (w-x_i) dz 
\end{align}
We can get stochastic estimates of the gradient by sampling from $Z$, setting $W= \phi_1+\phi_2 Z$ and 
\begin{align}
\nabla_{\phi_1} \hat L[\phi] &= - \sum_i (W-x_i) \\
\nabla_{\phi_2} L[\phi] &=- Z \sum_i    (W-x_i)  =-\frac{W- \phi_1}{\phi_2}   \sum_i   (W-x_i) 
\end{align}
Taking Expected Values
\begin{align}
\nabla_{\phi_1} L[\phi] &= - E[\sum_i (W-x_i)] =-  \sum_i (\phi_1 -x_i) \\
\nabla_{\phi_2} L[\phi] &=-E[Z\sum_i     (W-x_i) ]=  -\sum_i \phi_2
\end{align}
\subsection{Entropy Term}
\begin{align}
H[\phi] &= - \int q(w\given \phi) \log p(w\given \phi) dw \nonumber\\
&= \int p(z) \log |J(z,\phi)| dz=  \log \phi_2
\end{align}
where 
\begin{align}
w = \phi_1 + \phi_2 z
\end{align}
Taking gradients
\begin{align}
\nabla_{\phi_1} H[\phi]&= - \nabla_{\phi_1} \phi_2 =0\\
\nabla_{\phi_2} H[\phi] &= - \nabla_{\phi_1} \phi_2 = \frac{1}{\phi_2}
\end{align}
\subsection{Overall Gradient}
\begin{align}
&V[\phi] =  P[\phi]  +  L[\phi]  + H(\phi)
\end{align}
Thus
\begin{align}
\nabla_{\phi_1} \hat V[\phi] &=  -  \frac{W- \mu_0}{\sigma^2_0} -  \sum_i (W-x_i) \\
\nabla_{\phi_2} \hat V[\phi]  &=  -Z  \Big( \frac{W-\mu_0}{\sigma^2_0} +  n( W-\bar x) \Big) +\frac{1}{\phi_2}
\end{align}
where $\bar x = (\sum_i x_i)/n$. The expected value and variances of the gradient samples are as follows
\begin{align}
 E[\nabla_{\phi_1} \hat V[\phi] ] &=  \nabla_{\phi_1} V[\phi] =  -\frac{\phi_1 - \mu_0}{\sigma^2_0} -   \sum_i (\phi_1 -x_i)  \\
 Var[ \nabla_{\phi_1} \hat V[\phi] ]  &= \phi_2^2\: \big( \frac{1}{\sigma_0^2} + n^2\big)^2
 \end{align}
 and
 \begin{align}
 E[\nabla_{\phi_2} \hat V[\phi] ] &=\nabla_{\phi_2} V[\phi]  =- \frac{\phi_2}{\sigma^2_0}   -n \phi_2 +\frac{1}{\phi_2}\\
 Var[\nabla_{\phi_2} \hat V[\phi] ] &=\Big( \phi_1 (\frac{1}{\sigma_0^2} +n) - \frac{\mu_0}{\sigma_0^2} -n \bar x \Big)^2 
 + 2 \phi_2^2 \big(\frac{1}{\sigma^2_0} +n \big)^2
\end{align}
\paragraph{Enforcing non-negative constraint for $\phi_2$}
To enforce $\phi-2$ to be non-negative we typically use gradient descent on $\log \phi_2$ instead of $\phi_2$
In this case
\begin{align}
\nabla_{\log \phi_2} \hat V[\phi]  & = \nabla_{\log \phi_2} \phi_2  \nabla_{\phi_2} \hat V[\phi] =  -\phi_2 Z  \Big( \frac{W-\mu_0}{\sigma^2_0} +  n( W-\bar x) \Big) + 1\\
E[\nabla_{\log \phi_2} \hat V[\phi] ] &= - \frac{\phi_2^2}{\sigma^2_0}   -n \phi_2^2 + 1\\
 Var[\nabla_{\log \phi_2} \hat V[\phi] ] &= \phi_2^2 \Big( \phi_1 (\frac{1}{\sigma_0^2} +n) - \frac{\mu_0}{\sigma_0^2} -n \bar x \Big)^2 
 + 2 \phi_2^4 \big(\frac{1}{\sigma^2_0 }+n\big)^2 
\end{align}
\paragraph{Analytical solution}
Setting the expected values of the gradients to zero we get an analytical solution
\begin{align}
\phi_1 &= \frac{\mu_0/\sigma^2_0 + \sum_i x_i}{1/\sigma^2_0 +n}\\
\phi_2^2 &= \frac{1}{1/\sigma^2_0 +n}
\end{align}

\section{Drop Connect}
One problem with MAP/MLE approaches is that they have a tendency to overfit the training data, i.e., the parameter $\hat w$ results on predictions that over-estimate the accuracy of their predictions in response to new data. In other words the solutions found with MAP/MLE approaches often don't know what they don't know.  One approach to reduce the overfitting problem is DropConnect \cite{vdc}. In Drop Connect at each iteration Stochastic Gradient Ascent we   drop (set to zero) a randomly selected set of connections. Mathematically we can express this as multiplying each element of $W$ by a Bernoulli random vector $Z$
\begin{align}
W' = Z \cdot W
\end{align}
where $\cdot$ represents elementwise multiplication of the $Z$ and $W$ vectors. Thus if $Z_i=1$ then $W_i'=W_i$ and if $Z_i=0$ then $W'_i=0$.  We then compute and propagate the gradient using $Z\cdot W$ instead of $W$. More specifically,  on each iteration we get a sample $(x_i,y_i)$ from the training set and a sample $z_i$ of the Bernoulli vector $Z$. We then modify $w$ based on the following rule
 \begin{align}
 w(t+1)= w(t) + \kappa\: z \cdot \nabla_w \log p(y_i  \given x_i, z \cdot w(t)) 
\end{align}


 In \cite{vdc} it is shown that DropConnect is  as a Variational Approximation to Bayesian Learning but some of the mathematical details are omited. Here we clarify these details.   In DropConnect the approximating distribution is given by a seed parameter vector  $\phi$ and all the possible vectors resulting from setting a random set of components to zero. If $\phi$ has $m$ dimensions then $q_\phi()$ provides non-zero probabilities to  $2^m$ models $w$ each with a different set of values of $\phi$ set to zero. More formally
\begin{align}
W = Z \cdot \phi
\end{align}
and
\begin{align}
q(w \given \phi) = p(Z \cdot \phi =w) = \prod_j p(z_i \phi_i = w)
&= \begin{cases}
1,&\text{if $\phi_i$ =0}\\
0,&\text{if  $\phi_i,w_i \neq 0$ and  $w_i \neq \phi_i$}\\
\pi,& \text{ if $\phi_i \neq 0$ and $w_i = \phi_i$}\\
1-\pi,&\text{if $\phi_i \neq 0$, and $w_i=0$}\\
\end{cases}
\end{align}
where $\pi = p(Z_i=1)$ is a fixed parameter typically chosen using a validation set.

In the Appendix we show that if the prior distribution for $w$ is a zero mean Gaussian with constant diagonal covariance then the prior divergence term is proportional to the squared L2 norm of $\phi$ plus a constant
\begin{align}
D[p(W\given \phi), p(W\given d) ] = \lambda \|\phi\|^2 +K
\end{align}
Regarding the expected likelihood note
\begin{align}
\log p(d \given w) = \sum_j \log p( x_j,y_j \given w)
\end{align}
where
\begin{align}
\log p(x_j,y_j \given w) = p(x_j) p(y_j \given x_j,w) = \log p(y_j \given x_j, \phi \cdot w)+\log (x_j)
\end{align}
Taking gradients with respect to $\phi$ and considering $\phi = z \cdot w$
\begin{align}
\nabla_\phi \log p(x_j,y_j \given w) &=  \nabla_\phi \log p(y_j\given x_j,\phi) \nonumber\\
&=\nabla_\phi  w \nabla_w \log p(y_j \given x_j, w) = z \cdot \nabla_w \log p(y_j \given x_j, w)
\end{align}
Note $\log p(y_j \given x_j, w)$ measures the fit between the model response to $x_j$ and the desired response $y_j$. If $y_j$. For example if $y_j$ is Gaussian 
\begin{align}
\log p(y_j \given x_i, w) = -\frac{1}{2\sigma^2}\|y_j - f_w(x_j)\|^2 +C
\end{align}
which corresponds to the sum of squares loss. Similarly if $y_j$ is binary
\begin{align}
\log p(y_j \given x_i, w) =  y_j \log (f_w(x_j)) + (1-y_j) \log (1-f_w(x_j))
\end{align}
which corresponds to the cross entropy loss. This says that to compute the gradient with respect to $\phi$ first we sample $z$ and drop the  $\phi$ parameters for which $z_i=0$ to obtain $w=z \phi$. We then compute the gradient of the log likelihood of the output, using standard backpropagation, and set the gradient to zero for the parameters for which $z=0$. 

Bringing it all together
\begin{align}
D[q_\phi(W), p(W \given d)] &= \sum_j \int q(z) \log p(y_j \given x_j,  \phi \cdot z) dz + \frac{\pi}{2\sigma^2} \|\phi\|^2
\end{align}
\begin{align}
\nabla_\phi D[q_\phi(W), p(W \given d)] &= \sum_j \int q(z) z \nabla_w \log p(y_j \given x_j,  z\cdot w) dz -  \frac{\pi}{2\sigma^2} \phi 
\end{align}
\section{The Good, The Bad and The Ugly}
\begin{itemize}
\item The good: It's remarkable that we can approximate the posterior distribution even though we can't compute it. Drop Connect is a very simple algorithm. In addition to providing an approximation to the posterior it also helps reduce model overfitting during training. 
\item The bad: Since we can't compute the posterior distribution we don't know how good the approximation is. 
\item The ugly: In some cases the approximation may be really bad. Thus it is important to use empirical validation sets to see how well the approximation works in practice. 
\end{itemize}


\section{Appendix}
\subsection{Boltzmann Distribution}
Here we sketch the proof that negative Free Energies are maximized by the Boltzmann Distribution. Let
\begin{align}
F(q) = \int q(w) f(w) dw - \int q(w) \log q(w) dw
\end{align}
To maximize with respect to $q$ we add a Lagrange multiplier for the constraint that probability densities integrate to 1. 
\begin{align}
L(q) = F(q) + \lambda (\int q(w) dw -1)
\end{align}
Taking gradients and setting them to zero
\begin{align}
\frac{\partial L(q)}{\partial q(w)} = f(w) - \log q(w) -1 +\lambda=0\\
q(w) \propto e^{f(w)}
\end{align}


\subsection{Drop Connect}
Here we show that the prior divergence between the DropConnect approximate posterior and the prior Gaussian is the square norm of the parameter vector plus a constant. The prior distribution is Gaussian and the posterior is Bernouilli. We will approximate the Bernouill distribution was a mixture of two Gaussians the first with prior $\pi$, mean $\phi$ and the second with prior $1-\pi$ and mean vector $0$, both with an arbitrarily small variance $\epsilon^2$
\begin{align}
q_\phi(w) = \pi g(w,\phi,\epsilon^2) + (1-\pi) g(w,0,\epsilon^2)
\end{align}
The smaller $\epsilon^2$ the closer $q_\phi$ approximates a Bernoulli distribution. Thus the prior divergence looks as follows
\begin{align}
D[q_\phi(w), p(w) ] = \int q_\phi(w) \log \frac{q_\phi(w)}{p(w)} dw  = \sum_i q_\phi(w_i)  \log \frac{q_\phi(w_i)}{p(w_i)} dw_i
\end{align}
where 
\begin{align}
q_\phi(w_i) &= \pi g( w_i, \phi_i, \epsilon^2) + (1-\pi) g(w_i,0, \epsilon^2)\\\nonumber
p(w_i) &= g(w_i,0,\sigma^2)
\end{align}
Thus
\begin{align}
\int q_\phi(w_i) \log \frac{q_\phi(w_i)}{p(w)} dw_i  &=
\pi \int g(w_i,\phi_i,\epsilon^2)  \log \frac{q_\phi(w_i)}{p(w)}  dw_i\nonumber\\
&+ (1-\pi) \int g(0,\phi_i,\epsilon^2) \log \frac{q_\phi(w_i)}{p(w)} dw_i
\end{align}
I the two Gaussians  have negligible overlap, which we can achieve by making $\epsilon^2$ arbitrarily small, then 
\begin{align}
\int g(w_i,0,\epsilon^2)  &\log \frac{q_\phi(w_i)}{g(w_i,0,\sigma^2)}  dw_i \nonumber\\
&\approx \int g(w_i,0,\epsilon^2)  \log \frac{g(w_i,0,\epsilon^2)}{g(w_i,0,\sigma^2)}  dw_i \nonumber\\
&=\frac{1}{2} \left( \log \frac{\epsilon^2}{\sigma^2} + \frac{\epsilon^2}{\sigma^2} -1 \right)
\end{align}
and
\begin{align}
\int g(w_i,\phi_i,\epsilon^2)  &\log \frac{q_\phi(w_i)}{g(w_i,\phi_i,\sigma^2)}  dw_i \nonumber\\
&\approx \int g(w_i,\phi_i,\epsilon^2)  \log \frac{g(w_i,\phi_i,\epsilon^2)}{g(w_i,0,\sigma^2)}  dw_i \nonumber\\
&=\frac{1}{2} \left( \log \frac{\epsilon^2}{\sigma^2} + \frac{\epsilon^2 +\phi_i^2}{\sigma^2} -1 \right)
\end{align}
Thus as $\epsilon^2\to 0$
\begin{align}
D[q_\phi(w), p(w) ]&=  \frac{\pi}{2\sigma^2} \sum_i\phi_i^2  +K(\epsilon)\\
K(\epsilon)&= \sum_i \log \frac{\epsilon^2}{\sigma^2}+\frac{\epsilon^2}{\sigma^2} -1  
\end{align}


\paragraph{Divergence Method for Gaussian Example}


\begin{align}
&V(\phi) =  L(\phi) - D(\phi)\\
&L(\phi) = \int q(w\given \phi) \log p(x\given w) d\phi \\
&D(\phi) = \int q(w\given \phi) \frac{\log q(w\given \phi)}{\log p(w )} dw
\end{align}
where
\begin{align}
L &= \int g(w \given \phi_1,\phi_2^2) \Big(\sum_i \log g(x_i \given w, 1) \Big)dw \nonumber\\
&= \sum_i \Big( -\frac{1}{2} \log 2\pi - \frac{1}{2} ( \phi_1 -x_i)^2 -\frac{1}{2} \phi_2^2 \Big)
\end{align}
and
\begin{align}
D &= \int g(w \given \phi_1, \phi_2^2) \log \frac{p(w \given \phi_1,\phi_2^2)}{g(w\given \mu_0,\sigma^2_0)} dw\nonumber\\
&\frac{1}{2} \Big( \log \frac{\sigma^2_0}{\phi^2_2} +\frac{\phi_2^2}{\sigma_0^2} + \frac{(\phi_1- \mu_0)^2}{\sigma_0^2} -1\Big)
\end{align}
Taking Gradients
\begin{align}
\nabla_{\phi_1} L &= \sum_i (x_i - \phi_1) \\
\nabla_{\phi_2} L &= - \sum_i \phi_2\\
\end{align}
and
\begin{align}
\nabla_{\phi_1} D &= -\frac{\mu_0 - \phi_1}{\sigma_0^2}\\
\nabla_{\phi_2} D &=-\frac{1}{\phi_2} + \frac{\phi_2}{\sigma^2_0}
\end{align}
Bringing it together
\begin{align}
\nabla_{\phi_1} V &= \sum_i\Big( x_i - \phi_1 + \frac{\mu_0 - \phi_1}{n \sigma_0^2} \Big)\nonumber \\
\nabla_{\phi_2} V &= \sum_i \Big(-\phi_2   +  \frac{1}{n\phi_2} -\frac{ \phi_2}{n\sigma^2_0}  \Big)
\end{align}
We can do stochastic gradient ascent by sampling examples $x$ from the training set and modifying the parameters as follows
\begin{align}
\phi_1(k+1) &=\phi_1(k)  + \epsilon\Big(   x_i - \phi_1(k) + \frac{\mu_0 - \phi_1}{n \sigma_0^2}\Big)\\
\phi_2(k+1) &=\phi_2(k)  + \epsilon\Big(-\phi_2 (k)  + \frac{1}{n \phi_2(k)} - \frac{\phi_2(k)}{n \sigma^2_0}  \Big)
\end{align}
where $\epsilon$ is a small positive constant. 

\paragraph{Variance of the Gradient Estimate}
The variance of the gradient reduces to finding the variance of $Z(a + bZ)$ where $Z$ is a Standard Gaussian random variable and $a$, $b$ are constants

\begin{align}
Var[Z(a + bZ)] = E[Z^2(a+bZ)^2] - E[Z(a+bZ)]^2
\end{align}
where
\begin{align}
E[Z^2(a+bZ)^2]&= E[Z^2(a^2 + 2abZ + b^2Z^2)] =a^2E[Z^2] + 2abE[Z^3] + b^2 E[Z^4]\nonumber\\
&= a^2 + 3 b^2 
\end{align}
Moreover
\begin{align}
E[Z(a+bZ)] = b
\end{align}
Thus 
\begin{align}
Var[Z(a + bZ)] = a^2 + 2 b^2 
\end{align}

\newpage
\bibliographystyle{plainnat} % plainnat                                                        
\bibliography{vdc}



\end{document}
