import numpy as np
from matplotlib import pyplot as plt
import torch
from torch.utils.data import TensorDataset
from torch.utils.data import DataLoader
from torch.distributions import Normal
from scipy.stats import norm
from exponentialSmoother import *
import sys

#
# TODO: expected variance of the gradients
# TODO: sample multiple Ws per batch
#
# # Model
# X  ~ N(W,1)
# W ~ N(mu_0,sigma^2_0)
# d = {x_1, ...,x_n}
# q(w | phi) =  g(w| phi_1, phi_2^2) (g for Gaussian )
# q(w |phi) approx p(w | d)



class Model():
    # each model parameter comes equiped with a
    # variational gaussian distribution to approximate the marginal posterior
    # of the original model parameter
    # the parameters of the variational distribution are the mean and log
    # standard deviation
    def __init__(self, mu_0=0., sigma2_0 =1.,phi_1 = 0., log_phi_2= 0.,n_data=1):
        self.phi_1 =  torch.tensor(phi_1,requires_grad=True)
        self.log_phi_2 =  torch.tensor(log_phi_2,requires_grad=True)
        self.mu_0 =  torch.tensor(mu_0)
        self.sigma2_0 =  torch.tensor(sigma2_0)
        self.sigma_0 = np.sqrt(self.sigma2_0)
        self.n_data = float(n_data)
        # params are mean and std dev
        #self.q = torch.distributions.Normal(self.phi_1, self.log_phi_2.exp() )
        self.log={}
        self.log['phi_1']=[]
        self.log['log_phi_2']=[]
        self.log['V_mean']=[]
        self.log['V_var']=[]
        self.log['Grad_mean']=[]
        self.log['Grad_var']=[]
        self.log['Grad_mean_pytorch']=[]
        self.batchLog={}
        self.batchLog['V']=[]
        self.batchLog['V_grad']=[]

    def w_sample(self,n):
        return torch.distributions.Normal(self.phi_1, self.log_phi_2.exp()).rsample([torch.tensor(n)])

    def L(self,x_sample,w_sample):
        return Normal(w_sample,1).log_prob(x_sample).sum()

    def P(self,w_sample):
        return Normal(self.mu_0,self.sigma_0).log_prob(w_sample)/self.n_data

    def H(self, w_sample):
        return -torch.distributions.Normal(self.phi_1, self.log_phi_2.exp()).log_prob(w_sample)/self.n_data

    def V(self,x_sample,w_sample):
        V =  self.L(x_sample,w_sample)
        V = V+self.P(w_sample)
        V = V+self.H(w_sample)

        return(V)


    def V_grad(self,x_sample,w_sample):
        g1= -(w_sample - self.mu_0)/(self.sigma2_0*self.n_data)
        g1 = g1 - (w_sample - x_sample)
        phi_2= self.log_phi_2.exp()
        z = (w_sample-self.phi_1)/phi_2
        g2= ( w_sample- self.mu_0)/(self.sigma2_0*self.n_data) + (w_sample - x_sample)
        g2 = -phi_2*z*g2
        g2 = g2 +1./self.n_data
        return([float(g1),float(g2)])

    def updateBatch(self,x_sample,w_sample):
        V= self.V(x_sample,w_sample)
        V_grad = self.V_grad(x_sample,w_sample)
        self.batchLog['V'].append(float(V))
        self.batchLog['V_grad'].append(V_grad)
    def clearBatchLog(self):
        self.batchLog['V']=[]
        self.batchLog['V_grad']=[]

    def updateLog(self,x_sample):
        self.log['phi_1'].append(float(self.phi_1))
        self.log['log_phi_2'].append(float(self.log_phi_2))
        self.log['V_mean'].append(np.array(self.batchLog['V']).mean())
        self.log['V_var'].append(np.array(self.batchLog['V']).var())
        self.log['Grad_mean'].append(np.array(model.batchLog['V_grad']).mean(0))
        self.log['Grad_var'].append(np.array(model.batchLog['V_grad']).var(0))
        g1 = float(self.phi_1.grad)
        g2 = float(self.log_phi_2.grad)
        self.log['Grad_mean_pytorch'].append([g1,g2])




# DATA
inputs = np.array([[1.],[2.],[3.],[4.],[5.],[6.],[7.],[8.]])
inputs = torch.from_numpy(inputs)
n_data = len(inputs)
dataset = TensorDataset(inputs)
data_loader = DataLoader(dataset, batch_size=1, shuffle=True)


# MODEL
mu_0 = 0.
sigma2_0= 1
phi_1 = mu_0
log_phi_2 = (np.log(sigma2_0)/2.)
model = Model(mu_0=mu_0,sigma2_0 = sigma2_0,phi_1= phi_1,log_phi_2=log_phi_2,n_data=n_data)

# OPTIMIZER
nTrials=100
n_samples_per_update=50
lrate=0.2
optimizer = torch.optim.SGD([model.phi_1,model.log_phi_2], lr=lrate, momentum=0.0)




sample_count=0
loss=0
trial=0
while trial<nTrials:
    for x in data_loader:
        sample_count = sample_count+1
        x_sample = x[0].flatten()
        w_sample = model.w_sample(1)
        # acumulates manually computed grads
        model.updateBatch(x_sample,w_sample)
        loss = loss -model.V(x_sample,w_sample)
        if sample_count == n_samples_per_update:
            loss = loss/n_samples_per_update
            loss.backward(retain_graph=True)
            model.updateLog(x_sample)
            optimizer.step()
            trial = trial+1
            print('trial',trial)
            loss=0
            sample_count=0
            model.clearBatchLog()
            optimizer.zero_grad()


xbar = float(inputs.mean())
phi_1_solution= (xbar*n_data + mu_0/sigma2_0)/(n_data+ 1/sigma2_0)
log_phi_2_solution = np.log(np.sqrt((1/(n_data+1/sigma2_0))))
plt.subplot(5,1,1)
smoother = exponentialSmoother(1-lrate)
x=np.array(model.log['V_mean'])
Vs= smoother.smooth(x)
plt.plot(Vs)
plt.subplot(5,1,2)
x = np.array(model.log['phi_1'])
x_s = smoother.smooth(x)
plt.plot(x,'g')
plt.plot(x_s,'b')
plt.plot((0,nTrials),(phi_1_solution,phi_1_solution),'r--')
plt.subplot(5,1,3)
x = np.array(model.log['log_phi_2'])
plt.plot(x,'g')
x_s = smoother.smooth(x)
plt.plot(x_s,'b')
plt.plot((0,nTrials),(log_phi_2_solution,log_phi_2_solution),'r--')
plt.show(block=False)
plt.subplot(5,1,4)
x = np.array(model.log['Grad_mean'])
y = np.array(model.log['Grad_mean_pytorch'])
plt.scatter(x[:,0],-y[:,0])
plt.subplot(5,1,5)
plt.scatter(x[:,1],-y[:,1])
