import numpy as np

class exponentialSmoother():
    def __init__(self, alpha):
        self.alpha=alpha

    def filter(self,x):
        x = np.array(x)
        y = np.zeros(x.shape)
        n = x.size
        m = 1/(1-self.alpha)
        y[0]=x[0]
        for k in range(1,n):
            alpha = min((k-1)/k,(m-1)/m)
            y[k]= alpha*y[k-1]+(1-alpha)*x[k]
        return(y)
    def smooth(self,x):
        y = np.flip(self.filter(x))
        z = np.flip(self.filter(y))
        return(z)
