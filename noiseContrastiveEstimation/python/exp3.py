#Same as exp2 but
# using quadratic logistic regression with
# 3 parameters.

import torch
import torch.optim as optim
from torch.distributions.normal import Normal
import matplotlib.pyplot as plt

# Parameters and distributions
mu_d = 1.0  # Mean of data distribution
mu_r = 0.0  # Mean of reference distribution


theta_0 = 0   # Initial guesses
theta_1= mu_r - mu_d +1
theta_2=0


# Convert parameters to PyTorch tensors
theta = torch.tensor([theta_0,theta_1,theta_2], requires_grad=True)



# Logistic function
def logi(x):
    return 1 / (1 + torch.exp(-x))

# Probabilities and log likelihood
def log_likelihood(x_d, x_r, theta, mu_r):
    g_d = theta[0] + theta[1]*x_d + theta[2]*(x_d**2)
    g_r = theta[0] + theta[1]*x_r + theta[2]*(x_r**2)
    w_d = logi(g_d)
    w_r = logi(g_r)
    return torch.mean(torch.log(w_d)) + torch.mean(torch.log(1-w_r))

def prob_correct(x_d, x_r, theta, mu_r):
    g_d = theta[0] + theta[1]*x_d + theta[2]*(x_d**2)
    g_r = theta[0] + theta[1]*x_r + theta[2]*(x_r**2)
    p_d = torch.mean((logi(g_d) > 0.5).float())
    p_r = torch.mean((logi(-g_r) > 0.5).float())
    return p_d, p_r

# Data generation function
def generate_data(n_d, n_r, mu_d, mu_r):
    dist_d = Normal(mu_d, 1.0)
    dist_r = Normal(mu_r, 1.0)
    return dist_d.sample((n_d,)), dist_r.sample((n_r,))

# Learning parameters
n_d, n_r = 10000, 10000
epsilon_theta = 0.1  # Learning rate for theta
nTrials = 1000

# Optimizers
optimizer_theta = optim.SGD([theta], lr=epsilon_theta)


# Lists for plotting
likelihoodList, thetaList, mu_rList, pc_dList, pc_rList = [], [], [], [], []

# Learning process
for t in range(nTrials):
    x_d, x_r = generate_data(n_d, n_r, mu_d, mu_r)

    # Zero gradients for both optimizers
    optimizer_theta.zero_grad()


    # Compute loss and backpropagate
    l = -log_likelihood(x_d, x_r, theta, mu_r) # Negative LL for minimization
    l = l+0.001* torch.norm(theta) # regularizer
    pc_d, pc_r = prob_correct(x_d, x_r, theta, mu_r)
    pc_dList.append(pc_d.item())
    pc_rList.append(pc_r.item())

    # Record values for plotting
    likelihoodList.append(-l.item())  # Convert back to positive LL
    # we can estimate the mean from the parameter linear on the data
    muhat = theta[1].item()+mu_r
    thetaList.append(muhat)


    l.backward()

    # Update parameters
    optimizer_theta.step()




# Plotting
plt.figure(figsize=(15, 10))

# Theta and Mu_r evolution
plt.subplot(2, 2, 1)
plt.plot(thetaList, 'r', label='Theta')
plt.axhline(y=mu_d, color='g', linestyle='-', label='Mu_d (target for Theta)')
plt.title('Parameter Evolution')
plt.xlabel('Iteration')
plt.ylabel('Parameter Value')
plt.legend()

# Likelihood
plt.subplot(2, 2, 2)
plt.plot(likelihoodList)
plt.title('Log Likelihood')
plt.xlabel('Iteration')
plt.ylabel('Log Likelihood')

# Probability of Correct Classification
plt.subplot(2, 2, 3)
plt.plot(pc_dList, 'r', label='P(Correct|Data)')
plt.plot(pc_rList, 'b', label='P(Correct|Reference)')
plt.title('Probability of Correct Classification')
plt.xlabel('Iteration')
plt.ylabel('Probability')
plt.legend()

plt.tight_layout()
plt.show()
