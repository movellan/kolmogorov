import torch
import torch.optim as optim
from torch.distributions.normal import Normal
import matplotlib.pyplot as plt

# Parameters and distributions
mu_d = 0.0  # Mean of data distribution
theta_0 = 2.0  # Initial guess about mean of data
mu_r = theta_0  # Mean of reference distribution

# Convert parameters to PyTorch tensors
theta = torch.tensor([theta_0], requires_grad=True)
mu_r = torch.tensor([mu_r], requires_grad=True)

# Logistic function
def logi(x):
    return 1 / (1 + torch.exp(-x))

# Probabilities and log likelihood
def log_likelihood(x_d, x_r, theta, mu_r):
    g_d = 0.5 * ((x_d - mu_r) ** 2 - (x_d - theta) ** 2)
    g_r = 0.5 * ((x_r - mu_r) ** 2 - (x_r - theta) ** 2)
    w_d = logi(g_d)
    w_r = logi(-g_r)
    return torch.sum(torch.log(w_d)) + torch.sum(torch.log(w_r))

def prob_correct(x_d, x_r, theta, mu_r):
    g_d = 0.5 * ((x_d - mu_r) ** 2 - (x_d - theta) ** 2)
    g_r = 0.5 * ((x_r - mu_r) ** 2 - (x_r - theta) ** 2)
    p_d = torch.mean((logi(g_d) > 0.5).float())
    p_r = torch.mean((logi(-g_r) > 0.5).float())
    return p_d, p_r

# Data generation function
def generate_data(n_d, n_r, mu_d, mu_r):
    dist_d = Normal(mu_d, 1.0)
    dist_r = Normal(mu_r.item(), 1.0)
    return dist_d.sample((n_d,)), dist_r.sample((n_r,))

# Learning parameters
n_d, n_r = 50000, 50000
epsilon_theta = 0.00001  # Learning rate for theta
epsilon_mu_r = 0.00001  # Learning rate for mu_r
nTrials = 200

# Optimizers
optimizer_theta = optim.SGD([theta], lr=epsilon_theta)
optimizer_mu_r = optim.SGD([mu_r], lr=epsilon_mu_r)

# Lists for plotting
likelihoodList, thetaList, mu_rList, pc_dList, pc_rList = [], [], [], [], []

# Learning process
for t in range(nTrials):
    x_d, x_r = generate_data(n_d, n_r, mu_d, mu_r)

    # Zero gradients for both optimizers
    optimizer_theta.zero_grad()
    optimizer_mu_r.zero_grad()

    # Compute loss and backpropagate
    l = -log_likelihood(x_d, x_r, theta, mu_r) # Negative LL for minimization
    pc_d, pc_r = prob_correct(x_d, x_r, theta, mu_r)
    pc_dList.append(pc_d.item())
    pc_rList.append(pc_r.item())

    # Record values for plotting
    likelihoodList.append(-l.item())  # Convert back to positive LL
    thetaList.append(theta.item())
    mu_rList.append(mu_r.item())


    l.backward()

    # Update parameters
    optimizer_theta.step()
    optimizer_mu_r.step()



# Plotting
plt.figure(figsize=(15, 10))

# Theta and Mu_r evolution
plt.subplot(2, 2, 1)
plt.plot(thetaList, 'r', label='Theta')
plt.plot(mu_rList, 'b--', label='Mu_r')
plt.axhline(y=mu_d, color='g', linestyle='-', label='Mu_d (target for Theta)')
plt.title('Parameter Evolution')
plt.xlabel('Iteration')
plt.ylabel('Parameter Value')
plt.legend()

# Likelihood
plt.subplot(2, 2, 2)
plt.plot(likelihoodList)
plt.title('Log Likelihood')
plt.xlabel('Iteration')
plt.ylabel('Log Likelihood')

# Probability of Correct Classification
plt.subplot(2, 2, 3)
plt.plot(pc_dList, 'r', label='P(Correct|Data)')
plt.plot(pc_rList, 'b', label='P(Correct|Reference)')
plt.title('Probability of Correct Classification')
plt.xlabel('Iteration')
plt.ylabel('Probability')
plt.legend()

plt.tight_layout()
plt.show()
