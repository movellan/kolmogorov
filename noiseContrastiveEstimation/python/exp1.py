import numpy as np
from matplotlib import pyplot as plt

# Noise Contrast Estimation
# of the mean of a Gaussian distribution
# we use gradient ascent on data/noise discrimination with respect to
# the data model and gradient descent with respect to the reference model,
# i.e., we use an adversarial version of NCE

# Parameters and distributions
mu_d = 1  # Mean of data distribution
theta_0 = 5  # Initial guess about mean of data
mu_r = theta_0  # Mean of reference distribution
pi_d = 0.5  # Prior for the data distribution

# Logistic function
def logi(x):
    return 1 / (1 + np.exp(-x))

# Probabilities and gradients
def p_d_given_x(x, theta, mu_r):
    g = 0.5 * ((x - mu_r) ** 2 - (x - theta) ** 2)
    return logi(g)

def p_r_given_x(x, theta, mu_r):
    return 1 - p_d_given_x(x, theta, mu_r)

def grad_theta(x_d, x_r, theta, mu_r):
    w_d = p_r_given_x(x_d, theta, mu_r)
    sum_d = np.sum(w_d * x_d)
    n_d = np.sum(w_d)
    w_r = p_d_given_x(x_r, theta, mu_r)
    sum_r = np.sum(w_r * x_r)
    n_r = np.sum(w_r)
    gr = sum_d - n_d * theta
    gr = gr - (sum_r - n_r * theta)
    return gr

def grad_mu_r(x_d, x_r, theta, mu_r):
    w_d = p_r_given_x(x_d, theta, mu_r)
    sum_d = np.sum(w_d * x_d)
    n_d = np.sum(w_d)
    w_r = p_d_given_x(x_r, theta, mu_r)
    sum_r = np.sum(w_r * x_r)
    n_r = np.sum(w_r)
    gr = -(sum_d - n_d * mu_r)
    gr = gr + (sum_r - n_r * mu_r)
    return gr

def log_likelihood(x_d, x_r, theta, mu_r):
    w_d = p_d_given_x(x_d, theta, mu_r)
    w_r = p_r_given_x(x_r, theta, mu_r)
    return np.sum(np.log(w_d)) + np.sum(np.log(w_r))

def prob_correct(x_d, x_r, theta, mu_r):
    p_d = sum(p_d_given_x(x_d, theta, mu_r) > 0.5) / len(x_d)
    p_r = sum(p_r_given_x(x_r, theta, mu_r) > 0.5) / len(x_r)
    return p_d, p_r

# Data generation and parameter initialization
n_d, n_r = 100, 100
x_d = np.random.normal(loc=mu_d, scale=1, size=n_d)
x_r = np.random.normal(loc=mu_r, scale=1, size=n_r)
theta, epsilon, alpha = theta_0, 0.0001, 0.0001
nTrials = 4000

# Lists for plotting
likelihoodList, pc_dList, pc_rList, thetaList, mu_rList = [], [], [], [], []

# Learning process
for t in range(nTrials):
    x_d = np.random.normal(loc=mu_d, scale=1, size=n_d)
    x_r = np.random.normal(loc=mu_r, scale=1, size=n_r)

    l = log_likelihood(x_d, x_r, theta, mu_r)
    likelihoodList.append(l)
    pc_d, pc_r = prob_correct(x_d, x_r, theta, mu_r)
    pc_dList.append(pc_d)
    pc_rList.append(pc_r)
    thetaList.append(theta)
    mu_rList.append(mu_r)
    gr = grad_theta(x_d, x_r, theta, mu_r)
    theta = theta + epsilon * gr
    mu_r = mu_r - alpha * grad_mu_r(x_d, x_r, theta, mu_r)

# Plotting
plt.figure(figsize=(15, 5))

# Theta and Mu_r evolution
plt.subplot(1, 3, 1)
plt.plot(thetaList, 'r', label='Theta')
plt.plot(mu_rList, 'r--', label='Mu_r')
plt.axhline(y=mu_d, color='b', linestyle='-', label='Mu_d (target for Theta)')
plt.title('Parameter Evolution')
plt.xlabel('Iteration')
plt.ylabel('Parameter Value')
plt.legend()

# Likelihood
plt.subplot(1, 3, 2)
plt.plot(likelihoodList)
plt.title('Log Likelihood')
plt.xlabel('Iteration')
plt.ylabel('Log Likelihood')

# Probability of Correct Classification
plt.subplot(1, 3, 3)
plt.plot(pc_dList, 'r', label='P(Correct|Data)')
plt.plot(pc_rList, 'b', label='P(Correct|Reference)')
plt.title('Probability of Correct Classification')
plt.xlabel('Iteration')
plt.ylabel('Probability')
plt.legend()

plt.tight_layout()
plt.show()
