\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}

 \title{Bayesian Inferences about Unobserved Means and Variances.}
\author{Javier R. Movellan}


\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\R}{\mathcal{R}} 





\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}



\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{lem}{Lemma}[section]
\theoremstyle{definition}
\newtheorem{rem}{Remark}[section]
\theoremstyle{definition}
\newtheorem{ex}{Example}[section]




\usepackage{verbatim}

\begin{document}
\maketitle

    
\newpage

\section{Summary}
This document presents the Normal-Gamma Bayesian approach for estimating the mean $\mu$ and variance $\sigma^2$ of a Gaussian random variable  $X$ based on  $n$ iid samples  $x_{1:n} = (x_1,\cdots, x_n)$  from that variable.  In some cases it s useful to work with the inverse of the variance, also known as the precision. Here we use $\lambda$ to represent the precision, i.e.,  $\lambda = \sigma^{-2}$.  The posterior distribution of $\mu, \sigma^2$ is defined by four parameters: $\theta_n, \kappa_n, \alpha_n, \beta_n$.  

The statistics of the posterior distribution are as follows:
\begin{align}
&E[\mu\given x_{1:n} ] = \theta_n\\
&Var[\mu \given x_{1:n}] = \frac{E[\sigma^2 \given x_{1:n}]}{\kappa_n}\\
&E[\sigma^2\given x_{1:n}] = \frac{\beta_n}{\alpha_n -1}\\
&Var[\sigma^2 \given x_{1:n}] =  \frac{E^2[\sigma^2\given x_{1:n}]}{\alpha_n-2}, \; \text{for $\alpha_n >2$}\\
&E[\lambda \given x_{1:n}] = \frac{\alpha_n}{\beta_n}\\
&Var[\lambda\given x_{1:n}] =   E^2[\lambda\given x_{1:n}] \beta_0\\
&E[X\given x_{1:n}] = \theta_n\\
&Var[X\given x_{1:n}] = E[\sigma^2\given x_{1:n}]  \frac{\kappa_n+1}{\kappa_n} = Var[\mu\given x_{1:n} ]  (\kappa_n +1)
\end{align}
The posterior distribution parameters are obtained  as follows:
\begin{align}
&\theta_n = \frac{\kappa_0 \theta_0 + n \bar x_n}{\kappa_0 + n}\\
&\kappa_n = \kappa_0 + n\\
&\alpha_n = \alpha_0 + \frac{n}{2}\\
&\beta_n = \beta_0 + \frac{1}{2} ( n s_n^2 + \frac{k_0 n}{k_0 +n} (\bar x_n - \theta_0)^2)
\end{align}
where
\begin{align}
& \bar x_n = \frac{1}{n} \sum_{i=1}^n x_i\\
&s^2_n = \frac{1}{n} \sum_{i=1}^n (x_i - \bar x)^2
\end{align} 
and $\alpha_0,\beta_0,\theta_0,\kappa_0$ are the parameters of the prior distribution. 





\section{The Normal Gamma Model: Unobserved Mean and Variance}
Here we focus on  presentation of Bayesian methods for making inferences about  unobserved means and variances using the Normal Gamma Models, a convenient model under which Bayesian inference is  computationally efficient.  For mathematical proofs please refer to on \cite{Murphy07conjugatebayesian}.
We observe iid variables $X_1,\cdots, X_n$  with unobserved  mean $\mu$ and variance $\sigma^2$. We want to make inferences about the unobserved  parameters based on the observed variables. 
In some cases it is convenient to work with the inverse of the variance, also known as the precision, rather than with the variance. Here we use $\lambda$ to represent the precision, i.e.,  $\lambda=\sigma^{-2}$. We use the following generative model for the precision $\lambda$ and variance $\mu$: 
\begin{align}
p(\lambda, \mu) &= p(\lambda) p(\mu \given \lambda) \\
p(\lambda) &= \Gamma(\lambda \given \alpha_0, \beta_0) \propto \lambda^{\alpha_0 -1} e^{-\lambda \beta_0}\\
p(\mu \given \lambda) &= N(\mu \given \theta_0, (\lambda \kappa_0)^{-1}) \nonumber\\
&\propto  \lambda^{1/2} e^{-\frac{1}{2} \lambda\kappa_0 (\mu - \theta_0)^2} \\
p(x \given \mu,\lambda) &= N(x\given \mu, \sigma^2) \propto  \lambda^{1/2} e^{-\frac{1}{2} \lambda (x - \mu)^2} 
\end{align}
The generative model has 4 parameters: $\theta_0, \kappa_0, \alpha_0,\beta_0$.  The generation process is as follows. First a precision value $\lambda=\sigma^{-2} $ is generated from the prior Gamma distribution. Then $\mu$ is sampled from a prior normal, that has mean $\theta_0$ and variance $\sigma^2/\kappa_0$. Then an observable $X$ is generated from a Normal distribution with mean $\mu$ and variance $\sigma^2$. Here $\kappa_0$ is a prior parameter that scales the uncertainty of the mean with respect to the uncertainty of individual observations. It plays the role of the number of prior (virtual) observations that the prior belief about the man is based on. At inference time  we observe $X_1, \cdots,X_n$, i.i.d, Gaussian variables  with mean $\mu$ and variance $\lambda^{-1}$ hidden to us. Our goal are to make inferences based on the observed data:
\begin{align}
&p(\mu,\lambda \given x_{1:n}), \; \text{the posterior distribution of the mean and precision}\\
&p(\mu,\sigma^2 \given x_{1:n}), \; \text{the posterior distribution of the mean and variance}\\
& p(\mu  \given x_{1:n}), \; \text{the posterior distribution of the  mean}\\
& p(\lambda \given x_{1:n}), \; \text{the posterior distribution of the   precision}\\
& p(\sigma^2 \given x_{1:n}), \; \text{the posterior distribution of the   variance}\\
&p(x_{n+1} \given x_{1:n}), \;\text{the predictive distribution}\\
\end{align}
\subsection{Prior Marginal Distributions}
The marginal prior for the precision $\lambda$ is Gamma with shape parameter $\alpha_0$ and rate parameter $\beta_0$. Equivalently, the marginal prior for $\sigma^2$ is Inverse Gamma with shape parameter $\alpha_0$ and rate parameter $\beta^{-1}_0$.  This entails the following statistics:
\begin{align}
&E[\lambda] = \frac{\alpha_0} {\beta_0}\\
&Var[\lambda]= \frac{\alpha_0}{ \beta_0^2}= E^2[\lambda] \beta_0\\
&E[\sigma^2]= E[\sigma^2] = \frac{\beta_0}{\alpha_0 -1}  = \frac{1}{E^[\sigma^{-2}] } \frac{\alpha_0}{\alpha_0 - 1}\\
&Var[\sigma^2] =  \frac{E^2[\sigma^2]}{\alpha_0-2}, \; \text{for $\alpha_0 >2$}
\end{align}
See Appendix on Gamma and Inverse Gamma distributions for an in depth explanation. 


Regarding the marginal prior distribution of the mean, first we define the distribution of a  non-central $T$ random variable $X$ with $\nu$ degrees of freedom, location parameter $\mu$ and scale parameter $\epsilon$ as follows
\begin{align}
p(x\given \mu,\epsilon^2) \propto \left[  1+ \frac{1}{\nu} (\frac{x-\mu}{\epsilon})^2 \right]^{-(\nu+1)/2}
\end{align}
It can be shown
\begin{align}
&E[X] = \mu\\
&Var[X] = \epsilon^2 \frac{\nu}{\nu-2}, \text{for $\nu>2$}
\end{align}
The random variable $(X-\mu)/\epsilon$ is a standard  $T$ distribution with $\nu$ degrees of freedom. 

It can be shown that the marginal prior of the mean  has a T distribution with  $2\alpha_0$ degrees of freedom,  location parameter $\theta_0$ and scale parameter $\sqrt{\beta_0/( \kappa_0\alpha_0)}$
Thus
\begin{align}
&E[\mu] = \theta_0\\
&Var[\mu] =  \frac{\beta_0}{\kappa_0 \alpha_0} \frac{ 2\alpha_0}{2\alpha_0 -2} = \frac{\beta_0}{\kappa_0 (\alpha_0-1) } \nonumber\\
& = \frac{E[\sigma^2] }{\kappa_0}
\end{align}
Where we used the fact in the Appendix that $E[\sigma^2] = \frac{\beta}{\alpha-1}$.
In the next section we see that $\kappa_0$ can be interpreted as the number of prior observations supporting $\theta_0$. Thus we can interpret the prior variance of the mean as the expected variance for individual observations divided by the number of observations supporting $\theta_0$.
\subsection{Posterior Distributions}
\paragraph{Joint Posterior of $\mu,\lambda$:}
It can be shown that the posterior distribution of $\mu,\lambda$ given $n$ iid observations $x_{1:n} = x_1,\cdots, x_n$ is Normal Gamma with parameters

\begin{align}
&\theta_n = \frac{\kappa_0 \theta_0 + n \bar x_n}{\kappa_0 + n}\\
&\kappa_n = \kappa_0 + n\\
&\alpha_n = \alpha_0 + \frac{n}{2}\\
&\beta_n = \beta_0 + \frac{1}{2} ( n s_n^2 + \frac{k_0 n}{k_0 +n} (\bar x_n - \theta_0)^2)
\end{align}
where
\begin{align}
& \bar x_n = \frac{1}{n} \sum_{i=1}^n x_i\\
&s^2_n = \frac{1}{n} \sum_{i=1}^n (x_i - \bar x)^2
\end{align} 
\paragraph{Marginal Posterior of $\mu$:}
The marginal posterior of $\mu$ is a T distribution with $2\alpha_n$ degrees of freedom,  location parameter $\theta_n$ and scale parameter $\sqrt{\beta_n/( \kappa_n\alpha_n)}$
Thus
\begin{align}
&E[\mu \given x_{1:n}] = \theta_n\\
&Var[\mu \given x_{1:n}] =  \frac{\beta_n}{\kappa_n\alpha_n} \frac{ 2\alpha_n}{2\alpha_n -2} = \frac{\beta_n}{\kappa_n (\alpha_n-1) } \nonumber\\
& = \frac{E[\sigma^2\given x_{1:n}] }{\kappa_0}
\end{align}
We can interpret the posterior variance of $\mu$ as the inverse of the expected precision of individual observations, divided by the number of observations times the $\alpha_n/(\alpha_n -1)$ factor. 

\paragraph{Marginal Posterior of $\lambda$:}
The marginal posterior of the precision is Gamma with parameters $\alpha_n, \beta_n$
\begin{align}
p(\lambda \given x_{1:n}) = \Gamma(\lambda \given \alpha_n, \beta_n) \propto \lambda^{\alpha_n -1} e^{-\lambda \beta_n}
\end{align}



\section{Predictive distribution}
The predictive distribution for a new observation $X_{n+1}$ given the observed vector $x_{1:n}$ is $T$ with $2 \alpha_n$ degrees of freedom, location parameter $\theta_n$ and scale parameter $\gamma_n$ defined as follows
\begin{align}
\gamma_n = \sqrt{\frac{ \beta_n}{\alpha_n}\frac{\kappa_{n} +1 }{\kappa_n} }= \sqrt{E^{-1}[\lambda\given x_{1:n}] \frac{\kappa_{n} +1 }{\kappa_n} } = \epsilon_n \sqrt{\kappa_n+1}
\end{align}
where $\epsilon_n$ is the scale parameter of the posterior distribution for $\mu$.
Thus
\begin{align}
&E[X_{n+1}\given x_{1:n}] = \theta_n\\
&Var[X_{n+1} \given x_{1:n}]  = \gamma^2_n \frac{2 \alpha_n}{2\alpha_n -2}\nonumber\\
&\;=\frac{\beta_n}{(\alpha_n -1)\kappa_n} ( \kappa_{n} +1) =Var[\mu\given x_{1:n}] (\kappa_n+1) \nonumber\\
&= E[\sigma^2\given x_{1:n}] \frac{\kappa_n+1}{\kappa_n}
\end{align}
Finally lets get the predictive distribution of the sum and mean of $m$ future variables
\begin{align}
&S= \sum_{i=1}^m X_{n+i}\\
&M = \frac{S}{m}
\end{align}
It follows that 
\begin{align}
&E[S\given x_{1:n}] = m E[X_{n+1}\given x_{1:n}] = m \theta_n\\
&Var[S \given x_{1:n}] = m Var[S\given x_{1:n}]\\
&E[M \given x_{1:n}] = E[X_{n+1}\given x_{1:n}] = \theta_n\\
&Var[M \given x_{1:n} ] = \frac{1}{n^2} Var[S\given x_{1:n}]
\end{align}
and using the Central Limit Theorem we can approximate the posterior distribution as Normal with the mean and variance parameters above. 


 \section{Comparison with Frequentist Approach}
 
We observe $n$ iid Gaussian random variables $X_1, \cdot,X_n$ with unknown mean $\mu$ and variance $\sigma^2$. Let
\begin{align}
&\bar X = \frac{1}{n} \sum_{i=1}^n X_i\\
&S^2 = \frac{1}{n-1} \sum_{i=1}^n (X_i - \bar X)^2\\
&V = \frac{ (n-1) S^2}{\sigma^2}\\
&Z = \frac{\bar X - \mu}{\sqrt{S^2/n}}
 \end{align}
 \paragraph{Confidence Intervals for $\sigma^2$:}
It can be shown that $V$ has a chi-square distribution with $n-1$ df.  From the properties of the chi-square distribution it is interesting to note that
\begin{align}
&E[V] = n-1 \\
&Var[V] = 2 (n-1)\\
\end{align}
Thus,
\begin{align} 
&E[S^2] = \sigma^2\\
&Var[S^2] = 2 \sigma^2{n-1}
\end{align}
The fact tht $V$ ha a chi-squared distribiution can be used to get upper bounds, lower bounds, and confidence intervals for $\sigma^2$. In particular, let $1-\gamma$ be the desired confidence level, e.g. $\gamma = 0.05$ for $95 \%$ confidence,  and $v$ be a value such that $P(V< v) = 1- \gamma$. This can be obtained from the chi-square cumulative  distribution function with $n-1$ df.  Note
\begin{align}
&P(V < v) = P( \frac{ (n-1) S^2}{\sigma^2} < v)  = 1-  \gamma\\
&P( \lambda <  \frac{v}{(n-1) S^2} ) =   1- \gamma\\
\end{align}
where $\lambda = 1/\sigma^2$. Thus $\frac{v}{(n-1)S^2}$  upper bounds $\lambda$ with probability $1-\gamma$. Similarly if $v'$ is the value such that $P(V<v) = \gamma$ then 
\begin{align}
P( \lambda <  \frac{v'}{(n-1) S^2} ) =   \gamma\\
P(\lambda > \frac{v'}{(n-1) S^2}) = 1- \gamma
\end{align}
Thus $\frac{v'}{(n-1)S^2}$ lower bounds $\lambda$ with probability $1-\gamma$. Alternatively, since 
\begin{align}
P( \lambda <  \frac{v}{(n-1) S^2} ) =   1- \gamma\\
P( \lambda >  \frac{v'}{(n-1) S^2} ) =   1- \gamma
\end{align}
then
\begin{align}
P( \sigma^2 >  \frac{v'}{(n-1) S^2} ) =   1- \gamma\\
P( \sigma^2 < \frac{v}{(n-1) S^2} ) =   1- \gamma
\end{align}
Thus if $\lambda_lu\sigma^2_u$ are the $1-\gamma$ confidence upper bounds for $\lambda,\sigma^2$ and $\lambda_l, \sigma^2_l$ are the $1-\gamma$ confidence lower bounds then
\begin{align}
&\sigma^2_u = 1/\lambda_l\\
&\sigma^2_l = 1/\lambda_u
\end{align}
A similar approach can be used to get confidence intervals. To see the relationship with the Bayesian bounds we can use the fact that  the chi-square distribution  with $n-1$ degrees of freedom is a Gamma distribution with $\alpha = n-1$ and $\beta = 1/2$ and using the properties of the cumulative Gamma distribution it can be shown that
\begin{align}
\frac{v}{(n-1) S^2} = F^{-1}(1-\gamma)
\end{align}
where $F^{-1}$ is the inverse Gamma cumulative distribution function with parameters $\alpha = (n-1)/2$, $\beta = (n-1)S^2/2$. Thus \begin{align}
&P( \lambda <  W) = 1-\gamma
\end{align}
where $W$ is the value such that the Gamma cumulative distribution function with parameters $\alpha = (n-1)/2$, $\beta = (n-1)S^2/2$ evaluated at $v$ is $1-\gamma$. 

 \paragraph{Confidence Intervals for $\mu$:}
 It can be shown that $Z$ has a T distribution with $n-1$ df.  This can be used to get upper bounds, lower bounds, and confidence intervals for $\mu$. In particular, let $1-\alpha$ be the desired confidence level, e.g. $\alpha = 0.05$ for $95 \%$ confidence,  and $z$ be a value such that $P(Z < z) = \alpha$. This can be obtained from the T cumulative distribution function with $n-1$ df. Note
 \begin{align}
&P(Z < z) =  \alpha\\ 
&P( \frac{\bar X - \mu}{\sqrt{S^2/n}}  < z)  =  \alpha\\
&P(- \mu < - \bar X + z \sqrt{S^2/n}) = \alpha\\
& P( \mu >  \bar X -  z \sqrt{S^2/n}) = \alpha
 \end{align}
Thus $\bar X -  z \sqrt{S^2/n}$ upper bounds $\mu$ with probability $1-\alpha$. A similar approach can be used to get lower bounds and confidence intervals. 


 
 


\section{Appendix}
\subsection{Sequential Update of the Mean and  Variance}
We get an initial sample $x_1, \cdots,x_n$ with mean $\bar x$ and variance $s^2_x$
\begin{align}
&\bar x = \frac{\sum_{i=1}^n x_i}{n}\\
&s^2_x = \frac{\sum_{i=1}^n (x_i - \bar x)^2}{n}
\end{align}
We then get a sencond sample $y_1,\cdots, y_m$ with mean $\bar y$ and variance $s^2_y$
\begin{align}
&\bar y = \frac{\sum_{i=1}^m y_i}{m}\\
&s^2_y = \frac{\sum_{i=1}^n (y_i - \bar y)^2}{m}
\end{align}

Here we show that we can compute the overall mean and variance based on the sample means and variances
\begin{align}
\mu &= \frac{\sum_{i=1}^n x_i + \sum_{i=1}^m y_i}{n+m} = \frac{n \bar x + m \bar y}{n+m}\\
\sigma^2 &= \frac{\sum_{i=1}^n(x_i -\mu)^2 + \sum_{i=1}^m (y_i- \mu)^2}{n+m} \nonumber\\
&= \frac{n s^2_x + m s^2_y + nm (\bar x - \bar y)^2/(n+m)}{n+m}
\end{align}
The equation for the sum is obvious. Regarding the variance note
\begin{align}
&\sum_{i=1}^n(x_i - \mu)^2 = \sum_{i=1}^n(x_i - \bar x)^2 + n (\bar x - \mu)^2\\
&\sum_{i=1}^m(y_i - \mu)^2 = \sum_{i=1}^m(y_i - \bar y )^2 + m (\bar y - \mu)^2
\end{align}
Moreover
\begin{align}
\bar x - \mu &= \frac{\sum_{i=1}^n x_i}{n} - \frac{\sum_{i=1}^n x_i}{n+m} -\frac{\sum_{i=1}^m y_i}{n+m}\nonumber\\
&= (\sum_{i=1}^n x_i)(\frac{1}{n} - \frac{1}{n+m} ) - \frac{\sum_{i=1}^m y_i}{n+m}\nonumber\\
& = \frac{m}{m+n}(\bar x - \bar y)
\end{align}
Similarly
\begin{align}
\bar y - \mu = \frac{n}{m+n}(\bar y - \bar x)
\end{align}
Thus
\begin{align}
n (\bar x -\mu)^2 + m(\bar y - \mu)^2 &= \frac{n m^2}{(n+m)^2}  (\bar x - \bar y)^2 
+ \frac{m n^2}{(n+m)^2} \nonumber\\
&= \frac{n m}{n+m} (\bar x - \bar y)^2 
\end{align}
From which the desired formula follows.
\subsection{ Gamma and Inverse Gamma Probability Densities}
$X$ is a Gamma random variable with shape parameter $\alpha$ and rate parameter $\beta$ if 
\begin{align}
p(x\given \alpha, \beta) \propto x^{\alpha-1} e^{-x \beta}
\end{align}
To interpret the $\alpha$ and $\beta$ parameters it is helpful to consider that if $Y_1, ...,Y_{\alpha}$ are exponential random variables with rate parameter $\beta$, i.e.
\begin{align}
p(y_i) \propto e^{-\beta y_i}
\end{align}
Then $X= \sum_{i=1}^\alpha Y_i$ is a Gamma random variable with shape parameter $\alpha$ a rate parameter $\beta$. Thus we can interpret $\alpha$ as degrees of freedom contributing to the sum and $\beta$ as the rate of the underlying exponential variables. 
Below are some useful properties
\begin{align}
&E[X] = \frac{\alpha}{\beta}\\
&Var[X] = \frac{\alpha}{\beta^2}\\
&Mode[X] = \begin{cases}
&\frac{\alpha-1}{\beta},\;\text{for $\alpha \geq 1$}\\
&0,\;\text{else}
\end{cases}
\end{align}
If $X$ is Gamma with shape $\alpha$ and rate $\beta$ then $Y=X^{-1}$ is Inverse Gamma with shape $\alpha$ and rate $\beta^{-1}$
\begin{align}
p(y) \propto  y^{-\alpha -1} e^{-\beta/y}
\end{align}
With the following properties
\begin{align}
&E[Y] = \frac{\beta}{\alpha -1},\;\text{for $\alpha>1$}\\
&Var[Y] = \frac{\beta^2}{(\alpha-1)^2 (\alpha-2)} = \frac{E^2[Y]}{\alpha-2},\;\text{for $\alpha>2$}\\
&Mode[Y]= \frac{\beta}{\alpha+1}
\end{align}
Note: The Gamma distribution is also often parameterized by the inverse of the rate $\beta$,  called the scale. So if $X$ is Gamma with scale parameter $\beta^{-1}$ then $X^{-1}$ is inverse Gamma with scale parameter $\beta$.
\subsection{T distribution}
The density of a student T distribution with $\nu$ degrees of freedom, location parameter $\mu$ and scale parameter $\sigma$ is as follows
\begin{align}
f(x) \propto (1+ \frac{[(x-\mu)/\sigma]^2}{\nu})^{-\frac{\nu+1}{2}}
\end{align}
and it has the following statistics
\begin{align}
&E[X] = \mu\\
&Mode[X]=\mu\\
&Median[X]=\mu\\
&Var[Y]= \frac{\nu}{\nu-2} \sigma^2,\;\text{for $\nu>2$}
\end{align}

\subsection{Log Normal Distribution}
If $X\sim N(\mu,\sigma^2$ then $Y=e^X$ is log Normal with parameters $\mu,\sigma^2$. It can be shown
\begin{align}
&E[Y] = e^{\mu + \sigma^2/2}\\
&Mode[Y]=e^{\mu-\sigma^2}\\
&Median[Y]= e^\mu\\
&Var[Y]= (e^{\sigma^2}-1)e^{2\mu+\sigma^2}
\end{align}
\paragraph{Moment Matching}
The values of $\mu,\sigma^2$ that match $E[X]$, $Var[X]$ are as follows

\begin{align}
&\sigma^2= \log(E[Y^2]/E^2[Y])\\
 &\mu = \log(E[Y]) - \sigma^2/2
 \end{align}
where $E[Y^2] = Var[Y] + E^2[Y]$. 
 \paragraph{Sum of log normals}

 Let $S = Y_1, \cdots, Y_n$ where $Y_i$ are iid log normals with parameters $\mu,\sigma^2$.  Fenton Wilkinson show that $S$ is approximately log normal with $\mu_s, \sigma^2_s$ set to match $E[S] = n E[Y]$ and $Var[S] = n Var[Y]$, See Figure~\ref{fig:sumOfLogNormals}.

\begin{figure}[bht]\label{fig:sumOfLogNormals}
\begin{center}
\includegraphics[width=3in]{figures/sumOfLogNormals}
\end{center}
  \caption{Histograms of sum of $n$ log normals $\mu=\sigma=1$ and fit using Fenton-Wilkinson moment matching method.}
 \end{figure}

\end{document}

\subsection{Evidence}
The evidence for the model is the likelihood of the observed data given the model. It can be shown to be as follows
\begin{align}
p(x_{1:n}) = \frac{\Gamma(\alpha_n)}{\Gamma(\alpha_0)}\frac{\beta_0^{\alpha_0}}{\beta_n^{\alpha_n}} \left(\frac{\kappa_0}{\kappa_n}\right)^{1/2} (2\pi)^{-n/2}
\end{align}


\subsection{A note on the update equations}


Considering that
\begin{align}
E[\sigma^2] = \frac{\beta_0}{\alpha_0 -1}\\
E[\sigma^2\given x] = \frac{\beta_n}{\alpha_n-1}
\end{align}

Then the prior to posterior update equations reduce to the standard sequential update equations for the sample means and variances (see Appendix). If we define a virtual prior sample with size $n_0= \kappa_0=2 \alpha_0$, with sample mean $\theta_0$ and sample variance   $\beta_0/\alpha_0$ and a new sample with sample mean $\bar x$ and sample variance $s^2$ which are combined in the standard frequentist way to produce an updated sample mean $\theta_n$ sample variance $\beta_n/\alpha_n$ and unbiased estimate of the population variance $\beta_n/(\alpha_n-1)$.  
\begin{align}
\frac{\beta_n}{\alpha_n} &= \frac{\beta_0}{\alpha_0 + n/2} + 0.5\frac{n s^2}{\alpha_0 +n/2} + 0.5 \frac{\kappa_0 n/(\bar x - \theta_0)^2/(k_0+n)}{\alpha_0 n/2} \nonumber\\
=& \frac{ n_0 \beta_0/\alpha_0   + n s^2 + (n_0 n) (\bar x - \theta_0)^2/(n+m) }{n+m}
\end{align}
So the mechanics of the  Bayesian update equations are identical to the standard statistical update equations for the mean and variance,  but with the concept of  a virtual prior sample.  
\section{Setting Prior Parameters}
The prior distribution represents any prior knowledge we may have about the plausible values of the unobserved mean $\mu$ and variance $\sigma^2$. The Appendix shows a detailed analysis of methods to convert prior knowledge into the 4 parameters of the prior distribution: $\alpha_0, \beta_0, \theta_0,\kappa_0$. Here we Summarize two of the possible approaches in the Appendix: 

\subsection{Method 1: Prior Virtual Sample}
In this case we get a prior sample mean $\bar x$ based on $n_x>0$ prior observations and a prior sample variance $s^2$ based on $n_s>0$ prior observations. Note that $n_x, n_s$ can be any positive real valued number. This determines the 4 prior parameters
 
\begin{align} 
&\alpha_0 =\frac{n_s}{2}\\
&\beta_0 = \alpha_0 \;s^2= \frac{n_s s^2}{2}\\
&\theta_0 = \bar x\\
&\kappa_0 = n_x
\end{align}

It is interesting to note that under this methods we get the following moments for the prior distribution

\begin{align} 
&E[\sigma^{-2}] = E[\lambda] = \frac{\alpha_0}{\beta_0} = \frac{1}{s^2}\\
&Var[\sigma^{-2}] = Var[\lambda] = \frac{\alpha}{\beta_0^2} = E^2[\lambda] \beta_0= E^2[\lambda] \frac{n_s s^2}{2}\\
&E[\sigma^2]=  E[\sigma^2]  = s^2 \frac{n_s}{n_s-1}\\
&Var[\sigma^2] = Var[\sigma^2] =\frac{ E^2[\sigma^2]} {\alpha_0 -2} = \frac{ E^2[\sigma^2]} {n_s/2 -2} \\
&E[\mu] = \theta_0\\
&Var[\mu]  = \frac{E[\sigma^2]}{n_x}
\end{align}
Figure \ref{fig:priorsVirtualSample} shows the effect of the virtual prior sample size on the  prior distributions for the mean and variance. In this figure we set $n_x=n_s=n$, $\bar x= 100$, $s^2=100$.  

 \begin{figure}[bht]\label{fig:priorsVirtualSample}
\begin{center}
\includegraphics[width=5in]{figures/priorsVirtualSample}
\end{center}
  \caption{Prior mean (Left) and Variance (right) with Virtual Prior Samples of different size. In all cases size $n_x = n_s=n$, $\bar x =100$, $s^2=100$.}
 \end{figure}
 
 
\subsection{Method 2: Moment Matching}
We set the $\alpha_0,\beta_0,\theta_0,\kappa_0$ parameters to match 4 pieces of information:
\begin{itemize}
\item Expected Value of the mean: $E[\mu]$
\item Expected Value pf the variance: $E[\sigma^2]$
\item Range of plausible values for the mean: $R(\mu)= 16 Var[\mu]$
\item Range of plausible values for the variance: $R(\sigma^2) = 16 Var[\sigma^2]$
\end{itemize}
Where we interpret the plausible range as plus/minus 4 standard deviations, i.e., 16 variances. So knowledge about the range of $\mu$ and $\sigma^2$ is converted into knowledge about $Var[\mu]$, $Var[\sigma^2].$ We then use the method of moment to convert into the 4 parameters for the prior distribution:
\begin{align} 
&\alpha_0 = \frac{E^2[\sigma^2]}{Var[\sigma^2]} +2\\
&\beta_0 = E[\sigma^2] (\alpha_0-1)\\
&\theta_0 = E[\mu]\\
&\kappa_0 = \frac{E[\sigma^2]}{Var[\mu]}
\end{align}
It is interesting to note what would be the virtual prior sample equivalent to the moment matching method:
\begin{align} 
&n_s =  \frac{\alpha_0}{2} =  \frac{E^2[\sigma^2]}{2 Var[\sigma^2]} +1\\
&s^2  = E[\sigma^2]  \frac{n_s-1}{n_s} \\
&\bar x = E[\mu]\\
&n_x = \kappa_0 = \frac{E[\sigma^2]}{Var[\mu]}
\end{align}

\section{Log Normal Gamma Model: Draft}
Here is the gist:
In many cases the observable variable is positive so a Normal model may not be ideal. 
Here we observe $Y$ but send the model $X= \log(Y)$. We model $X$ as Normal with unobserved parameters $\mu,\sigma^2$. This will give us a posterior over $\mu,\sigma^2$ and a predictive distribution for $X$. Issue is how to convert into the corresponding distributions for $Y$. 

Here $Y$ is a log-normal random with  parameter $\mu,\sigma^2$. Problem is that the relationship between these parameters and the mean and variance of $Y$, the real goal of our inference,  is not trivial: 
\begin{align}
&E[Y] = e^{\mu + \sigma^2/2}\\
&Mode[Y]=e^{\mu-\sigma^2}\\
&Median[Y] = e^\mu\\
&Var[Y]= (e^{\sigma^2}-1)e^{2\mu+\sigma^2}
\end{align}
So the issue is that to get the marginal distribution of $E[Y]$ we need to integrate over the joint distribution of $\mu,\sigma^2$. Same for $Var[Y]$. The solution I'm investigating is to redefine our goal so instead of making inferences about $E[Y], Var[Y]$ we use alternative metrics for location and scale.
\begin{align}
p( e^\mu \leq Y) = p(\mu \leq X) = 0.5
\end{align}
Thus $e^\mu$ is the median and can be used as a statistic of location. Since $\mu$ has a $T$ distribution then $e^\mu$ has a log-T distribution. To interpret $\sigma$ note
\begin{align}
p(e^{\mu - k \sigma} \leq Y \leq e^{\mu + k\sigma}) = p( \mu - k \sigma \leq X \leq \mu+ k\sigma) 
\end{align}
Thus the ratio between the upper and lower limits of the confidence interval is
\begin{align}
\rho(k) = \frac{e^{\mu+k\sigma}}{ e^{\mu-k\sigma}} = e^{2k\sigma}
\end{align}
And approximating $X$ with a Gaussian distribution
\begin{align}
p( \mu - 0.5\sigma \leq X \leq \mu+ 0.5 \sigma)  =  0.382
\end{align}
then 
\begin{align}
\rho(0.5) = e^\sigma
\end{align}
is the ratio between the upper and lower 38\% confidence intervals. This metric is sometimes known as the Scatter. 
Similarly
\begin{align}
\rho(1.96) = e^{3.92\sigma}
\end{align}
is the ratio between the upper and lower 95\% confidence intervals. 

\paragraph{Setting up priors} 
We need to start the model with  $\mu$ and $\sigma^2$. For this we use the virtual prior method: A mean with initial estimate $\bar x$ and $s^2$ as well as sample sizes $n_x, n_s$.
For the initial estimate $\bar x$ we start with the best guess for the median of $Y$ and use the relation $\nu= e^\theta_0$ to set the corresponding $\bar x$, i.e.,
\begin{align}
\bar x = \log \nu
\end{align}
Sample size $n_x$ set as in the standard Normal Gamma case. 

For the initial estimate $s^2$ we can use our best guess for  the percentile ratio $\rho$. Then use the formula $\rho= e^{5 \sqrt{s^2}}$, to set $s^2$, i.e.,
\begin{align}
 s^2= (\log(\rho)/5)^2
 \end{align}
 Note: this is based on an approximation of the posterior of $\mu$ using a Normal instead of a T distribution. This approximation is very accurate for more than 30 degrees of freedom, $\alpha > 15$. In practice we used the log-normal as to initialize an iterative method until we find a value of $s^2$ whose $\rho$ matches the desired rho in a log-T distribution. 
  
  
 We can set  $n_s$ the same way we did for the standard Normal-Gamma model. 

\paragraph{Interpreting Posteriors}
The relationship between the distribution of $\mu,\sigma^2$ and the mean and variance of the observations is not trivial. 
\begin{align}
&E[Y] = e^{\mu + \sigma^2/2}\\
&Var[Y]= (e^{\sigma^2}-1)e^{2\mu+\sigma^2}
\end{align}
So, for example, the distribution of $Var[Y]$ depends on the distribution of $\mu$ and the distribution of $\sigma^2$. This is why its better to work with the distribution of $e^\mu$ the median of $Y$ and $e^{\sigma}$, the scatter of $Y$.  From the pdf of  $\mu$ we get the pdf for the median,  $e^\mu$. From the pdf of $\sigma^2$ we get the pdf of the scatter  $\rho = e^{\sigma}$.
ed the scatter. 



\paragraph{Predictive Distribution}

With regard to the predictive distribution for $S=\sum_{k=1}^m Y_{n+k}$. We know,  the predictive distribution for $X_{n+k}$ is T . If the df is large enough we can approximate it as Normal. This means the predictive distribution of $Y_{n+k}$ is log-Normal. From the mean and variance of the predictive distribution of $X_{n+k}$ we can derive the mean and variance of $Y_{n+k}$. It is well known that the pdf sum of log normal can  be approximated as a log normal. The $\mu,\sigma^2$ parameters of the log-normal are set to match the mean and variance of $S$ (see Appendix). This is known as the Felton-Wilkinson method. 

 
\section{Appendix 2: Setting the prior parameters for the variance $\sigma^2$}
Here we investigate several methods to express prior knowledge about the variance of individual observations $\sigma^2$.

The update equations for the $\alpha$ and $\beta$ parameters of the posterior distribution of the precision are as follows:
\begin{align}
&\alpha_n = \alpha_0 + \frac{n}{2}\\
&\beta_n = \beta_0 + \frac{1}{2} ( n s_n^2 + \frac{k_0 n}{k_0 +n} (\bar x_n - \mu_0)^2)
\end{align}
So $\alpha_0$ can be interpreted as 1/2 the number of prior observations and $\beta_0$ as 1/2 the sum of squared deviations. This suggests that for low impact priors we want $\alpha_0$ and $\beta_0$ small. 
 
\paragraph{Method 1: No Impact Prior.}
For the prior to have no impact on the probability update equations we can set set $\alpha_0 =0$, $\beta_0=0$. In this case we get $p(\lambda) \propto \sigma^2$ and  $p(var) \propto var^{-1}$, which corresponds to the popular Jeffery prior. This  is not a proper distribution since it has  no integral. Some authors recommend to use $\alpha=\beta=0.001$, which is practically equivalent to a Jeffrey prior but is a proper probability disrtribution , see Figure \ref{fig:lowImpactPriors}. 
 \begin{figure}[bht]\label{fig:lowImpactPriors}
\begin{center}
\includegraphics[width=3in]{figures/lowImpactPriors}
\end{center}
  \caption{Top: Prior variance  and precision distributions in log-log coordinates as we approach $\alpha=\beta=0$.}
 \end{figure}
 
\paragraph{Method 2: Minimum Plausible Variance Method.}
Set $\alpha_0=1$, this makes the precision prior exponential with rate $\beta_0$. Let $\rho$ be a plausible value for the  minimum variance, as set by the user. This set the maximum precision to $1/\rho$ We set $\beta_0$ so that the prior probability that the precision is smaller than the max plausible value is 0.99. Using the formula for the cumulative distribution of exponential random variables we get: 
\begin{align}
&0.99 = 1 - e^{-\frac{\beta_0}{\rho}}\\
&\beta_0 = -\log(0.01) \rho	
\end{align}
As we let $\rho \to 0$ $\beta_0 \to 0$ so we get a practically flat distribution  over the precision. 
 Figure \ref{fig:priorsBasedOnMinimumVariance} displays prior distributions for different values of the  minimum plausible variance.


\begin{figure}[bht]\label{fig:priorsBasedOnMinimumVariance}
\begin{center}
\includegraphics[width=3in]{figures/priorsBasedOnMinimumVariance}
\end{center}
  \caption{Prior variance (Top) and precision (Bottom) distributions for different values of the minimum plausible variance. } 
 \end{figure}

 
 

\paragraph{Method 3: Moment Matching.}
The random variable $\sigma^2$ is the variance of individual observations. In the Appendix it is shown that
\begin{align}
&E[\sigma^2] = \frac{\beta_0}{\alpha_0 -1}\\
&Var[\sigma^2] = \frac{E^2[\sigma^2]}{\alpha_0 -2}
\end{align}
Thus
\begin{align} 
&\alpha_0 = \frac{E^2[\sigma^2]}{Var[\sigma^2]} +2\\
&\beta_0 = E[\sigma^2] (\alpha_0-1)
\end{align}
One disadvantage of this method is that it never produces values of  $\alpha_0$ smaller than 2. Figure \ref{fig:momentMatchingPriors} shows Gamma priors with the same mean and different values of the variance.


\begin{figure}[bht]\label{fig:momentMatchingPriors}
\begin{center}
\includegraphics[width=3in]{figures/momentMatchingPriors}
\end{center}
  \caption{Variance (Top) and precision (Bottom) priors with the same mean and different variance. } 
 \end{figure}

\paragraph{Method 4: Prior Sample Method.}
The parameter update equations after observing a sample of size $n$ with sample variance $s^2_n$ are as follows
\begin{align}
&\alpha_n = \alpha_0 + \frac{n}{2}\\
&\beta_n = \beta_0 + \frac{1}{2} ( n s_n^2 + \frac{k_0 n}{k_0 +n} (\bar x_n - \mu_0)^2)
\end{align}
Thus we can think metaphorically of $\alpha_0, \beta_0$ as a prior sample size $n_0$and prior observed variance $s^2_0$. Note that this is just a metaphor. In fact $\alpha_0$ can be any  positive real valued number. There is no need to be an integer. 
\begin{align}
&\alpha_0 = \frac{n_0}{2}\\
&\beta_0 = \frac{1}{2} n_0 s^2_0= \alpha_0  s^2_0
\end{align}
Note under this scheme
\begin{align}
E[\lambda] = \frac{\alpha_0}{\beta_0} = \frac{1}{s^2_0}\\
Var[\lambda] = \frac{\alpha_0}{\beta_0^2} = E[\lambda] \frac{2}{s^2_0 n_0}
\end{align}
 \begin{figure}[bht]\label{fig:priorsBasedOnSampleSizeAndVariance}
\begin{center}
\includegraphics[width=3in]{figures/priorsBasedOnSampleSizeAndVariance}
\end{center}
  \caption{Top: Prior variance distributions for $s^2_0=10$ and different values of $n_0$. Bottom: Corresponding prior precision distributions.} 
 \end{figure}
Figure \ref{fig:priorsBasedOnSampleSizeAndVariance} displays prior distributions for the variance and the precision when $s^2_0=10$ for different values of $n_0$.



\section{Appendix: Setting the prior parameters for the Mean $\mu$}

\paragraph{Method 1: Moment Matching.}	
In this case we want to find $\theta_0,\kappa_0$ parameters to match desired  $E[\mu], Var[\mu]$ values. Note
\begin{align}
&E[\mu]= \theta_0\\
&Var[\mu] =\frac{ E[\lambda^-1 ]}{\kappa_0} 
\end{align}
where 
\begin{align}
E[\sigma^2] = \frac{\beta}{\alpha^-1}
\end{align}
Thus the Variance of $\mu$ is the expected variance of individual observations divided by $\kappa_0$, the number of prior observations supporting $\theta_0$. 
Thus we can set $\theta_0$, $\kappa_0$ to match the desired expected value and variance:
\begin{align}
&\theta_0 = E[\mu]\\
&\kappa_0  =\frac{ E[\lambda^-1 ]}{Var[\mu]}  
\end{align}
Figure \ref{fig:mean_priors}  Left shows several prior distributions with mean 10, variance 100, and several values of $\alpha$, $\beta$.
 \begin{figure}[bht]\label{fig:mean_priors}
\begin{center}
\includegraphics[width=2in]{figures/mean_MomentMatching}
\includegraphics[width=2in]{figures/mean_priorsBasedOnMaximumPlausibleMean}
\end{center}
  \caption{Left: Prior for $\mu$  with mean 100, variance 100 for several values of $\alpha$, $\beta$. Prior for $\mu$ with  most likely mean 100 and maximum plausible mean 125 for several values of $\alpha$, $\beta$} 
 \end{figure}
\paragraph{Method 2: Most Likely  and Maximum Plausible Value of the mean}
In this case we are given the most likely $ml$  and maximum plausible $mp$ value of the mean. We interpret the most likely mean as the expected value of the prior and the maximum plausible as the 99 percentile of the prior. Thus
\begin{align}
&\theta_0 = m_l\\
&0.99 = F_{2\alpha}( \frac{mp - ml}{\epsilon})\\
&\epsilon= \frac{mp -ml}{ F^{-1}_{2 \alpha} (0.99)} = \sqrt{\frac{\beta_0}{\alpha_0 \kappa_0}}
\end{align}  
Thus
\begin{align}
\kappa_0 &=\left(\frac{ F^{-1}_{2 \alpha} (0.99)}  {mp -ml}\right)^2 \frac{\beta_0}{\alpha_0} \\
&= F^{-1}_{2 \alpha} (0.99)^2  \left( \frac{1}{mp -ml }\right)^2  \frac{1}{E[\lambda]}
\end{align}
We can interpret $1/E[\lambda]$ as the expected variance of individual observations. Thus $\kappa_0$, the number of prior observations is proportional to the ratio between the variance of individual observations and the squared distance between the most likely and the maximum plausible values, i.e., the desired scale of the posterior distribution. 
Figure \ref{fig:mean_priors}  Right shows several prior distributions with $ml =10$, $mp=125$ and several values of $\alpha$, $\beta$.
\paragraph{No Impact Prior.}
The no impact prior corresponds to $\kappa_0=0$ and any value of the mean. This results on a uniform distribution over the realline, which is not a proper distribution.  The update equations will work but we will not be able to visualize the prior.




\bibliographystyle{apalike} % plainnat
\bibliography{bayesianMethods}
\end{document}
