from sklearn.linear_model import BayesianRidge
from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
from numpy import sqrt

boston = load_boston()
x, y = boston.data, boston.target
xtrain, xtest, ytrain, ytest=train_test_split(x, y, test_size=0.15)

bay_ridge = BayesianRidge()
print(bay_ridge)

bay_ridge.fit(xtrain, ytrain)

score=bay_ridge.score(xtrain, ytrain)
print("Model score (R-squared): %.2f" % score)

ypred = bay_ridge.predict(xtest)
mse = mean_squared_error(ytest, ypred)
print("MSE: %.2f" % mse)
print("RMSE: %.2f" % sqrt(mse))

x_ax = range(len(ytest))
plt.scatter(x_ax, ytest, s=5, color="blue", label="original")
plt.plot(x_ax, ypred, lw=0.8, color="red", label="predicted")
plt.legend()
plt.show()

# alpha_1float, default=1e-6
# Hyper-parameter : shape parameter for the Gamma distribution prior over the alpha parameter.
#
# alpha_2float, default=1e-6
# Hyper-parameter : inverse scale parameter (rate parameter) for the Gamma distribution prior over the alpha parameter.
#
# lambda_1float, default=1e-6
# Hyper-parameter : shape parameter for the Gamma distribution prior over the lambda parameter.
#
# lambda_2float, default=1e-6
# Hyper-parameter : inverse scale parameter (rate parameter) for the Gamma distribution prior over the lambda parameter.
#
# alpha_initfloat, default=None
# Initial value for alpha (precision of the noise). If not set, alpha_init is 1/Var(y).
#
# New in version 0.22.
# lambda_initfloat, default=None
# Initial value for lambda (precision of the weights). If not set, lambda_init is 1.
#
# coef_array-like of shape (n_features,)
# Coefficients of the regression model (mean of distribution)
#
# intercept_float
# Independent term in decision function. Set to 0.0 if fit_intercept = False.
#
# alpha_float
# Estimated precision of the noise.
#
# lambda_float
# Estimated precision of the weights.
#
# sigma_array-like of shape (n_features, n_features)
# Estimated variance-covariance matrix of the weights
#
