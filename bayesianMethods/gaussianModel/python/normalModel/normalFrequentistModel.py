from scipy.stats import t,norm,gamma,invgamma,chi2
import numpy as np

#warnings.simplefilter('error')


class NormalFrequentistModel():
    """"
    Frequentist model for inferences about
    unobserved mean and variance of normal observations
    """
    def __init__(self):
        """
        # sample mean
        # sample variance (biased version)
        # sample size
        """

        self.barX = 0.0
        self.S2 = 0.0
        self.n = 0.0

    def resetParameters(self):
        self.barX = 0.0
        self.S2 =  0.0
        self.n = 0.0

    def updateModelParameters(self,y):
        n = self.n
        xbar = self.barX
        s2_x = self.S2

        m = y.size
        ybar = y.mean()
        s2_y = y.var()

        self.n = n + m
        self.barX = (n*xbar + m* ybar)/(n+m)
        self.S2 =  s2_x * n + s2_y *m + n*m*((xbar - ybar)**2.0)/(n+m)
        self.S2 = self.S2/(n+m)




# we are overloading the meaning of posterior
# distribution parameters by using the closest
# frequentist interpretation.
# for example ExpectedMu shall really be Estimate of Mu


    def getExpectedMu(self):
    # frequentist estimate of Mu
        if self.n< 1.0:
            return np.nan
        return self.barX

    def getExpectedSigma2(self):
    # frequentist unbiased estimate of Sigma2
        if self.n < 2.0:
            return np.nan
        return self.S2* self.n/(self.n-1.0)


    def getVarianceOfMu(self):
    # frequentist estimate of variance of sample mean
        return self.getExpectedSigma2()/(self.n+0.0)

    def getVarianceOfSigma2(self):
    # frequentist estimate of variance of sample variance
        if self.n < 2.0:
            return np.nan
        else:
            s2 = self.getExpectedSigma2()
            return 2*(s2**2.0) / (self.n -1.0)

    def upperBoundOfMu(self,p=0.95):
        # p is the probability that mu is larger
        # than the upperBound
        df = self.n -1
        if df< 1:
            return np.nan
        s = np.sqrt(self.getVarianceOfMu())
        u = t.ppf(p,df=df, loc = 0,  scale = 1)
        upperBound =  self.barX + u * s
        return upperBound

    def lowerBoundOfMu(self,p=0.95):
        # p is the probability that mu is smaller
        # than the lower bound
        df = self.n -1
        if df< 1:
            return np.nan
        s = np.sqrt(self.getVarianceOfMu())
        u = t.ppf(p,df=df, loc = 0,  scale = 1)
        lowerBound = self.barX - u * s
        return lowerBound

    def confIntervalOfMu(self,p=0.95):
        # brackets mu with prob p
        mid = (1-p)/2.0
        upperBound = self.upperBoundOfMu(p+mid)
        lowerBound = self.lowerBoundOfMu(p+mid)
        return lowerBound, upperBound


    def upperBoundOfLambda(self,p=0.95):
        # upper bounds lambda with probability p
        alpha = (self.n -1) / 2.0
        beta = 0.5*self.S2*self.n
        upperBound= gamma.ppf(p,a= alpha, scale=1/beta)
        return upperBound


    def lowerBoundOfLambda(self,p=0.95):
        # lower bounds lambda with probability p
        alpha = (self.n -1) / 2.0
        beta = 0.5*self.S2*self.n
        lowerBound = gamma.ppf(1-p,a=alpha, scale = 1/beta)
        return lowerBound


    def confIntervalOfLambda(self,p=0.95):
        # brackets mu with prob p
        mid = (1-p)/2.0
        upperBound = self.upperBoundOfLambda(p+mid)
        lowerBound = self.lowerBoundOfLambda(p+mid)
        return lowerBound, upperBound

    def upperBoundOfSigma2(self,p=0.95):
        # upper bounds Sigma2 with probability p
        return 1/self.lowerBoundOfLambda(p)

    def lowerBoundOfSigma2(self,p=0.95):
        # lower bounds Sigma2 with probability p
        return 1/self.upperBoundOfLambda(p)

    def confIntervalOfSigma2(self,p=0.95):
        # brackets mu with prob p
        mid = (1-p)/2.0
        upperBound = self.upperBoundOfSigma2(p+mid)
        lowerBound = self.lowerBoundOfSigma2(p+mid)
        return lowerBound, upperBound
