from normalBayesianModel import *
from normalFrequentistModel import *

def processModel(m):
    m.updateModelParameters(o)
    esigma2 = m.ExpectedSigma2()
    vsigma2 = m.VarianceOfSigma2()
    emu = m.ExpectedMu()
    vmu = m.VarianceOfMu()
    n_1 = n_0+n
    xbar_1 = (xbar_0 * n_0 + xbar*n)/(n+n_0)
    s2_1 = (n*n_0)* ((xbar-xbar_0)**2)/(n+n_0)
    s2_1 = (s2_1 + n_0*s2_0 + n * s2)/(n+n_0)
    print('s2_1',s2_1)
    emu_ = xbar_1
    vmu_ =s2_1 /(n_1 -2.0)
    esigma2_ = s2_1 * (n_1)/(n_1-2)
    vsigma2_ = 2 * (esigma2**2)/(n_1-4)


    print('E[mu]',emu,'Var[mu]',vmu)
    print('E[mu]_',emu_,'Var[mu]_',vmu_)
    print('Med[mu]',m.MedianOfSigma2())
    print('SVariation[mu]',m.StandardVariationOfMu())
    upperBound = m.upperBoundOfMu(0.95)
    lowerBound = m.lowerBoundOfMu(0.95)
    print('Mu Lower Bound',lowerBound,'Mu Upper Bound',upperBound)


    print('E[sigma2]',esigma2,'Var[sigma2]',vsigma2)
    print('E[sigma2]_',esigma2_,'Var[sigma2]_',vsigma2_)
    print('Med[sigma2]',m.MedianOfSigma2())
    print('SVariation[sigma2]',m.StandardVariationOfSigma2())

    alpha = n_1/2.0
    beta = n_1 * s2_1/2.0
    upperBound_ =  invgamma.ppf(0.95,a=alpha, scale=beta)
    lowerBound_ =  invgamma.ppf(0.05,a=alpha, scale=beta)
    upperBound = m.upperBoundOfSigma2(0.95)
    lowerBound = m.lowerBoundOfSigma2(0.95)
    print('Sigma 2 Lower Bound',lowerBound,'Sigma 2 Upper Bound',upperBound)
    print('Sigma 2 Lower Bound_',lowerBound_,'Sigma 2 Upper Bound_',upperBound_)





    # predictive distribution single observation


    s = np.sqrt(s2_1 * (n_1 +1)/n_1)
    lowerBound = m.lowerBoundOfSum(n=1,p=0.95)
    lowerBound_ = xbar_1-t.ppf(0.95,df=n_1, loc = 0,  scale = 1)*s
    upperBound = m.upperBoundOfSum(n=1,p=0.95)
    upperBound_ = xbar_1+t.ppf(0.95,df=n_1, loc = 0,  scale = 1)*s
    print('Expected X', m.ExpectedX())
    print('Expected X_', xbar_1)
    print('Variance of X',m.VarianceOfX())
    vx = s2_1 * (n_1+1)/(n_1-2)
    print('Var of X_', vx)
    print('Median of X',m.MedianOfSum(1))
    print('SVariation of X',m.StandardVariationOfSum(1))
    print('X Lower Bound',lowerBound)
    print('X Lower Bound_',lowerBound_)
    print('X Upper Bound',upperBound)
    print('X Upper Bound_',upperBound_)
    # predictive distribution sum of 100 observations
    print('Expected Sum',m.ExpectedSum(n=100))
    print('Expected Sum_',100*xbar_1)
    print('Variance of Sum',m.VarianceOfSum(n=100))
    vsum = 100*s2_1*(n_1+1)/(n_1-2)
    s = np.sqrt(vsum)
    print('varSum_',vsum)
    print('Median of Sum',m.MedianOfSum(100))
    print('SVariation of Sum',m.StandardVariationOfSum(100))
    lowerBound = m.lowerBoundOfSum(n=100,p=0.95)
    lowerBound_ = 100*xbar_1-norm.ppf(0.95)*s
    upperBound = m.upperBoundOfSum(n=100,p=0.95)
    upperBound_ = 100*xbar_1+norm.ppf(0.95)*s
    print('Sum  of 100 Lower Bound',lowerBound)
    print('Sum of 100 Lower Bound_',lowerBound_)
    print('Sum of 100  Upper Bound',upperBound)
    print('Sum of 100 Upper Bound_',upperBound_)

# the observed sample
o = np.array([1,2,3,4,5,6,7,8,9,10])
# First we try a Bayesian model with influential priors
# prior virtual sample parameters as follows
n_0 = 1
xbar_0 = 4
s2_0 = 10
n= o.shape[0]
xbar = o.mean()
s2 = o.var()

print('observation',o)
print('n',n,'nxbar',xbar,'s2',s2)
# bayesian model with influential priors
print ('--------- Model 1: Bayesian with Influential Priors ---------')
print('n_0',n_0,'xbar_0',xbar_0,'s2_0',s2_0)

m1 = NormalBayesianModel(memory_size=np.inf, logNormal=False)
m1. setPriors(n_sample = n_0,mean=xbar_0,var=s2_0)
processModel(m1)

# bayesian model with no impact priors
n_0=0
print ('--------- Model 2 No Influence Priors ---------')
print('n_0',n_0,'xbar_0',xbar_0,'s2_0',s2_0)

m2 = NormalBayesianModel(memory_size=np.inf, logNormal=False)
m2. setPriors(n_sample = n_0,mean=xbar_0,var=s2_0)
processModel(m2)

# frequentist
model3 = NormalFrequentistModel()
model3.updateModelParameters(o)
print('Mu Estimate', xbar)
sigma2Hat = s2 * n/(n-1.0)
print('Estimate Variance of Estimate of Mu', sigma2Hat/n)
print('Sigma2 Estimate', sigma2Hat)
print('Estimate Variance of Estimate of Sigma2Hat', 2* (sigma2Hat**2)/(n-1))

s = np.sqrt(sigma2Hat/n)
lowerBound_ = xbar -t.ppf(0.95,df=n-1, loc = 0,  scale = 1)*s
print('Mu lowerBound_',lowerBound_)
upperBound_ = xbar +t.ppf(0.95,df=n-1, loc = 0,  scale = 1)*s
print('Mu upperBound_',upperBound_)


lowerBound = model3.lowerBoundOfSigma2(0.95)
upperBound = model3.upperBoundOfSigma2(0.95)
print('sigma2 lowerBound',lowerBound,'sigma2 upperBound',upperBound)
