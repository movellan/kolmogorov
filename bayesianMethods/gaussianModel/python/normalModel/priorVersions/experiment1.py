# simple experiment to verify things work as expected
from normalGammaModel import *
np.random.seed(seed=233423)

memory_sizeList=[20,200,2000,20000,200000]
lagPoints=[0,10,100,1000,10000,100000,1000000]
nsims=200
me = NormalGammaConjugateModelEnsemble(memory_sizeList,lagPoints)
for i in range(1,nsims+1):
    if i%50==0:
        print(i)

    lag=0
    xmean  = 10* np.sin(2*np.pi*(i+lag)/(150.0))
    xmean += 10* np.sin(2*np.pi*(i+lag)/(2400.0))
    xmean=100

    x = xmean+2*norm.rvs(size=1)

    nLags = len(me.model[0].lagPoints)
    r = randint.rvs(1,nLags)

    lag = me.model[0].lagPoints[r]
    xmeanlag  = 10* np.sin(2*np.pi*(i+lag)/(150.0))
    xmeanlag += 10* np.sin(2*np.pi*(i+lag)/(2400.0))
    xmeanlag=0
    xmeanlag += lag
    xmeanlag= 100+lag*0.5

    xlag = xmeanlag+2*norm.rvs(size=1)


    me.updateBestModelCounts(x)
    me.updateBestModelCounts(xlag,lag)

    me.updateModelParameters(x)
    me.updateModelParameters(xlag,lag)
