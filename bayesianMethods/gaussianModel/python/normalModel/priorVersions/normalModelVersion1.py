from scipy.stats import gamma,invgamma,t,lognorm,norm
from scipy.special import gamma as gammaFunction
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import NearestNeighbors
from scipy.stats import randint as randint
import warnings
import sys
#warnings.simplefilter('error')


class NormalGammaConjugateModel():
    """"
    Standard Bayesian Conjugate Model for making inferences about the
    unobserved mean and variance based on observed iid
    Normal random variables
    Math Documentation: https://gitlab.com/movellan/kolmogorov/
    -/blob/master/bayesianMethods
    X: Observable variable with unknown mean mu and variance sigma2
    Model Parameters: alpha,beta, theta, kappa
        alpha, beta determine the sampling distribution of sigma2
        theta, kappa,sigma2 determine the sampling distribution of mu
        mu, sigma2 determine the sampling distribution of X
        in some cases it is better to work with the precision lambda
        defined as 1/sigma2


    """
    SQ0 = 0.15865525393145707
    SQ1=  0.8413447460685429

    def __init__(self,memory_size=np.inf,logNormal=False):
        """

        """

        #default prior initialization
        # virtual prior samples. The less influential the prior is
        virtual_prior_samples = 1e-30
        # prior sum
        prior_expected_mu = 0
        # precision is the inverse of the variance
        prior_expected_lambda = 1
        self.alpha_0 = virtual_prior_samples/2.0
        self.beta_0 = self.alpha_0/prior_expected_lambda
        self.theta_0= prior_expected_mu
        self.kappa_0 = virtual_prior_samples


        # how much we remember the past
        # inf  for reme,ber all past
        self.memory_size = memory_size
        self.logNormal = logNormal
        # the parameters of the posterior are initialized with the prior
        self.theta = self.theta_0
        self.kappa = self.kappa_0
        self.alpha =self.alpha_0
        self.beta= self.beta_0


    def resetParameters(self):
        self.theta = self.theta_0
        self.kappa = self.kappa_0
        self.alpha= self.alpha_0
        self.beta=self.beta_0

    def setPriorsUsingVirtualSample(self,n_sample,mean,var):
        self.theta_0 = mean
        self.kappa_0 = n_sample
        self.alpha_0 = n_sample/2.0
        self.beta_0 =  var * n_sample/2.0
        self.resetParameters()

    def setMemorySize(self,memory_size):
        ''' how many past observations are used to make inferences
        about the mean and the variance
        inf  means use all past observations
        '''
        self.memory_size = memory_size

    def setPriors(self,theta,kappa,alpha,beta):
        self.theta_0 = theta
        self.kappa_0 = kappa
        self.alpha_0 = alpha
        self.beta_0 = beta
        self.resetParameters()



    def posteriorDistributionOfMu_ppf(self,p=0.5):
        # returns value such prob generating value or less is p
        df = 2*self.alpha
        if df< 1e-20:
            df = 1e-20
        gamma = np.sqrt((self.kappa+1)*self.beta/(self.alpha* (self.kappa)))
        ppf = t.ppf(p,df=df, loc = self.theta,  scale = gamma)
        return ppf

    def posteriorDistributionOfSigma2_ppf(self,p=0.5):
        # returns value such prob generating value or less is p
        ppf = invgamma.ppf(p,a = self.alpha, scale = self.beta)
        return ppf

        df = 2*self.alpha
        if df< 1e-20:
            df = 1e-20
        gamma = np.sqrt((self.kappa+1)*self.beta/(self.alpha* (self.kappa)))
        ppf = t.ppf(p,df=df, loc = self.theta,  scale = gamma)
        return ppf

    def posteriorDistributionOfSum_ppf(self,n=1,p=0.5):
        if n ==1 :
            df = 2*self.alpha
            if df< 1e-20:
                df = 1e-20
            gamma = np.sqrt((self.kappa+1)*self.beta/(self.alpha* (self.kappa)))
            ppf = t.ppf(p,df=df, loc = self.theta,  scale = gamma)
        else:
            theta_of_sum,sigma2_of_sum = self.getParametersOfposteriorDistributionOfSum(n)
            ppf= norm.ppf(p,loc = theta_of_sum,scale= np.sqrt(sigma2_of_sum))
        return ppf

    def getMedianOfMu(self):
        return self.posteriorDistributionOfMu_ppf(0.5)

    def getStandardRangeOfMu(self):
        r0 = self.posteriorDistributionOfMu_ppf(self.SQ0)
        r1 = self.posteriorDistributionOfMu_ppf(self.SQ1)
        return (r1-r0)/2.0

    def getMedianOfSigma2(self):
        return self.posteriorDistributionOfSigma2_ppf(0.5)

    def getStandardRangeOfSigma2(self):
        r0 = self.posteriorDistributionOfSigma2_ppf(self.SQ0)
        r1 = self.posteriorDistributionOfSigma2_ppf(self.SQ1)
        return (r1-r0)/2.0

    def getMedianOfSum(self,n=1):
        return self.posteriorDistributionOfSum_ppf(n=n,p=0.5)

    def getStandardRangeOfSum(self,n=1):
        r0 = self.posteriorDistributionOfSum_ppf(n=n, p=self.SQ0)
        r1 = self.posteriorDistributionOfSum_ppf(n=n, p=self.SQ1)
        return (r1-r0)/2.0

    def getExpectedLambda(self):
        return self.alpha/self.beta

    def printParameters(self):
        print(f"theta:{self.theta}, kappa:{self.kappa}")
        print(f"alpha:{self.alpha}, beta:{self.beta}")
        print('Memory Size',self.memory_size)
        if self.alpha >0:
            n = self.kappa
            xbar= self.theta
            s2 = self.beta/self.alpha
            print('n',n,'xbar',xbar,'s2',s2)
    def printStatistics(self):
        print("Mu is the unknown mean. Lambda the unknown precision (inverse variance). Sigma2 is the unknown variance")
        print('Expected Mu and Expected Lambda and Expected Sigma2 are the best guess about Mu,Labmda and Sigma2')
        print('Var Mu and Var Sigma2 represent uncertainty about true value of Mu Lambda and Sigma2')
        print(f"Expected Mu:{self.theta}")
        if self.alpha >1.0:
            sigma2 = self.beta /(self.alpha-1.0)
            print(f"Variance of Mu:{sigma2/self.kappa}")
        if self.beta >0 and self.alpha >0:
            lambda_=self.alpha/self.beta
            vlambda = (lambda_**2)/self.alpha
            print(f"Expected Lambda :{lambda_}, Variance of Lambda:{vlambda}")
        else:
            print('Expected Lambda and Variance of Lambda Undefined')
        if self.alpha > 1.0:
            sigma2 = self.beta /(self.alpha-1.0)
            print(f"Expected Sigma2 :{sigma2}")
        else:
            print('Expected Sigma2 Undefined')
        if self.alpha>2.0:
            vsigma2= sigma2/(self.alpha -2.0)
            print(f"Variance of Sigma2 :{vsigma2}")
        else:
            print("Variance of  Sigma2 Undefined")



    def getExpectedMu(self):
        return self.theta

    def getVarianceOfMu(self):
        return self.getExpectedSigma2()/self.kappa

    def getExpectedSigma2(self):
        if self.alpha <= 1.0:
            return np.inf
        else:
            return self.beta/(self.alpha -1.0)


    def getVarianceOfSigma2(self):
        if self.alpha <= 2.0:
            return np.inf
        else:
            return (self.getExpectedSigma2()**2.0)/(self.alpha -2.0)



    def getExpectedX(self):
        return self.theta

    def getVarianceOfX(self):
        return self.getExpectedSigma2()*(self.kappa+1.0)/self.kappa

    def getExpectedSum(self,n=1):
        return n* self.getExpectedX()
    def getVarianceOfSum(self,n=1):
        return n * self.getVarianceOfX()

    def getExpectedXLogNormal(self):
        mu = self.getExpectedX()
        sigma2 = self.getVarianceOfX()
        m = np.exp(mu+sigma2/2)
        return m

    def getVarianceOfXLogNormal(self):
        mu = self.getExpectedX()
        sigma2 = self.getVarianceOfX()
        v = (np.exp(sigma2) -1)* np.exp(2*mu+sigma2)
        return v


    def getVarianceOfTheta(self):
        if self.alpha>1.0:
            sigma2 = self.beta[k]/((self.alpha)*self.kappa)
        else:
            sigma2 = np.inf
        return sigma2

    def upperBoundOfMu(self,p=0.95):
        # p is the probability that mu is larger
        # than the upperBound
        df = 2*self.alpha
        if df< 1e-20:
            df = 1e-20
        s = np.sqrt(self.beta/(self.alpha* self.kappa))
        if self.logNormal:
            print('under construction')
        else:
            upperBound = self.theta + t.ppf(p,df=df, loc = 0,  scale = 1)*s
        return upperBound


    def lowerBoundOfMu(self,p=0.95):
        # p is the probability that mu is smaller
        # than the lower bound
        df = 2*self.alpha
        if df< 1e-20:
            df = 1e-20
        s = np.sqrt(self.beta/(self.alpha* self.kappa))
        df = 2*self.alpha
        if self.logNormal:
            print('under construction')
        else:
            lowerBound = self.theta - t.ppf(p,df=df, loc = 0,  scale = 1)*s
        return lowerBound


    def confIntervalOfMu(self,p=0.95):
        # brackets mu with prob p
        mid = (1-p)/2.0
        upperBound = self.upperBoundOfMu(p+mid)
        lowerBound = self.lowerBoundOfMu(p+mid)
        return lowerBound, upperBound



    def upperBoundOfSigma2(self,p=0.95):
        # p is the probability that sigma2 is larger
        # than the upper bound
        upperBound = invgamma.ppf(p,a = self.alpha, scale = self.beta)
        return upperBound

    def lowerBoundOfSigma2(self,p=0.95):
        # p is the probability that sigma2 is smaller
        # than the lower bound
        lowerBound = invgamma.ppf(1-p,a = self.alpha, scale = self.beta)
        return lowerBound

    def confIntervalOfSigma2(self,p=0.95):
        # brackets sigma2 with probability p
        mid = (1-p)/2.0
        upperBound = self.upperBoundOfSigma2(p+mid)
        lowerBound = self.lowerBoundOfSigma2(p+mid)
        return lowerBound,upperBound


    def upperBoundOfLambda(self,p=0.95):
        # p is the probability that lambda is larger
        # than the upper bound
        if self.beta ==0:
            return np.nan
        upperBound = gamma.ppf(p,a = self.alpha, scale = 1.0/self.beta)
        return upperBound

    def lowerBoundOfLambda(self,p=0.95):
        # p is the probability that lambda is smaller
        # than the lower bound
        if self.beta ==0:
            return np.nan
        lowerBound = gamma.ppf(1-p,a = self.alpha, scale = 1.0/self.beta)
        return lowerBound

    def confIntervalOfLambda(self,p=0.95):
        # brackets lambda with probability p
        mid = (1-p)/2.0
        upperBound = self.upperBoundOfLambda(p+mid)
        lowerBound = self.lowerBoundOfLambda(p+mid)
        return lowerBound,upperBound

    def upperBoundOfSum(self,n=1,p=0.95):
        # The probability that the sum of n Observations is larger than
        # the upper bound is p
        if n ==1 :
            ppf = self.predictiveDistribution_ppf(p)
            return ppf
        else:
            theta_of_sum,sigma2_of_sum = self.getParametersOfposteriorDistributionOfSum(n)
            print('expected value of sum',theta_of_sum,'sigma2_of_sum',sigma2_of_sum)
            if sigma2_of_sum == np.inf:
                print('illegal value for variance. Not enough observations')
                return('illegal value for variance. Not enough observations')
            upperBound= norm.ppf(p,loc = theta_of_sum,scale= np.sqrt(sigma2_of_sum))
        return upperBound

    def lowerBoundOfSum(self,n=1,p=0.95):
        # The probability that the sum of n Observations is smaller than
        # the lower bound is p
        if n ==1 :
            ppf = self.predictiveDistribution_ppf(1-p)
            return ppf
        else:
            theta_of_sum,sigma2_of_sum = self.getParametersOfposteriorDistributionOfSum(n)
            if sigma2_of_sum == np.inf:
                print('illegal value for variance. Not enough observations')
                return('illegal value for variance. Not enough observations')
            lowerBound= norm.ppf(1-p,loc = theta_of_sum,scale= np.sqrt(sigma2_of_sum))
        return lowerBound

    def confIntervalOfSum(self,n=1,p=0.95):
        # brackets the sum of n observations with probability p
        mid = (1-p)/2.0
        upperBound = self.upperBoundOfSum(n,p+mid)
        lowerBound = self.lowerBoundOfSum(n,p+mid)
        return lowerBound,upperBound


    def getParametersOfposteriorDistributionOfSum(self,n=1):
        var = self.getVarianceOfX()
        mean = self.getExpectedX()
        mean_of_sum = n * mean
        var_of_sum  = n* var
        return(mean_of_sum,var_of_sum)

    def predictiveDistribution_pdf(self,x):
        df = 2*self.alpha
        gamma = np.sqrt((self.kappa+1)*self.beta/(self.alpha* self.kappa))
        z = np.array((x-self.theta)/gamma)
        z[z>50] = 50
        z[z<-50]= -50
        pdfList =[]
        if self.logNormal:
            x = np.max((float(x),1e-30))
            #pdf = self.logTpdf(x,df=df, loc = self.theta,  scale = gamma)
            #tmp experiment. Should use logTpdf
            pdf = t.pdf(np.log(x),df=df, loc = self.theta,  scale = gamma)
        else:
            pdf = t.pdf(x,df=df, loc = self.theta,  scale = gamma)
        return pdf

    def logTpdf(self,x,df=10,loc=0,scale=1):
        """
        pdf of log T distribution with df degrees of freedom and
        given location and scale parameters
        """
        p = t.pdf(np.log(x),df=df, loc = loc,  scale = scale)/x
        return p

    def predictiveDistribution_ppf(self,p=0.5):

        # returns value such prob generating value or less is p
        df = 2*self.alpha
        if df< 1e-20:
            df = 1e-20
        gamma = np.sqrt((self.kappa+1)*self.beta/(self.alpha* (self.kappa)))
        ppf = t.ppf(p,df=df, loc = self.theta,  scale = gamma)
        return ppf

    def posteriorDistributionOfSum_pdf(self,n,x):
        # each element of x is the sum of n observables
        # the posterior is a sum of student T random variables.
        # we approximate with a normal distribution if n >1
        if n ==1 :
            pdf = self.predictiveDistribution_pdf(x)
        else:
            theta_of_sum,sigma2_of_sum = self.getParametersOfposteriorDistributionOfSum(n)
            pdf= norm.pdf(x,loc = theta_of_sum,scale= np.sqrt(sigma2_of_sum))
        return pdf



    def predictiveDistributionOfLogNormalSum_ppf(self,n=1,p=0.5):
        # Felton-Wilkinson Moment Matching Method for Sum of LogNormas
        # X now is the log(observation)
        # we approximate the predictive distribution of X
        # using a Normal distribution
        # first we get the mean and variance of X
        mu = self.getExpectedX()
        sigma2 = self.getVarianceOfX()
        if sigma2 == np.inf:
            print('illegal value for variance')
            return('illegal value for variance')
        # Observation = exp(X), which is log Normal with params mu,sigma2
        # we now get the mean and variance of the sum of n log normals
        lognormalMean = n* np.exp(mu+sigma2/2.0)
        lognormalVar =  n*(np.exp(sigma2)-1.0)*np.exp(2.0*mu+sigma2)
        lognormalSumSquares = lognormalVar+ lognormalMean**2.0
        # we now find the mu and sigma2 parameters of a logNormal
        # that matches the desired Mean and Variance, using Moment Matching
        sigma2Sum = np.log(lognormalSumSquares/(lognormalMean**2))
        muSum = np.log(lognormalMean) - sigma2Sum/2.0
        # to implement logNormal with parameters mu and sigma2
        # call lognorm.pdf(s=sigma,scale=np.exp(mu))
        ppf = lognorm.ppf(p,s=np.sqrt(sigma2Sum),scale= np.exp(muSum))
        return ppf




    def predictCompletionTime(self,n=1,p=0.5):
        # time to complete n tasks with prob p
        return self.posteriorDistributionOfSum_ppf (n,p)



    def updateModelParameters(self,x):
        x = np.array(x)
        if self.memory_size<x.size:
            x = x[-self.memory_size:]
        if self.logNormal:
            x[x<1e-30] = 1e-30
            x = np.log(x)
        n = x.size
        m = x.mean()
        v = x.var()
        self.alpha = self.alpha + n/2.0
        self.beta= self.beta + 0.5* (n*v + (self.kappa*n/(self.kappa+n))*(m-self.theta)**2)
        self.theta = (self.kappa * self.theta + n*m)/(self.kappa +n)
        self.kappa = self.kappa + n
        self.forget()

    def forget(self):
        if self.memory_size == np.inf:
            return
        if self.kappa > self.memory_size:
            self.kappa = self.memory_size
            c = self.beta/self.alpha
            self.alpha=  self.memory_size/2.0
            self.beta =  c* self.alpha

    def averageLogLikelihood(self, n,x):
        # eac element of x is the sum of n Observations
        pdf= self.predictiveDistributionOfSum_pdf(n,x)
        lpdf =np.log(pdf)
        return np.mean(lpdf)


    def displaySigma2Distribution(self,c='r'):
        plt.subplot(1,2,2)
        l0 = invgamma.ppf(0.005,a = self.alpha, scale = self.beta)
        l1 = invgamma.ppf(0.995, a = self.alpha, scale = self.beta)
        if np.isnan(l0) or np.isnan(l1) or np.isinf(l0) or np.isinf(l1):
            return
        step = (l1-l0)/10000.0
        if step ==0:
            return
        x_v = np.arange(l0,l1,step)
        pdf_v1 = invgamma.pdf(x_v,a = self.alpha, scale = self.beta)
        fsigma2 = plt.plot(x_v,pdf_v1,c)
        plt.xlabel('Predicted Variance')

    def displayExpSigmaDistributionLogNormal(self,c='r'):
        l0 = invgamma.ppf(0.005,a = self.alpha, scale = self.beta)
        l0 = np.exp(np.sqrt(l0))
        l1 = invgamma.ppf(0.995, a = self.alpha, scale = self.beta)
        l1 = np.exp(np.sqrt(l1))
        if np.isnan(l0) or np.isnan(l1) or np.isinf(l0) or np.isinf(l1):
            return
        step = (l1-l0)/10000.0
        if step ==0:
            return
        y = np.arange(l0,l1,step)
        x = (np.log(y))**2
        dydx = y/(2*np.sqrt(x))
        pdf = invgamma.pdf(x,a = self.alpha, scale = self.beta)/dydx
        plt.plot(y,pdf,c)
        plt.xlabel('Predicted Scatter')
    def displayMuDistribution(self,c='r'):
        plt.subplot(1,2,1)
        df = 2*self.alpha
        epsilon = np.sqrt(self.beta/(self.alpha* self.kappa))
        l0 = t.ppf(0.005,df=df, loc = self.theta,  scale = epsilon)
        l1 = t.ppf(0.995,df=df, loc = self.theta,  scale = epsilon)
        if np.isnan(l0) or np.isnan(l1) or np.isinf(l0) or np.isinf(l1):
            return
        step=(l1-l0)/10000.0
        if step ==0:
            return
        x_m = np.arange(l0,l1,step)
        pdf_t = t.pdf(x_m,df=df, loc = self.theta,  scale = epsilon)
        fm=plt.plot(x_m,pdf_t,c)
        plt.xlabel('Predicted Mean')
        plt.ylabel('Probability Density')
        return fm

    def displayExpMuDistributionLogNormal(self,c='r'):
        #expMu is the median of the lognmormal
        #plt.subplot(1,2,1)
        df = 2*self.alpha
        epsilon = np.sqrt(self.beta/(self.alpha* self.kappa))
        l0 = t.ppf(0.005,df=df, loc = self.theta,  scale = epsilon)
        l0 = np.exp(l0)
        l1 = t.ppf(0.995,df=df, loc = self.theta,  scale = epsilon)
        l1 = np.exp(l1)
        if np.isnan(l0) or np.isnan(l1) or np.isinf(l0) or np.isinf(l1):
            return
        step=(l1-l0)/10000.0
        if step ==0:
            return
        x_m = np.arange(l0,l1,step)
        pdf_t = self.logTpdf(x_m,df=df, loc = self.theta,  scale = epsilon)
        plt.plot(x_m,pdf_t,c)
        plt.xlabel('Predicted Median')
        plt.ylabel('Probability Density')
        return

    def displayPredictiveDistribution(self,c='r'):
        plt.subplot(1,2,1)
        df = 2*self.alpha
        gamma = np.sqrt((self.kappa+1)*self.beta/(self.alpha* self.kappa))
        l0 = t.ppf(0.005,df=df, loc = self.theta,  scale = gamma)
        l1 = t.ppf(0.995,df=df, loc = self.theta,  scale = gamma)
        if np.isnan(l0) or np.isnan(l1) or np.isinf(l0) or np.isinf(l1):
            return
        step=(l1-l0)/10000.0
        if step ==0:
            return
        x_m = np.arange(l0,l1,step)
        pdf_t = self.predictiveDistribution_pdf(x_m)
        fm=plt.plot(x_m,pdf_t,c)
        plt.xlabel('Predicted Observation ')
        plt.ylabel('Probability Density')
        return fm


    def displayPredictiveDistributionOfSum(self,n=1,c='r'):
        if n ==1:
            self.displayPredictiveDistribution(c)
            return
        plt.subplot(1,2,1)
        theta_of_sum,sigma2_of_sum = self.getParametersOfposteriorDistributionOfSum(n)
        l0 = norm.ppf(0.005,loc = theta_of_sum,scale= np.sqrt(sigma2_of_sum))
        l1 = norm.ppf(0.995,loc = theta_of_sum,scale= np.sqrt(sigma2_of_sum))
        if np.isnan(l0) or np.isnan(l1) or np.isinf(l0) or np.isinf(l1):
            return
        if l1 - l0 ==0:
            return
        x = np.arange(l0,l1,(l1-l0)/10000.0)
        y = norm.pdf(x,loc = theta_of_sum,scale= np.sqrt(sigma2_of_sum))
        y = self.predictiveDistributionOfSum_pdf(n,x)
        plt.plot(x,y,c)
        plt.xlabel('Predicted Sum '+str(n)+' Observations')
        plt.ylabel('Probability Density')


def compareModels(m1,m2):
    if m1.kappa != m2.kappa:
        print('different kappas')
        return -1
    if m1.theta != m2.theta:
        print('different thetas')
        return -1
    if m1.alpha !=  m2.alpha:
        print('different alphas')
        return -1
    if m1.beta !=  m2.beta:
        print('different betas')
        return -1
    if m1.memory_size != m2.memory_size:
        print('different memory size'   )
        return -1
    return 1
