from normalFrequentistModel import *


true_mean= 10
true_sd= 10
n_samples = 100


model = NormalFrequentistModel()
o = true_mean + true_sd*norm.rvs(size=n_samples)
model.updateModelParameters(o)

# confirm results
print('mu estimate', model.getExpectedMu(), 'sample mean', o.mean())
print('sigma2 estimate',model.getExpectedSigma2(),'unbiased sample variance', o.var()*(n_samples)/(n_samples -1.0))
print('Mu Lower Bound',model.lowerBoundOfMu())
print('Mu Upper Bound',model.upperBoundOfMu())
print('Mu Conf Interval',model.confIntervalOfMu())
print('Lambda Upper Bound ',model.upperBoundOfLambda())
print('Lambda Lower Bound ',model.lowerBoundOfLambda())
print('Lambda Upper Bound ',model.upperBoundOfLambda())
print('Lambda Conf Interval ',model.confIntervalOfLambda())
print('Sigma2 Lower Bound ',model.lowerBoundOfSigma2())
print('Sigma2 Upper Bound ',model.upperBoundOfSigma2())
print('Sigma2 Conf Interval',model.confIntervalOfSigma2(p=0.95))
