from normalGammaModel import *





class NormalGammaConjugateModelEnsemble():

    def __init__(self,memory_sizeList=[20,200,2000,20000,200000],logNormal=False):
#Approved 5/21/22
        self.memory_size= memory_sizeList
        self.nModels = len(self.memory_size)
        self.logNormal = logNormal
        self.model=[]
        n_sample= 1e-30
        mean_0 = 100
        var_0  = 100
        for k in range(self.nModels):
            self.model.append(NormalGammaConjugateModel(memory_size=self.memory_size[k],
                logNormal=self.logNormal))


        self.setPriorsUsingVirtualSample(n_sample,mean_0,var_0)

        self.bestModelCounts = np.zeros(self.nModels)
        self.betterThanModel_0_Counts = np.zeros(self.nModels)
        self.consecutiveLosses= np.zeros(self.nModels)
    def setPriorsUsingVirtualSample(self,n_sample,mean,var):
#Approved 5/21/22
        for k in range(self.nModels):
            self.model[k].setPriorsUsingVirtualSample(n_sample,mean,var)

    def updateModelParameters(self,x):
#Approved 5/21/22
        for k in range(self.nModels):
            self.model[k].updateModelParameters(x)

    def updateBestModelCounts(self,x):
#Approved 5/21/22
        bestModels,betterThanModel_0 = self.bestModels(x)
        for k in range(self.nModels):
            if bestModels[k] == True:
                self.bestModelCounts[k]+=1
            if betterThanModel_0[k] == True:
                self.consecutiveLosses[k]=0
                self.betterThanModel_0_Counts[k]+=1
            else:
                self.consecutiveLosses[k]+= 1

    def getAlpha(self):
#Approved 5/21/22
        alpha =[]
        for k in range(self.nModels):
            alpha.append(self.model[k].alpha)
        return np.array(alpha)

    def getBeta(self):
#Approved 5/21/22
        beta =[]
        for k in range(self.nModels):
            beta.append(self.model[k].beta)
        return np.array(beta)

    def getTheta(self):
#Approved 5/21/22
        theta=[]
        for k in range(self.nModels):
            theta.append(self.model[k].theta)
        return np.array(theta)

    def getKappa(self):
#Approved 5/21/22
        kappa =[]
        for k in range(self.nModels):
            kappa.append(self.model[k].kappa)
        return np.array(kappa)

    def getExpectedMu(self):
#Approved 5/21/22
        return self.getTheta()

    def getVarianceOfMu(self):
#Approved 5/21/22
        var=[]
        for k in range(self.nModels):
            var.append(self.model[k].getVarianceOfMu())
            return np.array(var)


    def getExpectedLambda(self):
#Approved 5/21/22
        l=[]
        for k in range(self.nModels):
            l.append(self.model[k].getExpectedLambda())
        return np.array(l)

    def getExpectedSigma2(self):
#Approved 5/21/22
        e =[]
        for k in range(self.nModels):
            e.append(self.model[k].getExpectedSigma2())
        return np.array(e)


    def getExpectedX(self):
#Approved 5/21/22
        return self.getExpectedMu()

    def getExpectedVarianceOfX(self):
#Approved 5/21/22
        var=[]
        for k in range(self.nModels):
            var.append(self.model[k].getVarianceOfX())
            return np.array(var)

    def getAverageKappa(self):
#Approved 5/21/22
        '''
        Each model has Kappa  prior+real observations
        the opinion of a model is weighted by the Probability
        that the model was best in past observations
        The average kappa is the weighted sum of the kappa of each model
        time is probability of winning
        It tells us how many observations we have supporting the model ensemble
        '''

        kappa=[]
        for k in range(self.nModels):
            kappa.append(self.model[k].kappa)
        freq = self.bestModelCounts
        s = freq.sum()
        if s>0:
            freq= freq/s
            averageKappa = np.sum(np.array(freq)*np.array(kappa))
        else:
            averageKappa=0.0
        return(averageKappa)

    def resetParameters(self):
#Approved 5/21/22
        for k in range(self.nModels):
            self.model[k].resetParameters()

    def pdf(self,x):
#Approved 5/21/22
        pdf=[]
        for k in range(self.nModels):
            pdftmp=self.model[k].predictiveDistribution_pdf(x)
            pdf.append(pdftmp)
        return pdf

    def bestModels(self,x):
#Approved 5/21/22
        bestModels = np.full(self.nModels,True)
        betterThanModel_0= np.full(self.nModels,True)
        pdf= self.pdf(x)
        runningMax = np.maximum.accumulate(pdf)
        for k in range(1,self.nModels):
            if pdf[k] < pdf[0]:
                betterThanModel_0[k] = False

        bestarg = np.argmax(pdf)
        best = pdf[bestarg]
        bestModels = np.squeeze(pdf==best)
        #print(pdf)
        return bestModels,betterThanModel_0



    def predictCompletionTime(self,n=1,p=0.5):
        ''' This is the most important function. Our goal
        is to predict how long it will take to complete n jobs
        with probability larger than p.
        We use an ensemble of models, each with different memory size
        Models with more memory tend to be better at predicting long term future
        Models with short memory tend to be better at predicting the near future
        Input n is the number of tasks we need to complete.
        Input p is the desired completion probability.


        Each model makes a prediction for the completion time. And these
        predictions are weighted by the proportion of time they made the best
        predictions in the past f

        First we measure the average alpha of the different
        models weighted by the proportion of times the models won in the past



        Given a query n we get the prediction from each model and weight
        this prediction by the number of times that each model made
        the best prediction  in the past.
    '''
        if n<=0:
            return 0



        completionTime=[]
        mem=[]
        freq=[]
        kappa =[]
        mc = np.array(self.bestModelCounts)

        for k in range(self.nModels):
            ct= self.model[k].predictCompletionTime(n,p)
            if type(ct) == str:
                print('Ignoring Model',k, 'Ilegal Variance')
            else:
                completionTime.append(ct)
                freq.append(mc[k])
                kappa.append(self.model[k].kappa)
        completionTime = np.array(completionTime)
        freq = np.array(freq)
        kappa = np.array(kappa)
        if sum(freq)>0:
            freq = freq/sum(freq)
        else:
            print('Not Enough Observations. Cannot Make Prediction')
            return -1
        averageCompletionTime= sum(freq*completionTime)
        averageKappa = sum(freq*kappa)
        return(averageCompletionTime,averageKappa)
