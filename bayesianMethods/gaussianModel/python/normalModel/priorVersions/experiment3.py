from scipy.stats import gamma,invgamma,t,lognorm,norm
from scipy.special import gamma as gammaFunction
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import NearestNeighbors
from scipy.stats import randint as randint
import warnings
import sys
warnings.simplefilter('error')

# this makes me wonder about the random number generator

x = norm.rvs(size=2000)
countList = []
counts=0
winList=[]
for i in range(1,len(x)):
    # with the line below the max count is about 17
    if x[i] > 0:
    # with the line below the max count is about 7
    # this shows the samples are not independent.
    # it kind of make sense ;{x_i > x_{i-1}} and {x_{i-1} > x_{i-2}} are
    # not independent events. if x_i < x_{i-1} then x_i is small
    # so it is hard for x_{i+1} to be smaller than x_i
    #if x[i] > x[i-1]:

        counts =0
        winList.append(1)
    else:
        counts +=1
        winList.append(0)
    countList.append(counts)

cl = np.array(countList)
print(max(countList))
wl = np.array(winList)
#
# counts, bins, bars= plt.hist(cl,rwidth=0.8)
# plt.show()
