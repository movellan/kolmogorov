# simple experiment to verify things work as expected
from normalGammaModel import *

#np.random.seed(seed=233423)


me = NormalGammaConjugateModelEnsemble(memory_sizeList=[10,1000],lagPoints=[0,1])
pdf=[]
v = 0
vList =[]
count =0
muList=[]
lowerBoundList=[]
upperBoundList=[]
uncertaintyList=[]
xList=[]
consecutiveLossesList=[]
cll=[]
xmean=100
nsims=3000
for i in range(1,nsims+1):
    if i%1==0:
        cl = np.copy(me.consecutiveLosses)
        consecutiveLossesList.append(cl)
        if i> 1000:
            xmean=130
        if i> 2000:
            xmean=100

    x = xmean+10*norm.rvs(size=1)
    xList.append(x)
    xpdf =me.getPdfs(x)
    print(i,cl,xpdf)



    consecutiveLossThreshold =14
    for k in range(me.nModels):
        if me.consecutiveLosses[k] > consecutiveLossThreshold:
            print('Disruption Detected')
            print(xpdf)
            print('Resetting Model',k)
            me.model[k].mu   = me.model[0].mu
            me.model[k].kappa = me.model[0].kappa
            c = me.model[0].alpha[0]/me.model[k].alpha[0]
            print('c',c)
            me.model[k].alpha[0] = c* me.model[k].alpha[0]
            me.model[k].beta[0] =  c* me.model[k].beta[0]
            me.consecutiveLosses[k] =0


    muList.append(me.model[-1].mu)
    if i <100:
        upperBoundList.append(0)
        lowerBoundList.append(0)
        uncertaintyList.append(0)
    else:
        upperBoundList.append(me.model[-1].predictiveDistribution_ppf(0.95))
        lowerBoundList.append(me.model[-1].predictiveDistribution_ppf(0.05))
        uncertaintyList.append(me.model[-1].predictiveDistribution_ppf(0.95)-me.model[-1].predictiveDistribution_ppf(0.05))
    pdf.append(xpdf)
    me.updateBestModelCounts(x)

    me.updateModelParameters(x)

pdf = np.squeeze(np.array(pdf))

cl = np.array(consecutiveLossesList)
cl = cl[:,1]

counts, bins, bars= plt.hist(cl,rwidth=0.8)
plt.show()
#
plt.plot(pdf[:,0],'r')
plt.plot(pdf[:,1],'g')
plt.show()
plt.plot(xList[100:],'r')
plt.plot(muList[100:],'b')
plt.plot(lowerBoundList[100:],'g--')
plt.plot(upperBoundList[100:],'g--')
plt.show()
