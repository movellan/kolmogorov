# simple experiment to verify things work as expected
from normalGammaModelEnsemble import *

#np.random.seed(seed=233423)


me = NormalGammaConjugateModelEnsemble(memory_sizeList=[10,np.inf])
pdf=[]
v = 0
vList =[]
count =0
expectedMuList=[]
lowerBoundList=[]
upperBoundList=[]
lowerBoundList2=[]
upperBoundList2=[]

uncertaintyList=[]
uncertaintyList2=[]
xList=[]
consecutiveLossesList=[]
cll=[]
xmean=100
nsims=3000
for i in range(1,nsims+1):
    if i % 100 ==0:
        print(i)

    cl = np.copy(me.consecutiveLosses)
    consecutiveLossesList.append(cl)
    if i> 1000:
        xmean=160
    if i> 2000:
        xmean=100

    x = xmean+20*norm.rvs(size=1)
    xList.append(x)
    xpdf =me.pdf(x)




    consecutiveLossThreshold =14
    for k in range(me.nModels):
        if me.consecutiveLosses[k] > consecutiveLossThreshold:
            print('Disruption Detected')
            print(xpdf)
            print('Resetting Model',k)
            me.model[k].theta   = me.model[0].theta
            me.model[k].kappa = me.model[0].kappa
            c = me.model[0].alpha/me.model[k].alpha
            print('c',c)
            me.model[k].alpha = c* me.model[k].alpha
            me.model[k].beta =  c* me.model[k].beta
            me.consecutiveLosses[k] =0


    expectedMuList.append(me.model[-1].theta)
    if i <100:
        upperBoundList.append(0)
        lowerBoundList.append(0)
        uncertaintyList.append(0)
        upperBoundList2.append(0)
        lowerBoundList2.append(0)
        uncertaintyList2.append(0)
    else:
        upperBoundList.append(me.model[-1].predictiveDistribution_ppf(0.995))
        lowerBoundList.append(me.model[-1].predictiveDistribution_ppf(0.005))
        lb2, averageKappa = me.predictCompletionTime(1,0.005)
        lowerBoundList2.append(lb2)
        ub2,averageKappa= me.predictCompletionTime(1,0.995)
        upperBoundList2.append(ub2)
        uncertaintyList.append(me.model[-1].predictiveDistribution_ppf(0.995)-me.model[-1].predictiveDistribution_ppf(0.005))
        uncertaintyList2.append(ub2-lb2)
    pdf.append(xpdf)
    me.updateBestModelCounts(x)

    me.updateModelParameters(x)

pdf = np.squeeze(np.array(pdf))

cl = np.array(consecutiveLossesList)
cl = cl[:,1]

counts, bins, bars= plt.hist(cl,rwidth=0.8)
plt.show()
#
plt.plot(pdf[:,0],'r')
plt.plot(pdf[:,1],'g')
plt.show()
plt.plot(xList[100:],'r')
plt.plot(expectedMuList[100:],'b')
plt.plot(lowerBoundList[100:],'g--')
plt.plot(upperBoundList[100:],'g--')
plt.plot(lowerBoundList2[100:],'b--')
plt.plot(upperBoundList2[100:],'b--')
plt.show()
plt.plot(uncertaintyList[100:],'r')
plt.plot(uncertaintyList2[100:],'g')
plt.show()
