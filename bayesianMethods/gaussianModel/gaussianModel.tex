\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}

 \title{Probabilistic Inference with Gaussian Data:  Bayesian and Frequentist Approaches}
\author{Javier R. Movellan}


\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\R}{\mathcal{R}} 





\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}



\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{lem}{Lemma}[section]
\theoremstyle{definition}
\newtheorem{rem}{Remark}[section]
\theoremstyle{definition}
\newtheorem{ex}{Example}[section]




\usepackage{verbatim}

\begin{document}
\maketitle

    
\newpage


\section{Problem Statement}
We present and compare Bayesian and Frequentist methods for making inferences about  the unknown mean $\mu$ and variance $\sigma^2$  of  a Gaussian random variable $X$ based on  $n$ independent samples $X_1,\cdots, X_n$  of that variable. 
\section{Bayesian Approach}
In the Bayesian approach $\mu$ and $\sigma^2$ are treated as random variables. The data generation process is modeled as follows:  First a variance  $\sigma^2 $ and a mean $\mu$ are sampled from a known prior  distribution. Then the observed data $X_1,\cdots, X_n$ are generated from a Gaussian distribution with mean $\mu$ and variance $\sigma^2$. Bayesian inference focuses on computing the posterior distribution of $\mu,\sigma^2$ given the observed data $X_1, \cdots, X_n$. This posterior distribution contains all the information about $\mu,\sigma^2$  in the prior knowledge and  the observed data. 
\subsection{The IGG Bayesian  Model}
The  Inverse-Gamma Gaussian model (iGG)  is  a popular Bayesian choice because it provides a simple way to compute posterior distributions and because its  relationship to  frequentist approaches is well understood.  The iGG model is determined by three parameters: $n,\bar x, s^2$. These parameters can be interpreted as the  size, mean, and variance of a virtual sample of observations that contain all the information accumulated so far, including prior information and information from the observed sample: 
\begin{itemize}
\item $n$: the size of the virtual sample. It may  be a non-integer.
\item $\bar x$: the mean of the virtual sample
\item $s^2>0$: the variance  of the virtual sample
\end{itemize}
\paragraph{Update Equations:}
It can be shown that if the prior distribution of $\sigma^2, \mu$ is iGG with parameters $n_0,\bar x_0, s^2_0$  and we observe a sample of size $n$ with mean $\bar x$ and variance $s^2$  then the posterior distribution is also iGG with parameters $n_1, \bar x_1, s^2_1$, where 
\begin{align}
&n_1 = n_0 +n \\
&\bar x_1 = \frac{n_0 \bar x_0 + n \bar x}{n_0 +n}\\
&s^2_1 = \frac{n_0 s^2_0 + n s^2 + n_0 n (\bar x - \bar x_0)^2/(n_0 +n)}{n_0+n}
\end{align}
Note the relative importance of prior  information and observed data is determined by the $n_0$ and $n$ parameters. If $n_0=0$ the prior information has no effect. It can be shown that the update equations are simply the standard equations for updating sample means and sample variances in an iterative manner (see Appendix).  Here we are simply combining the mean and variances of a sample of size $n_0$ with the means and variances of a sample of size $n$. In frequentist approaches $n_0=0$, i.e. the prior information has no effect. In the Bayesian approach we can opt to make $n_0 \neq 0$ thus letting prior knowledge have an effect in our estimates in the form o a prior "virtual sample" with parameters $\bar x_0, s^2_0,n_0$. These prior parameters represent the prior guess about the true mean, the prior guess about the true variance, and the desired impact of the prior guess. This "virtual sample" combines with the observed data in the way you would expect a  real sample available prior to data collection. \paragraph{Inferences about $\sigma^2$:}
If $\sigma^2,\mu$ have an  iGG posterior distribution with parameters $n_1,\bar x_1,s^2_1$ then the marginal posterior distribution of $\sigma^2$ is inverse Gamma (iG) with shape parameter $\alpha_1$ and scale parameter $\beta_1$ defined as follows (see Appendix):
\begin{align}
&\alpha_1 = \frac{n_1}{2}\\
&\beta_1 = \frac{n_1 s^2_1}{2}
\end{align}
We can use the Percent Point Function  $Q_{\sigma^2}$  of the iG distribution to get the posterior Median, Standard Variation,  Lower and Upper Confidence Bounds  on $\sigma^2$, see Appendix. 
\begin{align}
&Median[\sigma^2\given x_{1:n}] = Q_{\sigma^2}(0.5)\\
&SV[\sigma^2\given x_{1:n}] = \frac{1}{2} \Big(Q_{\sigma^2}(\Phi(1)) - Q_{\sigma^2}(\Phi(-1)) \Big)\\
&L_\gamma[\sigma^2\given x_{1:n}] = Q_{\sigma^2}( 1-\gamma)\\
&U_\gamma[\sigma^2\given x_{1:n}] = Q_{\sigma^2}(\gamma)\\
\end{align}
where $SV$ stands for Standard Variation, $\Phi$ is the zero mean, unit variance Gaussian cumulative distribution function (see Appendix), $L_\gamma$ lower bounds $\sigma^2$ with probability $\gamma$ and $U_\gamma$ upper bounds $\sigma^2$ with probability $\gamma$. 
Using the properties of the iG distribution (see Appndix)  it follows that: 
\begin{align}
&E[\sigma^2 \given x_{1:n}]=   s_1^2 \frac{n_1}{n_1-2}\\
&Var[\sigma^2 \given x_{1:n} ] =  \frac{2}{n_1-4}  E^2[\sigma^2 \given x_{1:n} ],\;\text{for $n_1>4$}
\end{align}

\paragraph{Inferences about $\mu$:}
If $\sigma^2,\mu$ have an iGG posterior distribution with parameters $n_1,\bar x_1,s^2_1$ then the marginal posterior distribution of $\mu$ is T with  $n_1$ degrees of freedom,  location parameter $\bar x_1$ and scale parameter $\sqrt{s^2_1/n_1}$. We can use the percent point function of the $T$ distribution to compute the posterior Median, Standard Variation, and Lower and Upper Confidence Bounds for $\mu$
\begin{align}
&Median[\mu \given x_{1:n}] = Q_{\mu}(0.5)\\
&SV[\mu \given x_{1:n}] = \frac{1}{2} \Big(Q_{\mu}(\Phi(1)) - Q_{\mu}(\Phi(-1)) \Big)\\
&L_\gamma[\mu \given x_{1:n}] = Q_{\mu}( 1-\gamma)\\
&U_\gamma[\mu \given x_{1:n}] = Q_{\mu}(\gamma)\\
\end{align}
Moreover, using the properties of the T  distribution (see Appendix): 
\begin{align}
&E[\mu \given x_{1:n}] = \bar x_1\\
&Var[\mu \given x_{1:n}] = \frac{s^2_1}{n_1-2},\;\text{for $n_1 >2$}
\end{align}
\paragraph{Inferences about Sums of Observations}
If the posterior distribution of $\mu,\sigma^2$ is iGG with parameters $n_1,\bar x_1, s^2_1$ then the posterior distribution for a new observation $X$ is  $T$ with $n_1$ degrees of freedom, location parameter $\bar x_1$ and scale parameter $\sqrt{s^2_1 (n_1+1)/n_1)}$. We can use the percent point function of the $T$ distribution to compute the posterior Median, Standard Variation, and Lower and Upper Confidence Bounds for $\mu$
\begin{align}
&Median[X \given x_{1:n}] = Q_{X}(0.5)\\
&SV[X\given x_{1:n}] = \frac{1}{2} \Big(Q_{X}(\Phi(1)) - Q_{X}(\Phi(-1)) \Big)\\
&L_\gamma[X \given x_{1:n}] = Q_{X}( 1-\gamma)\\
&U_\gamma[X \given x_{1:n}] = Q_{X}(\gamma)\\
\end{align}
Moreover, using the properties of the T distribution
\begin{align}
&E[X \given x_{1:n}] = \bar x_1\\
&Var[X \given x_{1:n}]  = s^2_1 \frac{n_1+1}{n_1-2}
\end{align}
Finally lets get the predictive distribution of the sum of $m$ future variables
\begin{align}
&S= \sum_{i=1}^m X_{n+i}\\
\end{align}
It follows that 
\begin{align}
&E[S\given x_{1:n}] = m E[X_{n+1}\given x_{1:n}] = m \bar x_1\\
&Var[S \given x_{1:n}] = m Var[X_{n+1} \given x_{1:n}] = m s^2_1 \frac{n_1+1}{n_1-2}
\end{align}
For $m$ sufficiently large we can use the Central Limit Theorem and  approximate the predictive  distribution as Normal with the mean and variance parameters described above. We can then use the Gaussian Percent Point Function to compute Medians, Standard Variation, and Lower/Upper confidence bounds. 
\begin{align}
&Median[S \given x_{1:n}] = Q_{S}(0.5)\\
&SV[S\given x_{1:n}] = \frac{1}{2} \Big(Q_{S}(\Phi(1)) - Q_{S}(\Phi(-1)) \Big)\\
&L_\gamma[S \given x_{1:n}] = Q_{S}( 1-\gamma)\\
&U_\gamma[XS\given x_{1:n}] = Q_{S}(\gamma)\\
\end{align}
\section{Frequentist Approach}
 
We observe $n$ iid Gaussian random variables $X_1, \cdots , X_n$ with unknown mean $\mu$ and variance $\sigma^2$. We summarize the observed data using 3 statistics:
\begin{itemize}
\item $n$. The sample size.
\item $\bar X = \frac{1}{n} \sum_{i=1}^n X_i$. The sample mean.
\item $S^2 = \frac{1}{n} \sum_{i=1}^n (X_i - \bar X)^2$. The sample variance. 
\end{itemize}
 \paragraph{Inferences about  $\sigma^2$:}
 Let $V =  n  S^2/\sigma^2$. It can be shown that $V$ has a Gamma distribution with shape parameter $\alpha = (n-1)/2$ and rate parameter $\beta= 0.5$.   From the properties of the gamma distribution it follows tht
 \begin{align}
&E[V\given \sigma^2] =E[ n S^2/\sigma^2 \given \sigma^2] =  n-1 \\
&Var[V\given \sigma^2] =  Var[S^2 \given \sigma^2] \frac{n^2 }{\sigma^4} =  2 (n-1)\\
\end{align}
Let $\hat \sigma^2 = nS^2/(n-1)$. Note
\begin{align} 
&E[\hat \sigma^2 \given \sigma^2]  = \sigma^2  \\
&Var[\hat \sigma^2 \given \sigma^2] =  \frac{2 \sigma^4 }{n-1}
\end{align}
Thus $\hat \sigma^2$ is un unbiased estimate of $\sigma^2$ and  $2\hat \sigma^4/(n-1)$ is an estimate of the variance of the estimate. For comparison with the Bayesian approach, if we use a prior iGG distribution with $n_0=0$ we get
\begin{align} 
& E[ \sigma^2 \given x_{1:n}] = s^2 \frac{n}{n-2} \\
&  Var[ \sigma^2 \given x_{1:n}] = \frac{2 E^2[\sigma^2\given x_{1:n}] }{n-4} = 2 s^4 \frac{n^2}{(n-2)^2 (n-4)}
\end{align}
For the lower and upper bounds of  $\sigma^2$ with confidence $\gamma$.  Let $F$ be  the iG cumulative distribution with shape parameter $\alpha= (n-1)/2$ and scale parameter $\beta = nS^2/2$. Let $L$, $U$ be values such that 
\begin{align}
&F[L] = 1- \gamma\\
&F[U] = \gamma
\end{align}
It can be sown (see Appendix) that 
\begin{align}
P( L < \sigma^2])= \gamma\\
P(U > \sigma^2) = \gamma
\end{align}
Thus $L,U$ are the lower and upper confidence bounds of $\sigma^2$ with confidence $\gamma$. Similarly
\begin{align}
P[L< \sigma^2<U] = 1 - 2(1-\gamma) = 2 \gamma -1
\end{align}
and therefore $[L,U]$ is the confidence interval for $\sigma^2$ with confidence $2\gamma -1$. 
For comparison, the Bayesian approach with an iGG prior with $n_0=0$ utilizes an iG cumulative distribution with shape parameter $\alpha = n/2$ and scale parameter $\beta = nS^2/2$.

 \paragraph{Inferences about $\mu$:}
Let $Z= (\bar X - \mu)/\sqrt{\hat \sigma^2/n}$.  It can be shown that $Z$ has a T distribution with $n-1$ df.  This can be used to get upper bounds, lower bounds, and confidence intervals for $\mu$. In particular, let $\gamma$ be the desired confidence level, typically 0.95, or 9.9. Let  $u$ be a value such that $P(Z < u) = 1- \gamma$. This can be obtained from the T cumulative distribution function with $n-1$ df. Note
 \begin{align}
&P(Z < u) =  1- \gamma\\ 
&P( \frac{\bar X - \mu}{\sqrt{\hat \sigma^2/n}}  < u  \given \mu,\sigma^2)  =  1- \gamma\\
&P(- \mu < - \bar X + u \sqrt{\hat \sigma^2/n} \given \mu,\sigma^2) = \gamma\\
& P( \mu >  \bar X -  u \sqrt{\hat \sigma^2/n} \given \mu,\sigma^2) = \gamma
 \end{align}

Thus $\bar X -  u \sqrt{\hat \sigma^2/n}$ lower bounds $\mu$ with probability $\gamma$. Similarly it can be shown that $\bar X + u\sqrt{\hat \sigma^2/n}$ upper bounds $\mu$ with probability $\gamma$.  For comparison, if the prior distribution is iGG with $n_0=0$ then the upper bound and lower bounds are 
$\bar X \pm u \sqrt{s^2/n}$ where $u$ is obtained from a T distribution with $n$ instead of $n-1$ df. 
Using the properties of the expected value and variance, it is easy to see that 
\begin{align}
&E[\bar X\given \mu, \sigma^2] = \mu\\
&Var[\bar X \given \mu,\sigma^2]  = \frac{\sigma^2}{n}
\end{align}

\begin{table}
\begin{center}
\begin{tabular}{|c|c|c|}
\hline
 &Bayesian&Frequentist \\\hline
  $\mu$, Core Distribution&T, df=n, $\beta = \sqrt{s^2_1 /(n_1-2)} $&T, df =n-1, $\beta = \sqrt{s^2/(n-1)}$ \\ \hline
 $\hat \mu$ &$\bar x_1$&$\bar x$\\ \hline
 $Var[\hat \mu]$ &$s^2_1 n_1/(n_1-2)$&$s^2n/(n-1)$\\ \hline
  $\sigma^2$, Core Distribution &iG, $\alpha = n_1/2, \beta = n_1 s^2_1/2$&iG, $\alpha= (n-1)/2, \beta= ns^2/2$ \\ \hline
  $\hat \sigma^2$    &$s^2_1 n_1/(n_1-2)$&$s^2n/(n-1)$ \\ \hline
 $Var[\hat \sigma^2]$ &2 $\hat \sigma^4/(n_1-4)$&$2 \hat \sigma^4/(n-1)$ \\ \hline
 $X$, Core Distribution &T, df = $n_1$, $\beta = \sqrt{s^2_1 (n_1+1)/(n_1-1)}$ &$\cdot$\\ \hline
 $E[X \given x_{1:n}]$ &$\bar x_1$ &$\cdot$ \\ \hline
  $Var[ X \given x_{1:n}]$ &$s^2_1 (n_1+1)/(n_1-2)$ &$\cdot$ \\ \hline
  $S$, Core Distribution &Gaussian&$\cdot$\\ \hline
 $E[S \given x_{1:n}]$ &$m \bar x_1$ &$\cdot$ \\ \hline
 $Var[ S\given x_{1:n}]$ &$m s^2_1 (n_1+1)/(n_1-2)$ &$\cdot$ \\ \hline
  \end{tabular}
  \caption{\it Comparison between the Bayesian and  Frequentist approach. $\hat \mu,\hat \sigma^2$ are the estimates for $\mu,\sigma^2$. $Var[\hat \mu], Var[\hat \sigma^2]$ are the estimate of the variance of $\hat \mu$ and  $\hat \sigma^2$. $E[X\given x_{1:n}]$, $Var[X \given x_{1:n}$ are the mean and variance of the predictive distribution for a single observation. $E[S\given x_{1:n}]$, $Var[S\given x_{1:n}]$ are the mean and variance for the predictive distribution of the sum of $m$ observations.  $Bounds[\mu]$, $Bounds[\sigma^2]$,$ Bounds[X]$, $Bounds[S]$  indicates the distribution used to get upper and lower bounds on $\mu$, $\sigma^2$, individual observations $X$ and sum of $m$ observations. T stands for the T distribution, df for degrees of freedom and $\beta$ for the scale parameter. iG stands for the inverse Gaussian distribution with shape parameter $\alpha$ and scale parameter $\beta$. }
\end{center}
\end{table}
There are no well known frequentist approaches to get confidence intervals for a sum of $m$ observations. The issue at hand is how to combine the uncertainty about $\mu$ and about $\sigma^2$ to estimate the uncertainty about individual observations or sums of it.



\section{Example}
Suppose we got a sample of 10 observations which happen to be the integers between 1 and 10, i.e., $x_{1:n} = [1,2,3,4,5,6,7,8,9,10]$. Thus
\begin{align}
&n = 10\\
&\bar x = \frac{ \sum_{i=1}^n x_i}{n} = 5.5\\
&s^2 = \frac{\sum_{i=1}^n (x_i - \bar x)^2}{n} = 8.25
\end{align}

We will consider three approaches: (1) A Bayesian approach with Influential Priors.  (2)  The frequentist approach. (3) A Bayesian approach with no influence priors. 
\paragraph{Case 1: Bayesian with Influential  Priors.}
For case 1, let's suppose based on prior experiments we think that the true mean is around $4$, and true variance is around $10$. We want for the inluence of the prior knowledge to be about 10\% of the weight of the knowledge provided by the observed sample. Thus
\begin{align}
&n_0 = n/10 =1\\
&\bar x_0 = 4\\
&s^2_0 = 10
\end{align}
Thus the posterior parameters are
\begin{align}
&n_1 = n_0 + n = 11\\
&\bar x_1 = \frac{ n_0 \bar x_0 + n_1 \bar x}{n_0 + n_1} = 5.36\\
&s^2_1 = \frac{n_0 s^2_0 + n s^2 + n_0 n (\bar x - \bar x_0)^2/(n_0 +n)}{n_0+n} \nonumber\\
&\;\;\;\;= \frac{ 10 + (10)(8.25) + 10 (10-5.5)^2/(11)}{11} 
= 8.59
\end{align}
Thus 
\begin{align}
&E[\mu \given x_{1:n}] = \bar x_1 = 5.36\\
&Var[\mu \given x_{1:n}] = \frac{s^2_1 }{n_1 -2} = 0.95\\
&E[\sigma^2 \given x_{1:n}] = s^2_1 \frac{n_1}{n_1-2} = 10.51\\
&Var[\sigma^2 \given x_{1:n}]  = 2\frac{2}{n_1-4}  E^2[\sigma^2 \given x_{1:n} ] = 31.53	
\end{align}
For the $\gamma = 0.95$ lower and upper bounds of $\sigma^2$ we use the cumulative distribution function of the iG distribution with shape parameter $\alpha = n_1/2=5.5$, and scale parameter $\beta = n_1  s^2/2=47.27$. We find that the cumulative distribution evaluated at $l=4.8$ is 0.05 and at $u= 20.67$ is 0.95. Thus the 95\% confidence lower and upper bounds of $\sigma^2$ are 4.8 and 20.67
\begin{align}
&P(\sigma^2 > 4.8\given x_{1:n})= 0.05\\
&P(\sigma^2 < 20.67\given x_{1:n}])= 0.95
\end{align}
For the $\gamma=0.95$ lower and upper bounds of $\mu$ we use the T distribution with $n_1 =11$ degrees of freedom. We find that the cumulative distribution evaluated at 1.796 is 0.95. Thus the upper and lower bounds for $\mu$ are as follows
\begin{align}
&l = \bar x_1 - 1.796 \sqrt{s^2_1/n_1}  =3.77\\
&u = \bar x_1 + 1.796 \sqrt{s^2_1/n_1} =6.95\\
&P(\mu > 3.77 \given x_{1:n}) =0.95\\
&P(\mu < 6.95 \given x_{1:n}) =0.95
\end{align}
For the predictive distribution of a new observation $X$ have
\begin{align}
&E[X\given x_{1:n}] = \bar x_n = 5.36\\
&Var[X \given x_{1:n}] =  s^2_1 \frac{n_1+1}{n_1-2} = 11.46
\end{align}
For the $\gamma= 0.95$ lower and upper bounds of $X$ we  use a T distribution with $n_1 =11$ degrees of freedom and scale parameter $\sqrt{s^2_1 (n_1+1)/n_1}$. We find that the cumulative distribution evaluated at 1.796 is 0.95. Thus the upper and lower bounds for $X$ are as follows
\begin{align}
&l = \bar x_1 - 1.796 \sqrt{s^2_1(n_1+1)/n_1}  =-0.14\\
&u = \bar x_1 + 1.796 \sqrt{s^2_1(n_1+1)/n_1} =10.86\\
&P(X> -0.14 \given x_{1:n}) =0.95\\
&P(X < 10.86 \given x_{1:n}) =0.95
\end{align}
We approximate the predictive distribution of the sum  $S$ of $m=100$ observations as a  Gaussian distribution with the following mean and variance: 
\begin{align}
&E[S\given x_{1:n}] = m  E[X\given x_{1:n}] = 100 \bar x_1= 536.36\\
&Var[S\given x_{1:n}] = m Var[X  \given x_{1:n} ]  = 1146.00
\end{align}
The standard cumulative Gaussian distribution evaluated at $-1.6448$ is 0.05. Thus the $\gamma=0.95$ confidence lower and upper bounds e sum are 
\begin{align}
&L = 536.36 - 1.65448\sqrt{1146.00} = 480.68\\
&U= 536.36 -1.6448 \sqrt{1146.00} = 592.05
\end{align}{
\paragraph{Case 2 Bayesian with no influence priors}
The formulas are the same as Case 1 but with $n_0=0$. The results are displayed in Table \ref{tab:results}. 
\paragraph{Case 3 Frequentist}
The unbiased estimates of $\mu$ and $\sigma^2$ and  the estimates of their variance are as follows:
\begin{align}
&\hat \mu = \bar x= 5.5\\
&\hat \sigma^2_{\hat \mu} = \frac{\hat \sigma^2}{n} = 0.92\\
&\hat \sigma^2 = s^2 \frac{n}{n-1} =9.17\\
&\hat \sigma^2_{\hat \sigma^2} = \frac{2 (\hat \sigma^2)^2}{n-1} = 18.67
\end{align}
We use the tables for the iG cumulative distribution function with shape parameter $\alpha = n-1 = 9$ and $\beta = n *s^2/2= 41.25$. We find that when evaluated at 4.87 the output is  $1-\gamma=0.05$  and when evaluated at 24.81   $\gamma = 0.95$. Thus the lower and upper bounds are 4.87 and 24.81. 

We use the T cumulative distribution function with $n-1 = 9$ degrees of freedom. We find that when evaluated at 1.83 the output is   $\gamma=0.95$. Thus the lower bound is $\hat \mu - 1.83  \sqrt{\hat \sigma^2/n} = 3.74$, and the upper bound is $\hat \mu + 1.83 \sqrt{\hat \sigma^2/n} = 7.26$. 
\begin{table}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
 &Bayesian $n_0=1$ &Bayesian $n_0=1$ &Frequentist \\\hline
 $\hat \mu$   &5.36&5.5&5.5 \\ \hline
 $Var[\hat \mu]$ &0.96&1.03&0.92 \\ \hline
 $LB[\mu]$ &3.78&3.85&3.74 \\ \hline
  $UB[\mu]$ &6.95&7.14&7.26 \\ \hline
  $\hat \sigma^2$   &10.51&10.31&9.17 \\ \hline
 $Var[\hat \sigma^2]$ &31.53&35.44&18.67 \\ \hline
 $LB[\sigma^2]$ &4.8&4.5&4.87 \\ \hline
  $UB[\sigma^2]$ &20.67&20.93&24.81\\ \hline
  $E[X \given x_{1:n}]$ &5.36 &5.5 &$\cdot$\\ \hline 
  $Var[X  \given x_{1:n}]$ &11.46 &11.34 &$\cdot$\\\hline
  $LB[X  \given x_{1:n}]$ &-0.13&0.04&$\cdot$ \\ \hline
  $UB[X  \given x_{1:n}]$ &10.86&10.96&$\cdot$\\ \hline
    $E[S  \given x_{1:n}]$ &536.36 &550 &$\cdot$\\ \hline 
  $Var[S  \given x_{1:n}]$ &1146.00 &1134.38 &$\cdot$\\\hline
  $LB[S  \given x_{1:n}]$ &480.68&494.06&$\cdot$ \\ \hline
  $UB[S \given x_{1:n}]$ &592.05&605.39&$\cdot$\\ \hline
  \end{tabular}
\end{center}
\caption{\it Results with the Bayesian Model 1 (influential prior), Bayesian Model 2 (no influence prior) and Frequentist Approach. $\hat \mu, \sigma^2$ are estimates of $\mu,\sigma^2$. $Var$ stands for estimate of the variability of the estimate. $X,S$ represent the predictive distribution for a single observation and a sum of 100 observations.  $LB,UB$ are the 95\% confidence Lower and Upper Bounds.  \label{tab:results}}
\end{table}


\section{Which Approach is the Right One:}
REFINE THIS SECTION:
First note  that as $n$ increases Bayesian and Frequentist  approaches converge to the same solution. However the  two approaches are solving different problems. A good way to understand the Bayesian approach is to consider the problem of guessing which integer, from 1 to 10 a person is thinking about. First we choose a prior distribution that encodes knowledge of how people choose numbers. A reasonable prior choice is that all integers have equal probability of being selected. Under this prior model, all numbers have equal probability. Suppose we learn that the number is even and larger than 2, this represents the observed data. The posterior probability given this new data would give zero probability to the numbers 1,2,3,5,7,9 and 1/3 probability to the numbers 4,6,8. This probability represents the proportion of times that the chosen number is given that the original number is chosen according with the prior and the fact that the chosen numbers is even and larger than 2.  





When the task is to make inferences about $\mu,\sigma^2$ the Bayesian approach is optimally solving the problem of guessing which $\mu,\sigma^2$ has been used to generate the observed data, given the fact that $\sigma^2,\mu$  are  generated according to a known prior probability. The  confidence intervals guarantee that when averaged across values of $\sigma^2,\mu$ as generated by the prior distribution they will cover the correct $\sigma^2,\mu$ with a known probability $\gamma$.   



The frequentist approach on the other hand defines probabilities  of observed data conditioned on the actual values of $\mu$ and $\sigma^2$. The confidence intervals guarantee that for any value  of $\sigma^2, \mu$  when averaged across experiments, the confidence intervals will cover the correct $\sigma^2,\mu$   with a known probability $\gamma$. 


\section{Appendix}
\subsection{Sequential Update of the Mean and  Variance}
We get an initial sample $x_1, \cdots,x_n$ with mean $\bar x$ and variance $s^2_x$
\begin{align}
&\bar x = \frac{\sum_{i=1}^n x_i}{n}\\
&s^2_x = \frac{\sum_{i=1}^n (x_i - \bar x)^2}{n}
\end{align}
We then get a sencond sample $y_1,\cdots, y_m$ with mean $\bar y$ and variance $s^2_y$
\begin{align}
&\bar y = \frac{\sum_{i=1}^m y_i}{m}\\
&s^2_y = \frac{\sum_{i=1}^n (y_i - \bar y)^2}{m}
\end{align}
Here we show that we can compute the overall mean and variance based on the sample means and variances
\begin{align}
\mu &= \frac{\sum_{i=1}^n x_i + \sum_{i=1}^m y_i}{n+m} = \frac{n \bar x + m \bar y}{n+m}\\
\sigma^2 &= \frac{\sum_{i=1}^n(x_i -\mu)^2 + \sum_{i=1}^m (y_i- \mu)^2}{n+m} \nonumber\\
&= \frac{n s^2_x + m s^2_y + nm (\bar x - \bar y)^2/(n+m)}{n+m}
\end{align}
The equation for the sum is obvious. Regarding the variance note
\begin{align}
&\sum_{i=1}^n(x_i - \mu)^2 = \sum_{i=1}^n(x_i - \bar x)^2 + n (\bar x - \mu)^2\\
&\sum_{i=1}^m(y_i - \mu)^2 = \sum_{i=1}^m(y_i - \bar y )^2 + m (\bar y - \mu)^2
\end{align}
Moreover
\begin{align}
\bar x - \mu &= \frac{\sum_{i=1}^n x_i}{n} - \frac{\sum_{i=1}^n x_i}{n+m} -\frac{\sum_{i=1}^m y_i}{n+m}\nonumber\\
&= (\sum_{i=1}^n x_i)(\frac{1}{n} - \frac{1}{n+m} ) - \frac{\sum_{i=1}^m y_i}{n+m}\nonumber\\
& = \frac{m}{m+n}(\bar x - \bar y)
\end{align}
Similarly
\begin{align}
\bar y - \mu = \frac{n}{m+n}(\bar y - \bar x)
\end{align}
Thus
\begin{align}
n (\bar x -\mu)^2 + m(\bar y - \mu)^2 &= \frac{n m^2}{(n+m)^2}  (\bar x - \bar y)^2 
+ \frac{m n^2}{(n+m)^2} \nonumber\\
&= \frac{n m}{n+m} (\bar x - \bar y)^2 
\end{align}
From which the desired formula follows.
\subsection{Cumulative Distribution Functions, Percent Point Functions,Median,Standard Variation}
The Cumulative Distribution Function (cdf) of a random variable $X$ is defined as follows
\begin{align}
F_X(x) = P(X<x) = \int_0^x p_X(u) du
\end{align}
where $p_X$ is the probability density function (pdf) of $X$. The Percent Point Function $Q_X$  is the inverse function of the cdf. Thus if 
\begin{align}
Q_X(p) =F^{-1}_X(p) =  u
\end{align}
then
\begin{align}
F_X(u) = p
\end{align}
The  Percent Point Function (ppf) is also known as the Quantile Function, Percentile Function,  and Inverse Cumulative Distribution Function (icdf). 
We can use the Percent Point Function to characterize the location and spread of a distribution. For the location we can use the Median
\begin{align}
Med[X] = Q_X(0.5)
\end{align}
And for the spread we can use the Standard Variation, a generalization of the Standard Deviation
\begin{align}
SV[X] = \frac{1}{2} \Big(Q_X(\Phi(1)) - Q_X(\Phi(-1)) \Big)
\end{align}
where $\Phi$ is the standard, zero mean, unit variance, cumulative distribution function. Note if $X$ is Gaussian then the Standard Variation equals the Standard Deviation. 

\subsection{ Chi-Squared ,Gamma, and  Inverse Gamma  Distributions}
$X$ is a Chi-squared random variable with $\nu$ degrees of freedom (df)  if 
\begin{align}
p(x \given \nu)  \propto x^{\nu /2 -1} e^{-x/2}
\end{align} 
$X$ is a Gamma random variable with shape parameter $\alpha$ and rate parameter $\beta$ if 
\begin{align}
p(x\given \alpha, \beta) \propto x^{\alpha-1} e^{-x \beta}
\end{align}
Thus a Chi-square random variable with $\nu$ dfs is also a Gamma random variable with shape $\alpha = \nu/2$ and rate parameter $\beta = 1/2$. The cumulative distribution function of a Gamma random variable is as follows
\begin{align}
F_X(x) \propto \gamma_l( \alpha, \beta x)
\end{align}
where $\gamma_l$ is the lower incomplete gamma function
\begin{align}
\gamma(\alpha, u) = \int_0^u s^{\alpha -1} e^{-s} ds
\end{align}
Note if $Y = cX$ where $c>0$ then
\begin{align}
F_Y(y) = P(X < y/c )  \propto \gamma_l( \alpha, \beta  y/c)
\end{align}
Thus $Y$ is  Gamma with shape $\alpha$ and rate $\beta/c$. Below are some useful statistics
\begin{align}
&E[X] = \frac{\alpha}{\beta}\\
&Var[X] = \frac{\alpha}{\beta^2}\\
&Mode[X] = \begin{cases}
&\frac{\alpha-1}{\beta},\;\text{for $\alpha \geq 1$}\\
&0,\;\text{else}
\end{cases}
\end{align}
If $X$ is Gamma with shape $\alpha$ and rate $\beta$ then $Y=X^{-1}$ is Inverse Gamma (iG) with shape $\alpha$ and scale parameter $\beta$ (or rate $\beta^{-1})$
\begin{align}
p(y) \propto  y^{-\alpha - 1} e^{-\beta/y}
\end{align}
The cumulative distribution function is as follows:
\begin{align}
F_Y(y) =  \frac{\gamma_u(\alpha, \beta / y) }{\Gamma(\alpha)} 
\end{align}
where $\gamma_u$ is the upper incomplete Gamma function 
\begin{align}
\gamma(\alpha,  u) = \int_u^\infty s^{\alpha -1} e ^{-s} ds
\end{align}
Note if $Y$ is iG with shape $\alpha$ and scale $\beta$ and if  $Z= c Y$ where   $c>0$ then
\begin{align}
F_Z(z )= P(Z< z) = P(Y < z/c)   \propto \gamma_u(\alpha,  \beta c/z )  
\end{align}
Thus $Z$ is iG with shape $\alpha$ and scale $\beta c$. Below are some useful statistics:
\begin{align}
&E[Y] = \frac{\beta}{\alpha -1},\;\text{for $\alpha>1$}\\
&Var[Y] = \frac{\beta^2}{(\alpha-1)^2 (\alpha-2)} = \frac{E^2[Y]}{\alpha-2},\;\text{for $\alpha>2$}\\
&Mode[Y]= \frac{\beta}{\alpha+1}
\end{align}
\subsection{T distribution}
The density of a student T distribution with $\nu$ degrees of freedom, location parameter $\mu$ and scale parameter $\sigma$ is as follows
\begin{align}
f(x) \propto (1+ \frac{[(x-\mu)/\sigma]^2}{\nu})^{-\frac{\nu+1}{2}}
\end{align}
and it has the following statistics
\begin{align}
&E[X] = \mu\\
&Mode[X]=\mu\\
&Median[X]=\mu\\
&Var[Y]= \frac{\nu}{\nu-2} \sigma^2,\;\text{for $\nu>2$}
\end{align}
\subsection{Log Normal Distribution}
If $X\sim N(\mu,\sigma^2$ then $Y=e^X$ is log Normal with parameters $\mu,\sigma^2$. It can be shown
\begin{align}
&E[Y] = e^{\mu + \sigma^2/2}\\
&Mode[Y]=e^{\mu-\sigma^2}\\
&Median[Y]= e^\mu\\
&Var[Y]= (e^{\sigma^2}-1)e^{2\mu+\sigma^2}
\end{align}
\paragraph{Moment Matching}
The values of $\mu,\sigma^2$ that match $E[X]$, $Var[X]$ are as follows

\begin{align}
&\sigma^2= \log(E[Y^2]/E^2[Y])\\
 &\mu = \log(E[Y]) - \sigma^2/2
 \end{align}
where $E[Y^2] = Var[Y] + E^2[Y]$. 
 \paragraph{Sum of log normals}

 Let $S = Y_1, \cdots, Y_n$ where $Y_i$ are iid log normals with parameters $\mu,\sigma^2$.  Fenton Wilkinson show that $S$ is approximately log normal with $\mu_s, \sigma^2_s$ set to match $E[S] = n E[Y]$ and $Var[S] = n Var[Y]$, See Figure~\ref{fig:sumOfLogNormals}.

\begin{figure}[bht]\label{fig:sumOfLogNormals}
\begin{center}
\includegraphics[width=3in]{figures/sumOfLogNormals}
\end{center}
  \caption{Histograms of sum of $n$ log normals $\mu=\sigma=1$ and fit using Fenton-Wilkinson moment matching method.}
 \end{figure}



\end{document}






