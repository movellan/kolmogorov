import numpy as np
from scipy.stats import gamma,invgamma
import matplotlib.pyplot as plt

def plot1(mean,var,c):
    alpha = mean**2/var +2
    beta= mean*(alpha-1)
    up = np.arange(0.006,0.015,0.001/100000.0)
    p = gamma.pdf(up, a=alpha, scale=1/beta)
    plt.subplot(2,1,2)
    plt.xlabel('Precision')
    plt.ylabel('Prob Density')
    lp=plt.plot(up,p,c)
    uv = np.arange(10,150,100/100000.0)
    v = invgamma.pdf(uv,a=alpha,scale = beta)
    plt.subplot(2,1,1)
    plt.xlabel('Variance')
    plt.ylabel('Prob Density')
    lv=plt.plot(uv, v,c)
    return lv,lp

lv1,lp1=plot1(100,10,'r')
lv2,lp2=plot1(100,100,'g')
lv3,lp3=plot1(100,1000,'b')
lv4,lp4=plot1(100,10000,'y')
plt.subplot(2,1,1)
plt.legend( (lv1[0],lv2[0],lv3[0],lv4[0]), ("m=100,var=10","m=100,var=100","m=100,var=1000","m=100,var=10000"))
plt.subplot(2,1,2)
plt.legend( (lv1[0],lv2[0],lv3[0],lv4[0]), ("m=100,var=10","m=100,var=100","m=100,var=1000","m=100,var=10000"))
plt.show()
