import numpy as np
from scipy.stats import t
from scipy.stats import norm

import matplotlib.pyplot as plt

def plot(expectedValue,variance, alpha, beta,c):
    expectedVariance = beta/(alpha-1)
    kappa = expectedVariance/ variance
    print('kappa',kappa)
    scale = np.sqrt(beta/(alpha* kappa))
    x = np.arange(70,130,1/1000.0)
    p = t.pdf(x, df = 2*alpha, loc = expectedValue, scale=scale)
    lp=plt.plot(x,p,c)
    plt.xlabel('Mean')
    plt.ylabel('Prob Density')
    lv=plt.plot(x, p,c)
    return lp

a= 1.5
b= 150
lp1=plot(100,110,a,b,'r')
lp2=plot(100,100,a*2,b*2,'g')
lp3=plot(100,100,a*4,b*4,'b')

# lp4=plot(100,110,2,2,'b')
# z =t.ppf(1-0.01,df=2000,loc=0,scale=1)
# s = (110-100)/z
x = np.arange(70,130,1/1000.0)
y = norm.pdf(x,loc=100,scale =np.sqrt(100))
lp5=plt.plot(x,y,'k--')

plt.legend( (lp1[0],lp2[0],lp3[0],lp5[0]), ("alpha=1.5,beta=150","alpha=3,beta=300","alpha=6,beta=600","Normal"))

plt.show()
