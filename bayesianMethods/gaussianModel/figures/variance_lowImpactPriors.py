import numpy as np
from scipy.stats import gamma,invgamma
import matplotlib.pyplot as plt

def plot1(alpha,beta,c):

    up = np.arange(0.00000001,0.1,0.1/100000.0)
    p = gamma.pdf(up, a=alpha, scale=1/beta)
    plt.subplot(2,1,2)
    plt.xlabel('Log Precision')
    plt.ylabel('Log Prob Density')
    lp=plt.plot(np.log(up),np.log(p),c)
    uv = np.arange(0.00000001,0.1,0.1/100000.0)
    v = invgamma.pdf(uv,a=alpha,scale = beta)
    plt.subplot(2,1,1)
    plt.xlabel('Variance')
    plt.ylabel('Prob Density')
    lv=plt.plot(np.log(uv), np.log(v),c)
    return lv,lp

lv1,lp1=plot1(1e-32,1e-32,'r')
lv2,lp2=plot1(1e-24,1e-24,'g')
lv3,lp3=plot1(1e-16,1e-16,'b')
lv4,lp4=plot1(1e-8,1e-8,'y')
plt.subplot(2,1,1)
plt.legend( (lv1[0],lv2[0],lv3[0],lv4[0]), ("alpha,beta=$10^{-8}$","alpha,beta=$10^{-16}$","alpha,beta=$10^{-24}$","alpha,beta=$10^{-32}$"))
plt.subplot(2,1,2)
plt.legend( (lv1[0],lv2[0],lv3[0],lv4[0]), ("alpha,beta=$10^{-8}$","alpha,beta=$10^{-16}$","alpha,beta=$10^{-24}$","alpha,beta=$10^{-32}$"))
plt.show()
