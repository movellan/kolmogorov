\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}

 \title{Bayes Linear Regression}
\author{ Javier R. Movellan}


\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\R}{\mathcal{R}} 





\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}



\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{lem}{Lemma}[section]
\theoremstyle{definition}
\newtheorem{rem}{Remark}[section]
\theoremstyle{definition}
\newtheorem{ex}{Example}[section]




\usepackage{verbatim}

\begin{document}
\maketitle



    
\newpage

\section{Linear Model}
Let 
\begin{align}
&Y=  b X + \sqrt{K}  W\\
&X = \mu + \sqrt{K} Z
\end{align}
where $Y\in \R^n, X\in \R^p, W \in \R^n,K \in \R$ are random variables
and $b, \in \R^{n\times p}$ are fixed matrices. We let $W$ be zero
mean Gaussian with covariance matrix $\sigma_w$, and $Z$ zero mean
Gaussian with covariance matrix $\sigma$. For fixed parameters
$\alpha,\beta\in \R$ we let
\begin{align}
p(k ) = \frac{(\beta/2)^{\alpha/2} }{\Gamma( \alpha/2)} k^{-(\alpha/2+1)} e^{- \beta/(2k)}
\end{align} 
Thus $K$ is an Inverse Gamma variable with parameters $\alpha/2,
\beta/2$ (see Appendix). For each $k$ we let the random vector $X$ be
 multivariate Gaussian with mean $\mu$ and covariance matrix $k \sigma$, i.e.,
\begin{align}
p(x \given k) = \frac{1}{(2\pi k)^{p/2}  |\sigma|^{1/2}} e^{-\frac{1}{2k} (x - \mu)' \sigma^{-1} (x-\mu)}
\end{align}
Note the distribution of $Y$ given $x,k$ is also Gaussian 
\begin{align}
p(y \given x, k) = \frac{1}{(2\pi k)^{n/2}  |\sigma_w|^{1/2}} e^{-\frac{1}{2k} (y - bx)' \sigma^{-1}_w (y- b x)}
\end{align}
Thus $k$ has an effect on the prior uncertainty about $X$ as well as the uncertainty about $Y$ given $X$. 

\subsection{Marginal Distributions}
We already know the marginal distribution of $k$. For the marginal distribution of $x$ note 
\begin{align}
p(x) =& \int p(k) p(x|k) dk\nonumber\\
=&\int
 \frac{(\beta/2)^{\alpha/2} }{\Gamma( \alpha/2)} k^{-(\alpha/2+1)} e^{- \beta/(2k)}
\frac{1}{(2\pi k)^{p/2}  |\sigma|^{1/2}} e^{-\frac{1}{2k} (x - \mu)' \sigma^{-1} (x-\mu)} \; dk
\end{align}
Taking out all the terms constant with respect to $k$ we get
\begin{align}
p(x) = 
 \frac{(\beta/2)^{\alpha/2} }{\Gamma( \alpha/2)(2\pi)^{p/2}|\sigma|^{1/2}}
\int
 k^{-(\alpha/2+p/2+1)}
 e^{-\frac{1}{2k}( \beta+ (x - \mu)' \sigma^{-1} (x-\mu))} \; dk
\end{align}
We note that the integrand looks like an Inverse Gamma distribution with parameters  
\begin{align}
&\tilde \alpha/2 = (\alpha +p)/2\\
 &\tilde \beta/2 =(\beta+ (x-\mu)' \sigma^{-1} (x-\mu))/2
\end{align}
Thus
\begin{align}
p(x) \propto \int
 k^{-(\alpha/2+p/2+1)}
 e^{-\frac{1}{2k}( \beta+ (x - \mu)' \sigma^{-1} (x-\mu))} \; dk 
=  \left(\frac{(\tilde \beta/2)^{\tilde \alpha/2} }{\Gamma(\tilde  \alpha/2)}\right)^{-1}
\end{align}
and
\begin{align}
p(x) &\propto (\beta + (x-\mu)'\sigma^{-1} (x-\mu) )^{-(\alpha+p)/2}\\
&\propto\Big( 1+ (x-\mu)'(\beta \sigma)^{-1} (x-\mu)\Big)^{-(\alpha+p)/2}
\end{align}
This is a $p$-dimensional $t$ distribution with $\alpha$ degrees of freedom.
\subsection{Statistics of the Prior Distribution}
\begin{align}
&E[X \given k] = \mu\\
&\text{Var}[X \given k] = k \sigma\\
&E[K] = \frac{\beta}{\alpha -2}\\
&\text{Mode}[K] = \frac{\beta}{\alpha +2}\\
&\text{Var}[K] = \frac{2\beta}{(\alpha -2)^2(\alpha -4)}\\
&E[X] = \int p(k) E[X|k] dk = \mu\\
&\text{Var}[X] = \int p(k) (X - E[X|k] + E[X|k] - E[X])^2 dk\nonumber \\
&\quad \quad = \int p(k) \text{Var}[ X| k] dk + \int p(k) (E[ X|k] -  E[X])^2 dl\nonumber\\
&\quad \quad = \int p(k) k \sigma dk = E[K] \sigma = \frac{\beta }{\alpha -2}\sigma
\end{align}
\subsection{Posterior Distributions}
Given a prior distribution $p(x,k)$ and an observation vector $y$, our goal is to find the posterior distribution $p(x,k \given y)$. Note
\begin{align}
p(x,&k\given y) \propto p(k) p(x\given k) p(y\given k,x)\nonumber\\
 &\propto
 k^{-(\alpha/2+1)} e^{- \beta/(2k)} \frac{1}{k^{p/2}} e^{-\frac{1}{2k} (x - \mu)'\sigma^{-1} (x-\mu)} 
\frac{1}{k^{n/2}} e^{-\frac{1}{2k} (y - bx )' \sigma_w^{-1} (y-bx)} 
\end{align}
Rearranging terms we get
\begin{align}
p(x,k\given y) &\propto
 k^{-(\alpha +p +n+2)/2} e^{- q/(2k)}  
\end{align}
where
\begin{align}
q = \beta + (x-\mu)'\sigma^{-1} (x-\mu) + (y - bx)'\sigma_w^{-1} (y-bx)
\end{align}
We will now aim to write  $q$  in the following form
\begin{align}
q = \tilde \beta + ( x- \tilde \mu)' \tilde \sigma^{-1} (x - \tilde \mu)
\end{align}
To do so we first gather all the terms quadratic on $x$
\begin{align}
x' \sigma^{-1} x + x' b' \sigma_w^{-1} b x = x' \tilde \sigma^{-1} x 
\end{align}
Thus
\begin{align}
\tilde \sigma^{-1} =  \sigma^{-1} + b'\sigma_w^{-1}  b\end{align}
Next we gather the terms linear on $x$
\begin{align}
-2 x' \sigma^{-1} \mu -2 x' b' \sigma_w^{-1} y = -2 x' \tilde \sigma^{-1} \tilde \mu
\end{align}
Thus
\begin{align}
\tilde \mu = \tilde \sigma \Big( \sigma^{-1} \mu + b' \sigma_w^{-1} y\Big)
\end{align}
We note that $\tilde \mu$ and $\tilde \sigma$ can also be expressed as follows
\begin{align}
&\tilde \mu = \mu + g (y - b \mu)\\
&\tilde \sigma = (I - g b) \sigma
\end{align}
where $g$ can be thought of as a gain term that modulates the effect of the error term $y- b\mu$ 
\begin{align}
g = \sigma b'( \sigma_w + b'\sigma b)^{-1}\\
\end{align}
Next we gather the terms constant with respect to $x$
\begin{align}
\beta + \mu' \sigma^{-1}\mu + y' \sigma_w^{-1} y =  \tilde \beta + \tilde \mu' \tilde \sigma^{-1} \tilde \mu
\end{align}
Thus
\begin{align}
\tilde \beta = \beta + \mu'\sigma^{-1} \mu + y' \sigma_w^{-1}  y - \tilde \mu '\tilde \sigma^{-1} \tilde \mu
\end{align}
Finally we let
\begin{align}
\tilde \alpha = \alpha + n
\end{align}
and note 
\begin{align}
p(x,k\given y) \propto k^{-( \tilde \alpha/2 +1 ) } e^{- \tilde \beta/(2k)}\frac{1}{k^{p/2}} e^{-\frac{1}{2k} (x - \tilde \mu)' \tilde \sigma^{-1} ( x - \tilde \mu)}
\end{align}
which we recognize as the product of an Inverse Gamma and a Gaussian distribution. 
\begin{align}
&p(k\given y) \propto k^{-(\tilde \alpha/2 +1)} e^{-\tilde \beta /(2k)}\\
&p(x \given k,y) \propto \frac{1}{k^{p/2}} e^{-\frac{1}{2k} (x - \tilde \mu)' \tilde \sigma^{-1} ( x - \tilde \mu)}
\end{align}
The proportionality constants follow
\begin{align}
&p(k\given y) = \frac{(\tilde \beta/2)^{\tilde \alpha/2}}{\Gamma(\tilde \alpha/2)} k^{-(\tilde \alpha/2 +1)} e^{-\tilde \beta /(2k)}\\
&p(x \given x,y) =  \frac{1}{(2 \pi k)^{p/2} |\tilde \sigma|^{1/2}}
 e^{-\frac{1}{2k} (x - \tilde \mu)' \tilde \sigma^{-1} ( x - \tilde \mu)}
\end{align}
\subsection{Statistics of the Posterior Distribution}
\begin{align}
&g = \sigma b'( \sigma_w + b'\sigma b)^{-1}\\
&\tilde \mu = \mu + g (y - b \mu)\\
&\tilde \sigma = (I - g b) \sigma\\
&\tilde \alpha = \alpha+n\\
&\tilde \beta = \beta + \mu' \sigma^{-1} \mu + y'\sigma_w^{-1} y - \tilde \mu \tilde \sigma^{-1} \tilde \mu\\
&E[X \given y, k] = \tilde \mu \\
&\text{Var}[X \given k] = k \tilde \sigma\\
&E[K\given y] = \frac{\tilde \beta}{\tilde \alpha -2}\\
&\text{Mode}[K \given y] = \frac{\tilde \beta}{\tilde \alpha +2}\\
&\text{Var}[K \given y] = \frac{2\tilde \beta}{(\tilde \alpha -2)^2(\tilde \alpha -4)}\\
&E[X\given y] = \tilde  \mu\\
&\text{Var}[X \given y] =  \frac{\tilde \beta }{\tilde \alpha -2} \tilde \sigma
\end{align}
\section{Appendix}

\subsection{ Interpretation if the Inverse Gamma Probability Density}
If $X$ has a Gamma $\alpha, 1/\beta$ distribution then $Y= 1/X$ has an inverse Gamma density function
\begin{align}
p(y\given \alpha, \beta) = 
\frac{\beta^\alpha}{ \Gamma(\alpha)} 
(1/y)^{(\alpha+1)} e^{-\frac{\beta}{ y}}
\end{align}
This follows from the fact that if $y= f(x)$ is a monotonic function then
\begin{align}
p_Y(y) = \frac{p_X(x)}{| f'(y)|} 
\end{align}
where $f'$ is the derivative of $f$.It can be shown that
\begin{align}
&E[Y\given \alpha, \beta] = \frac{\beta}{\alpha -1}\\
&\text{Mode}[Y \given \alpha,\beta] = \frac{\beta}{\alpha +1}\\
  &\text{Var}[Y\given \alpha,\beta]= \frac{\beta^2}{(\alpha-1)^2 (\alpha-2)}\\
&E\text[Y^{-1}\given \alpha,\beta] = \frac{\alpha}{\beta}
\end{align}

Consider the case in which we get an $n$-dimensional vector $x$ of Gaussian observations with known mean, and with an Inverse Gamma prior over the variance of the Gaussian
\begin{align}
\log p( \sigma^2, x) &=  \log p(\sigma^2) + \log p(x\given \sigma^2) = (\alpha +1) \log(\frac{1}{\sigma^2} ) - \beta\frac{1}{\sigma^2}  \nonumber\\
&+ \frac{n}2 \log(\frac{1}{\sigma^2})  - \frac{1}{2\sigma^2} s^2 +C\\
& s^2  =\sum_{i=1}^n (x_i -\mu)^2
\end{align}
where $C$ is constant with respect to $\sigma^2$. 




Taking gradients with respect to $\eta=1/\sigma^2$ and setting it to zero

\begin{align}
\nabla_{\eta} \log p( \sigma^2, x)  = (\alpha+1)\sigma^2 -\beta +\frac{n}{2}\sigma^2 -\frac{1}{2} s^2 =0
\end{align}

gives us the MAP estimate of $\sigma^2$
\begin{align}
\sigma^2 = \frac{\beta + s^2/2}{\alpha+1+ n/2}
\end{align}
We can interpret $\alpha+1$ as 1/2 the number of prior observations and $\beta$ as 1/2 the sum of squared deviations of the prior observations. From this point of view the variance of the prior observation is
\begin{align}
\sigma^2_p = \frac{\beta}{\alpha+1} 
\end{align}
which is the mode of the Inverse Gamma Distribution. Thus if we want for the prior to behave similarly to $n$ prior observations with variance $\sigma_p^2$ then we set
\begin{align}
&\alpha = \frac{n}{2} -1\\
&\beta = \sigma^2_p (\alpha+1)
\end{align}



Note the variance of the Inverse Gamma is 
\begin{align}
Var = \frac{\beta}{(\alpha-1)^2(\alpha-2)^2} \approx \frac{\beta^2}{(\alpha+1)^2} = (\sigma^2_p)^2
\end{align}
i.e., the larger the mode of the prior distribution, the larger the prior variance.   

\subsection{ Interpretation of the Gamma Probability Density}
\begin{align}
p(x\given \alpha, \beta) \propto x^{\alpha-1} e^{-x \beta}
\end{align}
Consider the case in which we get a vector of Gaussian observations with known mean, and with a Gamma prior over the precision (1/variance) of the Gaussian
\begin{align}
\log p( \eta, x) &=  \log p(\eta) + \log p(x\given \eta) = (\alpha -1) \log(\eta) -\eta \beta \nonumber\\
&+ \sum_{i=1}^n \log \Big(\frac{\eta}{2\pi} \Big)^{1/2} e^{-\frac{\eta}{2} (x_i - \mu)^2} +C
\end{align}
where $C$ is constant with respect to $\eta$. Taking the gradient with respect to $\eta$
\begin{align}
\nabla_\eta \log p(\eta,x) &= \frac{1}{\eta} (\alpha -1 )  -\beta + \frac{n}{2} \frac{1}{\eta}  - \frac{1}{2} s^2\\
s^2 &= \sum_{i=1}^n (x_i - \mu)^2
\end{align}
Setting the gradient to zero and solving for $\eta$ we get the MAP estimator of $\eta$
\begin{align}
\frac{1}{\eta} = \frac{\beta + \frac{1}{2} s^2 }{ \alpha -1 + n/2} 
\end{align}
Thus we can think of $\alpha -1 $ as $n_0/2$ one half of the number of prior observations,  and $\beta$ as $1/2 s_0^2$, one half the sum of squared deviations of the prior observations. Thus
\begin{align}
\eta_0 = \frac{\alpha-1}{\beta}
\end{align}
can be seen the precision (1/variance) of the prior observations, which is also the mode of the  prior Gamma distribution. If we want a prior distribution that behaves as $n_0$ prior observations with precision $\eta_0$ we get the parameters of the Gamma prior
\begin{align}
&\alpha = n_0/2+1\\
&\beta = \frac{\alpha-1}{\eta_0}
\end{align} 



Note the variance of the Gamma distribution is 
\begin{align}
Var = \frac{\alpha}{\beta^2} \approx \frac{\alpha-1}{\beta^2} = \frac{\eta_0}{\beta}   = 2 \frac{\eta^2_0}{n_0}
\end{align}
The fact that the Variance decreases with the number of prior observations $n_0$ makes sense, but the fact that it increases with the mode of the prior observations seems a bit counterintuitive. 

\bibliographystyle{apalike} % plainnat
\bibliography{bayesianMethods}
\end{document}
