import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import beta
from scipy.stats import binom



# Generate some x values
theta = np.linspace(0,1, 100)
#successes and trials

n =20

for u in range(0,11):
    x  =u *2 ;
    y = binom.cdf(x,n,theta)
    plt.plot(theta, y)


plt.axhline(0.05, color='green', linestyle='dashed', linewidth=1)
# L_x =  np.argmax(y2>0.05)
# print('L_x',theta[L_x])
#plt.axvline(theta[L_x],color='green', linestyle='dashed', linewidth=1)
plt.show()
