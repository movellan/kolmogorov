import numpy as np
from scipy.stats import lognorm,norm
import matplotlib.pyplot as plt
# Given a desired median nu and percentile ratio rho find
# mu and sigma parameters of log normal that satisfy
# desired values

nu = 1000
rho =1.1*100
def plotpdf(nu,rho,c):
    mu = np.log(nu)
    z1 = norm.ppf(0.995)
    sigma2 = (np.log(rho)/(2*z1))**2

    l0 = lognorm.ppf(0.005,s=np.sqrt(sigma2),scale= np.exp(mu))
    l1 = lognorm.ppf(0.995,s=np.sqrt(sigma2),scale= np.exp(mu))
    x= np.arange(l0,l1,(l1-l0)/100000.0)
    y = lognorm.pdf(x,s=np.sqrt(sigma2),scale= np.exp(mu))
    median = lognorm.ppf(0.5,s=np.sqrt(sigma2),scale= np.exp(mu))
    f=plt.plot(x,y,c)
    plt.plot((median,median),(0,lognorm.pdf(median,s=np.sqrt(sigma2),scale= np.exp(mu))),c+'--')
    plt.xlabel('X')
    plt.ylabel('Prob Density')
    return f
        #
        # print("nu",nu,"median",median)
#f1 = plotpdf(1,6,'r')
plt.subplot(2,1,1)
f2 = plotpdf(20,6,'r')
f3 = plotpdf(30,6,'g')
f4 = plotpdf(40,6,'b')
f5 = plotpdf(50,6,'c')
f6 = plotpdf(60,6,'y')
plt.legend( (f2[0],f3[0],f4[0],f5[0],f6[0]), ("nu=20$,rho=6","nu=30,rho=6","nu=40,rho=6","nu=50,rho=6","nu=60,rho=6"))

plt.subplot(2,1,2)
f2 = plotpdf(20,2,'r')
f3 = plotpdf(20,4,'g')
f4 = plotpdf(20,8,'b')
f5 = plotpdf(20,16,'c')
f6 = plotpdf(20,32,'y')
plt.legend( (f2[0],f3[0],f4[0],f5[0],f6[0]), ("nu=20$,rho=2","nu=20,rho=4","nu=20,rho=8","nu=20,rho=16","nu=20,rho=32"))
plt.xlabel('X')
plt.ylabel('Prob Density')

plt.show()
