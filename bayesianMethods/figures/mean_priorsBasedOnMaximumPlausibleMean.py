import numpy as np
from scipy.stats import t
from scipy.stats import norm

import matplotlib.pyplot as plt

def plot(mostLikelyMean,maxPlausibleMean, alpha, beta,c):
    z =t.ppf(0.99,df=2*alpha,loc=0,scale=1)
    epsilon = (maxPlausibleMean-mostLikelyMean)/z
    expectedPrecision = alpha/beta
    kappa =  1/((epsilon**2) * expectedPrecision)
    print('kappa',kappa)
    x = np.arange(70,130,1/1000.0)
    p = t.pdf(x, df = 2*alpha, loc = mostLikelyMean, scale=epsilon)
    print('cdf',t.cdf(125,df=2*alpha,loc=100,scale=epsilon))
    lp=plt.plot(x,p,c)
    plt.xlabel('Mean')
    plt.ylabel('Prob Density')
    lv=plt.plot(x, p,c)
    return lp


lp1=plot(100,125,1.5,150,'r')
lp2=plot(100,125,3,300,'g')
lp3=plot(100,125,6,600,'b')
# lp2=plot(100,110,0.5,0.5,'r')
# lp3=plot(100,110,1,1,'g')
# lp4=plot(100,110,2,2,'b')
z =t.ppf(0.99,df=2000,loc=0,scale=1)
scale = (125-100)/z
x = np.arange(70,130,1/1000.0)
#y = t.pdf(x, df = 2000, loc = 100, scale=scale)
y = norm.pdf(x,  loc = 100, scale=scale)
lp5=plt.plot(x,y,'k--')

plt.legend( (lp1[0],lp2[0],lp3[0],lp5[0]), ("alpha=1.5,beta=150","alpha=3,beta=300","alpha=6,beta=600","Normal"))


plt.show()
