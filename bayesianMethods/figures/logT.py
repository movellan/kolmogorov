import sys
sys.path.append('../python/normalGammaModel/')
import normalGammaModel as ngm
import numpy as np
import matplotlib.pyplot as plt

x = np.arange(0,4,1/10000)
plt.subplot(2,1,1)
a1=ngm.plotlogT(x,1,0,1,'r')
a2=ngm.plotlogT(x,1,1,1,'g')
a3=ngm.plotlogT(x,1,0,2,'b')
a4=ngm.plotlogT(x,1,0,0.5,'c')
a5=ngm.plotlogT(x,1,1,0.5,'tab:orange')
plt.ylim((0,0.8))
plt.ylabel('Prob Density')
plt.legend( (a1[0],a2[0],a3[0],a4[0],a5[0]), ("loc=0,sc=1","loc=1,sc=1", "loc=0,sc=2","loc=0,sc=0.5" ,"loc=1,sc=0.5"))

plt.subplot(2,1,2)
ngm.plotlogT(x,100,0,1,'r')
ngm.plotlogT(x,100,1,1,'g')
ngm.plotlogT(x,100,0,2,'b')
ngm.plotlogT(x,100,1,0.5,'c')
ngm.plotlogT(x,100,1,0.5,'tab:orange')
plt.ylim((0,1.8))
plt.xlabel('X')
plt.ylabel('Prob Density')
plt.legend( (a1[0],a2[0],a3[0],a4[0],a5[0]), ("loc=0,sc=1","loc=1,sc=1", "loc=0,sc=2","loc=0,sc=0.5" ,"loc=1,sc=0.5"))

plt.show()
#
# ngm.plotlogT(x,10,1,1,'r')
# ngm.plotlogT(x,5,1,1,'g')
# ngm.plotlogT(x,2,1,1,'b--')
# ngm.plotlogT(x,1,1,1,'c')
# ngm.plotlogT(x,0.5,1,1,'tab:orange')
# plt.ylim((0,0.8))
# plt.xlim((0,4))
# plt.show()


# n_base=4
# model1 =  NormalGammaConjugateModel(m_0=100, n_m_0=n_base, s2_0=100, n_s2_0=n_base)
# fm1,fv1=model1.displayPriorDistribution('r')
# model2 =  NormalGammaConjugateModel(m_0=100, n_m_0=n_base*10, s2_0=100, n_s2_0=n_base*10)
# fm2,fv2=model2.displayPriorDistribution('g')
# model3 =  NormalGammaConjugateModel(m_0=100, n_m_0=n_base*100, s2_0=100, n_s2_0=n_base*100)
# fm3,fv3=model3.displayPriorDistribution('b')
# plt.subplot(1,2,1)
# plt.legend( (fm1[0],fm2[0],fm3[0]), ("n=5","n=20","n=80"))
# plt.subplot(1,2,2)
# plt.legend( (fv1[0],fv2[0],fv3[0]), ("n=5","n=20","n=80"))
# plt.show()
# fx1=model1.displayPredictiveDistribution('r')
# fx2=model2.displayPredictiveDistribution('g')
# fx3=model3.displayPredictiveDistribution('b')
# plt.legend( (fx1[0],fx2[0],fx3[0]), ("n=5","n=20","n=80"))
# plt.show()
