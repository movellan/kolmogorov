import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import binom

# Given values
n = 20  # number of trials
p = 0.8  # probability of success on a single trial
alpha = 0.05  # significance level

x = np.arange(0, n+1)
#p(X >= x)
binom_cdf = binom.cdf(x, n, p)

# Find k for which to reject H0
k = np.max(np.where(binom_cdf <= alpha))

# Plotting the cumulative binomial distribution
plt.figure(figsize=(10, 5))
plt.plot(x, binom_cdf, 'bo', markersize=8)
#plt.plot(x, binom_cdf, 'b--', markersize=0.5)
plt.vlines(x, -0.05, binom_cdf, colors='b', linestyles='dashed')
plt.ylim(bottom=-0.05)

# Mark the alpha level on the graph
plt.axhline(y=alpha, color='r', linestyle='-',label=r'$\alpha$=0.05')
plt.axvline(x=k, color='purple', linestyle='--', label=f'k({p}) = {k}')
#
# # Annotate acceptance and rejection regions
# plt.text(n, alpha, f'accept H(p0) since p(x, p0) > {alpha}', horizontalalignment='right', verticalalignment='bottom')
# plt.text(k, 0, f'reject H(p0) since p(x, p0) <= {alpha}', horizontalalignment='left', verticalalignment='top')
#
# # Title and labels
plt.xlabel('x')
plt.ylabel(r'$p(X ≤ x \:|\: \theta=0.8)$')

plt.legend(loc='upper left', bbox_to_anchor=(0.6, 0.9))

plt.xticks(range(0, 21, 1))
# plt.grid(True)

# Save the plot to a file
#plt.savefig('/mnt/data/cumulative_binomial_distribution.png')
plt.show()
