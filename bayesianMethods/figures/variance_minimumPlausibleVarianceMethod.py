import numpy as np
from scipy.stats import gamma,invgamma
import matplotlib.pyplot as plt

def plot1(min_var,c):
    alpha =1
    max_precision = (1/min_var+0.0)
    beta = -np.log(0.01)* min_var
    up = np.arange(0,0.5,0.5/1000.0)
    p = gamma.pdf(up, a=alpha, scale=1/beta)
    plt.subplot(2,1,2)
    plt.xlabel('Precision')
    plt.ylabel('Prob Density')
    lp=plt.plot(up,p,c)
    max_var = 10*min_var
    scale_var = max_var-min_var
    uv = np.arange(0,50,50/1000.0)
    v = invgamma.pdf(uv,a=alpha,scale = beta)
    plt.subplot(2,1,1)
    plt.xlabel('Variance')
    plt.ylabel('Prob Density')
    lv=plt.plot(uv, v,c)
    return lv,lp

lv1,lp1=plot1(1.0,'r')
lv2,lp2=plot1(2.0,'g')
lv3,lp3=plot1(4.0,'b')
lv4,lp4=plot1(8.0,'y')
plt.subplot(2,1,1)
plt.legend( (lv1[0],lv2[0],lv3[0],lv4[0]), ("minVar=1","minVar=2","minVar=4","minVar=8") )
plt.subplot(2,1,2)
plt.legend( (lp1[0],lp2[0],lp3[0],lp4[0]), ("minVar=1","minVar=2","minVar=4","minVar=8") )
plt.show()
# def plot(n_0,c):
#     s2_0 = 10
#     alpha= n_0/2.0
#     beta=  alpha*s2_0
#     u_var = np.arange(0.00,2*s2_0,2*s2_0/1000.0)
#     v = invgamma.pdf(u_var,a=alpha,scale =beta)
#     plt.subplot(2,1,1)
#     lv=plt.plot(u_var,v,c)
#     u_prec = np.arange(0.0012,0.2,0.2/1000.0)
#     p = gamma.pdf(u_prec,a=alpha,scale = 1/beta)
#     plt.subplot(2,1,2)
#     lp = plt.plot(u_prec,p,c)
#     return lv,lp
# lv1,lp1=plot(0.2,'r')
# lv2,lp2=plot(2,'g')
# lv3,lp3=plot(20,'b')
# lv4,lp4=plot(200,'y')
# plt.subplot(2,1,1)
# plt.xlabel('Variance')
# plt.ylabel('Prob Density')
# plt.legend( (lv1[0],lv2[0],lv3[0],lv4[0]), ("n=0.2","n=2","n=20","n=200") )
# plt.subplot(2,1,2)
# plt.xlabel('Precision')
# plt.ylabel('Prob Density')
# plt.legend( (lp1[0],lp2[0],lp3[0],lp4[0]), ("n=0.2","n=2","n=20","n=200") )
#
#
# plt.show()
