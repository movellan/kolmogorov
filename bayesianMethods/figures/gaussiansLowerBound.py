import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm

# Define the parameters for the two normal distributions
mean1 = 0  # Mean for the first Gaussian
std_dev1 = 1  # Standard deviation for the first Gaussian

mean2 = 1  # Mean for the second Gaussian
std_dev2 = 1  # Standard deviation for the second Gaussian

# Generate some x values
x = np.linspace(min(mean1, mean2) - 3*max(std_dev1, std_dev2), max(mean1, mean2) + 3*max(std_dev1, std_dev2), 100)

# Calculate the probability density function (PDF) for both normal distributions
pdf1 = norm.pdf(x, mean1, std_dev1)
pdf2 = norm.pdf(x, mean2, std_dev2)

# Create a new figure with a specified figure size
plt.figure(figsize=(10,5))

# Plot the first normal distribution
plt.plot(x, pdf1, color='red')

# Plot the second normal distribution
plt.plot(x, pdf2, color='green')

# Optional: Fill the area under the curve for visual emphasis (can be adjusted or removed)
plt.fill_between(x, pdf1, where=(x > 2.0), color='red', alpha=0.5)
plt.fill_between(x, pdf2, where=(x > 2.0), color='green', alpha=0.5)

# Draw a dashed vertical line at the mean of each distribution
plt.axvline(mean1, color='red', linestyle='dashed', linewidth=1)
plt.axvline(mean2, color='green', linestyle='dashed', linewidth=1)

# Add labels, legend, and title

plt.ylabel('Probability Density', fontsize=20)


# Adjust the x-axis and y-axis limits if necessary
plt.xlim([min(x), max(x)])
plt.ylim(bottom=0)
plt.xticks([])
plt.yticks([])
plt.text(2.05, -0.01, 'x', horizontalalignment='center', verticalalignment='top', fontsize=20)
plt.text(0, -0.01, r'$\theta_1$', horizontalalignment='center', verticalalignment='top', fontsize=20)
plt.text(1, -0.01, r'$\theta_2$', horizontalalignment='center', verticalalignment='top', fontsize=20)


# Show the plot
plt.show()
