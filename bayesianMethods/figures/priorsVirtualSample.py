import sys
sys.path.append('../python/normalGammaModel/')
from  normalGammaModel import *
import numpy as np
import matplotlib.pyplot as plt




n_base=5
model1 =  NormalGammaConjugateModel(m_0=100, n_m_0=n_base, s2_0=100, n_s2_0=n_base)
fm1,fv1=model1.displayPriorDistribution('r')
model2 =  NormalGammaConjugateModel(m_0=100, n_m_0=n_base*4, s2_0=100, n_s2_0=n_base*4)
fm2,fv2=model2.displayPriorDistribution('g')
model3 =  NormalGammaConjugateModel(m_0=100, n_m_0=n_base*16, s2_0=100, n_s2_0=n_base*16)
fm3,fv3=model3.displayPriorDistribution('b')
plt.subplot(1,2,1)
plt.legend( (fm1[0],fm2[0],fm3[0]), ("n=5","n=20","n=80"))
plt.subplot(1,2,2)
plt.legend( (fv1[0],fv2[0],fv3[0]), ("n=5","n=20","n=80"))
plt.show()
fx1=model1.displayPredictiveDistribution('r')
fx2=model2.displayPredictiveDistribution('g')
fx3=model3.displayPredictiveDistribution('b')
plt.legend( (fx1[0],fx2[0],fx3[0]), ("n=5","n=20","n=80"))
plt.show()
