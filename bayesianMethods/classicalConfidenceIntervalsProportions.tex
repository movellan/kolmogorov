\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}

 \title{Classical Confidence Intervals for Proportions}
\author{ Javier R. Movellan\\2/17/2023}


\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\R}{\mathcal{R}} 





\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}



\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{lem}{Lemma}[section]
\theoremstyle{definition}
\newtheorem{rem}{Remark}[section]
\theoremstyle{definition}
\newtheorem{ex}{Example}[section]




\usepackage{verbatim}

\begin{document}
\maketitle
\section{Lower Bounds Binomial Proportion:}
Let $X$ a Binomial random variable with $n$ observations and $\mu$ the probability of success of individual observations, i.e., $E[X \given \mu] = n \mu$. 

\paragraph{Hypothesis Testing Framework:} We want to decide whether the unknown parameter $\mu$ is larger than a given threshold $\theta$. To this end we  consider two hypotheses: 
\begin{align}
&H^\theta_0: \mu = \theta\\
 &H^\theta_1: \mu > \theta
 \end{align}

Note for any observation  $x$ of $X$ if $\theta_2>\theta_1$ then $p(X\geq x \given \theta_2) > p(X \geq x \given \theta_1)$.  Thus we can  use small values  of $p(X\geq x\given \theta)$  as evidence that $\mu>\theta$ (see Figure \ref{fig:evidence}). We will use the following policy:  We choose a small value $\alpha$ and  
\begin{align}
\text{Reject} \; H^\theta_0\; \text{if}\;  p(X\geq x\given \theta) \leq \alpha
\end{align}
 We  refer to  $\gamma=1-\alpha$ as the confidence level in our decision to reject $H^\theta_0$. 

\begin{figure}[h]\label{fig:evidence}
\begin{center}
\includegraphics[width=3in]{figures/gaussian.jpg}
\end{center}
  \caption{Small values of $p(X \geq  x\given \theta_1)$ suggest that $\mu >\theta_1$}
 \end{figure}
 
 
\paragraph{Lower Bound:}
For each $x$ define the lower bound  $L(x)$  as the smallest value of $\theta$ that we can't reject given the observed data $x$
\begin{align}
L(x) = \min_\theta \{\theta \: : p(X \geq x \given \theta) \geq \alpha\}
\end{align}
Because $p(X\geq x\given \theta)$ is continuous and strictly monotonic with respect to $\theta$ it follows that
\begin{align}
p(X \geq x \given L(x))  = \alpha
\end{align}
Thus if $\theta \leq L(x)$ we reject $H^\theta_0$ and decide $\mu>\theta$, otherwise we don't reject $H^\theta_0$.  
\paragraph{Proposition:}
For all $\theta$ 
\begin{align}
p(\theta < L(X) \given \theta) \leq \alpha
\end{align}
i.e., $L(X)$ lower bounds $\theta$ with probability $1-\alpha$. 

\begin{proof}
For each $\theta$ define the threshold $k(\theta)$  as the smallest  integer so that $p(X> k(\theta) \given \theta)  \leq \alpha$ (see Figure~\ref{fig:k_lower}). 
For any integer $x< k(\alpha)$ we don't reject $H^\theta_0$ since $p(X\geq  x\given \theta) >\alpha$ and for any integer $x\geq k(\alpha)$ we reject $H^\theta_0$ since $p(X\geq x\given \theta) \leq \alpha$.
 
 
 
 


\begin{figure}[h]\label{fig:k_lower}
\begin{center}
\includegraphics[width=5in]{figures/k_lower_bound.jpg}
\end{center}
  \caption{$p(X\geq x \given \theta =0.2)$ for $n=20$. $k(0.2) =8$ is the smallest integer that allows us to reject $H_0^\theta$ and conclude that $\theta > 0.2$ . }
 \end{figure}



\begin{figure}[h]\label{fig:L_x}
\begin{center}
\includegraphics[width=5in]{figures/L_x_many.jpg}
\end{center}
  \caption{Left: $p(X\geq x \given \theta)$ as a function of $\theta$ for $x=12$, $n=20$.   $L(x)$ is the value of $\theta$ for which $p(X\geq x \given \theta)=\alpha$.   Right: value of  $p(X\geq x \given \theta)$ as a function of $\theta$ for $n=20$ and different values of $x$. }
 \end{figure}
 As illustrated in Figure \ref{fig:L_x}, Left, we note that for all $x$
\begin{align}
\theta < L(x) \iff  x\geq k(\theta) 
\end{align}
 Thus
\begin{align}
p(\theta < L(X)  \given \theta) = p( X \geq k(\theta) \given \theta  ) \leq \alpha
\end{align}
We can find $L(x)$ using numerical  methods. We start with $\theta=0$ and increase its value until $p(X \geq x\given \theta) \geq \alpha$. 
\end{proof}
\paragraph{Lower Bound for No Success:} Note $p(X\geq 0 \given \theta) =1$. Thus $L(0) =0$ the smallest value of $\theta$ for which $P(X\geq 0 \given \theta) \geq \alpha$. 
\paragraph{Clopper-Pearson Theorem}
The Clopper-Pearson Theorem says that for any $x>0$ we can find $L(x)$ analytically using a Beta distribution with parameters $a=x$ and $b= n-x+1$. 

\begin{align}
L(x) = F^{-1} (\alpha, a=x,b=n-x+1)
\end{align}
where $F^{-1}$ is the point percent function, aka inverse of the cumulative,  for the Beta distribution.
From a Bayesian point this is equivalent to using a Beta prior with parameters $a=0$, $b=1$. Thus the classical approach approach with $n$ observations and $x$ successes provides the same confidence intervals  as the Bayesian approach with uniform priors, $n$ observations and $x-1$ successes. 
\paragraph{Comment:} The approach we show guarantees that for all $\theta$ $p(\theta <L(X) \given \theta) < \alpha$. However there may be other approaches that provide tighter bounds with the same guarantees or approaches that provide other types of guarantees. 

\paragraph{No failures case: The Rule of 3.}
If $x=n$ then the Beta distribution has parameters $a=n$, $b=1$ and the cumulative distribution looks as follows:
\begin{align}
F(\theta, a=n, b=1) = \theta^n
\end{align}
To find $L(x)$ we get value of $\theta$ such that
\begin{align}
&\alpha = \theta^n\\
&\theta =  \alpha^{1/n} = \alpha^u
\end{align}
where $u = 1/n$.  In the limit as   $n\to \infty$, $u\to 0$
\begin{align}
\theta \approx 1 + \frac{\log \alpha}{n} 
\end{align}
where we used the fact that $d\alpha^u/du = u \log(\alpha)$.  And for $\alpha=0.05$, $\log \alpha = -2.9957 \approx -3$. Thus
\begin{align}
L(x) \approx 1 - \frac{3}{n}
\end{align}
which is  known as "The Rule of Three". 
\subsection{Example}
Table \ref{tab:lowerBounds} shows the 0.95 confidence lower bounds for several values of $n$ and $x$. We also show the Bayesian lower bounds using a uniform prior, $a=1,b=1$, and a Jeffrey prior, $a=1/2,b=1/2$.



\begin{table}\label{tab:lowerBounds}
\centering
\begin{tabular}{|c|c|c|c|c|}
\hline 
  x & n &Classical &Uniform &Jeffrey \\ \hline 
 0&10 &0 &0.04&0.02\\ \hline 
1 &10 &0.051&0.033 &0.018 \\ \hline 
5&10 &0.222&0.271&0.262\\ \hline
1&1 &0.05&0.224&0.228\\ \hline
2&2 &0.224&0.368&0.431\\ \hline
4&4 &0.472&0.549&0.637\\ \hline
6&6 &0.607&0.652&0.734\\ \hline
8&8 &0.688&0.717&0.792\\ \hline
10&10 &0.741&0.761&0.829\\ \hline
20&20 &0.86&0.867&0.91\\ \hline
50&50 &0.942&0.943&0.963\\ \hline
100&100 &0.97 &0.97&0.98\\\hline
1000&1000 &0.997 &0.997&0.998\\\hline
10000&10000 &0.9997 &0.9997 &0.9998\\\hline
\end{tabular}
 \caption{Lower Bounds for different values of $x$ and $n$}
\end{table}

\section{Upper Bounds Binomial proportion}


We want to decide whether the unknown parameter $\mu$ is smaller than a given threshold $\theta$. To this end we  consider two hypotheses: 
\begin{align}
&H^\theta_0: \mu = \theta\\
 &H^\theta_1: \mu < \theta
 \end{align}

Note for any observation  $x$ of $X$ if $\theta_2>\theta_1$ then $p(X\leq x \given \theta_2) < p(X \leq x \given \theta_1)$.  Thus we can  use small values  of $p(X \leq x\given \theta)$  as evidence that $\mu<\theta$ (see Figure \ref{fig:evidenceUpper}). We will use the following policy:  We choose a small value $\alpha$ and  
\begin{align}
\text{Reject} \; H^\theta_0\; \text{if}\;  p(X\leq x\given \theta) \leq \alpha
\end{align}
 We  refer to  $\gamma=1-\alpha$ as the confidence level in our decision to reject $H^\theta_0$. 



\begin{figure}[h]\label{fig:evidenceUpper}
\begin{center}
\includegraphics[width=3in]{figures/gaussiansUpperBound.jpg}
\end{center}
  \caption{Small values of $p(X  \leq  x\given \theta_2)$ suggest that $\mu <\theta_2$}
 \end{figure}
 
 

 
 
 
\paragraph{Upper  Bound:}
For each $x$ define the upper bound  $U(x)$  as the largest value of $\theta$ that we can't reject given the observed data $x$
\begin{align}
U(x) = \max_\theta \{\theta \: : p(X \leq x \given \theta) \geq \alpha\}
\end{align}
Because $p(X\leq x\given \theta)$ is continuous and  strictly monotonic with respect to $\theta$ it follows that
\begin{align}
p(X \leq x \given U(x))  = \alpha
\end{align}
Thus if $\theta \geq U(x)$ we reject $H^\theta_0$ and decide $\mu< \theta$, otherwise we don't reject $H^\theta_0$.  
\paragraph{Proposition:}
For all $\theta$ 
\begin{align}
p(\theta < U(X) \given \theta) \leq \alpha
\end{align}
i.e., $U(X)$ upper bounds $\theta$ with probability $1-\alpha$. 
\begin{proof}
For each $\theta$ define the threshold $k(\theta)$  as the largest  integer so that $p(X \leq  k(\theta) \given \theta)  \leq \alpha$ (see Figure~\ref{fig:k_upper}). 
For any integer $x> k(\alpha)$ we don't reject $H^\theta_0$ since $p(X\geq  x\given \theta) >\alpha$ and for any integer $x\leq k(\alpha)$ we reject $H^\theta_0$ since $p(X\geq x\given \theta) \leq \alpha$.
 
 
 

 
\begin{figure}[h]\label{fig:k_upper}
\begin{center}
\includegraphics[width=5in]{figures/k_upper_bound.jpg}
\end{center}
  \caption{$p(X \leq x \given \theta =0.8)$ for $n=20$. $k(0.8) =12$ is the largest integer that allows us to reject $H_0^\theta$ and conclude that $\theta < 0.8$ . }
 \end{figure}


 
 





\begin{figure}[h]\label{fig:U_x}
\begin{center}
\includegraphics[width=5in]{figures/U_x_many.jpg}
\end{center}
  \caption{Left: $p(X \leq x \given \theta)$ as a function of $\theta$ for $x=8$, $n=20$.   $U(x)$ is the value of $\theta$ for which $p(X\leq x \given \theta)=\alpha$.   Right: value of  $p(X\leq x \given \theta)$ as a function of $\theta$ for $n=20$ and different values of $x$. }
 \end{figure}


 
 
 As illustrated in Figure \ref{fig:L_x}, Left, we note that for all $x$
\begin{align}
\theta > U(x) \iff  x \leq k(\theta) 
\end{align}
 Thus
\begin{align}
p(\theta >   U(X)  \given \theta) = p( X \leq   k(\theta) \given \theta  ) \leq \alpha
\end{align}

We can find $U(x)$ using numerical  methods. We start with $\theta=1$ and decrease its value until $p(X \leq x\given \theta) \leq \alpha$. 


\paragraph{Upper Bound No Failures: } Note $p(X\leq 1 \given \theta) =1$.  Thus $U(n) =1$ the largest value of $\theta$ for which $P(X\leq n \given  \theta) \geq \alpha$. 
\end{proof}


\paragraph{Clopper-Pearson Theorem}
The Clopper-Pearson Theorem says that for any $x<n$ we can find $U(x)$ analytically using a Beta distribution with parameters $a=x+1$ and $b= n-x$. 

\begin{align}
U(x) = F^{-1} (1-\alpha, a=x+1,b=n-x)
\end{align}
where $F^{-1}$ is the point percent function, aka inverse of the cumulative,  for the Beta distribution.
From a Bayesian point this is equivalent to using a Beta prior with parameters $a=1$, $b=0$. Moreovcer the classical approach approach with $n$ observations and $x$ successes provides the same confidence intervals  as the Bayesian approach with uniform priors and $x$ successes in $n-1$ observations.
\paragraph{Comment:} The approach we show guarantees that for all $\theta$ $p(\theta > U(X) \given \theta) < \alpha$. However there may be other approaches that provide tighter bounds with the same guarantees or approaches that provide other types of guarantees. 

\paragraph{No success case: The Rule of 3.}
If $x=0$ then the Beta distribution has parameters $a=1$, $b=n$ and the cumulative distribution looks as follows:
\begin{align}
F(\theta, a=n, b=1) = 1 -  (1-\theta)^{n}
\end{align}
To find $U(x)$ we get value of $\theta$ such that
\begin{align}
&1-\alpha = 1- (1-\theta)^{n}\\
&\theta = 1- \alpha^{1/n}  = 1- \alpha^u\\
\end{align}
where $u = 1/n$.  In the limit as   $n\to \infty$, $u\to 0$
\begin{align}
\alpha^ u  \approx 1   +  u \log(\alpha)
\end{align}
where we used he fact that $d \alpha^u/du = u \log (\alpha)$. Thus
\begin{align}
U(x) =  1- \alpha^u \approx - u \log(\alpha) =- \frac{\log(\alpha)}{n}
\end{align}
For $\alpha=0.05$, $\log \alpha = -2.9957 \approx -3$. Thus
\begin{align}
 U(x)\approx  \frac{3}{n}
\end{align}
which is  known as "The Rule of Three". 
\subsection{Example}
Table \ref{tab:upperBounds} shows the 0.95 confidence lower bounds for several values of $n$ and $x$. We also show the Bayesian lower bounds using a uniform prior, $a=1,b=1$, and a Jeffrey prior, $a=1/2,b=1/2$.



\begin{table}\label{tab:upperBounds}
\centering
\begin{tabular}{|c|c|c|c|c|}
\hline 
  x & n & Classical &Uniform &Jeffrey \\ \hline 
 0&10 &0.259 &0.238&0.171\\ \hline 
1 &10 &0.394&0.364 &0.331 \\ \hline 
5&10 &0.778&0.729&0.738\\ \hline
10&10 &1&0.995&0.999\\ \hline
0&1 &0.95&0.776&0.771\\ \hline
0&4 &0.527&0.451&0.363\\ \hline
0&8 &0.312&0.283&0.207\\ \hline
0&10 &0.259&0.238&0.171\\ \hline
0&20 &0.139&0.133&0.09\\ \hline
0&50 &0.058&0.057&0.038\\ \hline
0&100 &0.029 &0.029&0.019\\\hline
0&1000 &0.003 &0.003&0.002\\\hline
0&10000 &0.0003 &0.0003&0.0002\\\hline
\end{tabular}
 \caption{Upper Bounds for different values of $x$ and $n$}
\end{table}




 \section{Confidence Intervals}
If $L(x)$ lower bounds $\theta$ with confidence $1-\alpha$ and $U(x)$ upper bounds it with confidence $1-\beta$ then

\begin{align}
p(L(x) < \theta < U(x) \given \theta) = 1 - p(L(x) \geq \theta\given \theta) - p(U(x) \leq \theta\given \theta)= 1-\alpha -\beta
\end{align}
We say that $[L(x), U(x)]$ is a $1-\alpha - \beta$ confidence interval for $\theta$. Note there are many values of $\alpha$, $\beta$ with equal confidence value. Two popular options are when $\alpha=\beta$, known as equal tailed intervals, or when $\alpha,\beta$ are chosen numerically to minimize the length of the interval, known as the Highest Likelihood Interval interval (HLD). 

	
\section{Quick and Dirty Power Analysis}
We want the sample size needed to get confidence intervals with a given width $w$, e.g. we want the approximation to be $\hat p \pm w/2$.

We approximate the binomial with a Gaussian distribution with mean $\theta$ and variance $\\theta (1-\theta)/n$. Under this approximation

\begin{align}
w = 2 z \sqrt{\theta(1-\theta)/n} \leq 2 z \sqrt{0.5 ^2/n} = z /\sqrt{n}
\end{align}
where $z = \Phi^{-1}(\alpha)$, the inverse standard Gaussian cumulative function evaluated at $\alpha = (1-c)/2$ where $c$ is the confidence level. For a 95\% confindence level, $\alpha = 0.025$ and $z=1.96$ Thus
\begin{align}
w \leq 1.96  /\sqrt{n} < 2/\sqrt{n}
\end{align}
and 
\begin{align}
n \leq 4/w^2
\end{align}
providing an upper bound on the sample size needed to get a width $w$. For example if we want a width of $0.1$, i.e., confidence intervals with $\pm 0.05$ then	 
\begin{align}
n \leq 4/0.01 = 400
\end{align}
a sample size of 400 observations is sufficient to obtain the desired width. 


\end{document}




\begin{figure}[h]\label{fig:U_x}
\begin{center}
\includegraphics[width=5in]{figures/U_x_many.jpg}
\end{center}
  \caption{Left: $p(X \leq x \given \theta)$ as a function of $\theta$ for $x=8$, $n=20$.   $L(x)$ is the value of $\theta$ for which $p(X\leq x \given \theta)=\alpha$.   Right: value of  $p(X\leq x \given \theta)$ as a function of $\theta$ for $n=20$ and different values of $x$. }
 \end{figure}

We can get the upper bound $U(x)$ of $\theta$ as one minus the lower bound of $1-\theta$, i.e.,

\begin{align}
U(x) = 1- F^{-1}(\alpha, n-x, x+1) = F^(1-\alpha, x+1,n-1)
\end{align}

\paragraph{The Zero Error, or Zero success cases}

