import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import binom
from scipy.stats import beta

# Classical and Bayesian coverage guarantees
# We focus on Lower Bound Coverage

def hinge(x):
    return np.maximum(0, x)



n =300 # number of observations
alpha = 0.11 # confidence level


# a and b parameter of Prior for
# Bayesian approach
#maximum entropy for hypothesis that theta =1  1/1million
prior_a =  693056
prior_b =  1
# a and b parameter of Prior for
# Bayesian approach
#maximum entropy for hypothesis that theta = 0.99

prior_a =  68.96
prior_b =  1
mu = 0.99

# a and b parameter of Prior for
# Bayesian approach
#maximum entropy for hypothesis that theta = 0.999

# prior_a =  692.80
# prior_b =  1

#maximum entropy for hypothesis that theta = 0.999

# prior_a =  3.1
# prior_b =  1
# mu = 0.8 # the hypothesis we care about
#


eps = 0.05

lb = beta.ppf(eps/2,prior_a,prior_b)
ub = beta.ppf(1-eps/2,prior_a,prior_b)
# theta, the true Probability
thetaList = np.linspace(lb,ub,1000)


x = np.arange(0, n+1)
coverageList_c=[]
coverageList_b=[]
mu_coverageList_b=[]
mu_coverageList_c=[]
lowerBounds_c =[]
lowerBounds_b =[]
riskList_c=[]
riskList_b=[]
prior_p = beta.pdf(thetaList,prior_a,prior_b)
prior_p = prior_p/np.sum(prior_p)
for theta in thetaList:
    p_x = binom.pmf(x,n,theta)
    L_c = beta.ppf(alpha,x,n-x+1)
    L_c[0] =0
    loss_c = hinge(L_c-theta)
    risk_c = np.sum(p_x*loss_c)
    riskList_c.append(risk_c)
    L_b = beta.ppf(alpha,x+prior_a,n-x+prior_b)
    loss_b = hinge(L_b-theta)
    risk_b = np.sum(p_x*loss_b)
    riskList_b.append(risk_b)

    mean_threshold_c = np.sum(p_x*L_c)
    mean_threshold_b = np.sum(p_x*L_b)
    lowerBounds_c.append(mean_threshold_c)
    lowerBounds_b.append(mean_threshold_b)
    if L_c[-1] < theta:
        coverage_c =0
    else:
        u = np.min(np.where(L_c >= theta))
        # 1- p(X >= u)
        coverage_c = 1- binom.cdf(u-1, n, theta)

    if L_b[-1] < theta:
        coverage_b =0
    else:
        u2 = np.min(np.where(L_b >= theta))
        # 1- p(X >= u)
        coverage_b = 1- binom.cdf(u2-1, n, theta)
        mu_coverage_b = 1- binom.cdf(u2-1, n, theta)

    if L_c[-1] < mu:
        mu_coverage_c=0
    else:
        u = np.min(np.where(L_c >= mu))
        mu_coverage_c = 1- binom.cdf(u-1, n, theta)

    if L_b[-1] < mu:
        mu_coverage_b=0
    else:
        u = np.min(np.where(L_b >= mu))
        mu_coverage_b = 1- binom.cdf(u-1, n, theta)




    coverageList_c.append(coverage_c)
    coverageList_b.append(coverage_b)
    mu_coverageList_b.append(mu_coverage_b)
    mu_coverageList_c.append(mu_coverage_c)
    print('theta',theta)

mean_coverage_c= np.sum(prior_p*coverageList_c)
mean_upper_bound_c = np.sum(prior_p*lowerBounds_c)
print('Classical mean_coverage',mean_coverage_c, 'Mean Lower Bound',mean_upper_bound_c)

prior = beta.pdf(thetaList,prior_a,prior_b)
prior = prior/max(prior)


mean_coverage_b= np.sum(prior_p*coverageList_b)
mean_upper_bound_b = np.sum(prior_p*lowerBounds_b)
print('Bayesian mean_coverage',mean_coverage_b, 'Mean Lower Bound',mean_upper_bound_b)

plt.plot(thetaList,coverageList_c,label='Coverage Classical')
plt.plot(thetaList,coverageList_b,'r',label='Coverage Bayesian')
plt.plot(thetaList,prior,'b--',label='Prior')
plt.axhline(y=alpha,color='r',linestyle='--',label='Confidence Level')
plt.xlabel(r'$\theta$')
plt.ylabel('1-Coverage')
plt.legend()

plt.axvline(x=lb, color='b', linestyle='--')
plt.axvline(x=ub, color='b', linestyle='--')
plt.axhline(y=alpha,color='r',linestyle='--')
plt.axvline(x=mu, color='g', linestyle='--',label=r'$\mu$')

plt.figure()
plt.plot(thetaList,lowerBounds_b,'r',label='Bayesian')
plt.plot(thetaList,lowerBounds_c,'b',label='Classical')
plt.plot(thetaList,thetaList,'k')
plt.plot(thetaList,prior,'b--',label='Prior')

plt.axvline(x=lb, color='b', linestyle='--')
plt.axvline(x=ub, color='b', linestyle='--')
plt.axvline(x=mu, color='g', linestyle='--',label=r'$\mu$')
plt.xlabel(r'$\theta$')
plt.ylabel(r'$E[L\:|\: \theta]$')
plt.legend()
plt.show()

plt.figure()

# The risk is the expected error of the lower bound
# If Lower bound > theta the error is Lower Bound - 0, otherwise 0
# If risk is 0.05 it means on average the lower bound will be 0.05 larger than
# true theta
plt.plot(thetaList,riskList_c,'b',label='Risk Classical')
plt.plot(thetaList,riskList_b,'r',label='Risk Bayesian')
plt.plot(thetaList,prior*max(riskList_b),'b--',label='Prior')
plt.axvline(x=mu, color='g', linestyle='--',label=r'$\mu$')
plt.xlabel(r'$\theta$')
plt.legend()
plt.show()

plt.figure()
plt.plot(thetaList,mu_coverageList_b,'r',label='Bayes')
plt.plot(thetaList,mu_coverageList_c,'b--',label='Classical')
plt.axvline(x=mu, color='g', linestyle='--',label=r'$\mu$')
plt.axhline(y=alpha, color='k', linestyle='--',label=r'$\alpha$')
plt.xlabel(r'$\theta$')
plt.ylabel(r'$p(\mu < L\:|\: \theta)$')
plt.legend()
plt.show()
