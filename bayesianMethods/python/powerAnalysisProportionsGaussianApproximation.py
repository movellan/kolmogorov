import numpy as np
from scipy.stats import norm
from matplotlib import pyplot as plt
# Power analysis for Proportions
# Clqssicl approach, Gaussian approximation
# three methods:
# decision making
# asymetric confidence intervals
# symetric confidence intervals

# If we test two hypotheses:
# H1: mu =  mu_1
# H2: mu = mu_2
# with known variance sigma2 under both hypoheses
# We let theta = (mu_1+mu_2)/2
# and decide H1 if Xbar < theta, H2 if Xbar > theta
# where Xbar is sample mean
# We want
# p(Xbar > theta | H1) = alpha, type I Error
# p(Xbar < theta | H2) = beta, type II Error
# then n = (2 Z_{1-alpha} /(mu_2 - mu_1) sigma)^2
# This n is also the sample isze required so  that total width
# for confidence interval with level 1 - 2 alpha is mu2- mu1


def posteriorcdf(u,x,sigma):
    #p(mu<u \given x)
    #p(mu|x) = p(x|mu)=g(mu|x,sigma^2_n)
    p = norm.cdf((u-x)/sigma)
    return(p)


# Power analysis based on precision of confidence interval
# alpha is the proportion of left side misses,
# i.e, true value is smaller than lower bound of conf interval
# beta is the proportion of right side misses
# i.e., true value is larger than upper bound of conf interval
# sigma2 expected variance
# delta is the desired width of the confidence interval
def get_n_asymetric_conf_int(sigma2,delta,alpha,beta):
    z_1  = norm.ppf(1-alpha)
    z_2  = norm.ppf(1-beta)
    n = sigma2*((z_1+z_2)/delta)**2
    return(n)
# Power analysis based on precision of confidence interval
# mu is the expected percentage
# delta is the desired width of the confidence interval
# confidence is the confidence level, from 0 to 1
def get_n_symetric_conf_int(sigma2,delta,confidence=0.95):
    alpha = (1-confidence)/2
    #import ipdb; ipdb.set_trace()
    n = get_n_asymetric_conf_int(sigma2,delta,alpha,alpha)
    return(n)

# Power analysis based on decision theory
# we want to mistakenly accept percentages smaller or equal
# to mu_1 with probability alpha
# we want to mistakenly reject percentages larger or equal
# to mu_2 with probability beta
def get_n_theta_decision_theory(mu_1,mu_2,sigma2,alpha,beta):
    z_1  = norm.ppf(1-alpha)
    z_2 = norm.ppf(1-beta)
    n = sigma2*((z_1 + z_1)/(mu_2-mu_1))**2
    sigma= np.sqrt(sigma2/n)
    theta = mu_1  + z_1 * sigma
    return(n,theta)

def get_beta(mu_1,mu_2,sigma,n,alpha):
    z_1  = norm.ppf(alpha)
    sigma_n = sigma/np.sqrt(n)
    theta = mu_1 - z_1*sigma_n
    z_2 = (mu_2 -theta)/sigma_n
    beta = 1-norm.cdf(z_2)
    return(beta,theta)



# Percentages smaller than mu are unnaceptable
def plotRegions(mu_1,mu_2,sigma,n,theta,legend,x_min,x_max):
    sigma_n = sigma/np.sqrt(n)
    x = np.arange(x_min,x_max,(x_max-x_min)/1000)
    y = norm.cdf((x-theta)/sigma_n)
    plt.plot(x,y,'k')
    plt.plot((mu_1,mu_1),(0,1),'r--')
    plt.plot((mu_2,mu_2),(0,1),'r--')
    label = 'Prob Accept mu > '+str(mu_2)
    plt.ylabel(label)
    plt.xlabel('True Percentage (mu)')
    #plt.text(70,0.6,'Unacceptable Region')
    # plt.text(80.5,0.6,'Uncertain Region')
    # plt.text(94.5,0.6,'Acceptable Region')
    plt.text(82,0.85,'n='+str(int(np.ceil(n))))
    plt.text(82,0.2,legend)




print('Power Analysis Decision Theory')
# upper limit of the unnaceptable region
mu_1= 0.85
# lower limit of the unnaceptable region
mu_2 = 0.95
# type I error
alpha = 0.01
# type II error
beta = 0.01
#expected proportion
mu = 0.8

sigma2 = mu*(1-mu)
n,theta = get_n_theta_decision_theory(mu_1,mu_2,sigma2,alpha,beta)
print('mu_1=',mu_1, 'mu_2=',mu_2)
print('alpha=',alpha,'beta=',beta)
print('n=',n)
print('theta=',theta)
plt.subplot(2,1,1)
xmin = max(0,mu_1- 10)
xmax = min(100,mu_2+ 10)
