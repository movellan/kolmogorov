from scipy.stats import binom, beta, norm
import numpy as np
import matplotlib.pyplot as plt

# We ground the problem on a specific scenario: deciding whether a product’s
# failure rate is acceptable. To this end, we get a sample,
# compute the empirical failure rate in this sample, and decide whether:
# (1) the product is acceptable.
# (2) the product is not acceptable.
# (3) more data or careful investigation is needed.
# Here we implement algorithms for deciding the sample size required for
# our experiments.
# Notation:
# • mu_1 ∈ [0, 1] Ideal failure rate. Products with failure rates smaller than μ1
#   are clearly acceptable.
# • mu_2 ∈ [0, 1] Maximum acceptable failure rate.
#   Products with failure rates larger than μ2 are not acceptable.
#   Products with failure rates between μ1 and μ2 require careful analysis.
# • theta_1 < theta_2 ∈ [0, 1]. Range of plausible failure rates.
# • kappa ∈ [0, 1] Confidence level for the confidence interval calculations.
# • delta ∈ [0, 1] The required maximal total width of the confidence intervals.
# • eta ∈ [0, 1] Proportion of confidence intervals required to be acceptable.
# A confidence interval is acceptable if its confidence level is smaller
# or equal to kappa and at least one of the following three conditions holds:
# (1) The upper limit of the interval is smaller than mu_1.
# (2) The lower limit of the confidence interval is larger than mu_2.
# (3) The width of the confidence interval is smaller than delta.

def powerAnalysisGaussianApprox(muhat, delta, kappa=0.95):
    """
    Perform power analysis of proportion using Gaussian approximation.

    Parameters:
    muhat (float): Expected proportion used to determine variance.
    delta (float): Desired full width of confidence interval.
    kappa (float): Confidence level (0 to 1).

    Returns:
    int: Required sample size.
    """
    z = norm.ppf(1 - (1 - kappa) / 2)
    sigma2 = muhat * (1 - muhat)
    n = sigma2 * (2 * z / delta) ** 2
    return int(np.ceil(n))

def clopperPearsonConfInterval(x, n, kappa=0.95):
    """
    Compute the Clopper-Pearson confidence interval.

    Parameters:
    x (int or array-like): Number of observed failures.
    n (int): Sample size.
    kappa (float): Confidence level (0 to 1).

    Returns:
    tuple: Lower and upper bounds of the confidence interval.
    """
    x = np.asarray(x)
    lb = beta.ppf((1 - kappa) / 2, x, n - x + 1)
    lb[np.isnan(lb)] = 0
    ub = beta.ppf(1 - (1 - kappa) / 2, x + 1, n - x)
    ub[np.isnan(ub)] = 1
    return lb, ub

def gaussianConfInterval(x, n, kappa=0.95):
    """
    Compute the Gaussian confidence interval.

    Parameters:
    x (int or array-like): Number of observed failures.
    n (int): Sample size.
    kappa (float): Confidence level (0 to 1).

    Returns:
    tuple: Lower and upper bounds of the confidence interval.
    """
    z = norm.ppf(1 - (1 - kappa) / 2)
    x = np.asarray(x)
    p = x / n
    sigma = np.sqrt(p * (1 - p) / n)
    lb = p - sigma * z
    ub = p + sigma * z
    return lb, ub

def probAcceptableConfidenceInterval(n, mu_1, mu_2, theta_1, theta_2, desired_interval_width, kappa=0.95):
    """
    Calculate the probability of having an acceptable confidence interval.

    Parameters:
    n (int): Sample size.
    mu_1 (float): Ideal failure rate.
    mu_2 (float): Maximum acceptable failure rate.
    theta_1 (float): Minimum plausible error rate.
    theta_2 (float): Maximum plausible error rate.
    desired_interval_width (float): Desired maximum width of the confidence interval.
    kappa (float): Confidence level (0 to 1).

    Returns:
    tuple: Lists of mu values and corresponding probabilities.
    """
    pList = []
    muList = []

    x = np.arange(0, n + 1)
    lb, ub = clopperPearsonConfInterval(x, n, kappa)
    delta = ub - lb

    acceptable_conf_int = delta <= desired_interval_width
    acceptable_conf_int |= (ub < mu_1)
    acceptable_conf_int |= (lb > mu_2)

    for mu_hat in np.linspace(theta_1, theta_2, 1000):
        px = binom.pmf(x, n, mu_hat)
        p_success = np.sum(px[acceptable_conf_int]) / np.sum(px)
        pList.append(p_success)
        muList.append(mu_hat)

    return muList, pList

def find_smallest_true_index(lists):
    """
    Find the index of the first True value in each column of a 2D list.

    Parameters:
    lists (list of lists): 2D list of boolean values.

    Returns:
    np.array: Indices of the first True value in each column.
    """
    array = np.array(lists)
    result = np.argmax(array, axis=0)
    mask = np.any(array, axis=0)
    result[~mask] = -1  # or any other value to indicate no True value found
    return result

# Parameters
kappa = 0.9  # Confidence level
mu_1 = 0.01  # Desired failure rate
mu_2 = 0.02  # Maximum acceptable failure rate
theta_1 = 0.0  # Minimum plausible error rate
theta_2 = 1.0  # Maximum plausible error rate
desired_interval_width = mu_2 - mu_1
eta = 0.95  # Required proportion of acceptable confidence intervals

print('Desired Conf Interval Width:', desired_interval_width)

# Largest sigma of [mu_1, mu_2] region
mu_hat = mu_1 if mu_1 >= 0.5 else mu_2

n_gaussian = powerAnalysisGaussianApprox(mu_hat, desired_interval_width, kappa=kappa)

n_min = int(n_gaussian / 10)
n_max = int(n_gaussian * 2)
delta_n = int((n_max - n_min) / 10)
pMatrix = []
nList = []

for n in range(n_min, n_max, delta_n):
    nList.append(n)
    print('n =', n, 'nmax =', n_max)
    muList, pList = probAcceptableConfidenceInterval(n, mu_1, mu_2, theta_1, theta_2, desired_interval_width, kappa=kappa)
    pMatrix.append(pList)

u = np.array(pMatrix)

v1 = u > eta

z1 = find_smallest_true_index(v1)

nList = np.array(nList)
power_n1 = nList[z1]
muList = np.array(muList)

plt.plot(muList, power_n1)
plt.xlabel('Mu')
plt.ylabel('Sample Size')
plt.title('Sample Size vs. Mu')
plt.show()
