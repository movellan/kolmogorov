from scipy.stats import beta
import numpy as np


prior= 1 # uniform prior
prior = 0.333 # neutral prior (Kerman)
prior=  0.5 # Jeffrey prior

def betaDifferences(d,a1,b1,a2,b2,dx=0.001):
    """ Probability density of difference
    D = Y - X
    where Y,X are Beta random variaables
    d in [-1,1]. the value whose density is evaluated
    a1,b1  parameters for distribution of X
    a2, b2 parameters for distribution of Y
    dx: precission for numerical integration
    returns probability density evaluated at d
    """
    if np.abs(d)>1:
        print 'd is not in [-1,1]'
        return
    x = np.arange(0,1,dx)
    y = x+d
    px = beta.pdf(x,a1,b1)
    py = beta.pdf(y, a2,b2)
    py[y>1]=0
    py[y<0]=0
    pd = sum(px*py)*dx
    return pd


def confIntervalPercentage(percent,n,confidence=0.99):
    """
    Bayesian central confidence intervals for percentages
    p percentage, between 0 and 100
    n number of observations in p
    confidence: confidence value.
    e.g, 0.99 for 99 percent confidence interval
    returns lower and upper limits of the interval
    """
    p = percent/100.0
    a = p*n +prior
    b = (1-p)*n +prior
    l0 = 100*beta.ppf((1-confidence)/2.0,a,b)
    l1 = 100*beta.ppf(1-(1-confidence)/2.0,a,b)
    return np.round([l0,l1],1)

def confIntervalDiffOfPercentages(percent1,n1,percent2,n2,confidence=0.99,dx=0.001):
    """
    Bayesian Central Confidence Interval (aka Credible Interval)
    for the difference between two percentages P2 - P1
    p1 first percentage (between 0 and 1)
    n1 number of observations in p1
    p2 second percentage
    n2 number of observations in p2
    confidence: confidence value
    e.g., confidence=0.99 for 99 percent  confidence interval
    dx precision for numerical integration.
    return lowerLimit and upperLimit of the confidence interval
    """

    p1 = percent1/100.0
    p2 = percent2/100.0
    a1 = p1*n1 + prior
    b1 = (1-p1)*n1 + prior
    a2 = p2*n2 + prior
    b2 = (1-p2)*n2 + prior
    d = np.arange(-1,1,dx)
    P=0
    limit = (1-confidence)/2.0
    k=0
    while P< limit:
        P = P+ betaDifferences(d[k],a1,b1,a2,b2)
        k=k+1
    lowerLimit= 100*d[k]
    Q=0
    k=0
    while Q< limit:
        nk = len(d) - k-1
        Q=Q+ betaDifferences(d[nk],a1,b1,a2,b2)
        k=k+1
    upperLimit = 100*d[nk]
    return np.round([lowerLimit,upperLimit],1)
def probPercent2LargerPercent1(percent1,n1,percent2,n2,confidence=0.99,dx=0.001):
    p1 = percent1/100.0
    p2 = percent2/100.0
    a1 = p1*n1 + prior
    b1 = (1-p1)*n1 + prior
    a2 = p2*n2 + prior
    b2 = (1-p2)*n2 + prior

    d = np.arange(0,1,dx)
    P=0.0
    for _d in d:
        P = P+ betaDifferences(_d,a1,b1,a2,b2)
    P = P*dx
    if P>1:
        P=1
    if P<0:
        P=0
    if P> 0.99:
        message='significant'
    else:
        message = 'not significant'
    return [np.round(P,3),message]
