from scipy.stats import gamma,invgamma,t,lognorm,norm
from scipy.special import gamma as gammaFunction
import numpy as np
import matplotlib.pyplot as plt

def logSquareRootInverseGammapdf(y, alpha,beta,rate):
    x = (np.log(y)/rate)**2
    dydx = 0.5*y* rate /np.sqrt(x)
    p = invgamma.pdf(x,a = alpha, scale = beta)/dydx
    return p

def logTpdf(x,df,loc=0,scale=1):
    """
    pdf of lot T distribution with df degrees of freedom and
    given location and scale parameters
    """
    p = t.pdf(np.log(x),df=df, loc = loc,  scale = scale)/x
    return p
def logTppf(p,df,loc,scale):
    x = t.ppf(p,df=df, loc = loc,  scale = scale)
    #print('t.ppf: p',p,'x',x)
    return np.exp(x)

def logTPercentileRatio(p0=0.005,p1=0.995,df=30,loc=0,scale=1):
    l0 = logTppf(p0,df,loc,scale)
    l1 = logTppf(p1,df,loc,scale)
    return(l1/l0)
def logNormPercentileRatio(p0=0.005,p1=0.995,mu=0,sigma2=1):
    l0 = norm.ppf(p0,loc=mu,scale= np.sqrt(sigma2))
    l1 = norm.ppf(p1,loc=mu,scale= np.sqrt(sigma2))
    #print('norm.ppf: p0',p0,'l0',l0,'p1',p1,'l1',l1)
    l0=np.exp(l0)
    l1=np.exp(l1)
    return(l1/l0)
def fitLogTPercentileRatio(targetPercentileRatio,p0=0.005,p1=0.995,df=30):
    if df <= 2:
        print('degrees of freedom has to be larger than 2')
        return -1
    z1 = norm.ppf(p1)
    z0 = norm.ppf(1-p0)
    sigma2  = (np.log(targetPercentileRatio)/(z1+z0))**2
    rho= logTPercentileRatio(p0,p1,df,0,scale=np.sqrt(sigma2))
    while (rho>targetPercentileRatio):
        previous_sigma2 = sigma2
        sigma2= sigma2*0.999
        rho= logTPercentileRatio(p0,p1,df,0,scale=np.sqrt(sigma2))
    sigma2  = previous_sigma2
    rho= logTPercentileRatio(p0,p1,df,0,scale=np.sqrt(sigma2))
    print('fit rho',rho,'fit sigma2',sigma2)
    return sigma2
def logTmedian(df,loc,scale):
    """
    pdf of lot T distribution with df degrees of freedom and
    given location and scale parameters
    """
    return logTppf(0,5,df,loc,scale)

    p = (1 + (((np.log(x) - loc)/scale)**2)/df)**( -( df +1)/2.0)/x
    k = gammaFunction((df+1)/2.0)/gammaFunction(df/2.0)
    k = k/(np.sqrt(np.pi*df)*scale)
    p = k*p
    print(k)
    return p

def plotlogT(x,df,loc,scale,c):
    y = logTpdf(x,df,loc,scale)
    plt.plot(x,y,c)
    median = logTmedian(df,loc,scale)
    f=plt.plot([median,median],[0,y.max()],c)
    return f

class LogNormalGammaConjugateModel():
    """"
    Standard Bayesian Conjugate Model for making inferences about the
    unobserved mean and variance based on observed iid
    Normal random variables
    Math Documentation: https://gitlab.com/movellan/kolmogorov/
    -/blob/master/bayesianMethods/normalGammaConjugate.pdf

    """
    def __init__(self):
        """

        """
# the only parameters needed are alpha beta, mu kappa
# nu, rho, n are
        # prior
        self.alpha_0 = 10
        self.beta_0 = 10
        self.mu_0= 10
        self.kappa_0 = 10

        # posterior
        self.alpha =self.alpha_0
        self.beta = self.beta_0
        self.mu = self.mu_0
        self.kappa = self.kappa_0


    def setPriorsVirtualSampleMethod(self,nu,rho,n):
        self.mu_0 = np.log(nu)
        self.kappa_0 = n
        self.alpha_0 = n/2.0
        variance_x = fitLogTPercentileRatio(rho,df = 2*self.alpha_0)
        self.beta_0 = variance_x * (self.alpha_0)* self.kappa_0/(self.kappa_0+1)
        self.alpha =self.alpha_0
        self.beta = self.beta_0
        self.mu = self.mu_0
        self.kappa = self.kappa_0

    def displayModelParametersAndStatistics(self):
        print('alpha_0',self.alpha_0,'beta_0',self.beta_0,'mu_0',self.mu_0, 'kappa_0',self.kappa_0)
        print('alpha',self.alpha,'beta',self.beta,'mu',self.mu, 'kappa',self.kappa)
        print('E(sigma2)',self.getExpectedSigma2(),'var(mu)',self.getVarianceOfMu())
        nu = np.exp(self.mu)
        print('Median of Raw Observations', nu)
        z1 = norm.ppf(0.995)
        variance_x = self.beta/(self.alpha-1)
        rho = np.exp(2*z1*np.sqrt(variance_x))
        df = 2* self.alpha
        scale = np.sqrt(variance_x)
        rho= logTPercentileRatio(df=df,scale=scale)
        print('Percentile Ratio Raw Observations',rho)

    def displaySigma2Distribution(self,c='r'):
        plt.subplot(1,2,2)
        l0 = invgamma.ppf(0.005,a = self.alpha, scale = self.beta)
        l1 = invgamma.ppf(0.995, a = self.alpha, scale = self.beta)
        x_v = np.arange(l0,l1,(l1-l0)/10000.0)
        pdf_v1 = invgamma.pdf(x_v,a = self.alpha, scale = self.beta)
        #fsigma2 = plt.plot(x_v,pdf_v1,c)
        z1 = norm.ppf(0.995)
        z0 = norm.ppf(1-0.005)
        rate = z1+z0
        l0 = np.exp(rate*np.sqrt(l0))
        l1 = np.exp(rate*np.sqrt(l1))
        x_v = np.arange(l0,l1,(l1-l0)/10000.0)
        pdf_v2 = logSquareRootInverseGammapdf(x_v,self.alpha, self.beta,rate=rate)
        fsigma2 = plt.plot(x_v,pdf_v2,c)
        median = invgamma.ppf(0.5, a = self.alpha, scale = self.beta)
        median = np.exp(rate*np.sqrt(median))
        print('invGamma Median',median, 'sigma2Mean',self.beta/(self.alpha-1.0),'rhoMedian',median)
        pdf_at_median = logSquareRootInverseGammapdf(median,self.alpha, self.beta,rate=rate)
        plt.plot([median,median],[0,pdf_at_median],c+'--')
        plt.xlabel('Percentile Ratio (99% region)')

    def displayMuPriorDistribution(self,c='r'):
        plt.subplot(1,2,1)
        df = 2*self.alpha_0
        epsilon = np.sqrt(self.beta_0/(self.alpha_0* self.kappa_0))
        l0 = t.ppf(0.005,df=df, loc = self.mu_0,  scale = epsilon)
        l1 = t.ppf(0.995,df=df, loc = self.mu_0,  scale = epsilon)
        l0 = np.exp(l0)
        l1 = np.exp(l1)
        step=(l1-l0)/10000.0
        x_m = np.arange(l0,l1,step)
        pdf_t = logTpdf(x_m,df,self.mu_0,epsilon)
        fm=plt.plot(x_m,pdf_t,c)
        median = np.exp(self.mu_0)
        plt.plot([median,median],[0,pdf_t.max()],c)
        plt.xlabel('Median (99% region)')
        plt.ylabel('Probability Density')
        return fm
    def displayPriorPredictiveDistribution(self,c='r'):
        plt.subplot(1,2,1)
        df = 2*self.alpha_0
        gamma = np.sqrt((self.kappa_0+1)*self.beta_0/(self.alpha_0* self.kappa_0))
        l0 = t.ppf(0.005,df=df, loc = self.mu_0,  scale = gamma)
        l1 = t.ppf(0.995,df=df, loc = self.mu_0,  scale = gamma)
        l0 = np.exp(l0)
        l1 = np.exp(l1)
        print('l0_predictive',l0,'l1_predictive',l1,'rho_predictive',l1/l0)
        step=(l1-l0)/10000.0
        x_m = np.arange(l0,l1,step)
        pdf_t = logTpdf(x_m,df,self.mu_0,gamma)
        fm=plt.plot(x_m,pdf_t,c)
        plt.xlabel('X (99% region)')
        plt.ylabel('Probability Density')
        return fm
    def getVarianceOfMu(self):
        sigma2 = self.beta/((self.alpha-1.0)*self.kappa)
        return sigma2
    def getVariancePredictiveDistribution(self):
        sigma2 = self.beta*(self.kappa+1)/((self.alpha-1.0)*self.kappa)
        return sigma2
    def getMeanPredictiveDistribution(self):
        return self.mu
    def getExpectedSigma2(self):
        return self.beta/(self.alpha-1)
    def getParametersOfPredictiveDistributionOfSum(self,n,c='r'):
        sigma2 = self.getVariancePredictiveDistribution()
        mu = self.getMeanPredictiveDistribution()
        mean, var = lognorm.stats(s=np.sqrt(sigma2),scale=np.exp(mu), moments='mv')
        mean_of_sum = n * mean
        var_of_sum  = n* var
        mean_of_squares = var_of_sum + mean_of_sum**2.0
        #moment matching Fenton Wilkinson approximation to sum of logNormals
        sigma2_of_sum = np.log(mean_of_squares/(mean_of_sum**2))
        mu_of_sum = np.log(mean_of_sum) - sigma2_of_sum/2.0
        return(mu_of_sum,sigma2_of_sum)

    def ppfOfPredictiveDistributionOfSum(self,p,n):
        mu_of_sum,sigma2_of_sum = self.getParametersOfPredictiveDistributionOfSum(n)
        x = lognorm.ppf(p,s=np.sqrt(sigma2_of_sum),scale= np.exp(mu_of_sum))
        return x
    def displayPredictiveDistributionOfSum(self,n,c='r'):
        if self.alpha <4: return
        plt.subplot(1,2,1)
        mu_of_sum,sigma2_of_sum = self.getParametersOfPredictiveDistributionOfSum(n)
        l0 = lognorm.ppf(0.005,s=np.sqrt(sigma2_of_sum),scale= np.exp(mu_of_sum))
        l1 = lognorm.ppf(0.995,s=np.sqrt(sigma2_of_sum),scale= np.exp(mu_of_sum))
        x = np.arange(l0,l1,(l1-l0)/10000.0)
        print('sigma2_of_sum',sigma2_of_sum,'mu_of_sum',mu_of_sum)
        y = lognorm.pdf(x,s=np.sqrt(sigma2_of_sum),scale= np.exp(mu_of_sum))
        plt.plot(x,y,c)
        #plt.plot([1,1],[0,y.max()])

        # plt.xlim([0,400])
        # plt.ylim([0,0.01])
    def updateModelParameters(self,x):
        x = np.array(x)
        x = np.log(x+1e-10)
        n = x.size
        m = x.mean()
        v = x.var()
        self.alpha = self.alpha + n/2.0

        self.beta = self.beta + 0.5* (n*v + (self.kappa*n/(self.kappa+n))*(m-self.mu)**2)

        self.mu = (self.kappa * self.mu + n*m)/(self.kappa +n)
        self.kappa = self.kappa + n


#
#
# model1 =  LogLogNormalGammaConjugateModel(100, 10, 40, 100)
# fsigma2 = model1.displaySigma2Distribution('r')
# fmu = model1.displayMuPriorDistribution('r')
# model2 =  LogNormalGammaConjugateModel(100, 100, 40, 1000)
# fsigma2 = model2.displaySigma2Distribution('g')
# fmu2 = model2.displayMuPriorDistribution('g')

model1 = LogNormalGammaConjugateModel()
model1.setPriorsVirtualSampleMethod(10,100,10)

for i in range(10):
    plt.subplot(1,2,1)
    plt.cla()
    plt.subplot(1,2,2)
    plt.cla()
    x = [10.0,10.0,10.0,10.0,10.0,10.0,10.0,10.0,10.0,10.0]
    print('x',x)
    model1.updateModelParameters(x)
    model1.displayPredictiveDistributionOfSum(1,'r')
    fsigma2 = model1.displaySigma2Distribution('r')
    plt.pause(0.01)


model1.displayModelParametersAndStatistics()
# n=100
# print('percentile 10',model1.ppfOfPredictiveDistributionOfSum(0.1,n))
# print('median',model1.ppfOfPredictiveDistributionOfSum(0.5,n))
# print('percentile 90',model1.ppfOfPredictiveDistributionOfSum(0.9,n))
# #fmu = model1.displayMuPriorDistribution('r')
# fsigma2 = model1.displaySigma2Distribution('r')
# fx = model1.displayPriorPredictiveDistribution('r')
# model1.displayPriorPredictiveDistributionOfSum(1,'b--')
# model1.displayPriorPredictiveDistributionOfSum(2,'b--')
# model1.displayPriorPredictiveDistributionOfSum(4,'b--')
# model1.displayPriorPredictiveDistributionOfSum(6,'b--')
# plt.show()
#
# model2 = LogNormalGammaConjugateModel()
# model2.setPriorsVirtualSampleMethod(100,10,64)
# model2.displayModelParametersAndStatistics()
# #fmu2 = model2.displayMuPriorDistribution('g')
# fsigma2_2 = model2.displaySigma2Distribution('g')
# fx2 = model2.displayPriorPredictiveDistribution('y')
# model3 = LogNormalGammaConjugateModel()
# model3.setPriorsVirtualSampleMethod(100,10,256)
# model3.displayModelParametersAndStatistics()
# #fmu2 = model3.displayMuPriorDistribution('b--')
# fsigma2_3 = model3.displaySigma2Distribution('b')
# fx3 = model3.displayPriorPredictiveDistribution('y')
# plt.show(block=False)
