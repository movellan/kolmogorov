from scipy.stats import norm
import numpy as np
import matplotlib.pyplot as plt



sample_size= 100*2 +1
sigma2= 0.027398974188114347
sigma = np.sqrt(sigma2)
mu=  7.9926680805561885
nsims =10000
lpdfList=[]
lpdfList_inf=[]




for i in range(nsims):
    # m simulates a samplemean
    m = norm.rvs(size=1,loc=mu,scale = sigma/np.sqrt(sample_size))
    # y simulates a new observation
    y = norm.rvs(size=1,loc=mu,scale=sigma)
    l =  np.log(norm.pdf(y,loc=m,scale = sigma))
    l_inf =  np.log(norm.pdf(y,loc=mu,scale = sigma))
    lpdfList.append(l)
    lpdfList_inf.append(l_inf)

lpdfList=np.array(lpdfList)
lpdfList2=np.array(lpdfList_inf)

mean_log_pdf = np.mean(lpdfList)
var_log_pdf = np.var(lpdfList)

mean_log_pdf_inf =  np.mean(lpdfList_inf)
var_log_pdf_inf =  np.var(lpdfList_inf)

sample_size=sample_size+0.0
predicted_mean= -0.5*np.log(2*np.pi*sigma2) - 0.5 *(1+1/sample_size)
predicted_mean_inf = -0.5*np.log(2*np.pi*sigma2) - 0.5
#predicted_variance= 3 + 3/(sample_size**2)+6/sample_size- (1+1/sample_size)**2
predicted_variance = (2*(1+1/sample_size)**2)/4.0
predicted_variance_inf= 0.5

print('sample_size',sample_size,'empirical mean log_pdf',mean_log_pdf,'theoretical mean',predicted_mean)
print('sample_size infinite','empirical mean log_pdf',mean_log_pdf_inf,'theoretical mean',predicted_mean_inf)
print('sample_size',sample_size,'empirical std log_pdf',np.sqrt(var_log_pdf),'theoretical std', np.sqrt(predicted_variance))
print('sample_size infinite','empirical std log_pdf',np.sqrt(var_log_pdf_inf),'theoretical std', np.sqrt(predicted_variance_inf))

required_n_for_2_sd= 4*predicted_variance_inf/((predicted_mean-predicted_mean_inf)**2)
print('required memory',(required_n_for_2_sd-1)/2)
