import numpy as np
from scipy.stats import lognorm
import matplotlib.pyplot as plt



def simulation(nvar,nsims,rows,cols,k):
    mu =1
    sigma=1
    sigma2 = sigma**2.0
    xlist =[]
    for s in range(nsims):
        xlist.append( np.sum(lognorm.rvs(s=sigma,scale=np.exp(mu),size=nvar)))
    # fig, ax = plt.subplots(1, 1)
    # xlist = np.array(xlist)
    plt.subplot(rows,cols,k)
    plt.hist(xlist, 100, density=True, facecolor='g', alpha=0.75)
    #plt.show()


    mean, var = lognorm.stats(s=sigma,scale=np.exp(mu), moments='mv')

    theoreticalMean = np.exp(mu+sigma2/2.0)
    theoreticalVar =  (np.exp(sigma2)-1)*np.exp(2*mu+sigma2)
    # print('theoreticalMean',theoreticalMean,'mean',mean)
    # print('theoreticalVar',theoreticalVar,'var',var)

    # mean and variance and sum of squares of the sum
    meanSum = nvar*mean
    VarSum = nvar*var
    varSumSquares = VarSum + meanSum**2

    # mu and sigma parameters of a lognormal with
    # the desired mean and variance of the sum
    sigma2Sum = np.log(varSumSquares/(meanSum**2))
    muSum = np.log(meanSum) - sigma2Sum/2.0
    x = np.arange(0,500,1/1000.0)
    y = lognorm.pdf(x,s=np.sqrt(sigma2Sum),scale= np.exp(muSum))
    plt.subplot(rows,cols,k)
    plt.plot(x,y)
    plt.xlim((0,500))

simulation(10,40000,1,1,1)
simulation(20,40000,1,1,1)
# simulation(40,40000,1,1,1)
# simulation(80,40000,1,1,1)
plt.show()
