from scipy.stats import gamma,invgamma,t,lognorm,norm
from scipy.special import gamma as gammaFunction
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import NearestNeighbors
from scipy.stats import randint as randint
import warnings
import sys
#warnings.simplefilter('error')


class NormalGammaConjugateModel():
    """"
    Standard Bayesian Conjugate Model for making inferences about the
    unobserved mean and variance based on observed iid
    Normal random variables
    Math Documentation: https://gitlab.com/movellan/kolmogorov/
    -/blob/master/bayesianMethods
    X: Observable variable with unknown mean mu and variance sigma2
    Model Parameters: alpha,beta, theta, kappa
        alpha, beta determine the sampling distribution of sigma2
        theta, kappa,sigma2 determine the sampling distribution of mu
        mu, sigma2 determine the sampling distribution of X


    """
    def __init__(self,memory_size=np.inf,logNormal=False):
        """

        """
# the only parameters needed are alpha beta, theta kappa
#memory size and whether we use a log normal model
        # priorS
        self.alpha_0 = 10
        self.beta_0 = 10
        self.theta_0= 10
        self.kappa_0 = 10
        n_sample= 1e-30
        mean_0 = 100
        var_0  = 100
        self.setPriorsUsingVirtualSample(n_sample,mean_0,var_0)
        # how much we remember the past
        # inf  for reme,ber all past
        self.memory_size = memory_size
        self.logNormal = logNormal
        # posterior
        self.theta = self.theta_0
        self.kappa = self.kappa_0
        self.alpha =self.alpha_0
        self.beta= self.beta_0


    def resetParameters(self):
        self.theta = self.theta_0
        self.kappa = self.kappa_0
        self.alpha= self.alpha_0
        self.beta=self.beta_0

    def setPriorsUsingVirtualSample(self,n_sample,mean,var):
        self.theta_0 = mean
        self.kappa_0 = n_sample
        self.alpha_0 = n_sample/2.0
        # shall really be var* (self.alpha_0 - 1) but would
        # not work for alpha <= 1
        self.beta_0 =  var * self.alpha_0
        self.resetParameters()

    def setMemorySize(self,memory_size):
        ''' how many past observations are used to make inferences
        about the mean and the variance
        inf  means use all past observations
        '''
        self.memory_size = memory_size

    def setPriors(self,theta,kappa,alpha,beta):
        self.theta_0 = theta
        self.kappa_0 = kappa
        self.alpha_0 = alpha
        self.beta_0 = beta
        self.resetParameters()

    def getExpectedLambda(self):
        return self.alpha/self.beta

    def getExpectedSigma2(self):
        if self.alpha <= 1.0:
            return np.inf
        else:
            return self.beta/(self.alpha -1.0)

    def getExpectedMu(self):
        return self.theta

    def getVarianceOfMu(self):
        return self.getexpectedSigma2()/self.kappa

    def getExpectedX(self):
        return self.theta

    def getVarianceOfX(self):
        return self.getExpectedSigma2()*(self.kappa+1.0)/self.kappa

    def getExpectedXLogNormal(self):
        mu = self.getExpectedX()
        sigma2 = self.getVarianceOfX()
        m = np.exp(mu+sigma2/2)
        return m

    def getVarianceOfXLogNormal(self):
        mu = self.getExpectedX()
        sigma2 = self.getVarianceOfX()
        v = (np.exp(sigma2) -1)* np.exp(2*mu+sigma2)
        return v


    def getVarianceOfTheta(self):
        if self.alpha>1.0:
            sigma2 = self.beta[k]/((self.alpha)*self.kappa)
        else:
            sigma2 = np.inf
        return sigma2

    def getParametersOfPredictiveDistributionOfSum(self,n=1):
        var = self.getVarianceOfX()
        mean = self.getMeanOfX()
        mean_of_sum = n * mean
        var_of_sum  = n* var
        return(mean_of_sum,var_of_sum)

    def predictiveDistribution_pdf(self,x):
        df = 2*self.alpha
        gamma = np.sqrt((self.kappa+1)*self.beta/(self.alpha* self.kappa))
        z = np.array((x-self.theta)/gamma)
        z[z>50] = 50
        z[z<-50]= -50
        pdfList =[]
        if self.logNormal:
            x = np.max((float(x),1e-30))
            #pdf = self.logTpdf(x,df=df, loc = self.theta,  scale = gamma)
            #tmp experiment. Should use logTpdf
            pdf = t.pdf(np.log(x),df=df, loc = self.theta,  scale = gamma)
        else:
            pdf = t.pdf(x,df=df, loc = self.theta,  scale = gamma)
        return pdf

    def logTpdf(self,x,df=10,loc=0,scale=1):
        """
        pdf of log T distribution with df degrees of freedom and
        given location and scale parameters
        """
        p = t.pdf(np.log(x),df=df, loc = loc,  scale = scale)/x
        return p


    def predictiveDistribution_ppf(self,p=0.5):

        # returns value such prob generating value or less is p
        df = 2*self.alpha
        if df< 1e-20:
            df = 1e-20
        gamma = np.sqrt((self.kappa+1)*self.beta/(self.alpha* self.kappa))
        ppf = t.ppf(p,df=df, loc = self.theta,  scale = gamma)
        return ppf

    def predictiveDistributionOfSum_pdf(self,n,x):
        # each element of x is the sum of n observables
        # the posterior is a sum of student T random variables.
        # we approximate with a normal distribution if n >1
        if n ==1 :
            pdf = self.predictiveDistribution_pdf(x)
        else:
            theta_of_sum,sigma2_of_sum = self.getParametersOfPredictiveDistributionOfSum(n)
            pdf= norm.pdf(x,loc = theta_of_sum,scale= np.sqrt(sigma2_of_sum))
        return pdf

    def predictiveDistributionOfSum_ppf(self,n=1,p=0.5):
        if n ==1 :
            ppf = self.predictiveDistribution_ppf(p)
        else:
            theta_of_sum,sigma2_of_sum = self.getParametersOfPredictiveDistributionOfSum(n)
            if sigma2_of_sum == np.inf:
                print('illegal value for variance')
                return('illegal value for variance')
            ppf= norm.ppf(p,loc = theta_of_sum,scale= np.sqrt(sigma2_of_sum))
        return ppf


    def predictiveDistributionOfLogNormalSum_ppf(self,n=1,p=0.5):
        # Felton-Wilkinson Moment Matching Method for Sum of LogNormas
        # X now is the log(observation)
        # we approximate the predictive distribution of X
        # using a Normal distribution
        # first we get the mean and variance of X
        mu = self.getExpectedX()
        sigma2 = self.getVarianceOfX()
        if sigma2 == np.inf:
            print('illegal value for variance')
            return('illegal value for variance')
        # Observation = exp(X), which is log Normal with params mu,sigma2
        # we now get the mean and variance of the sum of n log normals
        lognormalMean = n* np.exp(mu+sigma2/2.0)
        lognormalVar =  n*(np.exp(sigma2)-1.0)*np.exp(2.0*mu+sigma2)
        lognormalSumSquares = lognormalVar+ lognormalMean**2.0
        # we now find the mu and sigma2 parameters of a logNormal
        # that matches the desired Mean and Variance, using Moment Matching
        sigma2Sum = np.log(lognormalSumSquares/(lognormalMean**2))
        muSum = np.log(lognormalMean) - sigma2Sum/2.0
        # to implement logNormal with parameters mu and sigma2
        # call lognorm.pdf(s=sigma,scale=np.exp(mu))
        ppf = lognorm.ppf(p,s=np.sqrt(sigma2Sum),scale= np.exp(muSum))
        return ppf




    def predictCompletionTime(self,n=1,p=0.5):
        # time to complete n tasks with prob p
        return self.predictiveDistributionOfSum_ppf (n,p)



    def updateModelParameters(self,x):
        x = np.array(x)
        if self.logNormal:
            x[x<1e-30] = 1e-30
            x = np.log(x)
        n = x.size
        m = x.mean()
        v = x.var()
        self.alpha = self.alpha + n/2.0
        self.beta= self.beta + 0.5* (n*v + (self.kappa*n/(self.kappa+n))*(m-self.theta)**2)
        self.theta = (self.kappa * self.theta + n*m)/(self.kappa +n)
        self.kappa = self.kappa + n
        self.forget()

    def forget(self):
        if self.memory_size == np.inf:
            return
        if self.kappa > self.memory_size:
            self.kappa = self.memory_size
            c = self.beta/self.alpha
            self.alpha=  self.memory_size/2.0
            self.beta =  c* self.alpha

    def averageLogLikelihood(self, n,x):
        # eac element of x is the sum of n Observations
        pdf= self.predictiveDistributionOfSum_pdf(n,x)
        lpdf =np.log(pdf)
        return np.mean(lpdf)


    def displayModelParametersAndStatistics(self,title='model'):
        print(title)
        print('alpha_0',self.alpha_0,'beta_0',self.beta_0,'theta_0',self.theta_0, 'kappa_0',self.kappa_0)
        print(' alpha',self.alpha,'beta',self.beta,'theta',self.theta, 'kappa',self.kappa)
        print('E(sigma2)',self.getExpectedSigma2(),'var(theta)',self.getVarianceOftheta(),'var(X)',self.getVariancePredictiveDistribution())
        print('Virtual Sample Size Mean',n_mean,'Virtual Sample Mean',m)
        print('Virtual Sample Size Var',n_var,'Virtual Sample Var',v)

    def displaySigma2Distribution(self,c='r'):
        plt.subplot(1,2,2)
        l0 = invgamma.ppf(0.005,a = self.alpha, scale = self.beta)
        l1 = invgamma.ppf(0.995, a = self.alpha, scale = self.beta)
        if np.isnan(l0) or np.isnan(l1) or np.isinf(l0) or np.isinf(l1):
            return
        step = (l1-l0)/10000.0
        if step ==0:
            return
        x_v = np.arange(l0,l1,step)
        pdf_v1 = invgamma.pdf(x_v,a = self.alpha, scale = self.beta)
        fsigma2 = plt.plot(x_v,pdf_v1,c)
        plt.xlabel('Predicted Variance')

    def displayExpSigmaDistributionLogNormal(self,c='r'):
        l0 = invgamma.ppf(0.005,a = self.alpha, scale = self.beta)
        l0 = np.exp(np.sqrt(l0))
        l1 = invgamma.ppf(0.995, a = self.alpha, scale = self.beta)
        l1 = np.exp(np.sqrt(l1))
        if np.isnan(l0) or np.isnan(l1) or np.isinf(l0) or np.isinf(l1):
            return
        step = (l1-l0)/10000.0
        if step ==0:
            return
        y = np.arange(l0,l1,step)
        x = (np.log(y))**2
        dydx = y/(2*np.sqrt(x))
        pdf = invgamma.pdf(x,a = self.alpha, scale = self.beta)/dydx
        plt.plot(y,pdf,c)
        plt.xlabel('Predicted Scatter')
    def displayMuDistribution(self,c='r'):
        plt.subplot(1,2,1)
        df = 2*self.alpha
        epsilon = np.sqrt(self.beta/(self.alpha* self.kappa))
        l0 = t.ppf(0.005,df=df, loc = self.theta,  scale = epsilon)
        l1 = t.ppf(0.995,df=df, loc = self.theta,  scale = epsilon)
        if np.isnan(l0) or np.isnan(l1) or np.isinf(l0) or np.isinf(l1):
            return
        step=(l1-l0)/10000.0
        if step ==0:
            return
        x_m = np.arange(l0,l1,step)
        pdf_t = t.pdf(x_m,df=df, loc = self.theta,  scale = epsilon)
        fm=plt.plot(x_m,pdf_t,c)
        plt.xlabel('Predicted Mean')
        plt.ylabel('Probability Density')
        return fm

    def displayExpMuDistributionLogNormal(self,c='r'):
        #expMu is the median of the lognmormal
        #plt.subplot(1,2,1)
        df = 2*self.alpha
        epsilon = np.sqrt(self.beta/(self.alpha* self.kappa))
        l0 = t.ppf(0.005,df=df, loc = self.theta,  scale = epsilon)
        l0 = np.exp(l0)
        l1 = t.ppf(0.995,df=df, loc = self.theta,  scale = epsilon)
        l1 = np.exp(l1)
        if np.isnan(l0) or np.isnan(l1) or np.isinf(l0) or np.isinf(l1):
            return
        step=(l1-l0)/10000.0
        if step ==0:
            return
        x_m = np.arange(l0,l1,step)
        pdf_t = self.logTpdf(x_m,df=df, loc = self.theta,  scale = epsilon)
        plt.plot(x_m,pdf_t,c)
        plt.xlabel('Predicted Median')
        plt.ylabel('Probability Density')
        return

    def displayPredictiveDistribution(self,c='r'):
        plt.subplot(1,2,1)
        df = 2*self.alpha
        gamma = np.sqrt((self.kappa+1)*self.beta/(self.alpha* self.kappa))
        l0 = t.ppf(0.005,df=df, loc = self.theta,  scale = gamma)
        l1 = t.ppf(0.995,df=df, loc = self.theta,  scale = gamma)
        if np.isnan(l0) or np.isnan(l1) or np.isinf(l0) or np.isinf(l1):
            return
        step=(l1-l0)/10000.0
        if step ==0:
            return
        x_m = np.arange(l0,l1,step)
        pdf_t = self.predictiveDistribution_pdf(x_m)
        fm=plt.plot(x_m,pdf_t,c)
        plt.xlabel('Predicted Observation ')
        plt.ylabel('Probability Density')
        return fm


    def displayPredictiveDistributionOfSum(self,n=1,c='r'):
        if n ==1:
            self.displayPredictiveDistribution(c)
            return
        plt.subplot(1,2,1)
        theta_of_sum,sigma2_of_sum = self.getParametersOfPredictiveDistributionOfSum(n)
        l0 = norm.ppf(0.005,loc = theta_of_sum,scale= np.sqrt(sigma2_of_sum))
        l1 = norm.ppf(0.995,loc = theta_of_sum,scale= np.sqrt(sigma2_of_sum))
        if np.isnan(l0) or np.isnan(l1) or np.isinf(l0) or np.isinf(l1):
            return
        if l1 - l0 ==0:
            return
        x = np.arange(l0,l1,(l1-l0)/10000.0)
        y = norm.pdf(x,loc = theta_of_sum,scale= np.sqrt(sigma2_of_sum))
        y = self.predictiveDistributionOfSum_pdf(n,x)
        plt.plot(x,y,c)
        plt.xlabel('Predicted Sum '+str(n)+' Observations')
        plt.ylabel('Probability Density')


def compareModels(m1,m2):
    if m1.kappa != m2.kappa:
        print('different kappas')
        return -1
    if m1.theta != m2.theta:
        print('different thetas')
        return -1
    if m1.alpha !=  m2.alpha:
        print('different alphas')
        return -1
    if m1.beta !=  m2.beta:
        print('different betas')
        return -1
    if m1.memory_size != m2.memory_size:
        print('different memory size'   )
        return -1
    return 1
