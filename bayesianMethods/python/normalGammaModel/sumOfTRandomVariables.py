import numpy as np
from scipy.stats import lognorm, t,norm
import matplotlib.pyplot as plt
nvar = 1
nsims=2000



def simulation(nvar,nsims,rows,cols,k):
    mu =0
    scale=1
    df =10
    xlist =[]
    for s in range(nsims):
        xlist.append( np.sum(t.rvs(df=df,loc=mu,scale=scale,size=nvar)))
    # fig, ax = plt.subplots(1, 1)
    # xlist = np.array(xlist)
    plt.subplot(rows,cols,k)
    plt.hist(xlist, 400, density=True, facecolor='g', alpha=0.75)
    #plt.show()




    x = np.arange(-8*np.sqrt(nvar),8*np.sqrt(nvar),1/1000.0)
    #y = t.pdf(x,df = df*nvar,loc = mu*nvar, scale = 2**0.5)
    vhat = nvar * scale* scale * df/(df-2.0)
    sigmahat = np.sqrt(vhat)
    muhat = nvar * mu
    y = norm.pdf(x,loc = muhat, scale = sigmahat)
    plt.subplot(rows,cols,k)
    plt.plot(x,y)
    plt.xlim([-5*sigmahat,5*sigmahat])

# simulation(1,10000)
# simulation(10,10000)
simulation(10,100000,1,1,1)

plt.show()
