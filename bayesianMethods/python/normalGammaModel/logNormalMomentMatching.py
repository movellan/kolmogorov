import numpy as np
from scipy.stats import lognorm
import matplotlib.pyplot as plt

# We want the mu and sigma2 parameters
# of a log normal with mean m and var v

m = 100
v = 20
ss = v + m**2.0

sigma2 = np.log(ss/m**2)
mu = np.log(m) - sigma2/2.0

x = lognorm.rvs(s=np.sqrt(sigma2),scale=np.exp(mu),size=10000)


mhat = np.mean(x)
varhat = np.var(x)
print('mu',mu,'sigma2',sigma2)
print('mhat',mhat,'varhat',varhat)
