    # Same as experiment4 but using logNormals
from normalGammaModelEnsemble import *


np.random.seed(seed=123456)
#np.random.seed(seed=654321)

def lognormal_ppf(p,mean,std):
    var = std**2.0
    ss = var + mean**2.0
    sigma2 = np.log(ss/ (mean**2))
    mu = np.log(mean) - sigma2/2.0
    x = lognorm.ppf(p,s=np.sqrt(sigma2),scale=np.exp(mu))
    return(x)
# each model comes from a memory_size. The statistics are based on
# the last data points down to the memory_size
# so if a model has memory size 500, it will use the last 500
# points to predict the expected mean and variance
# sort from least to most conservative
# We define an ensemble of models, each with different memory-size
memList= [5,10,40,80,100,np.inf,np.inf,np.inf,np.inf]
memList= [40,80,100,120,140,np.inf,np.inf,np.inf,np.inf]
memList= [40,80,100,120,140,np.inf,np.inf,np.inf]
memList= [40,80,100,120,140,np.inf,np.inf]
memList= [20,40,50,60,70,np.inf,np.inf]
memList= [20,40,50,60,70,140,280,np.inf,np.inf]
memList= [20,40,80,160,320,640,np.inf,np.inf]
memList= [20,40,50,60,70,np.inf,np.inf]
memList= [20,40,50,60,70,np.inf]
memList= [20,np.inf] # best for disruptions
memList= [20,100,200,400,np.inf] # works well for disruptions and
                                 #continuous changes


memList= [20,100,200,800,np.inf]# surprisingly this has a lot of spikes  noise in constant case
memList= [20,100,200,600,np.inf]# this also creates spurious spikes
memList= [20,200,2000,20000,np.inf] #xsthis also creates spurious spikes
memList= [20,2000,np.inf]# this also creates spurious spikes in constant case
memList= [20,200,np.inf]#works well in constant case
memList= [20,500,np.inf]  # spikes
memList= [20,1000,10000,np.inf] # spikes
memList= [20,200,2000,20000,np.inf]
memList= [20,200,np.inf]#works well in constant case
memList= [5,10,20,40,80,160,320,640,1280,2560,5120,10240,20480,40960,811920,np.inf]
memList= [np.inf,np.inf]

modelThreshold=[]
for k in range(len(memList)):
    modelThreshold.append(np.inf)


#modelThreshold[3]= 7
#modelThreshold[4]= 22
#modelThreshold[5]= 14
#modelThreshold[6]= 24
#modelThreshold[7]= 25
#modelThreshold[8]= 26




resetSampleSize=[]
for k in range(len(memList)):
    resetSampleSize.append(memList[k])



nModels=len(memList)
pdf=np.zeros(nModels)
pdf2=np.zeros(nModels)
me = NormalGammaConjugateModelEnsemble(memory_sizeList=memList,logNormal=True)
pdfTrackers=[]
bestModelMemory=[]
for k in range(nModels):
    pdfTrackers.append(NormalGammaConjugateModelEnsemble(memory_sizeList=[np.inf],logNormal=False))





v = 0
entropyList=[]
vList =[]
count =0
meanList=[]
varList=[]
madList=[]
lowerBoundList=[]
upperBoundList=[]
trueMeanList=[]
trueUpperBoundList=[]
trueLowerBoundList=[]
oList=[]
consecutiveLossesList=[]
cll=[]
pdfList=[]
smooth_pdfList=[]
bestModelList=[]
updateUp=0
updates=0

for k in range(nModels):
    meanList.append([])
    varList.append([])
    madList.append([])
    lowerBoundList.append([])
    upperBoundList.append([])
    pdfList.append([])
    smooth_pdfList.append([])

# for combined models
meanList.append([])
varList.append([])
madList.append([])
pdfList.append([])
smooth_pdfList.append([])
lowerBoundList.append([])
upperBoundList.append([])



consecutiveLosses= np.zeros((me.nModels,me.nModels))
consecutiveLossers = np.zeros(me.nModels)
modelThresholds = np.ones((me.nModels,me.nModels))
for k in range(me.nModels):
    modelThresholds[k,:] = np.ones(me.nModels)*modelThreshold[k]






color =['r','g','b','y','c','r--','g--','b--','y--','c--','r','g','b','y','c','r--','g--','b--','y--','c--']
color = 6*color

nsims= 170
xmean0= 3000
xmean=xmean0
sd0= 500
sd = sd0
bestModelIndex=nModels-1
previousBestModelIndex=nModels-1
consecutiveBestModel=0
frameName=0
for i in range(1,nsims+1):
    sList =[]
    if i>1 and i%1==0:
        plt.clf()
        for k in range(nModels):
            plt.subplot(1,2,1)
            me.model[k].displayExpMuDistributionLogNormal(c='r')
            plt.plot([np.exp(mu),np.exp(mu)],[0,0.1],'b--')
            plt.xlim([2500,3500])
            plt.ylim([0,0.01])
            plt.subplot(1,2,2)
            me.model[k].displayExpSigmaDistributionLogNormal(c='r')
            plt.plot([np.exp(np.sqrt(sigma2)),np.exp(np.sqrt(sigma2))],[0,700],'b--')
            s =  '\u03B7'+': ' +str(me.model[k].kappa)
            if k==1:
                sList.append(s)
            plt.xlim([1.1,1.3])
            plt.ylim([0,35])
        # plt.text(3400,0.02,'Trial: '+str(i),ha="center", va="center", zorder=10)


        #plt.xlim([1.1,1.3])
        #plt.xlim([1500,5000])
        #plt.ylim([0,0.03])
        frameName+=1
        plt.legend(sList)
        plt.pause(0.1)
        plt.savefig('/tmp/images/'+str(frameName)+'.jpeg')

    cl = np.copy(me.consecutiveLosses)
    consecutiveLossesList.append(cl)
    if i % 100 ==0:
        s=str(i)+' Best Model '+str(bestModelIndex)+ ' consecutive' ,str(consecutiveBestModel),' MemSize '+ str(me.model[bestModelIndex].kappa)+\
        ' Threshold '+str(modelThreshold[bestModelIndex])
        print(s)





    if i> 2000:
        xmean= xmean0+0*sd
        sd = 1*sd0
    if i> 8000:
        xmean= xmean0+0*sd
        sd = 1*sd0
    if i> 8000:
        xmean= xmean0+0*sd
        sd = 1*sd0
    if i> 10000:
        xmean= xmean0+3*sd
        sd = 1*sd0
    if i > 20000:
        xmean= xmean0+0*sd
        sd = 1*sd0
    if i > 30000:
        xmean= xmean0+3*sd
        sd = 1*sd0
    #xmean = xmean0
    #optimal size infinity
    #xmean = xmean0+ 0.00*i
    #optimal size 800
    #xmean = xmean0+ 0.01*i
    #optimal size 400
    #xmean = xmean0+ 0.05*i
    #optimal size 200
    #xmean = xmean0+ 0.1*i
    #optimal isze 100
    #xmean = xmean0+ 0.2*i
    # optimal memory 100
    #xmean = xmean0+ 0.4*i
    #optimal memory 50
    #xmean = xmean0+ i
    # if i<=10000:
    #     xmean = xmean0 - 0.1*i
    # if i > 10000:
    #     xmean = xmean0 -0.1*10000
    if i>40000:
        xmean = xmean0 +3*sd0 - 0.1*(i-40000)
    if i > 50000:
        xmean = xmean0 +3*sd0-0.1*40000
    if i> 100000:
        xmean = xmean0 + 3*sd0-0.1*40000 + 0.0005*(i-100000)
    xmean = xmean0+ 0*sd0/(nsims+0.0)
    # if i < 20000:
    # if i<=20000:
    #     xmean = xmean0
    #     sd = sd0+ 2*sd0*i/20000.0
    # else:
    #     xmean = xmean0
    #     sd = 3*sd0
    #     xmean = xmean0 + np.abs(3*sd0*np.sin(2*np.pi*i/20000.0))
    # if i <20000:
    #     xmean = xmean0
    #     sd = sd0
    # else:
    #     xmean = xmean0
    #     sd = 3 *sd0


    # below is a failure mode for upperbound
    # xmean = xmean0 + np.abs(3*sd0*np.sin(2*np.pi*i/4000.0))
    #sd = 100
    # end of failure mode

    # the obsserved data is logNormal
    # with mean xmean and variance sd**2
    # we find mu and sigma2 parameters of logNormal
    # that match these moments
    var = sd**2.0
    ss = var + xmean**2.0

    sigma2 = np.log(ss/ (xmean**2))
    mu = np.log(xmean) - sigma2/2.0
    # to implement logNormal with parameters mu and sigma2
    # call lognorm.pdf(s=sigma,scale=np.exp(mu))

    m_2, var_2 = lognorm.stats(s=np.sqrt(sigma2),scale=np.exp(mu), moments='mv')
    entropy = np.log2(np.sqrt(sigma2)*np.exp(mu+0.5))
    entropyList.append(entropy)
    o = lognorm.rvs(s=np.sqrt(sigma2),scale=np.exp(mu),size=1)
    oList.append(float(o))
    # we pass the log of the observation to the model

    x = np.log(o)




    pdf3 = np.zeros(nModels)
    for k in range(nModels):
        pdf3[k] = np.array(pdfTrackers[k].getExpectedX())
    pdfmax = np.max(pdf3)
    bestModelIndex_ = np.argmax(pdf3)
    bestModelpdf_=pdf3[bestModelIndex_]
    bestModels_ = np.argwhere(pdf3 ==bestModelpdf_)
    bestModelIndex_ = np.max(bestModels_)
    for k in range(nModels):
        if k not in bestModels_ and bestModelIndex_ < k:
            consecutiveLossers[k]+=1
        else:
            consecutiveLossers[k]=0
    if i%100 ==0:
        print(consecutiveLossers)
        print(np.array(pdf3)*10000)

    for k in range(bestModelIndex_,nModels):
        if consecutiveLossers[k] > 300000000000000000:
            print('resetting',k, 'with', bestModelIndex_)
            me.model[k].theta=me.model[bestModelIndex_].theta
            ss = resetSampleSize[k]
            me.model[k].kappa = min([ss,me.model[bestModelIndex_].kappa])
            r =me.model[bestModelIndex_].beta/me.model[bestModelIndex_].alpha
            me.model[k].alpha = me.model[k].kappa/2.0
            me.model[k].beta = me.model[k].alpha*r
            pdfTrackers[k].model[0].alpha= pdfTrackers[bestModelIndex_].model[0].alpha
            pdfTrackers[k].model[0].beta= pdfTrackers[bestModelIndex_].model[0].beta
            pdfTrackers[k].model[0].theta= pdfTrackers[bestModelIndex_].model[0].theta
            pdfTrackers[k].model[0].kappa= pdfTrackers[bestModelIndex_].model[0].kappa
            consecutiveLossers[k] =0



    if i <100:
        trueMeanList.append(0)
        trueLowerBoundList.append(0)
        trueUpperBoundList.append(0)
        for k in range(nModels):
            meanList[k].append(0)
            upperBoundList[k].append(0)
            madList[k].append(0)
            lowerBoundList[k].append(0)
            varList[k].append(0)
            pdfList[k].append(0)
            smooth_pdfList[k].append(0)
        # for the combined ensemble predictions
        madList[nModels].append(0)
        pdfList[nModels].append(0)
        smooth_pdfList[nModels].append(0)
        meanList[nModels].append(0)
        upperBoundList[nModels].append(0)
        lowerBoundList[nModels].append(0)


    else:
        trueMeanList.append(xmean)
        trueUpperBoundList.append(lognormal_ppf(0.10,xmean,sd))
        trueLowerBoundList.append(lognormal_ppf(0.90,xmean,sd))

        for k in range(nModels):
            pdf[k] = float(np.log(me.model[k].predictiveDistribution_pdf(o)))
            pdfList[k].append(pdf[k])
            pdfTrackers[k].updateModelParameters(pdf[k])
            meanList[k].append(me.model[k].getExpectedXLogNormal())
            madList[k].append(np.abs(me.model[k].getExpectedXLogNormal() - xmean))
            upperBoundList[k].append(me.model[k].predictiveDistributionOfLogNormalSum_ppf(n=1,p=0.90))
            lowerBoundList[k].append(me.model[k].predictiveDistributionOfLogNormalSum_ppf(n=1,p=0.10))
            varList[k].append(me.model[k].getVarianceOfXLogNormal())
            smooth_pdfList[k].append(np.array(float(pdfTrackers[k].getExpectedX())))



        for k in range(nModels):
            pdf2[k] = np.array(pdfTrackers[k].getExpectedX())
            #pdf2[k] = np.array(np.log(me.model[k].predictiveDistribution_pdf(o)))

        # combined model ensemble

        best_mu=meanList[bestModelIndex][-1]
        meanList[nModels].append(best_mu)
        madList[nModels].append(np.abs(best_mu-xmean))
        varList[nModels].append(varList[bestModelIndex][-1])
        pdfList[nModels].append(pdfList[bestModelIndex][-1])
        smooth_pdfList[nModels].append(smooth_pdfList[bestModelIndex][-1])
        lowerBoundList[nModels].append(lowerBoundList[bestModelIndex][-1])
        upperBoundList[nModels].append(upperBoundList[bestModelIndex][-1])



        bestModelList.append(bestModelIndex)
        bestModelMemory.append(me.model[bestModelIndex].kappa)



        oldIndex =bestModelIndex

        # this is the model we will use for the next sample

        bestModelIndex = np.argmax(pdf2)
        bestModelpdf=pdf2[bestModelIndex]
        bestModels = np.argwhere(pdf2 ==bestModelpdf)
        bestModelIndex = np.max(bestModels)
        if oldIndex == bestModelIndex:
            consecutiveBestModel+=1
        else:
            consecutiveBestModel =0





        # # to switch a model needs to win twice in a row
        # if tmp2 != tmp1:
        #     bestModelIndex = tmp1
        #     previousBestModelIndex = tmp2




    me.updateBestModelCounts(o)
    me.updateModelParameters(o)



for k in range(nModels):
    lowerBoundList[k] = np.array(lowerBoundList[k])
    upperBoundList[k] = np.array(upperBoundList[k])
    meanList[k] = np.array(meanList[k])
    madList[k]=np.array(madList[k])
meanList[nModels]=np.array(meanList[nModels])
madList[nModels]=np.array(madList[nModels])
lowerBoundList[nModels]=np.array(lowerBoundList[nModels])
upperBoundList[nModels]=np.array(upperBoundList[nModels])




for k in range(nModels):
    print(k,'mad',np.mean(madList[k][500:]),np.std(madList[k][500:]))
print('bestModel mad',np.mean(madList[nModels][500:]),np.std(madList[nModels][500:]))

for k in range(nModels):
    print(k,'log pdf',np.mean(pdfList[k][500:]),np.std(pdfList[k][500:]))
print('bestModel log pdf',np.mean(pdfList[nModels][500:]),np.std(pdfList[nModels][500:]))


# for k in range(nModels):
#     print(k,'mean_pdf',np.mean(pdfList[k][500:]))
for k in range(nModels):
    print(k,'mean',np.mean(meanList[k][500:]))
print('bestModel mean',np.mean(meanList[nModels][500:]))
for k in range(nModels):
    print(k,'var',np.mean(varList[k][500:]))
print('bestModel var',np.mean(varList[nModels][500:]))


#plt.plot(oList[500:],'r')
plt.plot(oList,'r')
plt.ylabel('Time Per Task')
plt.xlabel('Time')
plt.show()

for k in range(nModels):
    plt.plot(lowerBoundList[k][500:],'g--')
    plt.plot(meanList[k][500:],'b')
    plt.plot(upperBoundList[k][500:],'g--')
    plt.plot(trueLowerBoundList[500:],'k--')
    plt.plot(trueMeanList[500:],'r')
    plt.plot(trueUpperBoundList[500:],'k--')
    plt.ylabel('Time Per Task')
    s='Model '+str(k)+ ' MemSize '+ str(memList[k])+' Threshold '+str(modelThreshold[k])+ ' resetSize '+str(resetSampleSize[k])
    plt.xlabel(s)
    plt.show()
plt.plot(meanList[nModels][500:],'b')
plt.plot(lowerBoundList[nModels][500:],'g--')
plt.plot(upperBoundList[nModels][500:],'g--')
plt.plot(trueLowerBoundList[500:],'k--')
plt.plot(trueMeanList[500:],'r')
plt.plot(trueUpperBoundList[500:],'k--')
plt.ylabel('Time Per Task')
plt.xlabel('Best Model')
plt.show()



u = np.array(smooth_pdfList)
#u[memModels,sample,smoother]

for k in range(nModels):
    plt.plot(np.exp(u[k,500:]),color[k])
plt.plot(np.exp(u[nModels,500:]),'k--')
plt.show()
plt.plot(bestModelMemory)
plt.xlabel('Time')
plt.ylabel('Memory Size')
plt.show()
plt.plot(100*me.bestModelCounts/(nsims+0.0),'r')
plt.plot(100*me.betterThanModel_0_Counts/(nsims+0.0),'g')
plt.ylabel('Model')
plt.xlabel('Percent Best')
plt.show()
plt.plot(bestModelList)
plt.xlabel('Time')
plt.xlabel('Best Model')
plt.show()
