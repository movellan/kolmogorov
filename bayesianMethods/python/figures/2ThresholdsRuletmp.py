import numpy as np
from scipy.stats import norm
from matplotlib import pyplot as plt

def posteriorcdf(u,x,sigma):
    #p(mu<u \given x)
    #p(mu|x) = p(x|mu)=g(mu|x,sigma^2_n)
    p = norm.cdf((u-x)/sigma)
    return(p)


def get_n(mu_1,mu_2,sigma,alpha,beta):
    z_1  = norm.ppf(alpha)
    z_2 = norm.ppf(1-beta)
    n = (sigma*(z_2 - z_1)/(mu_2-mu_1))**2
    theta = mu_1 - z_1*sigma/np.sqrt(n)
    return(n,theta)

def get_beta(mu_1,mu_2,sigma,n,alpha):
    z_1  = norm.ppf(alpha)
    sigma_n = sigma/np.sqrt(n)
    theta = mu_1 - z_1*sigma_n
    z_2 = (mu_2 -theta)/sigma_n
    beta = 1-norm.cdf(z_2)
    return(beta,theta)



# Percentages smaller than mu are unnaceptable
def plotRegions(mu_1,mu_2,sigma,n,theta,legend,x_min,x_max):
    sigma_n = sigma/np.sqrt(n)
    x = np.arange(x_min,x_max,(x_max-x_min)/1000)
    y = norm.cdf((x-theta)/sigma_n)
    plt.plot(x,y,'k')
    plt.plot((mu_1,mu_1),(0,1),'r--')
    plt.plot((mu_2,mu_2),(0,1),'r--')
    label = 'Prob Accept mu > '+str(mu_2)
    plt.ylabel(label)
    plt.xlabel('True Percentage (mu)')
    #plt.text(70,0.6,'Unacceptable Region')
    # plt.text(80.5,0.6,'Uncertain Region')
    # plt.text(94.5,0.6,'Acceptable Region')
    plt.text(82,0.85,'n='+str(int(np.ceil(n))))
    plt.text(82,0.2,legend)

def plotPosteriorProbabilities(mu_1,mu_2,sigma,n,theta,alpha,beta,x_min,x_max):
    sigma_n = sigma/np.sqrt(n)

    p_1 = posteriorcdf(mu_1,theta,sigma_n)
    print('prob(mu<mu_1| x = theta)=',p_1)

    p_2 = 1- posteriorcdf(mu_2,theta,sigma_n)
    print('prob(mu>mu_2| x = theta)=',p_2)

    x = np.arange(x_min,x_max,(x_max-x_min)/1000.0)
    p_1 = posteriorcdf(mu_1,x,sigma_n)
    plt.plot(x,p_1)
    p_2 = 1- posteriorcdf(mu_2,x,sigma_n)
    label = 'P(mu< '+str(mu_1)+' |x)'
    plt.plot(x,p_1,label=label)
    label = 'P(mu> '+str(mu_2)+' |x)'
    plt.plot(x,p_2,label=label  )
    label = 'P('+str(mu_1)+'< mu< '+str(mu_2)+' |x)'
    plt.plot(x,p_1-p_2,label=label  )
    plt.plot((theta,theta),(0,1),'r--')
    plt.xlabel('Observed Percentage (x)')
    plt.text(82,0.85,'n='+str(int(np.ceil(n))))
    plt.legend()

mu_1 =80
mu_2 =90
confidence_1=0.8
confidence_2 = 0.8

alpha= 1- confidence_1
beta=1- confidence_2
sigma2 =mu_1*(100-mu_1)
z_1 = norm.ppf(confidence_1)

z_2 = norm.ppf(confidence_2)

delta = 2

n = sigma2 * ( (z_2+z_1)/delta)**2
sigma_n = np.sqrt(sigma2/n)
#
theta_1 = mu_1 - z_1*sigma_n
theta_2 = mu_2 + z_2*sigma_n
#
#
# beta=  norm.cdf((mu_1 - theta_2)/sigma_n)
#



x = np.arange(70,100,0.001)

plt.subplot(2,1,1)
p1 = posteriorcdf(mu_1,x,sigma_n)
plt.plot(x,p1,'r',label='P(mu<mu_1)')


p2 = 1-posteriorcdf(mu_2,x,sigma_n)
plt.plot(x,p2,'g',label='P(mu > mu_2)')

plt.plot((70,100),(0.5,0.5),'y--')

plt.plot((theta_1,theta_1),(0,1),'y--')
plt.plot((theta_2,theta_2),(0,1),'y--')
plt.plot((70,100),(1-alpha,1-alpha),'k--')
plt.plot((70,100),(1-beta,1-beta),'k--')
plt.xlabel('Sample Proportion')

plt.plot((mu_1,mu_1),(0,1),'k--')
plt.plot((mu_2,mu_2),(0,1),'k--')
plt.plot(x,1-p1-p2,label='P(H_3|mu)')
plt.plot((70,100),(alpha,alpha),'g--')
plt.plot((70,100),(beta,beta),'g--')
plt.legend()

p5 = norm.cdf((mu_1-x)/sigma_n)
plt.plot(x,p5,'k')

plt.subplot(2,1,2)
# theta_1 =(mu_1+mu_2)/2
# theta_2=theta_1
p3 = norm.cdf((theta_1-x)/sigma_n)
plt.plot(x,p3,'r',label='P(Reject|mu)')
p4 = 1-norm.cdf((theta_2-x)/sigma_n)
plt.plot(x,p4,'g',label='P(Accept|mu)')
plt.plot(x,1-p3-p4,label='P(Withold|mu)')
plt.text(70,0.9,'N='+str(n))



plt.plot((mu_1,mu_1),(0,1),'k--')
plt.plot((mu_2,mu_2),(0,1),'k--')

plt.plot((theta_1,theta_1),(0,1),'y--')
plt.plot((theta_2,theta_2),(0,1),'y--')
plt.plot((70,100),(alpha,alpha),'k--')
plt.plot((70,100),(beta,beta),'k--')
plt.xlabel('True Proportion')
plt.plot(x,p3)
p5 = norm.pdf((x-theta_1)/sigma_n)
p5 = p5/norm.pdf(0)
plt.plot(x,p5,'r')
plt.legend()
plt.show()
