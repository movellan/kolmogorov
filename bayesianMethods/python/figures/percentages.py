from scipy.stats import beta
from scipy.stats import norm
from scipy import stats
import numpy as np
import betaDifference as bd
import matplotlib.pyplot as plt

''' Javier R. Movellan, @Apple, 2017, 2023
Probabilistic Inference for Proportions and Differences of Proportions
Arguments based on counts: number of successes and  of number of trials

'''


#TODO For difference between proportions treat it
# as model testing (same proportion model vs different prop model)
# but this does not give conf interval in Proportions
# TODO Poser analysis




prior=1 # uniform prior.





def confIntervalOfPercentage(success,counts,confidence=95):
    """
    Central confidence intervals for percentages
    p percentage, between 0 and 100
    n number of observations in p
    confidence: confidence value between 0 and 100
    Default confidence is 95%
    returns lower and upper limits of the confidence interval
    """
    confidence = confidence/100.0
    a =success +prior
    b = counts -success +prior
    l0 = 100*beta.ppf((1-confidence)/2.0,a,b)
    l1 = 100*beta.ppf(1-(1-confidence)/2.0,a,b)
    return(np.array([l0,l1]))

def upperBoundForPercentage(success,counts,confidence=95):
    """
    Confidence is a number between 0 and 100.  Default is 95%
    It equals the prob that the true percentage is smaller
    than the upper bound
    Returns upper bound on true success percentage

    """
    confidence = confidence/100.0
    a = success+prior
    b = counts - success + prior
    l = 100*beta.ppf(confidence,a,b)
    return l



def lowerBoundForPercentage(success,counts,confidence=95):
    """
    Confidence is a number between 0 and 100. Default is 95%
    It equals the prob that the true percentage is larger
    than the lower bound
    Returns lower bound on true success percentage
    """
    confidence = confidence/100.0
    a = success+prior
    b = counts - success + prior
    l = 100*beta.ppf(1-confidence,a,b)
    return l


def probPercentageLargerThanThreshold(success,counts,threshold):
    """
    Threshold is a number between 0 and 100
    Returns prob that true percentage is larger than Threshold
    """
    threshold = threshold/100.0
    a =success + prior
    b = counts - success +prior
    return 1-beta.cdf(threshold,a,b)

def probPercentageSmallerThanThreshold(success,counts,threshold):
    """
    Threshold is a number between 0 and 100
    Returns prob that true percentage is smaller than Threshold
    """
    threshold = threshold/100.0
    a =success + prior
    b = counts - success +prior
    return beta.cdf(threshold,a,b)

def probPercentage1MinusPercentage2LargerThanThreshold(success1,counts1,success2,counts2,threshold=0,upper_limit=100):
    beta_d = bd.BetaDiff(success1,counts1,success2,counts2, a_0=prior,b_0=prior,upper_limit=upper_limit)
    return(1-beta_d.cdf(threshold/100.0))

def upperBoundPercentage1MinusPercentage2(success1,counts1,success2,counts2,upper_limit=100,confidence=95):
    beta_d = bd.BetaDiff(success1,counts1,success2,counts2, a_0=prior,b_0=prior,upper_limit=upper_limit)
    return(100* beta_d.ppf(confidence/100.0))

def lowerBoundPercentage1MinusPercentage2(success1,counts1,success2,counts2,upper_limit=100,confidence=95):
    confidence = 100 - confidence
    beta_d = bd.BetaDiff(success1,counts1,success2,counts2, a_0=prior,b_0=prior,upper_limit=upper_limit)
    return(100* beta_d.ppf(confidence/100.0))

def confIntervalPercentage1MinusPercentage2(success1,counts1,success2,counts2,upper_limit=100,confidence=95):
    confidence = confidence/100.0
    lowerBound = (1- confidence)/2.0
    upperBound = 1- lowerBound
    beta_d = bd.BetaDiff(success1,counts1,success2,counts2, a_0=prior,b_0=prior,upper_limit=upper_limit)
    lb = 100* beta_d.ppf(lowerBound)
    ub = 100* beta_d.ppf(upperBound)
    return(np.array([lb,ub]))


def plotPosteriorDistributionOfPercentage(success,counts,upper_limit=100,perMillion=False,legend='pdf',resolution=1000000.0,ymax=-1,xmin=0,xmax=100,pdfOnly=True):
    if(perMillion==True):
        scale = 1000000
        xlabel = "Per Million"
    else:
        scale =100
        xlabel = "Percent"
    upper_limit = upper_limit/100
    lower_limit = max(0,- upper_limit)
    resolution = (upper_limit-lower_limit)/resolution
    x = np.arange(lower_limit,upper_limit+resolution,resolution)
    pdf = beta.pdf(x,success+prior,counts-success+prior)
    plt.plot(scale*x,pdf,label=legend)
    plt.legend(loc='best')
    plt.xlabel(xlabel)
    plt.ylabel('Posterior pdf')
    if pdfOnly== False:
        mdx=beta.ppf(0.5,success+prior,counts-success+prior)
        mdy = beta.pdf(mdx,success+prior,counts-success+prior)
        plt.plot([scale*mdx,scale*mdx],[0,mdy],'r--')
        llx = beta.ppf(0.025,success+prior,counts-success+prior)
        lly = beta.pdf(llx,success+prior,counts-success+prior)
        plt.plot([scale*llx,scale*llx],[0,lly],'r--')
        ulx = beta.ppf(0.975,success+prior,counts-success+prior)
        uly = beta.pdf(ulx,success+prior,counts-success+prior)
        plt.plot([scale*ulx,scale*ulx],[0,uly],'r--')
    plt.legend(loc='best')
    plt.xlabel(xlabel)
    plt.ylabel('Posteior pdf')
    if ymax>0:
        plt.ylim(0,ymax)
    plt.xlim(xmin,xmax)

def plotPosteriorDistributionOfPercentage1MinusPercentage2(success1,counts1,success2,counts2,upper_limit=100,legend='Beta1-Beta2',perMillion=False):
    beta_d = bd.BetaDiff(success1,counts1,success2,counts2, a_0=prior,b_0=prior,upper_limit=upper_limit)
    beta_d.plot(perMillion=perMillion,legend=legend)

# def welch_tTestForObservedProportions(p1,p2):
#         """
#         p1 is a list of per-person proportions in group2 (e.g., FRR in females)
#         p2 is a list of per-person proportions for group2 (e.g., FRR in males)
#         example: p1 =[0.27,0.22,0.3], p2=[0.4,0.39]
#         """
#         [t,p]=ttest_ind(p1, p2, equal_var = False)
#         n1= len(p1)+0.0
#         n2= len(p2)+0.0
#         v1= np.var(p1,ddof=1)
#         v2 = np.var(p2,ddof=1)
#         num = (v1/n1 +v2/n2)**2.0
#         den = v1**2.0/(n1*n1*(n1-1)) +v2**2.0/(n2*n2*(n2-1))
# 	df = num/den
#         print('Two Sided  Unequal Vars Welch T-test.  df=',df,'T-value',t,'p-value=',p)




if __name__ == '__main__':
    print('95 % Confidence Interval of 100 trials, 0 success ', confIntervalOfPercentage(0,100))
    print('95 % Cofidence Upper Bound on Percentage. 100 trials, 0 success: ', upperBoundForPercentage(0,100))
    print('95 % Confidence Lower Bound on Percentage. 100 trials, 0 success: ', lowerBoundForPercentage(0,100))
    print('Probability that True Percentage is larger than 1%. 100 trials, 0 success: ', probPercentageLargerThanThreshold(0,100,1))
    print('Probability that True Percentage is smaller than 1%. 100 trials, 0 success: ', probPercentageSmallerThanThreshold(0,100,1))
