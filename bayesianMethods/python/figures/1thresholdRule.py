# Power Analysis for OID project
import numpy as np
from scipy.stats import norm
from matplotlib import pyplot as plt

def get_n(mu_1,mu_2,sigma,alpha,beta):
    z_1  = norm.ppf(alpha)
    z_2 = norm.ppf(1-beta)
    n = (sigma*(z_2 - z_1)/(mu_2-mu_1))**2
    theta = mu_1 - z_1*sigma/np.sqrt(n)
    return(n,theta)


# Percentages smaller than mu are unnaceptable
def plotClassicalRegions(mu_1,mu_2,sigma_n,theta,x_min,x_max):
    x = np.arange(x_min,x_max,(x_max-x_min)/1000)
    y = norm.cdf((x-theta)/sigma_n)
    plt.plot(x,y,'k')
    plt.plot((mu_1,mu_1),(0,1),'r--')
    plt.plot((mu_2,mu_2),(0,1),'r--')
    plt.ylabel(r'Prob Accept $H_2$')
    plt.xlabel('True Percentage')
    plt.text(75,0.5,r'$H_1 True$')
    plt.text(80.5,0.5,r'$\mu_1=80$')
    plt.text(94.5,0.5,r'$H_2$ True')
    plt.text(90.5,0.5,r'$\mu_2=90$')
    plt.plot((theta,theta),(0,1),'c--')
    plt.text(86.5,0.9,r'$\theta=86$')
    plt.text(75,0.15,r'$\alpha=0.1$')
    plt.plot((75,82),(alpha,alpha))
    plt.text(92,0.82,r'$1-\beta=0.8$')
    plt.plot((88,95),(1-beta,1-beta))



def plotBayesRegions(mu_1,mu_2,sigma_n,theta,x_min,x_max):
    x = np.arange(x_min,x_max,(x_max-x_min)/1000)
    y1 = norm.cdf((mu_1-x)/sigma_n)
    plt.plot(x,y1,'b',label=r'$P(H_1)$')
    y2 = 1- norm.cdf((mu_2-x)/sigma_n)
    plt.plot(x,y2,'r',label=r'$P(H_2$)')
    plt.plot(x,1-y1-y2,'y',label=r'$1-P(H_1)-P(H_2)$')
    plt.ylabel('Posterior Probability')
    plt.xlabel('Empirical Percentage')
    plt.plot((theta,theta),(0,1),'c--')
    plt.text(86.5,0.9,r'$\theta=86$')
    plt.plot((80,87),(alpha,alpha))
    plt.text(80,0.15,r'$\alpha=0.1$')
    plt.plot((85,90),(beta,beta))
    plt.text(88,0.25,r'$\beta=0.2$')
    plt.text(80,0.8,r'$H_1$ Chosen')
    plt.text(87,0.8,r'$H_2$ Chosen')
    plt.legend()


# upper limit of the unnaceptable region
mu_1= 80
# lower limit of the unnaceptable region
mu_2 = 90
# type I error
alpha = 0.05
# type II error
beta = 0.1

sigma = np.sqrt(mu_1*(100-mu_1))
n,theta = get_n(mu_1,mu_2,sigma,alpha,beta)

sigma_n = sigma/np.sqrt(n)

plt.subplot(2,1,2)
plotClassicalRegions(mu_1,mu_2,sigma_n,theta,x_min=70,x_max=100 )
plt.subplot(2,1,1)
plotBayesRegions(mu_1,mu_2,sigma_n,theta,x_min=70,x_max=100 )
plt.show()
