import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt

# functions for Bayesian inference and decision making
# for proportions with a prior with mass in only two options:
# mu_0, mu_1

# 1) given a prior and a sample of size $n$ with $s$ successes
# compute the posterior

# 2) given a prior, hypotheses mu_0, mu_1 and confidence level c
# compute sample size to achieve the desired confidence level

# 3) given a prior, target mu_1  confidence level c and sample size n
# compute value mu_0 for which we can achieve the desired confidence
# with the given sample size


def compute_posterior(q,mu_1,mu_0, s,n):
    #q = prior for mu_1
    # p(mu_1) =q, p(mu_0)=1-q
    # n = sample size
    # s = observed succeesses in the sample
    p1 = q* mu_1**s * (1-mu_1)**(n-s)
    p0 = (1-q)* mu_0**s * (1-mu_0)**(n-s)
    pp = p1/(p1+p0)
    return(pp)

def compute_n(q,mu_1,mu_0, confidence=0.95):
    c = confidence
    num =np.log(c/(1-c))-  np.log(q/(1-q))
    den = mu_1 * np.log (mu_1/mu_0) + (1-mu_1) * np.log((1-mu_1)/(1-mu_0))
    n_1 = num/den
    if (n_1<0): n_1=0
    num =np.log((1-c)/c)-  np.log(q/(1-q))
    den = mu_0 * np.log (mu_1/mu_0) + (1-mu_0) * np.log((1-mu_1)/(1-mu_0))
    n_0 = np.abs(num/den)
    if (n_0<0): n_0=0
    n = q* n_1 + (1-q)*n_0
    if q>= c or  q <= 1-c: n =0
    print('n_1',n_1,'n_0',n_0,'n',n)
    return np.ceil(n)

def compute_lower_bound(q,mu_1,s,n,confidence=0.95,resolution= 1/1000000):
    #ugly. meed to use recursion instead
    m = np.arange(0,mu_1, mu_1*resolution)


    for mu_0 in m:
        pp = compute_posterior(q,mu_1,mu_0,s,n)

        if pp < confidence:
            lowerBound= mu_0- resolution
            if lowerBound<0: lowerBound=0
            return(lowerBound)
            break




mu_1=0.99
mu_0 = 0.95
q = 0.5
c = 0.90
n=  compute_n(q,mu_1,mu_0, c)
print('q',q)
print('first Sample Size',n)
s= mu_1 * n


pp = compute_posterior(q,mu_1,mu_0,s,n)
q= pp
print('q',q)
n= compute_n(q,mu_1,mu_0, c)
print('Second Sample Size',n)
s= mu_1 * n
pp = compute_posterior(q,mu_1,mu_0,s,n)
q= pp
print('q',q)
n= compute_n(q,mu_1,mu_0, c)
print('Third Sample Size',n)

mu_1=0.8
mu_1=1-1/1000000
c=0.95
n=5000
mu_min = 1-2.996/n
resolution = 1/100000000
resolution =1
x= np.arange(mu_min,mu_1,resolution)
s = n*x
q=0.5

for mu in x:
    s = n*mu
    pp =  compute_posterior(q,mu_1,mu,s,n)
    print('pp',pp)
    if (1-pp) <c:
        print('mu',mu,'pp',pp)
        break

# rule of three

n= 5000
mu_1 = 1
mu_0 = 1 - 3/n
q=0.5
pp = compute_posterior(q,mu_1,mu_0,n,n)
k = mu_0**n
pp = 1/(1+k)
# we expect 1-pp = 0.05
# i.e.,  if we reject mu_0 when  p(mu_0) < 0.05
# then the largest mu_0 we can reject is approximately 1-1/3n
print('Rule of 3 mu_0 =', mu_0,'P(mu_0|data)',1-pp)

n=100
s = 100
mu_1= 1
q=0.5

lb=compute_lower_bound(q,mu_1,s,n)
print('n',n,'s',s,'mu_1',mu_1,'mu_0',lb)

n=100
s = 100
mu_1= 0.8
q=0.5

lb=compute_lower_bound(q,mu_1,s,n)
print('n',n,'s',s,'mu_1',mu_1,'mu_0',lb)
