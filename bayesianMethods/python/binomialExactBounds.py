from scipy.stats import binom
from scipy.stats import beta
import numpy as np

def find_smallest_p(xm, n, alpha=0.05):
    """
    Find the smallest value of p such that P(X > xm | p) <= alpha for a binomial distribution.

    Parameters:
    - xm: The observed value.
    - n: The number of trials.
    - alpha: The significance level (default 0.05).

    Returns:
    - The smallest value of p that meets the condition.
    """
    # Start with a guess for p
    p_min = 0.0
    p_max = 1.0
    tolerance = 1e-9  # Set a tolerance for the search

    while p_max - p_min > tolerance:
        p_mid = (p_min + p_max) / 2
        # Calculate P(X > xm | p_mid)
        p_val = 1 - binom.cdf(xm-1, n, p_mid)
        print('lambda',p_mid,'p(X>x| lambda)',p_val)

        if p_val > alpha:
            # If the condition is not met, decrease  p
            p_max = p_mid
        else:
            # If the condition is met, increae  p
            p_min = p_mid

    return p_mid




def find_largest_p(xm, n, alpha=0.05):
    """
    Find the largest value of p such that P(X <leq xm | p) <= alpha for a binomial distribution.

    Parameters:
    - xm: The observed value.
    - n: The number of trials.
    - alpha: The significance level (default 0.05).

    Returns:
    - The smallest value of p that meets the condition.
    """
    # Start with a guess for p
    p_min = 0.0
    p_max = 1.0
    tolerance = 1e-9  # Set a tolerance for the search

    while p_max - p_min > tolerance:
        p_mid = (p_min + p_max) / 2
        # Calculate P(X > xm | p_mid)
        p_val =  binom.cdf(xm, n, p_mid)
        print('theta',p_mid,'p(X<=x| theta)',p_val)

        if p_val > alpha:
            # If the condition is not met, decrease  p
            p_min = p_mid
        else:
            # If the condition is met, increae  p
            p_max = p_mid

    return p_mid




# Example usage
xm = 10 # Example observation
n = 10  # Number of trials
alpha = 0.05  # Significance level

print('LOWER BOUNDS')
# Find the smallest p
p = find_smallest_p(xm, n, alpha)
print(f"The smallest value of p such that P(X > {xm} | p) <= {alpha} is approximately {p:.8f}.")
#now lets use the Clopper-Pearson theorem

pcp = beta.ppf(alpha,xm,n-xm+1)
print(f"The Clopper-Pearson solution is  {pcp:.8f}.")

pu = beta.ppf(alpha,xm+1,n-xm+1)
print(f"The Bayesian Uniform Prior Solution is  {pu:.8f}.")


pj = beta.ppf(alpha,xm+1/2,n-xm+1/2)
print(f"The Bayesian Jeffrey Prior Solution is  {pj:.4f}.")



print('UPPER BOUNDS')
# Find the smallest p
p = find_largest_p(xm, n, alpha)
print(f"The largest value of p such that P(X <= {xm} | p) <= {alpha} is approximately {p:.8f}.")
#now lets use the Clopper-Pearson theorem

pcp = beta.ppf(1-alpha,xm+1,n-xm)
print(f"The Clopper-Pearson solution is  {pcp:.8f}.")


pu = beta.ppf(1-alpha,xm+1,n-xm+1)
print(f"The Bayesian Uniform Prior Solution is  {pu:.8f}.")


pj = beta.ppf(1-alpha,xm+1/2,n-xm+1/2)
print(f"The Bayesian Jeffrey Prior Solution is  {pj:.4f}.")
