# test the convergence of mixture of Gaussians log likelihood function.
# start at a point x and do gradient descent of negative likelihood.
import numpy as np

# number of mixtures
m = 3
# number of dimensions
d= 1
# Columns of mu are the means of each mixture.
mu = np.random.normal(size=(d,m))
mu[0,0]= -1.5
#mu[1,0] =1
mu[0,1] = 2.0
mu[0,2] = 0.0
#mu[1,1] = -1
# assume equal diagonal covariance matrix for all mixtures
beta =1.0

# potential energy function
def V(x):
    # query vector
    #import pdb;  pdb.set_trace()
    v  = np.matmul(np.transpose(mu), x)
    v = np.exp(beta* v)/(m+0.0)
    v= - np.log(np.sum(v))/beta
    v= v + 0.5*np.matmul(np.transpose(x),x)
    return np.squeeze(v)

def gradV(x):
    # query vector
    u  = np.exp(beta*np.matmul(np.transpose(mu),x))
    p = u/np.sum(u)
    y = - np.matmul(mu,p)
    y = y + x
    return y



def update(x):

    u  = np.exp(np.matmul(np.transpose(mu),x))
    p = u/np.sum(u)
    y = np.matmul(mu,p)
    return y

x = np.random.normal(size=(d,1))
x[0,0]=0.1
#x[1,0]=1.0

nSteps=10
for k in range(nSteps):
    vold = V(x)
    print x
    x = update(x)

    vnew = V(x)
    print '<',vold - vnew,'>'

l1 = 6
u = np.arange(-l1,l1,0.001)
v = np.array(u)
gv = np.array(u)
k=0
for u_ in u:
    v[k] = V([[u_]])
    gv[k] = gradV([[u_]])
    k+=1

from matplotlib import pyplot as plt
plt.plot(u,gv)
plt.plot([-l1, l1],[0,0])
plt.plot([mu[0,0],mu[0,0]],[min(gv),max(gv)])
plt.plot([mu[0,1],mu[0,1]],[min(gv),max(gv)])
plt.show(block=False)
plt.figure()
plt.plot(u,v)
plt.plot([mu[0,0],mu[0,0]],[min(v),max(v)])
plt.plot([mu[0,1],mu[0,1]],[min(v),max(v)])
plt.show(block=False)
plt.figure()
plt.plot(u,np.exp(-v))
plt.plot([mu[0,0],mu[0,0]],[min(np.exp(-v)),max(np.exp(-v))])
plt.plot([mu[0,1],mu[0,1]],[min(np.exp(-v)),max(np.exp(-v))])





muprep = mu

mu = muprep[:,0]
v0 = np.array(u)
v1 = np.array(u)
v2 = np.array(u)
k=0
for u_ in u:
    v0[k] = V([[u_]])
    k+=1

mu = muprep[:,1]

k=0
for u_ in u:
    v1[k] = V([[u_]])
    k+=1

mu = muprep[:,2]
k=0
for u_ in u:
    v2[k] = V([[u_]])
    k+=1


plt.plot(u,np.exp(-v0),'--r')
plt.plot(u,np.exp(-v1),'--r')
plt.plot(u,np.exp(-v2),'--r')
plt.show(block=False)
