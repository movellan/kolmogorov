\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}
\usepackage{bbm}



\newcommand\argmin{\operatornamewithlimits{argmin}}

\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand\given{\medspace|\medspace}
\newcommand\argmax{\operatornamewithlimits{argmax}}


 \title{Variational Encoders}
\author{ Javier R. Movellan\\ 7/24/2022}




\newenvironment{namelist}[1]{%
\begin{list}{}
	{ 		\let\makelabel\namelistlabel
\skettowidth{\labelwidth}{#1}
\setlength{\leftmargin}{1.1\labelwidth} 	} }{%
\end{list}}

\empty

% Replace with the directory where the graphs have been placed
\newcommand{\graphdir}{./graphs}
\newcommand{\centerfig}[2]{\centerline{\psfig{figure=#1,width=#2}}}




\begin{document}
\maketitle






\newpage
Let $Y = f_\theta(Z) + W$ where $f_\theta$ is a family of functions parameterized by $\theta$, e.g., a deep network, $Z\sim N(0,I)$  is a zero mean unit covariance Gaussian random vector and $W\sim N(0, cI)$ where $c>0$ is a scalar and $I$ the identity matrix. Both $Z$ and $W$ are independent of the other random variables in the model.   We want to  find values of $\theta$ that best approximate a target  distribution for $Y$. This can be approached by maximizing the likelihood of samples $y$ obtained from the target distribution of $Y$. Using the law of total probability we get 
\begin{align}
p(y\given \theta) =  \int p(z) p(y \given z, \theta) dz
\end{align}
We maximize  $p(y\given \theta)$ using gradient ascent
\begin{align}
\nabla_\theta \log  p(y\given \theta) &= \frac{1}{p(y \given \theta)} \int p(z)   p(y\given z,\theta) \nabla_\theta \log p(y\given z,\theta) dz \nonumber\\
&= \int p (z\given y,\theta)  \nabla_\theta\log  p(y\given z,\theta) dz 
\end{align}
We could do a stochastic approximation to the gradient by getting samples $z$  from $p(z\given y,\theta)$. Unfortunately in many cases of interest  $p(z\given y,\theta)$ is not analytically tractable and it may not be possible to sample from it.  Variational approaches provide a solution to this  problem. Instead of sampling from $p(z \given y,\theta)$ we  sample from a tractable approximation  $q(z \given y, \phi)$. We measure the goodness of the approximating distribution using the KL divergence
\begin{align} 
D[q(Z\given y, \phi), p(Z\given y, \theta) ]= \int q(z \given  y, \phi) \log \frac{q(z\given  y, \phi)}{p(z\given y,\theta)} dz
\end{align}
After some algebra 
\begin{align} 
D[q(Z\given y,\phi), p(Z\given y,\theta)] &= \int q(z \given y,\phi) \log \frac{q(z\given y,\phi) p(y\given \theta) }{p(z, y\given \theta)} dz \nonumber\\
&= \log p(y \given \theta) -  L(\theta,\phi)
\end{align}
where
\begin{align}
L(\theta,\phi) =& \int q(z \given y,\phi) \log \frac{ p(z,y\given \theta)}{q(z \given y,\theta)} dz \nonumber \leq \log p(y\given \theta)
\end{align}
is known as the Evidence Lower Bound (ELBO). The inequality follows from the fact that the  KL divergence is non-negative. 

\subsection{Prior Divergence Approach}
Note 
\begin{align}
D[q(Z\given y,\phi), p(Z)] = \int q(z\given y,\phi) \log \frac{q(z\given y,\phi)}{p(z)} dz
\end{align}
Thus
\begin{align}
L(\theta,\phi) &= \int q(z \given y,\theta) \log \frac{p(z,y\given \theta)}{q(z \given y,\phi)} dz \nonumber\\
&= \int q(z \given y,\phi) \log p(y \given z, \theta) dz \nonumber\\
&- D[q(Z\given y,\phi), p(Z)]
\end{align}
The first element in the right hand side measures goodness of fit, i.e., whether the network produces the desired response $y$. Note the distribution of $Y$ given $z, \theta$ is Gaussian with mean $f_\theta(z)$ and covariance matrix $cI$. Thus
\begin{align}
 \log p(y \given z,\theta) = -\frac{1}{2c} \| y - f_\theta(z)\|^2 + K
\end{align}
where $K$ is a constant.    The prior divergence $D[q(Z\given y,\phi), p(Z)]$  can be seen as a regularization term that measures how far the approximating distribution is from the prior distribution of $Z$.   If the approximating distribution of $Z$ is Gaussian
\begin{align}
q(z \given y,\phi) = \frac{1}{\sqrt{2\pi|\sigma(y,\phi)|}} e^{-\frac{1}{2} ( x - \mu(y,\phi))' \sigma^{-2} (y,\phi) (x-\mu(y,\phi))}
\end{align}
where $\mu(y,\phi),\sigma^2(y,\phi)$ are parameterized functions  for the expected value and diagonal covariance matrix. It is easy to show that in this case:
\begin{align}
D[q(Z\given y,\phi), p(Z)] =\frac{1}{2}   \sum_{i} \left( 1 - \log \sigma^2_i(y,\phi) + \mu^2_i(y,\phi) + \sigma^2_i(y,\phi)\right)
\end{align}
which is minimized when $\sigma^2_i =1$ and $\mu_i=0$ for all $i$. Putting it all together we get
\begin{align}
L(\theta,\phi) = &- \frac{1}{2c} \int q(z \given y,\phi) \| y - f_\theta(z)\|^2  dz \nonumber\\
&- \frac{1}{2} \sum_{i}  \left( 1 - \log \sigma^2_i(y,\phi) +\mu^2_i(y,\phi) + \sigma^2_i(y,\phi)\right)\nonumber\\
&+K
\end{align}

The first term measures how well we reconstruct $y$ in response to $z$. The second term is a regularizer that encourages $\sigma^2_i(y,\phi)=1$, $\mu_i(y,\phi)=0$. The parameter $c$ weights the relative importance of the data fit term and the regularization term. We optimize $L$ with respect to $\theta,\phi$ using stochastic gradient ascent. 

\paragraph{Gradient with respect to $\theta$}
The gradient with respect to $\theta$ is straight forward
\begin{align}
\nabla_\theta L(\theta, \phi) &= -\frac{1}{2c}   \int q(z\given y,\phi) \nabla_\theta \| y - f_\theta(z)\|^2  dz \nonumber\\
&=  \frac{1}{c} \int q(z\given y,\phi) (y - f_\theta(z))  \nabla_\theta f_\theta(z)
\end{align}
which can be approximated using random samples from $z$ from $q(z\given y,\phi)$
\begin{align}
\nabla_\theta L(\theta, \phi) &\approx \frac{1}{c} (y - f_\theta(z))  \nabla_\theta f_\theta(z)
\end{align}

\paragraph{Gradient with respect to $\phi$:}
To get the gradient of the data fit term with respect to $\phi$ we  use the change of variables rule for expected values, aka the reparameterization trick. Let
\begin{align}
Z = \sigma(y,\phi) U + \mu(y,\phi)
\end{align}
where $U$ is a zero mean unit covariance Gaussian vector. Note that this produces the desired distribution of $Z$ under $q(\cdot\given y,\phi)$. Using the change of variables rule for expected values we get 
\begin{align}
 -\frac{1}{2c}  \int q(z \given y,\phi)  \| y - f_\theta(z)\|^2  dz = -\frac{1}{2c}  \int q(u)  \| y - f_\theta(z)\|^2 du
\end{align}
where $z=\sigma(y,\phi) U + \mu(y,\phi)$. Thus, using the chain rule
\begin{align}
 -\frac{1}{2c}  \nabla_\phi \int q(z \given y,\phi)  \| y - f_\theta(z)\|^2  dz 
= &  \frac{1}{c} \int q(u) (y - f_\theta(z))  \nabla_\phi z  \nabla_z \;f_\theta(z)    \; du
\end{align}
The gradient can be approximated taking random samples $u$ from $q(u)$, 
\begin{align}
-\frac{1}{2c}  \nabla_\phi    \int q(z \given y,\phi)  \| y - f_\theta(z)\|^2  dz \approx 
 \frac{1}{c}  ( y - f_\theta(z))\; \nabla_\phi z \;\nabla_ z   \;f_\theta(z)   
\end{align}
{\bf Summary:}
 We sample $u$ from $U$, a zero mean unit covariance Gaussian vector. We  let $z = \sigma(y,\phi) u + \mu(y,\phi)$, compute the gradients
 \begin{align}
\nabla_\theta L(\theta,\phi) \approx& (y - f_\theta(z))  \nabla_\theta f_\theta(z)\\
\nabla_\phi L(\theta,\phi) \approx & \nabla_\phi z \;\nabla_ z  f_\theta(z)  ( y - f_\theta(z)) \nonumber\\
& -\frac{1}{2} \sum_i \nabla_\phi \left( 1 - \log \sigma^2_i(y,\phi) +\mu^2_i(y,\phi) + \sigma^2_i(y,\phi)\right)
\end{align}
an update $\theta, \phi$ proportionally to the sample gradient. 

\subsection{Jacobian Approach}
If the prior divergence does not have an analytical form, we can use the following expression for the ELBO
\begin{align}
L(\theta,\phi) &= \int q(z\given y,\phi) \log p(y,z\given \theta) dz - \int q(z\given y,\phi)\log q(z\given y,\phi) dz\nonumber\\
&= \int q(u) \log p(y,z\given \theta) du - \int q(u) \log q(z\given y,\phi) du\\
z &= \mu(y,\phi) + \sigma(y,\phi) u 
\end{align}
If $\sigma$ is invertible, $u = \sigma^{-1}(y,\phi) (z- \mu(y,\phi))$ and 
\begin{align}
q(z\given y, \phi) = q(u) \frac{1}{|\sigma(y,\phi)|} 
\end{align}
Thus
\begin{align}
L(\theta,\phi) = &\int q(u) \log p(y\given z,\theta)  du\nonumber\\ 
&+ \int q(u)\Big( \log p(z \given \theta) + \log  | \sigma(y,\phi)|\Big) du + K 
\end{align}
where
\begin{align}
z =& \mu(y,\phi) + \sigma(y,\phi) u 
\end{align}
The first term 
\begin{align}
\int q(u) \log p(y\given z,\theta)  du
\end{align} 
is a data fit term, the second term, 
\begin{align}
\int q(u)\Big( \log p(z \given \theta) + \log  | \sigma(y,\phi)|\Big) du
\end{align}  is a parameter regularization term, and $K=\int q(u) \log q(u) du$ is a constant we can ignore. 


\paragraph{Gradient with respect to $\theta$:}
The gradient with respect to $\theta$ is the same as in the Prior Divergence case. Since $p(z,y\given \theta) = p(z) p(y\given z,\theta)$ we get
\begin{align}
\nabla_\theta L(\theta,\phi) = \int q(u) \nabla_\theta \log p(y\given z,\theta) du
= \frac{1}{c}   \int q(u) ( y - f_\theta(z)) \nabla_\theta  f_\theta(z)   du 
\end{align}
which can be approximated taking random samples $u$ from $U$, 
\begin{align}
\nabla_\theta \hat L(\theta,\phi) = -\frac{1}{c}    ( y - f_\theta(z))\nabla_\theta f_\theta(z)
\end{align}
where  $z= \mu(y,\phi)+\sigma(y,\phi)u$. 
\paragraph{Gradient with respect to $\phi$:}
\begin{align}
\nabla_\phi L(\theta, \phi) =\int q(u)\left( \nabla_\phi \log p(z) + \nabla_\phi \log p(y\given z, \theta)+ \nabla_\phi \log |\sigma(y,\phi)| \right)\; dz \
\end{align}
Using the chain rule
\begin{align}
\nabla_\phi \log p(z) = \nabla_\phi z \nabla_z \log p(z)  = -( \nabla_\phi z )\:z
\end{align}
\begin{align}
\nabla_\phi \log p(y\given z,\theta)  &= \nabla_\phi z \nabla_z f_\theta(z) \nabla_{f_{\theta}} \log p(y\given z,\theta) \nonumber\\
&= -\frac{1}{c} (\nabla_\phi z )(\nabla_z f_\theta(z))  (y - f_\theta(z))
\end{align}
\begin{align}
\nabla_\phi \log |\sigma(y,\phi)| =  \sum_i \nabla_\phi \log \sigma_{i,i}(y,\phi)   
\end{align}
where we used the fact that  $\sigma(y,\phi)$ is diagonal.


\section{Analytical Example}
Let $Y = f_\theta(Z) + W = \theta + Z + W$ where $ Z \sim \mathcal{N}(0,\sigma^2)$, $W \sim \mathcal{N}(0,\epsilon^2)$. Given a sample $y$ of $Y$ we want to find the value of $\theta $ that maximizes $p(y\given \theta)$
Note that $Y,Z$ are jointly Gaussian with the following statistics
\begin{align}
&E[Y] = \theta\\
&Var[Y] = \sigma^2+\epsilon^2\\
&E[Z] = 0\\
&Var[Z] = \sigma^2\\
&Cov(Y,Z) = \sigma^2
\end{align}
Using the properties of jointly Gaussian variables we get that $Y$ and $Z$ are also Gaussian when conditioned on each other, with the following statistics
\begin{align}
&E[Y\given z,\theta] =  \theta +z\\
&Var[Y\given z,\theta] = \epsilon^2\\
&E[Z\given y,\theta] = \frac{\sigma^2}{\sigma^2+\epsilon^2}(y - \theta)\\
&Var[Z\given y,\theta] = \sigma^2 \frac{\sigma^2}{\sigma^2+\epsilon^2}
\end{align}
For the variational approximation we will use
\begin{align}
q(z\given y,\phi) = g(x \given \phi_1 y + \phi_2 ,\phi_3)
\end{align}
Thus the variational model looks as follows
\begin{align}
Z =& \phi_1 Y + \phi_2 + \phi_3 U\\
Y =& Z + \theta + W
\end{align}
where $U \sim N(0,\epsilon^2)$, independent of $W$. To simplify the analysis we will assume the correct values of $\phi_2$, $\phi_3$ are known and focus on learning $\phi_1$
\begin{align}
\phi_2 = -\theta \frac{\sigma^2}{\sigma^2+\epsilon^2}\\
\phi_3 = \sigma \sqrt{\frac{\sigma^2}{\sigma^2+\epsilon^2}}
\end{align}
\paragraph{Prior Divergence Approach}
The variational loss looks as follows
\begin{align}
L(\theta,\phi) = \int q(z \given y,\phi)  \log p(y \given z,\theta) dz - K[ \ q(Z \given y, \phi), p(Z)]
\end{align}
Thus
\begin{align}
\nabla_\theta L(\theta,\phi) &= \frac{1}{ \epsilon^2}  \int q(y \given z,\phi) (y - \theta -z) dz \\ \nonumber 
&=  \frac{1}{\epsilon^2} (y-\theta) -   \frac{1}{\epsilon^2} \int q(z \given y,\phi) z dz \nonumber\\
&= \frac{1}{\epsilon^2} \left(y-\theta -\phi_1 y -\phi_2 \right)
\end{align}
Setting the gradient to zero
\begin{align}
\theta = y(1-\phi_1 ) - \phi_2
\end{align}


 For the gradient with respect to $\phi_1$ we use the reparameterization trick

\begin{align}
 &\int q(z \given y,\phi)  \log p(y \given z,\theta) dz
 =\int  g(u \given 0,\epsilon^2)  \log p(y \given z,\theta) du \\
&z = \phi_1 y  + \phi_2 + \phi_3 u
 \end{align}
 Note
\begin{align}
\nabla_{\phi_1} \log p(y \given z,\theta) &=\nabla_{\phi_1} z  \nabla_z \log g(y \given \theta + z, \epsilon^2)  \\
&= \nabla_{\phi_1}  z \frac{1}{\epsilon^2} (y - \theta -x) =  y  \frac{1}{\epsilon^2} (y - \theta -z)
 \end{align}
 Thus
 \begin{align}
\nabla_{\phi_1}& \int q(z \given y,\phi)  \log p(y \given x,\theta) dx  \nonumber\\
&=  \frac{1}{\epsilon^2} y   \left(y (1-\phi_1) - \theta  - \phi_2 \right) =0
\end{align}
where we used the  fact that $\theta = y(1-\phi_1 ) - \phi_2$ 

Regarding the Divergence
\begin{align}
K[ \ q(Z \given y, \phi), p(Z)] = \log \frac{\sigma^2}{\phi_3^2} + \frac{ \phi_3^2 (\phi_1y +\phi_2)^2}{2 \sigma^2} -\frac{1}{2}
\end{align}
Thus
\begin{align}
\nabla_{\phi_1} K[ \ q(Z \given y, \phi), p(Z)] =   \frac{ \phi_3^2 (\phi_1y +\phi_2)}{ \sigma^2} y
\end{align}
Setting the gradient to zero
\begin{align}
\phi_1 = - \frac{\phi_2}{y}
\end{align}
and
\begin{align}
\theta &= y(1-\phi_1 ) - \phi_2 =  y(1+\frac{\phi_2}{y} ) -\phi_2  =y\\
\phi_1 &= -\frac{\phi_2}{y} = \frac{\sigma^2}{\sigma^2+\epsilon^2}
\end{align}
\paragraph{Jacobian Approach}
The gradient with respect to $\theta$ is the same as in the previous case
\begin{align}
\theta = y(1-\phi_1) - \phi_2
\end{align}
Regarding the gradient with respect to $\phi_1$
We have
\begin{align}
&Z = \phi_1 Y + \phi_2 + \phi_3 U\\
&f_\theta(Z) = \theta + Z\\
&J_u z = \phi_3
\end{align}
Thus
\begin{align}
&\nabla_{\phi_1} z = y\\
&\nabla_z f_\theta(z) = 1\\
\end{align}
\begin{align}
&\nabla_{\phi_1} \log p(z) = -(\nabla_{\phi_1} z) z = -\frac{1}{\sigma^2} y z\\
&\nabla_{\phi_1} \log p(y \given z,\theta) = -\frac{1}{\epsilon^2} \nabla_{\theta_1} z \nabla_z f_\theta(z) ( y - f_\theta(z)) = \frac{1}{c} y (y - \theta -z)\\
&\nabla_{\phi_1} \log |J_u z| = 0
\end{align}
Note
\begin{align}
\int q(u) \nabla_{\phi_1} \log p(y \given z,\theta) du &= \frac{1}{c} y(y - \theta - \phi_1 y - \phi_2) \nonumber\\
&=\frac{1}{c} y(y - y( 1- \phi_1) + \phi_2 - \phi_1 y - \phi_2) = 0 
\end{align}
and 
\begin{align}
\int q(u) \nabla_{\phi_1} \log p(z) du = - \frac{1}{\sigma^2}  y( \phi_1 y + \phi_2) 
\end{align}
Setting the gradient to zero we get 
\begin{align}
&\phi_1 = - \frac{\phi_2}{y}\\
&\theta = y(1- \phi_1) - \phi_2 = y
\end{align}
\bibliographystyle{apalike} % plainnat                                                        
\bibliography{vae}

\end{document}
\section{Appendix}
\paragraph{Note}
If we make $q(z\given x,y,\theta) = p(z)$ then
\begin{align}
D[p(Z), p(Z\given x,y,\theta)]= \log p(y\given x,\theta) - \int p(z) \log p(y\given x,z,\theta) dz 
\end{align}
From which it follows
\begin{align}
\log \int p(z) p(y\given x,z,\theta) dz =  \int p(z) \log p(y\given x,z,\theta) dz +D[p(Z), p(Z\given x,y,\theta)]
\end{align}


\paragraph{Note}

If we If we make $q(z\given x,y,\theta) = p(z\given x,y,\theta)$ then
\begin{align}
 L(x,y,\theta) =& \int p(z \given x, y,\theta) \log \frac{ p(z,y\given x,\theta)}{p(z \given x,y,\theta)} dz\nonumber\\
=& \int p(z \given x, y,\theta) \log \frac{ p(y\given x,\theta) p(z\given x,y,\theta)}{p(z \given x,y,\theta)} dz \nonumber\\
&= \log p(y\given x,\theta) 
\end{align}

\bibliographystyle{apalike} % plainnat                                                        
\bibliography{vae}



\end{document}
