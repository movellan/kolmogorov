% Example Matlab Code for Sequential Least Squares
% Javier R. Movellan. Feb 2010
clear

p = 10; % The dimensionality of the imput
T = 1000; % The number of time steps
gamma = 0.99;
alpha = (1-gamma)/gamma;

trueTheta = (1:p)';
c = randn(p,p);
x = c*randn(p,T);
y = x'*trueTheta;



theta= zeros(p,T); % our running estimate of theta;

fxxinv = eye(p); % Initial value of the fxxInv matrix
fxy = zeros(p,1); % Initial value of the fxy vector
theta(:,1) = fxxinv*fxy; 


for t=2:T
  xt = x(:,t);
  zt = fxxinv*xt;
 
  fxy= (1-gamma)*xt*y(t)+ gamma*fxy;
  
  fxxinv = (fxxinv - (alpha/(1+ alpha*xt'*zt))*zt*zt')/gamma;
  
  theta(:,t) = fxxinv*fxy; 
end
plot(theta')
xlabel('Trial Number');
ylabel( '\theta');
