clear
%clf

horizontalFaceSize=48; % in inches
% median for men is 5.7 in. for women 5.4 
horizontalFaceSize=6; % in inches
horizontalFieldOfView= 72; % in degrees logitechc920


minimumFaceSize=48; % in pixels
horizontalResolution=[640; 1280; 1920];  % in pixels (1080p resolution)

distance=24:1:12*25; % in inches


n=length(distance);
for r=1:length(horizontalResolution)
  for k=1:n
    alpha = atan((horizontalFaceSize/2)/distance(k));
    alpha=2*alpha*180/pi;
    hPixels(r,k)= horizontalResolution(r)*alpha/horizontalFieldOfView;
  end


  hold on


end
  
  plot(distance/12,hPixels(1,:),distance/12,hPixels(2,:),distance/12,hPixels(3,:) )  
  
%plot([0 max(distance)/12],[minimumFaceSize minimumFaceSize],'k--')
plot([0 max(distance)/12],[1280 1280],'k--')
xlabel('Distance In Feet')
ylabel('Face Size in Pixels');
ylabel('Pixels for 48in Horizontal Ruler');
legend('640','720p','1040p')
title('Logitech c920')
set( 0,'DefaultAxesFontSize',18,'DefaultTextFontSize',18)     
set( 0,'DefaultAxesFontName','Times','DefaultTextFontName','Times')
set( 0,'DefaultAxesLineWidth', 2, 'DefaultLineLineWidth', 2);
set( 0,'DefaultLineMarkerSize',2);
cx=-1/12;
x=[35/12 ;3;6;9;(162+5/8)/12;(162+5/8)/12+3];
x=x+cx;
%y=[467c;233;156;105;88];

y=[1279;1237;621;413;277;227];
cy = -2;
y = y+cy;
scatter(x,y,100)