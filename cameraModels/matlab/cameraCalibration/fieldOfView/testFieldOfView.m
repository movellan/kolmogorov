% logitech c920
dFOV=78; % diagonal fov in degrees
sensorAspectRatio= 16/9; % the aspectRatio under which the dFOV was
                         % computed. not the physical sensorAspectRaitio


operatingAspectRatio =16/9;



[hFOV,vFOV]=fieldOfView(dFOV,sensorAspectRatio, ...
				  operatingAspectRatio);


disp(sprintf('Logitech c920'))

disp(sprintf('Horizontal FOV at 16:9 aspect Ratio: %f',hFOV))
disp(sprintf('Vertical FOV at 16:9 aspect Ratio: %f',vFOV))

tolerance = 0.0001;
expectedT(1) = 70.427966;
obtainedT(1) = hFOV;
expectedT(2) = 43.3067;
obtainedT(2) = vFOV;
for k=1:2
  if abs(expectedT(k) - obtainedT(k) ) < tolerance
    disp(sprintf('Test %d Passed', k));
  else
    display(sprintf('Test %d Failed',k));
  end
end


% logitech c910
dFOV=83; % diagonal fov in degrees
sensorAspectRatio= 4/3; % the aspectRatio of the Physical Sensor
operatingAspectRatio =4/3;



[hFOV,vFOV]=fieldOfView(dFOV,sensorAspectRatio, ...
				  operatingAspectRatio);
operatingAspectRatio=16/9;

[hFOV2,vFOV2]=fieldOfView(dFOV,sensorAspectRatio, ...
				  operatingAspectRatio);

disp(sprintf('Logitech c910'))
disp(sprintf('Horizontal FOV at 4:3 aspect Ratio: %f',hFOV))
disp(sprintf('Vertical FOV at 4:3 aspect Ratio: %f',vFOV))

disp(sprintf('Horizontal FOV at 16:9 aspect Ratio: %f',hFOV2))
disp(sprintf('Vertical FOV at 16:9 aspect Ratio: %f',vFOV2))

tolerance = 0.0001;
expectedT(1) = 70.5802;
obtainedT(1) = hFOV;
expectedT(2) = 55.9219;
obtainedT(2) = vFOV;
expectedT(3) = 70.5802;
obtainedT(3) = hFOV2;
expectedT(4) = 41.9414;
obtainedT(4) = vFOV2;
for k=1:4
  if abs(expectedT(k) - obtainedT(k) ) < tolerance
    disp(sprintf('Test %d Passed', k));
  else
    dispt(sprintf('Test %d Failed',k));
  end
end


