% computes vertical and horizontal field of view  based on diagonal
% field of view, sensor aspect ratio and operating aspect ratio
function [hFOV,vFOV]= fieldOfView(dFOV,sensorAspectRatio, ...
				  operatingAspectRatio)


% in order to obtain an aspect ratio of 16/9 using a sensor that is
% 4/3 we need to drop horizontal rows of pixels. this reduces the
% vertical field of iew. 



dFOV=dFOV/2; % half diagonal fov
dFOV = dFOV *pi/180; % convert to radians








d = tan(dFOV); % half diagonal at unitDistance

h = d/sqrt(1+ (1/sensorAspectRatio)^2);


v = h/sensorAspectRatio; % half vertical at unit distance

hFOV = 2*atan(h);
vFOV = 2*atan(v);
vFOV = vFOV*sensorAspectRatio/operatingAspectRatio;

hFOV = hFOV*180/pi;
vFOV = vFOV*180/pi;

