% estimate the focal lenght of the logitec920c camera
% based on a a series of pictures of squares taken at different
% distances from the camera.
% data are the length from the square to the camera lenght and the
% number of pixels taken by a side of the square

clf;clear;

% image resolution in pixels
hPixels=1280;
vPixels=720;
% the logitec920c uses a 1/3" style sensor. This sensor has 4.8 mm
% in widht and 3.6 mm in height. This gives an aspect ratio of
% 4:3. However when taking pictures in 1280x720 the aspect ratio is
% 16:9. This means part of the sensor is not used: The effective
% width is still 4.8 mm but the height is 2.7 mm. 

sensorWidth= 4.8; %mm
sensorHeight= 2.7;% mm

sensorDiag=sqrt(sensorWidth^2+sensorHeight^2);
pixelsPermm= hPixels/sensorWidth;

% physical lenght of the side of the square used as stimulus
squareSide= 27; % in mm


% first column is distance from camera to stimulus in 1/4 inch units
% second is l = width in pixels of the stimulus

load data;
% convet to mm
% one quarter inch is 6.35 mm
data(:,1) = data(:,1)*6.35;
data(:,2) = data(:,2) /pixelsPermm;



% z is the distance from stimulus to  lens. 
% we have a hidden parameter, the distance from lens to focal
% point. we choose the offset parameter that best fit the data

% offset in mm
o = -5:0.01:5;
for k=1:length(o)
z= data(:,1)+o(k); % change to mm
l=data(:,2); % change to mm

% the model  is 
%l = f* s/(z+o)
% were f is the focal length, s is the size of the object 
% z is distance from lens to object
% o is distance from lens to focal point.

% taking logs
% log(l) = log( f*s) - log(z+o)

logz= log(z);
logl = log(l);

% fit a line with slope -1. 
b=-1;
a = mean(logl) - b*mean(logz);


loglHat = a + b*logz;

% focal length
f(k)= exp(a)/squareSide;

mse(k) = norm(logl - loglHat);

o(k)
end
subplot(2,1,1)
plot(o,mse)
xlabel('Offset in mm')
ylabel('mse')


% find value of offset that mins mean square error

[m,k]=min(mse);
oHat=o(k);

z= data(:,1)+oHat; % change to mm
l=data(:,2); % change to mm


logz= log(z);
logl = log(l);
subplot(2,1,2)


scatter(z, l*pixelsPermm)
hold on
% fit a line with slope -1. 
b=-1;
a = mean(logl) - b*mean(logz);


loglHat = a + b*logz;

lHat = exp(loglHat);
fHat= exp(a)/squareSide;

mse(k) = norm(logl - loglHat);

plot(z,lHat*pixelsPermm,'r--')

xlabel('distance to object in  mm')
ylabel('object size in pixels')
display(sprintf('focal length %f mm',fHat));
hfov = atan((sensorWidth/2)/fHat)*2*180/pi;
display(sprintf('horizontal fov at 4:3 aspect ratio  %f degrees',hfov));
vfov=atan((sensorHeight/2)/fHat)*2*180/pi;
display(sprintf('vertical fov at 4:3 aspect ratio  %f degrees',vfov));
dfov=atan((sensorDiag/2)/fHat)*2*180/pi;
display(sprintf('Diagonal fov at 4:3 aspect ratio  %f degrees',dfov));