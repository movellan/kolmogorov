% Bayesian estimation  of scale matrix of a 3D object 
% given 2D projection of points from that object
% and given the widh and height of the vertically oriented
% minimally enclosing rectangle
% Copyright Emotient.com  2013
% Developer Javier R. Movellan
%

clear
clf

%%%%%%%%%% 3D object Model %%%%%%%%%%%%%
% x is horizontal axis, y vertical axis, z the depth axis
% x is a 3 x 3 matrix. The cols of 3 are 3D points. the rows are
% the x,y,z coordinates of the points
x(:,1) = [ 0, 0, 0]'; % the nose. we put the origin at the nose
x(:,2) = [ -51, 45, -43.1999999]'; % the person's left eye. 
x(:,3) = [46.5, 45, -43.19999]'; % the person's right eye

lambda(1) =204.33765/2 % radius of width of head model
lambda(2)= 273.88785/2; % radius of height of head model
lambda(3)=245.2051799/2;; % radius of depth of head model

q = [1 0 0; 0 1 0; 0 0 1]*10; %the prior precission for the mean

%%%%%%%%%% 2D Observations %%%%%%%%%%%%%%%%
l1 = lambda(1)*2; % width of minimally enclosing rectanle
l2 = lambda(2)*2; % height of minimally enclosing rectangle

l1=274;
l2 = 400;

y(:,1) = [0,0]'; % coordinates of the nose on image plane
y(:,2) = [-53.025634765625 34.7179424579327]';
y(:,3)= [56.205134465144255  35.33332707331729]';
%%%%%%%%% Observed 3D Rotation
% we simulate the user going through a range of rotation matrices %

angleStart = -50; % in degrees
angleEnd = 50; % in degrees
deltaAlpha = 0.5; % in degrees 
angleStart = angleStart*2*pi/360;
angleEnd = angleEnd*2*pi/360;
deltaAlpha = deltaAlpha*2*pi/360;

alpha = angleStart:deltaAlpha:angleEnd;
% the 3 Euler angles provided to us by the user 
% we assume an intrinsic ZYX Euler standard
% first we compute the rotation matrix corresponding to the Euler
% angles
u{1}=[0 0 1]; % the first axis (roll) 
u{2}=[0 1 0]; % the second axis (yaw) 
u{3}=[1 0 0]; % the third axis (pitch) 

for k=1:length(alpha) 
  euler{1} = 1*alpha(k); %roll 
  euler{2} = 0*alpha(k);  %yaw
  euler{3} = 0*alpha(k);  %pitch

%  euler{1} = 0; %roll 
%  euler{2} = 25.20040142897212*2*pi/360;  %yaw
%  euler{3} = 0;  %pitch


  for j=1:1:3
    reuler{j} = expm( R(u{j}*euler{j}));  
  end
  % the rotation matrix estimated by the user
  r = reuler{1}*reuler{2}*reuler{3};


  p= [ 1 0 0; 0 1 0]; % projection matrix. Projects 3d object into 2d
		  
  % compute the prior mean based on the minimally enclosing rectangle
  c=[(r(1,1)*lambda(1))^2+(r(1,2)*lambda(2))^2+(r(1,3)*lambda(3))^2; (r(2,1)*lambda(1))^2+(r(2,2)*lambda(2))^2+(r(2,3)*lambda(3))^2];
  L2 = [l1^2;  l2^2];
  m(1,1) =0.5*sqrt(pinv(c'*c)*c'*L2);
  m(2,1) = m(1,1);
  m(3,1) = m(1,1);
  m
  % now we try to estimate theta from the observation of y  
  a=p*r;
  xx=zeros(3,3);
  xy = zeros(3,1);
  aa= a'*a;
  for k2=1:size(x,2)
    xbar = diag(x(:,k2));
    xx = xx+ xbar*aa*xbar;
    xy = xy+ xbar*a'*y(:,k2);
  end
  co=[1 0 ; 0 1; 0 1]; % constraint matrix. sets theta2=theta3
  xxco= co'*xx*co;
  xyco = co'*xy;
  % Here we check that some sanity constraints are satisfied in the
  % obtained solutions. If not, we increase the strenght of the prior
  % until the constraints are satisfied
  priorStrength=1;
  while(1)
    qco= q(1:2,1:2)*priorStrength;
    mco=m(1:2,1);
    thetaHatCo = pinv(qco+xxco)*(qco*mco+xyco);  
    thetaHat = [thetaHatCo(1); thetaHatCo(2); thetaHatCo(2)];
    % sanity conditions. rely more on the mean until they are met
    if thetaHat(1)> 0 && thetaHat(2)>0 && thetaHat(2)/thetaHatCo(1) > 0.5 && thetaHatCo(2)/thetaHatCo(1) < 2
      break
    end
    % increase the strength of the prior if the sanity conditions are
    % not satisfied
    priorStrength = priorStrength*1.5;
  end
  
  thetaHat
%%%%%%%%%%%%%%%%%  Plot Things %%%%%%%%%%%%%%%%%%%%%%
alpha(k)
yhat = a*diag(thetaHat)*x;
eye1(:,k) = yhat(:,2);
eye2(:,k) = yhat(:,3);
nose(:,k) = yhat(:,1);

% let's get the parallel projection of the scaled and rotated
% ellipse
vv = p*r*diag(thetaHat)*diag(lambda);
vv= vv*vv';
vvi = pinv(vv);
[v d] =eig(vvi);
d = sqrt(inv(d));
angle = atan(v(2,1)/v(1,1));
sx = sqrt(vv(1,1));
sy= sqrt(vv(2,2));

subplot(2,1,1)
hold off
scatter(eye1(1,k),eye1(2,k),'r')
hold on
ellipse(d(1,1),d(2,2),angle,0,0,'r')
rh=rectangle('position',[-sx, -sy, 2*sx, 2*sy])
set(rh,'EdgeColor','r')

scatter(eye2(1,k),eye2(2,k),'r')
scatter(nose(1,k),nose(2,k),'k')


plot([eye1(1,k) eye2(1,k)],[eye1(2,k) eye2(2,k)],'k--')
plot([eye1(1,k) eye2(1,k)],[eye1(2,k) eye2(2,k)],'k--')
plot([nose(1,k) eye1(1,k)],[nose(2,k) eye1(2,k)],'b--')
plot([nose(1,k) eye2(1,k)],[nose(2,k) eye2(2,k)],'b--')
scatter(y(1,2:3) ,y(2,2:3),80,'b','filled')
bottomLeft= [ -l1/2; -l2/2];
topRight= [ l1/2;l2/2];

xlim([bottomLeft(1) topRight(1)]*1.5)
ylim([bottomLeft(2) topRight(2)]*1.5)
rectangle('position',[bottomLeft(1) bottomLeft(2) l1 l2])
title('Scaled')
axis equal
hold off
subplot(2,1,2)
yhat = a*x;
eye1(:,k) = yhat(:,2);
eye2(:,k) = yhat(:,3);
nose(:,k) = yhat(:,1);

scatter(eye1(1,k),eye1(2,k),'r')
hold on
scatter(eye2(1,k),eye2(2,k),'r')
scatter(nose(1,k),nose(2,k),'k')


plot([eye1(1,k) eye2(1,k)],[eye1(2,k) eye2(2,k)],'k--')
plot([eye1(1,k) eye2(1,k)],[eye1(2,k) eye2(2,k)],'k--')
plot([nose(1,k) eye1(1,k)],[nose(2,k) eye1(2,k)],'b--')
plot([nose(1,k) eye2(1,k)],[nose(2,k) eye2(2,k)],'b--')

scatter(y(1,2:3) ,y(2,2:3),80,'b','filled')
xlim([bottomLeft(1) topRight(1)]*1.5)
ylim([bottomLeft(2) topRight(2)]*1.5)

rectangle('position',[bottomLeft(1) bottomLeft(2) l1 l2])
title('Unscaled')
axis equal
pause
hold off
sum(thetaHat<0)
end