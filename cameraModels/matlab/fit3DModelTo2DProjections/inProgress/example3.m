%MODEL is wrong on z . eye should have negative z  values, not
%positive

% Infer orientation of a face based on landmark points in a 2D projection
% use deformation parameters for the center of the pupil
clear

clf

% x axis is horizontal points to right side of image
% y axis is vertical points to bottom of image
% z axis is depth, points away from the viewer of the image
% this is so that we can accomodate the pixel coordinate system of
% matlab
% the nose is at the origin. The mouth has positive depth with
% respect to the nose
% Note: 
% positive roll roates the head clockwise
% positve pitch moves rotates the head downwards
% positive yaw rotates the nose towards the left side of the image

%rng(123)
fileName='javi19';




load javiModel

x= javiModel
nPoints =size(x,2);

%%% we use left and right as seen by the viewer so left eye means
%the eye in the left side of the image, the right eye of the face displayed


% construct the feature matrices for each point. 
% 1 2 3 x,y,z scale parameters
% columns 4,5,6 are global shift parameters
% columns 7,8,9 model displacements of left mouth corners
% columns =10,11,12 model displacements of right mouth corners
% columns =13,14,15 model displacements of pupils

nFeatures=15;
for k=1: size(x,2)
  f{k}=zeros(3,nFeatures);
  f{k}(:,1:3)= diag(x(:,k));
  f{k}(:,4:6)= eye(3); % for global shift parameter
end
f{6}(:,7:9)=eye(3); % point 6 is left mouth corner
f{7}(:,10:12)=eye(3); % point 7 is right mouth corner
f{8}(:,13:15)=eye(3); % point 8 is left pupil
f{9}(:,13:15)=eye(3);% displacement of left and right pupil is same





% covariance matrices for the different landmarks
for k=1:nPoints
  q{k}= eye(2);
end




z = randn(3,15); % additional points for the prior distibution over rotations


lambda = 1; % weights the  prior (from zero to 1). The likelihood
              % term is weighted 1-lambda
	      
	      

p= [ 1 0 0; 0 1 0]; % projection matrix

upRightFrame =eye(3); % init rotation
upRightFrame(2,2)=-1;
% coordinates in image space are y down and x to the right 



% note the upright model is define so that the initial rotation is zero
rInit=eye(3); 


v = [0 0 1]';
delta = 0; % in degrees
v = v*delta*pi/180;
rInit = expm(R(v));% do not use upRightFrame. It would be wrong

v = randn(3,1);
v=v/norm(v);
delta = 20; % in degrees
v = v*delta*pi/180;
rInit= rInit*expm(R(v));





rho = rInit; % prior rotation 


for k=1:size(x,2)
[qve{k}, qva{k}] = eig(q{k});
ang{k} = acos(qve{k}(1,2));
end

rh=10;
k2=0;

qSum = zeros(2,2);
for k=1: size(x,2)
  qSum = qSum+q{k}; 
end
qSumInv= pinv(qSum);


% the prior covariance for gamma
 c = eye(nFeatures);
c = pinv(c*c');
c= 0.01*c;

c(1,1)=1000;
c(2,2)=1000;
c(3,3)=1000;
c(6,6)=10000;
c(9,9) =1000; % left mouth corner does not move  in depth
c(12,12) =1000; % right mouth corner does not move  in depth
c(14,14)=1; % we are certain the pupil deos not move vertically
c(15,15)=1000;% we are certain the pupil does not move in depth


% the prior mean for gamma
m = zeros(nFeatures,1);
m(1)=1;
m(2)=1;
m(3)=1;


load(fileName) % gets the 2d projection of landmarks



%%%% 

kappa=0;
[rHat, gammaHat, xHat,yHat,yHatHistory,sse]  = fit(f, z, y, q,  rInit, rho,m,c,kappa);
% %%%%%%%%%% to debug rotation 
% v = [0 1 0];
% delta = 0; % in degrees
% v = v*delta*pi/180;
% rHat=expm(R(v));
% v = [0 1 0];
% delta = 30; % in degrees
% v = v*delta*pi/180;
% rHat= rHat*expm(R(v));
% %%%%%%%%%

%%%%%%%%%% display results %%%%%%%%%%%

I = eye(3);

I(3,3)= -1;

subplot(2,1,1)
hold off

scatter(y(1,:),y(2,:),5,'k','filled')
hold on

scatter(yHat(1,:),yHat(2,:),100,'x','r')


%subplot(2,2,2)







subplot(2,1,2)
plot(sse)

drawnow

figure

[Im,map]= imread(strcat(fileName,'.tiff'));

imshow(Im(:,:,1:3),map)
hold on ;scatter(y(1,:),y(2,:),200)
scatter(yHat(1,:),yHat(2,:),200,'x','r')

IrHat= p*rHat;
muHat = yHat(:,5);
sigmaHat= 50*gammaHat(1);




leftMiddle= [muHat(1)-2*sigmaHat*IrHat(1,1),muHat(2)-2*sigmaHat* ...
	      IrHat(2,1)]
rightMiddle=[muHat(1)+2*sigmaHat*IrHat(1,1),muHat(2)+2*sigmaHat*IrHat(2,1)]

% remember the top of the head has lower value in y axis than
% bottom of the head

topMiddle = [muHat(1)-2.5*sigmaHat*IrHat(1,2),muHat(2)-2.5*sigmaHat* ...
	     IrHat(2,2)]

bottomMiddle=[muHat(1)+2.5*sigmaHat*IrHat(1,2),muHat(2)+2.5*sigmaHat*IrHat(2,2)]



hold on

% % horizontal axis
% plot([muHat(1)-2.5*sigmaHat*IrHat(1,1),  muHat(1)+2.5*sigmaHat*IrHat(1,1)] ...
%      ,[muHat(2)-2.5*sigmaHat*IrHat(2,1), muHat(2)+2.5*sigmaHat*IrHat(2,1)],'k','lineWidth',4)
% plot([muHat(1)-2.5*sigmaHat*IrHat(1,1),  muHat(1)+2.5*sigmaHat*IrHat(1,1)] ...
%      ,[muHat(2)-2.5*sigmaHat*IrHat(2,1), muHat(2)+2.5*sigmaHat*IrHat(2,1)],'r--','lineWidth',1)

% % vertical axis
% plot([   muHat(1)-2.5*sigmaHat*IrHat(1,2),muHat(1)+2.5*sigmaHat*IrHat(1,2)] ...
%      ,[  muHat(2)-2.5*sigmaHat*IrHat(2,2),muHat(2)+2.5*sigmaHat*IrHat(2,2)],'k','lineWidth',4)
% plot([   muHat(1)-2.5*sigmaHat*IrHat(1,2),muHat(1)+2.5*sigmaHat*IrHat(1,2)] ...
%      ,[  muHat(2)-2.5*sigmaHat*IrHat(2,2),muHat(2)+2.5*sigmaHat*IrHat(2,2)],'g--','lineWidth',1)

% depth axis
% we want an arrow starting at tip of nose and in the direction of
% the negative of the depth axis 
% remember the nose points in the direction of negative depth
 plot([muHat(1)+0*sigmaHat*IrHat(1,3),  muHat(1)- 2.5*sigmaHat*IrHat(1,3)] ...
      ,[muHat(2)+0*sigmaHat*IrHat(2,3),  muHat(2)-2.5*sigmaHat*IrHat(2,3)],'k','lineWidth',6)
 plot([muHat(1)+0*sigmaHat*IrHat(1,3),  muHat(1)- 2.5*sigmaHat*IrHat(1,3)] ,[muHat(2)+0*sigmaHat*IrHat(2,3),  muHat(2)-2.5*sigmaHat*IrHat(2,3)],'r--','lineWidth',2)

 
 

plot([   rightMiddle(1)+x(3,1)*IrHat(1,3),rightMiddle(1)+0.0*sigmaHat*IrHat(1,3)] ...
     ,[  rightMiddle(2)+x(3,1)*IrHat(2,3),rightMiddle(2)+0.0*sigmaHat*IrHat(2,3)],'k','lineWidth',4)
plot([   rightMiddle(1)+x(3,1)*IrHat(1,3),rightMiddle(1)+0.0*sigmaHat*IrHat(1,3)] ...
     ,[  rightMiddle(2)+x(3,1)*IrHat(2,3),rightMiddle(2)+0.0*sigmaHat*IrHat(2,3)],'r','lineWidth',1)

plot([   leftMiddle(1)+x(3,1)*IrHat(1,3),leftMiddle(1)+0.0*sigmaHat*IrHat(1,3)] ...
     ,[  leftMiddle(2)+x(3,1)*IrHat(2,3),leftMiddle(2)+0.0*sigmaHat*IrHat(2,3)],'k','lineWidth',4)
plot([   leftMiddle(1)+x(3,1)*IrHat(1,3),leftMiddle(1)+0.0*sigmaHat*IrHat(1,3)] ...
     ,[  leftMiddle(2)+x(3,1)*IrHat(2,3),leftMiddle(2)+0.0*sigmaHat*IrHat(2,3)],'r','lineWidth',1)


plot([   topMiddle(1)+0.6*x(3,1)*IrHat(1,3),topMiddle(1)+0.0*sigmaHat*IrHat(1,3)] ...
     ,[  topMiddle(2)+0.6*x(3,1)*IrHat(2,3),topMiddle(2)+0.0*sigmaHat*IrHat(2,3)],'k','lineWidth',4)
plot([   topMiddle(1)+0.6*x(3,1)*IrHat(1,3),topMiddle(1)+0.0*sigmaHat*IrHat(1,3)] ...
     ,[  topMiddle(2)+0.6*x(3,1)*IrHat(2,3),topMiddle(2)+0.0*sigmaHat*IrHat(2,3)],'r','lineWidth',1)

plot([   bottomMiddle(1)+0.6*x(3,1)*IrHat(1,3),bottomMiddle(1)+0.0*sigmaHat*IrHat(1,3)] ...
     ,[  bottomMiddle(2)+0.6*x(3,1)*IrHat(2,3),bottomMiddle(2)+0.0*sigmaHat*IrHat(2,3)],'k','lineWidth',4)
plot([   bottomMiddle(1)+0.6*x(3,1)*IrHat(1,3),bottomMiddle(1)+0.0*sigmaHat*IrHat(1,3)] ...
     ,[  bottomMiddle(2)+0.6*x(3,1)*IrHat(2,3),bottomMiddle(2)+0.0*sigmaHat*IrHat(2,3)],'r','lineWidth',1)


 
%%%%%%%%%%%%%%%% Plane in front of face centered at tip of nose %%%%%%%%%%5

plot([   rightMiddle(1)-1.0*sigmaHat*IrHat(1,2),rightMiddle(1)+1.0*sigmaHat*IrHat(1,2)] ...
     ,[  rightMiddle(2)-1.0*sigmaHat*IrHat(2,2),rightMiddle(2)+1.0*sigmaHat*IrHat(2,2)],'k','lineWidth',4)
plot([   rightMiddle(1)-1.0*sigmaHat*IrHat(1,2),rightMiddle(1)+1.0*sigmaHat*IrHat(1,2)] ...
     ,[  rightMiddle(2)-1.0*sigmaHat*IrHat(2,2),rightMiddle(2)+1.0*sigmaHat*IrHat(2,2)],'r','lineWidth',1)

plot([   leftMiddle(1)-1.0*sigmaHat*IrHat(1,2),leftMiddle(1)+1.0*sigmaHat*IrHat(1,2)] ...
     ,[  leftMiddle(2)-1.0*sigmaHat*IrHat(2,2),leftMiddle(2)+1.0*sigmaHat*IrHat(2,2)],'k','lineWidth',4)
plot([   leftMiddle(1)-1.0*sigmaHat*IrHat(1,2),leftMiddle(1)+1.0*sigmaHat*IrHat(1,2)] ...
     ,[  leftMiddle(2)-1.0*sigmaHat*IrHat(2,2),leftMiddle(2)+1.0*sigmaHat*IrHat(2,2)],'r','lineWidth',1)

plot([bottomMiddle(1)-1.0*sigmaHat*IrHat(1,1),  bottomMiddle(1)+1.0*sigmaHat*IrHat(1,1)] ...
     ,[bottomMiddle(2)-1.0*sigmaHat*IrHat(2,1), bottomMiddle(2)+1.0*sigmaHat*IrHat(2,1)],'k','lineWidth',4)
plot([bottomMiddle(1)-1.0*sigmaHat*IrHat(1,1),  bottomMiddle(1)+1.0*sigmaHat*IrHat(1,1)] ...
     ,[bottomMiddle(2)-1.0*sigmaHat*IrHat(2,1), bottomMiddle(2)+1.0*sigmaHat*IrHat(2,1)],'r','lineWidth',1)



plot([topMiddle(1)-1.0*sigmaHat*IrHat(1,1),  topMiddle(1)+1.0*sigmaHat*IrHat(1,1)] ...
     ,[topMiddle(2)-1.0*sigmaHat*IrHat(2,1), topMiddle(2)+1.0*sigmaHat*IrHat(2,1)],'k','lineWidth',4)
plot([topMiddle(1)-1.0*sigmaHat*IrHat(1,1),  topMiddle(1)+1.0*sigmaHat*IrHat(1,1)] ...
     ,[topMiddle(2)-1.0*sigmaHat*IrHat(2,1), topMiddle(2)+1.0*sigmaHat*IrHat(2,1)],'r','lineWidth',1)


%%%%%%%%% direction of gaze %%%%%%%%%%%%%%%

%center of right eye

eCenter= (xHat(:,1)+xHat(:,2))/2;
% now move in the direction of the head's depth axis
% we guess radius of eye is approximately 1/2 distance between eye
% corners
eyeRadius = norm(xHat(:,2)-xHat(:,1))/2;
eCenter = eCenter- eyeRadius*rHat*[0;0;1]
gazeEnd = p*(xHat(:,8) +2* (xHat(:,8) - eCenter));
plot([yHat(1,8), gazeEnd(1)], [yHat(2,8),gazeEnd(2)],'r','lineWidth',4)

%left eye

eCenter= (xHat(:,3)+xHat(:,4))/2;
% now move in the direction of the head's depth axis
% we guess radius of eye is approximately 1/2 distance between eye
% corners
eyeRadius = norm(xHat(:,3)-xHat(:,4))/2;
eCenter = eCenter- eyeRadius*rHat*[0;0;1]
gazeEnd = p*(xHat(:,9) +2* (xHat(:,9) - eCenter));
plot([yHat(1,9), gazeEnd(1)], [yHat(2,9),gazeEnd(2)],'r','lineWidth',4)


drawnow
sse(end)