clear; clf
fileName='javi39';

[I,map]= imread(strcat(fileName,'.tiff'));

imshow(I(:,:,1:3),map)

x(:,1) = [-6.1; 7.42; -2.3]; %lateral right eye corner
x(:,2) = [-0.4; 7.42; -2.3]; %medial left eye corner
x(:,3) = [6.1; 7.42; -2.3]; %lateral right eye corner
x(:,4) = [0.4; 7.42; -2.3]; %medial right eye corner
x(:,5) = [0;0;0]; % nose at origin

x(:,6) = [ -3.25; - 2.7; -2.3]; % right mouth corner
x(:,7) = [ 3.25; - 2.7; -2.3]; % left mouth corner
x(:,8) = [-3.25; 7.42; -2.3]; %right eye center of pupil 
x(:,9) = [3.25; 7.42; -2.3]; %left  eye  center of pupil 

ntrials=1;
ymean=zeros(2,9);
for k=1:ntrials
  k
  imshow(I(:,:,1:3),map)
  y{k} = zeros(2,9);
  
  [z1, z2] = ginput(9);
  y{k}  = [z1';z2'];
  ymean=ymean+y{k}/ntrials;
end
for k=1:ntrials
hold on ;scatter(y{k}(1,:),y{k}(2,:))
end
clear y;
y=ymean;

save(fileName,'y');