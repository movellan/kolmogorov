% Infer orientation of a face based on landmark points in a 2D projection
clear
clf
%rng(123)

dBetweenCenterOfEyes=6.5; % in cm
dEyeTipNose = 2.7; % in cm
mouthWidth= dBetweenCenterOfEyes;
fulcrumToTipOfNose= 2.3;% cm; 



x(:,1) = [-6.1; 7.42; -2.3]; %lateral right eye corner
x(:,2) = [-3.25; 7.42; -2.3]; %right eye center of pupil 
x(:,3) = [-0.4; 7.42; -2.3]; %medial left eye corner
x(:,4) = [6.1; 7.42; -2.3]; %lateral right eye corner
x(:,5) = [3.25; 7.42; -2.3]; %left  eye  center of pupil 
x(:,6) = [0.4; 7.42; -2.3]; %medial right eye corner
x(:,7) = [0;0;0]; % nose at origin
x(:,8) = [ -3.25; - 2.7; -2.3]; % right mouth corner
x(:,9) = [ 3.25; - 2.7; -2.3]; % left mouth corner

% covariance matrices for the different landmarks

q{1} = [ 1,  0; 0, 1]/2;
q{2} =[ 4, 0 ; 0, 1]/2;
q{3} = q{1};
q{4} = q{1};
q{5} = q{2};
q{6} = q{1};
q{7}=[ 3, 0; 0, 3;]/2
q{8}= [ 3, 0; 0, 3]/2;
q{9} = q{8}; 



z = randn(3,15); % additional points for the prior distibution over rotations


lambda = 1; % weights the  prior (from zero to 1). The likelihood
              % term is weighted 1-lambda
	      
	      

p= [ 1 0 0; 0 1 0]; % projection matrix

% True rotation matrix r
theta = randn(3,1);
theta=theta/norm(theta);
delta2=0;
theta = theta*rand(1)*delta2*pi/180;
trueR = expm(R(theta));


trueMu = 0*randn(2,1); % shift
muHat = trueMu;

trueSigma=1;
sigmaHat = trueSigma;
% lets rotate and project 
y = p*trueR*x*trueSigma +repmat(trueMu,1,size(x,2)); % The true projections 
trueY=y;
for k=1:size(x,2) 
  y(:,k) = y(:,k) +q{k}*randn(2,1);
  q{k} = pinv(q{k}*q{k}');
end




v = randn(3,1);
v=v/norm(v);
delta = 40; % in degrees
v = v*delta*pi/180;
thetaHat = theta+v;
rHat = expm(R(thetaHat));
rho = trueR; % prior rotation 


for k=1:size(x,2)
[qve{k}, qva{k}] = eig(q{k});
ang{k} = acos(qve{k}(1,2));
%q{k} = eye(2);
end

rh=10;
k2=0;

qSum = zeros(2,2);
for k=1: size(x,2)
  qSum = qSum+q{k}; 
end
qSumInv= pinv(qSum);


[rHat, muHat, sigmaHat, yHat,yHatHistory,sse]  = fit(x, z, y, q, ...
						  rho, rHat,lambda);



I = eye(3);


subplot(2,2,1)
hold off

scatter(y(1,:),y(2,:),'g')
hold on
scatter(trueY(1,:),trueY(2,:),'b')

for k=1:size(x,2)
  ellipse(1/sqrt(qva{k}(2,2)),1/sqrt(qva{k}(1,1)),ang{k},y(1,k), y(2,k),'g');
end
yHat = yHat;
scatter(yHat(1,:),yHat(2,:),'r')

for k=1:size(x,2)
  plot(yHatHistory(:,1,k),yHatHistory(:,2,k),'r--')
end

axis([-15 15 -15 15])

subplot(2,2,2)
IrHat= p*rHat*I;
Ir= p*trueR*I;


plot([muHat(1)-2*sigmaHat*IrHat(1,1),  muHat(1)+2*sigmaHat*IrHat(1,1)] ,[muHat(2)-2*sigmaHat*IrHat(2,1), muHat(2)+2*sigmaHat*IrHat(2,1)],'r--','lineWidth',2)
hold on
plot([muHat(1)-2*sigmaHat*IrHat(1,2),   muHat(1)+2*sigmaHat*IrHat(1,2)] ,[muHat(2)-2*sigmaHat*IrHat(2,2),  muHat(2)+2*sigmaHat*IrHat(2,2)],'g--','lineWidth',2)
plot([muHat(1)-2*sigmaHat*IrHat(1,3),  muHat(1)+ 2*sigmaHat*IrHat(1,3)] ,[muHat(2)-2*sigmaHat*IrHat(2,3),  muHat(2)+2*sigmaHat*IrHat(2,3)],'b--','lineWidth',2)




plot([trueMu(1)-2*trueSigma*Ir(1,1)  trueMu(1)+2*trueSigma*Ir(1,1)] ,[trueMu(2)-2*trueSigma*Ir(2,1)  trueMu(2)+2*trueSigma*Ir(2,1)],'r','lineWidth',2)
plot([trueMu(1)-2*trueSigma*Ir(1,2) trueMu(1)+2*trueSigma*Ir(1,2)] ,[trueMu(2)-2*trueSigma*Ir(2,2) trueMu(2)+2*trueSigma*Ir(2,2)],'g','lineWidth',2)
plot([trueMu(1)-2*trueSigma*Ir(1,3),  trueMu(1)+2*trueSigma*Ir(1,3)] ,[trueMu(2)-2*trueSigma*Ir(2,3), trueMu(2)+ 2*trueSigma*Ir(2,3)],'b','lineWidth',2)


axis([-15 15 -15 15])


hold off


subplot(2,2,3)
plot(sse)
hold off

k2
drawnow
%pause


