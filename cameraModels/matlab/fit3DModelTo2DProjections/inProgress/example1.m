% Object is a cloud of 3d points. We rotate it and project it on
% the image plane. Goal is to figure out the rotation matrix from
% the 2D cloud of points
clear
clf
rng(123)

x= randn(3,5); % Each column is a point in 3d
z = randn(3,15); % additional points for the prior distibution over rotations


lambda = 0.0; % weights the  prior (from zero to 1). The likelihood
              % term is weighted 1-lambda
	      
	      

p= [ 1 0 0; 0 1 0]; % projection matrix

% True rotation matrix r
theta = randn(3,1);
theta=theta/norm(theta);
delta2=90;
theta = theta*rand(1)*delta2*pi/180;
trueR = expm(R(theta));


trueMu = 3*randn(2,1); % shift
muHat = zeros(2,1);

trueSigma=2;
sigmaHat = 0.1;
% lets rotate and project 
y = p*trueR*x*trueSigma +repmat(trueMu,1,size(x,2)); % The true projections 
trueY=y;
y = y+0.0*randn(size(y));




v = randn(3,1);
v=v/norm(v);
delta = 45; % in degrees
v = v*delta*pi/180;
thetaHat = theta+v;
rHat = expm(R(thetaHat));
rho = trueR; % prior rotation 

for k =1: size(x,2)
q{k} = randn(2);
q{k} = q{k}*q{k}'; % generate positive definite marrices
if k<3 
  q{k} = q{k}/5;
end
if k==5
  q{k} = q{k}*5;
end

[qve{k}, qva{k}] = eig(q{k});
ang{k} = acos(qve{k}(1,2));
%q{k} = eye(2);
end

rh=10;
k2=0;

qSum = zeros(2,2);
for k=1: size(x,2)
  qSum = qSum+q{k}; 
end
qSumInv= pinv(qSum);


[rHat, muHat, sigmaHat, yHat,yHatHistory,sse]  = fit(x, z, y, q, ...
						  rho, rHat,lambda);



I = eye(3);


subplot(2,2,1)
hold off

scatter(y(1,:),y(2,:))
hold on
scatter(trueY(1,:),trueY(2,:),'g')

for k=1:size(x,2)
  ellipse(1/sqrt(qva{k}(2,2))/5,1/sqrt(qva{k}(1,1))/5,ang{k},y(1,k), y(2,k));
end
yHat = yHat;
scatter(yHat(1,:),yHat(2,:),'r')

for k=1:size(x,2)
  plot(yHatHistory(:,1,k),yHatHistory(:,2,k),'r--')
end

axis([-6 6 -6 6])

subplot(2,2,2)
IrHat= p*rHat*I;
Ir= p*trueR*I;


plot([muHat(1)-2*sigmaHat*IrHat(1,1),  muHat(1)+2*sigmaHat*IrHat(1,1)] ,[muHat(2)-2*sigmaHat*IrHat(2,1), muHat(2)+2*sigmaHat*IrHat(2,1)],'r--','lineWidth',2)
hold on
plot([muHat(1)-2*sigmaHat*IrHat(1,2),   muHat(1)+2*sigmaHat*IrHat(1,2)] ,[muHat(2)-2*sigmaHat*IrHat(2,2),  muHat(2)+2*sigmaHat*IrHat(2,2)],'g--','lineWidth',2)
plot([muHat(1)-2*sigmaHat*IrHat(1,3),  muHat(1)+ 2*sigmaHat*IrHat(1,3)] ,[muHat(2)-2*sigmaHat*IrHat(2,3),  muHat(2)+2*sigmaHat*IrHat(2,3)],'b--','lineWidth',2)




plot([trueMu(1)-2*trueSigma*Ir(1,1)  trueMu(1)+2*trueSigma*Ir(1,1)] ,[trueMu(2)-2*trueSigma*Ir(2,1)  trueMu(2)+2*trueSigma*Ir(2,1)],'r','lineWidth',2)
plot([trueMu(1)-2*trueSigma*Ir(1,2) trueMu(1)+2*trueSigma*Ir(1,2)] ,[trueMu(2)-2*trueSigma*Ir(2,2) trueMu(2)+2*trueSigma*Ir(2,2)],'g','lineWidth',2)
plot([trueMu(1)-2*trueSigma*Ir(1,3),  trueMu(1)+2*trueSigma*Ir(1,3)] ,[trueMu(2)-2*trueSigma*Ir(2,3), trueMu(2)+ 2*trueSigma*Ir(2,3)],'b','lineWidth',2)



axis([-8 8 -8 8])

hold off


subplot(2,2,3)
plot(sse)
hold off

k2
drawnow
%pause


