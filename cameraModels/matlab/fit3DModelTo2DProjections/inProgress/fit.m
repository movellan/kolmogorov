function [r, gammaHat, xHat,yHat,yHatHistory,sse]  = fit(f, z, y, q,  rInit, rho,m,c,kappa);


p= [ 1 0 0; 0 1 0]; % projection matrix

qSum = zeros(2,2);
  nPoints = length(f);
  nf = size(f{1},2);
for k=1: nPoints
  qSum = qSum+q{k}; 
end
qSumInv= pinv(qSum);


  
  

  sseTotal=10;
k2=0;
r = rInit; % initialize rotation
while sseTotal> 0.0001 && k2<400
  k2=k2+1;
  xBar = zeros(nf,nf);
for k=1: nPoints
    u{k}= p*r*f{k};
    xBar = xBar + u{k}'*q{k}*u{k};
  end
  xBar = c+xBar;
  xBarI= pinv(xBar);

  
  
  % for fixed rotation compute optimal gamma parameter 
  
  
  

  yBar = zeros(nf,1);
  for k=1: nPoints
    yBar = yBar + u{k}'*y(:,k);
  end
  yBar = yBar+ c*m;

  gammaHat= xBarI*yBar;


  %%%%%%%%% measure error %%%%%%%%%%%

  sse1=0;
  for k=1: nPoints
    rx{k} =r*f{k}*gammaHat;
    yHat(:,k) = p*rx{k};
    sse1 = sse1+ 0.5*(y(:,k) -yHat(:,k))'*q{k}*(y(:,k) -yHat(:,k));
  end

  
  sse2=0;


    sse2 = sse2+ 0.5*(gammaHat-m)'*c*(gammaHat -m);

  rz= r*z;
  rhoZ = rho*z;

  for k=1: size(z,2)
    sse2 = sse2+ 0.5*kappa*norm(rz(:,k) -rhoZ(:,k))^2;
  end

    
    
  sseTotal = sse1 + sse2;
  sse(k2) = sseTotal;

  %%%%%%%%%%% end measure error %%%%%%%%%55


  

  
  
%%%%%%%%%%%%%%%%%%%%%%%%% rotation %%%%%%%%%%%%%%%%%%  
  
  
  
    % for fixed scale and shift improve the rotation matrix by using
  % Newton-Raphson step


  % compute gradient
  for k=1:nPoints;
    g1(:,k)= R(rx{k}) * p'*q{k}*(yHat(:,k)  -y(:,k));
  end

  for k=1:size(z,2)
    g2(:,k)= R(rz(:,k)) * (rz(:,k) - rhoZ(:,k)); 
  end
 
%  grad = (1-lambda)*sum(g1,2)+ lambda*sum(g2,2);
grad = sum(g1,2)+kappa*sum(g2,2);
  h1=zeros(3);
  h2=zeros(3);
  I=eye(3);
  yHatHistory(k2,:,:) = yHat;
  % compute Hessian

  for i=1:3
    for j=1:3
      for l=1:nPoints
 	h1(i,j) = h1(i,j)+ rx{l}'*R(I(:,i))*p'*q{l}*p*R(I(:,j))* ...
 		  rx{l};
 	h1(i,j) = h1(i,j) +(yHat(:,l) - y(:,l))'*q{l}*p*(R(I(:,i))*R(I(:,j)) ...
 							 + R(I(:,j))*R(I(: ...
 						  ,i)))*rx{l}/2;
      end
      for l=1:size(z,2)
 	h2(i,j) = h2(i,j)+ rz(:,l)'*R(I(:,i))*R(I(:,j))* ...
 		  rz(:,l);
 	h2(i,j) = h2(i,j) +(rz(:,l) - rhoZ(:,l))'*(R(I(:,i))*R(I(:,j)) ...
 						   + R(I(:,j))*R(I(: ...
 						  ,i)))*rz(:,l)/2;
      end
    end
  end


 hess =h1+ kappa*h2;
 % hess =h1;
  rDelta = pinv(hess-1*eye(3))*grad;
    r = expm(R(rDelta))*r;

  
end
xHat = zeros(3,nPoints);
 for k=1: nPoints
    xHat(:,k) =r*f{k}*gammaHat;
 end
 

