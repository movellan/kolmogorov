clear
x= randn(3,5); % Each column is a point in 3d

p= [ 1 0 0; 0 1 0; 0 0  0]; % projection matrix. It needs to be m
                            % by 3. where m could be 1, 2,3, ...

% create a rotation matrix			    
theta = randn(3,1);
theta=theta/norm(theta);
delta2=90;
theta = theta*rand(1)*delta2*pi/180;
r = expm(R(theta));

% observed projectionso of x
y = p*r*x;

% estimate the rotation matrix based on observed projections
tol = 0.0001;
rInit=eye(3);
r
rHat = fitRotation(x,y,p,rInit,tol)