function rHat=fitRotation(x,y,p,rHat,tol) 
%rHat=fitProjection(x,y,p,rHat,tol) 
% minimizes error function of the form
% rho(r) = sum_i |p r x_i - y_i|^2
% where p is an m by 3 matrix, called the projection matrix
% r is a 3 by 3 rotation matrix
% x_i is a 3 by 1 vector, the ith column of x
% y_i is an m by 1 vector, the ith column of y
%
% rHat is the starting point for iterative optimization
% tol is the tolerance
rh=10;
k2=0;

while rh> tol &&  k2<100
k2=k2+1;





rx = rHat*x;
yHat = p*rx;
rh=0;

for k=1: size(x,2)
  rh = rh+ 0.5*(y(:,k) -yHat(:,k))'*(y(:,k) -yHat(:,k));

end



f(k2) = rh;


for k=1:size(x,2)
  g(:,k)= R(rx(:,k)) * p'*(yHat(:,k)  -y(:,k));
end





grad = sum(g,2);

h1=zeros(3);
h2=zeros(3);
I=eye(3);

%%%%%%%%%%%%%%% compute the Hessian %%%%%%%%%%%%%%
for i=1:3
  for j=1:3
    for l=1:size(x,2)
      h1(i,j) = h1(i,j)+ rx(:,l)'*R(I(:,i))*p'*p*R(I(:,j))* ...
		rx(:,l);
      h1(i,j) = h1(i,j) +(yHat(:,l) - y(:,l))'*p*(R(I(:,i))*R(I(:,j)) ...
						  + R(I(:,j))*R(I(: ...
						  ,i)))*rx(:,l)/2;
    end
  end
end


hess =h1;

rDelta = pinv(hess-0.5*eye(3))*grad;

rHat = expm(R(rDelta))*rHat;

end

function y = R(x)

y=[0, -x(3), x(2); x(3),0,-x(1);-x(2),x(1),0];
