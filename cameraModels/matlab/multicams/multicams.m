% we create a 3D Gaussian object, project it into multiple cameras and
% estimate the parameters of the object and the rotation of the
% object based on the projections

% we put the world's origin at the midpoint between the two
% cameras. The median distance between the center of the eyes in
% women is 6 cm. we choose that
clear; clf
% we'll use 4 cameras to simulate 2 eyes looking at the object from
% two different points  in time
% the first point in time the cameras look forward 
% the second point in time the cameras verge on the object
nCams =4;
distanceBetweenCameras =6; % in cm
% the location of the cameras at first time step
l{1} = [-distanceBetweenCameras;0;0];
l{2} = [distanceBetweenCameras;0;0];
% location of cameras second time step
l{3} = l{1};
l{4} = l{2};

% the focal length of the eye when focusing on infinity is
% approximately 20 mm
f = 2*ones(nCams,1); % focal length of cameras in cm
% se assume the cameras have the same orientation, and choose this
% to be the orientation of the world's frame of reference,

% cols of r{i} have coordinates of maiin axes of world in camera
% frame of reference. rows of r{i} have coordinate of main zes of
% camera in world's frame of reference 
u{1}=[0 0 1]; % the first axis (roll) 
u{2}=[0 1 0]; % the second axis (yaw) 
u{3}=[1 0 0]; % the third axis (pitch) 



% rotations of the two cameras at time step 1. They look forward
r{1}=eye(3);
r{2}=eye(3); 


% rotations at time step 2. the cameras rotate inwards to converge
% on object
r{3}=expm( R(-u{2}*pi/52));  
r{4}=expm( R(u{2}*pi/52));  



% location of the object in world coordinates
% x1 horizontal, x2 vertical, x3 depth
mu = [0;0;100]; % in centimeters
% we will get the object to simulate a female head
% Median female head: width= 14.8cm, height 22.5 cm, depth 18.1 cm 
% We model this as Gaussian ellipsoid. Diameter of ellipsoid of
% Mahalanobisd distance =1 same as diameters of head
sigma0 = [14.8; 22.5; 18.1]/2; 
sigma0 = sigma0.^2;
sigma0 = diag(sigma0); % sigma is the object's covariance matrix





% we rotate the object and pause after each rotation. 

angleStart = 0; % in degrees
angleEnd = 80; % in degrees
deltaAlpha = 1; % in degrees 
angleStart = angleStart*2*pi/360;
angleEnd = angleEnd*2*pi/360;
deltaAlpha = deltaAlpha*2*pi/360;

alpha = angleStart:deltaAlpha:angleEnd;

for k=1:length(alpha) 
  euler{1} = 1*alpha(k); %roll 
  euler{2} = 1*alpha(k);  %yaw
  euler{3} = 1*alpha(k);  %pitch
  for j=1:1:3
    reuler{j} = expm( R(u{j}*euler{j}));  
  end
  % object rotation
  ro = reuler{1}*reuler{2}*reuler{3};

  sigma = ro*sigma0*ro';

% project object onto each camera

for k=1:nCams
  muc= r{k} *(mu -l{k}); % object mean in camera coordinates
  p{k} = [1 0 0;0 1 0]*f(k)/muc(3);;
  c{k} = p{k}*r{k}*sigma*r{k}'*p{k}';
  m{k} = p{k}*muc;
end


% get location of focal point of each camera in world coordinates
for k=1:nCams
  fp(:,k)= r{k}(:,3)*f(k)+l{k};
end


% estimate sigma, the 3d covariance matrix
% note the procedure assumes we know the projection matrices, 
% which basically means we know the focal lengh and the 3D location
% of the object. the 3d location of the object can be obtained by
% triangulation. to be done later



% ko is a constraint matrix, it  constructs the vectorized version
% of a  symmetric 3x3 matrix from 6 parameters
ko(1,:)=[1 0 0 0 0 0];
ko(2,:)=[0 1 0 0 0 0];
ko(3,:)=[0 0 1 0 0 0];
ko(4,:)=ko(2,:);
ko(5,:)=[0 0 0 1 0 0];
ko(6,:)=[0 0 0 0 1 0];
ko(7,:)=ko(3,:);
ko(8,:)=ko(6,:);
ko(9,:)=[0 0 0 0 0 1];


a =zeros(6,6);
b=zeros(6,1);

for j=1:nCams
  u1 = kron(p{j}*r{j},p{j}*r{j})*ko;
  uu = u1'*u1;
  a =a +uu;
  v =reshape(c{j}',4,1);
  uv = u1'*v;
  b = b+uv;
end

theta = pinv(a)*b;
% construct the vectorized version of sigma using
% the estimated theta
sigmaHat = ko*theta;
sigmaHat = reshape(sigmaHat,3,3)


[roHat sigma0Hat ] = eig(sigmaHat);

% object dimensions and dimension estimates
% note the estimates are order from smaller to larger
2*diag(sigma0)
2*diag(sigma0Hat)


% object rotation and rotation estimate
% note the roation estimate is with respect to object axes that are 
% organized from smaller to larger
ro
roHat


% plot 
for k=1:nCams
  subplot(2,2,k)
  [v d] = eig(c{k});
  d = sqrt(d);
  angle = atan(v(2,1)/v(1,1));
  scatter(m{k}(1),m{k}(2));
  hold on
  ellipse(d(1,1),d(2,2),angle,m{k}(1),m{k}(2),'r');
  lims = [-1,1]/2;
  plot([0 0],[lims(1) lims(2)],'--b');
  plot([lims(1) lims(2)],[0 0],'--b');
  xlim(lims);
  ylim(lims);
  titl= strcat('Camera ',num2str(k));
  title(titl)
  axis equal
  hold off
end


drawnow
pause
end
