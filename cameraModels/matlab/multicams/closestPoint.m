function y= closestPoint(u,b)
% finds point closest to a set of lines
% columns of u define the orientation of each line
% columns of b are points on each line (the offsets)
 
clear
% each column of u is a unit lenght vector that specifies the
% direction of a line

u=[1 0 0;  1 0 0]';
b= [0 1 0; 0 0 0]';


% normalize cols of u
n = size(u,2);
d = size(u,1);
for k=1:n
  u(:,k) = u(:,k)/norm(u(:,k));
end
% each column of b is a point in the line, specifying its offset

s1= zeros(d,d);
s2 = zeros(d,1);
for k=1:n
  c = u(:,k)*u(:,k)'-eye(d);
  c = c'*c;
  s1 = s1+ c;
  s2 =s2+ c*b(:,k);
end

y = pinv(s1)*s2;
