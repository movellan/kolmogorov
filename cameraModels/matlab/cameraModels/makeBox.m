function x = makeBox(w,h,d)
% make a set of points inside a box  centered at 0
% w,h,d are width, height and depth
% cols of x are the 3D coordinates of the points
% can be plot using plot(x(1,:),x(2,:))
x1 = [-0.5  0.5  0.5  -0.5  -0.5  -0.5  0.5  0.5  -0.5  -0.5  0.5  0.5  0.5  0.5  -0.5  -0.5];
x2 = [-0.5  -0.5  0.5  0.5  -0.5  -0.5  -0.5  0.5  0.5  -0.5  -0.5  -0.5  0.5  0.5  0.5  0.5];
x3 = [-0.5  -0.5  -0.5  -0.5  -0.5  0.5  0.5  0.5  0.5  0.5  0.5  -0.5  -0.5  0.5  0.5  -0.5];

x = [x1;,x2;,x3];
s=diag([w,h,d]);
x= s*x;
