% para-perspective projection of 3d points for camera with focal
% length  f. 
% This is a first order Taylor approximation of perspective
% projection about the point m in world coordinates
% cols of x are world coordinates of object points. 
% f is camera's focal length
% o is the 3d vector with origin of camera in world coordinates
% r is rotation matrix. columns are the world coordinates of the
% camera's axes. 
% y has the  projection of x in camera coordinates
% very important! The origin of the coordinate system is at the
% optical center, the image plane is at f. otherwise the math does
% not work.  

function y = paraProject(x,o,r,m,f)

npoints = size(x,2);
% convert to camera  coordinates
x= r'*(x-repmat(o,1,npoints));
m = r'*(m-o);
p = [f/m(3),0,-f*m(1)/m(3)^2; 0, f/m(3),-f*m(2)/m(3)^2];
c=  [ f*m(1)/m(3); f*m(2)/m(3)];
y = p*x+repmat(c,1,npoints);
y = [y;f*ones(1,npoints)];




