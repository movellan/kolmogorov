% We are given a vector v 
% we need to align the z axis of the camera frame of reference with
% that vector
% we also want the vertical world axis to have a vertical
% orthographic projection on the image plane

clear;clf
x= [1;0;0];
y= [0;1;0];
z=[0;0;1];

for k=1:20 
v = randn(3,1);
v=v/norm(v);
a=rotationBetweenUnitVectors(z,v); 

% confirm that a*z=v
tol=0.00001;
if(norm(v-a*z) > tol)
  display('Test1 Failed')
else
  display('Test1 Passed')
end

% confirm that  additional rotations about the v axis will also work

r= axisToMatrix(v*randn(1)*pi);

if(norm(v-r*a*z) > tol)
  display('Test2 Failed')
else
  display('Test2 Passed')
end



  s= cameraAlign(v);
  if(norm(v-s*z) > tol)
    display('Test3 Failed')
  else
    display('Test3 Passed')
  end
  y2= s'*y; % camera coordinates of the world vertical axis

  % we want the cameras coordinates of the world vertical axis to
  % be orthogonal to the camera's horizontal axis

  if(abs(x'*y2) > tol)
    display('Test4 Failed')
  else
    display('Test4 Passed')
  end


  


  
  x2= s'*x; % camera coordinates of the world horizontal axis


  
  plot([0 y2(1)],[0,y2(2)],'r', [0 x2(1)],[0,x2(2)],'--g')
  hold off
  xlim([ -1 1])
  ylim([-1 1])

  drawnow
  pause  
  
end

