clear
clf
path(path,'../../../r/matlab/Conversions');

z=[0;0;1];

x = rand(3,1); % world coordinates of object point
m = rand(3,1); %  object centroid in world coordinates
m2= m/norm(m);
a=rotationBetweenUnitVectors(z,m2);
alphaHat=randn(1)*pi;
r = axisToMatrix(m2*alphaHat);
s= r*a; % camera rotation to align its axis to m
s2=a;
if norm(s*z - m2) <0.0000000000001
  disp('test1 passed')
else
  disp('test1 failed')
end
if norm(s2*z - m2) <0.0000000000001
  disp('test2 passed')
else
  disp('test2 failed')
end


p = [1 0 0 ; 0 1 0; 0 0 0];

b1=s*p*s'
b2=s2*p*s2'

if norm(b1 - b2) < 0.000000000001
  disp('Theorem Test Passed')
else
  disp('Theorem Test Failed')
end
