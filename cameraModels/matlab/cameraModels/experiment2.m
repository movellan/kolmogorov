% approximate perspective projection of object with a weak perspective of a
% rotation of the object 


% x_i = o_i + m where x_i,o_i, m are 3d vectors.
% m is the location of object centroid. o_i is the coordinate of
% point i in object prior to displacement. 
% x_i is location of the same point after displacement

% y_i = p(x_i) (o_i+m)  + [0 0 f]'  % perspective projection

% yHat_i = alpha * (p  rho o_i )  +  [beta1 beta2 f]

% where alpha, beta1, beta2 are scalars and rho is a rotation
% matrix

% in practice 

clear
clf
path(path,'../../../r/matlab/Conversions');
% parameters of the iPhone 4 camera
f = 4.28; % focal length in mm 
w = 4.592; % sensor width in mm
h = 3.4550; % sensor height in mm
hp = 3264; % number of horizontal pixels
vp = 2448; % number of vertical pixels




% width, height, depth of the box in milimeters 
s=[100; 100; 100];


l= [000;000;500]; % location of the center  of the box  in
                       % milimeters 

		       


x = makeBox(s(1),s(2),s(3));
x2= makeBox(0,0,5*s(3));
z=[0;0;1]*2*s(3);;

% define beggining and end rotation angles for animation



nAlpha = 100; % number of angles in between begin and end


r=randomRotation();
for k=1:nAlpha




  theta=100;
  while theta >40

    displace = [randn(1)*500;randn(1)*500;0.0];
    
    m = l+ displace;
    
    fToM = m;
    fToM = fToM/norm(fToM);
    % angle between optical axis and object
    theta=acos(fToM'*[0;0;1])*180/pi 
  end
  
  
  
  r = randomRotation();
  xr=r*x;
  xr2=r*x2;
  
  zr = r*z;
  




  npoints = length(x);
  xr = xr+repmat(m,1,npoints);

  
  zr = zr + m;

  co=zeros(3,1); % camera origin
  cr = eye(3); % camera rotation
  % perspective projection
  yp=perProject(xr,co,cr,f);
  yp2=perProject(xr2,co,cr,f);
  
  % this provides implicitely solutions to the alpha and beta parameters



  yw= weakRotatedProject(xr,co,cr,m,f);
  yw2= weakRotatedProject(xr2,co,cr,m,f);

  
  mHat = perProject(m,co,cr,f);

  
  zp = perProject(zr,co,cr,f);

  
  % convert to pixels
  pixelSize = w/hp;
  yp = yp/pixelSize; % 1 pixel is 1.4 micro meters
  yp2 = yp2/pixelSize; % 1 pixel is 1.4 micro meters
  yw=yw/pixelSize;
  yw2=yw2/pixelSize;
  zp = zp/pixelSize;
  

    mHat = mHat/pixelSize;

  
  plot(yp(1,:), yp(2,:),'b')
    hold on;
    plot(yp2(1,:), yp2(2,:),'b')

    
    plot(yw(1,:), yw(2,:),'g')
    plot(yw2(1,:), yw2(2,:),'g')
    

    
    plot([mHat(1),zp(1)],[mHat(2),zp(2)],'--b');


  hold off
 xlim([-hp , hp]/0.5)
  ylim([-vp , vp]/0.5)
  xlabel('Pixels');
  ylabel('Pixels')
  
drawnow  

pause
end


