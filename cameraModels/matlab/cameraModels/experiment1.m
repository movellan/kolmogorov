% locate object centroid at point m
% rotate object so it always have same coordinates with respect to
% canonical frame of reference.
% canonical frame of reference1: depth axis = m, end point of
% vertical unit vector as close as possible to ceiling
% canonical frame of reference2: dept axis =m, end point of
% horizontal unit vector as close as possible to right vertical wall.
%
% hypothesis1: perspective projection of rotated object looks
% approximately the same regardless of m
% corollary1: perspective projection of rotated object looks
% approximately same as parallel projection of object when m is at
% optical axis

% first axis: horizontal. second vertical, third depth. 
% image plane at origin
% optical axis is the depth axis
clear
clf
path(path,'../../../r/matlab/Conversions');
% parameters of the iPhone 4 camera
f = 4.28; % focal length in mm 
w = 4.592; % sensor width in mm
h = 3.4550; % sensor height in mm
hp = 3264; % number of horizontal pixels
vp = 2448; % number of vertical pixels




% width, height, depth of the box in milimeters 
s=[100; 100; 500];


l= [000;000;1000]; % location of the center  of the box  in
                       % milimeters 

		       


x = makeBox(s(1),s(2),s(3));


% define beggining and end rotation angles for animation


angleBegin = -45; % in degrees
angleEnd = 45; % in degrees
nAlpha = 100; % number of angles in between begin and end

angleBegin = angleBegin*2*pi/360;
angleEnd = angleEnd*2*pi/360;
deltaAlpha = angleEnd-angleBegin;

alpha=angleBegin:deltaAlpha/nAlpha:angleEnd;

if deltaAlpha ==0
  alpha= repmat(angleBegin,nAlpha,1);
end
  

u{1}=[0 0 1]; % the first axis (roll) 
u{2}=[0 1 0]; % the second axis (yaw) 
u{3}=[1 0 0]; % the third axis (pitch) 

r=randomRotation();

r=[ 0.9996   -0.0246   -0.0133;
    0.0248    0.9996    0.0164;
    0.0129   -0.0167    0.9998;
    ];

w1=rand(1);
w2=rand(1);
w1=1;
w2=1;

for k=1:length(alpha) 




  
  dis= sin(2*pi*k/length(alpha))*1000;
  displace = [w1*dis;w2*dis;0.0*abs(dis)];
  
  m = l+ displace;
%  m = m+randn(3,1)*10;
  xr=r*x;
  
  rot= cameraAlign(m/norm(m));
%  rot3= cameraAlignJunk(m/norm(m));
  m
  rot
%  rot3
  xhat = rot*xr;   
%  xhat3 = rot3*xr;   

  npoints = length(x);
  xr = xr+repmat(m,1,npoints);
  xhatr = xhat+ repmat(m,1,npoints);
  %xhatr3=xhat3+ repmat(m,1,npoints);

  co=zeros(3,1); % camera origin
  cr = eye(3); % camera rotation
  % perspective projection
  yp=perProject(xr,co,cr,f);
  ypHat = perProject(xhatr,co,cr,f);

  ypHat2= weakProject(xr,co,cr,m,f);
 % ypHat3 = perProject(xhatr3,co,cr,f);  
  
  mHat = perProject(m,co,cr,f);
  
  fToM = m;
  fToM = fToM/norm(fToM);
  % angle between optical axis and object
  theta=acos(fToM'*[0;0;1])*180/pi 


  
  % convert to pixels
  pixelSize = w/hp;
  yp = yp/pixelSize; % 1 pixel is 1.4 micro meters
  ypHat=ypHat/pixelSize;
  ypHat2=ypHat2/pixelSize;
  %ypHat3=ypHat3/pixelSize;
    mHat = mHat/pixelSize;

  
  plot(yp(1,:), yp(2,:),'g')
      hold on; 
    plot(ypHat(1,:), ypHat(2,:),'r')
 
%    plot(ypHat2(1,:), ypHat2(2,:),'r')
   % plot(ypHat3(1,:), ypHat3(2,:),'k--')

  hold off
  xlim([-hp , hp]/1)
  ylim([-vp , vp]/1)
  xlabel('Pixels');
  ylabel('Pixels')
  
drawnow  

pause
end


