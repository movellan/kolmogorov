% simulate rendering of a box by an iPhone 4 camera using perspective,
% linearized perspective, orhoperspective and and weak perspective
% projections Javier R. Movellan

% first axis: horizontal. second vertical, third depth. 
% image plane at origin
% optical axis is the depth axis
clear
clf
path(path,'../../../r/matlab/Conversions');
% parameters of the iPhone 4 camera
f = 4.28; % focal length in mm 
w = 4.592; % sensor width in mm
h = 3.4550; % sensor height in mm
hp = 3264; % number of horizontal pixels
vp = 2448; % number of vertical pixels




% width, height, depth of the box in milimeters 
s=[200; 200; 1000];


l= [1000;1000;1000]; % location of the center  of the box  in
                       % milimeters 

		       


x = makeBox(s(1),s(2),s(3));


% define beggining and end rotation angles for animation


angleBegin = -45; % in degrees
angleEnd = 45; % in degrees
nAlpha = 100; % number of angles in between begin and end

angleBegin = angleBegin*2*pi/360;
angleEnd = angleEnd*2*pi/360;
deltaAlpha = angleEnd-angleBegin;

alpha=angleBegin:deltaAlpha/nAlpha:angleEnd;

if deltaAlpha ==0
  alpha= repmat(angleBegin,nAlpha,1);
end
  

u{1}=[0 0 1]; % the first axis (roll) 
u{2}=[0 1 0]; % the second axis (yaw) 
u{3}=[1 0 0]; % the third axis (pitch) 


for k=1:length(alpha) 



%  for k=1:97
  euler{1} = 0*alpha(k); %roll 
  euler{2} = 2*alpha(k);  %yaw
  euler{3} = 1*alpha(k);  %pitch
  

  for j=1:1:3
    reuler{j} = expm( R(u{j}*euler{j}));  
  end
  r = reuler{1}*reuler{2}*reuler{3};

  objectAxis= r*[0;0;5000];

  % moving the object left and right
  dis= sin(10*2*pi*k/length(alpha))*1000;
  displace = [0.0*dis;0.0*dis;0.0*abs(dis)];
  
  m = l+ displace;
  
  xr = r*x;
  npoints = length(x);
  xr = xr+repmat(m,1,npoints);

  

  co=zeros(3,1); % camera origin
  cr = eye(3); % camera rotation
  % perspective projection
  yp=perProject(xr,co,cr,f);
  mHat = perProject(m,co,cr,f);
  % linearized perspective (parapersepctive projection)
  yl= paraProject(xr,co,cr,m,f);
  
  % orthoperspective projection
  yop = orthoperProject(xr,co,cr,m,f);
  %another version of orthoperspective projection. not sure why it
  %works perhaps better than the original
  yop2 = orthoperProject2(xr,co,cr,m,f);
  % weak  perspective projection
  yw=weakProject(xr,co,cr,m,f);

  fToM = m;
  fToM = fToM/norm(fToM);
  % angle between optical axis and object
  theta=acos(fToM'*[0;0;1])*180/pi 

  objectAxisPer=perProject(m+objectAxis,co,cr,f);
  
  % convert to pixels
  pixelSize = w/hp;
  yp = yp/pixelSize; % 1 pixel is 1.4 micro meters
  yl= yl/pixelSize;
  yw = yw/pixelSize;
  yop = yop/pixelSize;
  yop2 = yop2/pixelSize;
  mHat = mHat/pixelSize;
  objectAxisPer=objectAxisPer/pixelSize;
  % plot 
  subplot(2,2,1)
  plot(yp(1,:), yp(2,:),'b')
  hold on;
  plot([0 mHat(1)],[0  mHat(2)],'r--')
  plot([mHat(1), objectAxisPer(1)],[mHat(2), objectAxisPer(2)],'b--')
  hold off
  xlim([-hp , hp]/0.5)
  ylim([-vp , vp]/0.5)
  xlabel('Pixels');
  ylabel('Pixels')
  %axis equal
  title('Perspective Projection')
  subplot(2,2,2)
  plot(yl(1,:), yl(2,:),'b')
  hold on;
  plot([0 mHat(1)],[0  mHat(2)],'r--')
    plot([mHat(1), objectAxisPer(1)],[mHat(2), objectAxisPer(2)],'b--')
  hold off
  xlim([-hp , hp]/0.5)
  ylim([-vp , vp]/0.5)
    xlabel('Pixels');
  ylabel('Pixels')
  %axis equal
  title('Linearized Perspective Projection')
  subplot(2,2,3)
  plot(yop(1,:), yop(2,:),'b')
    hold on;
  plot([0 mHat(1)],[0  mHat(2)],'r--')
    plot([mHat(1), objectAxisPer(1)],[mHat(2), objectAxisPer(2)],'b--')
  hold off
  hold on
    plot(yop2(1,:), yop2(2,:),'g--')
    hold off
  xlim([-hp , hp]/0.5)
  ylim([-vp , vp]/0.5)
  xlabel('Pixels');
  ylabel('Pixels')
  %axis equal
  title('Ortho Perspective Projection')
  
  subplot(2,2,4)
  plot(yw(1,:), yw(2,:),'b')
    hold on;
  plot([0 mHat(1,:)],[0  mHat(2,:)],'r--')
    plot([mHat(1), objectAxisPer(1)],[mHat(2), objectAxisPer(2)],'b--')
  hold off
  xlim([-hp , hp]/0.5)
  ylim([-vp , vp]/0.5)
  xlabel('Pixels');
  ylabel('Pixels')
  %axis equal
title('Weak Perspective Projection')
drawnow  


end


