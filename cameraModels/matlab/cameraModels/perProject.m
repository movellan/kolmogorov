% perspective projection of 3d points for camera with focal
% length f.
% cols of x are world coordinates of object points. 
% f is camera's focal length
% o is the 3d vector with origin of camera in world coordinates
% r is rotation matrix. columns are the world coordinates of the
% camera's axes. 
% y has the  projection of x in camera coordinates
% very important! The origin of the coordinate system is at the
% optical center, the image plane is at f. otherwise the math does
% not work.  

function p = perProject(x,o,r,f)
n= size(x,2);
p = zeros(2,n);
x= r'*(x-repmat(o,1,n));
for k=1: size(x,2)
  p(:,k) = [f/x(3,k),0,0; 0, f/x(3,k),0]*x(:,k);
end
p = [p;f*ones(1,n)];
