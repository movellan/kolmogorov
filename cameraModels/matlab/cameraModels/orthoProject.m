% weak-perspective projection of 3d points for camera with focal
% length  f. 
% This is an approximation of perspective
% projection about the point m
% cols of x are world coordinates of object points. 
% f is camera's focal length
% o is the 3d vector with origin of camera in world coordinates
% r is rotation matrix. columns are the world coordinates of the
% camera's axes. 
% y has the  projection of x in camera coordinates
% for consistency with the other models we let the z coordinate
% equal to f, the focal length, 


function y = orthoProject(x,o,r,f)

npoints = size(x,2);
x= r'*(x-repmat(o,1,npoints));
p = [1,0,0; 0, 1,0];
y = p*x;
y = [y;f*ones(1,npoints)];
  




