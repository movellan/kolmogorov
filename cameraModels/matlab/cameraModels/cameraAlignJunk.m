function s = cameraAlignJunk(v)
% given a unit vector v, align the optical axis of the camera to v
% with the constraint that the orthographic projection of the vertical
% world axis y should look vertical on the camera's iamge plane
% i think this is the same as saying that if we take the y axis of
% the camera, we want to choose it so that its end point is as
% close as possible to the ceiling.


% s is the rotation from world frame of reference to camera frame
% of reference, i.e., cols of s have the world coordinates of the
% camera axes

% w =s c converts from camera coordinates c to world coordinates w
% c = s' w converts from world coordinates w to camera coordinates


path(path,'../../../r/matlab/Conversions');
x= [1;0;0];
y= [0;1;0];
z=[0;0;1];
y=x;

v=v/norm(v);
a=rotationBetweenUnitVectors(z,v); 

y2= a'*y; % camera coordinates of world vertical axis
y2(3)=0; % project to image plane
n=norm(y2);
if n >0 
  y2 = y2/norm(y2);
  alphaHat= acos(y2'*y);
else
  alphaHat=0;
end

% there are  two solutions to the acos: alphaHat and -alphaHat
% we know check which one is the correct one
r = axisToMatrix(v*alphaHat);
s2= r*a;
y2=s2'*y;
y2(3)=0;

s3=r'*a;
y3= s3'*y;
y3(3)=0;

n2= abs(y2'*x);
n3= abs(y3'*x);

if n3 <= n2
  s =s3;
else
  s=s2;
end



