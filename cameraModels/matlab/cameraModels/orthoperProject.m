% ortho-perspective projection of 3d points for camera with focal
% length  f. 
% An an approximation of perspective
% projection about the point m in world coordinates
% cols of x are world coordinates of object points. 
% f is camera's focal length
% o is the 3d vector with origin of camera in world coordinates
% r is rotation matrix. columns are the world coordinates of the
% camera's axes. 
% y has the  projection of x in camera coordinates
% very important! The origin of the coordinate system is at the
% optical center, the image plane is at f. otherwise the math does
% not work.  

function y = orthoperProject(x,o,r,m,f)
npoints = size(x,2);
% convert to camera coordinates
x= r'*(x-repmat(o,1,npoints));
m = r'*(m-o);

% rotate virtual camera so optical axis align with m. there are
% many solutions. they all work. 
%virtualR = cameraAlign(m); 
virtualR=cameraAlignJunk(m);
y = orthoProject(x,m,virtualR,0);
% create a plane object parallel to image plane
% y is in camera coordinates. 

y = virtualR*y; % Rotate virtual camera plane back so it aligns
                % with original camera 

y = y+repmat(m,1,npoints);
% do a weak perspective projection of the plane object into the
% original camera
co=[0;0;0];
cr=eye(3);
y=weakProject(y,co,cr,m,f);
