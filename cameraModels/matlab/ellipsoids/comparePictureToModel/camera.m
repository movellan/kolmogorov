clear; clf reset
%reset(gcf)

% Simulates iphone 4 back facing camera. (may be iphone 5)

% compares simulation of an ellipsoid to an actual picture 

% first axis: horizontal. second vertical, third depth. 
% image plane at origin
% optical axis is the depth axis

f = 4.28; % focal length in mm
w = 4.592; % sensor width in mm
h = 3.4550; % sensor height in mm
hp = 3264; % number of horizontal pixels
vp = 2448; % number of vertical pixels


% we will render a 3D Gaussian ellipsoid (a nerf ball)
% the  nerf ball has the following dimension 
% 23 cm in length
% 13.5 cm in width
% 13.5 cm in depth

s=[13.5; 23; 13]; % dimensions in centimeters
s = s/2; % convert to radii
s = s*10; % transform to milimiters




% the nerf ball was located 65 centimeters from the camera
% approximately aligned with its optical axis. 
% in practice it was 3 cm right and 0.8 cm below the axis

m= [3;-0.8;65]; % coordinates of the center of the object in cm;
m = m*10; % transform to milimiters








% weak  perspective projection
p = [f/m(3),0,0; 0, f/m(3),0];

% convert to pixels
pixelSize = w/hp;
hold on

% mean
mw = p*m/pixelSize


ps = p*diag(s);

l1 = ps(1,1)/pixelSize;
l2 = ps(2,2)/pixelSize;

scatter(mw(1), mw(2))


plot([mw(1) mw(1)+l1], [mw(2) mw(2)],'r')
plot([mw(1) mw(1)], [mw(2) mw(2)+ l2],'r')
angle = 0;
ellipse(l1,l2,angle,mw(1),mw(2),'b')

xlim([-hp , hp]/2)
ylim([-vp , vp]/2)
%axis equal
axis off
pause(1)

% overlay the photo on top of the figure
 ff= getframe;
 F= ff.cdata;
 F= double(F);

 hold off

 J= imread('ball.jpg');
 J=double(J);
 F = imresize(F,[size(J,1), size(J,2)]);
 K = 0.5*J+0.5*F;
 imshow(uint8(K))