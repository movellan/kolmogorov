clear; clf

%reset(gcf)

% simulates weak perpsective projectin of a 3d ellipsoid 
% first axis: horizontal. second vertical, third depth. 
% image plane at origin
% optical axis is the depth axis




% Use parameters  of iphone 4 higher resolution  camera. 



f = 4.28; % focal length in mm
w = 4.592; % sensor width in mm
h = 3.4550; % sensor height in mm
hp = 3264; % number of horizontal pixels
vp = 2448; % number of vertical pixels


% we will render a 3D Gaussian ellipsoid (a nerf ball)
% the  nerf ball has the following dimension 
% 23 cm in length
% 13.5 cm in width
% 13.5 cm in depth
s=[13.5; 23; 13]; % dimensions in centimeters
s = s/2; % convert to radii
s = s*10; % transform to milimiters




% the nerf ball is located 40 centimeters from the camera
% approximately aligned with its optical axis. 

m= [0;0;40]; % coordinates of the center of the object in cm;
m = m*10; % transform to milimiters

% let's generate a bunch of points on the unit sphere
npoints=100;
x = randn(3,npoints);
for k=1:npoints
  x(:,k) = x(:,k)/norm(x(:,k))
end


% first we compute the rotation matrix corresponding to the Euler
% angles

u{1}=[0 0 1]; % the first axis (roll) 
u{2}=[0 1 0]; % the second axis (yaw) 
u{3}=[1 0 0]; % the third axis (pitch) 

angleStart = -89; % in degrees
angleEnd = 89; % in degrees
deltaAlpha = 0.1; % in degrees

% convert to radians
angleStart = angleStart*2*pi/360;
angleEnd = angleEnd*2*pi/360;
deltaAlpha = deltaAlpha*2*pi/360;

alpha = angleStart:deltaAlpha:angleEnd
for k=1: length(alpha)

  euler{1} = 0.1*alpha(k); %roll 
  euler{2} = 0.1*alpha(k);  %yaw
  euler{3} = alpha(k);  %pitch
  

for j=1:1:3
  reuler{j} = expm( R(u{j}*euler{j}));  
end
% the rotation matrix estimated by the user
r = reuler{1}*reuler{2}*reuler{3};


% weak  perspective projection
p = [f/m(3),0,0; 0, f/m(3),0];


% convert to pixels
pixelSize = w/hp;

y = p*r*diag(s)*x/pixelSize;
% mean
mw = p*m/pixelSize
y = y+repmat(mw,1,npoints);

ps = p*diag(s);

prs = p*r*diag(s);
prs= prs/pixelSize;

kk = prs*prs'; % this corresponds to the variance matrix

l1 = sqrt(kk(1,1));
l2 = sqrt(kk(2,2)); 
% plot minimalluy enclosing rectangle
plot([mw(1)-l1, mw(1)+l1], [mw(2)-l2, mw(2)-l2])
hold on
plot([mw(1)+l1, mw(1)+l1], [mw(2)-l2, mw(2)+l2])
plot([mw(1)+l1, mw(1)-l1], [mw(2)+l2, mw(2)+l2])
plot([mw(1)-l1, mw(1)-l1], [mw(2)+l2, mw(2)-l2])



% plot projection of the 3 main axes of the 3D ellipsoid
plot([mw(1) mw(1)+prs(1,1)], [mw(2) mw(2)+ prs(2,1)],'r')
plot([mw(1) mw(1)+prs(1,2)], [mw(2) mw(2)+ prs(2,2)],'g')
plot([mw(1) mw(1)+prs(1,3)], [mw(2) mw(2)+ prs(2,3)],'b')


% plot the 2 main axes of the projected ellipsoid 
[v d] = eig(kk);
d = sqrt(d);
v= v*d;

plot([mw(1)-v(1,1) mw(1)+v(1,1)], [mw(2)-v(2,1) mw(2)+ v(2,1)],'k--')
plot([mw(1)-v(1,2)  mw(1)+v(1,2)], [mw(2)-v(2,2)  mw(2)+ v(2,2)],'k--')

% plot the projected ellipsoid
angle = atan(v(2,1)/v(1,1));
ellipse(d(1,1),d(2,2),angle,mw(1),mw(2),'b')

% plot center of the projected ellipsoid
scatter(mw(1), mw(2))
scatter(y(1,:),y(2,:))

xlim([-hp , hp]/2)
ylim([-vp , vp]/2)
pause(0.01)
drawnow
hold off
end
