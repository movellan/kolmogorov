import numpy as np
from matplotlib import pyplot as plt

L = 1.0
L2 = 1.0
def display(fig,x,y):
    print len(x)
    #import ipdb; ipdb.set_trace()
    fig.axes[0].cla()

    fig.axes[0].plot(x,y)
    fig.axes[0].plot(x,x)
    #fig.axes[0].set_axis_off()
    plt.draw()

    plt.pause(1)


def toRadians(x):
    return x*2*np.pi/360.0

def toDegrees(x):
    return x*360/(2*np.pi)

def phiToPsy(x):
    x = toRadians(x)
    u = np.tan(x)* L2/L
    y = np.arcsin(-u)
    y = toDegrees(y)
    return y



phi = np.arange(0,30,0.1)
psi = np.zeros(len(phi))



for i in np.arange(len(phi)):
    psi[i] = phiToPsy(phi[i])


psiDot = np.tan(phi) + np.sin(psi)*L/L2

plt.ion()
fig, axes = plt.subplots(ncols=1)

for k in range(len(phi)):
    print k
    display(fig,phi[0:k], psi[0:k])
