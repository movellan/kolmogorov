function drawCartPole(theta, f, cp)
	cx = theta(1);
	cy = 0;
	bx = theta(1) + cp.l*sin(theta(2));
	by = cy + cp.l*cos(theta(2));
	z=.2;
   %plot cart-pole
	cla
	hold on
	rectangle('Position',[cx-z cy-z 2*z 2*z]);
	line([cx bx], [cy by]);
	rectangle('Position',[bx-z by-z 2*z 2*z],'Curvature',[1 1])
	quiver(cx, cy, f, 0,'r');
	drawnow
	hold off
	

