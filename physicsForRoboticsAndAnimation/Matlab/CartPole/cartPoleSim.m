% vim:nospell
clear all

% Cart-Pole Phyiscal Parameters (SI units)
cp.m1= 1; 		% Cart Mass 
cp.m2 = 0.1;	% Ball Mass
cp.l= 3;		% Pole length
cp.k1 = 1e-2;	% Cart-ground (prismatic) friction
cp.k2 = 1e-2;	% Cart-Pole (revolve) friction

%simulation constants
gConst=9.8; %gravity
dt=0.001; %simulation time step
cdt=0.1 % control time step
theta = [-5 0.1]'; %initial position (cart offset, pole-tilt)
dtheta = [ 0 0]';  %initial velocity
sigma = 0.1; % state observation noise

% generalized Mass, Coriollis matrix and Torque
M = @(theta) ...
	[ cp.m1+cp.m2 cp.m2*cp.l*cos(theta(2));
	  cp.m2*cp.l*cos(theta(2)) cp.m2*cp.l^2];
C = @(theta, dtheta)  ...
	[ 0 -cp.l*cp.m2*dtheta(2)*sin(theta(2));
	  0  0 ] ;

Tau = @(f, theta, dtheta) ...
	[ f - cp.k1*dtheta(1);
	  cp.m2*gConst*sin(theta(2)) - cp.k2*dtheta(2)];

% Linearized system at theta(2) \approx  0 
% x_{t+1} = A x_{t} + B u
M0inv = inv(M([0 0]));
A1  = 	[0 0 1 0; 0 0 0 1]; 
A2 = M0inv*[0 0 -cp.k1 0; 0 cp.m2*cp.l*gConst 0 -cp.k2];
A = eye(4) + cdt*[A1;A2];
B = [0; 0; cdt*M0inv(:,1)];

% J = \sum x'Qx + u'Ru
Q = eye(4); % goal: stop at the origin with velocity 0
R = 100;    % use as small force as possible
K = dlqr(A,B,Q,R);


% setup the figure
figure(1)
axis equal manual
xlim([-10 10]);
ylim([-cp.l-1 cp.l+1]);
for t=1:dt:1000
	
	%Control at rate cdt
	if mod(t, cdt) ==0
		x=[theta;dtheta]; %state vector
		x=x+sigma*randn(4,1); %add observation noise to keep the cart running
		f = -K*x; % LQR controller
	end

	%simulation steps
	ddtheta = inv(M(theta))*Tau(f, theta, dtheta)-C(theta, dtheta)*dtheta;
	theta = theta + dtheta*dt + 0.5*ddtheta*dt*dt;
	dtheta = dtheta + ddtheta*dt;

	%display
	if mod(t, 0.1) == 0 
		drawCartPole(theta, f, cp);
	end
	%stop at the goal
	if norm(x) < 0.01
		fprintf(1,'goal achieved\n');
		break
	end
end
