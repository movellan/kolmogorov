function m = robotInit(m)
% initialize variables
m.njoints = length(m.u)-1;
m.d=zeros(m.njoints+1,1); 
m.bigTheta=[];
m.bigdTheta=[];
m.bigddTheta=[];
m.bigEta=[];
m.bigArmature=[];
m.bigIntTheta=[];

for k=2:1: m.njoints+1
  m.d(k) = size(m.u{k},2); %degrees of freedom. 
  if m.d(k) ~= 1
    error('Error: only 1 df joints are allowed')
    
  end
  m.tau{k}= zeros(m.d(k),1); % overall torque on each link
  m.tauG{k}= zeros(m.d(k),1); % gravitational torque. some weird
                              % here, since m.tauG{k} later takes
                              % dimensionality equal to the total
                              % number of degrees of freedom. i
                              % believe it means the graviational
                              % torque that link{k} exerts on each joint
  m.tauV{k}= zeros(m.d(k),1); % torque on each joint due to viscous
                              % forces 
  m.bigTheta=[m.bigTheta;m.theta{k}]; % joint angles in vector form
  m.bigdTheta=[m.bigdTheta;m.dTheta{k}];% joint angular velocities
                                        % in vector form
  m.ddTheta{k} = zeros(m.d(k),1); % joint angular accelerations
  m.bigddTheta=[m.bigddTheta;m.ddTheta{k}]; % joint angular
                                            % accelerations in
                                            % vector form
  m.intTheta{k} = zeros(m.d(k),1); % time integral of the joint angles
  m.bigIntTheta=[m.bigIntTheta;m.intTheta{k}]; % time integral of
                                               % joint angles in
                                               % vector form
  m.bigEta = [m.bigEta; m.eta{k}]; % % viscous friction coefficient
                                   % of each joint in (NewtonMeter/radian)/(radian/sec)
  m.bigArmature = [m.bigArmature; m.armature{k}]; % armature of
                                                  % motor driving 
                                                  % each joint. it
                                                  % adds to joint
                                                  % moment of inertia



end
m.bigEta = diag(m.bigEta); % the viscous friction ends up behaving
                           % as an additional diagonal component to
                           % the inertial matrix. See "Numerical
                           % Issues " on the Physics for Robotics
m.bigArmature= diag(m.bigArmature); % the armature of each joint
                                    % adds a diagonal component to
                                    % the moment of inertia
m.cd = cumsum(m.d); 
m.ndfree= sum(m.d);
m.bigTauG=zeros(m.ndfree,1); %graivational torque in vector form 
m.bigCF = zeros(m.ndfree,1); % coriollis torques in vector form
m.bigTauV=zeros(m.ndfree,1); % viscous torques in vector form
m.nt= ceil(m.T/m.dt);
m.l{1} = [0;0;0]; % 
m.I{1}= 0*eye(3);
m.m{1}=0;
m.r{1} = eye(3);
m.s{1} = m.r{1};
m.y{1}=zeros(3,1);
m.x{1} = zeros(3,1);
m.w{1}=zeros(3,m.ndfree);
m.dw{1} = zeros(3,m.ndfree);
m.H{1}=zeros(3,m.ndfree);
m.J{1}=zeros(3,m.ndfree);
m.q{1}=zeros(3,3);
m.u{1} = zeros(3,3);
m.v{1} = zeros(3,3);
m.theta{1} = zeros(3,1);
m.dTheta{1}=zeros(3,1);
m.thetaInt{1} = zeros(3,1);
m.wdTheta{1} = zeros(3,1);
m.dwdTheta{1} = zeros(3,1);
m.dHdTheta{1} = zeros(3,1);
m.cdTheta{1} = zeros(m.ndfree,1);
m = getJacobian(m);
m.error=0;

% set  default numerical integration method
if ~isfield(m, 'integration')
	m.integration='Euler';
end
