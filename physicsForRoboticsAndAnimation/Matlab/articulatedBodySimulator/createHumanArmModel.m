clear
% 7 df anatomically inspired human arm model
%
% Global reference axes
% In matlab x goes left to right. z is down to up. -y is depth towards
% screen +y is depth away from screen
% number of links
% Due to lack of zero-offset in matlab, we reference the world
% frame of reference as f1, frame of reference of first link as f2,
% second link, f3 ... This means the root joint is refered to as
% joint 2, second joint as joint 3 ...
% joint as joint 2, second joint as joint 3 ...
% We use SI Units:
% length: meter
% mass: Kg
% force: Newton
% torque: (Newton Meter)/ radian
% moment of inertia (NewtonMeter/radian)/(radians/sec^2)

%%%%%%%%%% Begin Model Definition %%%%%%%%%%%%%%%

% this is a compond pendulum with 4 links, 4 joints
% f1 = world frame of reference (think of it as the root link)
% f2 = first link frame of reference 
% f3 = second  link frame of reference
% f4 = third link frame of reference
% f5 = fourth link frame of refrence
% f6= fith link frame of reference
% f7= sixth link frame of reference
% f8= seventh link frame of reference


% rotation axes for each joint. 
ha.u{2} = [1 0  0]'; % in world frame of reference
ha.u{3} = [0 1 0]'; % in first link frame of reference
ha.u{4} = [0 0 1]'; % in second link frame of reference
ha.u{5} = [1 0 0]'; % in third link frame of reference
ha.u{6} = [0 1 0]'; % in fourth link frame of reference 
ha.u{7} = [0 1 0]'; % in fifth link frame of reference 
ha.u{8} = [0 0 1]'; % in sixth link frame of reference

% joint coordinates in meters
ha.l{2}= [0.0;0;0]; % in world frame of reference
ha.l{3}= [0.0;0;0]; % in first link frame of reference
ha.l{4}= [0.3;0;0]; % in second link frame of refrence
ha.l{5}= [0.0;0;0]; % in third link frame of reference
ha.l{6}= [0.3;0;0]; % in fourth link frame of rerernce
ha.l{7}= [0.0;0;0]; % in fifth link frame of reference
ha.l{8}= [0.05;0;0];% in sixth link frame of rerence  

% centers of mass of each link on that link's frame of reference 
ha.b{2}=ha.l{2}/2; 
ha.b{3}=ha.l{3}/2; 
ha.b{4}=ha.l{4}/2; 
ha.b{5}=ha.l{5}/2; 
ha.b{6}=ha.l{6}/2; 
ha.b{7}=ha.l{7}/2; 
ha.b{8}=ha.l{8}/2; 

% mass m{i} and moments of inertia I{i} for each link
% using that link's frame of reference
% Here we model each link as an ellipsoid
% and compute the m{i} and I{i}
rho = 1000; % density in Kg/cubicMeters
% lengt in meters of main half axes of the three links
a{2}=[ha.l{2}(1);0.1;0.1]/2; 
a{3}=[ha.l{3}(1);0.1;0.1]/2; 
a{4}=[ha.l{4}(1);0.1;0.1]/2; 
a{5}=[ha.l{5}(1);0.1;0.1]/2; 
a{6}=[ha.l{6}(1);0.1;0.1]/2; 
a{7}=[ha.l{7}(1);0.1;0.1]/2; 
a{8}=[ha.l{8}(1);0.1;0.1]/2; 

for k=2:1:length(a)
  ha.m{k} = rho*pi*4*a{k}(1)*a{k}(2)*a{k}(3)/3; % mass in Kg
  % moment of inertia with respect to center of mass
  % in (NewtonMeter/radian) /(radian/sec^2)
  ha.I{k} = ha.m{k}*[a{k}(2)^2+a{k}(3)^2, 0, 0; 0, a{k}(1)^2+a{k}(3)^2, 0; 0,0, a{k}(1)^2+a{k}(2)^2]/5;
  % Use parallel axis theorem to get moment of inertial with
  % respect to center of rotation
  cr = [ha.l{k}(1)/2;0;0];
  ha.I{k} = ha.I{k} - ha.m{k}*R(cr)*R(cr);
end
% viscous friction of each joint in (NewtonMeter/radian)/(radian/sec)
ha.eta{2} = diag([0.3 ]);
ha.eta{3} = diag([0.3 ]);
ha.eta{4} = diag([0.3 ]);
ha.eta{5} = diag([0.3 ]);
ha.eta{6} = diag([0.3 ]);
ha.eta{7} = diag([0.3 ]);
ha.eta{8} = diag([0.3 ]);

% moment of inertia of armature of each joint  
% (NewtonMeter/radian)/(radian/sec^2)
% it is the rotational inertia produced by the rotational motor
% driving each joint. 
ha.armature{2} = diag([0.05 ]);
ha.armature{3} = diag([0.05 ]);
ha.armature{4} = diag([0.05 ]);
ha.armature{5} = diag([0.05 ]);
ha.armature{6} = diag([0.05 ]);
ha.armature{7} = diag([0.05 ]);
ha.armature{8} = diag([0.05 ]);




% gravitational vector
ha.g=[0;0;-9.80665]; % in Newtons

% initial angles in rads
ha.theta{2} = 0;
ha.theta{3} = pi/4;
ha.theta{4} = 0;
ha.theta{5} = pi/4;
ha.theta{6} = 0;
ha.theta{7} = 0;
ha.theta{8} = 0;


% initial angular velocities in rads/sec
ha.dTheta{2} = 0;
ha.dTheta{3} = 0;
ha.dTheta{4} = 0;
ha.dTheta{5} = 0;
ha.dTheta{6} = 0;
ha.dTheta{7} = 0;
ha.dTheta{8} = 0;


ha.dt = 1/100; % sampling period in seconds 
ha.T = 10; % sihaulation time in secs

ha=robotInit(ha);

ha = robotUpdate(ha);


save('humanArmModel','ha');
