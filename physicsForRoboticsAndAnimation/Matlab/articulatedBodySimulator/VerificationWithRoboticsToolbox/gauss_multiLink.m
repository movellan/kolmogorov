function [m]=simplePendulum(q,qd)
	% Simulate non-planar robot arm using Gauss-Newton Algorithm
	% for notation see the "articulated bodies chapter" of "Physics for
	% Robotics and Animation"
	% Copyright @ Javier R. Movellan, 2012
	% MIT Open Source License.

	% Global reference axes
	% In matlab x goes left to right. z is down to up. -y is depth towards
	% screen +y is depth away from screen
	% number of links

	% Due to lack of zero-offset in matlab, we reference the first
	% joint as joint 2, second joint as joint 3 ...
	% We use SI Units:
	% length: meter
	% mass: Kg
	% force: Newton
	% torque: (Newton Meter)/ radian
	% moment of inertia (NewtonMeter/radian)/(radians/sec^2)
	%%%%%%%%%% Begin Model Definition %%%%%%%%%%%%%%%
	addpath ../

	% rotation axes for each joint. 
	m.u{2} = [0 0 1]'; % 1 df joint
	m.u{3} = [0 1 0]'; % 1 df joint
	m.u{4} = [0 0 1]'; % 1 df joint
	m.u{5} = [0 1 0]'; % 1 df joint

	% link ends at zero rotation
	m.l{2}= [0.4;0;0]; 
	m.l{3}= [0.3;0;0]; 
	m.l{4}= [0.3;0;0]; 
	m.l{5}= [0.3;0;0]; 


	% mass m{i} and moments of inertia I{i} for each link
	% all we need is m{i} and I{i}
	% Here we model each link as an ellipsoid
	% and compute the m{i} and I{i}
	rho = 1000; % density in Kg/cubicMeters
	% lengt in meters of main half axes of the three links
	a{2}=[m.l{2}(1);0.1;0.1]/2; %arm
	a{3}=[m.l{3}(1);0.1;0.1]/2; %arm
	a{4}=[m.l{4}(1);0.1;0.1]/2; %arm
	a{5}=[m.l{5}(1);0.1;0.1]/2; %arm

	% centers of mass at zero rotation
	b{2}=[0.4;  0;  0]/2;
	b{3}=[0.3;  0;  0]/2;
	b{4}=[0.3;  0;  0]/2;
	b{5}=[0.3;  0;  0]/2;
	for k=2:1:5
	  m.b{k} = b{k}; 
	  m.m{k} = rho*pi*4*a{k}(1)*a{k}(2)*a{k}(3)/3; % mass in Kg
	  % moment of inertia with respect to center of mass
	  % in (NewtonMeter/radian) /(radian/sec^2)
	  m.I{k} = m.m{k}*[a{k}(2)^2+a{k}(3)^2, 0, 0; 
						0, a{k}(1)^2+a{k}(3)^2, 0; 
						0,0, a{k}(1)^2+a{k}(2)^2]/5;
	  % Use parallel axis theorem to get moment of inertial with
	  % respect to center of rotation
	  cr = m.b{k}; 
	  m.I{k} = m.I{k} - m.m{k}*R(cr)*R(cr); % shouldn't this be +?

		% viscous friction in (NewtonMeter/radian)/(radian/sec)
		m.eta{k} = diag([0.01]);

		% moment of inertia of armature of each joint  (NewtonMeter/radian)/(radian/sec^2)
		m.armature{k} = diag([0.02]);
	end

	% gravitational vector
	%m.g=[0;0;-9.81];
	m.g=[0;-9.81;0]; 

	m.theta = num2cell([0 q(:)']);

	% initial angular velocities in rads/sec
	m.dTheta = num2cell([0 qd(:)']);

	m.dt = 1/20; % sampling period in seconds 
	m.T = 10; % simulation time in secs

	m.bigMdiag='default';
	m=robotInit(m);
    clf
    robotDisplay(m);
