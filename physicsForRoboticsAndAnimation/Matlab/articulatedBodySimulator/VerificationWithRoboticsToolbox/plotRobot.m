addpath ../VerificationWithRoboticsToolbox
q=[0 0 0 0]
qd=[0 0 0 0];

m=ne_multiLink(q,qd);
set(gca,'FontSize',12)
xlim([-.5 1])
ylim([-1 .5]);
zlim([-1.25 .5])
plot(m,[0 -pi/2 -pi/2 pi/2])



addpath ~/matlablib/
savefig('robot',gcf,'pdf');
