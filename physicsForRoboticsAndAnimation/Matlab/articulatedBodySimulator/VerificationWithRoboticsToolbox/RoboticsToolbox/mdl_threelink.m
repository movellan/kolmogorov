clear L
%
% theta : angle about previous z, from old x to new x
% d     : offset along previous z to the common normal
% a     : length of the common normal (aka a, but if using this notation, do not confuse with α). Assuming a revolute joint, this is the radius about previous z.
% alpha : angle about common normal, from old z axis to new z axis
%             th    d     a     alpha jointType
L(1) = Link([ 0     0    .4     -pi/2 	0], 'standard');
L(2) = Link([ 0 	0    .3     pi/2  	0], 'standard');
L(3) = Link([ 0     0    .2	 	0		0], 'standard');



%link shape
a{1}=[L(1).a;0.1;0.1]/2; %arm
a{2}=[L(2).a;0.1;0.1]/2; % forearm
a{3}=[L(3).a;0.09;0.02]/2; % hand
rho=1000; %density

for k=1:3
  L(k).m = rho*pi*4*a{k}(1)*a{k}(2)*a{k}(3)/3; % mass in Kg
  L(k).r = [a{k}(1);0;0]; %com

  % moment of inertia with respect to center of mass
  % in (NewtonMeter/radian) /(radian/sec^2)
  L(k).I = L(k).m*[a{k}(2)^2+a{k}(3)^2, 0, 0; 0, a{k}(1)^2+a{k}(3)^2, 0; 0,0, a{k}(1)^2+a{k}(2)^2]/5;

  L(k).B = 1; %viscusoty.

  %motor
  L(k).G=0; %gear ratio
  L(k).Jm=0;%armature
end

threelink = SerialLink(L, 'name', 'ThreeLink');
clear L

%%%%% testing kinematics
q=[-0.000979242618803   0.002057273225793  -0.002107866077703];
q=[0 0 0];
nL=length(threelink.links)
y = zeros(3,nL);
for i=1:nL
	subq = q(1:i);
	subL=SerialLink(threelink.links(1:i));
	y(:,i)=transl(subL.fkine(subq));
	subL.inertia(subq)
end
y
