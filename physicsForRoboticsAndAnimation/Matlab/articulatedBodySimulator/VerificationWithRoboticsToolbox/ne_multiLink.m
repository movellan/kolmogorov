function [pendulum]=mdl_simplePendulum(q,qd)
	addpath ./RoboticsToolbox/rvctools/
	startup_rvc
	clear L
	%
	% theta : angle xx @ z
	% d     : offset along previous z to the common normal
	% a     : length of the common normal (aka a, but if using this notation, do not confuse with α). Assuming a revolute joint, this is the radius about previous z.
	% alpha : angle about common normal, from old z axis to new z axis
	%             th    d     a     alpha jointType
	L(1) = Link([ 0     0     .4   -pi/2   0], 'standard');
	L(2) = Link([ 0     0     .3   pi/2   0], 'standard');
	L(3) = Link([ 0     0     .3   -pi/2   0], 'standard');
	L(4) = Link([ 0     0     .3   pi/2   0], 'standard');

	%link shape
	a{1}=[0.4;0.1;0.1]/2; %arm
	a{2}=[0.3;0.1;0.1]/2; %arm
	a{3}=[0.3;0.1;0.1]/2; %arm
	a{4}=[0.3;0.1;0.1]/2; %arm

	b{1}=[0.4;  0;  0]/2-[0.4;0;0];
	b{2}=[0.3;  0;  0]/2-[0.3;0;0];
	b{3}=[0.3;  0;  0]/2-[0.3;0;0];
	b{4}=[0.3;  0;  0]/2-[0.3;0;0];
	rho=1000; %density

	for k=1:4
	  L(k).m = rho*pi*4*a{k}(1)*a{k}(2)*a{k}(3)/3; % mass in Kg
	  L(k).r = b{k}; %CoMwrsp to joint i

	  % moment of inertia with respect to center of mass
	  % in (NewtonMeter/radian) /(radian/sec^2)
	  L(k).I = L(k).m*[	a{k}(2)^2+a{k}(3)^2, 0, 0; 
						0, a{k}(1)^2+a{k}(3)^2, 0; 
						0,0, a{k}(1)^2+a{k}(2)^2]/5;

	  L(k).B = 0.01; %viscusoty

	  %motor
	  L(k).G=1; %gear ratio
	  L(k).Jm=0.02;%armature
	end

	pendulum= SerialLink(L(1:3), 'name', 'robot');
	pendulum.gravity=[0 9.81 0]';

	%%%%% testing kinematics
	y = transl(pendulum.fkine(q));
	pendulum.inertia(q);
	clf
	plot(pendulum,q);
