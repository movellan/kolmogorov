clear
% ext toruqe
% bigM, bigC, tauG, Kinematics, Armature

rand('seed',1);
q=rand(1)*pi;
qd=rand(1)*pi;
tau=[0];

if 1
	disp('---Gauss---------');
	figure(1)
	mg=gauss_singlePendulum(q,qd);
	mg.tau = num2cell([0 tau(:)']);
	[acc mg] = getAccelerations(mg);

	bigM=mg.bigM
	bigCF=mg.bigCF'
	bigTauG=mg.bigTauG'
	yend = mg.y{end}'
	Jend=mg.H{end}
	acc'
end

disp('---NewtonEuler---');
figure(2)
mne=ne_singlePendulum(q,qd);

bigM=mne.inertia(q)
bigC=mne.coriolis(q,qd)
%bigCFi=mne.coriolis(q,qd)*qd'
bigCFd=mne.nofriction('all').rne(q, qd, zeros(size(q)), [0 0 0]')
bigTauG=mne.gravload(q)
yend= transl(mne.fkine(q))'
Jend = mne.jacob0(q,'trans')
acc=mne.accel(q, qd, tau)'
