clear
% ext toruqe
% bigM, bigC, tauG, Kinematics, Armature

%q=[0 1];
%rand('seed',1);
q=rand(1,4)*pi;
qd=[ rand(1,4)*pi ];
%qd=[1 0];
tau=rand(1,4);

if 1
	disp('---Gauss---------');
	figure(1)
	mg=gauss_multiLink(q,qd);
	mg.tau = num2cell([0 tau(:)']);
	[acc1 mg] = getAccelerations(mg);

	bigM=mg.bigM
	bigCF=mg.bigCF'
	bigTauG=mg.bigTauG'
	yend = mg.y{end}'
	Jend=mg.H{end}
	acc1'
end

disp('---NewtonEuler---');
figure(2)
mne=ne_multiLink(q,qd);

bigM=mne.inertia(q)
bigC=mne.coriolis(q,qd)
%bigCFi=mne.coriolis(q,qd)*qd'
bigCFd=mne.nofriction('all').rne(q, qd, zeros(size(q)), [0 0 0]')
bigTauG=mne.gravload(q)
yend= transl(mne.fkine(q))'
Jend = mne.jacob0(q,'trans')
acc2=mne.accel(q, qd, tau)'
