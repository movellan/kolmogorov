function displayContactForces(m,contactLinks,contactPoints, ...
			      contactForces)
nPoints=size(contactLinks,2);


for k=1:nPoints
  l=contactLinks(k);
  q= m.y{l-1}+ m.s{l}*contactPoints(:,k);
  c=0.2*contactForces/norm(contactForces);
  line([q(1) q(1)+c(1)], [q(2) q(2)+c(2)], [q(3) q(3)+c(3)],'LineWidth',1,'Color','g')
end

