function  robotDisplay(m)
  
    
  % Plot things
  line([0 1], [0 0], [0 0],'Color','b');

  hold on;
  line([0 0], [0 1], [0 0],'Color','r');
  line([0 0], [0 0], [0 1],'Color','g');

  plot3(m.y{1}(1), m.y{1}(2), m.y{1}(3), 'ro','MarkerSize',5,'LineWidth',5);
  for i=2:m.njoints+1
    line([m.y{i-1}(1) m.y{i}(1)], [m.y{i-1}(2) m.y{i}(2)], [m.y{i-1}(3) m.y{i}(3)],'LineWidth',6)
    vp = m.v{i}*m.theta{i};
    if norm(vp)>0
      vp = 0.1*vp/norm(vp);
      line([m.y{i-1}(1)-vp(1), m.y{i-1}(1) + vp(1)], [m.y{i-1}(2)-vp(2), m.y{i-1}(2)+vp(2)], ...
	   [m.y{i-1}(3)-vp(3),m.y{i-1}(3)+vp(3)],'Color','k','LineWidth',2)
      plotCircle3D(m.y{i-1}',vp',0.1);	
    end
    
    plot3(m.y{i}(1), m.y{i}(2), m.y{i}(3), 'ro','MarkerSize',5,'LineWidth',5);
    
  end

    xlabel('X')
    ylabel('Y')
    zlabel('Z')
    xlim([-0.5  0.5]);
    ylim([-0.5 0.5]);
    zlim([-0.5 0.5]);
    view(3)
