clear
load humanArmModel
%load robotController
load exp1Controller
%load thetaGoal
load exp1Theta
for k=2:7
ha.theta{k} = thetaGoal(k-1,1);
ha.dTheta{k}= 0;
end

ha = robotInit(ha);

x(:,1) = [ha.bigTheta;zeros(14,1)];
for t=1:length(rc.xi)
 y(:,t) = ha.y{7}; 
 
 ha = robotUpdate(ha);   
 
 acc = rc.w1{t}+rc.w2{t}*x(:,t);

 tau = ha.bigM*acc -  ha.bigTauG -ha.bigTauV +ha.bigCF;

 
 for k=2:ha.njoints+1
    ha.tau{k} = tau(k-1);
  end
 
  x(1:7,t+1)= ha.bigTheta;
  x(8:14,t+1) = x(8:14,t)+ x(1:7,t+1)*ha.dt;
  x(15:21,t+1) = (x(1:7,t+1) - x(1:7,t))/ha.dt;
  robotDisplay(ha);
  drawnow

  clf
end
