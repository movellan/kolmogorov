% input is a robot model 
% if one output only, then it is the vector of joint accelerations
% if two outputs, the first is the vector of accelerations, and the
% second is the updated model.

function [c] = getSystemIdCoeff(m,ddThetaVec)
debug=0;

[D3 D6] = getDMatrices();

m.bigM=zeros(m.ndfree,m.ndfree); % moment of inertia matrix  
m.bigCF=zeros(m.ndfree,1); % apparent (coriolis) torque vector
m.bigTauG=zeros(m.ndfree,1); % gravitational torque vector
m.bigTauV=[];
m.bigTauS=[];

errTh=1e-10; % for floating point comparison in asserts
for i=2:m.njoints+1 
  %%%%%%%%% compute global inertial matrix %%%%%%%%%%%
  m.r{i} = expm(R(m.u{i}*m.theta{i})); % rotation of each joint
  m.s{i} = m.s{i-1}*m.r{i}; % cumulative rotation
  m.v{i} = m.s{i-1} * m.u{i};
  m.w{i} = m.w{i-1}; 
  m.w{i}(:,m.cd(i-1)+1:m.cd(i))= m.v{i};
  m.y{i} = m.y{i-1} + m.s{i}*m.l{i};
%  m.x{i} = m.y{i-1} + m.s{i}*m.b{i};
  m.H{i} = m.H{i-1} +(R(m.y{i} - m.y{i-1}))' * m.w{i};
%  m.J{i} = m.H{i-1} +(R(m.x{i} - m.y{i-1}))' * m.w{i};

  
  m.a1{i} = m.H{i-1}'*m.H{i-1}*ddThetaVec;

  p1 = m.H{i-1}'*m.s{i};
  p2 = m.s{i}'*m.w{i}*ddThetaVec;
  p3 = m.w{i}'*m.s{i};
  p4 = m.s{i}'*m.H{i-1}*ddThetaVec;
  m.a2{i} = -kron(p2',p1) + kron(p4',p3);

  p5 = m.w{i}'*m.s{i};
  p6 = m.s{i}'*m.w{i}*ddThetaVec;
  m.a3{i} =  kron(p6',p5);

  %verification code
  if debug
	  [dummy m2]=getAccelerations(m);
	  MddTheta1=m2.M{i}*ddThetaVec ;
	  MddTheta2=m.a1{i}*m.m{i} + m.a2{i}*D3*m.m{i}*m.b{i}+ m.a3{i}*m.I{i}(:);
	  assert(norm(MddTheta1-MddTheta2)<errTh)
  end

  %%%%%%%%  compute Coriolis force vector %%%%%%%%%%
  m.wdTheta{i} = m.wdTheta{i-1} + m.v{i} *m.dTheta{i};
  m.dy{i} = R(m.y{i}-m.y{i-1})'*m.wdTheta{i};
  m.q{i} = m.q{i-1} + R(m.v{i-1}*m.dTheta{i-1});
  m.dv{i} = m.q{i}*m.v{i};
  m.dwdTheta{i} = m.dwdTheta{i-1} + m.dv{i} *m.dTheta{i};

  m.dHdTheta{i} =m.dHdTheta{i-1} +R(m.dy{i})'*m.wdTheta{i};
  m.dHdTheta{i} =m.dHdTheta{i} +R(m.y{i}-m.y{i-1})'*m.dwdTheta{i};

  m.a4{i} = m.H{i-1}'*m.dHdTheta{i-1};
  m.q0{i} = m.H{i-1}'*R(m.wdTheta{i})*R(m.wdTheta{i})*m.s{i};
  q1 = m.H{i-1}'*m.s{i};
  q2 = m.s{i}'*m.dwdTheta{i}; 
  q3 = m.w{i}'*m.s{i};
  q4 = m.s{i}'*m.dHdTheta{i-1};

  m.a5{i} = -kron(q2',q1) + kron(q4',q3);

  q5 = m.w{i}'*R(m.wdTheta{i})*m.s{i};
  q6 = m.s{i}'* m.wdTheta{i};
  q7 = m.w{i}'*m.s{i};
  q8 = m.s{i}'*m.dwdTheta{i};

  m.a6{i} = kron(q6',q5) + kron(q8',q7);

  %verification code
  if debug
	  [dummy m2]=getAccelerations(m);
	  CF1=m2.cF{i};
	  CF2=m.a4{i}*m.m{i} + (m.q0{i}+m.a5{i}*D3)*m.m{i}*m.b{i} + m.a6{i}*m.I{i}(:);
	  assert(norm(CF1-CF2)<errTh);
  end

  %%%%%%%% compute gravitational torques %%%%%%%
  m.a7{i} = m.H{i-1}'*m.g;
  h1=m.w{i}'*m.s{i};
  h2=m.s{i}'*m.g;
  m.a8{i} = kron(h2',h1);

  %verification code
  if debug
	  [dummy m2]=getAccelerations(m);
	  tauG1 = m2.tauG{i};
	  tauG2 = m.a7{i}*m.m{i} +m.a8{i}*D3*m.m{i}*m.b{i};
	  assert(norm(tauG1-tauG2)<errTh);
  end
  
  %%%%%%%% compute viscous torques %%%%%%%%%%%%%
  m.a9{i}= -m.dTheta{i};


  %verification code
  if debug
	  [dummy m2]=getAccelerations(m);
	  tauV1 = m2.tauV{i};
	  tauV2 = m.a9{i}*m.eta{i};
	  assert(norm(tauV1-tauV2)<errTh);
  end
end

%m.a10 = m.bigArmature * ddThetaVec;
c1=[];c2=[];c3=[];c4=zeros(m.njoints);
for i=2:1+m.njoints
	c1 = [c1 (m.a1{i}+ m.a4{i} - m.a7{i})];
	c2 = [c2 (m.a2{i}+ m.a5{i} - m.a8{i})*D3+m.q0{i}];
	c3 = [c3 (m.a3{i}+ m.a6{i})*D6];
	c4(i-1,i-1) =(-m.a9{i});
end
c=[c1 c2 c3 c4];

