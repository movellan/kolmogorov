% input is a robot model, including current joint angles, angular
% accelerations and toques  
% Output is the updated angles and angular accelerations.
% update is done using a 4th order Runge-Kutta method

function rm = robotUpdate(rm)



switch(rm.integration)

 case 'RK' % This method needs to be checked. May be implemented
           % incorrectly at this point
        % This is the Runge-Kutta 4th order method, also known as
        % RK4. For an explanation see the numerical methods section
        % of the Physics for Robotics and Animation tutorial 
	% the RK4 method updates the state based on the weighted
        % average of 4 updates, named k1,k2,k3,k4. 

	% first we compute the k1,k2,k3,k4 values for accelerations
        % and for velocities
	rm2 =rm; % keep the original model intact. 
	k1A = getAccelerations(rm2); % k1 term for the acceaccelerations
	k1V= rm2.dTheta; % k1 term for the velocities
	for k=2:rm.njoints+1
	  rm2.theta{k} = rm.theta{k}+ k1V{k}*rm.dt/2;
	  rm2.dTheta{k} = rm2.dTheta{k} + k1A(k-1)*rm.dt/2;
	end
	k2A = getAccelerations(rm2);
	k2V= rm2.dTheta;
	for k=2:rm.njoints+1
	  rm2.theta{k} = rm.theta{k}+ k2V{k}*rm.dt/2;
	  rm2.dTheta{k} = rm2.dTheta{k} + k2A(k-1)*rm.dt/2;
	end
	k3A = getAccelerations(rm2);
	k3V = rm2.dTheta;
	for k=2:rm.njoints+1
	  rm2.theta{k} = rm.theta{k}+ k3V{k}*rm.dt;
	  rm2.dTheta{k} = rm2.dTheta{k} + k3A(k-1)*rm.dt;
	end
	k4A = getAccelerations(rm2);
	k4V = rm2.dTheta;

	% next we update the angles and angular velocities of the input
	% model using the weighted sums of k1,k2,k3,k4 accordign to
        % the RK4 algorithm 

	for k=2:rm.njoints+1
	  % angular accelerations
	  rm.bigddTheta(k-1,1)= (k1A(k-1)+ 2*k2A(k-1)+2*k3A(k-1)+k4A(k-1))/6;
	  rm.dTheta{k} = rm.dTheta{k} +rm.dt*rm.bigddTheta(k-1,1);
	  % angular velocities
	  rm.bigdTheta(k-1,1)= (k1V{k}+ 2*k2V{k}+2*k3V{k}+ k4V{k})/6;
	  % angles
	  rm.theta{k} = rm.theta{k} +rm.dt*rm.bigdTheta(k-1,1);
	  rm.bigTheta(k-1,1)=rm.theta{k};
	  % angular integrals
	  rm.intTheta{k} = rm.intTheta{k}+ rm.theta{k}*rm.dt;
	  rm.bigIntTheta(k-1,1)= rm.intTheta{k};
	end
	% next we update the model, including coriolis friction and gravitational
	% forces 
	[acc rm] = getAccelerations(rm);
case 'Euler' % Default Euler's forward method is faster but less accurate,
             % order 1 instead of order 4 like the RK4 method
	[ddThetaVec rm] = getAccelerations(rm);
    % update angles and angular velocities 
    rm.bigTheta=[];
    for k=2:rm.njoints+1
      rm.ddTheta{k} = ddThetaVec(rm.cd(k-1)+1:rm.cd(k-1)+rm.d(k));
	  rm.bigddTheta(k-1,1)= rm.ddTheta{k};
    
      rm.dTheta{k} = rm.dTheta{k}+ rm.dt*rm.ddTheta{k};
	  rm.bigdTheta(k-1,1)= rm.dTheta{k};

      rm.theta{k} = rm.theta{k}+ rm.dt*rm.dTheta{k} +0.5*rm.dt*rm.dt* rm.ddTheta{k};
	  rm.bigTheta(k-1,1)=rm.theta{k};

    end
end

%reset torques to zero
for k=2:rm.njoints+1
  rm.tau{k} =  0;
end

