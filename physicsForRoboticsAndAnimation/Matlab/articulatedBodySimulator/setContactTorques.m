% given a set of contact points and of forces applies to those
% points, computes the resulting contact torques on each joint
%
% input is a robot model m
% a set of contactPoints and a set of contactForces
% contactLinks is a nx1 vector with the indexes of the links were
% contact happened 
% contactPoints is a 3 x n matrix.
% each column of contactPoints is a different contact point
% and has  the coordinates of the contact points in the
% frame of reference of the link where the contact happened
%
% contactForces is a 3xn vector. Each column corresponds to a
% contact point. The contact forces are given in world coordinates


function m = setContactTorques(m,contactLinks,contactPoints,contactForces)

nPoints = size(contactPoints,2);
for k1= 1:nPoints
  J = getJacobianOfPoint(m,contactLinks(k1),contactPoints(:,k1));
  tau= J'*contactForces(:,k1);
  
  for k=2:m.njoints+1
    m.tau{k} =  m.tau{k} +tau(k-1);
  end
  
end


