% computes the world coordinate veolocity   of a point
% attached to a link
% m is the robot model
% l is an integer indicating the link where the point is attached
% p are the coordinates of the point in the link's frame of reference
function v = getWorldVelocityOfPoint(m,l,p)

J = getJacobianOfPoint(m,l,p);
v = J*m.bigdTheta;

  
