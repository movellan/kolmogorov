clf
clear
path(path,'../../../Control/Matlab/ContinuousTimeLQT')



rc.tau =inf; 
           
          
load exp1Theta;
load exp1ArmModel


rc.T = ha.T; % in seconds

rc.dt = ha.dt; % time step in seconds





s=length(thetaGoal);

ndf = ha.ndfree;

nx=3*ndf;
nu=ndf;

rc.xi{1}(1:ndf,1)=thetaGoal(:,1); % joint angles
rc.xi{1}(ndf+1:ndf*2,1)=0 ; %integral
rc.xi{1}(2*ndf+1:3*ndf,1)=0 ; %derivative
                           

			  
for t=1:s
  rc.xi{t}(1:ndf,1)=thetaGoal(:,t) ; 
    
    if (t>1)
      rc.xi{t}(ndf+1:2*ndf,1)= rc.xi{t-1}(ndf+1:2*ndf,1)+rc.xi{t}(1:ndf,1)*rc.dt; %integral
      rc.xi{t}(2*ndf+1:3*ndf,1)= (rc.xi{t}(1:ndf,1)-rc.xi{t-1}(1:ndf,1))/rc.dt; % derivative
    end

  rc.omega{t} =zeros(nu,1); % action cost is (u_t - omega_t)' q_t (u_t - omega_t)
  
  rc.k{t} = zeros(nx,1); %  a constant external force applied to the system
  rc.a{t} = [zeros(ndf), zeros(ndf),eye(ndf); eye(ndf),zeros(ndf),zeros(ndf);zeros(ndf),zeros(ndf),zeros(ndf)];
  
  rc.b{t} = [zeros(ndf);zeros(ndf); eye(ndf)]; % the action is a force. i.e, it has an effect on the
  
  
  rc.c{t} = 0*eye(nx);
  rc.q{t} = eye(nu);
  
  
  rc.p1{t} = 5000*eye(nx);

  
  rc.p2{t} = zeros(nx,1); % linear state cost
 
  rc.w1{t} = zeros(nu,1);
  rc.w2{t}= zeros(nu,nx);
  
end

rc = lqt3(rc);


save('exp1Controller','rc');




% % Here we run the system with the optimal controller

u = zeros(nu,s);
sqdt = sqrt(rc.dt);
v=0;
x{1}= rc.xi{1};
for t=1:s-1
  u(:,t)= rc.w1{t}+ rc.w2{t} * x{t};
  dv= - (x{t}-rc.xi{t})'*rc.p1{t}*(x{t}-rc.xi{t}) + 2*rc.p2{t}'*x{t};
  dv= dv - (u(:,t)-rc.omega{t})'*rc.q{t}*(u(:,t)-rc.omega{t});
  dv = dv*exp(-(t-1)*rc.dt/rc.tau)*rc.dt;
  v = v+dv;
  dx = (rc.k{t}+rc.a{t}*x{t}  +rc.b{t}*u(:,t))*rc.dt ;
  x{t+1} = x{t}+ dx;
 end
v 


 
 mxi = cell2mat(rc.xi);
 
 mx = cell2mat(x);
 subplot(221)
  plot([1:s]*rc.dt,mx(1,:),[1:s]*rc.dt,mxi(1,:))
 subplot(222)
 plot([1:s]*rc.dt,mx(2,:),[1:s]*rc.dt,mxi(2,:))
 

 
 
 

 subplot(223)
 plot([1:s]*rc.dt,u(1,:));
 hold on
 plot([0 s*rc.dt], [0 0], 'r--');

  subplot(224)
 plot([1:s]*rc.dt,u(2,:));
 hold on
 plot([0 s*rc.dt], [0 0], 'r--');
pause

 %%%%%%%%% now we include the robot dynamics
 




