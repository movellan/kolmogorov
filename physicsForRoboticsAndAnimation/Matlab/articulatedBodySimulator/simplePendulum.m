% Simulate simple pendulum 
% for notation see the "articulated bodies chapter" of "Physics for
% Robotics and Animation"
% Copyright @ Javier R. Movellan, 2012
% MIT Open Source License.

clear
clf
% Global reference axes
% In matlab x goes left to right. z is down to up. -y is depth towards
% screen +y is depth away from screen
% number of links

% Due to lack of zero-offset in matlab, we reference the world
% frame of reference as f1, frame of reference of first link as f2,
% second link, f3 ... This means the root joint is refered to as
% joint 2, second joint as joint 3 ...
% joint as joint 2, second joint as joint 3 ...
% We use SI Units:
% length: meter
% mass: Kg
% force: Newton
% torque: (Newton Meter)/ radian
% moment of inertia (NewtonMeter/radian)/(radians/sec^2)

%%%%%%%%%% Begin Model Definition %%%%%%%%%%%%%%%

% f1 (world)   coordinates of axis of rotation of parent joint for
% the first link 
m.u{2} = [ 0 1 0]'; % first joint

% f2 (first joint) coordinates of child joint of the first link
m.l{2}= [0.3;0;0]; 

% f2 (first joint) coordinates of the center of mass of the first
% link 

m.b{2}=m.l{2}/2; 

% mass m{i} and moments of inertia I{i} for each link

% Here we model each link as an ellipsoid
% and compute the m{i} and I{i}
rho = 1000; % density in Kg/cubicMeters
% length in meters of main half axes of the three links
a{2}=[m.l{2}(1);0.1;0.1]/2; %arm
for k=2:1:2
  m.m{k} = rho*pi*4*a{k}(1)*a{k}(2)*a{k}(3)/3; % mass in Kg
  % moment of inertia with respect to center of mass
  % in (NewtonMeter/radian) /(radian/sec^2)
  m.I{k} = m.m{k}*[a{k}(2)^2+a{k}(3)^2, 0, 0; 0, a{k}(1)^2+a{k}(3)^2, 0; 0,0, a{k}(1)^2+a{k}(2)^2]/5;
  % Use parallel axis theorem to get moment of inertial with
  % respect to center of rotation
  cr = [m.l{k}(1)/2;0;0];
  m.I{k} = m.I{k} - m.m{k}*R(cr)*R(cr);
end
% viscous friction of each joint in (NewtonMeter/radian)/(radian/sec)
m.eta{2} = diag(0.01);

% moment of inertia of armature of each joint  (NewtonMeter/radian)/(radian/sec^2)
m.armature{2} = diag([0.01 ]);

% gravitational vector
m.g=[0;0;-9.80665]; % in Newtons

% initial angles
m.theta{2} = pi/4;

% initial angular velocities in rads/sec
m.dTheta{2} = 0;

m.dt = 1/20; % sampling period in seconds 
m.T = 10; % simulation time in secs

m=robotInit(m);
for t=1:100000
 t
% set torques and get accelerations
  for k=2:m.njoints+1
    m.tau{k} = 0.000*randn(m.d(k),1);
  end
  m = robotUpdate(m);   

  robotDisplay(m);    
  drawnow
  clf
  
    
  
end
