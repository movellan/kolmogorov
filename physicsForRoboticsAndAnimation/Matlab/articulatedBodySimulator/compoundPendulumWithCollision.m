%%%% DRAW CONTACT FORCES %%%%%%
% Simulate compound pendulum 
% end of the pendulum colides with a vertical viscous object
% we measure how much the object penetrates
% this gives us an indication of the force that the point put on
% the object at the poit and time of contact

% for notation see the "articulated bodies chapter" of "Physics for
% Robotics and Animation"
% Copyright @ Javier R. Movellan, 2012
% MIT Open Source License.

clear
clf

% Global reference axes
% In matlab x goes left to right. z is down to up. -y is depth towards
% screen +y is depth away from screen
% number of links


% Due to lack of zero-offset in matlab, we reference the world
% frame of reference as f1, frame of reference of first link as f2,
% second link, f3 ... This means the root joint is refered to as
% joint 2, second joint as joint 3 ...
% joint as joint 2, second joint as joint 3 ...
% We use SI Units:
% length: meter
% mass: Kg
% force: Newton
% torque: (Newton Meter)/ radian
% moment of inertia (NewtonMeter/radian)/(radians/sec^2)

%%%%%%%%%% Begin Model Definition %%%%%%%%%%%%%%%

% this is a compond pendulum with 4 links, 4 joints
% f1 = world frame of reference (think of it as the root link)
% f2 = first link frame of reference 
% f3 = second  link frame of reference
% f4 = third link frame of reference
% f5 = fourth link frame of refrence

% rotation axes for each joint. 
m.u{2} = [1 0 0]'; % axis of first joint in f1 (world) reference frame
m.u{3} = [0 1 0 ]'; % axis of second joint in f2 (first link)
                   % reference freame
m.u{4} = [0 0 1]'; % axis of thrid joint in f3 (second link) reference
m.u{5} = [1 0 0]'; % axis of 4th joint in f4 (third link )
                   % reference
		   
m.u{6} = [0 1 0]'; % axis of 4th joint in f4 (third link )

m.u{7} = [0 0 1]'; % axis of 4th joint in f4 (third link )

% joint coordinates in meters
m.l{2}= [0.0;0;0]; % first joint in world coordinates
m.l{3}= [0.0;0;0]; % second joint in first link coordinates
m.l{4}= [0.4;0;0]; % third joint in second link coordinates
m.l{5}= [0.0;0;0]; % 4th joint in third link coordintes 
m.l{6}= [0.0;0;0]; % 4th joint in third link coordintes 
m.l{7}= [0.2;0;0]; % 4th joint in third link coordintes 

% centers of mass of each link on that link's frame of reference
for k=2: length(m.l)
  m.b{k}=m.l{k}/2; 
end

% mass m{i} and moments of inertia I{i} for each link
% Here we model each link as an ellipsoid
% and compute the m{i} and I{i}
rho = 1000; % density in Kg/cubicMeters
% length in meters of main half axes of the three links
for k=2: length(m.l)
  a{k}=[m.l{k}(1);0.1;0.1]/2; 
  m.b{k}=m.l{k}/2; 
end

for k=2:1:length(m.l)
  m.m{k} = rho*pi*4*a{k}(1)*a{k}(2)*a{k}(3)/3; % mass in Kg
  % moment of inertia with respect to center of mass
  % in (NewtonMeter/radian) /(radian/sec^2)
  m.I{k} = m.m{k}*[a{k}(2)^2+a{k}(3)^2, 0, 0; 0, a{k}(1)^2+a{k}(3)^2, 0; 0,0, a{k}(1)^2+a{k}(2)^2]/5;
  % Use parallel axis theorem to get moment of inertial with
  % respect to center of rotation
  cr = [m.l{k}(1)/2;0;0];
  m.I{k} = m.I{k} - m.m{k}*R(cr)*R(cr);
end
% viscous friction of each joint (NewtonMeter/radian)/(radian/sec)
for k=2:1:length(m.l)
  m.eta{k} = diag([1 ]);
end
% moment of inertia of armature of each joint  
% (NewtonMeter/radian)/(radian/sec^2)
% it is the rotational inertia produced by the rotational motor
% driving each joint. 
for k=2:1:length(m.l)
m.armature{k} = diag([0.01 ]);
end




% initial angles in radians
m.theta{2} = 0;
m.theta{3} = 0;
m.theta{4} = 0;
m.theta{5} = 0;
m.theta{6} = 0;
m.theta{7} = 0;

% initial angular velocities in rads/sec
for k=2:1:length(m.l)
m.dTheta{k} = 0;
end


% gravitational vector
m.g=[0;0;-9.80665]; % in Newtons


m.dt = 1/100; % sampling period in seconds 
m.T = 2; % simulation time in secs

m=robotInit(m);
maxPenetration=0;
for t=1:ceil(m.T/m.dt)
 display(sprintf('time = %4.2f secs. penetration = %f centimeters', ...
		 t*m.dt, maxPenetration*100))

 
 
 contactLink=7;
 contactPoint=m.l{contactLink};
 contactForce=[0;0;0];
 
 contactPointWorldCoords =getWorldLocationOfPoint(m,contactLink, ...
						    contactPoint);
 
 if contactPointWorldCoords(1) <-maxPenetration% detect collision with viscous object
   penetration = abs(contactPointWorldCoords(1));
   if penetration > maxPenetration
     maxPenetration= penetration;
   end
   v=getWorldVelocityOfPoint(m,contactLink,contactPoint);
   directionViscousContact= [1;0;0];
   vx= directionViscousContact'*v;
   if vx <0
     viscosity=20; % we hit a nail, it exerts viscous force on us only in one direction
   else
     viscosity =0;
   end
   contactForce=-directionViscousContact*vx*viscosity;
 end

 m=setContactTorques(m,contactLink,contactPoint,contactForce);
  robotDisplay(m);
 displayContactForces(m,contactLink,contactPoint,contactForce);
 line([-maxPenetration,-maxPenetration-0.1],[0 0] ,[-0.6, -0.6],'Color','k','LineWidth',2)
 plotCircle3D([-0.1,0,-0.6],[1 0 0],0.05);	
 m = robotUpdate(m);   
 drawnow
 if t*m.dt >0.7
 %  pause
 end
 
 clf
 
end
