function lambdaReal = model2lambda(m)
		lambdaReal =[];
		if isfield(m,'mb')
			fprintf('model has mb set\n');
		end
		for i=2:1+m.njoints
			lambda_m= m.m{i};
			if isfield(m,'mb')
				lambda_mb= m.mb{i};
			else
				lambda_mb= m.m{i}*m.b{i};	
			end
			lambda_I= m.I{i}([1,2,3,5,6,9]');
			lambda_eta =m.eta{i};
			lambdaReal=[lambdaReal;{lambda_m,lambda_mb,lambda_I,lambda_eta}];
		end
		lambdaReal = cat(1, lambdaReal{:});

