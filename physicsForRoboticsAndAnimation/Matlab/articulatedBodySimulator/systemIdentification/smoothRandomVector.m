function tau=smoothRandomVector(scales, nLen)
			nDim=length(scales);
			tau = nan(nDim,nLen);
			for i=1:nDim
				trj=scales(i)*ones(nLen/10,1)*randn(1,10);
				trj=smooth(trj(:),nLen*0.1,'lowess')';
				tau(i,:)=trj;
			end
