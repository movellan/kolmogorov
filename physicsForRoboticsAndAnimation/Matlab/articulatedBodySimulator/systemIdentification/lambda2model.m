function m= lambda2model(m, lambda)
		
		nParamPerJoint=1+3+6+1;
		assert(length(lambda) == nParamPerJoint*m.njoints);
		
		lambda_m =  lambda(1:m.njoints);
		lambda_mb = reshape(lambda(m.njoints+1:4*m.njoints), 3,[]);
		lambda_I =  reshape(lambda(4*m.njoints+1:10*m.njoints), 6,[]);
		lambda_eta = lambda(10*m.njoints+1:end);

		[D3 D6] = getDMatrices();

		for i=1:m.njoints
			%lambda_m= m.m{i};
			m.m{i+1} = lambda_m(i);
			m.b{i+1} = lambda_mb(:,i)/lambda_m(i);
			m.mb{i+1} = lambda_mb(:,i); %the actual identified parameter
			m.I{i+1} = reshape(D6*lambda_I(:,i),[3 3]);
			m.eta{i+1} = lambda_eta(i);
		end
