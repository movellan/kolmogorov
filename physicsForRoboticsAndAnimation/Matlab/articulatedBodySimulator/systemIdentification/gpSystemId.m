function gpSystemId()
	rng('default');

	%generate some traces, 
	% trX : (q,qd,qdd)
	% trY : tau
	q0=[-pi/4 1 1];
	qd0=[1 1 1];
	global model
	model = gauss_multiLink(q0,qd0);

	if 1
		%grid joint angles
		nSpacing=5;
		nTrace=nSpacing^model.njoints;
		tau = zeros(model.njoints, nTrace);
		[q3 q2 q1]=ndgrid( linspace(0,pi,nSpacing), linspace(0,pi,nSpacing), linspace(-pi/2,pi/2,nSpacing));
		q=[q1(:) q2(:) q3(:)]'; 
		qd=zeros(model.njoints,nTrace);
		qdd=zeros(model.njoints,nTrace);
		mvar=model;
		for i=1:length(q)
			mvar.theta = num2cell([0 q(:,i)']);
			mvar.dTheta = num2cell([0 qd(:,i)']);
			mvar.ddTheta = num2cell([0 qdd(:,i)']);
			[dummy m2]=getAccelerations(mvar);
			%clf;robotDisplay(m2);drawnow
			tauf=zeros(mvar.njoints,1);
			if q(1,i)>0
				tauf(1)=tauf(1)+5;
			end
			tau(:,i)=m2.bigM*qdd(:,i) + m2.bigCF - m2.bigTauG -m2.bigTauV - tauf;
		end
	else
		%random torque
		tau = smoothRandomVector([3,1,1],nTrace);
		[q qd qdd] = generateTraces(model, tau, 1);%display=0
	end

	%display
	figure(1);
	clf
	subplot(2,1,1);
	plot(q')
	legend('1','2','3')
	title('q')
	%subplot(4,1,2);
	%plot(qd')
	%subplot(4,1,3);
	%plot(qdd')
	subplot(2,1,2);
	plot(tau')
	title('tau')


	model.idx=1;
	%permIdx = randperm(nTrace);
	permIdx = 1:nTrace;
	trIdx=permIdx([1:floor(nTrace/3) end-10:end]);
	trIdx = setxor(trIdx, 1:20:nTrace);
	%ttIdx=setdiff(permIdx, trIdx);
	ttIdx=permIdx;

	X = [q;qd;qdd];
	%X = [q];
	X=X+.001*randn(size(X));

	trX=X(:,trIdx)';
	trY = tau(model.idx,trIdx)';
	ttX=X(:,ttIdx)';
	ttYreal = tau(model.idx,ttIdx)';
	
	
	methods={'PR','GP','HGP'};
	caps={'PR','GP','HGP','PRGP'};
	%lgds = {'actual dynamics','observations'};
	col = {[.8,0,0],[0,.5,0],[0 0 1],[0,.75,.75],[.7,0,.5]};                % colors
	%lsty={'--','-'}
	%clf
	%plot(ttX,func(ttX),'Color',[.6 .6 .6],'LineWidth',4);hold on
	%plot(trX,trY,'ko')
	figure(2)
	for m=1:length(methods)
		if length(methods{m})==0;continue;end
		[ttY ttS hyp{m}]=metafit(methods{m}, trX,trY,ttX);

		subplot(3,1,m)
		hold on
		fill([ttIdx fliplr(ttIdx)],[ttY+ttS;flipud(ttY-ttS)], col{m}*0.1+0.9,'EdgeColor',col{m}*0.3+0.7)
		plot(tau(model.idx,:));
		plot(trIdx, trY, 'kx')
		errorbar(ttIdx, ttY, ttS,'r.');
		hold off
		legend('Y','trY','ttY');
		title(caps{m})

		%plot(ttX,ttY,'Color',col{m},'LineWidth',2,'LineStyle',lsty{mod(m,2)+1});
		%if strcmp(methods{m},'LR')
		%	lgd=sprintf('%-10s', caps{m});
		%else
		%	lgd=sprintf('%-10s \\sigma=%.3f', methods{m}, exp(hyp{m}.cov(1)));	
		%end
		%lgds=[lgds lgd];
	end

	%set(gca,'FontSize',15)
	%ylabel('accleration');
	%xlabel('position');
	%legend([ lgds],'Location','SouthEast')
	%for m=1:length(methods)
	%	if length(methods{m})==0;continue;end
	%	[ttY ttS hyp{m}]=metafit(methods{m}, trX,trY,ttX);
	%	keyboard
	%end
	%ylim([-1.1 1.1]);
	%addpath ~/matlablib/
	%savefig('methodscmp.pdf',gcf,'pdf','-r300');
	%savefig('lrgpcmp.pdf',gcf,'pdf');

function [ttY ttS hyp]=metafit(method, trX,trY,ttX)
	switch method
		case 'PRGP'
			[trYLR ttS hyp]=fit('PR', trX,trY,trX);
			[ttYLR ttS hyp]=fit('PR', trX,trY,ttX);
			[ttYdelta ttS hyp]=fit('GP', trX,trY-trYLR,ttX);
			ttY=ttYLR+ttYdelta;;
		otherwise
			[ttY ttS hyp]=fit(method, trX,trY,ttX);
	end

function c=getCoeff(x)
	global model
	x=reshape(x,[],model.njoints);
	model.theta = num2cell([0 x(:,1)']);
	model.dTheta = num2cell([0 x(:,2)']);
	C  = getSystemIdCoeff(model, x(:,3));
	c=C(model.idx,:);

function [ttY ttS hyp]=fit(method, trX,trY,ttX)
	%GP
	slf=1e-3; %smaller -> uninformative prior
	ell=std(trX,[],1)';
	sf=std(trY);
	covBasisRBD= @(varargin) covBasis(@(x) getCoeff(x) ,varargin{:});
	switch method
		case 'PR'
			cov={covBasisRBD}; 
			hyp.cov=log([slf]);
		case 'GP'
			%cov = {@covSEiso}; 
			cov = {@covSEard}; 
			hyp.cov=log([ell;sf]);
		case 'HGP'
			cov = {@covSum,{@covSEard, covBasisRBD}}; 
			hyp.cov=log([ell;sf;slf]);
	end
	sn=std(trY)/10;
	hyp.lik=log(sn);
	lik={@likGauss};

	hyp=minimize(hyp, 'gp',-200,[],[],cov,lik,trX,trY);
	[ttY ttS]=gp(hyp,[],[],cov,lik, trX, trY, ttX);
