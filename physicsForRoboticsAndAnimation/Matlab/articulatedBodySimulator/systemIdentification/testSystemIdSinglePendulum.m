function testSystemId()
		rand('seed',0);
		randn('seed',0);

		%%%%first obtain a model %%%%%%%%%%%%%%%%%%%%%%%%
		nLink=2; %change nLink in {1,2,3}, to try different dimensions
		switch(nLink)
		case 1
			q0=-pi/4;
			qd0=10;
			m = gauss_singlePendulum(q0,qd0);
		case 2
			q0=[-pi/4 0];
			qd0=[1 0];
			m = gauss_doublePendulum(q0,qd0);
		case 3
			q0=[-pi/4 1 1];
			qd0=[1 1 1];
			m = gauss_multiLink(q0,qd0);
		end

		%%%% generate Traces from the model%%%%%%%%%%%%%%%
		nTrace=2000;
		tau = smoothRandomVector(ones(1,m.njoints),nTrace);
		%tau = zeros(m.njoints, nTrace);

		[q qd qdd] = generateTraces(m, tau, 0);%display=0
		figure(2);
		clf
		plot([q;qd;qdd;tau]')

		%%%% Convert Traces to Design Matrix for Regression%%%
		nParamPerJoint=1+3+6+1;
		C=zeros(m.njoints*(nParamPerJoint),m.njoints,m.njoints*nTrace);

		%generate subset Indexes
		tSubset=randperm(nTrace);
		nSubset=floor(nTrace*0.1);
		tSubset=sort(tSubset(1:nSubset));

		for t=tSubset;
			m.theta = num2cell([0 q(:,t)']);
			m.dTheta = num2cell([0 qd(:,t)']);

			C(:,:,t) = getSystemIdCoeff(m, qdd(:,t))';
		end

		%crop the subset
		C = C(:,:,tSubset);
		tauS = tau(:,tSubset);

		%flatten C and S
		C = reshape(C, [m.njoints*nParamPerJoint, m.njoints*nSubset]);
		tauS = tauS(:);

		%verification reference answer
		lambdaReal = model2lambda(m);
		errReal = C'*lambdaReal - tauS; %error with real-parameter plugged-in
		maeReal = mean(abs(errReal));

		%the estimated parameter
		lambdaEst  = pinv(C*C')*C*(tauS);
		errEst= C'*lambdaEst- tauS;
		maeEst = mean(abs(errEst));

		%display some information
		cSample=C(:,1:3)' %display C
		cRank=rank(C*C')  %estimate rank

		disp('real  est');
		[lambdaReal lambdaEst]

		disp('err');
		[maeReal maeEst]

		%show error on tSubset
		figure(3)
		subplot(2,1,1);
		plot([errReal errEst])
		subplot(2,1,2);
		plot(qd')

		%compare {Real, Estimated} parameter -model prediction performance on a new trajectory
		tauTest = smoothRandomVector(ones(1,m.njoints),nTrace);
        %the starting posture is 
		mReal = lambda2model(m, lambdaReal);
		mEst  = lambda2model(m, lambdaEst);
		[qr qdr qddr] = generateTraces(mReal, tauTest, 0);%display=0
		[qe qde qdde] = generateTraces(mEst, tauTest, 0);%display=0
		trjErr= norm(qr-qe,1)+norm(qdr-qde,1)+norm(qddr-qdde,1)
        keyboard

