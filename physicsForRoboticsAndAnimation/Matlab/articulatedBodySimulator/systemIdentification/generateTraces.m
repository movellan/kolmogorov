function [q qd qdd]=generateTraces(m, tau, isDisplay)
%  m   : robot model
%  tau : #joint x #timesteps, external applied  torque
%  q   : #joint, initial joint angle
%  qd  : #joint, initial joint angular velocity
	[trNJoints trLength]=size(tau);
	assert(trNJoints==m.njoints);

	q=zeros(m.njoints,trLength);
	qd=zeros(m.njoints,trLength);
	qdd=zeros(m.njoints,trLength);

	for t=1:trLength
	% example forward dynamics
	% set torques and get accelerations
	  m.tau = num2cell([0 tau(:,t)']);
	  q(:,t) = m.bigTheta;
	  qd(:,t) = m.bigdTheta;
	  m = robotUpdate(m);   
	  qdd(:,t) = m.bigddTheta;

	  if exist('isDisplay')  && isDisplay==1
	   	  figure(1)
		  robotDisplay(m);    
		  drawnow
		  clf
	  end
	end
