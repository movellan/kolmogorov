clear
% 7df arm model
% definition is in createHumanArmModel.m
load humanArmModel

% initial angles  in radians
ha.theta{2} = pi/4;
ha.theta{3} = pi/4;
ha.theta{4} = pi/1;
ha.theta{5} = pi/4;
ha.theta{6} = pi/2;
ha.theta{7} = pi/2;
ha.theta{8} = pi/2;
% initial angular velocities in rads/sec
ha.dTheta{2} = 0;
ha.dTheta{3} = 0;
ha.dTheta{4} = 0;
ha.dTheta{5} = 0;
ha.dTheta{6} = 0;
ha.dTheta{7} = 0;
ha.dTheta{8} = 0;


ha.dt=1/100;
ha = robotInit(ha);
for t=1:100000000000
   t
   % simple PD gravity compensated controller
  for k=2:ha.njoints+1
    ha.tau{k} =  20*(0-ha.theta{k})+10*(0-ha.dTheta{k}) -ha.bigTauG(k-1);% 
  end
  

  ha = robotUpdate(ha);   

  robotDisplay(ha);
  drawnow
  clf
  
    
  
end



