% computes the Jacobian of the world coordinates of a point
% attached to a link
% m is the robot model
% l is an integer indicating the link where the point is attached
% p are the coordinates of the point in the link's frame of reference
function J = getJacobianOfPoint(m,l,p)

q= m.s{l}*p;
J = m.H{l} + R(q)'*m.w{l};

  
