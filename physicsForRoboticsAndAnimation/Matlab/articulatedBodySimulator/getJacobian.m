function m = getJacobian(m)
  

  m.bigTheta=[];    
  for i=2:m.njoints+1 
    m.bigTheta=[m.bigTheta;m.theta{i}];
    m.r{i} = expm(R(m.u{i}*m.theta{i})); % rotation of each joint
    m.s{i} = m.s{i-1}*m.r{i}; % cumulative rotation
    m.v{i} = m.s{i-1} * m.u{i};
    m.w{i} = m.w{i-1}; m.w{i}(:,m.cd(i-1)+1:m.cd(i))= m.v{i};
    m.y{i} = m.y{i-1} + m.s{i}*m.l{i};
    m.x{i} = m.y{i-1} + m.s{i}*m.b{i};
    m.H{i} = m.H{i-1} +(R(m.y{i} - m.y{i-1}))' * m.w{i};
    m.J{i} = m.H{i-1} +(R(m.x{i} - m.y{i-1}))' * m.w{i};
  end
  
