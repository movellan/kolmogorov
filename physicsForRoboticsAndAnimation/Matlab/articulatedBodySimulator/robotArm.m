% Simulate non-planar robot arm using Gauss-Newton Algorithm
% for notation see the "articulated bodies chapter" of "Physics for
% Robotics and Animation"
% Copyright @ Javier R. Movellan, 2012
% MIT Open Source License.

clear
clf
% Global reference axes
% In matlab x goes left to right. z is down to up. -y is depth towards
% screen +y is depth away from screen
% number of links

% Due to lack of zero-offset in matlab, we reference the first
% joint as joint 2, second joint as joint 3 ...
% We use SI Units:
% length: meter
% mass: Kg
% force: Newton
% torque: (Newton Meter)/ radian
% moment of inertia (NewtonMeter/radian)/(radians/sec^2)

%%%%%%%%%% Begin Model Definition %%%%%%%%%%%%%%%
% rotation axes for each joint. 
m.u{2} = [1 0  0]'; % 1 df joint 
m.u{3} = [0 1 0]'; % 1 df joint 
m.u{4} = [0 0 1]'; % 1 df joint 
m.u{5} = [1 0 0]'; % 1 df joint 
m.u{6} = [0 1 0]'; % 1 df joint 
m.u{7} = [0 1 0]'; % 1 df joint 
% link ends at zero rotation
m.l{2}= [0.0;0;0]; 
m.l{3}= [0.0;0;0]; 
m.l{4}= [0.3;0;0]; 
m.l{5}= [0.0;0;0];
m.l{6}= [0.3;0;0]; 
m.l{7}= [0.1;0;0]; 
% centers of mass at zero rotation

m.b{2}=m.l{2}/2; 
m.b{3}=m.l{3}/2; 
m.b{4}=m.l{4}/2; 
m.b{5}=m.l{5}/2; 
m.b{6}=m.l{6}/2; 
m.b{7}=m.l{7}/2; 

% mass m{i} and moments of inertia I{i} for each link
% all we need is m{i} and I{i}
% Here we model each link as an ellipsoid
% and compute the m{i} and I{i}
rho = 1000; % density in Kg/cubicMeters
% lengt in meters of main half axes of the three links
a{2}=[m.l{2}(1);0.1;0.1]/2; 
a{3}=[m.l{3}(1);0.1;0.1]/2; 
a{4}=[m.l{4}(1);0.1;0.1]/2; 
a{5}=[m.l{5}(1);0.1;0.1]/2; 
a{6}=[m.l{6}(1);0.1;0.1]/2; 
a{7}=[m.l{7}(1);0.1;0.1]/2; 

for k=2:1:7
  m.m{k} = rho*pi*4*a{k}(1)*a{k}(2)*a{k}(3)/3; % mass in Kg
  % moment of inertia with respect to center of mass
  % in (NewtonMeter/radian) /(radian/sec^2)
  m.I{k} = m.m{k}*[a{k}(2)^2+a{k}(3)^2, 0, 0; 0, a{k}(1)^2+a{k}(3)^2, 0; 0,0, a{k}(1)^2+a{k}(2)^2]/5;
  % Use parallel axis theorem to get moment of inertial with
  % respect to center of rotation
  cr = [m.l{k}(1)/2;0;0];
  m.I{k} = m.I{k} - m.m{k}*R(cr)*R(cr);
end
% viscous friction in (NewtonMeter/radian)/(radian/sec)
m.eta{2} = diag([0.1 ]);
m.eta{3} = diag([0.1 ]);
m.eta{4} = diag([0.1 ]);
m.eta{5} = diag([0.1 ]);
m.eta{6} = diag([0.1 ]);
m.eta{7} = diag([0.1 ]);




% gravitational vector
m.g=[0;0;-0.981]; % in Newtons

% initial angles
m.theta{2} = 0;
m.theta{3} = pi/4;
m.theta{4} = 0;
m.theta{5} = pi/4;
m.theta{6} = 0;
m.theta{7} = 0;


% initial angular velocities in rads/sec
m.dTheta{2} = 0;
m.dTheta{3} = 0;
m.dTheta{4} = 0;
m.dTheta{5} = 0;
m.dTheta{6} = 0;
m.dTheta{7} = 0;


m.dt = 1/25; % sampling period in seconds 
m.T = 10; % simulation time in secs

m=robotInit(m);



for t=1:100000
 t
%  example inverse dynamics
% set accelerations and infer torques
% for k=2:m.njoints+1
%    m.ddTheta{k} = 1*randn(m.d(k),1);
%  end
%  m = robotUpdate(m,'getTorques');    
  
% example forward dynamics
% set torques and get accelearations
  for k=2:m.njoints+1
  %  m.tau{k} = 0.000*randn(m.d(k),1);
  end
  m = robotUpdate(m,'getAccelerations');   

  robotDisplay(m);
  drawnow
  clf
  
    
  
end
