% minimum angular acceleration controller of robot Arm. There is a set
% of target points in 3D: begin point, end point and "via" points.
% We assume we start at the start point with zero velocity and we want
% to end at the end point with zero velocity at the required
% time. We want to reach the "via" points at required times. We
% dont care about the velocity at those points. We want to reach
% the end point at the target time with zero velocity. 

% The approach consists of the following steps
% (1) the first step is to convert the desired 3D points into desired
% joint angles using an inverse kinmentics engine.
% (2) the second step is to compute a minimum angular acceleration
% controller
% (3) the third step is to apply the controller 
clf
clear


path(path,'..')
load humanArmModel

dt=1/100;



% default angles for each joint
defaultAngles(1,1)=0;
defaultAngles(2,1) =-pi/2;
defaultAngles(3,1) =pi;
defaultAngles(4,1) = 0;
defaultAngles(5,1) = pi;
defaultAngles(6,1) = 0;
defaultAngles(7,1) = 0;

% sequence of target points we want tip of the arm to reach (in 3D
% euclidean space)
targetPoints{1}=[30;0;-20 ]/100;
targetPoints{2}=[40;0;-20]/100;
targetPoints{3}=[40;10;-20]/100;
targetPoints{4}=[40;10;-20]/100;


sigmaTorque=10; % torque noise
displayFlag=0;

ha.dt = dt; % time step in seconds for robot dynamics simulator
targetAngles= inverseKinematicsSolver(ha,defaultAngles,targetPoints,dt,displayFlag);





targetTimes=[0; 0.2; 0.99;1];


PCost= 1000000; % cost for not achieving the via and end points
                % play with this parameter and see how it affects
                % the solution. It is kind of fun.
ICost= PCost/100;

DCost= PCost/10000;  % cost for not having zero velocity at the
                       % end point
controller = minimumAngularAcceleration(targetAngles, targetTimes,dt,PCost,ICost,DCost);

  
ndfree= ha.ndfree;

% initialize the robot to the starting point 
for k=2:ndfree+1
   ha.theta{k} = targetAngles{1}(k-1);
   ha.dTheta{k}= 0;
end

ha = robotInit(ha);
% padd the robot angles with integral and derivative states
x(:,1) = [ha.bigTheta;zeros(2*ndfree,1)];
 
% run the simulation 
s= length(controller.xi)

proportionalIndexes=1:ndfree;
integralIndexes=ndfree+1:2*ndfree;
derivativeIndexes=2*ndfree+1:3*ndfree;

sqdt= sqrt(dt);
for t=1:s
  disp(s-t)
  y(:,t) = ha.y{end}; 
  ha = robotUpdate(ha);
  
  
  e= x(:,t) - controller.xi{t};
  % keep track of cost  
  pCost = e'*controller.p1{t}*e*controller.dt;
  qCost = ha.bigddTheta'*controller.q{t}*ha.bigddTheta* ...
	  controller.dt;
 
  if t==1
    totalCost(t) =0;
  else
    totalCost(t) = totalCost(t-1) + pCost+qCost;
  end



 

  
  % ask the controller for the desired acceleration given the
  % current state
  
  
  acc = controller.w1{t}+controller.w2{t}*x(:,t);

  % Given the desired accelerations  compute the torque
  % needed to produce it 
  torque = ha.bigM*acc -  ha.bigTauG -ha.bigTauV +ha.bigCF;
  
  for k=2:ha.njoints+1
    ha.tau{k} = torque(k-1)+sqdt*sigmaTorque*randn(1);
  end
  %  update the state including joint angles, angular integrals and
  %  angular derivatives  
  % note the integral starts accumulating only for the last
  % interval between the previous to last target and the last target
  x(proportionalIndexes,t+1)= ha.bigTheta;
   if t*ha.dt>targetTimes(end-1)
    x(integralIndexes,t+1) = x(integralIndexes,t)+ x(proportionalIndexes,t+1)*ha.dt;
   end
   x(derivativeIndexes,t+1) = (x(proportionalIndexes,t+1) - x(proportionalIndexes,t))/ha.dt; 
  robotDisplay(ha);
  drawnow
  clf
end
  


subplot(3,1,1)
plot(y(1,:),y(3,:))
hold on

for p=1: length(targetPoints)
  scatter(targetPoints{p}(1),targetPoints{p}(3),100)
end
title('Trajectory of Arm Tip')
xlabel('Horizontal Axis')
ylabel('Vertical Axis')

subplot(3,1,2)
plot(y(1,:),y(2,:))
hold on

for p=1: length(targetPoints)
  scatter(targetPoints{p}(1),targetPoints{p}(2),100)
end
title('Trajectory of Arm Tip')
xlabel('Horizontal Axis')
ylabel('Vertical Axis')


subplot(3,1,3)

title('Velocity in X,Y,Z axes')
     

 plot([1:s-1]*ha.dt,diff(y(1,:)),[1:s-1]*ha.dt,diff(y(2,:)),[1:s-1]*ha.dt,diff(y(3,:)))
 
 legend('X','Y','Z')
 xlabel('Time')
 ylabel('Velocity')



  


