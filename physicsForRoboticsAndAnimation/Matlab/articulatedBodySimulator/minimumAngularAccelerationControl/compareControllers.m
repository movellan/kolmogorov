function compareControllers(controller1,controller2)


s =length(controller1.xi);
failure=0;
if controller2.tau ~= controller1.tau
    display(sprintf('Different tau',k));
    failure=1;
end

if controller2.T ~= controller1.T
    display(sprintf('Different T',k));
    failure=1;
end

if controller2.dt ~= controller1.dt
    display(sprintf('Different dt',k));
    failure=1;
end


for k =1: s
  d = controller1.xi{k}-controller2.xi{k};
  if norm(d) >0
    display(sprintf('Different  xi{%d}',k));
    failure=1;
  end
end

for k =1: s
  d = controller1.q{k}-controller2.q{k};
  if norm(d) >0
    display(sprintf('Different  q{%d}',k));
    failure=1;
  end
end


for k =1: s
  d = controller1.p1{k}-controller2.p1{k};
  if norm(d) >0
    display(sprintf('Different  p1{%d}',k));
    failure=1;
  end
end


for k =1: s
  d = controller1.a{k}-controller2.a{k};
  if norm(d) >0
    display(sprintf('Different  p1{%d}',k));
    failure=1;
  end
end

for k =1: s
  d = controller1.omega{k}-controller2.omega{k};
  if norm(d) >0
    display(sprintf('Different  omega{%d}',k));
    failure=1;
  end
end

for k =1: s
  d = controller1.k{k}-controller2.k{k};
  if norm(d) >0
    display(sprintf('Different  k{%d}',k));
    failure=1;
  end
end

for k =1: s
  d = controller1.b{k}-controller2.b{k};
  if norm(d) >0
    display(sprintf('Different  b{%d}',k));
    failure=1;
  end
end

for k =1: s
  d = controller1.c{k}-controller2.c{k};
  if norm(d) >0
    display(sprintf('Different  c{%d}',k));
    failure=1;
  end
end

for k =1: s
  d = controller1.p2{k}-controller2.p2{k};
  if norm(d) >0
    display(sprintf('Different  p2{%d}',k));
    failure=1;
  end
end

for k =1: s
  d = controller1.w1{k}-controller2.w1{k};
  if norm(d) >0
    display(sprintf('Different  w1{%d}',k));
    failure=1;
  end
end

for k =1: s
  d = controller1.w2{k}-controller2.w2{k};
  if norm(d) >0
    display(sprintf('Different  w2{%d}',k));
    failure=1;
  end
end

for k =1: length(controller1.g)
  d = controller1.g{k}-controller2.g{k};
  if norm(d) >0
    display(sprintf('Different  g{%d}',k));
    failure=1;
  end
end

for k =1: length(controller1.h)
  d = controller1.h{k}-controller2.h{k};
  if norm(d) >0
    display(sprintf('Different  h{%d}',k));
    failure=1;
  end
end


d = controller1.alpha-controller2.alpha;
if norm(d) >0
  display(sprintf('Different  alpha',k));
  failure=1;
end

d = controller1.beta-controller2.beta;
if norm(d) >0
  display(sprintf('Different  beta',k));
  failure=1;
end

d = controller1.gamma-controller2.gamma;
if norm(d) >0
  display(sprintf('Different  gamma',k));
  failure=1;
end






if failure==0
  display('All Test Passed. Controllers are Identical');
else
  display('Some Tests Failed. Controllers are not Identical');
end
