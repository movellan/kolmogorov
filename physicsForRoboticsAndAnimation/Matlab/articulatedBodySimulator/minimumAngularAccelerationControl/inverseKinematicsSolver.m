% input is a robot arm and a sequence of 3D target points for the
% tip of the arm. 
% output is a sequence of joint angles that get as close as
% possible to the sequence of target points. 

function targetAngles= inverseKinematicsSolver(ha,defaultAngles,targetPoints,dt,displayFlag)

% Global reference axes
% In matlab x goes left to right. z is down to up. -y is depth towards
% screen +y is depth away from screen
% number of links

% Due to lack of zero-offset in matlab, we reference the first
% joint as joint 2, second joint as joint 3 ...
% We use SI Units:
% length: meter
% mass: Kg
% force: Newton
% torque: (Newton Meter)/ radian
% moment of inertia (NewtonMeter/radian)/(radians/sec^2)
% angles in radians
%%%%%%%%%% Begin Model Definition %%%%%%%%%%%%%%%


ha.dt = dt; % time step in seconds for robot dynamics simulator



  
% start at default angles  and zero velocity
for k=2:8
  ha.theta{k}=defaultAngles(k-1,1);
  ha.dTheta{k} = 0;
end



% we create a sequence of points by doing linear interpolation between
% the target points. this is to help the inverse kinematic controller
% to move from one point to the other using joint angles that do not
% change too much. once we have solved the inverse kinematics for
% the entire sequence we drop the interpolation and take only the
% inverse kinematic solution for the targetPointss{}

s=10; % number of points between the targetPointss 
xi=zeros(3,s);

targetTime(1)=1; % keeps track of where in the sequence are the true
                 % targetPointss as opposed to the interpolated points
t=0;


for k=2:length(targetPoints)
  for j=1:s
    t=t+1;
    xi(:,t) = targetPoints{k-1}+(targetPoints{k} - targetPoints{k-1})*(j-1)/(s-1);
  end
  targetTime(k)= t;
end



% inverse kinematics

for t=1:size(xi,2) % go through the entire interpolation sequence
  t
  % number of iterations for the inverse kinematics solver for each
  % time step. Note the first step is the hardest so we give more iterations.
  if t==1 
    nIterations=200;
  else 
    nIterations=10; 
  end
  
  for t2=1:nIterations
    ha= getJacobian(ha);
    epsilon = (xi(:,t) - ha.y{end});
    J = ha.H{end};
    delta= 1*ha.dt*(defaultAngles- ha.bigTheta);
      dTheta = delta+J'*pinv(J*J'+0.001*eye(3))*(epsilon -J*delta);
    for k=2:1:ha.ndfree+1
      gain=1;
      ha.theta{k} = ha.theta{k}+ gain*dTheta(k-1);
    end
  end
  
  targetTheta(:,t) = ha.bigTheta; 
  if displayFlag>0  
    robotDisplay(ha)
    drawnow
    clf    
  end

end

% get rid of the solutions for the interpolated points. the
% interpolation was only used to help the inversekinematics solver
% create a smooth solution.

targetTheta=targetTheta(:,targetTime);


for k=1:size(targetTheta,2)
  targetAngles{k} = targetTheta(:,k);
end
