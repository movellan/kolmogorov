% test the effect of the integral part of the controller.  Goal is
% to keep the arm at a desired location for 1 second. However the
% controller underestimates the weight of the arm. The integral
% part of the controller allows it to recover. 
clf
clear


path(path,'..')
load humanArmModel

dt=1/200;



% default angles for each joint
defaultAngles(1,1)=0;
defaultAngles(2,1) =-pi/2;
defaultAngles(3,1) =pi;
defaultAngles(4,1) = 0;
defaultAngles(5,1) = pi;
defaultAngles(6,1) = 0;
defaultAngles(7,1) = 0;

% sequence of target points we want tip of the arm to reach (in 3D
% euclidean space)
targetPoints{1}=[30;0;-20 ]/100;
targetPoints{2}=[30;0;-20]/100;



sigmaTorque=1; % torque noise
displayFlag=0;

ha.dt = dt; % time step in seconds for robot dynamics simulator
targetAngles= inverseKinematicsSolver(ha,defaultAngles,targetPoints,dt,displayFlag);





targetTimes=[0; 5];


PCost= 1000000; % cost for not achieving the via and end points
                % play with this parameter and see how it affects
                % the solution. It is kind of fun.
ICost= 5*PCost;

DCost= PCost/10000;  % cost for not having zero velocity at the
                       % end point
controller = minimumAngularAcceleration(targetAngles, targetTimes,dt,PCost,ICost,DCost);

  
ndfree= ha.ndfree;

% initialize the robot to the starting point 
for k=2:ndfree+1
   ha.theta{k} = targetAngles{1}(k-1);
   ha.dTheta{k}= 0;
end

ha = robotInit(ha);
% padd the robot angles with integral and derivative states
x(:,1) = [ha.bigTheta;zeros(2*ndfree,1)];
 
% run the simulation 
s= length(controller.xi)

proportionalIndexes=1:ndfree;
integralIndexes=ndfree+1:2*ndfree;
derivativeIndexes=2*ndfree+1:3*ndfree;

sqdt= sqrt(dt);
for t=1:s
  disp(s-t)
  y(:,t) = ha.y{end}; 
  ha = robotUpdate(ha);
  
  
  e= x(:,t) - controller.xi{t};
  % keep track of cost  
  pCost = e'*controller.p1{t}*e*controller.dt;
  qCost = ha.bigddTheta'*controller.q{t}*ha.bigddTheta* ...
	  controller.dt;
 
  if t==1
    totalCost(t) =0;
  else
    totalCost(t) = totalCost(t-1) + pCost+qCost;
  end



 

  
  % ask the controller for the desired acceleration given the
  % current state
  
  
  acc = controller.w1{t}+controller.w2{t}*x(:,t);

  % Given the desired accelerations  compute the torque
  % needed to produce it 
  torque = ha.bigM*acc -  ha.bigTauG -ha.bigTauV +ha.bigCF;
  
  for k=2:ha.njoints+1
    % note we multiply the desired torque times 0.1 this is the
    % same as underestimating the weight of the arm 
    ha.tau{k} = 0.1*torque(k-1)+sqdt*sigmaTorque*randn(1);
  end
  %  update the state including joint angles, angular integrals and
  %  angular derivatives  
  % note the integral starts accumulating only for the last
  % interval between the previous to last target and the last target
  x(proportionalIndexes,t+1)= ha.bigTheta;
   if t*ha.dt>targetTimes(end-1)
    x(integralIndexes,t+1) = x(integralIndexes,t)+ x(proportionalIndexes,t+1)*ha.dt;
   end
   x(derivativeIndexes,t+1) = (x(proportionalIndexes,t+1) - x(proportionalIndexes,t))/ha.dt; 
  robotDisplay(ha);
  drawnow
  clf
end
  



plot([1:s]*ha.dt,y(3,:))
hold on
plot([1,s]*ha.dt, [targetPoints{1}(3),targetPoints{end}(3)],'r--')
hold off


title('Trajectory of Arm Tip')
xlabel('Time')
ylabel('Vertical Axis')



  


