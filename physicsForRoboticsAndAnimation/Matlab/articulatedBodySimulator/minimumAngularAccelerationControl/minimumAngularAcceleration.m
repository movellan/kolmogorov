% function rc = minimumAngularAcceleration(tAngles,tTimes,dt,pCost,iCost,dCost)
% Create a minimumAngularAccelerationController to go from a start
% vector of joint angles to a target vector of joint angles via a
% sequence of joint angle vectors. The sequence of joint angle vectors
% has to be visited at spectific times.

% tAngles: target Angles.  tAngles{1}, ..., tAngles{n}, where
% tAngle{i} is a vector with the desired joint angles at time
% tTimes(i). typically tAngles{n-1} = tAngles{n}, ie., the last 2
% vectors of target Angles are identical. This is to provide a dwell
% time, i.e., after reaching tAngles{n-1} the target velocity is set
% to zero and the integral, proportional and derivative error affects
% the controller 
% tTimes. target Times. A vector with the desired times
% to visit each target joint angle vector. in seconds.  
% dt. time step for the simulator in seconds.  
% pCost. a scalar with the proportional cost 
% iCost. a scalar with the integral cost
% dCost. a scalar with the derivative (angular velocity) cost
% The controller
% minimizes the following cost rho = \sum_{k=1}^{n-1} |tAngles{k} -
% omega(t_k)|^2 * pCost + \int_{t_{n-1}}^{t_n} |omega(t) -
% tAngles{n}|^2 * pCost * dt + \int_{t_{n-1}}^{t_n} |\bar omega(t)-
% \bar tAngles{n} |^2 *iCost *dt + \int_{t_{n-1}}^{t_n} |\dot omega(t)
% |^2 * dCost *dt
% + \int_0^{t_n} | angularAcceleration(t)|^2
% 
% where tAngles(k) is the vector of target angles at time
% t_k. 0=t_1< t_2 < ... < t_n are the target times.
% omega(t) is the obtained vector of joint angles at time t
% \bar omega(t) = \int_{t_{n-1}}^t omega(s) ds
% \bar tAngles(t) = \int_{t_{n-1}}^ t  tAngles{n} ds = (t-
% t_{n-1}) tAngles{n} 
% \dot \omega(t) is the obtained angular velocity at time t.
% The system is assumed to start at joint angles tAngles(1) at time
% 0. Then the controller tries to go through target angles
% tAngles(2) ... tAngles(n-1) at times tTimes(2)
% ... tTimes(n-1). Then for the period from tTimes(n-1) to
% tTimes(n) it attempts to reach targetAngles(n), while achieving
% zero velocity and while  minimizing the difference between the integral of
% the obtained angles and the integral of the terminal desired
% angle. 


function rc = minimumAngularAcceleration(tAngles,tTimes,dt,pCost,iCost,dCost)


path(path,'../../../../Control/Matlab/ContinuousTimeLQT')


rc.tau =inf; 
rc.T = tTimes(end); % in seconds
rc.dt = dt;


ndf = length(tAngles{1}); 
nx=3*ndf; % for P, I and D
nu=ndf;

s=ceil(rc.T/rc.dt);
proportionalIndexes=1:ndf;
integralIndexes=ndf+1:2*ndf;
derivativeIndexes=2*ndf+1:3*ndf;
timeIndexes = floor(tTimes/rc.dt)+1;
if timeIndexes(end)>s
  timeIndexes(end)=s;
end

% set parameters for LQR
for t=1:s
  if t> timeIndexes(end-1) % trigger the dynamics for the integral time
    kt=1;
  else
    kt=0;
  end

  rc.xi{t}(1:nx,1)=0; % target 
  rc.q{t}=eye(ndf); % action cost matrix
  rc.p1{t} = zeros(nx); % state cost matrix
  
  for t2=1:length(tTimes)
    if(timeIndexes(t2) == t)
      rc.xi{t}(proportionalIndexes,1)=tAngles{t2}; % joint angles
      for k2=proportionalIndexes 
	rc.p1{t}(k2,k2) = pCost; % weight for thd proportional part
      end
    end
  end
  
  if kt>  0 % triggered for the period from previous to last to
            % last targets in the sequenc of angles. 
    rc.xi{t}(proportionalIndexes) = tAngles{end};
    for k2=proportionalIndexes 
      rc.p1{t}(k2,k2)=pCost; % weight for the proportional cost
    end    
    
    for k2=integralIndexes 
      rc.p1{t}(k2,k2)=iCost; % weight for the integral cost
    end    
    
    for k2=derivativeIndexes
      rc.p1{t}(k2,k2)=dCost; % weight for the derivative cost
    end    
  end

  % note the integral part accumulates only when kt is not zero  
  if t>1
    rc.xi{t}(integralIndexes) = rc.xi{t-1}(integralIndexes)+ kt*rc.xi{t}(proportionalIndexes)*rc.dt;
  end

  % note the integral part gets triggered only when kt is not zero  
  rc.a{t} = [zeros(ndf), zeros(ndf),eye(ndf); kt*eye(ndf),zeros(ndf),zeros(ndf);zeros(ndf),zeros(ndf),zeros(ndf)];
  
% a bunch of general purpose parameters for the lqr controller  
  rc.omega{t} =zeros(nu,1);
  rc.k{t} = zeros(nx,1); 
  rc.b{t} = [zeros(ndf);zeros(ndf); eye(ndf)];
  rc.c{t} = 0*eye(nx);
  rc.p2{t} = zeros(nx,1); 
  rc.w1{t} = zeros(nu,1);
  rc.w2{t}= zeros(nu,nx);
end

% here we compute the controller
rc = lqt3(rc);






 




