% input is a robot model 
% if one output only, then it is the vector of joint accelerations
% if two outputs, the first is the vector of accelerations, and the
% second is the updated model.

function [ddThetaVec,m] = getAccelerations(m)

m.bigM=zeros(m.ndfree,m.ndfree); % moment of inertia matrix  
m.bigCF=zeros(m.ndfree,1); % coriolis torque vector
m.bigTauG=zeros(m.ndfree,1); % gravitational torque vector
m.bigTauV=[]; %viscous torques 
%m.bigTauS=[];

%Precomputed mb if not specified, 
% in some cases, we can only identify 'm*b' as a 
%lumped parameter, so we have to be able to simulate with mb only.
if ~isfield(m,'mb')
	%fprintf('fill-in mb\n');
	for i=2:m.njoints+1 
		m.mb{i} = m.m{i}*m.b{i};
	end
	clean_mb=1;
else
	clean_mb=0;
	%fprintf('model has mb set already\n');
end

for i=2:m.njoints+1 
  %%%%%%%%% compute global inertial matrix %%%%%%%%%%%
  m.r{i} = expm(R(m.u{i}*m.theta{i})); % rotation of each joint
  m.s{i} = m.s{i-1}*m.r{i}; % cumulative rotation
  m.v{i} = m.s{i-1} * m.u{i};
  m.w{i} = m.w{i-1}; 
%  m.w{i}(:,m.cd(i-1)+1:m.cd(i))= m.v{i};
  m.w{i}(:,i)= m.v{i};
  m.y{i} = m.y{i-1} + m.s{i}*m.l{i};
  m.x{i} = m.y{i-1} + m.s{i}*m.b{i};
  m.H{i} = m.H{i-1} -R(m.y{i} - m.y{i-1}) * m.w{i};
  m.J{i} = m.H{i-1} - R(m.x{i} - m.y{i-1}) * m.w{i};


  m.M{i} =          -m.H{i-1}'*m.s{i}*R(m.mb{i})*m.s{i}'*m.w{i};
  m.M{i} = m.M{i} + m.M{i}';
  m.M{i} = m.M{i} + m.m{i}*m.H{i-1}'*m.H{i-1};
  m.M{i} = m.M{i} + m.w{i}'*m.s{i}*m.I{i}*m.s{i}'*m.w{i};


  m.bigM=m.bigM+m.M{i};
  
  %%%%%%%%  compute Coriolis force vector %%%%%%%%%%
  m.wdTheta{i} = m.wdTheta{i-1} + m.v{i} *m.dTheta{i};
  m.dy{i} = R(m.y{i}-m.y{i-1})'*m.wdTheta{i};
  m.q{i} = m.q{i-1} + R(m.v{i-1}*m.dTheta{i-1});
  m.dv{i} = m.q{i}*m.v{i};
  m.dwdTheta{i} = m.dwdTheta{i-1} + m.dv{i} *m.dTheta{i};

  m.dHdTheta{i} =m.dHdTheta{i-1} +R(m.dy{i})'*m.wdTheta{i};
  m.dHdTheta{i} =m.dHdTheta{i} +R(m.y{i}-m.y{i-1})'*m.dwdTheta{i};
 
  m.cF{i} = m.m{i}*m.H{i-1}'*m.dHdTheta{i-1};

  %m.cF{i} = m.cF{i}+ m.m{i}*m.H{i-1}'*R(m.wdTheta{i})*R(m.wdTheta{i})*(m.x{i} - m.y{i-1}); 
  m.cF{i} = m.cF{i}+ m.H{i-1}'*R(m.wdTheta{i})*R(m.wdTheta{i})*m.s{i}*m.mb{i};

  %m.cF{i} = m.cF{i}+ m.m{i}*m.H{i-1}'*R(m.x{i}- m.y{i-1})'*m.dwdTheta{i}; 
  m.cF{i} = m.cF{i}+ m.H{i-1}'*m.s{i}*R(m.mb{i})'*m.s{i}'*m.dwdTheta{i}; 

  %m.cF{i} = m.cF{i}+ m.m{i}*m.w{i}'*R(m.x{i}- m.y{i-1})* m.dHdTheta{i-1};
  m.cF{i} = m.cF{i}+ m.w{i}'*m.s{i}*R(m.mb{i})* m.s{i}'*m.dHdTheta{i-1};

  m.cF{i} = m.cF{i}+ m.w{i}'*R(m.wdTheta{i})*m.s{i}*m.I{i}*m.s{i}'* m.wdTheta{i}; 
  m.cF{i} = m.cF{i}+ m.w{i}'*m.s{i}*m.I{i}*m.s{i}'*m.dwdTheta{i};
  m.bigCF=m.bigCF+ m.cF{i};
  % compute viscous torques
  m.tauV{i}= -m.eta{i}*m.dTheta{i};
  m.bigTauV= [m.bigTauV; m.tauV{i}];
  % compute gravitational torques
  %m.tauG{i} = m.m{i}*(m.H{i-1}' + m.w{i}'*R(m.x{i} - m.y{i-1}))*m.g;
  m.tauG{i} = (m.m{i}*m.H{i-1}' + m.w{i}'*m.s{i}*R(m.mb{i})*m.s{i}')*m.g;
  m.bigTauG= m.bigTauG+ m.tauG{i};
end

if clean_mb
	m=rmfield(m,'mb');
end


tauVec=[];
for k=2:m.njoints+1
  tauVec=[tauVec;m.tau{k}];
end
m.bigM = m.bigM+ m.bigArmature;
try 
  % Note diagonal viscosity term to moment of inertia. See
  % "Numerical issues" section of Physics for
  % Robotics and Animation Tutorial for explanation. 
  % got rid of this trick because it messes up the system
  % identification  
  
  % M = m.bigM+m.bigEta*m.dt/2;
  
  %ddThetaVec= pinv(M)*(tauVec+ m.bigTauG + m.bigTauV - m.bigCF); 

  % the following approach is more numerically stable. 
  % we want accelerations that minimize the norm of the difference  between
  % M*acceleration and the overall torque 
  ddThetaVec= inv(M'*M)*M'*(tauVec+ m.bigTauG + m.bigTauV - m.bigCF); 
catch me
  m.error=1;
  return
end
