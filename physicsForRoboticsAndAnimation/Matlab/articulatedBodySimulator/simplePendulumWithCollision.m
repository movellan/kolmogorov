% Simulate simple pendulum 
% end of the pendulum colides with a vertical viscous object
% we measure how much the object penetrates
% this gives us an indication of the force that the point put on
% the object at the poit and time of contact

% for notation see the "articulated bodies chapter" of "Physics for
% Robotics and Animation"
% Copyright @ Javier R. Movellan, 2012
% MIT Open Source License.

clear
clf



% Global reference axes
% In matlab x goes left to right. z is down to up. -y is depth towards
% screen +y is depth away from screen
% number of links

% Due to lack of zero-offset in matlab, we reference the world
% frame of reference as f1, frame of reference of first link as f2,
% second link, f3 ... This means the root joint is refered to as
% joint 2, second joint as joint 3 ...
% joint as joint 2, second joint as joint 3 ...
% We use SI Units:
% length: meter
% mass: Kg
% force: Newton
% torque: (Newton Meter)/ radian
% moment of inertia (NewtonMeter/radian)/(radians/sec^2)

%%%%%%%%%% Begin Model Definition %%%%%%%%%%%%%%%

% f1 (world)   coordinates of axis of rotation of parent joint for
% the first link 
m.u{2} = [ 0 1 0]'; % first joint

% f2 (first joint) coordinates of child joint of the first link
m.l{2}= [0.3;0;0]; 

% f2 (first joint) coordinates of the center of mass of the first
% link 

m.b{2}=m.l{2}/2; 

% mass m{i} and moments of inertia I{i} for each link

% Here we model each link as an ellipsoid
% and compute the m{i} and I{i}
rho = 1000; % density in Kg/cubicMeters
% length in meters of main half axes of the three links
a{2}=[m.l{2}(1);0.1;0.1]/2; %arm
for k=2:1:2
  m.m{k} = rho*pi*4*a{k}(1)*a{k}(2)*a{k}(3)/3; % mass in Kg
  % moment of inertia with respect to center of mass
  % in (NewtonMeter/radian) /(radian/sec^2)
  m.I{k} = m.m{k}*[a{k}(2)^2+a{k}(3)^2, 0, 0; 0, a{k}(1)^2+a{k}(3)^2, 0; 0,0, a{k}(1)^2+a{k}(2)^2]/5;
  % Use parallel axis theorem to get moment of inertial with
  % respect to center of rotation
  cr = [m.l{k}(1)/2;0;0];
  m.I{k} = m.I{k} - m.m{k}*R(cr)*R(cr);
end
% viscous friction of each joint in (NewtonMeter/radian)/(radian/sec)
m.eta{2} = diag(0.1);

% moment of inertia of armature of each joint  (NewtonMeter/radian)/(radian/sec^2)
m.armature{2} = diag([0.1 ]);

% gravitational vector
m.g=[0;0;-9.80665]; % in Newtons

% initial angles
m.theta{2} = -pi/4;

% initial angular velocities in rads/sec
m.dTheta{2} = 0;

m.dt = 1/40; % sampling period in seconds 
m.T = 10; % simulation time in secs

m=robotInit(m);
maxPenetration=0;
for t=1:100000
 display(sprintf('time = %3.f secs. penetration = %f centimeters', ...
		 t*m.dt, maxPenetration*100))
		 

% set torques and get accelerations

contactLink=2;
contactPoint=m.l{contactLink}; % we make contact at the end point
                               % of the link. 

contactForce=[0;0;0];


% if the x coordinate of the end point is positive, the pendulum
% moves free. at 0 there is a viscous object with which the end of
% the pendulum collides. we measure how much the pendulum
% penetrates the object and this gives us an indication of the
% collision force 
contactPointWorldCoords =getWorldLocationOfPoint(m,contactLink,contactPoint);
if contactPointWorldCoords(1) <=0 % detect collision with viscous object

  penetration = abs(contactPointWorldCoords(1));
  if penetration > maxPenetration
    maxPenetration= penetration;
  end
  
  v=getWorldVelocityOfPoint(m,contactLink,contactPoint);
  directionViscousContact= [1;0;0];
  vx= directionViscousContact'*v;
  viscosity=10;
  contactForce=-directionViscousContact*vx*viscosity;

end


m=setContactTorques(m,contactLink,contactPoint,contactForce);
m = robotUpdate(m);   

robotDisplay(m);    
displayContactForces(m,contactLink,contactPoint,contactForce);
drawnow
clf
  
end
