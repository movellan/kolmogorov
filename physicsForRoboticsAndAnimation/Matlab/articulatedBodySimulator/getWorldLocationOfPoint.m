% computes  the world coordinates of a point
% attached to a link
% m is the robot model
% l is an integer indicating the link where the point is attached
% p are the coordinates of the point in the link's frame of reference
function x = getWorldLocationOfPoint(m,l,p)

x=m.y{l-1}+ m.s{l}*p;



  
