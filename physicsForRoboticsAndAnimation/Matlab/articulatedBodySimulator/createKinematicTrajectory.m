% Simulate non-planar robot arm using Gauss-Newton Algorithm
% for notation see the "articulated bodies chapter" of "Physics for
% Robotics and Animation"
% Copyright @ Javier R. Movellan, 2012
% MIT Open Source License.

clear
clf
% Global reference axes
% In matlab x goes left to right. z is down to up. -y is depth towards
% screen +y is depth away from screen
% number of links

% Due to lack of zero-offset in matlab, we reference the first
% joint as joint 2, second joint as joint 3 ...
% We use SI Units:
% length: meter
% mass: Kg
% force: Newton
% torque: (Newton Meter)/ radian
% moment of inertia (NewtonMeter/radian)/(radians/sec^2)

%%%%%%%%%% Begin Model Definition %%%%%%%%%%%%%%%
load humanArmModel


% initial angles
ha.theta{2} = 0;
ha.theta{3} = 0;
ha.theta{4} = pi;
ha.theta{5} = 0;
ha.theta{6} = 0;
ha.theta{7} = 0;

% initial angular velocities in rads/sec
ha.dTheta{2} = 0;
ha.dTheta{3} = 0;
ha.dTheta{4} = 0;
ha.dTheta{5} = 0;
ha.dTheta{6} = 0;
ha.dTheta{7} = 0;


ha.dt = 1/100; % sampling period in seconds 
ha.T = 10; % simulation time in secs

s= ceil(ha.T/ha.dt);


xi=[0.2;0.2;0];  % desired end point
for t=1:s
t
  f=1;
  xi(1) = +0.2+0.05*(sin(2*pi*f*ha.dt*t));
  xi(2) = -0.2+0.05*(cos(2*pi*f*ha.dt*t));
  xi(3) = -0.2;
  xit(:,t) = xi;
  thetaGoal(:,t) = ha.bigTheta; 
  
  for t2=1:10
    ha= getJacobian(ha);

    epsilon = (xi - ha.y{end});
    g(t) = epsilon'*epsilon;
    J = ha.H{end};
      
    deta= -1*ha.dt*ha.bigTheta;
    deta(1)= deta(1)*10;
    dTheta = deta+J'*pinv(J*J'+0.001*eye(3))*(epsilon -J*deta);
    for k=2:1:ha.ndfree+1
      ha.theta{k} = ha.theta{k}+ 0.1*dTheta(k-1);
    end
  end
  
  
  robotDisplay(ha)
  plot3(xi(1), xi(2), xi(3), 'bo','MarkerSize',30,'LineWidth',3);
  plot3(xi(1), xi(2), xi(3), 'bo','MarkerSize',30,'LineWidth',3);
  npoints =2;
  for t2=t:-npoints:11
  line([xit(1,t2),xit(1,t2-npoints)],[xit(2,t2), xit(2,t2-npoints)],[xit(3,t2), ...
		    xit(3,t2-npoints)])
  
  end
  drawnow
    clf    

    
  
end
figure
plot(g)
thetaStart = thetaGoal(ceil(2/ha.dt));
thetaEnd = thetaGoal(end);
for t=1:s
thetaGoal(:,t) = thetaStart

save('exp1Theta','thetaGoal')
save('exp1ArmModel','ha')