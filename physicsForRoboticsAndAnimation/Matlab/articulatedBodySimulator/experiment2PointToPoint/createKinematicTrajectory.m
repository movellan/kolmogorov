% Loads begin point snd end points for the Flash&Hogan
% experiment. Task is to move from point a to point b as quickly as
% posible. 

% creates a minimum jerk trajectory between begin and end
% points. Then perofrms inverse kinematics to create the desired
% joint angles that produce the minimum jerk trajectory
% these desired joint angles are saved on "minimumJerk.mat"

%  loads humanArmModel and adapts it to perform the experiments in
%  this directory. it saves the model into exp2ArmModel.mat

clear
clf
% Global reference axes
% In matlab x goes left to right. z is down to up. -y is depth towards
% screen +y is depth away from screen
% number of links

% Due to lack of zero-offset in matlab, we reference the first
% joint as joint 2, second joint as joint 3 ...
% We use SI Units:
% length: meter
% mass: Kg
% force: Newton
% torque: (Newton Meter)/ radian
% moment of inertia (NewtonMeter/radian)/(radians/sec^2)

%%%%%%%%%% Begin Model Definition %%%%%%%%%%%%%%%
path(path,'..')

load humanArmModel

ha.dt = 1/1000;


% initial angles
ha.theta{2} = -0.1969;
ha.theta{3} = -0.2014;
ha.theta{4} = 3.0953;
ha.theta{5} = 0.3585;
ha.theta{6} = 2.3259;
ha.theta{7} = 0.5549;
ha.theta{8} = 0.0969;

% initial angular velocities in rads/sec
ha.dTheta{2} = 0;
ha.dTheta{3} = 0;
ha.dTheta{4} = 0;
ha.dTheta{5} = 0;
ha.dTheta{6} = 0;
ha.dTheta{7} = 0;
ha.dTheta{8} = 0;





load FlashHogan % data from FlashHogan experiment


xiStart(1,1) = +0.2+1*FlashHogan.data(2,1)/100;
xiStart(2,1) = -0.2-1*FlashHogan.data(2,2)/100;
xiStart(3,1) = -0.2;

xiEnd(1,1) = +0.2  + 1*FlashHogan.data(6,1)/100;
xiEnd(2,1) = -0.2 -1*FlashHogan.data(6,2)/100;
xiEnd(3,1) = -0.2;



T=1*0.55;
s=ceil(T/ha.dt);
xi=zeros(3,s);
for i=1:3
  a= minimumJerk(xiStart(i), 0,0,xiEnd(i),0,0,T);
  for j=1:s
    t= j*ha.dt;
    for k=1:6
      xi(i,j) = xi(i,j)+a(k)*t^(k-1);
    end
  end
end

xi=[repmat(xi(:,1),1,ceil(0.15/ha.dt)) xi];
xi=[xi, repmat(xi(:,end),1,ceil(0.3/ha.dt))];
  % default angles for each point
  c(1,1)=0;
  c(2,1) =-pi/2;
  c(3,1) =pi;
  c(4,1) = 0;
  c(5,1) = 0;
  c(6,1) = 0;
  c(7,1) = 0;

% do inverse kinematics on the  starting point  
for t2=1:200
  t2
  ha= getJacobian(ha);
  epsilon = (xi(:,1) - ha.y{end});
  g(t2) = epsilon'*epsilon;
  J = ha.H{end};

  deta= 1*ha.dt*(c- ha.bigTheta);
  deta(3) = deta(3)*10;
  dTheta = deta+J'*pinv(J*J'+0.001*eye(3))*(epsilon -J*deta);
  for k=2:1:ha.ndfree+1
    if t2<100
      gain=1;
    else
      gain=1;
    end
    ha.theta{k} = ha.theta{k}+ gain*dTheta(k-1);
  end
 robotDisplay(ha)
 drawnow
  clf
end

% do inverse kinematics for the minimum jerk trajectory
for t=1:ceil(1/ha.dt)
  t
  thetaGoal(:,t) = ha.bigTheta; 
  for t2=1:50 % number of iterations for the inverse kinematics
              % solver at each time step
    ha= getJacobian(ha);

    epsilon = (xi(:,t) - ha.y{end});
    g(t) = epsilon'*epsilon;
    J = ha.H{end};
    deta= 1*ha.dt*(c- ha.bigTheta);
    deta(3) = deta(3)*10;
    dTheta = deta+J'*pinv(J*J'+0.001*eye(3))*(epsilon -J*deta);
    for k=2:1:ha.ndfree+1
      ha.theta{k} = ha.theta{k}+ 1*dTheta(k-1);
    end
  end
  
  
  robotDisplay(ha)
  drawnow
  clf    

  
  
end

save('minimumJerk','thetaGoal');
save('exp2ArmModel','ha')