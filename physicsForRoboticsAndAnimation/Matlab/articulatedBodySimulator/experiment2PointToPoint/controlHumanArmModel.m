clear

% moves an arm from point a to point b. the points are taken from
% the Flash and Hogan experiment
% it compares three types of controllers
% (1) minimum Angular Acceleration. A closed loop controller that
% determines the desired angular acceleration for each joint. Given
% the acceleartion we compute the torque needed to achieve it. 
% (2) computed Torque: uses PID controller to determine the
% acceleration for each joint. It then computes the toruqe needed
% to achieve the desired acceleration.
% (3) computed Torque Minimum Jerk. First computes the minimum Jerk
% trajectory from point a to point B. Then it uses a PID controller
% to determine the acceleration at each point to produce the
% trajectory. Finally computes the torque needed to achieve the
% desired acceleration. 



load exp2ArmModel % the arm model we use for this experiment
load exp2MinAngularAccelerationController % the angular
                                          % acceleration controller
%load exp2Theta
load minimumJerk  % the minimum jerk trajectory 


path(path,'..')

for type=1:1
  if type==1
    controlType='minimumAngularAccelerationComputedTorque';
  elseif type==2
      controlType ='PIDComputedTorque'
  else
    controlType='minimumJerkPIDComputedTorque'
  end
  
k=0;

gainP= 50:5:250;
gainD =0:0.01:0.4;
gainI=[0 0.01]
gainP=85;
gainD=0.18;
gainI=0;

%gainP=250;
%gainD=0.4;
%gainI=0;




 % get the start and end points in cartesian coordinates
ndfree= ha.ndfree;
   for k=2:ndfree+1
    ha.theta{k} = thetaGoal(k-1,1);
   end
   
  ha = getJacobian(ha)
  yStart = ha.y{end};
  
  for k=2:ndfree+1
    ha.theta{k} = thetaGoal(k-1,end);
  end
  ha = getJacobian(ha)
  yEnd = ha.y{end};

  

ntrials=2;
for trial0=1:ntrials
  for trial1=1:length(gainP)
    for trial2=1:length(gainD)
      for trial3=1:length(gainI)
	pCost=0;
	qCost=0;
	thetaGoalInt=zeros(ndfree,1);
	thetaGoalD=zeros(ndfree,1);
	totalEnergyCost(trial0,trial1,trial2,trial3)=0;
	for k=2:ndfree+1
	  ha.theta{k} = thetaGoal(k-1,1)+0*0.005*randn(size(ha.theta{k}));
	  ha.dTheta{k}= 0;
	  tauNoise{k}=0;
	  tauNoise2{k}=0;
	end

	ha = robotInit(ha);

	x(:,1) = [ha.bigTheta;zeros(2*ndfree,1)];
	
	s= length(rc.xi);

	for t=1:s
	  disp(sprintf('[%s %d %d %d %d %d ]', controlType,s-t,trial0,trial1,trial2,trial3))
	  y{trial0}(:,t) = ha.y{end}; 
	  u = yEnd - yStart;
	  u = u/norm(u);
	  

	  yhat(trial0,trial1,trial2,trial3,t) = y{trial0}(:,t)'*u;
	  ha = robotUpdate(ha);   
	  
	  if strcmp(controlType, 'minimumAngularAccelerationComputedTorque')
	    acc = rc.w1{t}+rc.w2{t}*x(:,t);
	    	      
	  end
	  
	  if strcmp(controlType, 'PIDComputedTorque')	
	    if t*ha.dt<0.15
	      goal = 1;
	    else
	      goal =s;
	    end
	    
	    for k=2:ha.njoints+1
	     
	      acc(k-1,1) =  gainP(trial1)*(thetaGoal(k-1,goal)-ha.theta{k});
	      acc(k-1,1) = acc(k-1,1) + gainP(trial1)*gainD(trial2)*(0-ha.dTheta{k}) ;
	      acc(k-1,1) = acc(k-1,1) + gainP(trial1)*gainI(trial3)* ...
		  (thetaGoalInt(k-1)-ha.intTheta{k}) ;

	      
	      thetaGoalInt(k-1,1)= thetaGoalInt(k-1,1)+thetaGoal(k- ...
						  1,goal)*ha.dt;   
	      if t>1
	      thetaGoalD(k-1,1)= (thetaGoal(k-1,t)-thetaGoal(k-1,t-1))/ha.dt;   
	      end
	    end

	  end
	  
	  if strcmp(controlType, 'minimumJerkPIDComputedTorque')	  
	    for k=2:ha.njoints+1
	      
	      acc(k-1,1) =  gainP(trial1)*(thetaGoal(k-1,t)-ha.theta{k});
	      acc(k-1,1) = acc(k-1,1) + gainP(trial1)*gainD(trial2)*(thetaGoalD(k-1)-ha.dTheta{k}) ;
	      acc(k-1,1) = acc(k-1,1) + gainP(trial1)*gainI(trial3)* ...
		  (thetaGoalInt(k-1)-ha.intTheta{k}) ;
	      
	      
	      thetaGoalInt(k-1,1)= thetaGoalInt(k-1,1)+thetaGoal(k- ...
						  1,t)*ha.dt;   
	      if t>1
		thetaGoalD(k-1,1)= (thetaGoal(k-1,t)-thetaGoal(k-1,t-1))/ha.dt;   
	      end
	    end
	    
	    
	  end
	  
	  % Given the desired accelerations we compte the torque
          % needed to produce it 
	  
	  tau = ha.bigM*acc -  ha.bigTauG -ha.bigTauV +ha.bigCF;
	  
	  % diagonal of rest Moment of Inertia squared and scaled
          % so max is 1
	  %ng(2) = 0.5068;
	  %ng(3)= 0.7196;
	  %ng(4)=1;
	  %ng(5)=0.0068;
	  %ng(6) =0.1248;
	  %ng(7)=0.0064;
	  %ng(8)=0.0064;
	  ng(2)=1;
	  ng(3)=1;
	  ng(4)=1;
	  ng(5)=1;
	  ng(6)=1;
	  ng(7)=1;
	  ng(8)=1;
	  
	  for k=2:ha.njoints+1
	    ha.tau{k} = tau(k-1);
	    tauNoise{k} = tauNoise{k} -10*tauNoise{k}*ha.dt+ng(k)*sqrt(2* ...
						  5)*sqrt(ha.dt)* ...
		randn(1);
	    
	    tauNoise2{k} = tauNoise2{k} +4* (tauNoise{k}-tauNoise2{k})*ha.dt;
	    
	    ha.tau{k} = ha.tau{k} + 100*randn(1)*sqrt(ha.dt);

	    
	    Tau{trial0}(k-1,t)=ha.tau{k};
	    
	    
	    % torque is proportional to current through motor
	    % power proportional to square of current.
	    totalEnergyCost(trial0,trial1,trial2,trial3) = ...
		totalEnergyCost(trial0,trial1,trial2,trial3) + ((ha.tau{k})^2)*ha.dt;
	  end

	e= x(:,t) - rc.xi{t};
	if t>0.99*s; 
	pCost = pCost+ e'*rc.p1{t}*e*rc.dt;
	end
	
	qCost = qCost+ha.bigddTheta'*rc.q{t}*ha.bigddTheta*rc.dt;
	x(1:ndfree,t+1)= ha.bigTheta;
	x(ndfree+1:2*ndfree,t+1) = x(ndfree+1:2*ndfree,t)+ x(1:ndfree,t+1)*ha.dt;
	x(2*ndfree+1:3*ndfree,t+1) = (x(1:ndfree,t+1) - x(1:ndfree,t))/ha.dt;
	
	robotDisplay(ha);
	drawnow
	%  pause(0.1)
	clf
      end

      clf
      totalPcost(trial0,trial1,trial2,trial3) = pCost;
      totalQcost(trial0,trial1,trial2,trial3) = qCost;

      
    end
  end
end
trial0


end
clf
subplot 211
for trial0=1:ntrials
  plot(y{trial0}(1,:),-y{trial0}(2,:))
  hold on
end

 
  scatter(yStart(1),-yStart(2),100)
  scatter(yEnd(1),-yEnd(2),100)
  
  ylim([0.12-0.15 0.12+0.15])
  
  subplot 212
  
  %1 project y on the line defined by the start and end points
  
   
  for trial0=1:ntrials
    plot([1:s-1]*ha.dt,diff(yhat(trial0,:)))
    hold on 
  end
  
results.control=controlType;  
results.yhat =yhat;
results.y=y;
results.Tau= Tau;
results.totalPcost=totalPcost;
results.totalEnergyCost = totalEnergyCost;
%file = strcat('SquareWave',controlType);
file = controlType;
save(file,'results')
end
