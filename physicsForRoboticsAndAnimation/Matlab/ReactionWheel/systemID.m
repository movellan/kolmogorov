% Systems identification of the reaction wheel data
% 
clear

load data
V = data{3}; % motor voltage

dt = data{1}; % time step
s = length(V); 


theta = data{2}; % pendulum and wheel angle through time
vel = (diff(theta')/dt)';
y = vel; % we try to predict the velocity: v_t = (theta_t - theta_{t-1})/dt

theta(:,1)=[]; % we dont have velocity for t=1 so get rid of first time step
V(1)=[];


% x(1,t) arm angular velocity at time t in rads/sec
% x(2,t) wheel angular velocity at time t in rads/sec
% x(3,t) sin(arm angle) at time t
% x(4,t) voltage at time t


x=[vel; sin(theta(1,:));V']; % we predict the next time step velocity
                             % based on this state vecotr



%y(1,t)  arm angular velocity at time t+1
%y(2,t)  wheel angular velocity at time t+1

y(:,1) = []; % we try to predict the next time step
             % so y(:,t) should be one time step ahead of 
             % x(:,t)

x(:,end)=[]; % we don't predict the last time step


sprintf('Theoretical weight matrix')

w = [ 1 0 0 0; 0 1 0 0] + dt*Mi*[ - nu1  0 c3 0; 0 -nu2-(Kb*Kt)/R 0 Kt/R] ...
    
    sprintf('Obtained weight matrix')
what = y*x'*inv(x*x')
what = w;

thetaHat= zeros(2,s);
thetaDotHat= zeros(2,s);
thetaHat(:,1) = theta(:,2);
thetaDotHat(:,1) = x(1:2,1);
% predict into the future in a recursive manner using only V  and the 
% initial conditions as the input

for t=1:s-1
  u = [ thetaDotHat(:,t); sin(thetaHat(1,t)); V(t)];
  thetaDotHat(:,t+1)= what*u;
  thetaHat(:,t+1) = thetaHat(:,t)+ dt*(thetaDotHat(:,t+1));
end

subplot(2,1,1)
s= min(length(thetaHat), length(theta));
plot([1:s]*dt,theta(1,1:s))
hold on
plot([1:s]*dt,thetaHat(1,1:s),'r--');
xlabel('Time (secs)')
ylabel('Arm angle (rads)')
legend('Obtained','Predicted');
subplot(2,1,2)
plot([1:s]*dt,theta(2,1:s))
hold on
plot([1:s]*dt,thetaHat(2,1:s),'r--');
xlabel('Time (secs)')
ylabel('Wheel Angle wrt Arm (rads)')
legend('Obtained','Predicted');

