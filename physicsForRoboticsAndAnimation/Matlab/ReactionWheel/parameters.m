

%'amax22withGear'
% gear is 110388 part number
mw = 300/1000; % mass of wheel in Kg
ma = 50/1000; % mass of arm in Kg
r = 6/100; % radius of wheel in Kg
l = 17/100; % length of pendulum in grams
%l =100/100; %tmp  get rid of it
%r = 90/100; % tmp get rid of it
g = 9.8; % gravitational acceleration
bma = ma*l*l; % arm moment of inertia in Kg m2

R = 1.71; % Motor electrical resistance in Ohms
Kt = 19*5.9/1000; % torque constant in Newton Meters/Amps
                  % gear reduction is 19:1
Kb = Kt; 
Mm = (4.38+0.5)/10000000; %Moment of inertia of motor plus gear Kg meter^2 
nuMotor = 17/100000000; % viscous friction of motor New m/rad/sec)
nuGear = 142*nuMotor; % viscous friction of gear New m/rad/sec)
nu2 = nuMotor+nuGear ; % viscous friction gear plus rotor  New
                       % m/(rad/sec)



nu1 = 5*nuMotor; % viscous friction of penculum's arm.


