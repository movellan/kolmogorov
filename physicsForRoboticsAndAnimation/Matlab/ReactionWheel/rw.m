% simulates reaction wheel pendulum. 
% saves the data in a structure called data
% Javier R. Movellan
% April 2010
%

clear

parameters
setThingsUp

theta(1,1) = 1; % initial pendulum angle
theta(1,2) =0*randn(1); % initial wheel position
omega(:,1) =0; % initial velocities

f = 1;
for t=1:s
    V(t) = 6*sin(2*pi*f*t*dt); % voltage applied to the motor
    I(t) = V(t)/R - Kb*Kt *theta(2,t); % current
    taug = c3*[sin(theta(1,t)) ;0]; %torque due to gravity
    torque = c1*V(t) - c2*omega(:,t) + taug;
    alpha(:,t) = Mi*torque; % acceleration
    omega(:,t+1) = omega(:,t)+ alpha(:,t)*dt;
    theta(:,t+1) = theta(:,t) + omega(:,t)*dt + 0*0.5*alpha(:,t)*dt*dt;
end
omega(:,end)=[];
theta(:,end)=[];

% save data
data{1}= dt;
data{2} = theta;
data{3} = V;
save data


% plot
subplot(3,1,1)
plot([1:s]*dt,theta(1,:))
xlabel('Time (seconds)');
ylabel('Arm Angle (Rads)');
subplot(3,1,2)
plot([1:s]*dt,theta(2,:))
xlabel('Time (seconds)');
ylabel('Wheel Angle wrt arm (Rads)');
subplot(3,1,3)
[AX, H1, H2] = plotyy([1:s]*dt,V,[1:s]*dt,I);
xlabel('Time (seconds)');
set(get(AX(1), 'Ylabel'), 'String', 'Voltage (Volts)');
set(get(AX(2), 'Ylabel'), 'String', 'Current (Amps)');



