
M = zeros(2,2);
M(1,1) = mw*(l^2+r^2) + bma;
M(1,2) = mw*(r^2);
M(2,1) = mw*(r^2);
M(2,2) = mw*(r^2);

Mi = inv(M);

c1 = zeros(2,1);
c1(1) = 0 ;
c1(2) = Kt/R;

c2 = zeros(2,2);
c2(1,1) = nu1;
c2(1,2) = 0;
c2(2,1) =0;
c2(2,2) = nu2+Kb*Kt/R; 

c3 = l*(ma+mw)*g; 

Ta = l/g; % Time constant for arm motion  in seconds 
Tw =  mw*r*r/(Kt*Kb/R + nu2); % Time constant for wheel motion
To = min([Ta Tw]) ; % Oveerall time constannt
dt = To/100; % time step for discrete time simulations

s = ceil(10/dt);
f= 1;
omega = zeros(2,s); % angular velocity in rads/sec
theta= zeros(2,s);
alpha = zeros(2,s); %acceleration 
V = zeros(s,1);
I=zeros(s,1);
