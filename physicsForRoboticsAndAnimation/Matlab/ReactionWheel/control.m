% lq control of reaction wheel linearizing about theta_1 =0

clear
parameters
setThingsUp

% put the system in form 
% dx = (a x + b u)dt +c dBt
% where
% x1 = theta1
% x2 = theta2
% x3 = dotTheta1
% x4 = dotTheta2

% we linearize about theta1 =0, in which case
% sin(theta_1) approx theta_1
%
kg = c3;
alpha = [ kg, 0, -nu1, 0; 0,0,0, -nu2-(Kb*Kt)/R];
alpha = Mi*alpha;
beta = Mi*[0; Kt/R];
a = [ 0 0 1 0; 0 0 0 1];
a = [a; alpha];
b = [0;0;beta];
% noise matrix
c=0.0*eye(4); 
c(1,1) =0;
c(2,2)=0;
c(4,4)= 0;
% Reward rate at time t is x'_t p x + u'q u

p = [ 1000 0 0 0; 0 1 0 0 ; 0 0 0 0; 0 0 0 0]; 

q = 0.1;
% Terminal reward  matrix
pT = [ 1 0 0 0; 0 0 0 0 ; 0 0 0 0; 0 0 0 0]; 
xiT=[0 0 0 0]'; % Terminal state
 
% Reward = \int_0^T e^{t/tau} (x'_t p x + u'q u) dt

T= 2; % in seconds
s = ceil(T/dt);

tau = T; % time constant for temporal discount 
tau = inf; % 

% The target trajectory. The trajectory can be constant if we just care
% about an end point. 
xi =zeros(4,s);
f=0.5;
for t=1:s
  xi(1,t) = 0.0*sin(2*pi*f*t*dt);
 % xi(1,t) = 0;
end
xi(:,s) = xiT;


[omega, K]= ctfhlqt(xi,a,b,c,p,pT,q,tau,dt);

% you may want to compare with matlab built in lqr function
% for no discount, infinte horizon
% kMatlab= lqr(a,b,p,q)

x = zeros(4,s);
V = zeros(s,1);
I = zeros(s,1);
sqdt = sqrt(dt);

% starting point
x(:,1) =[-5*2*pi/360;0;0;0]';

for t=1:s-1
  V(t) = omega(t) - K(:,:,t)*x(:,t);
  %if(V(t)>6) V(t) = 6;end
  %if(V(t)<-6) V(t) = -6;end
  I(t) = V(t)/R - Kb*Kt *theta(2,t); % current
  phi = x(:,t) ;phi(1) = sin(phi(1));
  dx = (a*phi+ b*V(t))*dt+c*randn(4,1)*sqdt;
  x(:,t+1) = x(:,t)+ dx;
end

subplot(3,2,1)
plot([1:s]*dt, x(1,1:s)*360/2/pi);
xlabel('Time (secs)')
ylabel('Arm angle (degs)')

subplot(3,2,2)
plot([1:s]*dt, x(2,1:s)*360/2/pi);
xlabel('Time (secs)')
ylabel('Wheel angle (degs)')

subplot(3,2,3)
plot([1:s]*dt, 60*x(3,1:s)/2/pi);
xlabel('Time (secs)')
ylabel('Arm Velocity (RPM)')

subplot(3,2,4)
plot([1:s]*dt, 60*x(4,1:s)/2/pi);
xlabel('Time (secs)')
ylabel('Wheel Velocity (RPM)')

subplot(3,2,5)
plot([1:s-1]*dt, V(1:s-1));
xlabel('Time (secs)')
ylabel('Voltage (Volts)')

subplot(3,2,6)
plot([1:s-1]*dt, I(1:s-1));
xlabel('Time (secs)')
ylabel('Current (Amps)')

