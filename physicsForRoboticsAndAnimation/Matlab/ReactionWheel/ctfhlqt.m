% Continuous time finite horizon linear quadratic tracker
% function [omega, k]= ctfhlqt(xi,a,b,c,p,pT,q,tau,dt) 
% a, b,c are matrices that define  the SDE
% dX = a X dt + b U dt + c dBt
% xi contains the target trajectory. (rows are states, cols are time steps)
% p is the matrix for the quadratic instantaneous state reward 
% pT is the matrix for the quadratic terminal state reward
% q is the matri for the quadratic action reward
% tau is the time constant for the temporal discount of the reward
% dt is the time step for discrete time approximation
%
% Finite Horizon Optimal Control of 
% Control dX = a X dt  + b  U dt + c dBt
% with r(x,u,t) = - (x-xi)'*p*(x-xi) -  u'*q*u;
% r_T(x) = - (x-xi_T)' p_T (x- xi_T)
% v(x,t)  = max_pi {  E[ int_t^T e^{-(s-t)/tau} r(X_s, U_s,t)ds  | X_t= x, pi) 
%              + e^{- (T-t)/tau} r_T(X_T) ] 
% where U_s = pi(X_s)
% 
%  tau =inf  is accepted. 
%
%  Can be shown 
%  v(x,t) = x' alpha_t x + beta_t' x + gamma_t
%  and optimal action
%  u_t = omega(t) - k(t)*x_t 
%  where omega(t) = q^{-1} b' beta_t
%        k(t) = q^{-1} b' *(alpha_t + alpha_t')/2
%
% Javier R. Movellan
% UCSD, April 2010
%


function [omega, k]= ctfhlqt(xi,a,b,c,p,pT,q,tau,dt)

  s = length(xi);
  itau = 1/tau;
 
  qinv = pinv(q);
  qinvbt = qinv*b';
  
  alpha = pT;
  beta = pT*xi(:,s);
  
  nu = length(q);
  omega = zeros(nu,s);
  nx = length(a);
  k2= zeros(nu,nx,s);
  for t=s:-1: 1
    alphaBar= (alpha + alpha')/2;
    dalpha = alpha*itau -p + alphaBar*b*qinv*b'*alphaBar  - 2*alphaBar*a;
    dbeta = -beta*itau - p'*xi(:,t)+ alphaBar*b*qinv*b'*beta  - a'*beta;
    
    omega(t) = qinvbt*beta;
    k(:,:,t) = qinvbt*alphaBar;
    alpha = alpha - dt*dalpha;
    beta = beta - dt*dbeta;
  end

