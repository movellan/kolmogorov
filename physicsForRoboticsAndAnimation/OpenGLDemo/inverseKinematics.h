#define ESCAPE 27
#define SPACE 32
#define GAUSS_NEWTON 1
#define INV_JACOB 2
#define GRAD_DESCENT 3 
#define NLINKS 10

void drawtriangle();


// Functions dealing with the main OpenGL View
void main_display();


// Functions that handle the left OpenGL View
void left_win_reshape(int width, int height);

void left_win_motion(int x, int y);


// Functions that handle the right top OpenGL View
void right_top_win_reshape(int width, int height);
void right_top_win_display();


// Functions that handle the right bottom OpenGL View
void right_bottom_win_reshape(int width, int height);
void right_bottom_win_display();

// Posts redisplay messages to all of the OpenGL Views
void redisplay_all();






