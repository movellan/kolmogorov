//////////////////////////////////////////////
// Inverse Kinematics of simple 2df arm 
// @ Javier R. Movellan
//  Compares Gauss-Newton with Ridge term Gradient Descent, and
//  Inverse Jacobian Methods
// There are bits and pieces from OpenGL tutorial programs from the Web. 
// 
//////////////////////////////////////////////

#include <OpenGL/gl.h>          // Header File For The OpenGL32 Library
#include <OpenGL/glu.h>         // Header File For The GLu32 Library
#include <GLUT/glut.h>          // Header File For The GLut Library
#include"matrix.h" 
#include "inverseKinematics.h"
#include "left_win_display.h"
#include "controller.h"



/* ASCII code for the escape key. */






// Program title
const char *strTitle = "Inverse Kinematics Demo";

//int controlMode = INV_JACOB;
//int controlMode = GRAD_DESCENT;
int controlMode = GAUSS_NEWTON;





GLint timeStep=0;
GLfloat trace[2][2000];
GLfloat x1Pos=20;
GLfloat  y1Pos=20;
double  *Rot;

GLfloat linkLength[NLINKS]={15,15,15,15,15,15,15,15,15,15};
GLfloat linkWidth[NLINKS]={4,4,4,4,4,4,4,4,4,4};


// Window handles - I use these so I can tell GLUT what
// view/window I want to work with
GLuint main_win, left_win, right_top_win, right_bottom_win;

int angle_y = 0, angle_x = 0;
int last_x = 0, last_y = 0;

// Object Drawing Functions


void drawtriangle()
{

}


void main_display()
{
  // clear out the screen
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


  glutSwapBuffers();
}


void left_win_reshape(int Width, int Height)
{

  glViewport(0, 0, Width, Height);		// Reset The Current Viewport And Perspective Transformation

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  //    gluPerspective(40.0f,(GLfloat)Width/(GLfloat)Height,0.1f,100.0f);
  gluPerspective(45.0f,(GLfloat)Width/(GLfloat)Height,1.f,560.f);	// Calculate The Aspect Ratio Of The Window

  glMatrixMode(GL_MODELVIEW);

}


void left_win_motion(int x, int y)
{

}

void left_win_mouse( int button, int state, int x, int y ) {
  if(state == GLUT_DOWN){
    controller(x1Pos, y1Pos);
  }

}



void right_top_win_reshape(int Width, int Height)
{



  glViewport(0, 0, Width, Height);		// Reset The Current Viewport And Perspective Transformation

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  //    gluPerspective(40.0f,(GLfloat)Width/(GLfloat)Height,0.1f,100.0f);
  gluPerspective(45.0f,(GLfloat)Width/(GLfloat)Height,1.f,560.f);	// Calculate The Aspect Ratio Of The Window

  glMatrixMode(GL_MODELVIEW);



}

// This function displays the cube for the right_top view
// looking at it from above and off to the left.

void right_top_win_display()
{

  float maxVelocity=-1.0;
  glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
  glLoadIdentity();				// Reset The View






  gluLookAt(0,0,250,0,0,-100,0,1,0);
 
  // draw velocity curve
  glColor3f(0.9f, 0.2f, 0.2f);
  int i;
  float v;
  glBegin(GL_LINE_STRIP);
  for( i=2;i<=timeStep;i++){
    v = (trace[0][i] - trace[0][i-1])*(trace[0][i] - trace[0][i-1]);
    v = (trace[1][i] - trace[1][i-1])*(trace[1][i] - trace[1][i-1]);
    v=sqrt(v);
    if (v> maxVelocity) maxVelocity=v;
    glVertex3f(-100+(float) i/5.0,-100+200.0*v/maxVelocity,0);
  }
  glEnd();

 
  glutSwapBuffers();




}



void right_bottom_win_reshape(int width, int height)
{
  glEnable(GL_DEPTH_TEST);


  glClearColor(0.0, 0.3, 0.2, 1.0);

  glViewport(0, 0, width, height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-1, 1, -1, 1, -1, 1);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}


void right_bottom_win_display()
{

  glLoadIdentity();


  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


  drawtriangle();
  glutSwapBuffers();
}

// This function forces all of the views to be
// redrawn
void redisplay_all()
{
  glutSetWindow(main_win);
  glutPostRedisplay();

  glutSetWindow(left_win);
  glutPostRedisplay();

  glutSetWindow(right_top_win);
  glutPostRedisplay();
}






/* The function called whenever a key is pressed. */
void keyPressed(unsigned char key, int x, int y) 
{
  /* avoid thrashing this call */
  usleep(100);

  /* If escape is pressed, kill everything. */
  if (key == ESCAPE) 
    { 
      /* exit the program...normal termination. */
      exit(0);                   
    }

  if (key == SPACE) 
    { 
      controller(x1Pos, y1Pos);
     }




  if (key == 'u'){
    y1Pos = y1Pos +4;
  }

  if (key == 'd'){
    y1Pos = y1Pos -4;
  }

  if (key == 'l'){
    x1Pos = x1Pos -4;
  }


  if (key == 'r'){
    x1Pos = x1Pos +4;
  }
    
  if (key == 'n'){
    controlMode = GAUSS_NEWTON;
    printf("Gauss Newton\n");
  }
    

  if (key == 'g'){
    controlMode = GRAD_DESCENT;
    printf("Grad Descent\n");
  }


  if (key == 'i'){
    controlMode = INV_JACOB;
    printf("Inverse Jacobian\n");
  }
    

  glutPostRedisplay();
}












void main(int argc, char* argv[])
{




  Rot=vector(NLINKS);
  int i;
  for(i=0;i<NLINKS;i++){
    Rot[i]=10*i; // initial rotation angles of each link
  }

  
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

  glutInitWindowSize(475, 320);
  glutInitWindowPosition(10, 10);
  glutInit(&argc, argv);

  // create the main view/window, the black area of the window
  main_win = glutCreateWindow( strTitle );

  glutDisplayFunc(main_display);
  // set the clear (background) color of the main view/window to black
  glClearColor(0.0, 0.0, 0.8, 1.0);

  // Left Window
  // Position inside main window: (10, 10)
  // Width: 100
  // Height: 100
  left_win = glutCreateSubWindow(main_win, 10, 10, 300, 300);
  glutReshapeFunc(left_win_reshape);
  glutDisplayFunc(left_win_display);
  glutMotionFunc(left_win_motion);
  glutMouseFunc(left_win_mouse);
  glutKeyboardFunc(&keyPressed);

  // Right Top Window
  // Position inside main window: (320, 10)
  // Width: 145
  // Height: 145
  right_top_win = glutCreateSubWindow(main_win, 320, 10, 145, 145);
  glutReshapeFunc(right_top_win_reshape);
  glutDisplayFunc(right_top_win_display);

  // Right Bottom Window
  // Position inside main window: (320, 165)
  // Width: 145
  // Height: 145
  right_bottom_win = glutCreateSubWindow(main_win, 320, 165, 145, 145);
  glutReshapeFunc(right_bottom_win_reshape);
  glutDisplayFunc(right_bottom_win_display);

  glutMainLoop();
}
