// Draws the kinematic chain
// Javier R. Movellan
#include <OpenGL/gl.h>          // Header File For The OpenGL32 Library
#include <OpenGL/glu.h>         // Header File For The GLu32 Library
#include <GLUT/glut.h>          // Header File For The GLut Library

#include "inverseKinematics.h"
// Global variables

extern GLint timeStep;
extern  double  *Rot; // Declared static in external file
extern GLfloat trace[2][2000];
extern GLfloat x1Pos;
extern GLfloat  y1Pos;
extern GLfloat linkLength[NLINKS];
extern GLfloat linkWidth[NLINKS];

void left_win_display()
{

  glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();				// Reset The View





  /// building background grid for reference

  gluLookAt(0,-150,250,0,0,-100,0,1,0);
  glColor3f(0.2f, 0.2f, 0.8f);
  int i;
  for (i=-100; i<=100;i=i+10) {
    glPushMatrix();
    glTranslatef(i, 0, 0);
    glBegin(GL_LINES);
    glVertex3f(0.0f, 100.0f, 0.0f);
    glVertex3f(0.0f, -100.0f, 0.0f); 
    glEnd();
    glPopMatrix();
  }
  for (i=-100; i<=100;i=i+10) {
    glPushMatrix();
    glTranslatef(0, i, 0);
    glBegin(GL_LINES);
    glVertex3f(100.0f, 0.0f, 0.0f);
    glVertex3f(-100.0f, 0.0f, 0.0f); 
    glEnd();
    glPopMatrix();
  }

  // draw movement trace
  glColor3f(0.5f, 0.5f, 0.2f);


  glBegin(GL_LINE_STRIP);
  for( i=1;i<=timeStep;i++){
    glVertex3f(trace[0][i],trace[1][i],0);
  }
  glEnd();

  glColor3f(0.2f, 0.2f, 0.8f);



  // build sphere for current target  pos
  glPushMatrix();
  glTranslatef(x1Pos, y1Pos, 0);
  glColor3f(0.8, 0.8, 0.8);
  glutSolidSphere(2, 20, 20);
  glPopMatrix();	



  // draw the links

  for(i=0;i<NLINKS;i++){
    glPushMatrix();
    
    if(i>0){
      glTranslatef( linkLength[i-1],0.0, -0.f);
    }
    glColor3f(0.8f, 0.2f, 0.2f);
    glutSolidSphere(3, 20, 20);
    glColor3f(0.2, 0.8, 0.2);;
    glRotatef(Rot[i], 0.0f, 0.0f, 1.0f); 
    
    
    glPushMatrix();
    glScalef(linkLength[i],linkWidth[i],1);
    glTranslatef(0.5,0.0,0);
    glutSolidCube(1.0);
    glPopMatrix();
  }
  
  //glLoadIdentity();				// Reset The View

  // Restore transformations from (top).
   for(i=0;i<NLINKS;i++){
     glPopMatrix();
   }


  // swap the buffers to display, since double buffering is used.
  glutSwapBuffers();

}

