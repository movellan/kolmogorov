#include <math.h>
#include <stdio.h>
#include "typedefs.h"
#include "matrix.h"

FLOATTYPE * vector(n)
int n;
{
  int i;
  FLOATTYPE *p;
  p = (FLOATTYPE *)calloc(n,sizeof(FLOATTYPE));
  for(i=0;i<n;i++)
    p[i] = 0.0;
  return p;
}

int * ivector(n)
int n;
{

  return (int *)calloc(n,sizeof(int));
}

FLOATTYPE ** matrix(n,m)
int n,m;
{
  FLOATTYPE **h;
  int i;

  h = (FLOATTYPE **)calloc(n,sizeof(FLOATTYPE *));
  for (i=0;i<n;i++) h[i]=vector(m);

  if(!h){
	 printf("Allocation Error\n");
		exit(1); }

return h;
}


FLOATTYPE *** matrix3(n,m,o)
int n,m,o;
{
  FLOATTYPE ***h;
  int i;

  h = (FLOATTYPE ***)calloc(n,sizeof(FLOATTYPE **));
  for (i=0;i<n;i++) h[i]=matrix(m,o);
 
 if(!h){
	printf("Allocation Error\n");
	        exit(1); }
 
 return h;
}




int ** imatrix(n,m)
int n,m;
{
  int **h;
  int i;

  h = (int **)calloc(n,sizeof(int *));
  for (i=0;i<n;i++) h[i]=ivector(m);
  return h;
}

void vector_zero(a,n)
FLOATTYPE *a;
int n;
{
  int i;
  for(i=0;i<n;i++) a[i] = 0.0;
}

void matrix_zero(a,m,n)
FLOATTYPE **a;
int m,n;
{
  int i,j;
  for(i=0;i<m;i++) 
    for(j=0;j<n;j++) 
      a[i][j] = 0.0;
}


void print_matrix(a,m,n)
FLOATTYPE **a;
int m,n;
{
  int i,j;

  if (n>0)
  {
  for (i=0;i<m;i++)
  {
    for (j=0;j<n;j++)
    {
      printf("%10.3e ",a[i][j]);
    }
    printf("\n");
  }
  printf("\n");
  }
}

void print_matrix3(a,m,n,o)
FLOATTYPE ***a;
int m,n,o;
{
  int i,j,l;

  if (n>0 && o>0)
  {
  for(l=0;l<o; l++)
   {for (i=0;i<m;i++)
     {
      for (j=0;j<n;j++)
      {
      printf("%10.3e ",a[i][j][l]);
      }
    printf("\n");
   } 
  printf("\n");
  } 
 
 printf("\n");}
}
void print_vector(v,n)
FLOATTYPE *v;
int n;
{
  int i;

  for (i=0;i<n;i++)
  {
    printf("%10.3e ",v[i]);
  }
  printf("\n\n");
}

void row_vector(a,v,i,n)
FLOATTYPE **a,*v;
int i,n;
{
  int j;

  for (j=0;j<n;j++)
  {
    v[j] = a[i][j];
  }
}

void vector_to_matrix(v,a,m,n)
FLOATTYPE **a,*v;
int m,n;
{
  int i,j,indx=0;

  for (i=0;i<m;i++)
  for (j=0;j<n;j++)
  {
    a[i][j] = v[indx++];
  }
}

void scale_matrix(a,s,n,m)
FLOATTYPE **a,s;
int n,m;
{
  int i,j;

  for (i=0;i<n;i++) 
  for (j=0;j<m;j++) 
    a[i][j] *= s;
}
void scale_matrix3(a,s,n,m,o)
FLOATTYPE ***a,s;
int n,m,o;
{
  int i,j,k;

  for (i=0;i<n;i++) 
  for (j=0;j<m;j++) 
  for (k=0;k<o;k++) 
    a[i][j][k] *= s;
}
void init_vector(a,s,n)
FLOATTYPE *a,s;
int n;
{
  int i;

  for (i=0;i<n;i++) 
    a[i] = s;
}

void init_ivector(a,s,n)
int *a,s;
int n;
{
  int i;

  for (i=0;i<n;i++) 
    a[i] = s;
}

void init_matrix(a,s,n,m)
FLOATTYPE **a,s;
int n,m;
{
  int i,j;

  for (i=0;i<n;i++) 
  for (j=0;j<m;j++) 
    a[i][j] = s;
}

void init_matrix3(a,s,n,m,o)
FLOATTYPE ***a,s;
int n,m,o;
{
  int i,j,k;

  for (i=0;i<n;i++) 
  for (j=0;j<m;j++) 
  for (k=0;k<o;k++) 
    a[i][j][k] = s;
}

void normalize_matrix(a,n,m)
FLOATTYPE **a;
int n;
{
  FLOATTYPE sum=0;
  int i,j;

  for (i=0;i<n;i++) 
  for (j=0;j<m;j++) 
    sum += a[i][j]*a[i][j];
  scale_matrix(a,n/sqrt(sum),n,m); 
/*  scale_matrix(a,n/sum,n,m); */
}

void matrix_copy(a,b,n,m)
FLOATTYPE **a,**b;
int n,m;
{
  int i,j;

  for (i=0;i<n;i++) for (j=0;j<m;j++) b[i][j] = a[i][j];
}

void matrix_copy3(a,b,n,m,o)
FLOATTYPE ***a,***b;
int n,m,o;
{
  int i,j,l;

  for (i=0;i<n;i++) for (j=0;j<m;j++) for (l=0;l<o;l++) 
	b[i][j][l] = a[i][j][l];
}



void matrix_copy2(a,b,n,m,sno,smo,tno,tmo)
FLOATTYPE **a,**b;
int n,m,sno,smo,tno,tmo;
{
  int i,j;

  for (i=0;i<n;i++) for (j=0;j<m;j++) b[i+tno][j+tmo] = a[i+sno][j+smo];
}

void matrix_transpose(a,at,n,m)
FLOATTYPE **a,**at;
int n,m;
{
  int i,j;

  for (i=0;i<m;i++)
  for (j=0;j<n;j++)
    at[i][j] = a[j][i];
}


void matrix_add(a,b,c,n,m)
FLOATTYPE **a,**b,**c;
int n,m;
{
  int i,j;

  for (i=0;i<n;i++)
  for (j=0;j<m;j++)
    c[i][j] = a[i][j]+b[i][j];
}

void matrix_add2(a,b,c,f1,f2,n,m)
FLOATTYPE **a,**b,**c,f1,f2;
int n,m;
{
  int i,j;

  for (i=0;i<n;i++)
  for (j=0;j<m;j++)
    c[i][j] = f1*a[i][j]+f2*b[i][j];
}

void matrix_multiply(a,b,c,n,m)
FLOATTYPE **a,**b,**c;
int n,m;
{
  FLOATTYPE sum;
  int i,j,k;

  for (i=0;i<n;i++)
  {
    for (j=0;j<n;j++)
    {
      sum = 0.0;
      for (k=0;k<m;k++)
        sum += a[i][k]*b[k][j];
      c[i][j] = sum;
    }
  }
}

void matrix_multiply2(a,b,c,n,m,l)
FLOATTYPE **a,**b,**c;
int n,m,l;
{
  FLOATTYPE sum;
  int i,j,k;

  for (i=0;i<n;i++)
  {
    for (j=0;j<l;j++)
    {
      sum = 0.0;
      for (k=0;k<m;k++)
        sum += a[i][k]*b[k][j];
      c[i][j] = sum;
    }
  }
}

void matrix_angles(a,b,c,n,m)
FLOATTYPE **a,**b,**c;
int n,m;
{
  FLOATTYPE sum,asum,bsum;
  int i,j,k;

  for (i=0;i<n;i++)
  {
    for (j=0;j<m;j++)
    {
      sum = asum = bsum = 0.0;
      for (k=0;k<m;k++)
      { 
        sum += a[i][k]*b[k][j];
        asum += a[i][k]*a[i][k];
        bsum += b[k][j]*b[k][j];
      }
      c[i][j] = (sum==0.0)?0.0:sum/sqrt(asum*bsum);
    }
  }
}

void copy_vector(a,b,n)
FLOATTYPE *a,*b;
int n;
{
  int i;

  for (i=0;i<n;i++)
    b[i] = a[i];
}

void scale_vector(a,s,n)
FLOATTYPE *a,s;
int n;
{
  int i;

  for (i=0;i<n;i++)
    a[i] = s*a[i];
}

void vector_subtract(a,b,c,n)
FLOATTYPE *a,*b,*c;
int n;
{
  int i;

  for (i=0;i<n;i++)
    c[i] = a[i]-b[i];
}

void vector_multiply(a,b,c,n,m)
/* a is n x m by is m x 1 */
FLOATTYPE **a,*b,*c;
int n,m;
{
  FLOATTYPE sum;
  int i,j;

  for (i=0;i<n;i++)
  {
    sum = 0.0;
    for (j=0;j<m;j++)
      sum += a[i][j]*b[j];
    c[i] = sum;
  }
}

void vector_multiply_left(a,b,c,n,m)
FLOATTYPE *a,**b,*c;
int n,m;
{
  FLOATTYPE sum;
  int i,j;

  for (j=0;j<m;j++)
  {
    sum = 0.0;
    for (i=0;i<n;i++)
      sum += a[i]*b[i][j];
    c[j] = sum;
  }
}

void outer(v1,v2,M,n,m)
FLOATTYPE *v1,*v2,**M;
int n,m;
{
  int i,j;

  for (i=0;i<n;i++)
  for (j=0;j<m;j++)
    M[i][j] = v1[i]*v2[j];
}

FLOATTYPE inner(v1,v2,n)
FLOATTYPE *v1,*v2;
int n;
{
  int i;
  FLOATTYPE sum=0;

  for (i=0;i<n;i++)
    sum += v1[i]*v2[i];
  return sum;
}

void derivative_matrix(D,dy,dx)
FLOATTYPE **D;
int dx,dy;
{
  int i,j,n=dx*dy;

  for (i=0;i<n;i++)
  for (j=0;j<n;j++)
    D[i][j]=0.0;
  for (i=0;i<n;i++)
  {
    D[i][i] = 1;
    D[i][(i+1)%n] = -1;
  }
}

void regularization_matrix(R,n)
FLOATTYPE **R;
int n;
{
  int i,j;

  for (i=0;i<n;i++)
  for (j=0;j<n;j++)
    R[i][j]=0;
  for (i=0;i<n;i++)
  {
    R[i][(i==0)?n-1:i-1] = -1.0;
    R[i][i] = 2.1;
    R[i][(i==n-1)?0:i+1] = -1.0;
  }
}

void covariance_matrix(R,n)
FLOATTYPE **R;
int n;
{
  int i,j;

  for (i=0;i<n;i++)
  for (j=0;j<n;j++)
  {
    R[i][j]=1.0/(1.0+fabs(i-j));
/*    printf("i=%d,j=%d,R[i][j]=%f\n",i,j,R[i][j]); */
  }
}


void comp_cov_mat(X, n, m, C)
FLOATTYPE **X, **C;
int n, m;
{
FLOATTYPE *M, **M2, **N, **O;
float scaling_factor;
int i,l;

M = vector(m);
M2 = matrix(m,m);
N = matrix(m, n);
O = matrix(m, m);

for(i = 0; i < m; i++)
	{M[i] = 0.0;
	 for(l = 0; l < n; l++)
		M[i] = M[i] + X[l][i]/(float)(n);}

for(i = 0; i < m; i++)
	for(l = 0; l < m; l++)
		M2[i][l] = M[i] * M[l];

scaling_factor = 1/(float)(n);
matrix_transpose(X, N, n, m);
matrix_multiply(N, X, O, m, n);
scale_matrix(O, scaling_factor, m, m);
scale_matrix(M2, -1.0, m, m);
matrix_add(O, M2, C, m, m);
}

void identity_matrix(I,n)
FLOATTYPE **I;
int n;
{
  int i,j;

  for (i=0;i<n;i++) for (j=0;j<n;j++) I[i][j]=(i==j)?1.0:0.0;
}

void nrerror(s)
char * s;
{
  printf("%s\n",s);
  exit(0);
}

void ludcmp(a,n,indx,d)
FLOATTYPE **a,*d;
int n,*indx;
{
  int i,imax,j,k;
  FLOATTYPE big,dum,sum,temp;
  FLOATTYPE *vv,*vector();

  vv=vector(n);
  *d=1.0;
  for (i=0;i<n;i++)
  {
    big=0.0;
    for (j=0;j<n;j++)
      if ((temp=fabs(a[i][j])) > big) big=temp;
    if (big == 0.0) nrerror("Singular matrix in routine LUDCMP");
    vv[i]=1.0/big;
  }
  for (j=0;j<n;j++)
  {
    for (i=0;i<j;i++)
    {
      sum=a[i][j];
      for (k=0;k<i;k++) sum -= a[i][k]*a[k][j];
      a[i][j]=sum;
    }
    big=0.0;
    for (i=j;i<n;i++)
    {
      sum=a[i][j];
      for (k=0;k<j;k++)
        sum -= a[i][k]*a[k][j];
      a[i][j]=sum;
      if ((dum=vv[i]*fabs(sum)) >= big)
      {
        big=dum;
        imax=i;
      }
    }
    if (j != imax)
    {
      for (k=0;k<n;k++)
      {
        dum=a[imax][k];
        a[imax][k]=a[j][k];
        a[j][k]=dum;
      }
      *d = -(*d);
      vv[imax]=vv[j];
    }
    indx[j]=imax;
    if (a[j][j] == 0.0) a[j][j]=TINY;
    if (j != n-1)
    {
      dum=1.0/(a[j][j]);
      for (i=j+1;i<n;i++) a[i][j] *= dum;
    }
  }
  free(vv);
}

void lubksb(a,n,indx,b)
FLOATTYPE **a,*b;
int n,*indx;
{
  int i,ii= -1,ip,j;
  FLOATTYPE sum;

  for (i=0;i<n;i++)
  {
    ip=indx[i];
    sum=b[ip];
    b[ip]=b[i];
    if (ii>-1)
      for (j=ii;j<i;j++) sum -= a[i][j]*b[j];
    else if (sum) ii=i;
    b[i]=sum;
  }
  for (i=n-1;i>=0;i--)
  {
    sum=b[i];
    for (j=i+1;j<n;j++) sum -= a[i][j]*b[j];
    b[i]=sum/a[i][i];
  }
}

void inverse(a,y,n) /* this one may be buggy (Javier ) */
FLOATTYPE **a,**y;
int n;
{
  FLOATTYPE d,*col;
  int i,j,*indx;

  col = vector(n);
  indx = ivector(n);
  ludcmp(a,n,indx,&d);
  for (j=0;j<n;j++)
  {
    for (i=0;i<n;i++) col[i]=0.0;
    col[j]=1.0;
    lubksb(a,n,indx,col);
    for(i=0;i<n;i++) y[i][j]=col[i];
  }
}
/* compute pseudo inverse of a using svd method  */
/* substitutes a by its inverse */

void pseudoinverse(u,n) 
FLOATTYPE **u;
int n;
{
  FLOATTYPE *w, *temp;
  FLOATTYPE **v;        /* note I need n^2 +2n memory units */
  FLOATTYPE dummy;
  int r,c,i,j;
  void svdcmp();


  w = vector(n);
  temp =vector(n);
  v = matrix(n,n);

  svdcmp(u,w,v,n,n); /* input matrix = u w vt */
                     /* inverse = v  (1/w) ut */

   /* first I get (1/w) */
  for(r=0;r<n;r++){ 
    if(w[r]) w[r] = 1.0/w[r]; /* divide only if w[i] non-zero */
  }
/* now I transpose u */
  for(r=0;r<n;r++){
    for(c=r+1;c<n;c++){
      dummy = u[r][c];
      u[r][c] = u[c][r];
      u[c][r] = dummy;
    }
  }

/* now I multiply ( v * 1/w) by u_ transposed in one shot*/
/* note (v /w)_{ij} = v_{ij} w_j */
  for(c=0;c<n;c++){
    for(r=0;r<n;r++){
      temp[r] = 0.0;
      for(j=0;j<n;j++){
	temp[r]+= (v[r][j]*w[j])*u[j][c]; /* note I'm using colum c of u */
      }
    }
    for(i=0;i<n;i++){
      u[i][c] = temp[i]; /* we wont use u[i][c] anymmore. save memory */
    }
  }


}




FLOATTYPE determinant(a,n)
FLOATTYPE **a;
int n;
{
  FLOATTYPE d;
  int j,*indx;

  indx = ivector(n);
  ludcmp(a,n,indx,&d);
  print_matrix(a,n,n);
  for (j=0;j<n;j++) d *= a[j][j];
  return d;
}

FLOATTYPE svd(A,V,z,m,n)
FLOATTYPE **A,**V,*z;
int n,m;
{
  int i,j,k,count;
  FLOATTYPE c,s,p,q,r,v,toll=0.1;

  identity_matrix(V,n);
  for (count=n*(n-1)/2;count>0;)
  {
    count=n*(n-1)/2;
    for (j=0;j<n-1;j++)
    {
      for (k=j+1;k<n;k++)
      {
        p=q=r=0;
        for (i=0;i<m;i++)
        {
          p += A[i][j]*A[i][k];
          q += A[i][j]*A[i][j];
          r += A[i][k]*A[i][k];
        }
        if ((q*r==0)||(p*p/(q*r)<toll)) count--;
        if (q<r)
        {
          c=0;
          s=1;
        } else
        {
          q = q-r;
          v = sqrt(4*p*p+q*q);
          c = sqrt((v+q)/(2*v));
          s = p/(v*c);
        }
        for (i=0;i<m;i++)
        {
          r = A[i][j];
          A[i][j] = r*c+A[i][k]*s;
          A[i][k] = -r*s+A[i][k]*c;
        }
        for (i=0;i<n;i++)
        {
          r = V[i][j];
          V[i][j] = r*c+V[i][k]*s;
          V[i][k] = -r*s+V[i][k]*c;
        }
      }
    }
    printf("count=%d\n",count);
    print_matrix(A,m,n);
  }
  for (j=0;j<n;j++)
  {
    q = 0;
    for (i=0;i<m;i++) q += A[i][j]*A[i][j];
    q = sqrt(q);
    z[j] = q;

    for (i=0;i<m;i++) A[i][j] /= q;

  }
}
 
static FLOATTYPE at,bt,ct;
#define PYTHAG(a,b) ((at=fabs(a)) > (bt=fabs(b)) ? \
(ct=bt/at,at*sqrt(1.0+ct*ct)) : (bt ? (ct=at/bt,bt*sqrt(1.0+ct*ct)):0.0))

static FLOATTYPE maxarg1,maxarg2;
#define MAX(a,b) (maxarg1=(a),maxarg2=(b),(maxarg1) > (maxarg2) ? (maxarg1):(maxarg2))
#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

void svdcmp(a,w,v,m,n)
/* given a matrix a[m][n] it computes its singular value decomp */
/* a = (u) (diagonal w) (vT) */
/* u is returned as a by this program */
/* and v is returned instead of vT */

FLOATTYPE **a,*w,**v;
int m,n;
{
  int flag,i,its,j,jj,k,l,nm;
  FLOATTYPE c,f,h,s,x,y,z;
  FLOATTYPE anorm=0.0,g=0.0,scale=0.0;
  FLOATTYPE *rv1;

  if (m<n) nrerror("SVDCMP: too few rows in A");
  rv1=vector(n);
  for (i=0;i<n;i++)
  {
    l=i+1;
    rv1[i]=scale*g;
    g=s=scale=0.0;
    if (i<m)
    {
      for (k=i;k<m;k++) scale += fabs(a[k][i]);
      if (scale)
      {
        for (k=i;k<m;k++)
        {
          a[k][i] /= scale;
          s += a[k][i]*a[k][i];
        }       
        f=a[i][i];
        g = -SIGN(sqrt(s),f);
        h=f*g-s;
        a[i][i]=f-g;
        if (i != n-1)
        {
          for (j=l;j<n;j++)
          {
            for (s=0.0,k=i;k<m;k++) s += a[k][i]*a[k][j];
            f=s/h;
            for (k=i;k<m;k++) a[k][j] += f*a[k][i];
          }
        }
        for (k=i;k<m;k++) a[k][i] *= scale;
      }
    }
    w[i]=scale*g;
    g=s=scale=0.0;
    if (i < m && i != n-1)
    {
      for (k=l;k<n;k++) scale += fabs(a[i][k]);
      if (scale)
      {
        for (k=l;k<n;k++)
        {
          a[i][k] /= scale;
          s += a[i][k]*a[i][k];
        }
        f=a[i][l];
        g = -SIGN(sqrt(s),f);
        h=f*g-s;
        a[i][l]=f-g;
        for (k=l;k<n;k++) rv1[k]=a[i][k]/h;
        if (i != m-1)
        {
          for (j=l;j<m;j++)
          {
            for (s=0.0,k=l;k<n;k++) s += a[j][k]*a[i][k];
            for (k=l;k<n;k++) a[j][k] += s*rv1[k];
          }
        }
        for (k=l;k<n;k++) a[i][k] *= scale;
      }
    }
    anorm=MAX(anorm,(fabs(w[i])+fabs(rv1[i])));
  }

  for (i=n-1;i>=0;i--) 
  {
    if (i < n-1)
    {
      if (g)
      {
        for (j=l;j<n;j++) v[j][i]=(a[i][j]/a[i][l])/g;
        for (j=l;j<n;j++)
        {
          for (s=0.0,k=l;k<n;k++) s += a[i][k]*v[k][j];
          for (k=l;k<n;k++) v[k][j] += s*v[k][i];
        }
      }
      for (j=l;j<n;j++) v[i][j]=v[j][i]=0.0;
    }
    v[i][i]=1.0;
    g=rv1[i];
    l=i;
  }

  for (i=n-1;i>=0;i--) 
  {
    l=i+1;
    g=w[i];
    if (i < n-1)
      for (j=l;j<n;j++) a[i][j]=0.0;
    if (g)
    {
      g=1.0/g;
      if (i != n-1)
      {
        for (j=l;j<n;j++)
        {
          for (s=0.0,k=l;k<m;k++) s += a[k][i]*a[k][j];
          f=(s/a[i][i])*g;
          for (k=i;k<m;k++) a[k][j] += f*a[k][i];
        }
      }
      for (j=i;j<m;j++) a[j][i] *= g;
    } else
    {
      for (j=i;j<m;j++) a[j][i]=0.0;
    }
    ++a[i][i];
  }

  for (k=n-1;k>=0;k--)
  {
    for (its=1;its<=30;its++)
    {
      flag=1;
      for (l=k;l>=0;l--)
      {
        nm=l-1;
        if ((FLOATTYPE)(fabs(rv1[l])+anorm) == anorm)
        {
          flag=0;
          break;
        }
        if ((FLOATTYPE)(fabs(w[nm])+anorm) == anorm) break;
      }
      if (flag)
      {
        c=0.0;
        s=1.0;
        for (i=l;i<=k;i++)
        {
          f=s*rv1[i];
          rv1[i]=c*rv1[i];
          if ((FLOATTYPE)(fabs(f)+anorm) == anorm) break;
          g=w[i];
          h=PYTHAG(f,g);
          w[i]=h;
          h=1.0/h;
          c=g*h;
          s=(-f*h);
          for (j=0;j<m;j++)
          {
            y=a[j][nm];
            z=a[j][i];
            a[j][nm]=y*c+z*s;
            a[j][i]=z*c-y*s;
          }
        }
      }
      z=w[k];
      if (l==k)
      {
        if (z < 0.0)
        {
          w[k] = -z;
          for (j=0;j<n;j++) v[j][k]=(-v[j][k]);
        }
        break;
      }
      if (its==30) nrerror("SVDCMP: No convergence in 30 iterations");
      x=w[l];
      nm=k-1;
      y=w[nm];
      g=rv1[nm];
      h=rv1[k];
      f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y);
      g=PYTHAG(f,1.0);
      f=((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;

      c=s=1.0;
      for (j=l;j<=nm;j++)
      {
        i=j+1;
        g=rv1[i];
        y=w[i];
        h=s*g;
        g=c*g;
        z=PYTHAG(f,h);
        rv1[j]=z;
        c=f/z;
        s=h/z;
        f=x*c+g*s;
        g=g*c-x*s;
        h=y*s;
        y=y*c;
        for (jj=0;jj<n;jj++)
        {
          x=v[jj][j];
          z=v[jj][i];
          v[jj][j]=x*c+z*s;
          v[jj][i]=z*c-x*s;
        }
        z=PYTHAG(f,h);
        w[j]=z;
        if (z)
        {
          z=1.0/z;
          c=f*z;
          s=h*z;
        }
        f=(c*g)+(s*y);
        x=(c*y)-(s*g);
        for (jj=0;jj<m;jj++)
        {
          y=a[jj][j];
          z=a[jj][i];
          a[jj][j]=y*c+z*s;
          a[jj][i]=z*c-y*s;
        }
      }
      rv1[l]=0.0;
      rv1[k]=f;
      w[k]=x;
    }
  }
  free(rv1);
}
