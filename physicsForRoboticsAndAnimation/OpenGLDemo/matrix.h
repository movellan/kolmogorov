#define TINY 1.0e-20
#define SQR(x) ((x)*(x))
#define FLOATTYPE double
FLOATTYPE * vector();
int * ivector();
FLOATTYPE ** matrix();
FLOATTYPE determinant();
FLOATTYPE inner();
void pseudoinverse(double **,int); 
void print_matrix(double**, int,int);
void matrix_multiply(FLOATTYPE **, FLOATTYPE **, FLOATTYPE **,int, int);
void matrix_multiply2(FLOATTYPE **, FLOATTYPE **, FLOATTYPE **, int, int,int);
void matrix_transpose(FLOATTYPE **,FLOATTYPE **,int, int);
void matrix_copy(FLOATTYPE **,FLOATTYPE **,int, int);
void matrix_add(FLOATTYPE **,FLOATTYPE **,FLOATTYPE **,int, int);
void scale_matrix(FLOATTYPE **, FLOATTYPE,int, int);


void vector_multiply(FLOATTYPE **, FLOATTYPE *, FLOATTYPE *, int, int);





void scale_vector(FLOATTYPE *,FLOATTYPE,int);


void vector_subtract(FLOATTYPE *,FLOATTYPE *,FLOATTYPE *,int);

void copy_vector(FLOATTYPE *,FLOATTYPE *, int );
void print_matrix(FLOATTYPE **,int , int);
