// Implements several simple methods for solving inverse kinematics problems
// Author: Javier R. Movellan
//         Thanks to bits and pieces from several OpenGl tutorial templates

#include <OpenGL/gl.h>          // Header File For The OpenGL32 Library
#include <OpenGL/glu.h>         // Header File For The GLu32 Library
#include <GLUT/glut.h>          // Header File For The GLut Library

#include <stdlib.h>
#include <math.h>

#include "matrix.h"
#include "inverseKinematics.h"



extern GLint timeStep;
extern  double  *Rot; // Declared static in external file
extern GLfloat trace[2][2000];
extern int controlMode;
extern GLuint right_top_win,left_win;
extern GLfloat linkLength[NLINKS];

void controller(GLfloat targetX1, GLfloat targetX2) 
{

  GLfloat change;



  double gainScalar;
  double ** tmpMatrix; double ** jacobian; double **jacobianT; 
  double ** pseudoInvJacobian, **gainMatrix, **pseudoInvJacobianNoRidge;
  double *currentX; double *residual; double *theta; double *tmpVector;

  tmpMatrix = matrix(NLINKS,NLINKS); // stores jacobianT * jacobian
  jacobian = matrix(2,NLINKS);
  jacobianT = matrix(NLINKS,2);
  pseudoInvJacobian= matrix(NLINKS,2);
  gainMatrix= matrix(NLINKS,2);
  pseudoInvJacobianNoRidge= matrix(NLINKS,2);
  currentX = vector(2);
  residual = vector(2);
  theta=vector(NLINKS);
  tmpVector = vector(NLINKS); //holds theta vector

  change =2;
  timeStep=0;
  

  while (change>1 && timeStep<2000){
	
    timeStep++;
    int i2, i3;
    for (i2=0;i2<NLINKS;i2++){
      theta[i2] = Rot[i2]/57.3f;
    }

    //forward kinematics

	float thetaCum;


	  currentX[0]=0;
	  currentX[1]=0;
	
	for( i2=0;i2<NLINKS;i2++){
	  thetaCum=0;
	  for (i3=0;i3<=i2;i3++){
	    thetaCum+= theta[i3];
	  }
	  currentX[0] += linkLength[i2]*cos(thetaCum);
	  currentX[1] += linkLength[i2]*sin(thetaCum);
	}
	



    // to draw the trajectory
    trace[0][timeStep]=currentX[0];
    trace[1][timeStep]=currentX[1];





    // Hereafter we do inverse kinematics
    
    residual[0] = (targetX1-currentX[0]);
    residual[1] = (targetX2-currentX[1]);	

    // the goal is to minimize the square distance from the target
    change = (float) hypot((targetX1-currentX[0]),(targetX2-currentX[1]));



    if (change > 1) {
	for (i2=NLINKS-1;i2>=0;i2--){
	  thetaCum=0.0;
	  for (i3=0;i3<=i2;i3++){
	    thetaCum+= theta[i3];
	  }
	  if(i2< NLINKS-1){
	    jacobian[0][i2] = jacobian[0][i2+1] - linkLength[i2]*sin(thetaCum);
	    jacobian[1][i2] = jacobian[1][i2+1] + linkLength[i2]*cos(thetaCum);
	  }
	  if(i2 ==  NLINKS-1){
	    jacobian[0][i2] =  - linkLength[i2]*sin(thetaCum);
	    jacobian[1][i2] =   linkLength[i2]*cos(thetaCum);
	  }
	}
	


	
	// NewtonGauss Method with a ridge term

	matrix_transpose(jacobian,jacobianT,2,NLINKS);


	matrix_multiply(jacobianT,jacobian,tmpMatrix,NLINKS,2);


	if (controlMode != 44){
	  int i;
	  for(i=0;i<NLINKS;i++){
	    tmpMatrix[i][i]+=10.0; //The ridge term. Avoids singularities in inverse Jacobian
	  }                        // My experience is that without
				   // this term the system dies with
				   // more than 2 links. Even with two
				   // links, the system dies when the
				   // two links are aligned
	}

	pseudoinverse(tmpMatrix,NLINKS);


	

	matrix_multiply2(tmpMatrix,jacobianT,pseudoInvJacobian,NLINKS,NLINKS,2);

	


	if(controlMode==GAUSS_NEWTON ) {
	  matrix_copy(pseudoInvJacobian,gainMatrix,NLINKS,2);
	  gainScalar=-0.4;
	}

    if(controlMode==INV_JACOB) {
      matrix_copy(pseudoInvJacobian,gainMatrix,NLINKS,2);
      gainScalar=-0.04;
    }



	

	if(controlMode==GRAD_DESCENT){
	  matrix_copy(jacobianT,gainMatrix,NLINKS,2);
	  gainScalar=-0.0001;
	}


	vector_multiply(gainMatrix, residual,tmpVector, NLINKS,2);

	scale_vector(tmpVector,gainScalar,NLINKS);

	vector_subtract(Rot,tmpVector, Rot,NLINKS); // we dont have vector
					       // add so we substract
					       // the negative of what
					       // we want to add

	//	copy_vector(Rot, tmpVector,NLINKS);
	//scale_vector(tmpVector, 1.0/57.3f,NLINKS);
	//copy_vector(tmpVector, theta,NLINKS);

      }
	

	
    glutSetWindow(right_top_win);
    right_top_win_display();
	


    glutSetWindow(left_win);
	
    left_win_display();
	
	
}


  free(tmpMatrix); free(jacobian); free(jacobianT);free(pseudoInvJacobian);
  free(gainMatrix);free(pseudoInvJacobianNoRidge);free(currentX);free(residual);free(theta);
  free(tmpVector);


}

