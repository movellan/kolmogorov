#!/usr/bin/env python
"""
Plot electric potentical (scalar field) of oscilating charge
"""
# pragma pylint: disable=unexpected-keyword-arg
# %% Setup script
import matplotlib.pyplot as plt
import numpy as np
import pycharge as pc
from matplotlib.animation import FuncAnimation
from matplotlib.colors import LogNorm

# %% Calculate and plot E field
# Create charge and simulation objects


frequency = 1e+8  # in cycles per second
# I run into numerical issues when running at smaller frequencies.


omega = 2*np.pi* frequency  # angular frequency in rads per second
period = 1 / frequency  # period in seconds
sol = 299792458.0 # speed of light in m/sec
wavelength =  sol * period # in meters
amplitude = wavelength/8.0

average_speed = 2*amplitude/period

#dt = 2.330212619485086e-18  # time per simulated frame
n_frames=36
frames_per_period = n_frames
dt = period/frames_per_period



# Create meshgrid in x-y plane between -n to n wavelenghts
lim =  0.5* wavelength


# charge = pc.OscillatingCharge(origin=(0, 0, 0), direction=(1, 0, 0),
#                               amplitude=amplitude, omega=omega)

charge = pc.LinearVelocityCharge(speed=average_speed,init_pos =0)
simulation = pc.Simulation(charge)

npoints = 200  # Number of grid points (reduced for faster computation)
coordinates = np.linspace(-lim, lim, npoints)  # grid from -lim to lim
x, y, z = np.meshgrid(coordinates, coordinates, 0, indexing='ij')  # z=0

# Determine global color limits for the 2D plots
global_v_min, global_v_max = float('inf'), float('-inf')

for t_index in range(n_frames):
    t = t_index * dt
    V = simulation.calculate_V(t=t, x=x, y=y, z=z)
    V_plane = V[:, :, 0]  # Extract 2D plane at z=0
    global_v_min = min(global_v_min, np.min(V_plane[V_plane > 0]))  # Positive values for log scale
    global_v_max = max(global_v_max, np.max(V_plane))

# Set up the figure for animation
fig, ax = plt.subplots(figsize=(8, 8))
cmap = plt.get_cmap('viridis')
im = ax.imshow(np.zeros((npoints, npoints)), cmap=cmap,
               norm=LogNorm(vmin=global_v_min, vmax=global_v_max),
               extent=[-lim, lim, -lim, lim])
ax.set_title("Scalar Potential at t = 0 dt")
ax.set_xlabel("x (m)")
ax.set_ylabel("y (m)")
fig.colorbar(im, ax=ax, label="Potential (log scale)")

# Update function for each frame
def update(frame):
    t = frame * dt
    V = simulation.calculate_V(t=t, x=x, y=y, z=z)
    V_plane = V[:, :, 0]  # Extract 2D plane at z=0
    V_plane[V_plane <= 0] = global_v_min  # Replace non-positive values for log scale
    # transpose so X is horizontal
    im.set_data(V_plane.T)
    ax.set_title(f"Scalar Potential at t = {t*1e9:.2f} nanoseconds")
    return im,

# Create animation
ani = FuncAnimation(fig, update, frames=36)

# Save animation
ani.save("constantVelocity.mp4", fps=36, dpi=200)

plt.show()
