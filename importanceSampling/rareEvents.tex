\documentclass{article}    \usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
\usepackage{fancybox}
\usepackage[round]{natbib}
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}

 \title{Rare Events}
\author{Copyright \copyright Javier R. Movellan}

\newenvironment{frcseries}{\fontfamily{frc}\selectfont}{}
\newcommand{\mathfrc}[1]{{\frcseries#1}}

\newcommand{\vc}{\text{Vec}}
\newcommand{\vct}{\mathcal{T}}
\newcommand{\Rl}{\mathcal{R}}


\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}
\newcommand\vx{\nabla_x v(x,t)}
\newcommand\vt{\nabla_t v}
\newcommand\vxx{\nabla_x^2 v(x,t)}
\newcommand\tr{\text{Tr}}



\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}

\newtheorem{prop}{Proposition}[section]
\newtheorem{defi}{Definition}[section]
\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{lem}{Lemma}[section]
\theoremstyle{definition}
\newtheorem{rem}{Remark}[section]
\theoremstyle{definition}
\newtheorem{ex}{Example}[section]
\newtheorem{note}{Note}[section]






\begin{document}
\maketitle




\newpage

\paragraph{ Problem Statement:}
We have a collection of images, which act as elementary outcomes in a probability space. The random variable $X$ identifies the image, the binary random variable $Y(X)$ represents a ground truth property of the image that we care about, for example, the image has direct sunlight, or the image causes a detector to fail, or the combination of the two. The binary random variable $S(X)$ predicts $Y$ and it is used to decide whether or not an image should be sampled. Observing $S$ of an image is inexpensive. Observing $Y$ is expensive, e.g., it may require a slow and costly labeling process. For convenience we sometime suppress the argument of $S$ or $Y$, so $S=1$ is a shortcut for $S(X)=1$


We let $p$ represent the nominal distributions induced by the images collection. Typically, under $p$ each image has equal distribution.  We let $q$ represent a sampling distribution defined as follows
\begin{align}
q(x) = \epsilon p(x) + (1-\epsilon)  p(x | S=1)
\end{align} 
where 
\begin{align}
\delta(u,v) = \begin{cases}
1,&\text{if $u=v$}\\
0,&\text{else}
\end{cases}
\end{align}
This is known as a "defensive" importance sampling distribution. With probability $\epsilon$ we sample images using the nominal distribution $p$  (e.g., all the images have equal probability). With probability $1-\epsilon$ we sample only from the images for which $S=1$.

Our goal is to estimate $\mu=E^P[Y]$, the proportion of images that exhibit the property  $Y=1$, for example the proportion of images that have direct sunlight and for which an object detector makes a mistake. 


We collect $n$ independently sampled images $X_1, X_2, \cdots, X_n$ with sampling distribution $q$ and use the following importance sample estimator

\begin{align}
\hat \mu = \frac{1}{n} Y_i W(X_i)
\end{align}
where
\begin{align}
W(x) = \frac{p(x)}{q(x)}
\end{align}
where $Y_i = Y(X_i)$. 

\paragraph{Mean, Variance and Coefficient of Variation of the Estimator}

Note that $\hat \mu$ is unbiased:
\begin{align}
E^Q[\hat \mu] &= \frac{1}{n} \sum_{i=1}^n E^Q[Y_i W(X) ]=  E^Q[ Y W(X)] \\
E^Q[Y W(X)] &= \sum_x q(x) f(x) \frac{p(x)}{q(x)} = \sum_x p(x) f(x) = \mu
\end{align}
Moreover the variance of $\hat \mu$ is as follows
\begin{align}
Var^Q[\hat \mu] = \frac{1}{n^2} \sum_i \sigma^2 = \frac{\sigma^2}{n}
\end{align}
where
\begin{align}
\sigma^2 &= Var^Q[Y W(X)] = E^Q[Y^2 W^2(X)] - \mu^2\nonumber\\
&= \sum_x q(x) Y(x)^2 \frac{p(x)^2}{q(x)^2} - \mu^2
= \sum_x  p(x) Y(x) W(x)   -\mu^2\nonumber\\
\end{align}
where we used the fact that $Y(x)=Y^2(x)$
An important measure of precision is the coefficient of variation. It measures the ratio between the standard deviation of $\hat \mu$ and $\mu$. 
\begin{align}
CV(\hat \mu) = \frac{SD(\hat \mu)}{\mu} = \frac{1}{\sqrt{n}} \frac{\sigma}{\mu}
\end{align}
Suppose we want the uncertainty about $\mu$ to be no more than 1/10th the value of $\mu$in this case the CV shoiuld be no more than 0.1




\paragraph{Case 1: Nominal Sampling}
In this case
\begin{align}
\epsilon &=1\\
q(x) &= p(x)\\
W(x) &= 1\\
\sigma^2  &= \sum_x p(x) Y(x) - \mu^2 = \mu - \mu^2 = \mu(1-\mu) \\
Var^Q[\hat \mu] &= \frac{1}{n} \mu (1 - \mu)
\end{align}
and
\begin{align}
CV[\hat \mu] = \frac{\sqrt{1-\mu}}{ \sqrt{n \mu}}
\end{align}
For rare events $\mu \to 0$,  $1-\mu \to 1$ and
\begin{align}
CV[\hat \mu] \approx \frac{1}{\sqrt{n \mu}}
\end{align}
For a desired value of $CV$ we need to get $n$ samples
\begin{align}
n = \frac{1}{ CV[\hat \mu]^2 } \frac{1}{\mu} 
\end{align}
Note
This formula illustrates the difficulty of estimating the probability of rare events. Suppose for example that we want to estimate the probability of a rare event. We know the probability is around 1 in a million, and we want the error in our estimate to be no more than 10 $\%$ of the estimate 

\begin{align}
n = \frac{1}{10^{-2}} \frac{1}{10^{-6}} = 10^8 
\end{align}
i.e., we would need 100 million samples to be able to estimate the probability of the event with the desired accuracy.


\paragraph{Case 2: Perfect Sampler}
Suppose $S=Y$, i.e., the sampler perfectly predicts $Y$ . If we choose $\epsilon =0$ the variance of $\hat \mu$  is zero:
\begin{align}
\sigma^2 &= \sum_x p(x) Y(x)  \frac{p(x)}{p(x|S=1)}   -\mu^2  \nonumber \\
&= \sum_x p(x) Y (x) \frac{p(x)p(S=1) }{p(x) p(S=1|x)} - \mu^2\nonumber\\
&= p(S=1) \sum_x  p(x) Y(x) - \mu^2= 0
\end{align}
Thus even with $n=1$ we can get a perfect estimate of $\mu$ the reason is that $S$ is  a perfect estimator of $Y$. 


\paragraph{Case 3: Imperfect Sampler}
Let $\alpha = p(S=1| Y=0)$. the sampler's false accept rate and $\beta = p(S=0 | Y=1)$, the sampler's false reject rate. Note
\begin{align}
W(x) &= \frac{p(x)}{q(x)} = \frac{p(x)}{\epsilon p(x) + (1-\epsilon) p(x | S=1)}
= \frac{1}{\epsilon + (1-\epsilon) p(x|S=1)/p(x)}\nonumber\\
&= \frac{1}{\epsilon + (1-\epsilon) p(S=1|x)/p(S=1)}\nonumber\\
&= \frac{1}{\epsilon + (1-\epsilon) S(x)/p(S=1)} 
\end{align}
where
\begin{align}
p(S=1)  &= p(Y=1) p(S=1 | Y=1) + p(Y=0) p(S=1| Y=0) \nonumber\\
&= 
\mu (1- \beta) + (1-\mu) \alpha
\end{align}
Thus
\begin{align}
W(x) = \frac{1}{\epsilon + (1-\epsilon)S(x) /(\mu (1- \beta) + (1-\mu) \alpha)}
\end{align}
Note 

\begin{align}
\sigma^2 &= \sum_x p(x) Y(x)  W(x) - \mu^2 = \sum_{Y(x) =1, S(x)=0} p(x) \frac{1}{\epsilon}\nonumber\\
&+  \sum_{Y(x) =1, S(x)=0} p(x) \frac{1}{\epsilon +(1-\epsilon)/p(S=1)}  -\mu^2 \nonumber\\
&= \frac{1}{\epsilon} p(Y=1, S=0) + \frac{1}{\epsilon + (1-\epsilon)/p(S=1)} p(Y=1,S=1) -\mu^2\nonumber\\
&=  \frac{1}{\epsilon} \mu \beta+ \frac{1}{\epsilon + (1-\epsilon)/p(S=1)}  \mu  (1-\beta)  -\mu^2 \nonumber\\
\end{align}

\end{document}
