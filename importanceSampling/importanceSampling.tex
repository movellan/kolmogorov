\documentclass{article}    \usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}

 \title{Importance Sampling}
\author{Copyright \copyright Javier R. Movellan}

\newenvironment{frcseries}{\fontfamily{frc}\selectfont}{}
\newcommand{\mathfrc}[1]{{\frcseries#1}}

\newcommand{\vc}{\text{Vec}}
\newcommand{\vct}{\mathcal{T}}
\newcommand{\Rl}{\mathcal{R}}


\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}
\newcommand\vx{\nabla_x v(x,t)}
\newcommand\vt{\nabla_t v}
\newcommand\vxx{\nabla_x^2 v(x,t)}
\newcommand\tr{\text{Tr}}



\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}

\newtheorem{prop}{Proposition}[section]
\newtheorem{defi}{Definition}[section]
\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{lem}{Lemma}[section]
\theoremstyle{definition}
\newtheorem{rem}{Remark}[section]
\theoremstyle{definition}
\newtheorem{ex}{Example}[section]
\newtheorem{note}{Note}[section]






\begin{document}
\maketitle

    

    
\newpage

\paragraph{ Problem Statement:}

Given a random vector $X$ and scalar function $f$, estimate

\begin{align}
E[f(X)] = \int x p(x) dx
\end{align}
Let $S,S_1,\cdots,S_n$, iid with pdf $q$. The Montecarlo importance samplign estimate of $\mu$ is defined as follows

\begin{align}
\hat \mu = \frac{1}{n} \sum_i f(S_i) w(S_i)
\end{align}

where $w(x)  = p(x)/q(x)$.

$q$ is known as the nominal distribution and $p$ as the sampling distribution. 
\prop{Unbiased Estimator}

\begin{align}
E[f(S_i) w(S_i)] &= \int f(s) w(s) q(s) ds = \int f(s) p(s) ds  = \mu\\
E[\hat \mu] &= \frac{1}{n} n \mu = \mu
\end{align}

\defi{Efficiency}

\begin{align}
E[(\hat \mu -  \mu)^2] = Var[\hat \mu] = \frac{1}{n} Var[ f(S) w(S)]  
= \frac{E[f^2(S) w^2(S)]}{n} - \mu^2
\end{align}

\prop{Efficient Sampling Distribution}

The most efficient sampling distribution $\bar q$ has the following form

\begin{align}
\bar q(x) &= \frac{1}{c} |f(x)| p(x)\\
c &=  \int |f(s)| p(s) ds 
\end{align}

\begin{proof}
Let $w(s) = p(s)/q(s)$ and $\bar w(s) = p(s)/\bar q(s)$. 
\begin{align}
E[f^2(S) \bar w^2(S) ] &= \int f^2(s) \frac{p^2(s)}{\bar q^2(s)} \bar q(s) ds =  c \int |f(s)| p(s) ds   \nonumber\\
&=c^2 = \left( \int |f(s) |  p(s) ds \right)^2 = 
 \left( \int |f(s) | w(s)  q(s) ds \right)^2 \nonumber\\
&\leq  \int f^2(s)  w^2(s)  q(s) ds  \nonumber\\
& = E[f^2(S) w^2(S)]
\end{align}

\end{proof}

\prop{Estimator Variance}
Since the variance of $\mu$ is 
\begin{align}
Var[\hat \mu] = \frac{1}{n} Var[f(S) W(S)] = \frac{1}{n}  E[ (f(S) w(S) - \mu)^2]
\end{align}
a popular estimator of the variance of $\mu$ is
\begin{align}
\hat Var[\hat \mu] 
= \frac{1}{n} \sum_i ( f(s_i) w(s_i) - \hat \mu)^2
\end{align}

\note{\bf Optimal Sampling Distribution.}
We want to minimize 
\begin{align}
Var[\hat \mu] = \frac{1}{n} E[(f(S) w(S) - \mu)^2] = \frac{1}{n} E[f(S)^2w(S)^2] - \mu^2
\end{align}
We can set the optimization problem as a variational problem
\begin{align}
\rho = \int f^2(s) \frac{p^2(s)}{q^2(s)} q(s) ds + \lambda (\int q(s) ds -1) 
\end{align}
Thus

\begin{align}
\frac{\partial \rho}{\partial q(s)} = - f^2(s) \frac{p^2(s)}{q^2(s)} + \lambda
\end{align}
Setting the gradient to zero we get the optimal sampling distribution
\begin{align}
\bar q^2 (s) \propto f^2(s) p^2(s)
\end{align}

\prop{Efficiency Gradient}
We have a family of sampling distributions $q(s,\theta)$ parameterized by $\theta$. Our goal is to find an element of this family which is a local minimum with respect to the expected variance of the estimator. 

Our loss function is 
\begin{align}
\rho(\theta) = \int f^2(s) w^2(s,\theta) q(s,\theta) ds =
\int f^2(s) \frac{p^2(s)}{q(s,\theta)} ds
\end{align}
Thus
\begin{align}
\nabla_\theta \rho(\theta) &= 
- \int f^2(s) \frac{p^2(s)}{q^2(s,\theta)} \nabla_\theta q(s,\theta) ds\nonumber\\
&= -  \int f^2(s) w^2(s,\theta) \left( \nabla_\theta \log q(s,\theta)\right) q(s,\theta) ds
\end{align}
We can sample from $q(s,\theta)$ and get the following gradient estimate 


\begin{align}
\nabla_\theta \rho(\theta) \approx \frac{1}{n}\sum_{i=1}^n f^2(s_i) w^2(s_i,\theta) \nabla_\theta \log q(s_i,\theta)
\end{align}

\prop{Self Normalized Importance Sampling}
Suppose we only know the nominal or the sampling distributions up to a proportionality constant. 

\begin{align}
p(x) = \frac{1}{Z_p} \tilde p(x)\\
q(x) = \frac{1}{Z_q} \tilde q(x)\\
\tilde w(x) = \frac{\tilde p(x)}{\tilde q(x)} = \frac{Z_p}{Z_q} w(x) 
\end{align}

Where $\tilde p$, $\tilde q$, $\tilde w$ are the unnormalized versions of $p$, $q$, $w$

In such cases it is common to use the following estimate

\begin{align}
\tilde \mu = \frac{\sum_{i=1}^n f(S_i) \tilde w(S_i)}{\sum_{i=1}^n \tilde w(S_i)}
 \end{align}
Note

\begin{align}
\tilde \mu = \frac{\sum_{i=1}^n f(S_i)  w(S_i)Z_p/Z_q}{\sum_{i=1}^n w(S_i)Z_p/Z_q}= \frac{\sum_{i=1}^n f(S_i)  w(S_i)}{\sum_{i=1}^n w(S_i)} 
 \end{align}
Moreover, since 
\begin{align}
E[w(S_i)] = \int \frac{p(x)}{q(x)} q(x) dx =1
\end{align}
then the expected value of the denominator is $n$. 

Note the ratio of two unbiased estimators is not unbiased so in general for finete sample sizes 
\begin{align}
E[\tilde \mu] \neq \mu
\end{align}
However $\tilde \mu$ converges to $\mu$ with probability 1 as $n$ increases
\begin{align}
  P(\lim_{n \to \infty} \tilde \mu = \mu) = 1
\end{align}
i.e., $\tilde \mu$ is a strongly consistent estimator of $\mu$

Using the Delta Method (see Section below) we get 
\paragraph{Variance of Self Normalized Importance Sampling}
\begin{align}
Var[\tilde \mu] &\approx \frac{1}{n} Var[(f(S) -\mu)W(S)] = \frac{1}{n} E[(f(S) -\mu)^2 W^2(S)]
\end{align}
with the following sample estimate
\begin{align}
Var[\hat \mu] &\approx \sum_{i=1}^n \bar w(s_i)^2 (f(s_i) - \tilde \mu)^2\\
\bar w(s_i) &= \frac{\tilde w(s_i)}{\sum_j w(s_j)}
\end{align}


\paragraph{Optimal Self Normalized Sampling Distribution}
We formulate this as a variational problem

\begin{align}
\rho = \int (f(s) - \mu)^2 \frac{p^2(s)}{q^2(s)} q(s) ds+\lambda(\int q(s) ds -1)
\end{align}

\begin{align}
\frac{\partial \rho}{\partial q(u)} = - (f(u) -\mu) \frac{p^2(u)}{q^2(u)} +\lambda
\end{align}
Setting the gradient to zero we get the optimal sampling distribution
\begin{align}
q(u) \propto | f(u) -\mu| p(u)
\end{align}





\ex{Event Probability}

Let $f(x) = 1_A(x)$ where $A$ is a set. Thus $E[f(X)] = p(A)$. The optimal samplign distribution for the unnormalized case is 

\begin{align}
q(x) \propto f(x) p(x) = \begin{cases}
1&\text{if $x \in A$}\\
0&\text{else}
\end{cases}
\end{align}
Thus $q(A) = 1$. For the self normalized case
\begin{align}
q(x) &\propto| f(x)-\mu|  p(x)\\
q(A) &= \frac{\int_A | f(x) - \mu|p(x) dx}{\int |f(x) - \mu|p(x)}
= \frac{(1-\mu)\mu}{(1-\mu)\mu + \mu(1-\mu)} = 0.5
\end{align}

\prop{Regression Estimator}
We can use $w(S)$ to estimate $f(S)w(S)$ and reduce the variance of the estimator. This regression based estimator takes the following form

\begin{align}
\bar \mu &= \frac{1}{n}\sum_i f(S_i) w(S_i) - \beta (w(S_i) -1)\\
\beta &= \frac{\sum_i (w(S_i) -\bar w)(f(S_i) w(S_i))}{\sum_i(w(S_i) -\bar w)^2}\\
\bar w &= \frac{1}{n} \sum_i w(S_i)
\end{align} 
Note this estimator is unbiased since $E[w(S_i) -1]=0$. The variance of the estimator is 
\begin{align}
Var[\bar \mu] = (1-\rho^2)  Var[\hat \mu]
\end{align}
where $\rho$ is the Pearson correlation between $f(S)w(S)$ and $w(S)$


\section{Delta Method}
A method to estimte the variance of  some estimators. Let $\mu = E[X] \in \Rl^d$, $g:\Rl^d \to R$. Let $S_1,\cdots, S_n$, iid with same distribution as $X$. Let

\begin{align}
\hat \theta  &= g(\bar S)\\
\bar S &= \frac{1}{n} \sum_i S_i
\end{align}
We want to estimate the variance and bias of $\hat \theta$. To first order
\begin{align}
\hat \theta = g(\bar S) \approx g(\mu) + \nabla g(\mu)' (\bar S - \mu)
\end{align}
Thus
\begin{align}
Var[ \hat \theta] & \approx \nabla f(\mu) ' \;Cov[\bar S]\;\nabla f(\mu)\\
Cov[\bar S] &= \frac{1}{n} Cov[X] = \frac{1}{n} \Sigma
\end{align}
where
\begin{align}
\nabla g_i(u) = \frac{\partial g(u)}{\partial u_i}
\end{align}
This motivates the delta method estimator of the variance

\begin{align}
Var[\theta] \approx\frac{1}{n} \nabla g(\bar S)'\; \hat \Sigma\; \nabla g(\bar S)
\end{align}
where
\begin{align}
\hat \Sigma = \frac{1}{n} (S_i - \bar S)' (S_i - \bar S)
\end{align}
is the estimate of the covariance of $Cov(X)$. 

To estimate the bias we use a second order expansion

\begin{align}
g(\bar S) \approx g(\mu) + \nabla g(\mu)' (\bar S - \mu) + \frac{1}{2} (\bar S - \mu)' H (\mu) (\bar S - \mu)
\end{align}
where
\begin{align}
H_{i,j}(u) = \frac{\partial ^2 g(u)}{\partial u_i \partial u_j}
\end{align}
Thus
\begin{align}
E[g(\bar S)] - g(\mu) \approx \frac{1}{2n} \sum_i \sum_j \Sigma_{i,j} H_{i,j}(\mu) 
\end{align}
The squared error  
\begin{align}
E[(\hat \theta - \theta)^2] &= Var[\hat \theta] + (E[\theta] - \theta)^2
\end{align}
for large $n$ it is dominated by the variance, so we typically ignore the bias. 

\subsection{Application to Estimates of Ratios}
In this case  
\begin{align}
g(x) &= x_1/x_2\\
\theta &=\mu_1/\mu_2\\
\nabla g(x) &= \frac{1}{x_2}\left[\begin{array}{c}1\\- \frac{x_1}{x_2}\end{array}\right] 
\end{align}
Thus
\begin{align}
Var[\hat \theta] &= \frac{1}{n} \frac{1}{\mu_2^2} \Big( Var(X_1) + \frac{\mu_1^2}{\mu_2^2} - 2 Cov(X_1,X_2) \frac{\mu_1}{\mu_2}  \Big)\nonumber\\
&= \frac{1}{n} \frac{1}{\mu_2} \Var[ X_1 - \theta X_2]
\end{align}
\subsection{Application to Self Normalized Importance Sampling}
In this case
\begin{align}
X_1 &= f(S) W(S)\\
X_2 &= W(S)\\
\mu_1 &= E[f(S) W(S) = \mu\\
\mu_2 &=E[W(S)] =1\\
\theta&= \frac{\mu_1}{\mu_2} = \mu\\
\hat \theta &= \tilde \mu = \frac{\sum f(S_i) W(S_i)}{\sum W(S_i)}
\end{align}
and
\begin{align}
\Var[\tilde \mu] \approx \frac{1}{n} Var[f(S) W(S) - \mu W(S)] = Var[(f(S) - \mu) W(S)]
\end{align}

\end{document}


