# goal is to estimate the proportion of scores larger than a threshold in a standard Gaussian distribution
#
# sampling distrribution is gaussian with mean theta and standard deviation 1
# the original distribvution has mean 0 and standard deviation 1
# we perform stochastic gradient descent on the estimator's variance
# to try and find a good sampling distribution
import numpy as np
def importanceWeight(x,theta):
    '''
    get importance weights with respect to samplign
    distribution with parameter theta
    '''
    w =[]
    for u in x:
        p = np.exp(-0.5*(u**2.0))
        q =  np.exp(-0.5*((u-theta)**2.0))
        w.append(p/q)
    return np.array(w)

def sample(n,theta):
    '''
    get n observations from samplign distribution with parameter HOST_ORCHESTRATOR
    '''
    return np.random.normal(theta,1,n)

def gradient(x,theta):
    '''
    gradient of log q(x,theta) with respect to theta
    '''
    return(x-theta)
theta =0
n=100
stepSize=10
threshold=4
from  scipy.stats import norm as gaussian

trueValue  = 1 - gaussian.cdf(threshold)

errorHistory=[]
thetaHistory=[]
for t in range(10000):
    s = sample(n,theta)
    w = importanceWeight(s,theta)
    f = (s>threshold)*1.0
    estimate = np.mean(f*w)
    grad = gradient(s,theta)
    nabla = np.mean(f*f*w*f*grad)
    percentError = 100.0* np.abs(estimate -trueValue)/trueValue
    errorHistory.append(percentError)
    thetaHistory.append(theta)
    print theta,percentError
    theta += stepSize*nabla

from matplotlib import pyplot as plt
