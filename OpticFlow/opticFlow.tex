\documentclass{article}    
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}

 \title{Optic Flow: A Bayesian Approach}
\author{Copyright \copyright Javier R. Movellan}

\newenvironment{frcseries}{\fontfamily{frc}\selectfont}{}
\newcommand{\mathfrc}[1]{{\frcseries#1}}


\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}
\newcommand\vx{\frac{\partial v(x,t)}{\partial x}} 
\newcommand\vt{\frac{\partial v(x,t)}{\partial t}} 
\newcommand\vxx{\frac{\partial^2 v(x,t)}{\partial x^2}} 
\newcommand\R{\mathcal{R}}



\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}



\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{lem}{Lemma}[section]
\theoremstyle{definition}
\newtheorem{rem}{Remark}[section]
\theoremstyle{definition}
\newtheorem{ex}{Example}[section]






\begin{document}
\maketitle


    
\newpage
\section{Basic Formulation}
Let $f: R^2\to R^c$ be an image that maps pixel locations in $\R^2$
into a vector values in $\R^c$ representing intensity over $c$
channels (e.g., color channels). Let $x_1, \cdots, x_n$, where $x_i
\in \R^2$ represent the 2D image coordinates of n image point. Let $u_1, \cdots,u_n$ where
$u_i \in \R^c$ be defined as follows 
\begin{align}
u_i = f(x_i)
\end{align}
Let $\Theta \in \R^2$, be a random vector, known as the {\em optic
  flow}, or the displacement.  It represents an image shift which
we'll need to infer from data. Let $V_i\in \R^n$ a random vector defined
as follows
\begin{align}
V_i= f(u_i+\Theta) + W_i
\end{align}
Where $W_i \in \R^c$ is an iid, zero mean, Gaussian random vector with
precission matrix $\eta_i$.  Given $u_1, \cdots, u_n$ and a random sample $v_1,\cdots,v_n$ of $V_1,\cdots, V_n$
our task is to infer the value of $\Theta$. Note for a fixed value 
$\theta$ of $\Theta$ we get
\begin{align}
p(\theta \given u_{1:n},v_{1:n}) \propto p(u_{1:n}) p(\theta \given u_{1:n}) p(v_{1:n} \given u_{1:n},\theta) 
= p(\theta) p(v_{1:n}\given u_{1:n},\theta)
\end{align}
where
\begin{align}
p(v_{1:n} \given u_{1:n}, \theta) = \sum_i \phi( v_i \given f(x_i+\theta), \eta^{-1}_i)
\end{align}
and $\phi(\cdot\given \mu,\sigma)$ is a multivariate Gaussian
density function with mean $\mu$ and covariance matrix $\sigma$. For small 
$\theta$ 
\begin{align}
f(x_i+\theta) \approx f(x_i) + J_i \; \theta = u_i + J_i \;\theta
\end{align}
where $J_i(x) \in \R^c\times \R^2$ is the Jacobian of $f$ with respect to $x$, i.e.
\begin{align}
J_i = \frac{\partial f(x_i)}{\partial x^T} = 
\left[\begin{array}{cc}
\frac{ \partial f_1(x_i)}{\partial x_{i,1}} &\frac{ \partial f_1(x_i)}{\partial x_{i,2}}\\
\vdots &\vdots \\
\frac{ \partial f_c(x_i)}{\partial x_{i,1}} &\frac{ \partial f_c(x_i)}{\partial x_{i,2}}
\end{array}\right]
\end{align}
Thus, for small $\theta$
\begin{align}
p(v_i \given u_i,\theta) = \phi( d_i \given J_i \theta , \eta_i^{-1})
\end{align}
where
\begin{align}
d_i = v_i-u_i
\end{align}
We let the prior distribution for $\theta$ be Gaussian with mean $\mu_\theta$ and precision matrix $\eta_\theta$. We choose this prior  such that only small values of $\theta$ have large posterior distribution. This makes for the linear approximation to be accurate with high probability. Note 
\begin{align}\label{eqn:logp}
\log p(\theta\given u_i,v_i,\eta_i) = -\frac{1}{2} (\theta - \mu_\theta)' \eta_\theta (\theta - \mu_\theta) -\frac{1}{2} ( d_i - J_i\; \theta)' \eta_i (d_i - J_i \;\theta)+ c
\end{align}
Thus

\begin{align}
\log p(\theta\given u_{1:n},v_{1:n},\eta_i) = - \frac{1}{2} (\theta - \mu_\theta)' \eta_\theta (\theta - \mu_\theta) -\sum_i \frac{1}{2} ( d_i - J_i\; \theta)' \eta_i (d_i - J_i \;\theta)+ c
\end{align}
Note that the log of the posterior distribution has only linear and
quadratic terms on $\theta$ and therefore it has to be Gaussian. The
covariance matrix of the posterior distribution can be derived from
the quadratic terms in \eqref{eqn:logp}
\begin{align}
\sigma_\theta=\text{Cov}(\Theta\given u,v) = \Big(\eta_\theta + \sum_i J' \eta_i J\Big)^{-1}
\end{align}
  Note this covariance matrix does not depend on
$v$.  The posterior  mean can be derived from the linear terms on $\theta$
\begin{align}
\mu_\theta=\text{E}[\Theta \given u,v] = (\eta_\theta + \sum_i J' \eta_i J)^{-1}  (\eta_\theta \mu_\theta +\sum_i J_i' \eta_i d_i)
\end{align}

Note  the Jacobian $J_i\in \R^c\times R^2$ has the following form 
\begin{align}
J_i = \frac{\partial f(x_i)}{\partial x_i'} = 
\left(\begin{array}{cc}
\frac{ \partial f_1(x_i)}{\partial x_{i1}} &\frac{ \partial f_1(x_i)}{\partial x_{i2}}\\
\vdots &\vdots \\
\frac{ \partial f_c(x_i)}{\partial x_{i1}} &\frac{ \partial f_c(x)}{\partial x_{i2}}
\end{array}\right)
= \left(\begin{array}{c} J_{i1}\\\vdots\\J_{ic}\end{array}\right)
\end{align}
where
\begin{align}
J_{ij}= \frac{\partial f_j(x_i)}{\partial x_i'} = (\frac{ \partial f_j(x_i)}{\partial x_{i1}}, \frac{ \partial f_j(x_i)}{\partial x_{i2}})
\end{align}
In this case
\begin{align}
J_i'J_i = \sum_{j=1}^c J'_{ij} J_{ij}
\end{align}
Moreover the difference image $d \in \R^c$ also has $c$ channels and
\begin{align}
J_i'd_i = \sum_{j=1}^c J'_{ij} d_j
\end{align}
Thus
\section{Uncertainty About future Flow Prior to Observing $V$}
Here we try to anticipate prior to observing $V$ what the unertainty about the flow will be once we observe $V$. One way to characterized this uncertainy is by averaging accross possible values of $v$
\begin{align}
\text{E}[\text{Cov}[\Theta \given u,V]] = \int p(v\given u) \text{Cov}[ \Theta \given u,v] \; dv =  \Big(\eta_0 + J' \eta_w J\Big)^{-1}
\end{align}
\mynote{NOte this is not necessarily the same as $Cov(\Theta \given u)$ This is weird though. In principle we said that $p(\theta\given u) = p(\theta)$ yet it seems like once we observe $u$ we can change our belief about the uncertainty about $\Theta$. I think the issue is similar to the difference between  $H(\theta\given u)$ and $H(\Theta \given u,V)$}
We can get a scalar measure of uncertainy using the expected entropy of the posterior distribution. From the formula for the entropy of the Gaussian distribution we get
\begin{align}
H(\Theta \given u,v) =  \frac{1}{2} \log \Big| \text{Cov }(\Theta \given u,v)\Big| +C = - \frac{1}{2} \log \Big|\eta_0 + J' \eta_w J\Big| + C
\end{align}
where $C$ is a constant. Note the posterior entropy is independent of
$v$.  Thus we can compute the entropy prior to observing $v$ expected
to be most informative, prior to observing $v$
\begin{align}
H(\Theta\given V,u) &= \int  p(v\given u) H(\theta \given u,v)\; dv \nonumber\\
&= -\frac{1}{2} \log \Big|\sigma^{-1}_0 + J' \eta_w J\Big| + C
\end{align}
Note we can use $H(\Theta \given V,u)$ as a measure of uncertainty about the shift $\Theta$ to come, prior to observing $V$. 

An alternative measure of uncertainty is the trace of the covariance
of the posterior distribution
\begin{align}
\text{Tr\Big(Cov}[\Theta\given u,v]\Big) = \int p(v\given u) \text{Tr\Big(Cov}[\Theta\given u,v]\Big) dv=  \text{Tr}\Big( (\sigma_\theta^{-1} + J' \sigma_w^{-1} J)^{-1}\Big)
\end{align}
For the justification of this measure, see the literature on A optimality. 
\subsection{Example}
A simple case occurs when $n=1$, i.e., we calculate flow based on a
single point. In this case $x \in \R^2$
\begin{align}
J = (
\frac{\partial f(x)}{\partial x_1}, 
\frac{\partial f(x)}{\partial x_2})\\
J'J = \left(\begin{array}{cc}
(\frac{\partial f(x)}{\partial x_1})^2 & 
\frac{\partial f(x)}{\partial x_1}\frac{\partial f(x)}{\partial x_2} \\
\frac{\partial f(x)}{\partial x_1}\frac{\partial f(x)}{\partial x_2} &
(\frac{\partial f(x)}{\partial x_2})^2  
\end{array}
\right)
\end{align}
Assuming diagonal matrices of the form
\begin{align}
\sigma_\theta = s_\theta I\\
\sigma_w = s_w I
\end{align}
where $s_\theta, s_w$ are scalars and $I$ the $2\times 2$ identity
matrix, we get
\begin{align}
\text{Cov}(\Theta \given u,v) 
= \Big(\frac{1}{s_w} ( J'J + \frac{s_w}{s_\theta}I) \Big)^{-1}
\end{align}
Thus
\begin{align}
\log \big| \text{Cov}(\Theta \given u,v)\big| &= 
 \log s_w - \log  \Big| J'J + \frac{s_w}{s_\theta}\Big| \nonumber\\
&= \log s_w - \log  ((J'J)_{1,1} + \frac{s_w}{s_\theta} )  ((J'J)_{2,2} + \frac{s_w}{s_\theta} ) - (J'J)_{1,1} (J'J)_{2,2}) \nonumber \\
&= \log s_w - \log( \frac{s_w}{s_\theta} ( (J'J)_{1,1} + (J'J)_{2,2}))\nonumber\\
&= \log s_\theta - \log \Big( (\frac{\partial f(x)}{\partial x_1} )^2 + 
(\frac{\partial f(x)}{\partial x_2})^2 \Big)\nonumber \\
&= \log s_\theta - \log \| \nabla_x  f(x)\|^2
\end{align}
In other words, the expected uncertainty about $\Theta$ at a point $x$
is a constant plus the log of the length of the gradient of the image at that point. 



\section{Practical Aspects}

\end{document}
