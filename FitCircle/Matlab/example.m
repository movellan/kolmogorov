clear
clf
n=6;
r=11;



alpha = rand(n,1)*pi/2;
x = r*[sin(alpha) cos(alpha)];

x(:,1) = x(:,1) + 10;
x(:,2) = x(:,2) + 11;

x(:,3) = ones(n,1);

tolerance = 0.001;

[center, radius, mse] = fitCircle(x,tolerance)


subplot(2,1,1)
scatter(x(:,1),x(:,2),'r*' );
scatter(x(:,1),x(:,2),'r','filled' );
hold on

alpha = rand(1000,1)*2*pi;

x = radius*[sin(alpha) cos(alpha)];
x(:,1) = x(:,1)+center(1);
x(:,2) = x(:,2)+center(2);


scatter(x(:,1),x(:,2),'b.');
hold off
subplot(2,1,2)
plot(mse)