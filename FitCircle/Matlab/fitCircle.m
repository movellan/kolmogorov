% function [center, radius, mse] = fitCircle(x,tolerance)
% returns center and radius of circle that best fits the points in x
% rows of x have the different points to be fit
% columns are the coordinates of x. Program accepts 1,2 ,3 coordinate points
% x should have  2 columns and as many points as coordinates plus 1
%
% The problem is solved iteratively using gradient descent
%
% Copyright @2009 Javier R. Movellan
% BSD style license
%

function [center, radius, mse] = fitCircle(x,tolerance)

epsilon = 0.01;  % step size for gradient descent
maxIter=100000;


m=size(x,2);
theta=mean(x,1);  
barX = mean(x,1);
for (i=1:m)
  delta(:,i) = barX(i) - x(:,i) ;
end


  
k=1;



rho= zeros(maxIter,1);
rho(1) = tolerance+10;
while((rho(k) > tolerance) && k<maxIter)
  k=k+1;
  for(i=1:m)
    lambda(:,i) = x(:,i) - theta(i);
  end
  
  lambda = lambda.^2;
  lambda = sum(lambda,2);
  barLambda = mean(lambda);
  lambda= barLambda -lambda;
  for(i=1:m)
    s(:,i) = delta(:,i).*lambda;
  end
  
    theta = theta + epsilon*mean(s,1);
  rho(k) = std(lambda);
end
center=theta
radius=sqrt(barLambda)
mse= rho(k)

