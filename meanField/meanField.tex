\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}
\usepackage{bbm}



\newcommand\argmin{\operatornamewithlimits{argmin}}

\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand\given{\medspace|\medspace}
\newcommand\argmax{\operatornamewithlimits{argmax}}


 \title{The Mean Field Algorithm}
\author{ Javier R. Movellan}





\newenvironment{namelist}[1]{%
\begin{list}{}
	{ 		\let\makelabel\namelistlabel
\skettowidth{\labelwidth}{#1}
\setlength{\leftmargin}{1.1\labelwidth} 	} }{%
\end{list}}

\empty

% Replace with the directory where the graphs have been placed
\newcommand{\graphdir}{./graphs}
\newcommand{\centerfig}[2]{\centerline{\psfig{figure=#1,width=#2}}}




\begin{document}
\maketitle






\newpage

\section{Variational Approximations}

Let $X,Y$ be random vectors. We wish to approximate $p(y \given x)$ for cases in which it is intractable. To this end we use an approximating distribution $q$ that minimizes the KL divergence with $p(y |x)$. In this case it is convenient to use the version of the KL divergence  such that the approximating function $q$  averages the difference between the approximation and the approximated distributions.
\begin{align}
D_x(q|p) &= \int q(y) \log \Big(\frac{q(y)}{p(y|x)}\Big) dy =  \int q(y) \big(\log  q(y) -\log p(y |x) \Big) dy \nonumber\\
&=  \int q(y)  \log q(y) dy - \int  q(y) \log p(x,y) dy + \log p(x)  \nonumber\\
& = \log p(x) - L_x(q,p)
\end{align}
where
\begin{align}
L_x(q,p) &=   \int  q(y) \log p(x,y) dy -\int q(y)  \log q(y) dy \nonumber\\
&  = \log p(x) - D(q|p) \leq \log p(x)
\end{align}
is known as the evidence lower bound (ELBO). Thus to minimize $D_x$ we need to maximize $L_x$

\section{Mean Field Approximation}
in the mean field approximation the distribution $q$ is constrained to be a product of its components:
\begin{align}
q(y) = \prod_i q_i(y_i)
\end{align}
and we typically find a solution via coordinate-wise gradient descent. To do so we identify the components of $D_x$ that depend on $q_j(y_j)$. First note
\begin{align}
q(y) = q_j(y_j) q_{-j}(y_{-j})
\end{align}
where $y_{-j}$ refers to all the components of $y$ excluding $y_j$. Thus
\begin{align}
\int q(y) \log q(y) dy &= \int q_j(y_j) \log q(y_j) dy_j \nonumber \\
&+  \int q_{-j}(y_{-j}) \log q_{-j}(y_{-j}) dy_{-j}
\end{align}
where the second component does not depend on $q_j(y_j)$.  Next we get the components of $ \int  q(y) \log p(x,y) dy$ that depend on $q_j(y_j)$.  Note 
\begin{align}
p(x,y) = p(x) p(y_{-j}|x) p(y_j| x, y_{-j})
\end{align}
.Thus
\begin{align}
\int q(y) \log p(y|x) dy &=   \log p(x) \nonumber \\
& + \int q(y) \log p(y_{-j} | x) dy \nonumber \\
&+ \int q(y) \log p(y_j | x, y_{-j}) dy 
\end{align}
The first term is independent of $q_j(y_j)$. Regarding the second term,
\begin{align}
\int q(y) \log p(y_{-j} | x) dy  &= \int q_j(y_j) \int q_{-j}(y_{-j})  \log p(y_{-j} | x) dy_{-j} dy_j \nonumber\\
&=  \int q_{-j}(y_{-j})  \log p(y_{-j} | x) dy_{-j} 
\end{align}
which is also independent of $q_j(y_j)$.  Regarding the last term
\begin{align}
 \int q(y) \log p(y_j | x, y_{-j}) dy &= \int q(y_i) \Big( \int q_{-j} (y_{-j}) \log p(y_j | x, y_{-j}) d y_{-j}\Big) d y_j \nonumber\\
 &= \int q_j (y_j) E^Q[ \log p(y_j | x, Y_{-j} )] dy_j
\end{align}
Thus
\begin{align}
\argmin_{q_j(y_j)}  D(q|p) = \argmin_{q_j(y_j) }&\int q_j(y_j) \log q_j(y_j) dy_j  \nonumber\\
&+  \int q_j (y_j) E^Q[ \log p(y_j | x, Y_{-j} )] dy_j 
\end{align}
Where 
\begin{align}
E^Q[ \log p(y_j | Y_{-j},x )]  = \int q(y_{-j}) \log p(y_j \given  y_{-j},x ) dy_{-j} 
\end{align}
Using Lagrange multipliers to ensure that $\sum_u q_j(u) =1$, differentiating and setting the gradient to zero we get

\begin{align}
q_j(y_j) \propto e^{ E^Q[ \log p(y_j | x, Y_{-j} )]}
\end{align}
Note
\begin{align}
E^Q[ \log p(y_j | Y_{-j},x )]  =& \int q(y_{-j}) \log p(y_j \given  y_{-j},x ) dy_{-j} \nonumber\\
 =&  \int q(y_{-j}) \log p(y_j ,  y_{-j},x ) dy_{-j} \nonumber\\
  &-  \int q(y_{-j}) \log p( y_{-j}|x ) dy_{-j}  
\end{align}
and since  $\int q(y_{-j}) \log p( y_{-j}|x ) dy_{-j} $ is a constant with respect to  $y_j$
\begin{align}
q_j(y_j) \propto e^{V_j(y_j) }
\end{align}
where 
\begin{align}
V_j(y_j) = E^Q[ \log p(y_j , Y_{-j} | x  )]
\end{align}
Note
\begin{align}
&q(y) \propto e^{V(y)}\\
&V(y) = \sum_i V_i(y_i)
\end{align}
In other words, under $Q$ the potential function only has unary terms. 


\paragraph{Example: Mean Field Boltzmann machines}
Boltzmann machines have binary units, i.e., $Y_i \in \{0,1\}$  their equilibrium probability has the following distribution
\begin{align}
p(y) &= \frac{1}{Z} e^{-V(y)}\\
V(y) = &-\frac{1}{2} y' w y \\
Z=& \int e^{-V(y)} dy
\end{align}
Where $w$ is a symmetric, zero diagonal matrix.  In this case there is no inputs so we do not need to worry about $x$. In addition
\begin{align}
V(y) = y_j n_j (y_{-j})+ K\\
n_j(y_{-j} ) = \sum_{k\neq j}  w_{j,k} y_k  
\end{align}
where $K$ is independent of $y_j$. Note
\begin{align}
p(Y_j =1 , y_{-j} ) &= \exp\{y_j n_j(y_{-j}) \} \frac{1}{ZK}\nonumber\\
E^Q[\log p_j(y_j, Y_{-j})] &= \sum_{y_{-j}} q(y_{-j}) \log p(y_j , y_{-j}) \nonumber\\
&=
\sum_{y_{-j}}  q(y_{-j})  \log\Big( \frac{\exp\{y_j n_j(y_{-j}) \}}{ \sum_{y_j} 
\exp\{y_j n_j(y_{-j}) \} } \Big)\nonumber\\
&= \sum_{y_{-j}}  q(y_{-j})  y_j n_j(y_{-j}) +W\nonumber\\
&= W  + y_j  \sum_{y_{-j}}  q(y_{-j}) n_j(y_{-j})\nonumber\\
&= W  + y_j    \sum_{k\neq j} w_{j,k} q_k
\end{align}
where
\begin{align}
&W=- \sum_{y_{-j}}  q(y_{-j})  
\log \Big(  \sum_{y_j} 
\exp\{y_j n_j(y_{-j}) \} \Big) 
\end{align}
is independent of $y_{j}, y_{-j}$. 

Thus
\begin{align}
&q_j(1) = q_j(Y_j=1)  \propto  \exp \Big(1  \sum_{k\neq j} w_{j,k} q_k\Big)\\
&q_j(0) = q_j(Y_j=0) \propto  \exp \Big(0  \sum_{k\neq j} w_{j,k} q_k\Big) =1\\
& q_j(1) = \frac{e^{\sum_{k\neq j}  w_{j,k} q_k}}{e^{-\sum_k w_{j,k} q_k}+1} = \frac{1}{1+ e^{-\sum_k w_{j,k} q_k}} = \text{logistic}(\sum_k w_{jk} q_k )
\end{align}
In vector form:
\begin{align}\label{eq1}
q = \text{logistic}(wq)
\end{align}
To find the vector $q$ that satisfies \eqref{eq1} we can update one unit at a time until equilibrium or we can simulate the following differential equation to equilibrium
\begin{align}
\frac{d q_t}{dt} = \text{logistic}(wq_t) - q_t
\end{align}
where $q_t$ is the probability vector at time $t$. 
\end{document}






