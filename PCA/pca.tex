\documentclass{article}
\usepackage{nips00e}
\usepackage{kolmogorovmath}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage[pdftex]{graphicx}
\usepackage{psboxit}
 \title{Tutorial on Principal Component Analysis}
\author{ Javier R. Movellan}








\begin{document}
\maketitle

    
\newpage


\section{Spectral Theorem}

Let $A$ be a $k \times k$ positive definite symmetric matrix. Then $A$
can be decomposed as follows:


\begin{equation}\label{eq:spectral}
A = P \Lambda P^T
\end{equation}
where $P$ is a $p \times p$ orthonormal matrix: $PP^T = I$, and $\Lambda$ is a $p \times p$ diagonal matrix.

\begin{equation}
\Lambda  = 
\begin{bmatrix}
\lambda_1 & 0         &\cdots &0 \\
0         & \lambda2  &\cdots &0 \\
0         &0          &0      &\lambda_p
\end{bmatrix}
\end{equation}



\noindent If all $\lambda_i$ are different, $P$ is unique up to multiplication of each column by $-1$ otherwise there is an infinite number of solutions.


\subsection{Eigen Form}

From \ref{eq:spectral} it follows that
\begin{equation}
A P = P \Lambda
\end{equation}

or equivalently,

\begin{equation}
A e_i = \lambda_i e_i
\end{equation}
where $e_i$ is the $i^{th}$ column of $P$. Thus, {\sl the
columns of $P$ are the eigenvectors of $A$,} and the $\lambda_i$ are
the eigenvalues associated to each column. For convenience the
columns of $P$ are organized such that $\lambda_1 \geq \lambda_2
\cdots \geq \lambda_p $.

\section{The Linear Compression Problem}

Let 
\begin{itemize}

\item $X^T = [X_1, \cdots , X_p]$ be a random vector on $(\Omega, \F, P)$, with mean $\mu$ and variance matrix $\Sigma$. Let $\{ \lambda_1 >
\lambda_2 > \cdots > \lambda_p > 0\}$ be the eigenvalues of the
covariance matrix, and $\{e_1, \cdots e_p\}$ the associated
eigenvectors.

\item $\B=\{u_i \in R^p; i=1 \cdots k\}$,  an orthonormal basis of $\R^k$: $u_i \cdot u_j = \delta_{i,j}$. 

\item Let $U=[u_1,\cdots, u_k]$ a matrix whose columns are the basis vectors. 

\item $V$, the  subspace spanned by $\B$

\item $Y_i = u_i^T X$, the length of the projection of $X$ onto
$u_i$. In matrix form: $Y = U^T X$. Call $Y_i$ the $i^{th}$ component of $X$ w.r.t. the basis $\B$


\item $\hat{X} = Proj_V X = \sum_{i=1}^k Y_i u_i$, the projection of $X$
onto subspace $V$. In matrix notation $\hat{X} = U Y = U U^T X$.


\end{itemize}

\noindent Our goal is to find an orthonormal basis that minimizes the mean square error
\begin{equation}
E \| X - \hat{X} \|^2 = \sum_{i=1}^p E (X_i - \hat{X}_i)^2
\end{equation}

\subsection{Example}

$X$ may be a random variable describing a sample of $N$ images. Thus,
$x = (x_1, \cdots, x_p)$ represents a specific image. Each component
$x_i$ is a pixel value.  The distribution of $X$ is defined by the
sample: $P(X= x) =1/N$ if $x$ in is in the sample, $0$ otherwise.
We want to approximate all the images in the sample as a linear
combination of a set of images $\{ u_1, \cdots u_k \}$. The basis
images have to be orthonormal and we want to choose them s.t. the mean
squared error made on a pixel by pixel basis be as small as
possible. This gives us a nice compression scheme. The sender and
receiver know the basis images. The receiver simply sends the
components of a new image (i.e., the inner product between the new
image and each of the images in the codebook) and the receiver
reconstruct the new image by adding up the images in the codebook
times their corresponding weights (i.e., the components).

\subsection{Network Interpretation}

We can frame the problem from a neural net point of view as an
auto-encoder problem. An input pattern is transformed into a set of
activations in the hidden layer. The output has to be a reconstruction
of the input based on the activations of the hidden layer. Let $X^T =
[X_1 \cdots X_p]$ is the input to a {\sl linear feed-forward
network}. The components $Y=[Y_1 \cdots Y_k]$ are the activations of
the hidden layer. The matrix $U = [u_1 \cdots u_p]$ is the matrix of
connections between the input and hidden unit. Each vector $u_i$ in
the orthonormal basis is the fan-in weight vector from the input $X$
onto the $i^{th}$ hidden unit: $Y_i = u^T_i X = \sum u_{i,j} X_i
$. The random vector $\hat{X}$ is the activation of the output
layer. The transpose of $U$ is the matrix of connections from hidden
units to output units. Thus $\hat{X} = U Y = U U^T X.$ Our goal is to
find a weight matrix $U$ that minimizes the mean squared difference
between the input $X$ and the output $\hat{X}$.

The step from input to hidden unit can be seen as an analysis
process. The $X$ are modeled as being formed by a combination of {\sl
uncorrelated sources}, the components, that we want to recover. The
step from hidden to outputs can be seen as a synthesis process. Given
the estimated sources, we reconstruct the input.
\begin{center}
\begin{figure}[h]
\input{Media/encoder.ltx}
\caption{ A network interpretation of the compression problem}
\label{fig:ouproc}
\end{figure}
\end{center}


\subsection{Fact 1} 

First of all, using the projection theorem, we know that for a given linear subspace $V$, the best linear approximation to $X$ is the
projection of $X$ onto $V$. We
symbolize this projection as $\hat{X}$. It follows that,

\begin{equation}
E \| X \|^2 = E \| \hat{X} \|^2 + E \| X - \hat{X} \|^2 
\end{equation}

\noindent{\bf Proof:}
For each $x = X(\omega)$, 
\begin{equation}
x= \hat{x} + (x - \hat{x})
\end{equation}
 
and since $\hat{x}$ is the projection of $x$ onto subspace $V$, 

\begin{equation}
\|x\|^2 = \|\hat{x}\|^2 + \|x - \hat{x}\|^2
\end{equation}

Taking expected values, Fact 1 follows.\\ 


\begin{flushright} $\square$ \end{flushright}

\subsection{Fact 2}
\begin{equation}
E \| \hat{X} \|^2 = \sum_{i=1}^k E(Y_i^2)
\end{equation}

\noindent{\bf Proof:}

For each $x= X(\omega)$,  the projection of $x$ onto $V$, is 

\begin{equation}
\hat{x} = \sum_{i=1}^k y_i u_i
\end{equation}

where $y_i = u_i^T x$ is the length of the projection onto the basis vector $u_i$. Since the $u_i$ are orthogonal to each other,

\begin{equation}
\|\hat{x}\|^2 = \sum_{i=1}^k \|y_i u_i\|^2
\end{equation}

and since $u_i$ have unit length, 

\begin{equation}
\|\hat{x}\|^2 = \sum_{i=1}^k y_i^2
\end{equation}
 
Taking expected values, Fact 2 follows.
\begin{flushright} $\square$ \end{flushright}

\subsection{Corollary to Fact 2}

Note  $E(Y_i) = E(u_i^TX) = u_i^T \mu$. Moreover, $Var(Y_i) =
E(Y_i^2) - E^2(Y_i)$. Therefore,

\begin{equation}
E\|\hat{X}\|^2 = \sum_{i=1}^k Var(Y_i) - (u_i^T \mu)^2
\end{equation}

Thus, if $X$ has zero mean, $\mu^T=[0 \cdots 0]$ 

\begin{equation}
E\|\hat{X}\|^2 = \sum_{i=1}^k Var(Y_i) 
\end{equation}

\subsection{Remark}
 
Assume the input variables have zero mean $\mu^T=[0 \cdots 0]$, which
is always easy to achieve.  Using  Fact 1 and 2, 

\begin{equation}\label{eq:maxvar}
E \| X - \hat{X} \|^2  = E \| X \|^2 - \sum_{i=1}^k Var(Y_i) 
\end{equation}


{\em Since $E \| X \|^2$ is fixed, minimizing the mean square error is
equivalent to maximizing the variance of the components.
}

\subsection{Fact 3} The variance of the coefficients is maximized by using the first $k$ eigenvector of $\Sigma$ as the basis  set: $u_i = e_i, i=1 \cdots k$.

\noindent{\bf Proof:}

The proof works by induction. First we assume it is true for $k-1$ and
prove it true for $k$. Then we show it is true for $k=1$.

We know
\begin{equation}
Var(Y_k) = Var(u^T_k X) = u^T_k \Sigma u_k =  u^T_k P \Lambda P^T  u_k  = 
 w^T \Lambda w
\end{equation}

\noindent where $w^T = [w_1 \cdots w_k] = u^T_k P$. Note $w^T \Lambda w$ is a quadratic form, thus

\begin{equation}
Var(Y_k) = \sum_{i=1}^p w_i^2 \lambda_i
\end{equation}


Note $\| w \|^2 =1$, since $u_k$ has unit length and $P$ is an
orthonormal matrix. Now let's assume the first $k-1$ basis vectors are
the eigenvector of $\Sigma$, it follows that $w_i = 0, i=1,\cdots
k-1$, because $u_k$ has to be orthogonal to the previous basis vectors. Thus,

\begin{equation}
Var(Y_k) = \sum_{i=k}^p w_i^2 \lambda_i
\end{equation}

Since the $\lambda_i$ are in strictly decreasing order, then the
variance is maximized by making $w_k=1$ and $w_j = 0, j \neq k$. And
since $w^T = u_k P$, it follows that $u_k$ has to be $e_k$. 

Making $k=1$ and following the same procedures it is easy to show that $u_1 = e_1$, the first eigenvector, completing the proof.

Note that if some eigenvalues are equal, the solution is not
unique. Also if one eigenvalue is zero, there is an infinite number of
solutions.




\begin{flushright} $\square$ \end{flushright}

\subsection{Decomposition of Variance}

Assuming the inputs have zero mean, then $E\|X\|^2 = \sum_{i=1}^p
Var(X_i)$. Moreover, from the spectral decomposition of the covariance
matrix, we know that $\sum_{i=1}^p Var(X_i) = \sum_{i=1}^p
\lambda_i$. Moreover, 
\begin{equation}
Var(Y_i) = Var( e_i' X) = e_i' \Sigma e_i = e_i' P \Sigma P e_i  = \lambda_i
\end{equation}


Recalling equation \ref{eq:maxvar}, it follows that when our basis  is
the first $k$ eigenvalues of $\Sigma$,

\begin{equation}
E \| X - \hat{X} \|^2  = \sum_{i=1}^p \lambda_i  - \sum_{i=1}^k Var(Y_i)  =
\sum_{i=k}^p \lambda_i
\end{equation}

In other words, the error of the approximation equals the sum of the
eigenvalues associated with eigenvectors not used in our basis set.


\subsection{Remarks}
\begin{itemize}
\item Note that the proof works {\sl regardless of the distribution of
$X$}. An interesting aspect to this is that as long as we are
projecting onto a linear space, optimal solutions can be achieved
using only the mean and covariance matrix of the entire distribution.

\item The results do not depend on the fact that the eigenvectors are
orthogonal. To see why suppose we are considering a vector $\tilde
u_k$ which is not-necessarily orthogonal to $u_1 \cdots u_{k-1}$. Then
we can choose a vector $u_k$ that is orthogonal to $u_1 \cdots u_k$
and that spans the same space as $u_1 \cdots \tilde u_k$. The
reconstructions made from the two spaces would be indistinguishable.

\item The results depend crucially on the fact that the
reconstructions are constrained to be a linear combination of the
components of $Y$. To illustrate this fact consider the example in
Figure~\ref{fig:example1}. There is a 2 dimensional distribution with
equal probability mass at 4 points A, B, C and D. The first
eigenvector of this distribution is the vertical axis. Note that the
projections of points A and B on the vertical axis are
indistinguishable. However the projections of the 4 points on the
horizontal axis are distinguishable. A non-linear decoder would be
able to perfectly reconstruct those points from the projections on the
horizontal axis not from the vertical axis.


\end{itemize}
\begin{center}
\begin{figure}[ht]
\centering
\includegraphics[height=3.5in]{Media/example1.pdf}
\caption{Consider a distribution of 4 equally probably points A, B, C and D. The first principal component in this distribution is the vertical axis. The projections of points A and B on this axis are indistinguishable. However the projections on the horizontal axis are distinguishable. Thus a non-linear decoder would be able to perfectly reconstruct the points using the horizontal projections but not the vertical projections.  }
\label{fig:example1}
\end{figure}
\end{center}
\subsection{Definition of Principal Components}

The random variable $Y_i$ is called the $i^th$ principal component of
the random vector $X$ iff,

\begin{enumerate}

\item $Y_k = u'_k X$ where $u_k \in R^p$ and

\begin{equation}
u_k = \argmax_u Var(l' X)
\end{equation}

\item $Y_i$ is uncorrelated with the previous components $Y_1 \cdots Y_{k-1}$.

\item $u_k$ is orthogonal to the previous basis vectors; $u'_k u_i = \delta_{i,k}, i \leq k$

\end{enumerate}

\noindent From the previous sections it follows that if the eigenvalues of
$\Sigma$ are different from each other and different from zero, the
solution is unique, and $Y_i = e_i X, i=1 \cdots p$. Otherwise there is an infinite number of solutions. Moreover, the principal components are associated with the mean square error solution only if the mean of $X$ is the zero vector.

\subsection{Remark}
Note that 
\begin{equation}
Cov(e_i X, l X) = e_i' \Sigma l = e_i' P \Lambda P' l = \lambda_i e_i' l
\end{equation}
Assuming the first principal component is $Y_i = e_i' X$, and
$\lambda_2 \neq 0$ it follows that making $l$ orthogonal to $e_1$ is
equivalent to making $Y_2$ uncorrelated with $Y_1$. However, if
$\lambda_2 = 0$, then all solutions, orthogonal and non-orthogonal
are uncorrelated.

\subsection{Interpretation}

Principal component analysis models $X$ as a linear combination of uncorrelated
hidden sources, which are called the principal components.

If our goal is to decompose $X$ into its  underlying hidden sources, we can do so using the following equation:

\begin{equation}
Y_i = e_i X
\end{equation}
From this point of view, each component of the $i^{th}$ eigenvector
tells us how much each of the random variables in $X$ should be
weighted to recover the $i^{th}$ ``hidden'' source.

If our goal is to synthesize $X$ when given a set of underlying
sources, we can do so by using the following equation

\begin{equation}
X \approx \hat{X} = \sum_{i=1}^k Y_i e_i
\end{equation}
From this point of view, the $j^{th}$ component of the $i^{th}$
eigenvector tells us the weight of the $i^{th}$ hidden source onto the
$j^{th}$ component of X.



\subsection{Bayesian Interpretation}

Section under construction. It will have to do with optimal
distribution of X ...


\section{Implementation issues}
Let $X$ be our matrix of data (images in our particular case): 
\( X \in {\R}^{m \times N} \) 
where $m$ is the number of images and $N$ is the total number of pixels. 
In our case then $m \ll N$.\\
The covariance matrix $C_{x}$ is defined as \[ C_{x}=\frac{X'X}{m-1} \]
The matrix of eigenvectors $P$ is such that $C_{x}=P D P'$ , where $D$ is 
diagonal 
and $P$ is orthogonal (since $C_{x}$ is symmetrical and positive definite).\\
Now define \[ A=\frac{X'}{\sqrt{m-1}} \] therefore $C_{x}=AA'$.

$A$ can be decomposed using singular value decomposition (SVD) in $A=LMO'$
where $L$ and $O$ are orthogonal matrices and $M$ is diagonal. 
\( L \in {\R}^{N \times N} \), \( M \in {\R}^{N \times m} \), 
\( O \in {\R}^{m \times m} \).
We can then rewrite $C_{x}$ as:
\[ C_{x}=AA'=L M O' O M' L' \] 
since $O$ is orthogonal $\Rightarrow O'O=I \Rightarrow C_{x}=L M M' L'$.\\
Comparing this equation with 
$C_{x}=P D P' \Rightarrow L \equiv P,\; M M' \equiv D$.

\subsection{Pentland's shortcut (Turk \& Pentland, 1991)}
Consider $T=\frac{XX'}{m-1}=A'A$: \(T \in {\R}^{m \times m} \). 
$T=O M' L' L M O'= O M' M O' \Rightarrow O$ is the matrix of eigenvectors of $T$.

By definition of eigenvectors: if $v_{i}$ is a generic eigenvector 
corresponding to an
eigenvalue $\lambda_{i}$: 
\[ T v_{i}=\lambda_{i} v_{i} \Rightarrow  A'A v_{i}=\lambda_{i} v_{i}  \Rightarrow  A A'A v_{i}=A \lambda_{i} v_{i} \Rightarrow  C_{x} Av_{i}=\lambda_{i} Av_{i} \] 
(the conclusion follows from $C_{x}=A A'$ and the fact that 
$\lambda_{i}$ is a scalar) $\Rightarrow Av_{i}$ represent the 
eigenvectors of the original covariance matrix $C_{x}$. 
Since $O$ was the matrix of eigenvectors of $T$ then $A O \equiv P$.

In this case we are interested only in the matrix $O$ of the singular value
decomposition, then we can use the ``economy'' version of the SVD in 
{\em MATLAB} which 
computes only the first $m$ columns of $L$ and therefore the first $m$ rows 
of $M$ giving $L$
of size $N \times m, M$ of size $m \times m$ (the only portion of the original 
$M$ of size $N \times m$ that was not identically zero) and $O$ exactly as 
before.

Combining the use of SVD with {\em Pentland's shortcut} we avoid 
the multiplication 
needed to obtain $C_{x}$ and the computation of all the columns of $L$ 
and $M$ we are not interested in.


\section{History}
\begin{itemize}
\item The first version of this document was written by Javier R. Movellan
in 1997 and used in one of the courses he taught at the Cognitive Science Department at UCSD. 

\item The document was made open source under the GNU Free
Documentation License Version 1.2 on October 9  2003, as part of the
Kolmogorov project.


\end{itemize}



\end{document}





