\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}
\usepackage{bbm}





\newcommand{\note}[1]{\textcolor{red}{\it Javier:  #1}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argsup{\operatornamewithlimits{argsup}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\mathbbm{E}}
\newcommand{\tr}{\text{trace}}
\newcommand{\Var}{\text{Var}}
\newcommand{\Cov}{\text{Cov}}
\newcommand{\Ent}{\mathbbm{H}}
\newcommand{\Pm}{\mathbbm{P}}
\newcommand{\Qm}{\mathbbm{Q}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\R}{\mathbbm{R}}

\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{lem}{Lemma}[section]
\theoremstyle{definition}
\newtheorem{rem}{Remark}[section]
\theoremstyle{definition}
\newtheorem{ex}{Example}[section]



 \title{Linear Systems}
\author{ Javier R. Movellan}


\begin{document}



{Please cite as
J. R. Movellan (2011) Linear Systems.  MPLab Tutorials. UCSD. 
}


\maketitle

\begin{center} 
 Copyright \copyright{}  2011, Javier R. Movellan 
\end{center} 
 






\newpage    
\section{The Laplace Transform}


\subsection{The Bilateral Laplace Transform}
Given a function $f$ and a complex number $s= (s_r , s_i)$ the Laplace transform of
$f$ evaluated at the complex number $s$ takes the following form
\begin{equation}
F(s) = \mathcal{L}[f](s) \bydef  \int_{-\infty}^{\infty} f(t) e^{-st} dt 
\end{equation}
where 
\begin{equation}
e^{-st}  = e^{-s_r t} \Big( \cos(-s_i t )  + j \sin(-s_i t) \Big)
\end{equation}
{\bf Example:}\\
Let 
\begin{equation}
u(t) = \begin{cases}
0, &\text{for $t<0$}\\
1, &\text{else}
\end{cases}
\end{equation}
then
\begin{align}
U(s) &= \int_0^\infty u(t) e^{-st} dt = \int_0^\infty e^{-st} dt = \left[\frac{-1}{s} e^{-st}\right]_0^{\infty} \\
&=\frac{1}{s}, \;\text{for $Re(s) > 0$}
\end{align}
The subset of complex numbers $s$ for which the Laplace transform
integral exists is called the ``Region of Convergence'' or ROC. In the
previous example, the region of convergence is the set of complex
numbers with positive real part.

\subsection{The Unilateral Laplace Transform}

The unilateral transform of $f(t)$ is the bilateral transform of
$f(t)u(t)$.  The unilateral transform is useful for analysis of linear
systems in which the initial conditions are important. 
The bilateral transform is useful for analysis of a system in which
the effect of the initial conditions is negligible, i.e., we can think
of the input signal as having operated forever in the past. The main
difference between the two transforms is in the differentiation
property: An initial conditions terms appears in the unilateral
transform but not in the bilateral transform.
\subsubsection{Properties of the Unilateral Laplace Transform}
\begin{enumerate}
\item{\bf Linearity:} 
\begin{equation}
\mathcal{L}[ a_1 f_1(t) +a_2f_2(t)](s) = a_1 F_1(s) + a_2 F_2(s)
\end{equation}

\item {\bf  Differentiation:}
Let 
\begin{equation}
h(t) = \frac{df(t)}{dt}
\end{equation}
Then 
\begin{equation}
H(s) = s F(s) - f(0)
\end{equation}
{\em Proof:} 
Using integration by parts we have
\begin{equation}
\int_0^{\infty} f'(t) e^{-st} dt = \left[ f(t) e^{-st}
\right]_0^{\infty} + s \int_0^\infty f(t) e^{-st}dt \end{equation}
Note the dependency of the unilateral Laplace transform on initial
conditions.  In the bilateral transform for signals with no beginning and end this dependency disappears. 
\mynote{Show why}


\item{\bf Multiple Differentiation}
Using the differentiation rule repeatedly we get that if
\begin{equation}
 h(t) =\frac{d^nf(t)}{dt^n} 
\end{equation}
then  
\begin{equation}
H(s) = s^n F(s) -  \sum_{k=0}^n s^k    \frac{d^k f(0)}{dt^k}
\end{equation}
\item {\bf Integration:}
Let
\begin{equation}
h(t) =\int_0^t f(u) du 
\end{equation}
Then
\begin{equation}
H(s) = \frac{1}{s} F(s)
\end{equation}
{\em Proof:} 
\begin{equation}
f(t) = \frac{dh(t)}{dt}
\end{equation}
Thus
\begin{equation}
F(t) = s H(s) - F(0) = s H(s)
\end{equation}
\item {\bf Complex Translation:}
Let
\begin{equation}
H(s) = F(s+a) 
\end{equation}
Then
\begin{equation}
h(t) = e^{-at} f(t)
\end{equation}
{\em Proof:} 
\begin{equation}
H(s) = \int_0^{\infty} f(t) e^{-(a+s)t} dt =  \mathcal{L}[e^{-at} f(t)](s)\end{equation}
with the region of convergence for $h$ switched accordingly.
\item {\bf Complex Differentiation:}
Let
\begin{equation}
H(s) = \frac{d F(s)}{ds}
\end{equation}
Then
\begin{equation}
h(t) = - t f(t)
\end{equation}
{\em Proof:} 
\begin{equation}
\frac{d F(s) }{ds}
=\int_0^{\infty} f(t) \frac{d}{ds} e^{-st} dt
= -\int_0^{-\infty} t f(t) e^{-st} dt
\end{equation}
with the region of convergence for $h$ switched accordingly.
 \item {\bf Time Scaling:}
Let $a >0  $ and
\begin{equation}
h(t) = f(at)
\end{equation}
Then
\begin{equation}
H(s) = \frac{1}{a} F(s/a)
\end{equation}

{\em Proof:}
Using change of variables $u = a t$  we have 
\begin{equation}
H(s) = \int_0^{\infty} h(t) e^{-st}dt =
\int_0^{\infty} f(a t) e^{-st}dt =
\frac{1}{a} \int_0^\infty f(a) e^{-\frac{s}{a} u } du
\end{equation}
with the region of convergence for $h$ scaled accordingly.
\item {\bf Convolution:}
Let $h$ be the convolution between $f$ and $g$ 
\begin{align}
h(t) = \int_0^t f(t) g(t-\tau) d\tau = \int_0^t f(t-\tau) g(\tau) d\tau
\end{align}
then
\begin{align}
H(s) = F(s) G(s)
\end{align}
{\em Proof:}
\begin{align}
H(s) &= \int_{t=0}^\infty e^{-st} h(t) dt = \int_{t=0}^\infty e^{-st} \int_{\tau=0}^t f(\tau) g(t-\tau) d\tau dt\nonumber\\
&= \int_{t=0}^\infty \int_{\tau=0}^t  e^{-st} (\tau) g(t-\tau) d\tau dt\nonumber\\
&= \int_{\tau=0}^\infty \int_{t=\tau}^\infty  e^{-st}  f(\tau) g(t-\tau) dt d\tau\nonumber\\
\end{align}
Let $\bar t = t-\tau$, $d\bar t = dt$. Then
\begin{align}
H(s) &= \int_{\tau=0}^\infty \int_{\bar t=0}^\infty  e^{-s(\bar t+\tau)}  f(\tau) g(\bar t) d\bar t d\tau\nonumber\\
&= \int_{\tau=0}^\infty e^{-s \bar t} g(\bar t) d\bar t \int_{\bar t=0}^\infty e^{-s \tau} f(\tau) d\tau = G(s) F(s) = F(s) G(s)
\end{align}
\end{enumerate}

\mynote{We have been ignoring the effect of the transform on ROC. These effects should be considered part of the transformation also}

\begin{table}
\begin{center}
\begin{tabular}{|c|c|c|}\hline
$f(t)$ &$F(s)$ &ROC\\\hline
&&\\
$\delta(t)$      &$1$              &$\mathcal{C}$\\&& \\\hline
&&\\
$u(t)$           &$\frac{1}{s}$    &$ \Re(s) >0$\\&&\\\hline
&&\\
$u(t) t$           &$\frac{1}{s^2}$    &$ \Re(s) >0$\\&&\\\hline
&&\\
$\frac{t^n}{n!} e^{-at}u(t)$    &$\frac{1}{(s+a)^{n+1}}$  &$\Re(s+a) > 0$\\&&\\ \hline
&&\\
$u(t) \text{cos}( \omega t)$ &$\frac{s}{s^2+\omega}$   &$ \Re(s) >0$\\&&\\ \hline
&&\\
$u(t) \text{sin} (\omega t)$ &$\frac{\omega}{s^2+\omega}$ &$\Re(s) >0$\\&&\\ \hline
&&\\
$u(t) e^{-at} \text{cos} (\omega t)$ &$\frac{s+a}{(s+a)^2+\omega^2}$ &$\Re(s) >0$\\&&\\ \hline
$u(t) e^{-at} \text{sin} (\omega t)$ &$\frac{\omega}{(s+a)^2+\omega^2}$ &$\Re(s) >0$\\&&\\ \hline
\end{tabular}
\caption{Some Useful Laplace Transforms}
\end{center}
\end{table}


\subsection{Transfer Function, Impulse Response, Frequency Response}
Consider a system with input $u(t)$ and output $x(t)
$ Let the tranfer function $H(s)$ be defined as follows
\begin{align}
X(s) = H(s) U(s)
\end{align}
Therefore
\begin{align}
x(t) = \int_0^t u(t) h( t-\tau) d\tau
\end{align}
i.e, the state of the system is the convolution between the driving signal and the inverse Laplace transform of the transfer function. 
\paragraph{Impulse Response Function}
Let the driving function be an impulse, i.e.  $u(t)= \delta(t)$. In this case $U(s) =1$ and 
\begin{align}
&X(s) = H(s) \\
&x(t) = h(t)
\end{align}
Thus $h(t)$ is called the impulse response function. It is the inverse Laplace transform of the Transfer Function. 
\paragraph{Steady State Response. Transient Response}
The response of the system to a driving signal $u(t)$ is the convolution between the impulse response and the signal, i.e., 
\begin{align}
x(t) &= \int_0^t h(\tau) u(t-\tau)  d\tau\nonumber\\
&= \int_0^\infty h(\tau) u (t-\tau)  d\tau 
-\int_t^\infty h(\tau) u(t-\tau) d\tau 
\end{align}
The first term 
\begin{align}
\hat x(t) = \int_0^\infty h(\tau) u(t-\tau)  d\tau 
\end{align}
is called the steady state response. If the system is stable the
second term decays to zero and is called the transient response. 

Regarding the steady state response, changing variables $\bar \tau =
t-\tau$, $d\bar \tau = - d \tau$. We get
\begin{align}
\hat x(t) &= \int_{t}^{-\infty} h(t-\bar \tau) u(\tau) (-d\bar \tau)\nonumber\\
&= \int_{-\infty }^{t} h(t-\bar \tau) u(\tau) d\bar \tau
\end{align}
Thus the steady state response at time $t$ can be seen as the response of a system that started at time $-\infty$. 
\paragraph{Frequency Response Function}
 Consider the steady state response to a complex exponential $u(t) = a e^{st}$
\begin{align}
\hat x(t) & =a \int_0^\infty h(\tau) e^{s(t-\tau)} d\tau  = 
a e^{ st} \int_0^\infty h(\tau) e^{-s \tau} d\tau  =   H(s) u(t)
\end{align}
Thus the output is equal to the driving signal times a constant $H(s)$. A case of interest is when the driving signal is a pure sinusoid, i.e., 
\begin{align}
u(t) = \cos(\omega t) = \frac{1}{2}( e^{j \omega t} - e^{-j\omega t})
\end{align}
Thus in this case the steady state response is as follows
\begin{align}
\hat x(t) &= \frac{1}{2} (H(j\omega) - H(-j\omega)) \cos(\omega t) \nonumber\\
&= H(j \omega) \cos(\omega t) 
\end{align}
where we use the fact that $H(-s) = - H(s)$. The function $F(\omega) =
H(j\omega)$ is called the Frequency Response Function. Let $m, \phi$
represent the magnitude and phase of $F( \omega)$. Note
\begin{align}
\hat x(t) &= m e^{j \phi} \frac{1}{2}( e^{j \omega t} - e^{-j\omega t})
= m \frac{1}{2}( e^{j (\omega t +\phi)} - e^{-j(\omega t +\phi)})\nonumber\\
&m \cos(\omega t + \phi) 
\end{align}
 Thus the magnitude of $F(\omega)$ tells us how the system amplifies
 the sinusoid input $\cos(\omega t)$. The phase of $F(\omega)$ tells
 us the phase shift introduced by the system.

\section{Scalar First Order Systems}
\begin{align}
a \dot x_t + b x_t =u_t
\end{align}
Where $u_t$ is called the driving or forcing signal.
\begin{align}
&\tau \dot x_t + x_t = \frac{1}{a} u_t \\
&  \tau = \frac{a}{b}
\end{align}
where $\tau$ is called the time constant. We will analyze the behavior of the unforced system, i.e., $u_t=0$. Taking the Laplace transform
\begin{align}
\tau( s X(s) - x_0) + X(s) = 0
\end{align}
Thus
\begin{align}
X(s) = \frac{\tau x_0}{\tau s +1} = \frac{x_0}{s+ \frac{1}{\tau}}
\end{align}
Taking the inverse Laplace 
\begin{align}
x_t = x_0 e^{ -t /\tau}
\end{align}
\paragraph{Time Constant}
Thus first order systems are characterized by the parameter $\tau$ named the time constant. Note if $\tau>0$ then $x_t$ decays exponentially. By time $t$ $x_t$ is
$e^{-t\tau}$ the initial value of $x$. Thus, since $\log(1) = 0.37$ by
time $\tau$ it is $37 \%$ of the original value. Since $\log(2) =
0.693$ then by time $0.693 \tau$ it has decayed to $50\%$ of the orignal value. Since $\log(10) = 2.3$ then by time $2.3\tau$ it has decayed to 1/10 of the original value. By time $4.6 \tau$ it is 1/100 of the orginal value. Bu time $6.9\tau$ is 1/1000 of the original value ...
\section{Scalar Second Order Systems}
Let
\begin{align}
a \ddot x_t + b \dot x_t + c x_t = u_t
\end{align}
In mechanical systems $a$ represents the mass, $b$ a viscous friction, $c$ a spring force and $u$ an external force.  
where $u_t$ is the driving signal. We will assume $a>0$, otherwise multiply the equation times $-1$.  As in the linear case it is useful to reparameterize the system as follows
\begin{align}
&\ddot x_t + 2 \xi \omega \dot x_t + \omega^2 x_t =   u_t /a\\
&\omega = \sqrt{c/a},\;\text{The undamped natural frequency}\\
&\xi = \frac{ b}{d},\; \text{The viscous damping ratio} \\
&d = 2 \sqrt{ca},\;\text{The critical damping value}\\
&\tau = \frac{1}{\xi \omega},\;\text{The time constant}
\end{align}
To simplify the notation here after we let $u_t$ represent the original $u_t/a$. We will study the behavior of the unforced system, i.e., $u_t=0$. Taking the Laplace transform
\begin{align}
s^2 X(s) - s x_0 - \dot x_0 + 2\xi \omega ( s X(s) - x_0) +  \omega^2 X(s) =0
\end{align}
Thus
\begin{align}
&X(s) = \frac{ as + b}{s^2 + 2\xi\omega s + \omega^2}\\
&a = x_0\\
&b = \dot x_0 + 2\xi \omega x_0
\end{align}
The denominator of the Laplace transform $X(s)$ is called the characteristic polynomial. The roots of the polynomial have the following form
\begin{align}
\lambda= \frac{-2\xi \omega\pm \sqrt{ 4 \xi^2 \omega^2 - 4 \omega^2}}{2}
\end{align}
Equivalently
\begin{align}
\lambda=  \omega( -\xi \pm \sqrt{\xi^2-1})
\end{align}
Depending on the nature of the roots of this polynomial, there can be three types of solutions.
\paragraph{Overdamped systems: $\xi>1$. No oscilations}
In this case the two roots of the polynomial are real and distinct, i.e.
\begin{align}
&\lambda_1 =  \omega(  \sqrt{\xi^2-1} -\xi)\\
&\lambda_2 = - \omega(  \sqrt{\xi^2-1} +\xi)
\end{align}
Thus 
\begin{align}
s^2 + 2\xi \omega s + \omega^2 =  (s - \lambda_1) (s- \lambda_2)
\end{align}
\begin{align}
X(s) = \frac{a s+b}{(s - \lambda_1) (s -\lambda_2) } = \frac{c_1}{s-\lambda_1} + \frac{c_2}{s-\lambda_2}
\end{align}
where
\begin{align}
&c_1( s - \lambda_2) + c_2( s-\lambda_1) = as +b \\
&(c_1+ c_2) s - c_1 \lambda_2 - c_2 \lambda_1 = as +b\\
&c_1 + c_2 = a\\
&-c_1\lambda_2 - c_2\lambda_1 =b \\
&c_1 = -\frac{a \lambda_1 +b}{\lambda_2 - \lambda_1}\\
&c_2 = \frac{a \lambda_2 +b}{\lambda_2 - \lambda_1}\\
\end{align}
Taking the inverse Laplace transform
\begin{align}
x_t = c_1 e^{\lambda_1 t} + c_2 e^{\lambda_2 t}
\end{align}
Thus the solution is a sum of exponentials. Note if $\lambda_1, \lambda_2 <0$ then the system goes to zero with time constants $\frac{1}{\lambda_1}$ and $\frac{1}{\lambda_2}$. Let $\tau_1 = 1/\lambda_1$ and $\tau_2 = 1/\lambda_2$ be the time scales of the exponentials. Note
\begin{align}
&\frac{1}{\tau_1}\frac{1}{\tau_2} = \lambda_1 \lambda_2 = \omega^2\\
&\frac{1}{\tau_1} +\frac{1}{\tau_2} =  \lambda_1 + \lambda_2 = -2 \omega \xi
\end{align}
Thus  from the two time constants we can easily recover $\omega$ and $\xi$\footnote{Note if we need for $\omega \xi >0$ we can always choose $\omega<0$.}
\paragraph{Critically damped systems. $\xi=1$. No oscillations. Fastest to damp. }  In this case the two roots are real and equal, i.e., $\xi =1$. Thus
\begin{align}
&\lambda = - \omega \xi \\
&s^2 + 2\xi \omega s + \omega^2 = (s -\lambda)^2\\
& X(s) = \frac{a s+b}{(s-\lambda)^2} = \frac{c_1}{s-\lambda} + \frac{c_2}{(s-\lambda)^2} 
\end{align}
where
\begin{align}
&c_1(s-\lambda) + c_2 = as+b\\
&c_1 = a\\
&c_2 = b+  a \lambda 
\end{align}
Taking the inverse Laplace
\begin{align}
x_t = c_1 e^{\lambda t} + c_2 t e^{\lambda t}
\end{align}
\paragraph{Underdamped systems. $o<\xi<1$, Oscillatory}. 
In this case the root are complex, i.e., $\xi <1$, and
\begin{align}
\lambda_1 = -\omega\xi + j \omega \sqrt{1 - \xi^2}\\
\lambda_2 = -\omega \xi - j \omega \sqrt{ 1 - \xi^2}
\end{align}
We note that the two roots are complex conjugates, i.e.,
\begin{align}
&\lambda_1 =\lambda= \lambda_r + j \lambda_i\\
&\lambda_2  =\bar \lambda= \lambda_r - j \lambda_i
\end{align}
Thus,
\begin{align}
&X(s) = \frac{c_1}{s-\lambda_1} + \frac{c_2}{s-\lambda_2}\\ 
&c_1 = - \frac{a \lambda_1 + b}{\lambda_2-\lambda_1}\\
&c_2 = -\frac{a \lambda_2 + b}{\lambda_2 - \lambda_1}
\end{align}
We note $c_1, c_2$ are also complex conjugates, i.e.,
\begin{align}
&c_1 = c= c_r + j c_i\\
&c_2 = \bar c= c_r - j c_i
\end{align}
Thus the solution is of the form
\begin{align}
x_t &= c_1 e^{\lambda_1 t} + c_2 e^{\lambda_2 t} = c e^{\lambda t} + \bar ce^{\bar \lambda t}\nonumber\\
&= |c| e^{j \phi} e^{\lambda t} + |c| e^{-j \phi} e^{\bar \lambda t}\nonumber\\
&=|c| e^{\lambda_r t} ( \cos( \lambda_i t +\phi) ) + j \sin( \lambda_i t+ \phi))\nonumber\\
&\quad\quad+|c| e^{\lambda_r t} ( \cos( \lambda_i t +\phi) )  -j \sin( \lambda_i t+ \phi))\nonumber\\
&= |c| e^{\lambda_r t} \cos(\lambda_i t +\phi)
\end{align}
where $|c|,\phi$ are the magnitude and phase of the complex number $c$. 

The frequency of oscillation is 
\begin{align}
  \lambda_i = \omega \sqrt{ 1 - \xi^2} \;\;\text{radians/sec}
\end{align}
Is called the {\em damped natural frequency}, and 
$\omega$ is the undamped ($\xi=0$) natural frequency 
\paragraph{Frequency Response Function}
If we have a driving signal $u_t$, i.e.
\begin{align}
\ddot x_t + 2\xi \omega \dot x_t + \omega^2 x_t =  u_t
\end{align}
Taking the bilateral Laplace Transform we get
\begin{align}
s^2 X(s) +2 \xi \omega s X(s) + \omega^2 X(s) =U(s)
\end{align}
whose transfer function is 
\begin{align}
H(s) = \frac{X(s)}{U(s)} = \frac{1}{s^2 + 2 \xi \omega s + \omega^2}
\end{align}
Note $H(j \alpha)$ is the steady state response of the system to an
input function of the form $u(t) = \cos (\alpha t)$. To find the peak response frequency we need to find the value of $\alpha$ that maximizes the magnitude of $H(j\alpha)$. Note
\begin{align}
 H(j\alpha) = \frac{1}{-\alpha^2 + j 2 \alpha \xi \omega + \omega^2}
\end{align}
and
\begin{align}
|H(j\alpha)| =\frac{1}{\sqrt{ (\omega^2 - \alpha^2)^2 + 4 \alpha^2 \xi^2\omega^2}} 
\end{align}
To find the angular frequency that maximizes the response we take the gradient of the denominator with respect to $\alpha$ and set it to zero, i.e,
\begin{align}
&2(\omega^2 - \alpha^2) 2\alpha + 8 \alpha \xi^2 \omega^2=0\\
& -\omega^2 + \alpha^2 + 2 \xi^2 \omega^2
\end{align}
Thus the peak (resonant) frequency is 
\begin{align}
\hat \alpha = \omega\sqrt{1 - 2 \xi^2}
\end{align}

\paragraph{Step Response Function}
We will study the response to a step function scaled times
$\omega^2$. Assuming zero initial condition and considering that the
Laplace transform of the scaled step function is $U(s)= \omega^2/s$, we get the
following step response
\begin{align}
X(s) = \frac{\omega^2}{s(s^2+ 2\xi \omega s + \omega^2)}= \frac{\omega^2}{s( (s+ \xi \omega)^2 + \omega^2(1- \xi^2))} 
\end{align}
To facilitate obtaining the Laplace transform we express $H(s)$ as follows
\begin{align}
X(s) &= \frac{a}{s} + \frac{bs + c}{(s+\xi\omega)^2 + \omega^2(1 - \xi^2)}\nonumber\\
&= \frac{ a(s + \xi \omega)^2 + a \omega^2 (1-\xi^2) + bs^2 + cs}{s(s^2+ 2\xi \omega s + \omega^2)}\nonumber\\
&= \frac{ s^2( a+b) + s( 2 a\xi \omega +c) + a\omega^2}{s(s^2+ 2\xi \omega s + \omega^2)}\nonumber\\
\end{align}
Thus
\begin{align}
&a+b =0\\
&2a \xi \omega +c =0\\
&a\omega^2 = \omega^2
\end{align}
and
\begin{align}
&a = 1\\
&b=-1\\
&c = - 2 \xi\omega
\end{align}
Thus
\begin{align}
X(s) = \frac{1}{s} -\frac{ s +\xi\omega}{(s+\xi\omega)^2 + \omega^2(1 - \xi^2)}
-\frac{ \xi\omega}{(s+\xi\omega)^2 + \omega^2(1 - \xi^2)}
\end{align}
Using the table of Laplace tranforms we find for $t\geq 0$
\begin{align}
  x_t =& 1 - e^{-\xi\omega t}\Big( \cos(\omega\sqrt{1-\xi^2} t)\nonumber\\
&+ \frac{\xi}{\sqrt{1-\xi^2}} \sin(\omega\sqrt{1-\xi^2} t)\Big) \nonumber\\
&= 1 - \frac{e^{-\xi\omega t}}{\sqrt{1-\xi^2}} \Big(\sqrt{1-\xi^2} \cos(\omega\sqrt{1-\xi^2} t)\nonumber\\
&+ \xi \sin(\omega\sqrt{1-\xi^2} t)\Big) \nonumber\\
&= 1 - \frac{e^{-\xi\omega t}}{\sqrt{1-\xi^2}} \sin(\omega\sqrt{1-\xi^2} t +\phi)\\
&\phi = \cos^{-1}(\xi)
\end{align}
Thus the response of a system with zero initial conditions to the
input $u_t = k \omega^2$ for $t>0$ is as follows
\begin{align}
x_t = k\Big( 1 - \frac{e^{-\xi\omega t}}{\sqrt{1-\xi^2}} \sin(\omega\sqrt{1-\xi^2} t +\phi)\Big)
\end{align}
where 
\begin{align}
\phi = \cos^{-1} (\xi)
\end{align}
\paragraph{Time to peak overshoot:}
Differentiating the step response and setting  $\dot x_t =0$ we get, after some derivations, that for times $\omega \sqrt{1- \xi^2} t = n \pi$ the velocity is zero. Thus the first peak overshot is for $n=1$ with $t_{po} = \pi/(\omega \sqrt{1- \xi^2})$. It is interesting to note that it is indpependent of the size $k$ of the driving function. 
\paragraph{Value at Peak Overshoot}
Putting $t= t_{po}$  in the step response we get that 
\begin{align}
x_{po} = k (1+ e^{-\pi\xi/\sqrt{1-\xi^2}})
\end{align}
\paragraph{Maximum Velocity}
Differentiating again and setting the second derivative to zero we get the time of maximimum velocity $t_{mv}$
\begin{align}
t_{mv} = \omega \sqrt{1 - \xi^2} \tan^{-1} (\frac{1 - \xi^2}{\xi})
\end{align}
It is interesting to note that the time of the peak velocity is independent of the amplitude of the step driving function. 
\begin{itemize}
\item damping ratio: $\xi$, dimensionless
\item undamped natural frequency: $\omega$, rads/sec.
\item damped natural frequency: $\omega \sqrt{1 -\xi^2}$, rads/sec 
\item peak (resonant) frequency: $\omega \sqrt{1 -2 \xi^2}$, rads/sec. 
\item time to peak overshoot: $\pi/(\omega \sqrt{1-\xi^2})$,secs.
\item value at peak overshoot: $k (1+ e^{-\pi \xi/\sqrt{1-\xi^2}})$
\item maximum velocity: $\omega\sqrt{1-\xi^2} \tan^{-1}(\frac{1-\xi^2}{\xi^2})$. 
\end{itemize}



\section{Multidimensional Systems }
\paragraph{Constant Coefficients}
Let $x_t \in \Re^n$ be defined by the following ode
\begin{align}
&\frac{dx_t}{dt} = a x_t + u 
\end{align}
 The solution takes the following form:
\begin{equation}
x_t = e^{at}x_0 +  a^{-1} (e^{at} -I) u
\end{equation}
To see why note 
\begin{align}
\frac{dx_t}{dt} = a e^{at} x_0 + e^{at} u 
\end{align}
and 
\begin{align}
ax_t + u = a e^{at} x_0 + e^{at} u -u + u = d{x_t}{dt} 
\end{align}
\paragraph{Example:}
Let  $x_t$ be a scalar such that
\begin{equation}
\frac{dx_t}{dt} = \alpha \left(u- x_t\right)
\end{equation}
Thus
\begin{align}
 x_t &= e^{-\alpha t} x_0 -\frac{1}{\alpha} (e^{-\alpha t} -1) \alpha u\nonumber\\
&= e^{-\alpha t} x_0 + ( 1 - e^{-\alpha_t})u  
\end{align}
\paragraph{Time variant coefficients}
Let $x_t \in \Re^n$ be defined by the following ode
\begin{align}
&\frac{dx_t}{dt} = a_t x_t + u_t \\
&x_o = \xi
\end{align}
where $u_t$ is known as the driving, or input, signal. The solution
takes the following form:

\begin{equation}
x_t = \Phi_t \left( x_0 + \int_0^t \Phi^{-1}_s u_s ds\right)
\end{equation}
where $\Phi_t$ is an $n \times n$ matrix, known as the fundamental
solution, defined by the following ODE
\begin{align}
&\frac{ d \Phi_t}{dt} = a_t \Phi_t\\
&\Phi_0 = I_n
\end{align}

\paragraph{Example}
Let $x_t \in \Re$ 
\begin{align}
\frac{dx_t}{dt} =  \alpha(u - x_t) 
\end{align}
Thus
\begin{align}
  x_t &= \Phi_t(x_0 + \int_0^t \Phi_s^{-1} \alpha  u_s ds)\\
  \frac{d\Phi_t}{dt} &= -\alpha \Phi_t\\
  \Phi_0 &=1
\end{align}
Thus
\begin{align}
\Phi_t &= e^{-\alpha t}\\
x_t &= e^{-\alpha t}( x_0 - u\int_0^t \alpha e^{\alpha s} ds )\nonumber\\
&= e^{-\alpha t} \Big(x_0  - \Big[u e^\alpha s\Big]_0^t \Big)\nonumber\\
&= e^{-\alpha t} x_0 + (1 - e^{-\alpha t} ) u
\end{align}

\section{System Coupling }
Talk about connecting multiple systems in series and the result on the
transfer function. Talk about feedback systems.

\end{document}
