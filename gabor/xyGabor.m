% Simulate biologically inspired 2D spatial Gabor. Images produced by this
% program are designed to cover about 4 degrees of visual angle.  A degree
% of visual angle is approximately the width of a thumb when viewed with the
% arm extended.

% The produced images are the real and imaginary part of a Gabor filter
% impulse response (i.e., space domain). 


% Copyright @ Javier R. Movellan, UCSD, 2008

%%%%%%%%%%%%%%%%%%%%% Filter Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear

omega0 = 45; % Peak spatial frequency orientation ( in degrees). It
             % corresponds to the angle of a vector perpedicular to the
             % front wave of the sinusooid carrier.  For example for
             % vertical bars omega0 is zero degrees. For horizontal bars it
             % is 90 degrees


f0 = 10; % Peak spatial frequency magnitude in cycles per degree of visual
         % angle. Biologically plausible values range from 0.5 to 20 cycles
         % per degree of visual angle. Most frequent values are about 5
         % cycles per degree.



deltaOmega = 20 ; % Half magnitude orientation bandwidth (in degrees). In
                  % macaque median is about 40 degrees Distribution goes
                  % from 8 degrees (very narrow tuning) to 180 degrees (no
                  % orientation tuning).

deltaF = 1.4; % Half magnitude spatial frequency bandwidth in Octaves. In
            % macaque median is 1.4. Most neurons are between 1 and 1.5
            % octaves.


twist =0; % Misalignment (in degrees) between the orientation of the peak
          % spatial frequency and the orientation of the Gaussian
          % envelope. In macaque Typical value is 0. Average absolute
          % deviation from zero is around 10 degrees.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



omega0 = omega0*2*pi/360;


theta = omega0+ twist; % Orientation of smaller axis of Gaussian half
                       % magnitude ellipsoid in space domain (larger axis
                       % in freq domain). Measured in degrees with
                       % positive. For example for a ellipsois with
                       % smaller axis parallel to vertical axis, theta =
                       % 90. 

% Transform the peak spatial frequency from polar to cartesian coordinates
f(1,1) = f0; % in cycles per degree of visual angle
f(2,1) = f(1)*tan(omega0);
f = f0*f/norm(f);


Ka = (2^deltaF-1)/(2^deltaF+1);
deltaOmega = deltaOmega*2*pi/360;
Kb = tan(0.5*deltaOmega);

lambda = Ka/Kb; % Aspect ratio of Gaussian half magnitude ellipsoid. 

a= Ka*f0*2; % length of smaller axis of Gaussian half magnitude ellipsoid in
            % spatial domain % (largest in freq domain).
b= a/lambda; % length of larger axis of Gaussian half magnitude ellipsoid in
             % spatial domain


r = [cos(theta)  sin(theta);    -sin(theta) cos(theta)]; % rotation matrix
s = diag([a b]); % scale matrix



dx1 = 1/50; % the sampling period in degrees of visual angle. Largest target
           % spatial frequency is 25 cycles per degree. So a sampling period
           % of 1/50 samples per degree is sufficient

dx2 = dx1; % sampling period for vertical axis

x(1,:) = [ -1:dx1: 1];
x(2,:) = [ 1: -dx2 : -1];
nx1 = size(x,2);
nx2 = nx1;

frot = (inv(s*r))'*f; 
c = exp( -pi*frot'*frot); % Bias term for the sinusoid carrier. It is used
                          % to eliminate the DC component of the Gabor
                          % Filter

% very inneficient, but easy to understand, way to get the 2D image of
% the Gaussian kernel and sinusoid carrier. 


for j1 = 1: nx1
  for i1 = 1: nx2
    xtemp = [x(1,j1) x(2,i1)]';
    h(i1,j1) = exp(j* 2*pi*(xtemp'*f))-c; % Sinusoid carrier
    xrot = s*r*xtemp;
    w(i1,j1) = exp(-pi*xrot'*xrot);  % Gaussian envelope
  end
end

g = h.*w; % Gabor filter is product of Gaussian Carrier and sinusoidal envelope







% Plot the impulse response of the 'real' filter
subplot(1,5,1)
imshow(real(g),[])


% Plot the impulse response of the 'imaginary' filter
subplot(1,5,2)
imshow(imag(g),[])



% Get the magnitude spectrum of the impulse respose.
% Zero spatial frequency set at the center of the image
subplot(1,5,3)
ghat = fft2(g);
imshow(abs(fftshift(ghat)),[])



% Construct and display a test image
subplot(1,5,4)
test = zeros(101,101);
test(30:32,40:75) = 1;
test(60:80,60:62) = 1;
imshow(test,[])



% Filter the test image and display it
subplot(1,5,5)
testFilteredHat = fft2(test).*ghat;
testFiltered = ifftshift(ifft2(testFilteredHat));
imshow(abs(testFiltered),[])

