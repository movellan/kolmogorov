% The signal is a a combintaiton of a 2 Hz and 15  Hz simusoid
% The noise is at 30 plus minus 15 Hz 
% We compare how canonical correlation and regression do to reconstruct
% the original signal
%
%Results: 
% Canonical correlation lowpasses the signal so as to completely get rid
% of noise. 
% Regression on the other hand focuses more on the high frequency
% components and as a consequence it gets more noise

clear
clf
randn('state',0); % Set state so we get same result every time we run program
rand('state',0);
dt= 1/100;; % sampling period in seconds.
NyquistFreq = 0.5/dt;


% First we will create bandpass noise
f0= 40; % peak freq
a = 10; % half magnitude bandwidth
[g,t]  = createGaborKernel(dt,f0,a);
t2= -5:dt:5;


noise= randn(size(t2)); % Noise for channel 1
noise = real(doFilter(noise,g, ceil(length(g)/2)));

noise2= randn(size(t2)); % Noise for channel 2
noise2= real(doFilter(noise2,g, ceil(length(g)/2)));

% Now we create the signal. It is the sum of two sinusoids
% of frequencies 2 and 30 Hz
fSignal = 2; 
fSignal2 = f0;
signal = 0.5*sin(2*pi*fSignal*t2)+ 0.5*sin(2*pi*fSignal2*t2);
signal= signal.*(hanning(length(t2))'); 

y = signal+20*noise; % Obsserved channel one is signal plus noise
y= y.*(hanning(length(t2))'); 

y2 = signal+ 20*noise2; % Observed channel is signal plus noise
y2= y2.*(hanning(length(t2))'); 


% we now get a bunch of samples to train  the canonical correlation and regression
nSamples=100000;
nmin=300; % We pick our samples from the center of the signal, where the
          % effect of the hanning window is lesss pronouce
nmax =600;

nT = length(g); % number of time steps used for the regression and
                % canonical correlation
x= zeros(nSamples,nT);
x2= zeros(nSamples,nT);
train = zeros(nSamples,1);

for t=1:nSamples % we randomly grab windows of the two signals and put
                 % them into the rows of x, x2, and y2
  r= ceil((nmax-nmin)*rand);
  x(t,:) = y(r:r+nT-1); 
  x2(t,:) = y2(r:r+nT-1);
  train(t) = signal(r+ceil(nT/2)); % the training signal for regression
end

% do regression
b = regress(train,x);
  
% reconstruct the original signal using the filter given by regression
signalHatReg  = real(doFilter(y, b,ceil(length(b)/2)));


% do canonical correlation
[c d R] = canoncorr(x, x2);

% reconstruct a filtered version of the signal using the filter given by
% canonnical correlation
signalHatCan  = real(doFilter(y, c(:,1),ceil(length(b)/2)));


subplot(4,1,1)
[X F] = getSpectrum(noise,dt)
plot(F,abs(X),'r')
hold on
[X F] = getSpectrum(signal,dt)
plot(F,abs(X))
xlabel('Frequency (Hz)')
ylabel('Magnitude Spectrum')
title('Magnitude Spectrum of Signal and Noise');
legend('Noise','Signal');

subplot(4,1,2)
[XReg Freg] = getSpectrum(signalHatReg,dt);
[XCan FCan] = getSpectrum(signalHatCan,dt);
plot(Freg, abs(XReg)/sum(abs(XReg)))
hold on
plot(Freg, abs(XCan)/sum(abs(XCan)),'r--')
xlabel('Frequency (Hz)')
ylabel('Magnitude Spectrum')
title('Magnitude Spectrum of  Reconstructed Signals');
legend('Regression', 'Canonical');



subplot(4,1,3)
tmp = signalHatReg(nmin:nmax);
tmp = (tmp-mean(tmp))/std(tmp);
plot(tmp);
hold on
tmp = signalHatCan(nmin:nmax);
tmp = (tmp-mean(tmp))/std(tmp);
plot(tmp,'r--')
xlabel('Time (seconds)')
title('Reconstructed Signals');
legend('Regression', 'Canonical');


subplot(4,1,4)
tmp = signal(nmin:nmax);
tmp = (tmp-mean(tmp))/std(tmp);
plot(tmp,'r')
xlabel('Time (seconds)')
title('Original Signal');


