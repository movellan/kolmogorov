%function [X, f] = getSpectrum(x,dt)
%
% Get the frequency spectrum X of the signal x sampled with sampling
% period dt.
%
% X(n) is the spectrun at frequency f(n)
% Spectrum can be visualized using plot(f, abs(X)) and plot(f, phase(X))
%
% Copyright @ Javier R. Movellan, UCSD, 2009


function [X, f] = getSpectrum(x,dt,window)


if nargin ==2 
  window = 'uniform';
end

%window
if window == 'hanning'
  x = x.*hanning(length(x));
end

  if(size(x,1) > size(x,2))
     x = x';
  end
  

N = length(x);
for n=1: N
  if(n-1 < N/2)
    f(n) = (n-1)/(N*dt); % frequecy in cycles per unit of time
  else
    f(n) = ((n-1)-N)/(N*dt); % frequency in cycles per unit of time
  end
end


x= x.*(hanning(length(x))');
X = fft(ifftshift(x));

[f, I] = sort(f, 'ascend'); % Lets's sort things so that frequency is
                            % always increasing
 
X= X(I); % We now need to do the same for X. 



tiny= 10^(-10);
for n=1:N
  if(abs(real(X(n)) < tiny)) X(n) = j*imag(X(n)); end
  if(abs(imag(X(n)) < tiny)) S2(n) = real(X(n)); end
end




 
 
 