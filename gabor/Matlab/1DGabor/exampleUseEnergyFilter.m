% Copyright @ Javier R. Movellan, UCSD, 2009
% Example of how to use a Gabor Energy Filter 
% A 2 Hz sinusoid appear in the middle of a signal 
% We create a Gabor energy filter that detects the pressence of the sinusoid




f0= 2; % peak freq
a = 1; % half magnitude bandwidth
dt= 1/100;; % sampling period in seconds.


% first we get a sinusoid that lasts for 6 seconds 
t= 0:dt:10;
y= sin(2*pi*f0*t);
y(1:300) = 0;
y(end-300:end) =0;

% Now we create a Gabor Energy filter

[g,t2]  = createGaborKernel(dt,f0,a);


% convolve the signal with the complex Gabor filter
yFilter = doFilter(y,g, ceil(length(g)/2));


subplot(4,1,1)
% the original signal
plot(t,y)
subplot(4,1,2)
% The real part of the complex gabor filter output
plot(t,real(yFilter))
subplot(4,1,3)
% The imaginary part of the complex gabor filter output
plot(t,imag(yFilter))
subplot(4,1,4)

% The magnitude of the Gabor filter (the energy) indicates the pressence
% of the target sinusoid
plot(t,abs(yFilter))
