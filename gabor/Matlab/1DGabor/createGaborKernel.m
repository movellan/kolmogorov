% Create Gabor Kernel in the temporal doamin
%
% function k = createGaborKernel(dt, f0, a)
% dt: sampling period 
% f0: Peak Frequency ( in cycles per time unit.  Time unit same as used
% for dt. 
%  Half Magnitude Bandwidth: In cycles per time unit
%
% k: Kernel (y axis)
% t: time   (x axis)
%
%
% Example: Create Gabor filter with peak freq 100 Hz, and bandwidth 1
% Hz, for signals sampled at 1000 samples per second.  
%
% [k t] = createGaborKernel(1/1000,100,10)
%
% Copyright @ Javier R. Movellan, UCSD, 2009

function [k t] = createGaborKernel(dt,f0,a)
ainv = 1/a;
t = (-1.5*ainv:dt:1.5*ainv);
w = exp(-pi*(a*t).^2);
s = exp(j*2*pi*f0*t);
k = s.*w;
sum(abs(k))
k = k/sum(abs(k));  %normalize to add up to 1. 