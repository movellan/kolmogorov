% Copyright @ Javier R. Movellan, UCSD, 2009
% Example of how to use a Gabor Fitler 
% Well make a Gabor filter with 
% Peak Frequency 10 Hz.
% Bandwidth 5 Hz
% We use it to filter a signal with frequency components at 10 and 15 Hz

clear
clf


f0= 10; % peak freq
a = 5; % half magnitude bandwidth
dt= 1/100;; % sampling period in seconds.


[g,t]  = createGaborKernel(dt,f0,a);
[G f] = getSpectrum(g,dt);


 subplot(2,1,1)
 plot(t,real(g))
 

 line([0 0], [0 max(g)])
 subplot(2,1,2)
 plot(f,abs(G))
 line([f0 f0], [0 max(abs(G))])
 axis([min(f) max(f) -0.05 max(abs(G))])

t2= -5:dt:5;
y= sin(2*pi*f0*t2)+sin(2*pi*(f0+3*a)*t2);;



[Y f2]  = getSpectrum(y,dt);

figure

 subplot(2,1,1)
 plot(t2,y)
 subplot(2,1,2)
 plot(f2,abs(Y))

figure
 
yFilter = real(doFilter(y,g, ceil(length(g)/2)));
[ YFilter f3] = getSpectrum(yFilter,dt);



 plot(t2,yFilter)

 plot(f3,abs(YFilter))


