% Illustrates how to use convolve()
% Copyright Javier R. Movellan,2009, UCSD

s =1:10 ; % The signal
k=1:3; % The kernel
c = 3; % This is a causal filter

doFilter(s,k,c)

