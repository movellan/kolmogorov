% Filters a signal using the conv function
%
% s : The signal
% k : The filter kernel (time flipped impulse response
% c:  Integer with index for origin  of he kernel, points to 
%     the right of the origin are non-causal.
% z:  The output of the filter to each point of s. The outputs for the
%     left and right limits of s are obtained by zero padding s. 
%
%
% Example1; Filter a signal s using a causal ramp filter with 4 time steps
% z = doFilter(s, 1:4, 4)
%
% Example2; Filter a signal s using a ramp filter that looks 3 steps
% into the future
% z = doFilter(s, 1:4, 1)
%
% Copyright Javier R. Movellan,2009, UCSD
function z = doFilter(s,k,c)
  

z=conv(s,fliplr(k)); % note we need to convolve with the impulse response,
                     % which is the time flipped version of the kernel


%Matlab automatially pads s with zeros to the left and right. 
% We now get rid of the response of the filter at times other than the
% times of the original data

z(1:(length(k)-c))=[];
z(end-(c-2):end)=[];

