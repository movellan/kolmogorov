% Simulate biologically plausible spatio-temporal Gabor filter (2 spatial
% dimensions and 1 temporal dimensions. 
%
% Copyright @ Javier R. Movellan, UCSD, 2008
% Berkeley open source style license and distribution
%
% We will simulate 1 second of a 1x1 degree of visual angle retina. To
% view the resulting movie, have it span size of human thumb with arm
% extended. 

clear

%%%%%%%%%%%%%%%%%%%%% Filter Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


preferredSpatialFrequencyMagnitude = 6; % In cycles per degree of visual
                                     % angle. In adult macaque V1, bulk
                                     % values are between 0.5 and 15 cycles
                                     % per degree. Mean value in the fovea
                                     % is 4.25 cycles per degree. Mean value
                                     % in the parafovea is 2.7 cycles per
                                     % degree.


preferredOrientation = 0; % In Degres. The sinusoid carrier's wavefront is
                           % perpendicular to the orientation. 90 degrees
                           % orientation produces horizontal wavefronts. 0
                           % degrees produces vertical wavefronts.

preferredWaveFrontVelocity =1; % In degrees of visual angle per second. In
                           % adult cat V1, bulk of neurons have preferred
                           % velocities from 0 to 15 degrees per second. The
                           % mode is 3 degrees per second. We measure
                           % velocity in the direction of the spatial
                           % frequency vector.



bandwidthSpatialFrequencyMagnitude = 1.4; % In Octaves. Half magnitude
                                          % bandwidth for the spatial
                                          % frequency magnitude. In adult
                                          % macaque V1 the median is 1.4
                                          % octaves. Most neurons have
                                          % bandwidths between 1 and 1.5
                                          % octaves.

orientationBandwidth = 45; % in Degrees. In macaque V1 orientation
                           % bandwidth ranges from 8 degrees to no
                           % orientation selectivity (bandwidth close to
                           % 90 degreees). The mean bandwidth is 65
                           % degrees, median 42 degrees, mode 30
                           % degrees. 

timeFrequencyBandwidth = 2; % In Hz. In adult cats the half-magnitude high
                            % frequency cut-off points range from 4 to 12
                            % Hz, with an average of 7.2 Hz. Time domain
                            % analysis suggests that in adult cat bandiwdths
                            % range between 2.5 and 25 Hz are
                            % reasonable. Note 2.5 Hz gives a temporal
                            % window size of about 1/2.5 = 400 millisecs


% in cat area 21a the halft height bandwidth of neurons classified as
% bandpass ranged from 0.8 to 2.9
% octaves with a mean 0f 1.92 octaves. 

speedTuned =0; % if 1 the prefered temporal frequency is inversely
               % proportional to the magnitude of the temporal
               % frequency. If 0 the preferred temporal frequency does
               % not change with the spatial frequency (temporal
               % frequency tuned). Simple cells in V1 are frequency
               % tuned, not speed tuned. About 1/4 of complex cells in V1
               % are speed tuned
dcCompensated=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%

xRange= 1; % range of retina simulated in degrees of visual angle.
yRange = 1; % range of retina simulated in degrees of visual angle.
tRange = 500/1000; % In seconds. In adult cat V1, most neurons have temporal
              % kernels that span less than 400 millisecons




framesPerSecond = 30; 


maxSpatialFreqMagnitude = 15; % In cycles per degree of visual
                              % angle. Determines the spatial sampling
                              % rate. For adult macaque V1 macaque most neurons
                              % have preferred values between 0.5 and 15
                              % cycles per degree. And bandwidths about
                              % as large as the preferred frequencies. Thus
                              % 15+15 = 30 cycles per degree is a
                              % reasonable maximum value.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dx = 1/(10*maxSpatialFreqMagnitude); % 10 times larger than Nyquist sampling
                                    % period for x dimension.
dy = 1/(10*maxSpatialFreqMagnitude); % 10 times larger than Nyquist sampling
                                    % period for y
                                    % dimension. 

dt = 1/framesPerSecond; 

x = [ -xRange/2:dx: xRange/2];
y = [ yRange/2: -dy : -yRange/2];
t= [[-tRange/2:dt:0-dt] [0:dt:tRange/2]]; 

[a1, a2, a3] = meshgrid(x,y,t); % horizontal axis is matrix columns
q = [a1(:), a2(:), a3(:)]; % contains all combinations of x, y and t
                           % values. 



omega0 = preferredOrientation;
omega0 = omega0*2*pi/360;
dOmega = orientationBandwidth*2*pi/360;
F0 = preferredSpatialFrequencyMagnitude;


uo=cos(omega0)*F0;
vo=sin(omega0)*F0;
wo= -preferredWaveFrontVelocity*F0;
fo = [uo vo wo]';


r_uu = [cos(omega0), sin(omega0); -sin(omega0), cos(omega0)];


dF = bandwidthSpatialFrequencyMagnitude;

a = F0*(2^dF -1)/(2^dF+1)*sqrt(pi/log(2)); 
b = F0*tan(dOmega/2)*sqrt(pi/log(2));

d_uu = diag([ 1/a 1/b])'; % rememember we are building the freq domain k matrix

k_uu = r_uu'*d_uu'*d_uu*r_uu;



k_uw =  -k_uu * [uo vo]'/wo*speedTuned;
k_ww =  4*log(2)/pi/(timeFrequencyBandwidth^2)  + speedTuned*[uo vo]*k_uu*[uo vo]'/(wo^2)';


% k matrix in the frequency domain
khat = [ k_uu k_uw ; k_uw' k_ww];

% k matrix in the space/time domain
k = inv(khat);

%%%%%%%%%% tmp experiment erase 
k(1,3) = -37.63;
k(2,3) =0;
k(3,1) = -37.63;
k(3,2) =0;
k(3,3) =50;
%%%%%%%%%%%%%%%%%%%%%%%%


qk = q*k; 

md = sum(qk.*q,2); 
w = exp(-pi*md); % gaussian envelope


% the sinusoid carrier
s= exp(j*2*pi*q*fo); %
% Here we compute the constant to get rid of the DC component at the peak
C = exp(-pi*fo'*(inv(k)')*fo);
s = s - dcCompensated*repmat(C,size(s));

g = w.*s;



g = reshape(g, size(y,2), size(x,2),size(t,2));
w = reshape(w, size(y,2), size(x,2),size(t,2));
s = reshape(s, size(y,2), size(x,2),size(t,2));



for t1=1:size(t,2)
  t1  
  imshow(real(g(:,:,t1)),[-1 1]);
  M(t1) = getframe;
end
framesPerSecond = 1/dt;
%movie(M,10,framesPerSecond);
movie(M,10,floor(framesPerSecond));

ghat = fftn(g);
save gaborHat ghat