%
load gaborHat

 for t1=1:size(ghat,3)
   m(t1) = max(max(abs(ghat(:,:,t1))));
 end
[m am] = max(m);

% draw half magnitude countour
x = fftshift(abs(ghat(:,:,am)));
x(x< m/2) =0;

 imshow(x);

