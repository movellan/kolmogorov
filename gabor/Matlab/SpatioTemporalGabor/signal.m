% Simulate biologically plausible spatio-temporal Gabor filter (2 spatial
% dimensions and 1 temporal dimensions. 
%
% Copyright @ Javier R. Movellan, UCSD, 2008
% Berkeley open source style license and distribution
%
% We will simulate 1 second of a 1x1 degree of visual angle retina. To
% view the resulting movie, have it span size of human thumb with arm
% extended. 

clear

%%%%%%%%%%%%%%%%%%%%% Filter Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


preferredSpatialFrequencyMagnitude = 3; % In cycles per degree of visual
                                     % angle. In adult macaque V1, bulk
                                     % values are between 0.5 and 15 cycles
                                     % per degree. Mean value in the fovea
                                     % is 4.25 cycles per degree. Mean value
                                     % in the parafovea is 2.7 cycles per
                                     % degree.


preferredOrientation = 0; % In Degres. The sinusoid carrier's wavefront is
                           % perpendicular to the orientation. 90 degrees
                           % orientation produces horizontal wavefronts. 0
                           % degrees produces vertical wavefronts.

preferredWaveFrontVelocity = 2; % In degrees of visual angle per second. In
                           % adult cat V1, bulk of neurons have preferred
                           % velocities from 0 to 15 degrees per second. The
                           % mode is 3 degrees per second. We measure
                           % velocity in the direction of the spatial
                           % frequency vector.







%%%%%%%%%%%%%%%%%%%%%%%%%%%
spatialScale = 1;
temporalScale= 2;


xRange= 1; % range of retina simulated in degrees of visual angle.
yRange = 1; % range of retina simulated in degrees of visual angle.
tRange = 500/1000; % In seconds. In adult cat V1, most neurons have temporal
              % kernels that span less than 400 millisecons




framesPerSecond = 30; 


maxSpatialFreqMagnitude = 15; % In cycles per degree of visual
                              % angle. Determines the spatial sampling
                              % rate. For adult macaque V1 macaque most neurons
                              % have preferred values between 0.5 and 15
                              % cycles per degree. And bandwidths about
                              % as large as the preferred frequencies. Thus
                              % 15+15 = 30 cycles per degree is a
                              % reasonable maximum value.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dx = 1/(10*maxSpatialFreqMagnitude); % 10 times larger than Nyquist sampling
                                    % period for x dimension.
dy = 1/(10*maxSpatialFreqMagnitude); % 10 times larger than Nyquist sampling
                                    % period for y
                                    % dimension. 

dt = 1/framesPerSecond; 

x = [ -xRange/2:dx: xRange/2];
y = [ yRange/2: -dy : -yRange/2];
t= [[-tRange/2:dt:0-dt] [0:dt:tRange/2]]; 

[a1, a2, a3] = meshgrid(x,y,t); % horizontal axis is matrix columns
q = [a1(:), a2(:), a3(:)]; % contains all combinations of x, y and t
                           % values. 



omega0 = preferredOrientation;
omega0 = omega0*2*pi/360;
F0 = preferredSpatialFrequencyMagnitude;


uo=cos(omega0)*F0;
vo=sin(omega0)*F0;
wo= -preferredWaveFrontVelocity*F0;
fo = [uo vo wo]';


r_uu = [cos(omega0), sin(omega0); -sin(omega0), cos(omega0)];




a = 1/spatialScale;
b = 1/spatialScale;
c = temporalScale;


k = diag(2*[a b c]);


qk = q*k; 

md = sum(qk.*qk,2); 
w = exp(-pi*md); % gaussian envelope



% the sinusoid carrier
s= exp(j*2*pi*q*fo); %
% Here we compute the constant to get rid of the DC component at the peak


% multiply times a gaussian window to avoid border effects
s = w.*s;




s = reshape(s, size(y,2), size(x,2),size(t,2));

  for t1=1:size(t,2)
    t1  
    imshow(real(s(:,:,t1)),[-1 1]);
    M(t1) = getframe;
  end
  framesPerSecond = 1/dt;
  movie(M,10,framesPerSecond);
  movie(M);



shat = fftn(s);
save signalHat shat