\documentclass{article}
\usepackage{nips00e}
\usepackage[pdftex]{graphicx}
\usepackage{kolmogorovmath}
\usepackage[round]{natbib}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}


\usepackage{graphicx}

 \title{Velocity Tuned Filters}
\author{ Javier R. Movellan}











\begin{document}
\maketitle
For simplicity we will study the case with one  spatial dimension and one temporal dimension. The formula for the Gabor function in space-time is as follows
\begin{align}
&g(u) = b s(u) w(u)\\
&s(u) = e^{-j 2 \pi \omega' u}\\
&w(u) = e^{-\pi u' a u}
\end{align}
where $u =(x,t)'$, and $\omega = (\omega_x,\omega_t)'$ is the peak
spatio-temporal frequency of the filter. The Fourier transform of $g$
is as follows
\begin{align}
\hat g(f) = b\sqrt{|c|}  e^{-\pi ( f - \omega)' c (f-\omega)}
\end{align}
where $f= (f_x,f_t)'$ and $c = a^{-1}$. 

\subsection{Derivation with $f_x$ given $f_t$}
Using the spatio-temporal rotation lemma we note
\begin{align}
& ( f - \omega)' c (f-\omega) = (f_x - \hat \omega_x)' c_{xx} (f_x - \hat \omega_x) + (c_{tt} + c_{tx} \kappa )(f_t - \omega_t)^2\\
&\hat \omega_x = \omega_x + \kappa  (f_t - \omega_t)\\
& \kappa = - c_{xx}^{-1} c_{xt}
\end{align}
Note for a given sinusoid carrier with temporal frequency $f_t$ the peak response is obtained for a sinusoid with the following   spatial frequency
\begin{align}
\hat \omega_x = \omega_x + \kappa (f_t - \omega_t) 
\end{align}
The wave front velocity of a sinusoid with spatial frequency $f_x$ and temporal frequency $f_t$ is $- f_t/f_x$. If we want a
filter that responds maximally to a carrier with wave front velocity
$\nu$ regardless of the temporal frequency then we need the filter to respond maximaly to spatial frequency $f_x = - f_t/\nu$. Thus we need
\begin{align}
\hat \omega_x = -\frac{f_t}{\nu}
\end{align}
This can be accomplished by setting 
\begin{align}
&\omega_t = - \nu \omega_x\\
&\kappa = -\frac{1}{\nu}
\end{align}
Thus, we need 
\begin{align}
&\kappa = - c_{xx}^{-1} c_{xt} = - \frac{1}{\nu}
\end{align}
i.e.,
\begin{align}
&c_{xt}= \frac{c_{xx}}{\nu}
\end{align}
Note
\begin{align}
\label{c2a}
a = c^{-1} = |a| \left(\begin{array}{cc}c_{tt} &-c_{xt}\\-c_{tx} &c_{xx}\end{array}  \right)
\end{align}
Thus
\begin{align}
&c_{xt}= \frac{c_{xx}}{\nu}
\end{align}
implies 
\begin{align}
&a_{xt}= - \frac{a_{tt}}{\nu} 
\end{align}
Using the spatio-temporal rotation Lemma in the spatio-temporal domain we get that
\begin{align}
 u' a u = (x - \hat x)' a_{xx} (x - \hat x) + (a_{tt} + a_{tx} \alpha) t^2  
\end{align}
where 
\begin{align}
&\hat x = \alpha t\\
&\alpha = - a_{xx}^{-1} a_{xt} = c_{tt}^{-1} c_{xt} = \frac{c_{tt}^{-1} c_{xx} }{\nu} \label{alpha}
\end{align}

Note $\hat x$ is the spatial peak of the Gaussian function. Thus
$w(u)$ is a Gaussian function whose spatial peak starts at zero at
time $0$ and moves with velocity $\alpha$. If for any reason we want $\alpha=\nu$ then we need to set $c_{tt}^{-1} c_{xx} = \nu^2$.
\paragraph{Construction:}
From the required spatial frequency bandwidth set $a_{xx}$ from the
required temporal frequency bandwidth set $a_{tt}$. From the required
velocity $\nu$ set $a_{xt}$.

\subsection{Derivation with $f_t$ given $f_x$}
Apply the spatio-temporal rotation lemma in the other direction by expanding the $f_t$ term first, we obtain
\begin{align}
& ( f - \omega)' c (f-\omega) = (f_t - \hat \omega_t)' c_{tt} (f_t - \hat \omega_t) + (c_{xx} + c_{xt} \xi)(f_x - \omega_x)^2\\
&\hat \omega_t = \omega_t + \xi (f_x - \omega_x)\\
& \xi= - c_{tt}^{-1} c_{tx}
\end{align}
The conditional spectrum given $f_x=w_x$ is a 1D Gaussian function of $f_t$ with maximum 
$f_t=\hat\omega_t = \omega_t + \xi (f_x-w_x)$. Instead of tuning at certain
temporal and spatial frequency, the maximum response temporal frequency of 
a velocity tuned filter change with spatial frequency with the following relationship,
$f_t = -\nu f_x$, where $\nu$ is a constant velocity of the sinusoidal carrier.
To satisfy the relationship, we need to set  $\omega_t, \omega_x, \xi$ such that $\hat\omega_t = -\nu f_t$,
which lead to following constraints,
\begin{align}
\begin{cases}
	\xi = -\nu \\
	w_t = \xi \omega_x
\end{cases}
\end{align}
For the first constraint, since $\xi = -\nu = - c_{tt}^{-1} c_{tx}$, 
we have $c_{tx} = \nu c_{tt}$.  This implies $a_{xt}= - \nu a_{xx}$ due to \eqref{c2a}.
Plugging the result into \eqref{alpha},
\begin{align}
	&\alpha = - a_{xx}^{-1} a_{xt} = -a_{xx}^{-1} (-\nu a_{xx}) = \nu.
\end{align}
Therefore, the Gaussian envelope moves at velocity $\nu$.

The second constraint sets the peak of the of the $\hat g$ on $(f_x, f_t)$ spectrum,
$\omega_t = \xi \omega_x = -\nu\omega_x$, which determines the carrier direction on $(x,t)$ plane.
The corresponding spatial propagation velocity of wavefront in $x$ is $x = \nu t$.

In summary, we obtain a design of velocity tuned filter whose Gaussian envelope moves at the same
speed of the carrier.

\section{Lemmas}
\subsection{Conditional Gaussian Distribution}
Given a bi-variate Gaussian distribution $X=(X_1, X_2)'$  with mean $E[X]=(\mu_1,\mu_2)'$ and variance 
$\mbox{Var}[X]=\begin{bmatrix} \sigma_{11} & \sigma_{12}\\
		         \sigma_{21} & \sigma_{22}\end{bmatrix}$.
The conditional distributions are
\begin{align}
	E[X_2|x_1] = \mu_2 + \sigma_{12}\sigma_{11}^{-1}(x_1-\mu_1)\\
	E[X_1|x_2] = \mu_1 + \sigma_{21}\sigma_{22}^{-1}(x_2-\mu_2)
\end{align}
Assuming $\mu_1=0, \mu_2=0$,
\begin{align}
	E[X_2|x_1] = \sigma_{12}\sigma_{11}^{-1} x_1\\
	E[X_1|x_2] = \sigma_{21}\sigma_{22}^{-1} x_2
\end{align}
The two lines $x_2=\sigma_{12}\sigma_{11}^{-1} x_1$ and $x_1=\sigma_{12}\sigma_{22}^{-1} x_2$ are
two distinct lines except when $\sigma_{12}\sigma_{11}^{-1}\cdot\sigma_{12}\sigma_{22}^{-1}=1$.

 
\subsection{Spatio-temporal rotations}
{\em 
Let $q = (x', t)'$, where $x = (x_1, x_2)'$ be the space-time
coordinates of a point on the image plane let represent its Let $a$ be
a $3 \times 3$ {\bf symmetric positive definite} matrix
partitioned as follows
\begin{align}
a = \left(
\begin{array}{cc}
a_{xx} &a_{xt}\\
a_{tx} &a_{tt}
\end{array}
\right)
\end{align}
where $a_{xx}$ is a $2 \times 2$ matrix, $a_{xt}$ is $2\times 1$ ,
$a_{tx}$ is $1\times 2$, and $a_{tt}$ is a scalar.  
 Let
\begin{align}
  \rho   = (q - \mu)' a (q -\mu )
\end{align}
where $\mu = (\mu_x' , \mu_t)'$. Then
\begin{align}
  \rho   = (x - \hat x )' a_{xx} (x - \hat x) + (a_{tt} + a_{tx} \nu)\;( t - \mu_t)^2
\end{align}
where
\begin{align}
&\hat x = \mu_x + \nu \; ( t- \mu_t)\\
&\nu = - (a_{xx}^{-1} a_{xt} )
\end{align}
}

\begin{proof}


\begin{align}
\rho = (x - \mu_x)' a_{xx} (x - \mu_x) + 2 (x - \mu_x)'  a_{tx}(t- \mu_t) + (t- \mu_t)^2 a_{tt}
\end{align}
and
\begin{align}
( x - \hat x)' &a_{xx} ( x - \hat x) = (x -\mu_x - \nu (t - \mu_t) )' a_{xx} ( x - \mu_x - \nu (t - \mu_t) )\\
  &= (x - \mu_x)' a_{xx} (x - \mu_x) + \nu' a_{xx} \nu (t - \mu_t)^2 - 2 (x- \mu_x)' a_{xx} \nu  (t - \mu_t)
\end{align}
where
\begin{align}
&\nu' a_{xx} \nu  = - a_{tx}  \nu    
\end{align}
and
\begin{align}
&-2 (x - \mu_x) a_{xx} \nu  = 2 (x -\mu_x)'  a_{xt} 
\end{align}
Thus
\begin{align}
( x - \hat x)' a_{xx} ( x - \hat x) &= (x - \mu_x)' a_{xx} (x - \mu_x)\\
& \quad - a_{tx}\; \nu\; (t - \mu_t)^2 + 2 (x- \mu_x)' a_{xt}  (t - \mu_t)
\end{align}
and

\begin{align}
\rho = ( x - \hat x)' a_{xx} ( x - \hat x) + (a_{tt}  +  a_{tx} \nu )( t - \mu_t)^2
\end{align}

\end{proof}


\end{document}
