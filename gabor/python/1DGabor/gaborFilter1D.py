


from numpy import *
from scipy.signal import *
from math import e



def gaborFilter1D(y,f0,a,dt,padMode):
	[g,t]=create1DGaborKernel(dt,f0,a)
	# find the index to the filter origin (closest to 0)
	c = argsort(abs(t))[0]
	yFilter = filter1D(y,g,c,padMode)
	return yFilter

def create1DGaborKernel(dt,f0,a):
# dt: sampling period in secs
# f0: Peak frequency in Hz
# a: Half magnitude bandwidth in Hz
# Example: create1DGaborKernel(1/1000.,100,10.)
# creates a complex Gabor filter with peak freq 100 Hz and bandwidth 10 Hz.
# sampling period is 1 m=sec
# Javier R. Movellan, Emotient 2015.
	ainv = 1./(a+0.)
	t = arange(-1.5*ainv,1.5*ainv,dt)
	w = e**(-pi*(a*t)**2.0)
	s = e**(2j*pi*f0*t)
	k = s*w
	#print '>>',sum(abs(k))
	k = k/sum(abs(k))
	return [k,t]

def filter1D(x,h,c,padMode):
# filter the sequence x using a filter with inpulse response sequence h. 
# c is the index in r that points to the  origin of the impulse response sequence r. 
# c is the 0-offset index for the origin of h  
#padMode can be: 'constant','edge','linear_ramp','mean','median','minimum','symmetric','wrap','reflect'
# constant padding = zero padding
# 
# output is a sequence y of same len as x.
# to this end we use left right padding of x
# below are some examples using zero padding

# Example: x = [1,1,1,1], h=[1,2,-1], c=0
# xpadded=[0,0,1,1,1,1]
# y=[ 1,3,2,2]
# Example: x = [1,1,1,1], h=[1,2,-1], c=1
# xpadded=[0,1,1,1,1,0]
# y=[ 3,2,2,1]
 #Example: x = [1,1,1,1], h=[1,2,-1], c=2
# xpadded=[1,1,1,1,0,0]
# y=[ 2,2,1,-1]

	M = len(h)
	leftPad= M-1-c
	rightPad =c
	x=pad(x,(leftPad,rightPad),padMode)
	y=fftconvolve(x,h,'valid')
	return y
