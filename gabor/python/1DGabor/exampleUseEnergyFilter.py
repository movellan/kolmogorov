from gaborFilter1D import *
from matplotlib.pyplot import *




f0=2
a =1
dt =1/100.
T=10.
t= arange(0,T,dt)
y =  sin(2*pi*f0*t)

y[1:300]=0
y[-301:-1]=0
padMode='constant'
padMode='edge'
#padMode='wrap'
padMode='reflect'
yFilter=gaborFilter1D(y,f0,a,dt,padMode)


subplot(4,1,1)
plot(t,y)
subplot(4,1,2)
plot(t,real(yFilter))

subplot(4,1,3)
plot(t,imag(yFilter))

subplot(4,1,4)
plot(t,abs(yFilter))

show()