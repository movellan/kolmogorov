\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}

 \title{Calculus of Variations}

\author{ Javier R. Movellan }




\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}





\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}



\newtheorem{thm}{Theorem}
\newtheorem{res}{Result}
\newtheorem{cor}{Corollary}
\newtheorem{lem}{Lemma}
\theoremstyle{definition}
\newtheorem{rem}{Remark}
\theoremstyle{definition}
\newtheorem{ex}{Example}
\theoremstyle{definition}
\newtheorem{defi}[thm]{Definition}




\begin{document}
\maketitle
\Large
\begin{center} 
 Copyright \copyright{}  2003 Javier
R. Movellan. \end{center} 


    
\newpage
Our goal is to find a function $x()$ that minimizes the following function

\begin{equation}
I(x ) = \int_a^b F(t,x(t),  \dot{x}(t)) dt
\end{equation}
where
\begin{equation}
\dot{x}(t) \bydef \frac{d x(t)}{dt}
\end{equation}
We will find a necessary condition for $x$ to be at a minimum. The condition is equivalent to the zero gradient condition for the discrete time case. 

Let $ x$ be a local minimum of $I$. Let's  define a 
family of functions of the following form
\begin{equation}
h(\epsilon,t) =  x(t) + \epsilon \delta(t)
\end{equation}
where $\delta$ is an arbitrary function with continuous second partial derivatives and  such that $\delta(a) = \delta(b)
=0$. Let
\begin{equation}
\tilde I(\epsilon) 
= \int_a^b F(t,h(\epsilon,t), \dot{h}(\epsilon,t)) dt
\end{equation}
A necessary condition  for $ x$ to minimize $I$ is that 
\begin{equation}\label{eqn:1}
\left.\frac{d \tilde I(\epsilon)}{d \epsilon} \right|_{\epsilon=0} = 0
\end{equation}
thus 
\begin{align}
 \int_a^b \Big(\frac{\partial  F(t, h(\epsilon,t), \dot{h}(\epsilon,t)) }{\partial h(\epsilon,t)}\frac{\partial h(\epsilon,t)}{\partial \epsilon} \\\nonumber +
\frac{\partial  F(t, h(\epsilon,t),  \dot{h}(\epsilon,t)) }{\partial  \dot{h}(\epsilon,t)} \frac{\partial  \dot{h}(\epsilon,t)}{\partial  \epsilon}
\Big) dt \Big|_{\epsilon=0} = 0
\end{align}
Note for a fixed function  $\delta$

\begin{align}
\frac{\partial  {h}(\epsilon,t)}{\partial \epsilon} = \delta(t)\\
\frac{\partial  {\dot h}(\epsilon,t)}{\partial \epsilon} = \dot \delta(t)
\end{align}
and for $\epsilon =0$
\begin{equation}
\frac{d  F(t, h(,\epsilon,t),  \dot{h}(\epsilon,t) )}{d  h(\epsilon,t)}
= \frac{d  F(t,  x (t),   \dot{x}(t) )}{d  x (t)}
\end{equation}
\begin{equation}
\frac{d  F(t, h(\epsilon,t),  \dot{h}(\epsilon,t) )}{d  \dot{h}(\epsilon,t)}
= \frac{d  F(t,  x (t),   \dot{x}(t)) }{d  \dot{x} (t)}
\end{equation}
Thus \eqref{eqn:1} can be expressed as follows
\begin{align}\label{eqn:2}
&\int_a^b 
\frac{\partial  F(t,  x (t),   \dot{x}(t)) }{\partial {x} (t)} {\delta}(t) dt\nonumber\\
&\quad+
\int_a^b \frac{\partial  F(t,  x (t),   \dot{x}(t)) }{\partial  \dot{x} (t)} \dot{\delta}(t) dt= 0
\end{align}
Using integration by parts
\begin{align}
\int_a^b& \frac{d  F(t,  x (t),   \dot{x}(t)) }{d  \dot{x} (t)} \dot{\delta}(t) dt=
\left. F(t,  x(t),  \dot{x}(t)) \delta(t)\right|_a^b\\ \nonumber
&\quad- 
\int_a^b \left( \frac{d}{d t} \frac{d  F(t,  x (t),   \dot{x}(t) }{d  \dot{x} (t)}\right) \delta(t) dt \\\nonumber
&=-  \int_a^b \left(\frac{d}{d t} \frac{d  F(t,  x (t),   \dot{x}(t) }{d  \dot{x} (t)}\right) \delta(t) dt 
\end{align}
because $\delta(a) = \delta(b) =0$. Thus \eqref{eqn:2} can be
expressed as follows
\begin{align}
\int_a^b & \Big[ 
\frac{\partial  F(t,  x(t),  \dot{x}(t) )}{\partial  x(t)} 
- \Big( \frac{d}{d t}  \frac{\partial  F(t,  x (t),   \dot{x}(t) )}{\partial  \dot{x} (t)}
\Big)\Big] \delta(t) dt 
\end{align}
The fundamental lemma of calculus of variations states that if
\begin{equation}
\int_a^b m(x) g(x) dx =0
\end{equation}
for all $g$ with continuous second partial derivatives, then 
\begin{equation}
m(x) =0,\;\text{for $x \in (a,b)$}
\end{equation}
Applying the Lemma to our case,   we get that
\begin{equation}
\frac{d  F(t,  x(t),  \dot{x}(t)) }{d  x(t)}
- \frac{d}{d t}  \frac{d  F(t,  x (t),   \dot{x}(t) )}{d  \dot{x} (t)} =0,\quad \text{for $t\in (a,b)$}
\end{equation}
Or in more succinct notation
\begin{align}
\frac{d  F}{d  x}
- \frac{d}{d t} \left( \frac{d  F} {d  \dot{x} }\right) =0,
\end{align}
This is called the Euler-Lagrange differential equation. 
\section{Examples}
\subsection{Shortest path between two points on a plane} 
Let $x$ be a function in the real plane. Points of this function take
the form $t, x(t)$.  We constrain the function to start at the origin
$(0,a)$ and end at $(T,b)$.  We can sample the function at a set of of
points $(x_1, t_1), (x_2, t_2) \cdots$ on the plane. We will use a generalized version of distance
\begin{align}\label{eqn:none}
d(x,y) = \sqrt{(x-y)' a (x-y)} 
\end{align}
where $a$ is a positive definite matrix. Note this definition includes
Euclidean distance as a special case The total length of the resulting
trajectory is
\begin{align}
I(x) &= \sum_i \sqrt{ a_{11} \delta_{t,i}^2+ 2 a_{12} \delta_{t,i} \delta_{x,i}+  a_{22} \delta_{x,i}^2} \\
&= \sum_i 
\sqrt{a_{11}  \delta_{t,i}^2+  + 2 a_{12} \delta_{t,i}^2  \frac{\delta_{x,i}}{\delta_{t,i}}  + (\delta_{x,i} \frac{\delta_{x,i}}{ \delta_{t,i}   }  )^2} \\
&=
\sqrt{   a_{11} + 2a_{12} \frac{\delta_{x,i}}{ \delta_{t,i}}+  a_{22} \left(\frac{\delta_{x,i}}{ \delta_{t,i}}\right)^2} \delta_{t,i}
\end{align}
where $\delta_{x,i} = x_{i+1} - x_i$ and $\delta_{t,i} = t_{i+1} - t_i$. Thus, in the limit
\begin{align}
&I(x) = \int_a^b F(t,x(t), \dot{x}(t) )\;dt\\ \nonumber
&F(t,x(t), \dot{x}(t) ) =  \sqrt{a_{11} + 2a_{12} \dot{x}(t)+ a_{22}\dot{x}^2(t)} 
\end{align}
In this case $F$ does only depend on $\dot{x}$, thus the Euler-Lagrange equation takes the following form
\begin{align}
0&=- \frac{d}{dt} \Big(  \frac{d}{d \dot{x}(t)} \sqrt{a_{11} + 2a_{12} \dot{x}(t) + a_{22} \dot{x}^2(t)} \Big) \\\nonumber
&=
 -\frac{d}{dt} 
\frac{a_{22} \dot{x}(t) + a_{11}}{\sqrt{a_{11} + 2a_{12} \dot{x}(t) +a_{22} \dot{x}^2(t)}}\\ \nonumber
&=-\frac{a_{22}\ddot{x}(t)}{\sqrt{a_{11} +2a_{12} \dot{x}(t) +a_{22} \dot{x}^2(t)} }\\\nonumber
&+ \frac{(a_{22} \dot{x}(t) + a_{11} )( a_{22} \dot{x}(t) \ddot{x}(t)+ 2a_{12} \ddot{x}(t))}{(a_{11} + 2a_{12} \dot{x}(t) +a_{22} \dot{x}^2(t))^{3/2}}
\end{align}
Note that $a_{11} + 2a_{12} \dot{x}(t) + a_{22} \dot{x}^2(t) \geq 0$
because $a$ is positive definite. Thus, the Euler-Lagrange equation is
satisfied when $\ddot{x}(t) =0$, i.e., a straight line between $(0,a)$
and $(T, b)$. It may seem surprising that the line is the shortest
trajectory even for the non-Euclidean case, when $a \neq I$. It makes
more sense after noticing that the triangle inequality holds: Consider the point $p+q$. 
\begin{align}
d^2(0,p+q ) &= (p+q)' a a (p+q)= (pa + qa)' (pa + qa) \nonumber\\
& = p'a'a p + q'a'aq + 2 p'a'aq  \nonumber \\
&\leq   p'a'a p + q'a'aq + 2 d(0, p ) d(0, q) \nonumber\\
& = (d(0,p) + d(0,q) )^2 
\end{align}
where $d(0,v)$ is the distance between the origin and the point $v$. Thus if I want to go from the origin to a point $p+q$ any deviations from the line would result on a longer path, when length is measured using the non-Euclidean norm in \eqref{eqn:none}.
\section{Beltrami Identity}
If $F$ does not depend on $t$, the Euler-Lagrange equation takes the
following form, known as the Beltrami Identity
\begin{align}
F - \dot{x} \frac{\partial F}{\partial \dot{x}} = C
\end{align}
where $C$ is a constant. 
\section{Multivariate Case}
If $x(t) \in \Re^n$ the Euler-Lagrange Equation takes the following form 
\begin{align}
\nabla_{x(t)} F(t, x(t), \dot x(t) ) - \frac{d}{dt} \nabla_{\dot x(t)} F(t, x(t) ,\dot x(t)) = 0 
\end{align}
or more succintly
\begin{align}
\nabla_{x} F  - \frac{d}{dt} \nabla_{\dot x} F = 0 
\end{align}





\section{Appendix}

\begin{lem}{Fundamental Lemma of Calculus of Variations}

If 
\begin{equation}
\int_a^b m(x) g(x) dx =0
\end{equation}
for all $g$ with continuous second partial derivatives, and $g(a) =
g(b) =0$ then
\begin{equation}
m(x) =0,\;\text{for $x \in (a,b)$}
\end{equation}
\end{lem}
\begin{proof}
Let
\begin{align}
g(x) = m(x)(x-a)(b-x)
\end{align}
Note $g$ is continuous and $m(x) g(x) \geq 0$ for $x \in [a,b]$. If the integral of a non-negative function is zero then the function must be zero at all points, i.e., $m(x)g(x) =0$ for $x \in [a,b]$. Thus
\begin{align}
m(x)^2 (x-a) (b-x) =0,\; \text{for $x \in [a,b]$}
\end{align}
and since $(x-a)(b-x) \geq 0$ for $x\in [a,b]$ then $m(x)^2 \geq 0$ for $x \in [a,b]$, thus $m(x) =0$ for $x \in [a,b]$.
\end{proof}
\bibliographystyle{abbrvnat} \bibliography{movellan.stats}


\end{document}
