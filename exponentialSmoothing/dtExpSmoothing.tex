\documentclass{article}
\usepackage{graphicx}
\usepackage{nips00e}
\usepackage{amsgen,amssymb,amsopn,amsmath}
\usepackage[square]{natbib}

\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand\given{\medspace|\medspace}

 \title{Discrete Time Exponential  Smoothing}
\author{ Javier R. Movellan}





\newenvironment{namelist}[1]{%
\begin{list}{}
	{ 		\let\makelabel\namelistlabel
\skettowidth{\labelwidth}{#1}
\setlength{\leftmargin}{1.1\labelwidth} 	} }{%
\end{list}}

\empty

% Replace with the directory where the graphs have been placed
\newcommand{\graphdir}{./graphs}
\newcommand{\centerfig}[2]{\centerline{\psfig{figure=#1,width=#2}}}




\begin{document}
\maketitle

\newpage	
Let $X_1, X_2 \cdots$ be a sequence of random variables. Let $\bar X_n = \sum_{i=1}^n X_i/n$ be the sequence of running means.  We can recursively compute these means as follows
\begin{align}
\bar X_n &= \frac{ (n-1) \bar X_{n-1}+X_n }{n} \\
&= \frac{n-1}{n}   \bar  X_{n-1} + \frac{1}{n}  X_n\\
\end{align}
where
\begin{align}
&\bar X_0 = 0\\
\end{align}
 Thus the mean at time $n$  is a normalized weighted sum: The weight given to the previous mean is $n-1$ and the weight given to the new data is $1$.   We can try and limit the weight of the prior data to a maximum value $m-1$. We denote the resulting values with $\hat X_1, \hat X_2 \cdots$ to distinguish them from the running means

\begin{align}
&\hat X_0 =0\\
&w_0 =0 \\
&\hat  X_n = \frac{X_n+ w_n \hat X_{n-1} }{w_n+1}  \\
& w_n = \min\{n,m-1\}\\
\end{align}
Equivalently
\begin{align}
&\hat X_0 =0\\
&\hat  X_ n= \alpha_n\hat X_{n-1} + (1-\alpha_n) X_n\\
& \alpha_n = \min\{(n-1)/n, (m-1)/m\}
\end{align}
Thus for $n\geq m$
\begin{align}
\hat X_{n} &= \alpha \hat X_{n-1} + (1-\alpha) X_n\\
\alpha &= \frac{m-1}{m}
\end{align}
or
\begin{align}
m = \frac{1}{1-\alpha}
\end{align}
If $X_1,X_2,\cdots$ are iid random variables with mean $\mu$ and variance $\sigma^2$ we have that for $n\geq m$
\begin{align}
E[\hat X_{n}] = \alpha E[\hat X_{n-1}] + (1-\alpha) \mu \\
\end{align}
and since $E[\hat X_1] = E[X_1] = \mu$ then 
\begin{align}
E[\hat X_{n}] = \mu + (1-\alpha) \mu  = \mu\\
\end{align}
Moreover
\begin{align}
Var[\hat X_n ] = \alpha^2 Var[\hat X_{n-1}] + (1-\alpha)^2 \sigma^2
\end{align}
At equilibrium
\begin{align}
Var[\hat X_\infty ] &= \alpha^2 Var[\hat X_{\infty}] + (1-\alpha)^2 \sigma^2\\
Var[\hat X_\infty ]&=\frac{(1-\alpha)^2}{1-\alpha^2} \sigma^2 =\frac{1-\alpha}{1+\alpha} \sigma^2
\end{align}
Since averaging $n$ observations reduces the variance by a factor of $n$ then we can say that the effective number of observations $\eta$ for an smoother with parameter $\alpha$ is
\begin{align}
\eta=  \frac{1+\alpha}{1-\alpha} = (1+\frac{m-1}{m}) m = (2 - \frac{1}{m}) m = 2m -1
\end{align}

\section{Frequency Response}
Applying the Discrete Fourier Transform to the smother update equation:
\begin{align}
&\hat X_n = \alpha \hat X_{n-1} + (1-\alpha) X_n\\
&\hat X[f]  = \alpha e^{-i 2\pi f} \hat X[f] + (1-\alpha) X[f]\\
\end{align} 
where $\hat X[f_n]$, $X[f_n]$ are the Fourier transform of $\hat X$, $X$ evaluated at the frequency $f$, which takes values between 0 and 0.5 cycles per second. After some algebra

\begin{align}
H[f]= \left| \frac{\hat X[f] }{X[f] }\right| &= \left|\frac{1-\alpha}{ 1 - \alpha e^{-i 2\pi f}}\right|\\
&=  \frac{1-\alpha}{\sqrt{1 + \alpha^2 - 2 \alpha \cos(2\pi f)}}
\end{align}
where we used the fact that
\begin{align}
\left|1 - \alpha e^{i 2\pi f}\right|^2  &=
\left|(1 - \alpha ( \cos(-2 \pi f) + i \sin(-2\pi f) \right|^2\\
& = 	(1- \alpha \cos(2\pi f))^2 + \alpha^2 \sin^2( 2\pi f)\\
&=1 -2 \alpha \cos(2\pi f) + \alpha^2 \cos^2(2\pi f) + \alpha^2 \sin^2(2\pi f)\\
&=1 + \alpha^2 - 2 \alpha \cos(2\pi f)
\end{align}

Note
\begin{align}
&H[0]= 1\\
&H[0.5] = \frac{1-\alpha}{1+\alpha}=\frac{1}{\eta}
\end{align}
Figure~\ref{fig:freqResponse} shows the frequency response function for several values of $\alpha$ and their corresponding $\eta$.



 \begin{figure}[bht]\label{fig:freqResponse}
\begin{center}
\includegraphics[width=2in]{figures/freqResponse} \includegraphics[width=2in]{figures/freqResponse2}
\end{center}
  \caption{Frequency response of smoothers for different values of $\alpha$ and corresponding $\eta$. Left: as a function of frequency. Right: as a function of period.}
 \end{figure}

\subsection{Cutoff Frequency}
The frequency for which the response magnitude is $\theta$ or less is given by the following equation
\begin{align}
f =  \frac{1}{2\pi}\arccos{\left(\frac{1+\alpha^2 - (1-\alpha)^2/\theta^2}{2\alpha}\right)} 
\end{align}
Figure~\ref{fig:freqResponse} Right shows the 10 \% cut off period (in samples per cycle)  as a function of the effective memory size $\eta$.  For large $\eta$ the cutoff period is approximately $0.32 \eta$. For example, for $\eta=1000$ periods of 320 samples per cycle or less get attenuated to 10\% or less of the original signal. The left side displays the relatoinship between $\alpha$ and the cutoff frequency. For example,  $\eta = 10^3$, corresponds to $\alpha = 0.998$ and  the 10\% cutoff frequency is $0.03$ cycles per sample. Frequencies higher than 0.03 cycles per second will be attenuated to  less than 10\% of the original amplitude. 


 \begin{figure}[bht]\label{fig:freqResponse}
\begin{center}
\includegraphics[width=2in]{figures/freqResponse3}\includegraphics[width=2in]{figures/freqResponse4}
\end{center}
  \caption{Left: 10\% cutoff period (in samples per cycle) as a function of the effective memory size. Right: cutoff frequency (in cycles per sample) as a function of the   $\alpha$ parameter. }
 \end{figure}


\bibliographystyle{plainnat} % plainnat
\bibliography{expSmooth}

\end{document}
