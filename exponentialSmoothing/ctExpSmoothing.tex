\documentclass{article}
\usepackage{graphicx}
\usepackage{nips00e}
\usepackage{amsgen,amssymb,amsopn,amsmath}
\usepackage[square]{natbib}

\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand\given{\medspace|\medspace}

 \title{Variable and Fixed Interval Exponential Smoothing}
\author{ Javier R. Movellan\\University of California San Diego}





\newenvironment{namelist}[1]{%
\begin{list}{}
	{ 		\let\makelabel\namelistlabel
\skettowidth{\labelwidth}{#1}
\setlength{\leftmargin}{1.1\labelwidth} 	} }{%
\end{list}}

\empty

% Replace with the directory where the graphs have been placed
\newcommand{\graphdir}{./graphs}
\newcommand{\centerfig}[2]{\centerline{\psfig{figure=#1,width=#2}}}




\begin{document}
\maketitle

\newpage	
\section{Introduction}
Exponential smoothers provide an efficient way to compute moving
averages of signals \cite{Tong90,Box2004,Luktepohl2010}. This can be
particularly useful for real time applications. Here we define,
describe and analyze exponential smoothing algorithms, with an eye
towards practical applications.

Let $X_{t_1},X_{t_2},\cdots $ be a sequence of random variables observed at times
$t_1\leq t_2\leq t_3\leq\cdots$. We define the exponentially smoothed average $\hat X_t$
evaluated at time $t$ as follows

\begin{align}
\hat X_t &= \frac{\tilde X_t}{\tilde w_t}\label{eqn:hatx}\\
\tilde X_t &= \sum_{t_k \leq t} e^{-(t-t_k)/\tau} X_{t_k} \\
\tilde w_t &= \sum_{t_k \leq t} e^{-(t-t_k)/\tau} 
\end{align}
where $\tau \geq 0$ is the time constant (aka time scale). 

\subsection{Time Constant}
The time constant $\tau$ is the time needed for the weight of an observation to decay to 36.8 \% of the original value. To see why consider a sample observed at time $t_k = t-\tau$. The weight of this observation would be
\begin{align}
e^{- (t - t_k)/\tau} = e^{-1} = 0.3679
 \end{align}


\section{Least Squares interpretation}

\begin{align}
\rho(m) = \sum_{t_k \leq t} e^{-(t-t_k)/\tau} (X_{t_k} - m)^2
\end{align}
Taking the gradient with respect to $m$,  setting it to zero and solving for $m$ we get the exponetially smothed average 
\section{Recursive Update Equations}
A key advantage of exponential smoothing is that it does not require storage of  past observations in memory. The current smoothed average, the current new observation, the time of the last observation and the time of the current observation determine the new smoothed value.  
Note
\begin{align}
\tilde X_{t_k} = X_{t_k} + e^{-\delta(k,k-1)/\tau} X_{t_{k-1}} +  e^{-\delta(k,k-2)/\tau} X_{t_{k-2}} + \cdots + e^{-\delta(k,1)/\tau} X_{t_{1}}
\end{align}
where $\delta(k,j) = t_k -t_j$. Moreover
\begin{align}
\tilde X_{t_{k-1}} = X_{t_{k-1}} + e^{-\delta(k-1,k-2)/\tau} X_{t_{k-2}} + \cdots+ e^{-\delta(k-1,1)/\tau} X_{t_{1}}
\end{align}
Thus
\begin{align}
&\tilde X_{t_k} = X_{t_k} + \alpha_{t_k} \tilde X_{t_{k-1}}\\
&\alpha_{t_k} = e^{-\delta(k,k-1)/\tau}
\end{align}
with initial condition $\hat X_{t_1} = X_1$. Similarly
\begin{align}
\tilde w_{t_k} = \tilde w_{t_k} + \alpha_{t_k} 
\end{align}
with initial condition $\tilde w_{t_1} = 1$.  If $t$ is not one of the
sampling times, i.e., $t \in (t_{k}, t_{k+1})$ then
\begin{align}
\bar X_t & = e^{-(t-t_k)/\tau} \tilde X_{t_k}\\
\tilde w_t &= e^{-(t-t_k)/\tau} \tilde w_{t_k}\\
\hat X_t &= \frac{\tilde X_t}{\tilde w_t} = \hat X_{t_k}
\end{align}



\section{Constant Sampling Rate}
It is useful to analyze how the update equations behave when the
sampling rate is constant. In this case the time interval between
observations is a constant $\delta$ and thus $\alpha =
e^{-\delta/\tau}$ is also constant.
\paragraph{Equilibrium Update Equations:} Consider update equations of the form
\begin{align}
u_{k+1} = c + \alpha u_k
\end{align}
where $c$ is a constant. At equilibrium 
\begin{align}
&u_k = c + \alpha u_k \\
&u_k = \frac{c}{1-\alpha}
\end{align}
Thus at equilibrium 
\begin{align}
&\tilde w_{t_k} =  1 + \alpha \tilde w_{t_{k-1}}\\
&\tilde w_{t_k}  = \frac{1}{1-\alpha}
\end{align}
and
\begin{align}
  \hat X_{t_k} &= \frac{\tilde X_{t_k}}{\tilde w_{t_k}}= (1-\alpha) \tilde X_{t_k}\nonumber\\
  &= (1-\alpha) \Big(X_{t_k} +\alpha \tilde X_{t_{k-1}}\Big)\nonumber\\
  &= (1-\alpha) X_{t_k} + \alpha  \hat X_{t_{k-1}}\nonumber\\
  &= \hat X_{t_k} + \alpha( \hat X_{t_{k-1}} - X_{t_k})
  \end{align}

If the $X_{t_k}$ variables are independent identically distributed random
variables with mean $\mu$ and variance $\sigma^2$ then 
the equilibrium equations for the expected value are as follow:
\begin{align}
&E[\tilde  X_{t_k}] =  \mu + \alpha E[X_{t_k}] \\
&E[\tilde X_{t_k}] = \frac{\mu}{1-\alpha}\\
&E[\hat X_{t_k}] = \frac{E[\bar X_{t_k}]}{\bar w_{t_k} } = \mu
\end{align}
and the  equilibrium equations for the variance are as follow
\begin{align}
&Var[\tilde  X_{t_k}] =  \sigma^2  + \alpha^2  Var[X_{t_k}] \\
&Var[\tilde  X_{t_k}] =  \frac{\sigma^2}{1 - \alpha^2}   \\
&Var[\hat X_{t_k}] = \frac{(1-\alpha)^2}{1-\alpha^2} \sigma^2 = \frac{1-\alpha}{1+\alpha} \sigma^2
\end{align}


\paragraph{Effective Number of Averaged Observations:}
If  $\hat X$ were the average of $n$ independent observations  the variance of $\hat X$  would be $\sigma^2/n$. Thus
asymptotically, the effective number of observations averaged by the
exponential smoother can be defined as follows

\begin{align}
n = \frac{1+\alpha}{1-\alpha}\\
\alpha = \frac{n-1}{n+1}
\end{align}
Thus, for $\alpha=0$ we average one observation. The number of
averaged observations increases unboundedly as $\alpha$ increases.
\paragraph{Time Constant and Effective Time Window:}
In a time window of size $T$ we get $T/\delta$ observations. Thus
\begin{align}
\alpha = e^{-\delta/\tau} = \frac{T/\delta -1}{T/\delta +1} = \frac{T-\delta}{T+\delta}
\end{align}
Taking logs
\begin{align}
-\frac{\delta}{\tau} = \log(T-\delta) - \log(T+\delta)\\
\frac{1}{\tau} = \log (T+\delta) - \log(T-\delta)\\
\frac{1}{\tau} = \frac{\log (T+\delta) - \log(T-\delta)}{\delta}
\end{align}
Thus, in the limit as $\delta \to 0$
\begin{align}
&\frac{1}{\tau}= 2 \frac{d \log(T)}{dT} = \frac{2}{T}\\
&\tau  =\frac{T}{2}
\end{align}
i.e., the effective window size $T$ is twice the time scale parameter $\tau$. 
Turns out for $\delta <0.1$, the approximation
\begin{align}
\frac{\log (T+\delta) -\log(T-\delta)} {\delta} \approx \frac{2}{T}
\end{align}
is quite good. Under this approximation we get
\begin{align}
&\delta/\tau = \frac{2\delta}{T} = \frac{2}{n}\\
&\alpha = e^{-\delta/\tau}=e^{-2/n}\\
&n = 2 \log(\alpha^{-1} )
\end{align}



\section{Alternative Version of Update Equations:}
The previous update equation can run into numerical problems since
$\tilde w_t, \tilde X_t$ may grow unbounded as $t$ increases. To avoid this problem we may multiply the numerator and denominator of
\eqref{eqn:hatx} by $1-\alpha$, which is acceptable as long as $\alpha
<1$. Regarding the numerator of \eqref{eqn:hatx}, let
\begin{align}
\bar X_{t_k} &= (1- \alpha ) \tilde X_{t_k} =
(1- \alpha )  X_{t_k} \nonumber\\
&+ (1- \alpha ) \alpha \tilde X_{t_{k-1}}\nonumber \\
&=(1- \alpha)  X_{t_k}  +  \alpha  \bar X_{t_{k-1}} \\
&= \bar X_{t_k} + (1-\alpha) (X_{t_k} - \bar X_{t_{k-1}})
\end{align}
Regarding the denominator of \eqref{eqn:hatx} let
\begin{align}
\bar w_{t_k} =& (1- \alpha) \tilde w_{t_k} =
(1- \alpha ) + (1- \alpha  ) \alpha \tilde w_{t_{k-1}}\nonumber \\
=&(1- \alpha )   +  \alpha  \bar w_{t_{k-1}} \nonumber\\
=& \bar w_{t_k} + (1-\alpha) (1 - \bar w_{t_{k-1}})
\end{align}
Then, for $\alpha \neq 1$
\begin{align}
\hat X_{t_k} =\frac{\tilde X_{t_k}}{\tilde w_{t_k}}  =\frac{(1- \alpha ) \tilde  X_t}{(1- \alpha )  \tilde w_t} = \frac{\bar X_{t_k}}{\bar w_{t_k}}
\end{align}
\paragraph{Equilibrium Equations:}
In this case 
\begin{align}
&\bar X_{t_k} =(1-\alpha)  X_{t_k} + \alpha \bar X_{t_{k-1}}= \bar X_{t_k} + (1-\alpha) (X_{t_k} - \bar X_{t_{k-1}}) \\
&\bar w_{t_k} = (1-\alpha) + \alpha \bar w_{t_{k-1}}
= \bar w_{t_k} + (1-\alpha)(1 - \bar w_{t_{k-1}})
\end{align}
The equilibrium equation  for $\bar w$ is as follows
\begin{align}
\bar w_{t_k} = (1-\alpha) + \alpha \bar w_{t_k}\\
\end{align}
Thus at equilibrium
\begin{align}
\bar w_{t_k} =1
\end{align}
This can be also seen using  the sum of geometric series
\begin{equation}
\alpha^0 + \alpha^1 + \alpha^2 + \cdots + \alpha^{n-1} = \frac{1- \alpha^n}{1 - \alpha}
\end{equation}
Thus
\begin{align}
&\bar w_{t_k} = 1 - \alpha^k\\
&\lim_{k\to \infty} \bar w_{t_k}= 1
\end{align}
Thus, at equilibrium, 
\begin{align}
&\hat X_{t_k} = (1- \alpha) X_{t_k} +  \alpha \bar X_{t_{k-1}} =
(1-\alpha) X_{t_k} + \alpha  \hat X_{t_{k-1}}
\end{align}
\section{Continuous Time System Interpretation}
Consider a differential equation of the form

\begin{align}\label{eqn:ode}
\frac{dx_t}{dt} = a (u_t - x_t) 
\end{align}
The solution takes the following form:
\begin{align}
  x_t &= e^{-at} x_0 + e^{-at} \int_0^t a e^{as} u_s ds\nonumber\\
  &= e^{-at} x_0 + \int_0^t a e^{-a(t-s)} u_s ds
\end{align}
Note
\begin{align}
  x_{t+\Delta_ t} &= e^{-a(t+ \Delta_t)} x_0 + e^{-a(t+\Delta_t) } \Big( \int_0^t  a e^{a s} u_s ds +\int_t^{t+\Delta_t} a e^{-as} u_s ds\Big)\nonumber\\
  &= e^{-a \Delta_t} \Big( e^{-a t} x_0 + e^{a t} \int_0^t a e^{-as} u_s ds\Big) +
  e^{-a \Delta_t} \int_t^{t+\Delta_t} a e^{as} u_s ds \\
   &= e^{-a \Delta_t} x_t +
  e^{-a \Delta_t} \int_t^{t+\Delta_t} a e^{as} u_s ds 
\end{align}
and if $u_s$ is constant int $[t,t+\Delta_t]$
\begin{align}
  x_{t+\Delta_ t}
   &= e^{-a \Delta_t} x_t +
  e^{-a \Delta_t} u \int_t^{t+\Delta_t} a e^{as}  ds\\
  &= e^{-a \Delta_t} x_t +  (1- e^{-a \Delta_t})  u 
\end{align}
which corresponds to an exponential smoother with $\alpha = e^{-a \Delta_t}$. It is interesting to compare with the Discrete time Euler approximation
\begin{align}
  x_{t+\Delta_t} \approx x_t + \Delta_t a (u_t - x_t) \nonumber\\
  &= (1- a \Delta_t) x_t + a u_t
\end{align}
which makes sense considering that to first order
\begin{align}
  e^{-a \Delta_t} &\approx   1- a \Delta_t  
\end{align}
\subsection{Frequency Response}
Taking the bilateral Laplace transform (see primer on Linear Systems)
on the left and right side of \eqref{eqn:ode} we get
\begin{align}
s X(s) = a U(s) - a X(s)
\end{align}
Thus the transfer function looks as follows:
\begin{align}
H(s) = \frac{X(s)}{U(s)} = \frac{1}{1+ s/a}
\end{align}
Thus steady state response to a sinuosid $u_t = \cos(2 \pi f t)$ is as follows
\begin{align}
x_t = H(j 2 \pi f) \cos(2 \pi f t) = m(f)  \cos(2 \pi f t+ \phi(f))
  \end{align}
where $m,\phi$ are the magnitude and phase of the transfer function evaluated aty $s= j 2 \pi f$. 
Note for a complex number of the form 
\begin{align}
  1+ j v  = \sqrt{1 + v^2 } e^{ \alpha}  \\
  \alpha = \arctan(v)
  \end{align}
and
\begin{align}
\frac{1}{ 1+ j v}  =\frac{ \sqrt{1 + v^2 }} e^{ - \alpha}
  \end{align}
Thus,
\begin{align}
  m(f) &= \frac{1}{\sqrt{1+ 4 \pi^2 f^2 \tau^2}}\\
  \phi(f) &= - \arctan(2 \pi f)
  \end{align}
and $\tau = a^{-1}$ is the time constant.

Suppose we want to find the frequency $f$ for which the magnitude of the output is $k$ times the mangitude of the input. We have
\begin{align}
&\frac{1}{1+ 4 \pi^2 f^2 \tau^2} = k^2\\
&f = \frac{\sqrt{1/k^2 -1}}{ 2 \pi \tau}
\end{align}
\subsection{Example}
Consider an exponential smoother with a time constant of 7 seconds. What frequencies are attenuated by more than 99\% ?

\begin{align}
f = \frac{\sqrt{1/0.01^2 -1}}{2\pi 7} = 2.27 \;Hz
\end{align}


\section{Alternative Approaches}
Kalman filters provide an alternative way to deal with variable
interval observations. In the constant interval case Kalman filters
and exponential smoothers are asymptotically equivalent. In the
variable interval case they both weight less the last smooth value the
longer the time between the last observation and the current
observation. However they differ in the way they do so. An advantage
of exponential smoothers over Kalman filters is their simplicity. An
advantage of Kalman filters is that they provide estimates of the
uncertainty of the smoothed values. This can be useful in some
situations.

\bibliographystyle{plainnat} % plainnat
\bibliography{expSmooth}

\end{document}
