import numpy as np
from matplotlib import pyplot as plt

def cutoffFrequency(theta,alpha):
    x = (1+alpha**2 - (1-alpha)**2/(theta**2))/(2*alpha)
    y = np.arccos(x)/(2.0*np.pi)
    print(x,y)
    return y

def power(f,alpha):
    return((1-alpha)/np.sqrt(1 - 2*alpha*np.cos(2*np.pi*f)+ alpha**2))

f = np.arange(0,0.5,0.00001)
t = 1/f
fig =[]
alphaList= np.array([0.5,0.9,0.95,0.99,0.999])
mList = np.ceil((1+alphaList)/(1-alphaList))
lList=[]
for k in range(5):
    s = '\u03B1'+': ' +str(alphaList[k])+ ', \u03B7'+': ' +str(int(mList[k]))
    lList.append(s)

for alpha in alphaList:
    h = power(f,alpha)
    fig.append(plt.semilogy(f,h))


plt.xlabel('Cycles per Sample')
plt.ylabel('Magnitude')
for k in range(5):
    plt.legend( lList)

plt.show()

for alpha in alphaList:
    h = power(f,alpha)
    fig.append(plt.loglog(t,h))


plt.xlabel('Samples per cycle')
plt.ylabel('Magnitude')
for k in range(5):
    plt.legend( lList)

plt.show()

alphaList= np.arange(0.8,0.9999,0.00001)
mList = (1+alphaList)/(1-alphaList)
h = cutoffFrequency(0.1,alphaList)
plt.loglog(mList,h)
plt.xlabel('Effective Memory Size (\u03B7)')
plt.ylabel('Cycles Per Sample ')
plt.show()

plt.plot(alphaList,h)
plt.xlabel('\u03B1')
plt.ylabel('Cycles Per Sample ')
plt.show()



plt.loglog(mList,1/h)
plt.xlabel('Effective Memory Size (\u03B7)')
plt.ylabel('Samples Per Cycle')
plt.show()

from sklearn.linear_model import LinearRegression

x = mList
y = 1/h
x=x[2000:]
y=y[2000:]
x = x.reshape(-1,1)
model = LinearRegression(fit_intercept=False)
model.fit(x,y)
