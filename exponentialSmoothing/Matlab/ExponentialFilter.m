% Exponential smoothing of a univariate time series. 
%
%  xhat(t) = alpha * x(t) + (1-alpha) *xhat(t-1); 
%  initial condition xhat(0) = x(1)
%  n is effective number of observations being averaged by the filter
% Javier R. Movellan Feb 2007

function xhat = ExponentialFilter(x,n)
  alpha = 2/(n+1);
  offset = x(1);
  x = x- offset; % trick to set initial input to zero
  a=[1, -(1-alpha);]
  b = alpha;
  xhat = filter(b, a, x) +  offset;
  
 