% Exponential smoothing of a univariate time series. 
% Handles the initial conditions in a nice manner
% n = Equivalent number of observations averaged by smoother 
% Asymptotic lag: (1-alpha)/alpha
% Javier R. Movellan Feb 2007
function xhat = ExponentialSmoothing(x,n)

  n = max(size(x,1), size(x,2));
  xhat = zeros(n,1);
  xbar =0;
  w=0;
  for t=1:n
    xbar = alpha * x(t) + (1-alpha) *xbar;  
    w  = alpha + (1-alpha)*w;
    xhat(t) = xbar/w;
  end
  