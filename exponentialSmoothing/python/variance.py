import numpy as np
from matplotlib import pyplot as plt

alpha = np.arange(0.01,0.99999,0.001)
nu = (1+alpha)/(1-alpha)
m = 1/(1-alpha)
plt.plot(alpha,nu,'r')
plt.plot(alpha,2*m -1 ,'b--')
plt.show
