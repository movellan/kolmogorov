\documentclass[12pt]{article}
%\usepackage{Mynips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath,amsthm}
\usepackage[pdftex]{graphicx}
\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[square,numbers,sort]{natbib} 
%\usepackage[round,sort]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}

 \title{Primer on Support Vector Machines\\
{}}
\author{ }
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\R{\mathcal{R}}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\pfrac}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\ppfrac}[3]{\frac{\partial^2 #1}{\partial #2 \partial #3}}

\newtheorem{prop}{Proposition}[section]
\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{lem}{Lemma}[section]
\newtheorem{rem}{Remark}[section]
\newtheorem{defi}{Definition}[section]
\newtheorem{ex}{Example}[section]





\begin{document}
\maketitle

\begin{center} 
 Copyright \copyright{}  2015 Javier
R. Movellan. \end{center} 
 
\newpage
\section{Preliminaries}
Let $w\in \R^n$,  $b\in \R$.  Let $\mathcal{S}$ be a hyperplane
defined by the set of points $x\in \R^n$ satisfying
\begin{align}
w'x +b =0
\end{align}
Consider a point $x_0$ defined as follows
\begin{align}
x_0 = - b \frac{w}{\|w\|}
\end{align}
Thus $x_0$ is collinear with $w$, of length $|b|$. If $b$ is negative then $x_0$ is in the same direction as $w$, if positive in the opposite direction. Note $x_0$ belongs to $\mathcal{S}$
\begin{align}
w' x_0 +b = -b +b =0
\end{align}
Thus $-b/\|w|$ is the distance between the origin and the hyperplane
$\mathcal{S}$. The sign tells us the orientation with respect to $w$.
If $b$ is positive then the origin is ``below'' $\mathcal{S}$, i.e. ,
in the opposite direction to $w$.


To compute the distance between an arbitrary point $u \in \R^n$ and the hyperplane $\mathcal{S}$ we project $u$ onto $w$ and add the distance between the origin and $\mathcal{S}$, i.e.,
\begin{align}
d = \frac{w'u}{|w|} + \frac{b}{\|w\|} =\frac{w' y+ b}{\|w\|}
\end{align}

\begin{defi}{\bf Linear Classifier:}
  A linear classifier, defined by vector $w\in \Re^n$ and scalar $b$ takes vectors $x\in \R^n$ as inputs and outputs a binary classification $\hat y(x)$
\begin{align}
\hat y(x) = \text{sign}(w'x +b)
\end{align}
We note that $w'x +b=0$ is the separating hyperplane between the two categories. 


\end{defi}  
\begin{defi}{\bf Margin of an example with respect to a classifier:}
Let $x_i\in \R^n$, $y_i \in \{1,-1\}$ represent an input vector and
its category label. The margin $\gamma_i$ of $(x_i,y_i)$ with respect
to the hyperplane $x'w +b=0$ is defined as follows
\begin{align}
  \gamma_i(w,b) &=  \frac{\eta_i(w,b)}{\|w\|}\\
  \eta_i(w,b) &= y_i( w'x_i +b)
\end{align}
We refer to $\eta_i(w,b)$ as the net product for example $i$ or
unnormalized margin of example $i$.  Note $|\gamma_i|$ is the distance
between $x_i$ and the hyperplane defined by $w,b$. If $\gamma_i$ is
positive then $w'x_i +b$ and $y_i$ have the same sign, i.e., $w,b$
correctly classify $x_i$. If negative they have opposite signs,i.e.,
$w,b$ classify $x_i$ incorrectly.  Intuitively the absolute value of
the margin could be taken to indicate the certainty of the
classifier. Large positive margins indicate that the classifier is
confident and correct. Large negative margins indicate that the
classifier is confident but incorrect. \footnote{While this notion is
  intuitive it may not be a good idea in some cases.  For example, if
  a point is very far away from the separating hyperplane but we have
  never seen points that far in the training set, we may not want the
  classifier to be confident. }
\end{defi}
\begin{prop}{\bf Scale Invariance:}
  Margin is invariant to scaling of $(w,b)$, i.e, for $k>0$

  \begin{align}
\gamma_i(w,b) = \gamma_i(kw,kb)
    \end{align}
  \begin{proof}
    \begin{align}
  \gamma_i(kw,kb) = \frac{y_i(kw'x_i+kb)}{\|kw\|} = \frac{k}{k} \frac{y_i(w'x_i+b)}{\|w\|} = \gamma(w,b)
      \end{align}

 \end{proof}
 \end{prop}
    
\begin{defi}{\bf Margin of a set with respect to a classifier}
  Let $\mathcal{S} = \{ (x_i, y_i), i=1, \cdots, m\}$ be a collection of vectors $x_i \R^n$ and binary labels $y_i \in \{-1,1\} $. The margin of the set with respect to a binary classifier $w,b$ is the minimum margin of the points in the set, i.e.
  \begin{align}
&\eta(w,b) = \min_i \eta_i(w,b) =    \min_i y_i(w'x_i+b)\\ 
&\gamma(w,b) = \frac{\eta(w,b)}{\|w\|} = \min_i \gamma_i(w,b) = \min_i \frac{ w' x_i +b}{\|w\|}
\end{align}
\end{defi}
We refer to $\eta(w,b)$ as the net product, or unnormalized margin of
$w,b$ with respect to the set.  Intuitively it seems like a good idea
to try to find classifiers with large margin. Note however that other
choices could also have appeal, e.g., classifiers with large average
margin.

\begin{defi}{\bf Maximum Margin Classifier}
A classifier $(\hat w, \hat b)$ is maximum margin with respect to a training set $\mathcal{S}=  \{ (x_i, y_i), i=1, \cdots, m\}$, if it satisfies the following condition
\begin{align}
  (\hat w, \hat b) &= \argmax_{w,b} \gamma(w,b)
   = \argmax_{w,b}\frac{ \eta(w,b)}{\|w\|}\nonumber\\
  &= \argmax_{w,b} \frac{1}{\|w\|} \min_i\;\; y_i  (w' x_i +b) 
\end{align}
\end{defi}
While this is typically called a Maximum margin solution, we could as well have called it Maxi-Min (or Mini-Max) solution (maximize the minimum margin). 
\begin{prop}{\bf Infinite Solutions:}
  Let $( w,  b)$ be a maximum margin classifier with respect to $\mathcal{S}$. Let $k>0$ then, $(w k , bk)$ is also a maximum margin solution
\end{prop}
\begin{proof}
 Follows from the fact that margins are scale invariant. 
\end{proof}
 Note the proof would not work for $k\leq 0$. Note  all the solutions have the same separating hyperplane, and
same margins but different net products  $\eta_i$.
\begin{defi}{\bf Standard Maximum Margin Classifier}
  We say that a maximum margin solution $w, b$ is standard if its net product is 1, i.e., 
 \begin{align}
\eta(w,b)= \min_i \eta_i(w,  b)=1
 \end{align}
\end{defi}
\begin{rem}{\bf Standard Maximum Margin Classifier}
  Note the constraint that the net product be 1 is arbitrary. We could as well have constrained  the solution weights to have unit length or simply have put not constraint at all.  I suspect the reason for the unit net product rather than unit length weights is that it facilitates the machinery of quadratic programming optimization. 
  \end{rem}
\begin{prop}{\bf Existence of Standard Maximum Margin Classifier}
If $\mathcal{S}$ is linearly separable then it has a Standard Maximum Margin Classifier
\end{prop}
\begin{proof}
  If $\mathcal{S}$ is linearly separable then all the  maximum margin  classifiers have positive margin. Let   $\tilde w, \tilde b$ be one such classifier. Let
\begin{align}
  & \hat w = k \tilde w\\
  &\hat b = k \tilde b\\
 & k=  \frac{1}{\eta(\tilde w,\tilde b)}
\end{align}
Since $\eta(\tilde w, \tilde b) > 0$ it follows that 
\begin{align}
    \gamma(\hat w,\hat b) =   \gamma(\tilde w,\tilde b)
\end{align}
i.e., $(\hat w,\hat b)$ is a maximum margin classifier. 
In addition,
\begin{align}
\eta(\hat w, \hat b) =   \min_i  y_i  (\hat w  x_i +\hat b)  &=
k \min_i  y_i  (\tilde w  x_i +\tilde b)  \nonumber\\
  &=k  \eta(\tilde w, \tilde b) = 1 
\end{align}



\end{proof}

\begin{prop}{\bf Standard Maximum Margin Classifier}
 The classifier $(\hat w,\hat b)$ is a standard maximum margin classifier with respect to the training set  $\mathcal{S}=  \{ (x_i, y_i), i=1, \cdots, m\}$ if it satisfies
 \begin{align}
\max_{w,b} \frac{1}{\|w\|}    \;\text{s.t.}\; \eta(w,b)=1
\end{align}
 \begin{proof}
   Follows from the fact  that the net product of a standard maximum margin classifier is 1. 
   \end{proof}
\end{prop}

\begin{prop}{\bf Standard Maximum Margin Classifier}
 The classifier $(\hat w,\hat b)$ is a standard maximum margin classifier with respect to the training set  $\mathcal{S}=  \{ (x_i, y_i), i=1, \cdots, m\}$ iff it satisfies
 \begin{align}
\min_{w,b}  \frac{1}{2} \|w\|^2   \;\text{s.t.}\; \eta(w,b)=1
\end{align}
 \begin{proof}
 Given the set of $w,b$ with $\eta(w,b)=1$ maximizing $1/\|w\|^2$ is equivalent to minimizing $\|w\|^2$.
   \end{proof}
\end{prop}

  

\begin{prop}{\bf Standard Maximum Margin Classifier}
  The classifier $(\hat w,\hat b)$ is a standard maximum margin classifier with respect to the training set  $\mathcal{S}=  \{ (x_i, y_i), i=1, \cdots, m\}$ iff it satisfies
 \begin{align} \label{eqn:smm}
\min_{w,b}  \frac{1}{2} \|w\|^2   \;\text{s.t.}\; \eta_i(w,b)\geq 1, \;\text{for} \; i=1,\cdots, n
 \end{align}
\begin{proof}
  Let
  \begin{align}
 &S_1=\{ (w,b) : y_i (w' x_i +b) \geq 1,\; i=1,\cdots, n\}\\
 &S_2 =\{(w,b) : \min_i y_i (w' x_i +b) =1\}
    \end{align}
Note $S_2 \subset S_1$. Let $(\tilde w,\tilde
 b)\in S_1 $, $(\tilde w,\tilde b)\notin S_2 $. Thus there is an
 $\epsilon >0$ such that
 \begin{align}
\min_i y_i (\tilde w'
 x_i +\tilde b) =1+\epsilon
\end{align}
Let $\hat w = \tilde
 w/(1+\epsilon), \tilde b = \hat b/(1+\epsilon)$. Note
\begin{align}
\min_i  y_i (\hat w' x_i +\hat b) =1
\end{align}
Thus $(\hat w, \hat b) \in S_2$ and  $\|\hat w\|^2 < \|\tilde w\|^2$. Thus loosening the constraint set from $S_2$ to $S_1$ does not affect the solution. 


\end{proof}
\end{prop}

\begin{prop}{\bf Uniqueness of Standard Maximum Margin Solution}
  If there is a Standard Maximum Margin Solution, it is unique.
  \begin{proof}
 Follows from the fact that \eqref{eqn:smm} defines a quadratic programming, convex optimization problem. 
    \end{proof}
\end{prop}

\begin{defi}{\bf Hinge Loss:}
  The Hinge loss function $h$ be defined as follows
 \begin{align}
h(y,v) = \max(0,1-y v ),\; \text{for $y\pm 1 ,\; u \in \R$}
 \end{align}
\end{defi}

\begin{prop}{\bf Hinge Loss:}
 Let the  training set $S$ be linearly separable. Then $(\hat w, \hat b)$ is the standard maximum margin solution iff
 \begin{align}
   (\hat w, \hat b) = & \argmin_{w,b}\frac{1}{2} \|w\|^2  \;\text{s.t.}\;, c \sum_i h(y_i, w'x_i+b)=0
 \end{align}
 
\begin{proof}
 Follows from the fact that $h(y_i,w'x_i+b) =0$ iff $y_i(w'x_i+b) \geq 1$
\end{proof}
\end{prop}


\begin{prop}{\bf Hinge Loss:}
 Let the  training set $S$ be linearly separable. Then $(\hat w, \hat b)$ is the standard maximum margin solution iff
 \begin{align}
   (\hat w, \hat b) = & \lim_{c\to \infty} \argmin_{w,b}\frac{1}{2} \|w\|^2  + c \sum_i h(y_i, w'x_i+b)
 \end{align}
 
\begin{proof}
  Let
  \begin{align}
 &S_1=\{ (w,b) : y_i (w' x_i +b) \geq 1,\; i=1,\cdots, n\}\\
 &S_2 =\{(w,b) : \sum_i h(y_i, w'x_i+b) =0\}
  \end{align}
  Note $y_i(w'x_i +b) \geq 1$ iff $h(y_i, (w'x_i +b)) =0$. Thus, since the training set is separable, there is a non-empty set of $(w,b)$ such that $\sum_i h(y_i, w'x_i +b) =0$.  Thus for $\hat c$ large enough the constraint $\hat c \sum_i h(y_i, w'x_i+b) =0$ ensures the solution is in $S_1$. Moreover  the function $\sum_i h(y_i, w'x_i+b) =0$  is neutral for all members of $S_1$, i.e., only the $\|w\|^2$ has an effect on the choice of $w,b$.
  Note however things would be different if the training set were not separable. In this case \eqref{eqn:h1} would have a solution while
\eqref{eqn:h2} would have no solution.
\end{proof}
\end{prop}

\begin{defi}{\bf Soft Margin Classifier:}
$(\hat w, \hat b)$ is a standard soft margin classifier with parameter
  $c>0$ if it satisfies
  \begin{align}
   & (\hat w, \hat b) = \argmin_{w,b}\frac{1}{2} \|w\|^2  + c \sum_i h(y_i, w'x_i+b \end{align}
\end{defi}
\begin{rem}{\bf Soft Margin Classifier:} There is a misconception that soft margins are needed for the case in which the data are not separable. That's not the case, the maximum margin objective function is applicable to non-separable cases,
\begin{align}
(\hat w,\hat b) = \argmax_{w,b} \min_i \frac{y_i(w'x_i+b)}{\|w\|}
\end{align}
However quadratic programing algorithms may not be applicable for finding solutions in such cases.

\end{rem}

\begin{prop}{\bf Soft Margin Classifier:}

  \begin{align}
   & \min_{w,b}\frac{1}{2} \|w\|^2  + c \sum_i h(y_i, w'x_i+b)\label{eqn:h1} \\
   &=   \min_{w,b,\xi_1 \cdots \xi_n}\;\;\frac{1}{2} \|w\|^2+c \sum_i \xi_i  \nonumber\\
   &\text{s.t.}\;\;  y_i (w' x_i +b) \geq 1 - \xi_i,\; \text{and}\; \xi_i \geq 0,\; i=1,\cdots, n\label{eqn:h2}
 \end{align}

\begin{proof}
 For $y\pm 1$, $v\in \R$,
 \begin{align}
   \min_{\xi} yv +\xi \geq 1&=\begin{cases}
   0&\text{if $yv \geq 1$}\\
   1-yv&\text{else}
   \end{cases}
   \nonumber\\
   &= h(y,v)
   \end{align}
 
\end{proof}

\end{prop}
\begin{prop}{\bf Soft Margin Classifier:}
  The solution to the soft margin classification problem is unique
  \begin{proof}
    Follows from the fact that \eqref{eqn:h2} is a convex, quadratic optimization problem. 
    \end{proof}
\end{prop}
  
\section{Bayesian Interpretation}
\subsection{Bayesian Methods}
We are given a training set of i.i.d. random samples
$\mathcal{S}=\{(x_1,y_1),\cdots, (x_m,s_m)\}$,$x_i\in \R$, $y_i =\pm
1$ a prior distribution over parameters $p(\theta)$. We are given
a query input $x\in\R$ and asked to make a prediction $\hat y$ for
that input. To this end we formulate a loss function $\rho(y,\hat y)$
where $y$ represents the true label and $\hat y$ the predicted
label. In the Bayesian approach we choose the query answer that
minimizes the average expected cost, aka, the risk
\begin{align}
  \hat y &= \argmin_v E[ h(Y,v)\;|\;x, \mathcal{S}]\nonumber\\
 & = \argmin_{v} \sum_y \int p(\theta \;|\; \mathcal{S} ) p(y|x,\theta) h(y,v) d\theta 
  \end{align}
where
\begin{align}
  p(\theta| \mathcal{S}) = \frac{p(\theta) p(\mathcal{S}|\theta)}{p(\mathcal{S})}\\
  p(\mathcal{S}) = \int p(\theta) p(\mathcal{S} |\theta) d\theta\\
  p(\mathcal{S}\given \theta) = \prod_i p(x_i,y_i| \theta)
\end{align}
It is common to approximate the posterior distribution $p(\theta| \mathcal{S})$ with a delta function at an estimate $\hat \theta$ of $\theta$, i.e.,
\begin{align}
E[h(Y,v)\given x,S] \approx \sum_y p(y|x,\hat \theta)h(y,v) 
\end{align}

A popular estimate of $\theta$  is the posterior mode 
\begin{align}
  \hat \theta = \max_\theta \log p(\theta|\mathcal{S}) =
  \max_\theta \log p(\theta) + \sum_i \log p(y_i |x_i, \theta)
  \end{align}
This is sometimes as the MAP solution (maximum posterior). 
Another popular estimate is the posterior mean
\begin{align}
\hat \theta= E[\theta|\mathcal{S}]= \int \theta
\;p(\theta|\mathcal{S}) d\theta \end{align} Here we show that SVMs
have a Bayesian interpretation with $\theta=(w,b)$ and a solution is
based on the posterior mode  of the $w,b$ parameters under a
specific data-generating likelihood function $p(y_i, x_i | w,b)$


\subsection{SVMs}
The parameter $\theta$  consists of the weight vector $w$ and the bias $b$. A MAP approach calls for finding the peak of the  posterior distribution of $w,b$ given the training data $x,y$
\begin{align}
p(w,b|x,y)= \frac{p(w,b)p(x,y|w,b)}{p(x,y)}
\end{align}
where
\begin{align}
p(x,y) = \int p(w,b) p(x,y|w,b) dwdb 
\end{align}
is a constant with respect to $w,b$. We assume the priors for  $X,Y$ satisfy the following constraints
\begin{align}
 p(x,y|w,b) = p(x|w,b) p(y|w,b,x) =p(x) p(y|x,w,b)
\end{align}
We then make the likelihood of the label given $x,w,b$ proportional to a exponential function of the error. Specifically, for a fixed $k>0$ we let
\begin{align}
p(y|w,b,x) = \frac{e^{-k h(y,w'x+b)}}{Z(k)}
\end{align}
We need the partition constant $Z(k)$ independent of $w,b$. This would not work if we were to make
\begin{align}
 Z=  e^{-k h(-1,w'x+b)} + e^{-k h(1,w'x+b)}
\end{align}
Instead we make
\begin{align}
Z(k) = \max_u e^{-k h(-1,u)} + e^{-k h(1,u)} = 1+e^{-2k}
\end{align}
This guarantees that $p(y|w,b,x) \in [0,1]$. However the probabilities for $Y=-1$ and $Y=1$ will not add up to one. To take care of this problem we add a dummy value $Y=0$. This value can be interpreted as ``neither of them'', or ``unclear''. The final likelihood model looks as follows
\begin{align}  
  p(y|w,b,x) = \begin{cases}
    \frac{ e^{-k h(y,u)}}{1+e^{-2k}}&,\text{for $y=\pm1$}\nonumber\\
    1- \frac{e^{-k h(-1,u)} +e^{-k h(1,u)}}{1+e^{-2k}}&,\text{for $y=0$}\nonumber\\
    0&\text{else}
    \end{cases}
\end{align}


Assuming conditionally independent samples $(x_i,y_i)$,  with the prior for $w$ Gaussian with zero mean,diagonal variance $\sigma^2$ and with the prior for $b$ having variance much larger than $b$ we get
\begin{align}
  \max_{w,b} \log p(w,b|x_1,\cdots, x_m,y_1,\cdots y_m)&= \max_{w,b} - \frac{1}{2\sigma^2} \|w\|^2 -  k \sum_{i=1}^m h(y_i,w'x_i+b)\nonumber\\
  &=\min \frac{1}{2} \|w\|^2+ c \sum_{i=1}^m h(y_i,w'x_i+b)\\
c&=  \frac{k}{\sigma^2}
 \end{align}
Figure \ref{fig:svmLike} center and right show the SVM likelihood model for $c=1$ and $c=3$. 

\begin{figure}[h] 
\begin{center}
\includegraphics[width=.7\textwidth]{figures/svmLikelihoodModel.pdf}
\end{center}
\caption{\it Left: Logistic Regression label likelihood model. Center: Label likelihood model for  SVMs with $c=1$. Right: Label likelihood model for SVMs with $c=3$ }\label{fig:svmLike}
\end{figure}



\subsection{Bayes Point Machines}
Bayes Point Machines basically utilize the same probabilisitic model as SVMs but estimate the mean rather than the mode of the posterior distribution of $w,b$ \cite{Kapoor2002}.

The Bayes Point is a classifier tht best mimics the Bayesian classification strategy. It has been shown that under some conditions the expected value of the posterior distribution converges quickly to the bayes point.
\begin{verbatim}

5] T. Watkin. Optimal learning with a neural network. Europhysics Letters, 21:871–877,
1993.
Bayes point machine is the linear classifier that best approimates the bayes classifier. Watkin showed that under some conditions, the expected value of the posterior distribution is a very good approxikmation to the bayes point.  

  [2] Ralf Herbrich, Thore Graepel, and Colin Campbell. Bayes point machines. Journal of Machine Learning Research, 1:245–279, 2001.
  Call bayes point machines, to algorithms that attempt to find the expected value of the posterior distribution of the parameters of a classifier 

\end{verbatim}
\subsection{Restricted Bayes Optimal Classifiers}
RBC model the joint density of $x,y$ using standard density estimation methods, like Parzen windows or mixtures of Gaussians. They then find an optimal linear classifier given the estimated density. They show that for liniearly separable data, as the width of the Parzen windows goes to zero, the solution converges to the maximum margin solution. For non-linearly separable error, it chooses among all the hyperplanes that minimize the training error, the hyperplane that maximizes the margin.


\begin{verbatim}
Tong \& Koller

Proceedings of the Seventeenth National Conference on Artificial Intelligence (AAAI-00),
pages 658-664, Austin, Texas, August 2000
\end{verbatim}

\subsection{Logistic Regression}
Logistic regression models take the following form
\begin{align}
&  p(Y_i=1| x_i,w) = \frac{1}{1+e^{-w'x_i}}\\
&  p(Y_i=-1| x_i,w) = 1- p(Y_i=1| x_i,w) = \frac{1}{1+e^{w'x_i+b}}
\end{align}
The posterior probability of $w$ given an  observed input $x$  and label $y$ takes the following form
\begin{align}
p(w,b|x,y) = \frac{p(w,b|x) p(y|w,b,x)}{p(x,y)}
\end{align}
Using the formula for the log probability of a Bernoulli random variable
\begin{align}
  \log p(y|w,b,x) &= \frac{1+y}{2}\log p(Y=1|w,b,x) + \frac{1-y}{2}\log p(Y=-1|w,b,x)\nonumber\\
&  = - \log(1+e^{-y (w'x+b)})
\end{align}
Assuming $p(w,b|x)= p(w)p(b)$ and $p(w)$ is Gaussian, zero mean, diagonal variance proportional to $\sigma^2$, and $p(b)$ has a very large variance compared to $\sigma^2$,  and assuming the labels are conditionally independent we get
\begin{align}
  \log p(w,b|x_1,\cdots, x_m, y_1, \cdots, y_m) =
-  \sum_{i=1}^m \log(1+ e^{-y_i (w'x_i+b)}) - \frac{1}{2}  \sigma^2 \|w\|^2 +c
\end{align}
Thus maximizing the log posterior is equivalent to minimizing the following loss function
\begin{align}
\rho(w,b) = \frac{1}{2} \|w\|^2+ \sigma^2   \sum_{i=1}^m \log(1+ e^{-y_i (w'x_i+b)})
\end{align}
Figure~\ref{fig:logisvm} shows the comparison between the logistic loss function and the hinge loss utilized in SVMs. Figure \ref{fig:svmLike} left compares the likelihood model of logistic regression and SVMs. 
\begin{figure}[h] 
\begin{center}
\includegraphics[width=.7\textwidth]{figures/logisvm.pdf}
\end{center}
\caption{\it The SVM (hinge) and logistic regression loss functions for $y=1$ and $y=-1$}\label{fig:logisvm}
\end{figure}


\bibliographystyle{apalike} % plainnat
\bibliography{svm}



\end{document}
