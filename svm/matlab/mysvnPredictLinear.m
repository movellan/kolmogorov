% function m = mysvnPredictLinear(q,model)
% q is a matrix of new query points. 
% rows are variables, cols are points 
% model is an svm model using libsvm matlab format
% it can be obtained using the libsvm svntrain function
% m is a vector witht the margin from the separating manifold for
% each query point
% 
% the function is very inefficient. It is just meant to clarify how
% the libsvm model format specifies an svm model
% works only with Linear kernels
%
% @ Javier R. Movellan. 2011


function m = mysvnPredictLinear(q,model)

nSupportVectors = size(model.SVs,1);
nVars = size(model.SVs,2);

sv=zeros(nVars,nSupportVectors);
g=model.Parameters(4);
threshold = model.rho
% put the alpha parameters in a vector called alpha
alpha= zeros(nSupportVectors,1);

for j=1:nSupportVectors
  alpha(j) = model.sv_coef(j);
  for i=1:nVars
    sv(i,j) = model.SVs(j,i);
  end
end

% to get the prediction m to a query q we compute the innerproduct
% (linear distance) 
%  between the query vector and each of the support vectors.
% we then get a weighted sum of those distances. We then substract a
% threshold. That's the prediction.


for i=1: size(q,2)   % iterate over query points
  m(i)=0; 
  for j=1:nSupportVectors
    d= q(:,i)' *sv(:,j); 
    m(i) = m(i)+d*alpha(j); 
  end
  m(i) = m(i) - threshold;
end

% since the distance to each suport vector is a linear operation we
% the weight sum over distances can be sumarized as an inner
% product with a single vector w

w=zeros(size(sv(:,1)));
for j=1:nSupportVectors
  w= w+sv(:,j)*alpha(j);
end

 m = q'*w;
