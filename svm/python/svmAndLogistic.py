

# plot the error function for svm and logistic regression

from numpy import *
from matplotlib.pyplot import *

x=arange(-4.,4.,0.1)


# logistic regression error when y=1
l1 = log(1. + exp(-x))

# logistic regression error when y=0
l0 = log(1+exp(x))

# svm hinge error

h1 = 1.-x
h1[argwhere(h1<0.)]=0.
h0=1.+x
h0[argwhere(h0<0.0)]=0.


# In[35]:

plot(x,h0,'b')
plot(x,h1,'b',label='svm')
plot(x,l0,'r')
plot(x,l1,'r',label='logistic')
ylim((-0.1,6))
text(-1.9,3.5,'$y=1$',fontsize=25)
text(1,3.5,'$y=-1$',fontsize=25)
legend(loc=2)
xlabel('$w^{\prime}x+b$',fontsize=30)
savefig('logisvm.pdf')
