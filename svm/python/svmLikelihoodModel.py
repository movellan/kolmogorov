

# plot the likelihood model implicit in svm
from numpy import *
from matplotlib.pyplot import *

x=arange(-6.,6.,0.1)


# logistic regression error when y=1
l1 = log(1. + exp(-x))

# logistic regression error when y=0
l0 = log(1+exp(x))

# svm hinge error

h1 = 1.-x
h1[argwhere(h1<0.)]=0.
h0=1.+x
h0[argwhere(h0<0.0)]=0.

c=1
z=1+exp(-2*c)
p0=exp(-c*h0)/z
p1=exp(-c*h1)/z
pn= 1-p0-p1

subplot(1,3,2)

plot(x,p0,label='y=-1',c='b')
plot(x,p1,label='y=1',c='r')
plot(x,pn,label='y=0',c='g')
legend(loc=2)
ylim((0,1))
xlabel('$w^{\prime}x+b$',fontsize=30)

c=3
z=1+exp(-2*c)
p0=exp(-c*h0)/z
p1=exp(-c*h1)/z
pn= 1-p0-p1

subplot(1,3,3)

plot(x,p0,label='y=-1',c='b')
plot(x,p1,label='y=1',c='r')
plot(x,pn,label='y=0',c='g')
legend(loc=2)
ylim((0,1))
xlabel('$w^{\prime}x+b$',fontsize=30)


l1=1./(1.+exp(-x))
l0=1-l1

subplot(1,3,1)
plot(x,l0,label='y=-1',c='b')
plot(x,l1,label='y=1',c='r' )
legend(loc=2)
ylim((0,1))
xlabel('$w^{\prime}x+b$',fontsize=30)
show()


# savefig('svmLikelihoodModel.pdf')


