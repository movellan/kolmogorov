\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}
\usepackage{listings}
\usepackage{boxedminipage}

 \title{Primer on Fisher Information Theory}
\author{Copyright \copyright Javier R. Movellan}



\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\R}{\mathcal{R}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}





\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}


\theoremstyle{plain}% default
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem*{cor}{Corollary}

\theoremstyle{definition}
\newtheorem{defn}{Definition}[section]
\newtheorem{conj}{Conjecture}[section]
\newtheorem{exmp}{Example}[section]

\theoremstyle{remark}
\newtheorem*{rem}{Remark}
\newtheorem*{note}{Note}
\newtheorem{case}{Case}







\begin{document}
\maketitle


Please cite as

{ Movellan J. R. (2012) {\em Primer on Fisher  Information Theory.} MPLab Tutorials,
University of California San Diego }

    
\newpage

\section{The Fisher Information Inequality}
Here we motivate the Fisherian approach to information via its role
bounding mean squared efficiency of estimators. 
\begin{defn}{\bf The Fisher Score}
Let $X,Y$ be random vectors. We call $X$ the parameter (the value we
want to guess) and $Y$ the sensor data. The Fisher score
$S=(S_1,\cdots,S_n)'$ is a random vector defined as follows
\begin{align}
S= f(Y,X)
\end{align}
where
\begin{align}
f(y,x) = \nabla_x \log p(y\given x) = \frac{\partial \log p(y\given x)}{\partial x}
\end{align}
\end{defn}
\begin{thm}
\begin{align}
E[S\given x] =0
\end{align}
\begin{proof}
\begin{align}
E[S \given x] = \int p(y\given x) \frac{\partial \log p(y\given x)}{\partial x} dy = \int \frac{ \partial p(y\given x)}{\partial x} dy = \frac{\partial}{\partial x}  \int p(y\given x) dy =0
\end{align}
\end{proof}
\end{thm}
\begin{rem}{\bf Fisher Information Inequality}
 We are interested in defining how much information $Y$ provides about $X$. The approach taken by Fisher is to provide an lower bound on how accurately one can possibly estimate $X$ using arbitrary functions of $Y$. 
\end{rem}
\begin{thm}{\bf Fisher Information Inequality}
Given a scalar function $f$, let $T = f(Y)$. Then
\begin{align}
Var[T\given x] \geq \nabla_x E[T\given x]' Cov[S\given x]^{-1} \nabla_x E[T\given x]
\end{align}
\begin{proof}
{\bf Step 1}\\
For a fixed vector $u\in \R^n$ be note that 
\begin{align}
Var[T- u' S\given x] &= Var[T\given x] + Var[-u'S\given x] + 2Cov[T,-u'S\given x]\nonumber\\
&=Var[T\given x] + u'Cov[S\given x] u - 2 u' Cov[S,T\given x]
\end{align}
Since the score has zero mean it follows that
\begin{align}
Cov[S,T\given x] = E[ST\given x]
\end{align}
Moreover
\begin{align}
E[ST\given x] &= \int f(y) p(y\given x) \nabla_x  \log p(y\given x)
 dy\nonumber\\
&= \int f(y) \nabla_x  p(y\given x)  dy 
= \nabla_x  \int f(y) p(y \given x) dy\nonumber\\
&= \nabla_x  E[ T\given x]
\end{align}
Thus
\begin{align}
Var[T- u' S\given x] 
=Var[T\given x] + u'Cov[S\given x] u - 2 u' \nabla_x E[T\given x]
\end{align}
{\bf Step 2}
\begin{align}
\nabla_c Var[T- u'S\given x] = 2 Cov[S\given x] - 2 \nabla_x E[T\given x] 
\end{align}
Setting the gradient to zero, considering that covariance matrices are positive definite, we find that the minimum for $Var[T-u'S\given x]$ with respect to $u$ is achieved for 
\begin{align}
u =  Cov[S\given x]^{-1} \nabla_x E[T\given x] 
\end{align}
Thus
\begin{align}
\min_u Var[T- u'S \given x] &= Var[T] + \nabla_x E[T\given x]' Cov[S\given x]^{-1} \nabla_x E[T\given x] \nonumber\\
& - 2\nabla_x E[T\given x] '  Cov[S\given x]^{-1} \nabla_x E[T\given x] \nonumber\\
&=Var[T\given x] -\nabla_x E[T\given x] ' Cov[S\given x]^{-1} \nabla_x E[T\given x] \geq 0
\end{align}
and
\begin{align}
Var[T\given x] \geq \nabla_x E[T\given x]' Cov[S\given x]^{-1} \nabla_x E[T\given x] 
\end{align}
\end{proof}
\end{thm}
\begin{cor}{\bf Cramer Rao Lower Bound for Biased Estimators}
Let $f,g$ be scalar functions. Let $T=f(Y)$ be an estimator of $g(X)$
with bias function $b$, i.e., 
\begin{align}
b(x) = E[T\given x] - g(x)
\end{align}
The mean square error of $T$  bound as follows
\begin{align}
E[(T - g(x))^2 \given x] \geq&   \nabla_x (b(x)+g(x))' Cov[S\given x]^{-1} \nabla_x (b(x)+g(x)) + b(x)^2
\end{align}
\begin{proof}
Follows trivially from the fact that for any random variable $V$, and constant $c$
\begin{align}
E[(V-c)^2] = Var[V] + (E[V] -c)^2
\end{align}
\end{proof}
\end{cor}
\begin{cor}{\bf Cramer Rao  Lower Bound (CRLB)}
Let $\hat X = f(Y)$ be an unbiased estimator of $X$, i.e. $E[\hat X
  \given x] = x$.  Then for any vector $u\in R^n$
\begin{align}
Var[u' \hat X\given x] \geq Var[u'S] =u' Cov[S\given x]^{-1} u 
\end{align}
\begin{proof}
Let $g(X) = u' X$  and let $T= u'\hat X$. Since $\hat X$ is an unbiased estimator of $X$ then $T$ is an unbiased  estimator of $u'X$, i.e.
\begin{align}
&E[T\given x] = u' X\\
&\nabla_x E[T\given x] = u
\end{align}
Using the Fisher information inequality theorem
\begin{align}
Var[T\given x] = Var[u'\hat X\given x] \geq u' Cov[S\given x]^{-1} u 
\end{align}
\end{proof}
\end{cor}
\begin{rem}
Since for any vector $u$ 
\begin{align}
Var[u'T\given x] = u' Cov[T\given x] u 
\end{align}
Then the CRLB is sometimes expressed as follows
\begin{align}
Cov[\hat X \given x] \geq Cov[S\given x]^{-1}
\end{align}
meaning that for any arbitrary $u$
\begin{align}
u' Cov[\hat X\given x] u' \geq u' Cov[S\given x]^{-1} u
\end{align}
\end{rem}
\begin{exmp}{\bf Estimation of the Mean}
Let $Y =(Y_1, \cdots Y_n)'$ where 
\begin{align}
 Y_i= X_i +\sigma Z_i, 
\end{align}
$\sigma$ is a known positive scalar and $Z_1, \cdots, Z_n$ are iid standard
Gaussian random variable. Thus
\begin{align}
\log p(y \given x) = \sum_{i=1}^n \log \frac{1}{\sqrt{2 \pi \sigma^2}}  -\frac{1}{2}  \frac{(x-y_i)^2}{\sigma^2}
\end{align} 
Note
\begin{align}
\frac{\partial \log p(y\given x)}{\partial x} =
 \sum_{i=1}^n \frac{y_i-x}{\sigma^2}
\end{align}
Thus the Fisher score takes the following form
\begin{align}
S = \sum_{i=1}^n \frac{Y_i-X}{\sigma^2}
\end{align}
Moreover
\begin{align}
Var[S\given x] = \frac{1}{\sigma^4} \sum_{i=1}^n Var[Y_i\given x] = 
\frac{n}{\sigma^2}
\end{align}
Thus for any estimator $T$ of $X$ based on $Y$, it follows that 
\begin{align}
&g(x) = x\\
&\nabla_x (b(x) + g(x)) = 1 + \nabla_x b(x)
\end{align}
and 
\begin{align}
E[(T-x)^2\given x] \geq (1+\nabla_x b(x))' \frac{\sigma^2}{n}  (1+\nabla_x b(x)) + b(x)^2
\end{align}
Let's study the class of estimators with bias of the form
\begin{align}
b(x) = k x
\end{align}
for some cosntant $k$. In this case
\begin{align}
E[(T-x)^2\given x] \geq (1+k)^2 \frac{\sigma^2}{n} + k^2 x^2
\end{align}
For large $n$ the $k^2 x^2$ term dominates. We note  this term is minimized for  $k=0$. Thus, for large $n$
\begin{align}
E[(T-x)^2\given x] \geq   \frac{\sigma^2}{n}
\end{align}
This suggests that if we had a choice over $n$ and $\sigma$ and our
goal is to estimate $X$ then we should choose $n$ as large as possible
and $\sigma$ as small as possible. Now consider the following
estimator of $X$
\begin{align}
T = \frac{1}{n} \sum_{i=1}^n Y_i
\end{align}
We note
\begin{align}
E[T\given x] =k  \sum_{i=1}^n E[Y_i\given x] =  x\\
Var[T\given x] =\frac{1}{n^2} \sum_{i=1}^n Var[Y_i\given x] = \frac{\sigma^2}{n}
\end{align}
Thus $T$ is an unbiased estimator of $X$ and it achieves the CRLB for
the class of estimators with a linear bias function $b(x) = k x$.
\end{exmp}
\begin{rem}
Note the variance of the Fisher score tells us how well we can expect
to estimate $X$. The bound puts no constraints on how to estimate $X$
from $Y$. This suggests that the variance of the Fisher score may be a
useful way to quantify the information that $Y$ provides about $X$.
\end{rem}
\begin{defn}{\bf Fisher Information Matrix}
Let $X$ be a scalar random variable and $Y$ a vector random variable. Let $S$ be the Fisher score, i.e.,
\begin{align}
&S = f(Y,X)\\
&f(y,x) = \nabla_x \log p(y\given x)
\end{align}
The (Fisher) information  that $Y$ provides about $X$ when
$X$ takes the value $x$ is defined by the following matrix
\begin{align}
I_{Y|X}(x) = Cov[S \given x] 
\end{align}
\end{defn}
\begin{rem}{\bf Interpretation of the Scalar Fisher Information} 
The Fisher information $I_{Y|X}(x)$ gives an idea of how precisely we
can estimate $x$ based on $Y$.  It is useful to think of $I_{Y|X}(x)$
as a sensor sensitivity function. For example, think of $X$ as the
true weight of a person and $Y$ as the weight reading provided by a
weight sensor. The Fisher information $I_{Y|X}(x)$ tells us how
sensitive the sensor when measuring people of weight $x$.


The Fisher information also provides a useful heuristic to choose
sensor. If you have two sensor $Y_1$, $Y_2$ and you want to use one of
them to estimate $X$, then it is reasonable to choose the sensor based
on the Fisher information function. Note the problem is that in order
to make this choice you need to know the value of $x$ which is what
you are trying to estimate to begin with. However, if you are lucky
and $I_{Y_1\given X} (x) > I_{Y_2\given X}(x)$ for all values of $x$
of interest, then you have a reasonable criterion to choose $Y_1$ over
$Y_2$.
\end{rem}
\begin{rem}{\bf Interpretation of the  Fisher Information Matrix}
Shannon information theory provides a single number to describe the
information that a sensor variable $Y$ provides about a parameter
vector $X$. Thus, it is a bit surprising that the Fisher information
comes in the form of a matrix. One way to think about it is that the
Fisher information matrix contains all the data needed to bound the
precision of queries about functions of $X$ based on the data $Y$.
\end{rem}
\begin{rem}{\em Fisher Information and non Bayesian statistics}
The Fisher information and the CRLB play an important role in non
Bayesian approaches to statistics. In Bayesian approaches parameters
are treated as random variables with a known prior distribution. Thus
the mean efficiency of an estimator $T$ of the parameter $X$ is
defined with respect to a known prior distribution over the parameters
\begin{align}
E[(T - X)^2] = \int p(x) p(t \given x) (t -x)^2 dy dx
\end{align}
\end{rem}
Non Bayesian approaches treat the parameters as fixed, unknown constants,
not random variables. This creates a problem: since we don't
know $x$ it is not possible to know the mean quadratic efficiency of
the estimator $T$ of $x$, i.e.,
\begin{align}
E[(T-x)^2 \given x]
\end{align}
The CRLB tells us that for any unbiased estimator 
\begin{align}
E[(T-x)^2 \given x] = Var[T\given x] \geq I_{Y| X} (x)^{-1}
\end{align}
Thus, if we can show that for any $x$ the variance of $T$ equals
$I(x)$ then we know that $T$ is the best possible unbiased estimator
of the constant parameter $x$. 
\begin{exmp}{\em Sensor Sensitivity Function:}
Let the sensor $Y$ be defined as follows $Y = s(X) + c Z$ where $c$ is
a positive constant, and $Z$ is standard Gaussian noise. We can think
of the sensor as passing $X$ through some function and adding some
Gaussian noise. To find the sensor sensitivity function we get the
Fisher information that $Y$ provides about $X$
\begin{align}
\frac{\partial \log p(y|x)}{\partial x} = \frac{y -s(x)}{c^2}  \frac{\partial s(x)}{\partial x}\\
\end{align}
Note 
\begin{align}
 I_{Y|X}(x) =Var[\frac{Y -s(x)}{c^2} \frac{\partial s(x)}{\partial x}\given x] =
Var[cZ\given x] \left(\frac{\partial s(x)}{\partial x} \frac{1}{c^2}\right)^2
= \frac{1}{c^2}\left( \frac{\partial s(x)}{\partial x} \right)^2
\end{align}
To get the measure of sensitivity in the same units as $X$ we take the square root of the Fisher information,
\begin{align}
h(x) = \sqrt{I_{Y|X}(x)} = 
\left| \frac{\partial s(x)}{\partial x} \right|
  \frac{1}{c}
\end{align}
This suggest that the sensor is more sensitive in regions where $s(x)$
is steep (large derivative) and less sensitivy in regions where $s(x)$
is flat.  We will now explore a different argument for why $h(x)$ is a
reasonable measure of sensitivity. Let's assume $f$ is invertible and
let's have $\hat X$ be the maximum likelihood estimate of $X$, i.e,
\begin{align}
\hat X = \argmax_x (Y - s(x))^2 = s^{-1}(Y)
\end{align}
Let $x$ be a fixed value and $y= s(x)$. Using a Taylor expansion we get
\begin{align}
\hat X &\approx s^{-1}(y) + (Y-y) \frac{d s^{-1}(y)}{d y} \nonumber\\
&= x + (Y-y) \frac{d x}{d y} = x + (Y-y)\frac{1}{\frac{dy}{dx}}
\end{align}
Thus, to first order $\hat X$ is unbiased, i.e.
\begin{align}
E[\hat X\given x] \approx x + E[Y-y\given x] \frac{1}{\frac{dy}{dx}} = x
\end{align}
Moreover, to first order,  the variance of $\hat X$ given $x$ is as follows
\begin{align}
Var[\hat X \given x] \approx Var[Y \given x] \left(\frac{1}{\frac{dy}{dx}}\right)^2 = c^2 \left(\frac{1}{\frac{dy}{dx}}\right)^2  = h^2(x)
\end{align}
Thus, the sensitivity function $h$ has the following interpretation
\begin{align}
h(x) \approx \frac{1}{\sqrt{Var[\hat X\given x]}}
\end{align}
\end{exmp}

\begin{thm}{\bf Independent Variables}
Let $Y=(Y'_1, \cdots, Y'_n)'$ where the $Y_i$ vectors are
conditionally independent random variables given $X$ then
\begin{align}
I_{Y| X}(x) = \sum_{i=1}^n I_{Y_i |X}(x)
\end{align}
\begin{proof}
The score for $Y$ given $X$ is as follows
\begin{align}
S = \frac{\partial }{\partial x} \log p(Y\given x) 
\end{align}
and since the $Y_i$ variables are conditionally independent
\begin{align}
S= \frac{\partial }{\partial x} \sum_{i=1}^n \log p(Y_i\given x) = \sum_{i=1}^n
S_i
\end{align}
where $S_i$ is the score of $Y_i$ given $X$
\begin{align}
S_i = \frac{\partial }{\partial x} \log p(Y_i\given x) 
\end{align}
Note the $S_i$ variables are also conditoinally independent of given $x$. Thus
\begin{align}
I_{Y|X}(x) = Var[S\given x] = \sum_{i=1}^n Var[S_i \given x] = \sum_{i=1}^n I_{Y_i|X}(x)
\end{align}
\end{proof}
\end{thm}

\begin{thm}{\bf Gradient of the Score}
Let 
\begin{align}
&S = f(Y,X)\\
&D = g(Y,X)
\end{align}
where
\begin{align}
&f(x,y) = \nabla_x \log p(y\given x)\\
&g(x,y) = \nabla_x \nabla_x \log p(y\given x)
\end{align}
Then 
\begin{align}
I_{Y| X} (x) = Cov[S\given x] = - E[D  \given x] 
\end{align}
\begin{proof}
We will prove it for the scalar case
\begin{align}
I_{Y| X}(x) &= Var[V\given x] =  \int p(y \given x) \frac{\partial \log p(y\given x)}{\partial x} \frac{\partial \log p(y\given x)}{\partial x}dy\nonumber\\
&= \int \frac{\partial  p(y\given x)}{\partial x} 
\end{align}
Using integration by parts
\begin{align}
I_{Y| X}(x) & =  p(y\given x) \frac{\partial  \log p(y\given x)}{\partial x}\Big|_{-\infty}^{+\infty} - \int p(y\given x) \frac{\partial^2  \log p(y\given x)}{\partial x^2} dy\nonumber\\
&= \frac{\partial   p(y\given x)}{\partial x}\Big|_{-\infty}^{+\infty} - \int p(y\given x) \frac{\partial^2  \log p(y\given x)}{\partial x^2} dy
\end{align}
\end{proof}
\end{thm}
\begin{exmp}{\bf Estimation of the Variance}
Let $Y =(Y_1, \cdots Y_n)'$ where 
\begin{align}
Y_i= \mu+ \sqrt{X} Z_i,
\end{align}
$\mu$ is a known constant, $X$ an unknown parameter and $Z_1, \cdots,
Z_n$ are iid standard Gaussian random variable. Thus $X$ is the
variance of $Y_i$ and 
\begin{align}
\log p(y_i \given x) = \log \frac{1}{\sqrt{2 \pi  x}}  -\frac{1}{2}  \frac{(y_i-\mu)^2}{x}
\end{align} 
Note
\begin{align}
\frac{\partial \log p(y_i\given x)}{\partial x} =
  \frac{1}{2}\frac{ (y_i -\mu)^2}{x^2}  -\frac{1}{2x}
\end{align}
and
\begin{align}
\frac{\partial}{\partial x} \frac{\partial \log p(y_i\given x)}{\partial x} =
  -\frac{ (y_i -\mu)^2}{x^3}  +\frac{1}{2x^2}
\end{align}
Thus  the derivative of the Fisher score takes the following form
\begin{align}
D_i =  -\frac{ (Y_i -\mu)^2}{X^3}  +\frac{1}{2X^2}
\end{align}
Thus the Fisher information is as follows
\begin{align}
I_{Y_i\given X}(x) = - E[D_i\given x] = E[\frac{(Y_i-\mu)^2}{X^3}\given x] - E[\frac{1}{2X^2}\given x] = \frac{x}{x^3} +\frac{1}{x^2} = \frac{1}{2 x^2}
\end{align}
The total information provided by $Y$ is the sum of the information provided by each component of $Y$, thus
\begin{align}
I_{Y| X}(x) = \frac{n}{2 x^2}
\end{align}
Thus for any unbiased estimator $T$ of $X$
\begin{align}
Var[T\given x] \geq I^{-1}_{Y|X}(x) = \frac{2x^2}{n}
\end{align}
Lets analyze the following estimator
\begin{align}
T = \sum_{i=1}^n \frac{(Y_i - \mu)^2}{n}
\end{align}
Note
\begin{align}
Var[T\given x] =  \sum_{i=1}^n\frac{1}{n^2}  Var[ (\sqrt{X} Z_i)^2 \given x] 
= n \frac{1}{n^2} x^2 Var[Z_1^2]  = \frac{2 x^2}{n}
\end{align}
where we used the fact that since $Z_i$ is a standard Gaussian random
variable then $Var[Z^2_i] =2$. Thus $T$ reaches the CRLB, showing that
it is as good as it gets within the category of unbiased estimators of
$X$.  As we show now, this does not mean that $T$ is the best possible
estimator.  Let's now consider estimators of the form
\begin{align}
T  = k \sum_{i=1}^n (Y_i - \mu)^2
\end{align}
for some constant $k$. Note the unbiased estimator $T$ is a special
case with $k=1/n$. The bias function of $T$ is as follows
\begin{align}
b(x) = E[T\given x] -x = k n  x - x = x( nk-1)
\end{align}
In this case we are trying to estimate $X$, thus,  $g(x) = x$ and 
\begin{align}
&\nabla_x  g(x) =1\\
&\nabla_x b(x) = nk-1\\
&\nabla_x (b(x) +g(x)) = nk
\end{align}
Using the CRLB for biased estimators 
\begin{align}
E[(T- x)^2\given x] \geq (nk)^2 \frac{2x^2}{n} + x^2(nk-1)^2
= k^2   2 x^2 n + (nk-1)^2 x^2
\end{align}
We can now try and minimize the lower bound as a function of $k$. Taking he gradient with respect to $k$ and setting it to zero we get
\begin{align}
&4 k x^2 n +(nk-1)n  x^2= x^2n ( 4k +nk-1)=0 \\
&k(4+n) -1=0
\end{align}
Thus $k=1/(n+4)$ is optimal. For the class of estimators we examined, the estimator
\begin{align}
T = \frac{\sum_{i-1}^n (Y_i -\mu)^2}{n+4}
\end{align}
has the smallest lower bound. In this case 
\begin{align}
E[(T - x)^2\given x] \geq 2 x^2 \frac{n}{(n+4)^2} + x^2 (\frac{n}{n+4}  -1)^2 = 2 x^2 \frac{n+8}{(n+4)^2}
\end{align}
We can now check whether $T$ achieves the lower bound. 
\begin{align}
Var[ T \given x] = 
 \sum_{i=1}^n  Var[ (\frac{1}{n+4} \sqrt{X} Z_i)^2] = 2 x^2 \frac{n}{(n+4)^2} 
\end{align}
and
\begin{align}
E[(T - x)^2\given x] &= Var[T\given x] + b(x)^2\nonumber\\
&=
2 x^2 \frac{n}{(n+4)^2} + x^2 \frac{16}{(n+4)^2} = 2x^2 \frac{n+8}{(n+4)^2}
\end{align}
Thus $T$ achieves the lower bound. This means that $T$  is the most efficient
estimator  for the entire category of estimators with bias of the form
\begin{align}
b(x) = x (n k -1)
\end{align}
which includes the category of unbiased estimators. 

\end{exmp}

\section{Under Construction}

\subsection{Jeffrey Priors}
Jeffrey proposed a definition for what it means for a prior
distribution to be uninformative. He proposed that an uninformative
prior should maximize the influence of the likelihood function, i.e.,
it shall favor the values of $x$ for which the likelihood function
provides more information.

In addition he proposed that whatever criteria are used to recognize
whether a prior distribution is ``uninformative'' the criteria should
be invariant under re-parameterization: if the prior (marginal)
distribution for $X$ satisfies the criteria to be recognized as
``uninformative'' then the prior (marginal) distribution for $g(X)$
shall also satisfy those criteria, i.e., it should be recognized as
``uninformative''.

He proposed a distribution is uninformative if it takes the
following form
\begin{align}
p(x) \propto \sqrt{I_{Y|X}(x)}
\end{align}
We note that this prior maximizes the influence of the likelihood function, i.e., favors value of $x$ for which the likelihood function is more informative. Moreover if the distribution of $X$ is
uninformative then the distribution of $f(X)$ is also uninformative.
To see why let $U=f(X)$ and for a fixed $u$ let $u = f(x)$. Then
\begin{align}
I_{Y|X}(x) = I_{Y|U}(u) (\nabla_x f(x))^2
\end{align}
\begin{align}
p_X(x) \propto \sqrt{I_{Y|U}(u) (\nabla_x f(x))^2} = p_U(u) | \nabla_x f(x)|
\end{align}
\begin{rem}
The defnition of uninformativeness depends on the choice of the
likelihood function. 
\end{rem}

\subsection{Limitations of Fisher Information}
\paragraph{It is based on a lower bound}
\paragraph{Definition Restricts to information provided by unbiased estimators}
If using consistent estimator and $n$ is large, as typical for max likelihood, then this is ok. 

\paragraph{Local measure of sensitivity} 
See what happens in example if $f(x) = sin(2\pi k x)$ as $k$ increases and we sum over all $x$ it appears that we increase the information provided by the sensor.  Basically it was designed to be useful for the case in which we dont want to treat $X$ as a random variable but as a fixed parameter. In a way it appears as if this measure of sensitivity allows to use a different estimator for each value of $x$ which may be too generous. 
\paragraph{Tied up to Variance as a measure of efficiency}
\subsection{Links to Bayesian Approaches}

\subsection{Links to Shannon Information Theory}
Here we show that the Fisher information evaluated at $\theta$ is a local version of the KL divergence. The KL divergence $K(p,q)$ is somteimes called the KL information of $q$ about $p$. It can be seen as the information losst by using candidate model $q$ to encode the true world model $p$. 


Let $\theta$ be a parameter for a family of distributions. Then

\begin{align}
\mathcal{K}(\theta, \theta +\delta) \approx \frac{1}{2} \delta ' I(\theta) \delta
\end{align}
where $I(\theta)$ is the Fisher information matrix
\begin{align}
I(\theta) = -\int p(x\given \theta) \nabla^2_\theta \log p(x\given \theta) dx
\end{align}
\begin{proof}
\begin{align}
\mathcal{K}(\theta, \theta +\delta) = \int p(x\given \theta) \log p(x \given \theta) - \log p(x \given \theta +\delta) dx
\end{align}
where
\begin{align}
\log p(x \given \theta +\delta)  \approx \log p(x \given \theta) + \delta' \nabla_\theta \log p(x\given \theta) + \frac{1}{2} \delta'\nabla_\theta^2 \log p(x\given \theta) \delta
\end{align}
Thus
\begin{align}
\mathcal{K}(\theta, \theta +\delta) \approx &- \delta ' \int p(x\given \theta) \nabla_\theta \log p(x \given \theta)dx \nonumber\\
&- \frac{1}{2}\delta' \int p(x\given \theta) \nabla^2_\theta \log p(x\given \theta) dx \delta\nonumber\\
&= \frac{1}{2} \delta' I(\theta) \delta
\end{align}
\end{proof}
From this it follows that if $\theta$ is a scalar then 
\begin{align}
\lim_{\delta \to 0} \frac{1}{\delta^2} \mathcal{K}(\theta, \theta+\delta) = \frac{1}{2}I(\theta)
\end{align}
\section{Fisher Information and Shannon Mutual Information }
\mynote{Needs polishing but result is solid} 

\section{Problem Description}
Consider the problem of discriminating between two values of a
parameter vector based on observations from a random vector. The two
parameter vectors can be considered as hypotheses about the world and
the observation of the random vector as an experiment to decide which
of the two hypotheses is correct. Here we focus on analying whether
the experiment (the observation of the random vector) is expected to
provide useful information about the hypotheses of interest.


In Bayesian approaches this problem is approached by treating the
hypotheses as samples from a random vector and computing the expected
Shannon mutual information between the observations and the
hypotheses. A good experiment is one that is expected to provide a
large amount of information about the hypotheses of interest.
Classical statistical approaches do not treat parameters as random
variables and thus expected Shannon information gain cannot be used.
Instead classical approaches utilize the Fisher information, which is
known to determine a lower bound on the accuracy of unbiased
estimators of parameters.

Here we show an interesting relationship between the two approaches. 

\section{Problem Formalization}

Let $m_0,m_1 \in \R^p$ be the two parameter values we wish to
discriminate. We let $m_1 = m_0 + \alpha u$, where $\alpha$ is a
non-negative scalar representing the magnitude of the effect we wish
to disriminate, and $u$ a unit vector representing the direction of
the effect. 

Within the Bayesian framework we think of $m_0,m_1$  as
samples from a random vector $\mu$ with a probability mass function
that reflects our uncertainty prior to conducting the experiment
\begin{align}
p(\mu = m) = \begin{cases}
\pi &\text{if $m=  m_0$}\\
(1-\pi)  &\text{if $m= m_1$}\\
0&\text{else}
\end{cases} 
\end{align}

We quantify the expected usefulness of a random vector $X \in \R^n$ to
disambiguate the two hypotheses in terms of the mutual Shannon
information between $X$ and $\mu$. We condition on $u,\alpha$ so we
can keep track of how the mutual information behaves as a function of
these parameter
\begin{align}
I(X,\mu \given u,\alpha) &= H(\mu\given u,\alpha) - H(\mu\given X, u,\alpha) = \sum_{i=0}^1 \int p(x,\mu_i\given u,\alpha) \log \frac{1}{p(\mu_i\given u,\alpha)} dx \nonumber\\
&-  
\sum_{i=0}^1 \int p(x,\mu_i\given u,\alpha ) \log \frac{1}{p(\mu_i \given x,u,\alpha)} dx \nonumber\\
&= \sum_{i=0}^1 \int p(x,\mu_i\given u,\alpha) \log \frac{p(\mu_i\given u,\alpha) p(x\given \mu_i)}{p(\mu_i\given u,\alpha) p(x))} dx \nonumber\\
&= \sum_i \pi_i \int p(x\given \mu_i,u,\alpha) \log\frac{p(x\given \mu_i,u,\alpha)}{p(x\given u,\alpha)} dx
\end{align}
where
\begin{align}
p(x\given u,\alpha) = \pi_0 p(x\given m_0) + \pi_1 p(x\given m_1)
\end{align}
where $m_1= m_0+u \alpha$.  In classical statistical approaches we
measure the expected usefulness of $X$ based on the Fisher information
matrix
\begin{align}
F_X(m) = \E[\nabla_m \log p(X\given m) (\nabla_m \log p(X\given m))']= -  \E[\nabla^2_m \log p(X\given m)]
\end{align}
The efficiency unbiased estimator $h(X)$ of the parameter $\mu$ based on
$X$ is bound by $F_X(m)$. In particular for any  vector $v\in R^p$
\begin{align}
\Var[v'h(X) \given m] \geq v' F_X(m) 
\end{align}
The trace, determinant, or largest eigenvector of $F_X(m)$ are
typically used, to choose observation variables that are expected to be efficient for estimating the parameter of interest. 



\section{Analysis}
Consider $\log p(x)$ a function of $m_1$ and
approximate it using a second order Taylor expansion about
$m_0$
\begin{align}
f(m_1) &= \log (\pi_0 p(x\given m_0) + \pi_1 p(x\given m_1) )\nonumber\\
& =  f(m_0) + \delta' \nabla_{m_1} \left.f(m_1)\right|_{m_1=m_0} + \left.\delta' \nabla^2_{m_1} f(m_1) \delta \right|_{m_1=m_0} + \mathcal{O}(\alpha^3)
\end{align}
where $\delta = m_1-m_0 = u \alpha$. Note
\begin{align}
\nabla_{m_1} f(m_1) = ( \nabla_{m_1} \log p(x\given m_1) )\frac{ \pi_1 p(x\given m_1)}{\pi_0 p(x\given m_0)+\pi_1 p(x\given m_1)} 
\end{align}
Thus
\begin{align}
\left.\nabla_{m_1} f(m_1)\right|_{m_1=m_0}  = (  \left.\nabla_{m_1} \log p(x\given m_1) \right|_{m_1=m_0})\pi_1 = 
(\nabla_{m_0} \log p(x\given m_0))\pi_1
\end{align}
Moreover
\begin{align}
\nabla^2_{m_1} f(m_1) &= \nabla_{m_1} \Big((\nabla_{m_1} \log p(x\given m_1))  \frac{\pi_1 p(x\given m_1)}{\pi_0 p(x\given m_0)+\pi_1 p(x\given m_1)}\Big)\nonumber\\
&=  \frac{\pi_1 p(x\given m_1)}{\pi_0 p(x\given m_0)+\pi_1 p(x\given m_1)}\nabla^2_{m_1} \log p(x\given m_1) \nonumber\\
&+(\nabla_{\mu} \frac{\pi_1 p(x\given m_1)}{\pi_0 p(x\given m_0)+\pi_1 p(x\given m_1)}) (\nabla_{m_1} \log p(x\given m_1) )'\nonumber\\
\end{align}
where
\begin{align}
\nabla_{\mu} \frac{\pi_1 p(x\given m_1)}{\pi_0 p(x\given m_0)+\pi_1 p(x\given m_1)}
&= \nabla_{m_1} \Big(1+ \frac{\pi_o}{\pi_1} \frac{p(x\given m_0)}{p(x\given m_1)} \Big)^{-1} \nonumber\\
&=\Big(\nabla_{m_1} \log p(x\given m_1) \Big)  \frac{\pi_o}{\pi_1} \frac{p(x\given m_0)}{p(x\given m_1)} \Big(1+ \frac{\pi_o}{\pi_1} \frac{p(x\given m_0)}{p(x\given m_1)} \Big)^{-2}
\end{align}
Thus
\begin{align}
\left. \nabla^2_{m_1} f(m_1) \right|_{m_1=m_0}&=  \pi_1 \nabla^2_{m_1} \log p(x\given m_0) \nonumber\\
&+ \nabla_{m_0} \log p(x\given m_0) 
\frac{\pi_o}{\pi_1} \Big(1+ \frac{\pi_o}{\pi_1} \Big)^{-2} (\nabla_{m_0} \log p(x\given m_0) )'\nonumber\\
&=\pi_1 \nabla^2_{m_0} \log p(x\given m_0)
+ \pi_0\pi_1(\nabla_{m_0} \log p(x\given m_0) )(\nabla_{m_0} \log p(x\given mu_0) )'
\end{align}
and
\begin{align}
\int p(x \given m_0) \log \frac{p(x\given m_0)}{p(x)} dx &= 
 \int p(x \given m_0) \log p(x\given m_0) dx 
-  \int p(x m_0) \log p(x) dx \nonumber\\
&= \int p(x\given m_0) \pi_1 \delta' \nabla_{m_0} \log p(x\given m_0)dx \nonumber\\
&-\int p(x\given m_0) \pi_1 \delta' \nabla_{m_0} \log p(x\given m_0)dx \nonumber\\
&- \pi_1 \frac{1}{2} \delta' \int p(x\given m_0)  \nabla^2_{m_0} \log p(x\given m_0)dx \delta\nonumber\\
&-\pi_0\pi_1 \frac{1}{2} \delta' \int p(x\given m_0) ( \nabla_{m_0} \log p(x\given m_0))(\nabla_{m_0} \log p(x\given m_0))'dx \delta +\mathcal{O}(\alpha^3)\nonumber\\
&= \frac{1}{2} \delta'\Big(  \pi_1 F(m_0) -\pi_0\pi_1 F(m_0)\Big) \delta +\mathcal{O}(\alpha^3)\nonumber\\
&= \frac{1}{2} \pi_1^2  \delta' F_X(m_0)\delta +\mathcal{O}(\alpha^3)
\end{align}
$F_X(m_0)$ is the Fisher information matrix evaluated at $m_0$. Using analogous argument, 
\begin{align}
\int p(x \given m_1) \log \frac{p(x\given m_1)}{p(x)} dx 
\frac{1}{2} \pi_0^2  \delta' F(m_1)\delta +\mathcal{O}(\alpha^3) 
\end{align}
Thus 
\begin{align}
I(X,\mu\given u,\alpha) &=  \frac{1}{2} \pi_0 \pi_1^2  \delta' F(m_0 )\delta
+ 
\frac{1}{2} \pi_1 \pi_0^2  \delta' F(m_1 )\delta \nonumber\\
&\approx \frac{1}{2} (\pi_0 \pi_1^2 + \pi_1 \pi_0^2) \delta' F(m_0) \delta +\mathcal{O}(\alpha^3) \nonumber\\
&= \frac{1}{2} \pi_0 \pi_1 \delta' F(m_0) \delta + \mathcal{O}(\alpha^3)
\end{align}
Note the uncertainty (variance) about the magnitude of the effect parameter prior to conducting is as follows
\begin{align}
\Var\Big[\;|\mu|\;  \Big|\; u, \alpha\Big] = \alpha^2 \pi_0 \pi_1 
\end{align}
 Thus
\begin{align}
\lim_{\alpha^2\to 0} I(X,\mu\given u, \alpha) = \frac{1}{2} 
\left( \frac{\partial}{\partial \alpha^2} 
\Var\Big[\;|\mu|\;  \Big|\; u, \alpha\Big]
\right)\;  u' F(m_0) u
\end{align}
\paragraph{Interpretation}
The Fisher information matrix provides a second order approximation to the Shannon Mutual Information between 
\end{document}

