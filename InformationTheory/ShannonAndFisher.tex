\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}
\usepackage{listings}
\usepackage{boxedminipage}

 \title{A relationship between Shannon Mutual Information and Fisher Information}
\author{Copyright \copyright Javier R. Movellan}



\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\R}{\mathcal{R}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}





\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}


\theoremstyle{plain}% default
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem*{cor}{Corollary}

\theoremstyle{definition}
\newtheorem{defn}{Definition}[section]
\newtheorem{conj}{Conjecture}[section]
\newtheorem{exmp}{Example}[section]

\theoremstyle{remark}
\newtheorem*{rem}{Remark}
\newtheorem*{note}{Note}
\newtheorem{case}{Case}







\begin{document}
\maketitle



    
\newpage

\section{Problem Description}
Consider the problem of discriminating between two values of a
parameter vector based on observations from a random vector. The two
parameter values are hypotheses about the world and the observation of
the random vector is as an experiment to decide which of the two
hypotheses are correct.  


We examine two popular approaches to evaluate whether the observation
vector is adequate to test the hypotheses of interest.  In Bayesian
approaches this problem is approached using the expected Shannon
mutual information between the observations and the hypotheses. A good
observation vector is one that is expected to provide large
information about the hypotheses of interest.  Classical statistical
approaches do not treat parameters as random variables and thus
expected Shannon information gain cannot be used.  Instead classical
approaches utilize the Fisher information matrix, which determines a
lower bound on the accuracy of unbiased estimators of parameters. Here
we show an interesting relationship between the two approaches.

\section{Problem Formalization}

Let $m_0,m_1 \in \R^p$ be the two parameter values we wish to
discriminate. We let  $m_1 = m_0 + \alpha u$, where $\alpha$ is a
non-negative scalar representing the magnitude of the effect we wish
to discriminate, and $u$ a unit vector representing the direction of
the effect. 

Within the Bayesian framework we think of $m_0,m_1$  as
samples from a random vector $\mu$ with a probability mass function
that reflects our uncertainty prior to conducting the experiment
\begin{align}
p(\mu = m) = \begin{cases}
\pi_0, &\text{if $m=  m_0$}\\
\pi_1,  &\text{if $m= m_1$}\\
0,&\text{else}
\end{cases} 
\end{align}
In Bayesian approaches the value of $X$ to learn about the hypotheses
of interest is measured by the mutual Shannon information between
between $X$ and $\mu$, which is defined as follows:
\begin{align}
I(X,\mu \given u,\alpha) &= H(\mu\given u,\alpha) - H(\mu\given X, u,\alpha) = \sum_{i=0}^1 \int p(x,\mu_i\given u,\alpha) \log \frac{1}{p(\mu_i\given u,\alpha)} dx \nonumber\\
&-  
\sum_{i=0}^1 \int p(x,\mu_i\given u,\alpha ) \log \frac{1}{p(\mu_i \given x,u,\alpha)} dx \nonumber\\
&= \sum_{i=0}^1 \int p(x,\mu_i\given u,\alpha) \log \frac{p(\mu_i\given u,\alpha) p(x\given \mu_i)}{p(\mu_i\given u,\alpha) p(x)} dx \nonumber\\
&= \sum_i \pi_i \int p(x\given \mu_i,u,\alpha) \log\frac{p(x\given \mu_i,u,\alpha)}{p(x\given u,\alpha)} dx
\end{align}
where
\begin{align}
p(x\given u,\alpha) = \pi_0 p(x\given m_0) + \pi_1 p(x\given m_1)
\end{align}
In classical statistical approaches the value value of $X$ to learn
about the hypotheses is based on the Fisher information matrix
\begin{align}
F_X(m) = \E[\nabla_m \log p(X\given m) (\nabla_m \log p(X\given m))']= -  \E[\nabla^2_m \log p(X\given m)]
\end{align}
The logic for using the Fisher information matrix, is that the
efficiency of any umbiased estimate of the parameter $m$ is bound by
$F_X(m)$. In particular if $m$ is the true parameter vector, and $h(X)$ is an estimator of $X$, such that $\E[h(X)\given m]=m$ then for any vector $v\in R^p$
\begin{align}
\Var[v'h(X) \given m] \geq v' F_X(m) 
\end{align}
The trace, determinant, or largest eigenvector of $F_X(m)$ are
typically used, to choose observation variables that are expected to be efficient for estimating the parameter of interest. 

\section{On the  Relationship Between the Bayesian and Classical Approaches}
Consider $\log p(x)$ a function of $m_1$ and
approximate it using a second order Taylor expansion about
$m_0$
\begin{align}
f(m_1) &= \log (\pi_0 p(x\given m_0) + \pi_1 p(x\given m_1) )\nonumber\\
& =  f(m_0) + \alpha u' \nabla_{m_1} \left.f(m_1)\right|_{m_1=m_0} + \frac{1}{2}\alpha^2  \left.u' \nabla^2_{m_1} f(m_1) u \right|_{m_1=m_0} + \mathcal{O}(\alpha^3)
\end{align}
Note
\begin{align}
\nabla_{m_1} f(m_1) = ( \nabla_{m_1} \log p(x\given m_1) )\frac{ \pi_1 p(x\given m_1)}{\pi_0 p(x\given m_0)+\pi_1 p(x\given m_1)} 
\end{align}
Thus
\begin{align}
\left.\nabla_{m_1} f(m_1)\right|_{m_1=m_0}  = (  \left.\nabla_{m_1} \log p(x\given m_1) \right|_{m_1=m_0})\pi_1 = 
(\nabla_{m_0} \log p(x\given m_0))\pi_1
\end{align}
Moreover
\begin{align}
\nabla^2_{m_1} f(m_1) &= \nabla_{m_1} \Big((\nabla_{m_1} \log p(x\given m_1))  \frac{\pi_1 p(x\given m_1)}{\pi_0 p(x\given m_0)+\pi_1 p(x\given m_1)}\Big)\nonumber\\
&=  \frac{\pi_1 p(x\given m_1)}{\pi_0 p(x\given m_0)+\pi_1 p(x\given m_1)}\nabla^2_{m_1} \log p(x\given m_1) \nonumber\\
&+(\nabla_{\mu} \frac{\pi_1 p(x\given m_1)}{\pi_0 p(x\given m_0)+\pi_1 p(x\given m_1)}) (\nabla_{m_1} \log p(x\given m_1) )'\nonumber\\
\end{align}
where
\begin{align}
\nabla_{\mu} \frac{\pi_1 p(x\given m_1)}{\pi_0 p(x\given m_0)+\pi_1 p(x\given m_1)}
&= \nabla_{m_1} \Big(1+ \frac{\pi_o}{\pi_1} \frac{p(x\given m_0)}{p(x\given m_1)} \Big)^{-1} \nonumber\\
&=\Big(\nabla_{m_1} \log p(x\given m_1) \Big)  \frac{\pi_o}{\pi_1} \frac{p(x\given m_0)}{p(x\given m_1)} \Big(1+ \frac{\pi_o}{\pi_1} \frac{p(x\given m_0)}{p(x\given m_1)} \Big)^{-2}
\end{align}
Thus
\begin{align}
\left. \nabla^2_{m_1} f(m_1) \right|_{m_1=m_0}&=  \pi_1 \nabla^2_{m_1} \log p(x\given m_0) \nonumber\\
&+ \nabla_{m_0} \log p(x\given m_0) 
\frac{\pi_o}{\pi_1} \Big(1+ \frac{\pi_o}{\pi_1} \Big)^{-2} (\nabla_{m_0} \log p(x\given m_0) )'\nonumber\\
&=\pi_1 \nabla^2_{m_0} \log p(x\given m_0)
+ \pi_0\pi_1(\nabla_{m_0} \log p(x\given m_0) )(\nabla_{m_0} \log p(x\given \mu_0) )'
\end{align}
and
\begin{align}
\int p(x \given m_0) \log \frac{p(x\given m_0)}{p(x)} dx &= 
 \int p(x \given m_0) \log p(x\given m_0) dx 
-  \int p(x \given m_0) \log p(x) dx \nonumber\\
&- \pi_1  \alpha u' \int p(x\given m_0) \ \nabla_{m_0} \log p(x\given m_0)dx \nonumber\\
&- \frac{1}{2} \pi_1 \alpha^2 u' \Big(  \int p(x\given m_0)  \nabla^2_{m_0} \log p(x\given m_0)dx \Big) u \nonumber\\
&-\frac{1}{2} \pi_0\pi_1 \alpha^2 u' \Big(   \int p(x\given m_0) ( \nabla_{m_0} \log p(x\given m_0))(\nabla_{m_0} \log p(x\given m_0))'dx\Big) u +\mathcal{O}(\alpha^3)\nonumber\\
&= \frac{1}{2} \alpha^2 u'\Big(  \pi_1 F(m_0) -\pi_0\pi_1 F(m_0)\Big) u +\mathcal{O}(\alpha^3)\nonumber\\
&= \frac{1}{2} \pi_1^2  \alpha^2  u' F_X(m_0) u +\mathcal{O}(\alpha^3)
\end{align}
Using an analogous argument, 
\begin{align}
\int p(x \given m_1) \log \frac{p(x\given m_1)}{p(x)} dx = 
\frac{1}{2} \pi_0^2 \alpha^2  u' F(m_1) u +\mathcal{O}(\alpha^3) 
\end{align}
Thus 
\begin{align}
I(X,\mu\given u,\alpha) &=  \frac{1}{2} \pi_0 \pi_1^2  \alpha^2 u' F(m_0 )u 
+ 
\frac{1}{2} \pi_1 \pi_0^2 \alpha^2  u' F(m_1 ) u + \mathcal{O}(\alpha^3) \nonumber\\
&= \frac{1}{2} \alpha^2 \pi_0 \pi_1 u' \Big( \pi_0 F(m_1) + \pi_1 F(m_0)\Big) u 
 +\mathcal{O}(\alpha^3) 
\end{align}
Note the uncertainty (variance) about the magnitude of the effect parameter prior to observing $X$ is as follows 
\begin{align}
\Var\Big[\;|\mu|\;  \Big|\; u, \alpha\Big] = \alpha^2 \pi_0 \pi_1 
\end{align}
 Thus
\begin{align}
\lim_{\alpha^2\to 0} I(X,\mu\given u, \alpha) = \frac{1}{2} 
\left( \frac{\partial}{\partial \alpha^2} 
\Var\Big[\;|\mu|\;  \Big|\; u, \alpha\Big]
\right)\;  u' F(m_0) u
\end{align}
\subsection{Interpretation}
The expected Shannon Information Gain is quadratically approximated by the Fisher information evaluated at the null hypothesis.  Under such approximation: 
\begin{itemize}
\item  For a fixed sensor variable $X$ the
optimal query is in the direction of the first principal component of the Fisher information matrix.
\item For a fixed query, the optimal sensor variable $X$ maximizes the Fisher information distance between the null and alternative hypotheses.

\item The uncertainty of the prior distribution, provided is not zero, has no effect on the previous results, it simply scales the expected information gain.  

\end{itemize}
\end{document}

