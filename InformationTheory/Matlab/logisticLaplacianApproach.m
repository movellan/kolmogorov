% Goal is to maximize L(y) = h(y' theta) y'c y
% as a function of y  subject to constraint that -1< y_i<1 for all _i
% where c is positive semidefinite
% h(y) = l(y' theta) (1-l(y'theta)), for a fixed vector theta 
% and l is the logistic funtion


% To satisfy the constraint we parameterize L as a function of x where 
% y_i = tanh(x_i) 

%rand('seed',2);
%randn('seed',2)

clear all
clf


l = @(z) 1./ (1+exp(-1*z)); %logisitc

dl = @(z) (z).*(1-z);  % derivative of logistic




p = 10;

c = eye(p);

x = randn(p,1);
y = zeros(p,1);
theta = sign(randn(p,1));

epsilon = 1/length(x); % step size for gradient ascent
epsilon = 1;

lambda = 0.01; % L2 regularization

T= 1; 
for t=1:T
  
y = tanh(x);
u = theta'*y; 
v = l(u);
h = dl(v); % The part that likes for y to be orthogonal to x
q = y'*c*y; % The quadratic part
L(t) = h*q;
L1(t) = h;
L2(t) = q;
% Derivative of h
dh = h * (1 -2 *v);

dy = 1 - y.^2; % derivative of hyperbolic tangent
g1 = q*dh*(theta.*dy); % This part of the gradient wants for y to  become
                          % orthogonal with theta

ydy = y.*dy;
g2 = 2*h*c*(ydy); % This part of the gradient wants for y to max the
                    % quadratic part of L

g3 =  lambda*(1 - y'*y)*ydy;

if(t< T/2)
  k = 1;
else 
  k=1;
end

g = g1 +k*g2 +g3; % the gradient (first we focus on being orthogonal to theta)

%h = g*g' ;
keyboard
h = h + p*diag(diag(h));


x = x + epsilon*inv(h)*g; % Do gradient ascent.

end
subplot(3,1,1)
plot(L1)
subplot(3,1,2)
plot(L2)
subplot(3,1,3)
plot(L)
y














