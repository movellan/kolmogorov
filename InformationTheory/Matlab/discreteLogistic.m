clear all
close all

bins = -30:30; %discrete bins for theta
xbins = -30:30; % opt x search space

nbins = length(bins);
pTheta = normpdf(bins, 0, 3); %prior of theta
pTheta = pTheta/sum(pTheta);
realTheta = -10;  %real theta

logit = @(z) 1./ (1+exp(-1*z));

accpTheta = [pTheta];
accxy = [];
for iter=1:50
    
    %argmax
    minEnt = inf;
    optx = -1;
    opt_pTheta_xY = [];
    
    ent=[]
    for x=xbins
        yhat = logit(x-bins);
        
        % p(Theta Y |x)
        pThetaY_x = [(1-yhat) .* pTheta; % y=0
                    yhat .* pTheta];     % y=1
        pThetaY_x = pThetaY_x*(1-2*eps) + eps;
        
        % p(Theta| x Y)
        pTheta_xY = pThetaY_x ./ repmat(sum(pThetaY_x,2), [1,nbins]); 
        
        % H[theta|x Y]
        H_xY = sum(-pTheta_xY.* log(pTheta_xY),2);
        
        % p(Y|x)
        pY_x = sum(pThetaY_x, 2);
        
        % H[theta|x]
        H = sum(pY_x .* H_xY);
        
        ent=[ent H];
        
        %find minimum entropy, and store the posterior of Theta
        if H<minEnt
            minEnt =H;
            optx = x;
            opt_pTheta_xY = pTheta_xY;
        end
    end
pause

    
    %inspect
    figure(1)
    subplot(2,1,1)
    plot(bins,pTheta)
    subplot(2,1,2);
    plot(xbins, -ent,'r+');
    suptitle(sprintf('iter=%d',iter));
    drawnow
    pause(0.1)
    
    %perform optimal probing experiment
    py1 = logit(optx-realTheta)
    y = rand(1) < py1;

    
    %update the posterior
    pTheta = opt_pTheta_xY(y+1,:);
    
    %data collections
    accxy = [accxy;[optx y]];
    accpTheta = [accpTheta;pTheta];

end

%iterations on one figure
figure(2)
imagesc(bins,[1,iter],accpTheta);
hold on
pidx = find(accxy(:,2)==1);
plot(accxy(pidx,1),pidx,'w+')
nidx = find(accxy(:,2)==0);
plot(accxy(nidx,1),nidx,'wo')
xlabel('x');
ylabel('iter');
hold off
%print -dpng bedLogisticDelta.png