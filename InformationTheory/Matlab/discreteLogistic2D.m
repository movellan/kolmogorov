% Discretized approximation to logistic infomax for 2D case
% Model is p(Y=1| x, theta) = logistic( x'*theta);
% Javier R. Movellan, 2009

clear all
clf
logi = @(z) 1./ (1+exp(-1*z));

muTheta = [1 1   ]'; % The mean of the prior distribution

sigmaTheta = 0.1*eye(2); % The covariance of the prior distribution


l = 3; % -l is the min and l is the max for the sampled values of theta
dx = 0.3; % the interval between sampled values


[Theta1 Theta2] = meshgrid(-l:dx:l, -l:dx:l);
muTheta1 = repmat(muTheta(1), size(Theta1));
muTheta2 = repmat(muTheta(2), size(Theta2));

%p(theta). Construct a prior distribution. Here we use a Gaussian Prior
pTheta = exp(- 0.5*(Theta1- muTheta1).^2/ sigmaTheta(1,1)   -  0.5*(Theta2- ...
                                                  muTheta2).^2/ sigmaTheta(2,2));

pTheta = pTheta/sum(sum(pTheta));

% prior Entropy
H  = -sum(sum( pTheta.*log(pTheta)));


k1=0;
for t1 = -l:dx:l
  k2 =0;
  k1 = k1+1;
  for t2 = -l:dx:l
    k2=k2+1;
    
    x = [ t1 t2   ]';

    
    net = Theta1*x(1) + Theta2*x(2); 
    yHat = logi(net);

    
    
    
    %P0 = p(  theta | x, Y=0 )
    P0 = (1-yHat)*(1-2*eps) + eps;% likelihood
    P0 = P0.*pTheta; % likelihood time prior
                     % pY0 = p(Y=0)
    pY0 = sum(sum(P0));  % marginalize to get 
    P0 = P0/pY0;
    
    %p(  theta | x, Y=1 )
    P1 = yHat *(1-2*eps) + eps;
    P1 = P1.*pTheta;
    % pY1 = p(Y=1)
    pY1 = sum(sum(P1));
    P1 = P1/pY1;
    

    %H(theta | x, Y=0)
    H0 = -sum(sum(P0.*log(P0)));
    %H(theta | x, Y=1)
    H1 = -sum(sum(P1.*log(P1)));
    % H(theta | x Y)
    
    HY = H0*pY0 + H1*pY1; 
    
    %Information Gain
    I(k1,k2) = H - HY ;
    
    % Uncomment below if you want to constrain the solution to have a norm
    % of 2. 
    
    % if(norm(x) > 2) I(k1,k2) =0; end
    

    
    
  end
end

surfc(Theta1, Theta2, I)
xlabel('\theta_2')
ylabel('\theta_1')
zlabel('Information Gain')


