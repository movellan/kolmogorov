% plot the loss function minimized using: (1)  the riemannian metric
% (squared angle difference), (2) the froebinius norm of matrix
% differences  ( cos angle)
% and (3) the euclidean norm of quaternion differences (cos angle/2)
% 


x=[-pi:0.01:pi];
k=1:length(x)
y1= 4*(1-cos(x));% matrix distance 
y2=2*(1-cos(x/2)); % quaternion distance 
y3= x.^2; %Riemannian distance
y1=y1/2;
y2=y2*4; 
x=x*360/2/pi;
plot(x,y1,'-.',x,y2,'--',x,y3)
xlabel('Angle (Degrees)')
set(gca,'FontSize',16)
set( gca,'DefaultAxesFontSize',16,'DefaultTextFontSize',18)     
set( gca,'DefaultAxesFontName','Times','DefaultTextFontName','Times')
set( gca,'DefaultAxesLineWidth', 2, 'DefaultLineLineWidth', 2);
set( gca,'DefaultLineMarkerSize',2);
