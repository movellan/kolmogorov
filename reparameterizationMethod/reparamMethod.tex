\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}
\usepackage{bbm}



\newcommand\argmin{\operatornamewithlimits{argmin}}

\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand\given{\medspace|\medspace}
\newcommand\argmax{\operatornamewithlimits{argmax}}


 \title{The Reparameterization Method}
\author{ Javier R. Movellan\\ 7/23/2022}





\newenvironment{namelist}[1]{%
\begin{list}{}
	{ 		\let\makelabel\namelistlabel
\skettowidth{\labelwidth}{#1}
\setlength{\leftmargin}{1.1\labelwidth} 	} }{%
\end{list}}

\empty

% Replace with the directory where the graphs have been placed
\newcommand{\graphdir}{./graphs}
\newcommand{\centerfig}[2]{\centerline{\psfig{figure=#1,width=#2}}}




\begin{document}
\maketitle






\newpage

Let $X$ be a random variable and $Y= f_\theta(X)$. We want to minimize a loss function of the following form using stochastic gradient descent
\begin{align}
L(\theta) = \int p(y\given \theta) h(y) dy
\end{align}
\paragraph{Method 1: Probability Gradient}
This method, also known as the Score method  is popular in policy gradient approaches to Reinforcement Learning. Machine literatures utilizes the gradient of the probability distribution
\begin{align}
\nabla_\theta p(y\given \theta) = p(y\given \theta) \nabla_\theta \log p(y\given \theta) 
\end{align}
Thus
\begin{align}
G(\theta) = \nabla_\theta L(\theta) = \int p(y\given \theta) h(y) \nabla_\theta \log p(y\given \theta) dy
\end{align} 
We can get estimates of the gradient by sampling from $Y$ with a fixed value of $\theta$
\begin{align}
\hat G_1(\theta) = h(Y) \nabla_\theta \log p (Y\given \theta)
\end{align}
\paragraph{Example:}
Let $X\sim N(0,1)$,  a zero mean, unit variance Gaussian random variable, $Y= f_\theta(X) = X + \theta$ and $h(y) = y^2$. In this case
\begin{align}
\nabla_\theta \log p(y\given \theta) = \nabla_\theta \log \frac{1}{\sqrt{2\pi}} e^{-\frac{1}{2}(y - \theta)^2}
= y -\theta
\end{align}
Thus    
\begin{align}
G(\theta) &= \nabla_\theta L(\theta) = \int p(y\given \theta) y^2 (y-\theta)  dy = E[Y^2 (Y-\theta)\given \theta] = E[(X+\theta)^2 X] \\\nonumber
&= E[X^3 ]+ 2 \theta E[X^2] + \theta^2 E[X] = 2 \theta
\end{align}
The stochastic estimate of the gradient takes the following form
\begin{align}
G_1(\theta) = (Y)^2 (Y-\theta) = (X+\theta)^2 X 
\end{align}
Note the expected value of the estimate is the actual gradient
\begin{align} 
E[G_1(\theta)\given \theta] = E[(X+\theta)^2 X] = 2 \theta
\end{align}
Moreover, see the Appendix, 
\begin{align}
Var[G_1(\theta)\given \theta] =Var[X(X+\theta)^2] =  15+ 18\theta^2 +\theta^4
\end{align}

\paragraph{Method 2: Reparameterization Method}
This method is also know as the Pathwise Derivative method. Using the change of variables property of the expected values (see Appendix) we get: 
\begin{align}
L(\theta) = \int p(y\given \theta) h(y) dy = \int p(x) h(f_\theta(x)) dx 
\end{align}
Taking gradients with respect to $\theta$
\begin{align}
G(\theta) = \nabla_\theta L(\theta) = \int p(x)  \dot f_\theta(x) \dot h(f_\theta(x))    dx
\end{align}
where
\begin{align}
\dot f_\theta(x) &= \nabla_\theta f_\theta(x) \\
\dot h(y) &= \nabla_y h(y)
\end{align}
This suggests the following stochastic estimate of the gradient
\begin{align}
\hat G_2(\theta) = \dot f(X) \dot h(f_\theta(X)) 
\end{align}
This approach is sometimes called "The reparametization Trick", \cite{vae}. We call $\hat G_2(\theta)$  the reparameterized stochastic estimate of the gradient. In many cases  $\hat G_2(\theta)$ has lower variance than $\hat G_1(\theta)$ and thus it is preferable. 
\paragraph{Example}
In the example above 
\begin{align}
&Y= f_\theta(X) = X+ \theta\\
&\dot f_\theta(x) = \nabla_\theta (X+\theta) =1\\
&h(y) = y^2\\
&\dot h(y) = \nabla_y y^2 = 2 y
\end{align}
Thus
\begin{align}
G(\theta) = \nabla_\theta L(\theta) = \int p(x) 2(X+\theta)dx = 2\theta
\end{align}
 and the reparameterized stochastic estimate of the gradient would be as follows:
\begin{align}
\hat G_2(\theta) = 2( X+\theta)
\end{align}
We confirm that  the estimate is unbiased
\begin{align}
E[\hat G_2(\theta) \given \theta]= 2( E[X] +\theta) = 2 \theta
\end{align}
and its variance is quite smaller than the variance of $\hat G_1(\theta)$
\begin{align}
Var[\hat G_2(\theta)\given \theta]  = 4 Var[X] = 4 < Var[\hat G_1(\theta)] = 15 + 14 \theta^2 +\theta^4
\end{align}
\bibliographystyle{apalike} % plainnat
\bibliography{reparam}

\section{Appendix}
\begin{align}
Var[G_1(\theta)\given \theta] =Var[X(X+\theta)^2] =  E[X^2 (X+\theta)^4] - E[X (X+\theta)]^2 
\end{align}
Using the fact that 
\begin{align}
(a+b)^4 = a^4 + 4 a^3 b + 6 a^2b^2 + 4 ab^3 + b^4
\end{align}
We get that 
\begin{align}
E[X^2 (X+\theta)^4] &= E[X^6]+ 4 \theta E[X^5] + 6 \theta^2 E[X^4] + 4 \theta^3 E[X^3] + \theta^4 E[X^2]
\end{align}
And since $X$ is a zero mean, unit variance Gaussian random variable
\begin{align}
E[X^n]= \begin{cases}
0,&\text{for $n$ odd}\\
\frac{n!}{(n/2)!\; 2^{n/2}},&\;\text{for $n$ even}
\end{cases} 
\end{align}
then
\begin{align}
E[X^2 (X+\theta)^4] 
= 15+ 18\theta^2 +\theta^4
\end{align}
Moreover
\begin{align}
E[X(X+\theta)^2] = E[X^3] + 2 \theta E[X^2] + E[X] \theta^2 =2 \theta 
\end{align}
Thus
\begin{align}
Var[G_1(\theta)] &=Var[X(X+\theta)^2] =  E[X^2 (X+\theta)^4] - E[X (X+\theta)]^2  \nonumber\\
&= 15+ 14 \theta^2 + \theta^4
\end{align}
\section{Appendix: Change of Variables Property of Expected Values}
Let $Y= h(X)$ then
\begin{align}
E[Y]=\int p(y) y dy = \int p(x) h(x) dx
\end{align}
\end{document}
