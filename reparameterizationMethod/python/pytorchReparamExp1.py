import torch
from torch.distributions import Normal, Gamma
from torch.autograd import Variable

import numpy as np
import matplotlib.pyplot as plt

# verify torch.distribution.rsample uses the reparameterization method
# for computing the gradient

theta = Variable(torch.ones(1)*100,requires_grad=True)


batchsize=1
optimizer = torch.optim.SGD([theta], lr=0.01/batchsize, momentum=0.0)
nsteps=500
thetaList=[]
for k in range(nsteps):
    thetaList.append(float(theta))
    y = torch.distributions.Normal(theta, 1).rsample([batchsize]).flatten()
    x = y-theta
    # theoretical gradient for reparameterization method
    theoryGrad = 2*(x+theta)
    loss = y.dot(y)
    optimizer.zero_grad()
    loss.backward()
    print('step',k,'theta',float(theta),'pytorchGrad',float(theta.grad.sum()),'theoryGrad',float(theoryGrad.sum()))
    optimizer.step()

plt.plot(thetaList)
plt.plot([0,nsteps],[0,0],'k--')
plt.show()
