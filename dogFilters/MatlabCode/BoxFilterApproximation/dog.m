% 2Dimensional  Difference of Gaussian function. 
% s1 is  stnd dev of center area, s2 is stnd dev of surroound area
% g is gain of surround

function z = dog(x,y, s1, s2, g)



s2Center=  s1*s1*eye(2);
s2Surround = s2*s2*eye(2);
mu= zeros(2,1);
gSurround = g;

z = mvnpdf([x,y],0,s2Center) - gSurround*mvnpdf([x,y],0,s2Surround);

