% provides error obtained when approximating a dog filter with 
% a different of rectangular filters

function [e,z,y] =dogError(p)


  sCenter=  0.3;
  sSurround = 1.5;

  gSurround = 5;
  npoints = 200;
  range = 4*sSurround;
  del = range/npoints;
  e=0;
  j=0;
  i=0;
  z=zeros(2*npoints+1,2*npoints+1);
  y=zeros(2*npoints+1,2*npoints+1);
 % x2=0;
 % i=1;
  for x1=-range:del:range
    j=0;i=i+1;
   for x2=-range:del:range
    j=j+1;
     if abs(x1)<p(1) &  abs(x2)<p(1)
      y(i,j)=p(2);
     elseif abs(x1)>p(3) ||  abs(x2)> p(3);
       y(i,j)= 0;
     else
       y(i,j)=p(4);
     end
    z(i,j) = dog(x1,x2,sCenter,sSurround,gSurround);

    e= e+ abs(y(i,j)-z(i,j));
  end
 end
 
 