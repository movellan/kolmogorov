% Test discrete time finite horizon lqr
%
clear
a= eye(4);
b = ones(4,1);
c = 0.01*eye(4);

T=10;

% Here we put running penalties for actions and for not being at the desired
% state as soon as possible
w_x = 1; % scales quadratic loss for state 
w_u = 0.0001; % scales quadratic loss for action
for t = 1:T
    q{t} = w_x*eye(size(a));
    g{t} = w_u * eye(size(b,2));
end

% X_{t+1} = a _t X_t + b u_t + c Z_t
% R_t = X_t' q_t X_t + U_t' g_t U_t

[gain, alpha, beta] = dtfhlqr(a, b, c, q,g,T)
gain{1}

  

