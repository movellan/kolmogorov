% approximate the inertial matrix of a solid pendulum 

% First we get the arm

armMass = 30; % in grams
armLength= 15; % in cm

% the moment of inertia is \int_0^l x^2 rho dx = rho l^3/3 = m l^2/3
% where rho is the mass density

armM = armMass*armLength^2/3; 



% now the wheel at the end of the arm
wheelMass = 55; % in grams
wheelM = wheelMass*armLength*armLength; % moment of inertial in wheel at end
                                      % of arm
M = armM + wheelM % moment of inertia in grams*cm^2

% Now for the rotational matrix

% the motor is 55 gr lets say 75 % of it rotating
rotatingMass = wheelMass*0.75;
% the radius of the wheel is 2.5 cm. Much of the weight is in the motor,
% let's say that the effective raadius is 2.5*0.25
effectiveRadius = 2.5*0.75;

rotatingMoment= rotatingMass*(effectiveRadius^2)

% for the torque produced by gavity we need

%wheelMass*armLength+ \int_0^L  x rho dx = m L^2/2

wheelMass*armLength+ armMass*(armLength^2)/2


%Inertial Matrix_11 = 14625 g cm^2 = 0.0014625 Kg m^2
%Inertial matrix_22 = 145 g cm^2 = 145* 10^{-7} Kg m^2
% gravitational constant = 981 cm/ s^2 = 9.81 m/s^2
% gravitional mass coefficient = 4200 g cm = 4200* 10^{-5} Kg m

