%load Noda.dat
clear
load alpha
%alpha(1:100,:) =[];

dt = 1/1000; % sampling period

y = alpha(:,1:2);
tau = alpha(:,3);


v = diff(y)./dt;
a = diff(v)./dt;
y(1:2,:) =[];
v(1,:) =[];
tau(1:2)=[];




n = length(tau);
nx=5;
x = ones(n,nx);
x(:,1) = v(:,1);
x(:,2) = v(:,2);
x(:,3) = sin(y(:,1));
x(:,4) = cos(y(:,1));
x(:,5) = tau;



x1 = ones(n,3);
x1(:,1) = v(:,1); 
x1(:,2)= sin(y(:,1));
x1(:,3) = tau;

%b = regress(a,x);
b1 = inv(x1'*x1 )*x1'*a(:,1);
x2=ones(n,2);
x2(:,1)=v(:,2);
x2(:,2)=tau;

b2 = inv(x2'*x2 )*x2'*a(:,2);

ahat(:,1) = x1*b1;
ahat(:,2) = x2*b2;

PercentCorrect= corr(a,ahat)

b3 = inv(x'*x+0.1*eye(nx))*x'*a;
ahat2 = x*b3;
corr(a,ahat2)



subplot(2,1,1)
plot(a(:,1));
hold on
plot(ahat2(:,1), 'r')

subplot(2,1,2)
plot(a(:,2));
hold on
plot(ahat2(:,2), 'r')


