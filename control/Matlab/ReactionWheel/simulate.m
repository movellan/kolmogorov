% Simulates a reaction wheel. It consists of a pendulum suspended
% vertically and with a rotating wheel attached to a motor at the end of
% the pendulum
% theta(1) is the angulum of the pendulum with respect to the
% vertical. It is 180 degrees if the pendulum points vertically
% downwards.
% theta(2) is the rotation of a reference point of the wheel with respect
% to the vertical line

clear; clf
%load Noda.dat

nu1= 100000; % rotational viscosity for pendulum
nu2= 100000; %rotational viscosity for wheel
l = 15; % arm lenght in cm
r = 2;%  wheel radius in cm
am = 30; % arm mass in g
wm = 55; % wheel mass in g
g = 9.81; % gravitational constant m sec^2
g = 981; % cm sec2
M = zeros(2,2); %moment of inertia
theta = [0.23 0]'; % initial angle of pendulum and wheel
thetaV = [0 0 ]'; % initial velocities in m/sec
sr = 1000; % sampling rate in Hz
dt = 1/sr; % sampling period
T = 10; % observation time in secods



dt2 = dt*dt;
nT = ceil(T*sr);




tau= zeros(1,nT); %torques
f= 0.2;

% Torque is in dyne centimeters: One 1 oz-in = 70615.5 Dyne cm
% The Maxon Amax-22 motor produces about 5 milli Newton Meters per Amp. 
% and it has a 19 to 1 gear ratio -> Lets say 0.1 Newton Meters per amp
% or 10,0000 dynes centimeter


tau = 200*10000*(cos(2*pi*f*(0:nT-1)*dt));


% arm moment of inertia = \int_0^l  x^2 rho dx =rho l^3/3 = m l^2/3
% where rho is density and m = rho*l = mass

% Unit for moment of inertia is gram* cm^2

M(1,1) = wm * (l^2) + am*(l^2)/2 ;
M(2,2) = wm*r^2;


Min= inv(M);
%Min = eye(2);

% for arm we now need \int_0^l l rho dx = rho l^2/2 = m l/2
G= zeros(2,1);

G(1,1)  = g* (wm*l + am*l/2);

Nu = zeros(2,2); 
Nu(1,1) = nu1; Nu(2,2) = nu2;
Nu(2,1) = - nu2; % Note a force applied on the wheel produces an opposite
                 % torque on the arm 

Theta = zeros(2,nT);
ThetaV = zeros(2,nT);
ThetaA = zeros(2,nT);
Theta(:,1) = theta;
ThetaV(:,1) = thetaV;


for t=2: nT
%  if(mod(t,1) ==0)
%    tau(t) = -10000000*theta(1)+10000*randn(1);
%  else tau(t) = tau(t-1);
%  end
  
  tmp=  [ tau(t); -tau(t)]  - Nu*thetaV + G* sin(theta(1));
  thetaA = Min*tmp;
  thetaV = thetaV + thetaA*dt;
  theta = theta+ thetaV*dt + 0.5*thetaA*dt*dt;
 
  
  Theta(:,t) = theta;
  ThetaV(:,t) = thetaV;
  ThetaA(:,t) = thetaA;
end



subplot(3,1,1)

clear tmp;
alpha = (Theta(1,:) )*360/2/pi;
plot([1:nT]*dt, alpha)
xlabel('Time in Secs')
ylabel('\theta_1 in degrees')



subplot(3,1,2)
tmp = (Theta(2,:) )/2/pi;
plot([1:nT]*dt, tmp,'r')  
xlabel('Time in Secs')
ylabel('\theta_2 in revolutions')



subplot(3,1,3)
plot([1:nT]*dt, tau*141.612/100000, 'g'); 

xlabel('Time in Secs')
ylabel('Torque in Oz Inches')


alpha= zeros(nT,3);
alpha(:,1:2) =Theta';
alpha(:,3) = tau;
save alpha





% x = Theta(2,:)';
% vhat= diff(x)/dt;
% ahat = diff(vhat)/dt;
% a = ThetaA(2,:)';
% a(1:2) = [];
% %scatter(a,ahat)
% corr(a,ahat)
