% trying a darned simple second order system
% Results:

% Seems to make a big difference if instead of using the sample average
% of Psi R we use the sample covariance of Psi R. Even better if we use
% the sample correlation of Psi and R.
% 



%randn('seed',1);

clear

n=30;
lambda = 0;

%lambda=-1+0.001;

beta = 0.1; %Weight decay

sx = 0.0001;
s2x = sx^2;

sv = 0.0001;


eps = 0.1;


T= 20;
dt = 0.01;
sqdt = sqrt(dt);

s = ceil(T/dt);


R= zeros(n,1);

dLogPSequence = zeros(n,1);
for trial=1:1000
  
  
  
  randn('seed',1);
  s=100;
  trial
RToGo = zeros(s,n);
dLogPUpTo = zeros(s,n);
dLogPAt = zeros(s,n);
for k=1:n
  
  
  
  v=0;
  R(k) =0;
  dLogP(k)=0;
  x=2 ;
  
  for t=1:s
     

       
    X(t,k) = x;
 
    V(t,k) = v;

    dMu = x;
    tmp6=0;
    if( t >0 )
      tmp6= -( ((x-0).^2)*dt+ 0*v^2*dt)/(s*dt);
      R(k) = R(k) +tmp6;
    end
    
    R2(t) =tmp6;    
    
    dIx = sqdt*sx*randn(1,1);
    dx = v*dt+ dIx;
    
    dLogPSequence(k) = dLogPSequence(k) + dMu*dIx/s2x;
    dLogPUpTo(t,k) = dLogPSequence(k);
    dLogPAt(t,k) = dMu*dIx/s2x;
    
    dIv = sqdt*sv*randn(1,1);
    a = x+lambda*x;
    dv = a*dt + dIv;
    
    x= x+ dx;
    v= v+dv;

  end % time 

%  R(k) = -(X(t,k)-2)^2;
  RToGo(:,k) = (sum(R2) - cumsum(R2) + R2)';

    
  
  
end %samples
%R(k) = (X(s,k)-2).^2;


co(trial)=0;
co2(trial)=0;
co3(trial)=0;
co4(trial)=0;
co5(trial) =0;
co6(trial)=0;
co7(trial)=0;
co8(trial)=0;
co9(trial) =0;
RToGoNormalized = RToGo - repmat(mean(RToGo,2),[1,n]);
tmp4 = dLogPAt - repmat(mean(dLogPAt,2),[1,n]);
sd = std((RToGoNormalized.*tmp4)');

tmp5 = tmp4./ repmat(sd',[1,n]);



for t=1:s
%  tt(t)= corr(RToGo(t,:)', dLogPUpTo(t,:)')*std(RToGo(t,:))*std(dLogPUpTo(t,: ...
                                             %       ));

  co(trial)=co(trial)+corr(RToGo(t,:)', dLogPUpTo(t,:)');
  tmp = corr(RToGo(t,:)', dLogPAt(t,:)');
  co2(trial)=co2(trial)+tmp;
  tmp2 = tmp* std(RToGo(t,:))*std(dLogPAt(t,:));
  co3(trial)=co3(trial)+tmp2;
  co4(trial) = co4(trial) + sign(tmp);
  co5(trial) = co5(trial) + sign(tmp2);
  
  
  co6(trial) = co6(trial) + mean(dLogPAt(t,:).* RToGoNormalized(t,:));
  co7(trial) = co7(trial) + mean(tmp4(t,:).* RToGo(t,:));
  co8(trial) = co8(trial) + mean(dLogPAt(t,:).*RToGo(t,:));
  co9(trial) = co9(trial) + mean(tmp5(t,:).*RToGoNormalized(t,:));
  
  tt(t)= (corr(RToGo(t,:)', dLogPUpTo(t,:)'))*std(RToGo(t,:))*std(dLogPUpTo(t,:));
end


%dLogPUpTo =  dLogPUpTo -repmat(tmp,[1,n]);





c(trial) = corr(R,dLogPSequence);
tmp = R.*dLogPSequence;
d1(trial) = mean(tmp);%/std(tmp);
d2 (trial) = mean(mean(dLogPUpTo.*RToGo));


tmp = dLogPSequence.^2;
tmp2= R.*tmp;
alpha = mean(tmp2)/mean(tmp);
d3(trial) = mean((R-alpha).*(dLogPSequence - mean(dLogPSequence)));;
d4(trial) = mean(R.*(dLogPSequence - mean(dLogPSequence)));
d5(trial) = mean((R-alpha).*dLogPSequence );
d6(trial) = mean((R-mean(R)).*(dLogPSequence - mean(dLogPSequence)))/std(R)/std(dLogPSequence);
d7(trial) = mean(R.*(dLogPSequence - mean(dLogPSequence)))/std(R)/std(dLogPSequence);
d8(trial) = mean((R-mean(R)).*(dLogPSequence - mean(dLogPSequence)));
d9(trial) = mean((R-mean(R)).*dLogPSequence);
Lambda(trial) = lambda;
rho(trial) = mean(R);



Delta(trial)= sign(co6(trial)-beta*lambda); %c(trial);



lambda = lambda+ eps*Delta(trial);

subplot(6,1,1);
scatter((R-mean(R))/std(R),(dLogPSequence-mean(dLogPSequence))/std(dLogPSequence));
subplot(6,1,2)
plot(Lambda)
subplot(6,1,3)
plot(rho)
% pValue(trial) = 1.0;
% if(trial>3)
%    tmpx = [ones(1,length(rho)); 1:length(rho)];
%    [b, j1 j2  j3 stats ]= regress(rho',tmpx');
%    rhohat = tmpx'*b;
%    pValue(trial) = stats(3);
%    hold on
%    if(pValue(trial) < 0.05)
%      plot(rhohat,'r--')
%    else
%      plot(rhohat,'g--')
%    end
   
%    hold off
% end

subplot(6,1,4)
plot(X(:,end-1:end))
if (trial ==1 || trial == 10)
  %keyboard
end

subplot(6,1,5)
plot(tt)
%axis([1,s, -1 1])
subplot(6,1,6);
%plot(pValue)
drawnow



end

mean(c)/std(c)

mean(d1)/std(d1)
mean(d2)/std(d2)
mean(d3)/std(d3)
mean(d4)/std(d4)
mean(d5)/std(d5)
mean(d6)/std(d6)
