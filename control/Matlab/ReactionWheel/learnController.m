% WHY is Fihser so high 119788.833599745 for s=2  
%XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

% Simulates a reaction wheel. It consists of a pendulum suspended
% vertically and with a rotating wheel attached to a motor at the end of
% the pendulum
% theta(1) is the angulum of the pendulum with respect to the
% vertical. It is 180 degrees if the pendulum points vertically
% downwards.
% theta(2) is the rotation of a reference point of the wheel. 

clear; clf

lambda =0; % initial guess for control law
s=3;
n =2000; % number of trajectories

sr = 1000; % sampling rate in Hz
dt = 1/sr; % sampling period
T = 0.5; % observation time in secods
alpha = 000; % cost of action
eps = 0.1; % step size

sigmav = 0.1*eye(2);
sigmax = 0.001*eye(2);



sm2 = (inv(sigmav))^2;
%sm2=eye(2);
sqdt = sqrt(dt);

dt2 = dt*dt;
nT = ceil(T*sr);

%s = ceil(T/dt);







w= ones(2,5); % system vector

 w = [-6.35405914455846       -0.0164359881562874;
      5.69947894188221         -1317.29259008944 ;
      65.5736785290834        0.0671720965680027 ;
      -4.31972481053924e-05        0.0456272501801866 ;
      0.000120685530185796       -0.0131723507254172 ];


 w(5,:) = w(5,:)*10000000;

%w = [ -10 0 100 0 10; 0 0 0 0 -10]';

w= w';

a = w(1:2,1:4);

b = w(1:2,5);




for trial =1 : 1
  trial
  randn('seed',1);
  
  R=zeros(n,1);
  gradR= zeros(n,1);
  gradL= zeros(n,1);

  x1= zeros(s,n);  
  x2= zeros(s,n); 
  U = zeros(s,n);
  for k=1:n % sample trajectories
    
    
    x = 0.1*ones(2,1); % the 2 angles
    v = zeros(2,1); % the angular velocities


    for t=1:s 

      x1(t,k) = x(1); % the angle of the arm
      x2(t,k) = v(1); % the speed of the arm

      

      phi = [x(1)]; %  features for controller

      u = lambda'*phi; % action
      
      
      
      U(t,k) = u;
      
      
      

      
      
      
            
      f = [ v ; sin(x(1)); cos(x(1))] ; % function of state used to
                                        % predict angular acceleration
      

      acc = a*f + b*u; % acceleration

      dIx =  sqdt*sigmax*randn(2,1); % the innovation 
      

      
      
      x  = x + v*dt + 0*0.5*acc*dt^2 + dIx; % the angles
      
      dIv =  sqdt*sigmav*randn(2,1); % the innovation 
      
      v = v + acc*dt + dIv; % angular accelerations
 
      
           R(k) = R(k)-  (alpha*u^2 + 1*(x(1))^2  + 0*v(1)^2)*dt/(s*dt); % the reward


      gradR(k) = gradR(k) -2*alpha*u*phi*dt/(s*dt); % the gradient of the reward
 
      
      gradDrift = b*phi';  
      gradL(k) = gradL(k) + gradDrift'*sm2*dIv/(s*dt);
%      gradL(k) = gradL(k) + gradDrift'*dIv;

      
    end

  end


  
% gradLambdaSamples = gradL.*R + gradR;
  
   gradLambdaSamples = corr( gradL,R);
  gradLambda = mean(gradLambdaSamples)
  Fisher = std((gradL.*R)')/sqrt(n)
  
  gL(:, trial) = gradLambda;
 
  Lambda(:,trial) = lambda; 
  lambda = lambda + eps*tanh(gradLambda); 
  rho(trial) = mean(R);
  

subplot(6,1,1)


plot(mean(x1'),'r');
hold on
plot(min(x1'))
hold on
plot(max(x1'))
hold off
ylabel('\theta_1')
axis([1 s -0.2 0.2])
subplot(6,1,2)
plot(min(x2,[],2));
hold on
plot(max(x2,[],2));
plot(mean(x2'),'r');
hold off
ylabel('Velocity of \theta_1')
axis([1 s -0.2 0.2])
subplot(6,1,3)
plot(min(U,[],2))
hold on
plot(max(U,[],2))
hold off
ylabel('U')
subplot(6,1,4)
plot(rho)
ylabel('\rho')
subplot(6,1,5)
plot(Lambda)
ylabel('\lambda')
subplot(6,1,6)
%plot(F)
ylabel('Grad \lambda')
drawnow
end


