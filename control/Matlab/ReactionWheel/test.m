clear
n = 5000;
x = randn(n,1);

y = 1+ 12*x + randn(n,1);
y(4000:end) = 12+1*x(4000:end);



xbar =0;
ybar=0;
x2bar = 0;
xybar =0;
z=0;
lambda =0.95;
a = zeros(2,n);
for t=1:n
  xbar = lambda*xbar + (1-lambda)*x(t);
  ybar = lambda*ybar + (1-lambda)*y(t);
  x2bar = lambda*x2bar + (1-lambda)*x(t)*x(t);
  xybar = lambda*xybar + (1-lambda)*x(t)*y(t);
  z = lambda*z+(1-lambda);
  xx = [ x2bar xbar; xbar z];
  xy = [ xybar; ybar];
  a(:,t) = inv(xx+0.001)*xy;
end
plot(a(1,2:end)); hold on ;plot(a(2,2:end),'r'); hold off
