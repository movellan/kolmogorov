clear
clf
%from top
%x = [0 0]';
%from bottom
x = [pi 0]';

nstep=400;
maxIter=200;
u = 0.0;

simStep=2000;
szX=length(x);
dt=0.01;
X = nan(szX, simStep);
U = nan(1,simStep);
ulimit=inf;
[x_ u_ L_ cost_] = ilqg_det(@dynamics, @cost, dt, nstep, x, u, maxIter);
%[x_ u_ L_ cost_] = ilqg_det(@oldDynamics, @cost, dt, nstep, x, u, maxIter);

%only execute the first part
for j=1:simStep
	if j<nstep
		tpolicy=j;
	else
		tpolicy=nstep-1; %reuse policy at last time step
	end
	X(:,j)=x;
	u=u_(tpolicy)+L_(:,:,tpolicy)*(x-x_(:,tpolicy));
	U(j)= u;
	x=x + dynamics(x,u)*dt + 0*sqrt(dt)*10*randn(2,1)*pi/180;
end

subplot(1,2,1)
plot((1:simStep)*dt, [X(1:2,:); U], '-');
hold on
plot((1:nstep)*dt, [x_(1:2,:);u_ NaN],':');
hold off
legend('\theta','d\theta','u');

%phase portrait
subplot(1,2,2)
plot(X(1,:),X(2,:), '.-');
hold on
plot(x_(1,:), x_(2,:),':');
xlabel('\theta');ylabel('d\theta');
text(0,0,'g');
hold off
legend('\theta1','\theta2','p\theta1','p\theta2');
