%dynamics of a pendulum

% x = [theta, theta_dot];

function [xdot, xdot_x, xdot_u] = dynamics(x,u)

	k=9.82;
	nu =0.3;
	le = 1;
	me = 3;
	a = [0, 1; -k/abs(le), -nu/me];
	b=[0; 1/me];
	
	fx=[-sin(x(1)); x(2)];
	
	xdot = a*fx+ b*u;
	   		
	
	if nargout==3
		xdot_x = a*[ -cos(x(1)),0; 0,1];
		xdot_u =b;
		
	end
