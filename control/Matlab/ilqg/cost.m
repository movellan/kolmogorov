%>> plot(-pi:.1:pi,arrayfun(@(x) cost([x 0],0,0),-pi:.1:pi))

function [l, l_x, l_xx, l_u, l_uu, l_ux] = cost(x, u, t)
	target=0;
	w=10;
	if isnan(t)
		l=w*(x(1)-target)^2 + x(2)^2;
		l_x = [2*w*(x(1)-target);
			   2*x(2)];
		l_xx=[2*w 0;
			  0   2];
		%add these to make codegen happy
		l_u =0;
		l_uu=0;
		l_ux=zeros(1,2);

	else
		wu=0.0001;
		l=wu*u^2;
		l_u = wu*2*u;
		l_uu = wu*2;
		l_x= zeros(1,2);
		l_xx=zeros(2,2);
		l_ux=[0 0];
	end
