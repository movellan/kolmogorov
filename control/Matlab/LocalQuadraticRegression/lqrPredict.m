function y = lqrPredict(x,f);

%%  create a vector with all quadratic pairs
nSamples = size(x,1);

if f.localOrder >=0
  X = ones(nSamples,1); % constant term
end
if f.localOrder >=1
  X= [X x]; %linear terms
end
 if f.localOrder ==2
  for i=1:f.nDimensions
    for j=i:f.nDimensions
      X= [X x(:,i).*x(:,j)]; % quadratic terms
    end
  end

 end
 

%% create vector of kernel weights
for i=1:nSamples
  for j=1: f.nFilters
    md = x(i,:)'- f.mu{j};
    d(j) = md'*f.nu{j}*md;
    phi(i,j) = exp(- 0.5*d(j));    
  end
  if (sum(phi(i,:)) ==0)
    [km kj] = min(d);
    phi(i,kj) =1;
  else
    phi(i,:) = phi(i,:)/sum(phi(i,:));    
  end
  
end

Xphi=[];
for i=1:f.nFilters
  wi = repmat(phi(:,i), 1,size(X,2));
  Xphi= [ Xphi  X.*wi];
end
w=[];
for i=1:f.nFilters
  w = [w ; f.w{i}];
end

y = Xphi*w;

