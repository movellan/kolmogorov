% develops a proximity based covariance matrix
% function cov = covMatrix(x,v,s);
% x is an nVars * nSamples matrix
% v is an nSamples*1 vector of variances asociated to each sample
% s is a scalar representing the width of Gaussian kernel based
% correlations 
% between columns of x
% c(x(:,i), x(:,j)) = exp(-0.5* norm(x(:,i) - x(:,j))/s)
% where c is the Pearson corrleation coefficient
%
% Output is an nSamples * nSamples covariance matrix
%
function cov = covMatrix(x,v,s);

nSamples = size(x,2);
nVars = size(x,1);
c= zeros(nSamples,nSamples);
for k1=1:nSamples
  for k2 = k1: nSamples
    c(k1,k2) = (norm(x(:,k1)- x(:,k2)))^2;
    c(k2,k1) = c(k1,k2);
  end
end
c = exp(-0.5*c/s); % correlation matrix
sd = sqrt(v);
for k1=1:nSamples
  for k2 = k1: nSamples
    cov(k1,k2) = c(k1,k2)*sd(k1)*sd(k2); 
    cov(k2,k1) = cov(k1,k2);
  end
end