%LocalGlobalQuadraticRegression

clf;clear
f.nDimensions= 1; % number of dimensions

%x: nd * ns  matrix of sample points: 
%x = randn(ns, nd);
x = -10:0.2:10;

f.nSamples = length(x);
f.localOrder =2; % -1: no local 0 for constant, 1 fo linear, 2 for quadratic
f.globalOrder =-1; % -1 no global, 0 constant
f.normalizeLocal=1;
priorVariance = 1000000; % pior variance for parameter gets
                            % times a unit matrix
sigmaZ= 0.1; % variance of observation noise
priorMean=0; % scalar for prior parameter mean vector
%ns = 10; % number of sample points
f.nLocalFilters = 100; % number of features

% mu{i} is nd * 1 vector of means for feature i
% nu{i} is nd * nd precission matrix for feature i
% w{i} is nd*(nd+1)/2 +nd+ 1  weights for feature i



%sigmaZ = sigmaZ*eye(f.nSamples);
sz=sigmaZ;
sigmaZ = covMatrix(x,sigmaZ*ones(f.nSamples,1),0.2);



%diag(x.^2/10+1);

 

if (f.globalOrder <0)
  f.nvarsGlobal=0;
elseif (f.globalOrder==0)
  f.nvarsGlobal=1;
elseif (f.globalOrder==1)
  f.nvarsGlobal=1+f.nDimensions;
elseif (f.globalOrder==2)
  f.nvarsGlobal=1+f.nDimensions + f.nDimensions*(f.nDimensions+1)/2;
end

if (f.localOrder <0)
  f.nvarsLocal=0;
elseif (f.localOrder ==0)
  f.nvarsLocal =1;
elseif f.localOrder ==1
  f.nvarsLocal = 1+f.nDimensions;
else
  
  f.nvarsLocal = 1+ f.nDimensions+f.nDimensions*(f.nDimensions+1)/2;
end

if f.localOrder >=0
  for i=1:f.nLocalFilters
    f.mu{i} = 3*randn(f.nDimensions,1);
    f.nu{i} = 10*diag(f.nDimensions);
    f.w{i} = zeros(f.nvarsLocal,1);
  end
end

%y: ns vector of outputs for each sample point

%y = 0.002*x.^3+exp(-4*(x-2).^2) - exp(- 4*(x+1).^2);
trueY = 2 +0.08*x+ 0.01*x.^2 +0*exp(-4*(x).^2) ;
z= sqrtm(sigmaZ)*randn(f.nSamples,1);
z=z';
y = trueY+z;

%%  create a vector with all quadratic pairs
if f.localOrder ==2 | f.globalOrder ==2
  x2=[];
  for i=1:f.nDimensions
    for j=i:f.nDimensions
      x2= [x2; x(i,:).*x(j,:)]; % quadratic terms    
    end
  end
end

if f.localOrder >=0
  X = ones(1,f.nSamples); % constant term
end
if f.localOrder >=1
  X= [X; x]; %linear terms
end
if f.localOrder ==2
  X=[X; x2];
end
if f.localOrder>=0
  %% create vector of kernel weights
  for i=1:f.nSamples
    for j=1: f.nLocalFilters
      md = x(:,i)'- f.mu{j};
      d(j) = md'*f.nu{j}*md;
      phi(i,j) = exp(- 0.5*d(j));    
    end
    %phi(i,j) is the relevance of sample i to kernel j
    % normalize weights
    if f.normalizeLocal==1
      if (sum(phi(i,:)) ==0)
	[km kj] = min(d);
	phi(i,kj) =1;
      else
	phi(i,:) = phi(i,:)/sum(phi(i,:));    
      end
    end
  end
  
end

Xphi=[];
if f.localOrder >=0
  for i=1:f.nLocalFilters
    wi = repmat(phi(:,i)', f.nvarsLocal,1);
    Xphi= [ Xphi  ;X.*wi];
  end
end

% now append global terms
if f.globalOrder >=0
  Xphi = [Xphi; ones(1,f.nSamples)];
end
if f.globalOrder > 0
  Xphi = [Xphi ;x];
end

if f.globalOrder >1
  Xphi = [Xphi; x2];
end
f.totalVars = f.nvarsGlobal+f.nLocalFilters*f.nvarsLocal;
priorPrecission= eye(f.totalVars)/priorVariance;

isz = inv(sigmaZ);
K = Xphi*isz*Xphi' + priorPrecission;
K2 = Xphi*Xphi'/sz + priorPrecission;


varW = inv(K);
varW2 = inv(K2);

Ew = varW*(Xphi*isz*y'+priorPrecission*priorMean*ones(f.totalVars, ...
						  1));

Ew2 = varW2*(Xphi*y'/sz+priorPrecission*priorMean*ones(f.totalVars, ...
						  1));

yHat= Xphi'*Ew;
yHat2= Xphi'*Ew2;

for k=1:f.nSamples
  yHatSD(k,1) = sqrt(Xphi(:,k)'*varW*Xphi(:,k));
end

% Ew = reshape(Ew,f.nvarsLocal, f.nLocalFilters);
% for i=1:f.nLocalFilters
%   f.w{i} = Ew(:,i);
% end



%yHat = lqrPredict(x,f)
subplot(211)
plot(x,y,'ob',x,yHat,'r--')
hold on
plot(x,trueY,'b',x,yHat2,'k--')

plot(x,yHat+yHatSD,'g--');
plot(x,yHat-yHatSD,'g--');

if f.localOrder >=0
  subplot(212)
  for i=1:f.nLocalFilters
      plot(x,phi(:,i));
    hold on
  end
end
