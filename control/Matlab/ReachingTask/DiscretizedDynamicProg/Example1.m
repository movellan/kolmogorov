% Simple example illustrating how to use the FiniteHorizonDP function
% Javier R. Movellan

clear
rand('seed',1);
nu = 10; % number of actions
ns = 10; % number of states
T = 10; % time steps
rhoU = [1:nu]'; % Immediate value of action
                % Least expensive action is the first one
rhoU = -0.02*rhoU.*rhoU;
rhoU = repmat(rhoU, 1,T);
rhoS = zeros(ns,1); % Immediate value of states
                    % Best state is state 1
rhoS(1) = 1; 
rhoS= repmat(rhoS, 1,T);





for u =1: nu
  p{u} = 0.01*rand(ns,ns);
  for i=1:ns
    for j=1:ns 
      % uniform random transitions except for trhnsition from state
      % i- u to state i, which has high transition probability
      if (j == i -u) p{u}(i,j) = p{u}(i,j) +1;
      end
    end
  end
  p{u} = p{u}./repmat(sum(p{u},2),1,ns); % normalize transition matrix
end


[v, uMax] = FiniteHorizonDP(p, rhoU, rhoS)  ;
v 
uMax'

