% k time steps of a linear stochastic difference equation
% 
% X_{t+1} = a X_t + u + c Z_t
% where dimensionality of X_t and Z_t may be different. 
%
% Copyright MPLab, Javier R. Movellan 2007

function [mu, sigma2] = nSDESteps(a,u,c, xt, k)
  
  b = a^0;
  for i=1: k-1
    b = b+a^i;
  end
  
  mu = (a^k)* xt+ b*u;
  sigma2 = b *c*c'*b';
  
  

