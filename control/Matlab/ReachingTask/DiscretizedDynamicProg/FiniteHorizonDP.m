% Finite Horizon Dynamic Programming Solution for Markov Decision Processes
%
% We use the convention that an action at time t has an effect on the
% transition probability between state at time t and state at time t+1
% p: is the state transtion probability is a cell array with as many
%    cells as possible actions. Each cell  has an (ns x ns) transition
%    probability matrix
%
% rhoU: an nActions x (TimeSteps-1)  matrix with the value of each action at
%       each time step.
% 
% rhoS:  an nState x TimeSteps matrix with the value of each state at
%        each time step
%
% v: an nStates x TimeSteps matrix with the optimal value of each state
%    at each time step.
%
% uMax: an nStates x (TimeSteps-1) with the optimal action for each state
%       and each time
%     
% Javier R. Movellan Copyright @ 2007
%
%
function [v uMax] = FiniteHorizonDP(p, rhoU, rhoS)
 
  
nu = size(p,2);
ns = size(p{1},1);
T = size(rhoS,2);


uMax= zeros(ns,T-1);


v = zeros(ns,T);
v(:,T) = rhoS(:,T);    % terminal value of states
for t=T-1:-1: 1
  vMax = -1000000000000000000000*ones(ns,1);
  uMax(:,t) = zeros(ns,1);
  for u=1: nu
    tmp = repmat(rhoU(u,t),ns,1) +rhoS(:,t)+ p{u}*v(:,t+1);
    k = find(tmp>vMax);
    uMax(k,t) = u;
    vMax(k) = tmp(k);
  end
  v(:,t) = vMax;
end



  


