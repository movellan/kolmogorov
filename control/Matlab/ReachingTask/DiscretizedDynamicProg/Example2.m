% Simulate a controller for a point mass moving in a viscous liquid along
% a line.
clear

% Here is a big issue: If we use a Dt = 0.01 then the change in location
% will be in the order of 0.01 and the change in velocity in the order of
% 0.005, given the forces allowed to use for control. If we want to be
% able to detect a change in state of this magnitude, then we need about 
%10/0.01 = 1000 states for location and % 2/0.005 = 400 states for
%velocity, for a total of 400,000 states. Way too much. 




Dt = 0.01; % Time step in seconds (Newtonian mechanics are discretized)
nDt = 100; % number of continuous state Dts per discrete state Dt
T= 4000;    % in time steps
n = 2; % dimensionality of state (one for position one for velocity)
m = 1; % dimensionaliry of U_t (force = U_t * mass)
eta = 0.1; % viscosity coefficient 
mass = 10; % in Kg

% Equation Dynamics
% X_{t+1} = a _t X_t + b u_t + c Z_t, where Z_t multivariate  Gaussian noise
a =[ 1 Dt- eta *Dt^2/2 ; 0 1 - eta*Dt] ;
b = [ Dt^2/(2*mass) ; Dt/mass];

% The c matrix simulates the application of Gaussian random forces on the
% point mass
%sigma=10.5;
sigma=4;
c = [ 0 sigma* Dt^2/(2*mass); 0 sigma*Dt/mass];
mass = 10; % in kilos

% In optimal controller the position goes from -11 to 1, the veclocity
% from -0.5 to 1.5 m/sec  and the force from -10 to 10 newtons 


xMin = -1;
xMax = 1;
nX = 21; % number of discete states used for X (location)
vMin = -1;
vMax = 1;
nV = 21; % number of discrete states used for V (velocity)
uMin = -1;
uMax = 2;
nU = 21; % number of discrete states used for force

i=0;
deltaX = (xMax - xMin)/nX;
deltaV = (vMax - vMin)/nV;
for x=xMin+deltaX/2:(xMax-xMin)/(nX): xMax-deltaX/2  
  for v = vMin+deltaV/2:(vMax-vMin)/(nV): vMax - deltaV/2
    i=i+1;
    SDAC{i} = [ x;v]; % digital to analog conversion table for state
  end
end
nStates= i;

i=0;
deltaU = (uMax - uMin)/nU;
for u=uMin+deltaU/2:(uMax-uMin)/(nU): uMax-deltaU/2
  i = i+1;
  UDAC{i} = u; % digital to analog conversino table for action
end
nActions=i;


%%%%%%%%%%%% Begin  Compute approximate transition prob matrices %%%%%%%%%%%%%%

for u = 1:nActions
  u
  for s = 1:nStates
    xt = SDAC{s};
    ut= UDAC{u};

    
    % We take 100 time steps each of size Dt. The reason we need to take
    % more than 1 time step, is that 1 time step is not enough to move
    % from one discrete state to another. So in practice one consequence
    % of discretizing continuous states, is that effectivelly we need to
    % also work with longer time constants, i.e., our controller cannot
    % be as responsive in time as a continuous time controller would be. 
    
    [MeanS, VarS]  = nSDESteps(a, b*ut, c, xt, 100);
    if (MeanS(1) < xMin ) MeanS(1) = xMin; end
    if (MeanS(1) > xMax)  MeanS(1) = xMax; end
    if (MeanS(2) < vMin ) MeanS(2) = vMin; end
    if (MeanS(2) > vMax)  MeanS(2) = vMax; end
    InvVarS = pinv(VarS); % I wonder what happens when the variance is
                          % not really invertible
    
    % We got the mean and variance of the state at the next time step, and the
    % distributikon about the next step is Gaussian. Now we computer what
    % the prob density would be for the center of all the discretized
    % states. 
    
    for sPrime = 1:nStates
      xtp1 = SDAC{sPrime};
      p{u}(s,sPrime) = exp(-0.5*(MeanS-xtp1)'*InvVarS*(MeanS-xtp1));
    end
    %% If the sum of probs accross states is very small, assign all the
    %probability to the nearest neibhgoour
    tmpSum= sum(p{u}(s,:));
    if( tmpSum <=0.000001)
      [junk, nnIndex] = min(p{u}(s,:));
      p{u}(s,nn)= 1
    end
    p{u} = p{u}./repmat(sum(p{u},2),1,nStates); % normalize transition matrix
  end
end
%%%%%%%%%%%% End Compute approximate transition prob matrices %%%%%%%%%%%%%%%

  
% Now define the state and action values


nT = T/nDt;


q{T} = [ 2000 0 ; 0 4000]; 



for s= 1:nStates
  rhoS(s,T)  = - SDAC{s}' *q{T}*SDAC{s};
end


% Here we put running penalties for actions and for not being at the desired
% state as soon as possible
w_x = 0.0; % scales quadratic value for state 
w_u = 0.0; % scales quadratic value for action
for t = 1:nDt-1
    q{t} = w_x*eye(size(a));
    if t > nDt -100
      q{t} = [ 2000 0 ; 0 4000]; 
    end
    
    for s= 1:nStates
      rhoS(s,t)  = - 50* SDAC{s}' *q{t}*SDAC{s};
    end
    
    g{t} = w_u*eye(size(b,2));
    for u =1: nActions
      rhoU(u,t) = - 50*UDAC{u}'*g{t}*UDAC{u};
    end
end







[OptV, uMax] = FiniteHorizonDP(p, rhoU, rhoS);


for t=nT-1:nT-1
  for s=1:nStates
    SDAC{s}
    UDAC{uMax(s,t)}
   
  end
end


