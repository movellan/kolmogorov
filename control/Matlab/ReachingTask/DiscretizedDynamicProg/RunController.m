%load matlab
clear x
%%%%%%%%%%% Now we run the controller
x(:,1) = [-2;0];
for t=1:4000
  t
  dState=0;
  tmp = 99999999999999;
  for s=1:nStates
    if(norm(SDAC{s} - x(:,t)) < tmp)
      tmp = norm(SDAC{s} - x(:,t));
      dState=s;
    end
  end
   tmax = size(uMax,2);
    u(:,t) = UDAC{uMax(dState,min(tmax,ceil(t/100)))};
  x(:,t+1) = a*x(:,t) + b*u(:,t) + 0*c*randn(2,1);
end

figure
plot(x(1,:))
figure
plot(x(2,:))
figure
plot(u)