% Simulate optimal application of force to a mass moving in a viscous
% liquid moving along a line. The 
%
% Copyright @ Javier R. Movellan 2007

clear

Dt = 0.01; % Time step in seconds (Newtonian mechanics are discretized)
T= 1000;    % in time steps
mass = 10; % in Kilos
n = 2; % dimensionality of state (one for position one for velocity)
m = 1; % dimensionaliry of U_t (force = U_t * mass)

eta = 0.1; % viscosity coefficient 

% Equation Dynamics
% X_{t+1} = a _t X_t + b u_t + c Z_t, where Z_t multivariate  Gaussian noise
for t=1:T
  a{t} =[ 1 Dt- eta *Dt^2/2 ; 0 1 - eta*Dt] ;
  b{t} = [ Dt^2/(2*mass) ; Dt/mass];
end

% The c matrix simulates the application of Gaussian random forces on the
% point mass

sigma=100;
%sigma=10; % standard deviation in Newtons
for t=1: T
  c{t} = [ 0 sigma* Dt^2/(2*mass); 0 sigma*Dt/mass];
end



% The loss is of the form 
% x_T' q_T x_T + sum_{t=1}^{T-1} u_t' g_t u_t + x_t' q_t x_t 
% We neeed to specify the state cost matrices q and the action cost
% matrices g


% At terminal time T, we want the system to be at the origin with zero
% velocity

q{T} = [ 20000 0 ; 0 200000]; 


% Here we put running penalties for actions and for not being at the desired
% state as soon as possible
w_x = 0.0; % scales quadratic loss for state 
w_u = 0.01; % scales quadratic loss for action
for t = 1:T-1
    q{t} = w_x*eye(size(a{1}));
    g{t} = w_u * eye(size(b{1},2));
end



% This gives us the optimal gain matrices for each time step

% Now we simulate  a run with the optimal controller


for t=1:T-1
  k{t} = zeros(m,n); 
  h{t} = zeros(m,1);
end

  
for trial =1: 200
x(:,1) = [-10 -2 ]'; % Initial location and velocity  in meters and
%met/sec 
x(:,1) = [10*randn(1) randn(1)]'; % Initial location and velocity  in meters and met/sec 
x(:,1)
Cost(trial) =0;
for t=1:T-1
  u(:,t) = k{t}*x(:,t)+ h{t};
  Cost(trial) = Cost(trial) + x(:,t)'*q{t}*x(:,t)+ u(:,t)'*g{t}*u(:,t);
  x(:,t+1) = a{t}*x(:,t) + b{t}*u(:,t) + c{t}*randn(2,1);
end

Cost(trial) = Cost(trial) + x(:,T)'*q{T}*x(:,T);

if (trial ==1) lambda =0; end
if (trial > 1 && Cost(trial) < Cost(trial-1))
  lambda = 0.99*lambda+ 0.01*1;% move towards 1, i.e., more Gauss-Newton
end

if (trial > 1 && Cost(trial) > Cost(trial-1))
  lambda = 0.99*lambda;  % move towards 0. i.e., less Gauss-Newton
end
lambda


[k, h]= lqr(a, b, c, q,g,T,x, k, h, lambda);







for t=1:T-1
  g1(t) = -k{t}(1,1);
  g2(t) = -k{t}(1,2);
end

subplot(2,3,1)
plot(x(1,:));
ylabel('Position (meters)');
xlabel('Time (centisecs)');
subplot(2,3,2)
plot(x(2,:) );
ylabel('Velocity meters/sec');
xlabel('Time (centisecs)');
subplot(2,3,3)
plot(u(:));
ylabel('Control Force in Newtons '); 
xlabel('Time (centisecs)');
subplot(2,3,4)
plot(g1);
ylabel('Position Gain');
xlabel('Time (centisecs)');
subplot(2,3,5)
plot(g2);
ylabel('Velocity Gain');
xlabel('Time (centisecs)');
drawnow

subplot(2,3,6)
plot(Cost);
ylabel('Cost');
xlabel('Trials');
drawnow

end