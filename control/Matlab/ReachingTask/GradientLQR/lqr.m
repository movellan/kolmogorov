% X_{t+1} = a _t X_t + b u_t + c Z_t
% R_t = X_t' q_t X_t + U_t' g_t U_t
% u_t = k_t x_t + h_t
% Phi_t(x_t) = x'_t alpha_t x_t + x'_t beta_t + gamma_t



function [k, h]= lqr(a, b, c, q,g,T,x, k, h, lambda)

epsilon = 0.0001;
alpha{T} = q{T};
beta{T}=zeros(size(a{1},1),1);
gamma{T} = beta{T};


 
for t = T-1:-1:1
  abk = a{t} + b{t}*k{t};
  alpha{t} = abk'* alpha{t+1} *abk+ k{t}' * g{t} *k{t} + q{t};

  beta{t} = (( 2*h{t}'*b{t}'*alpha{t} + beta{t+1}') * abk + 2*h{t}'*g{t}* ...
            k{t})';
  gamma{t} = h{t}'*b{t}'*alpha{t+1}* b{t}*h{t}+ h{t}' * g{t}*h{t} + beta{t+1}'* ...
      b{t}*h{t} + trace(c{t}'*alpha{t+1} *c{t}) + gamma{t+1};

  
  gradK = 2*(( b{t}'*alpha{t+1}*b{t} + g{t})*k{t} + b{t}'*alpha{t+1}*a{t}) * x(:,t)* ...
          x(:,t)'+ 2 *( b{t}' * alpha{t+1} *b{t} + g{t} )* h{t} * x(:,t)' + b{t}' * ...
          beta{t+1} * x(:,t)';
  
 k{t} = k{t} - epsilon*gradK;
  
  gradH = 2*(b{t}'*alpha{t+1}*b{t}+ g{t})*k{t}*x(:,t)+ 2* b{t}'*alpha{t+ 1}*a{t}*x(:,t) ...
          + 2*( b{t}'*alpha{t+1}*b{t} + g{t} )*h{t} + b{t}'*beta{t+1};
  h{t} = h{t} - 100*epsilon*gradH';

  
 
%%%%%% uncomment to get analytical solution %%%%%%%%%%%%
tinv = inv(b{t}'*alpha{t+1}* b{t} + g{t} );
 GaussKt =  - tinv*b{t}'*alpha{t+1}*a{t};
 GaussHt =  - tinv*b{t}'*beta{t+1};

 k{t}  = (1 - lambda)*k{t} + lambda* GaussKt;
 h{t} = (1 - lambda)* h{t} + lambda*GaussHt;

%  alpha{t} = q{t}+ a'*alpha{t+1}*(a +b*k{t});
%  h{t} = zeros(size(b{1},2),1);
  

end


