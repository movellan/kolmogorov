% Simulate optimal application of force to a mass moving in a viscous
% liquid moving along a line. The 
%
% Here we develop a PID controller as the solution to a an
% infinite horizon LQR problem 
% Copyright @ Javier R. Movellan 2007
clear
clf

Dt = 0.01; % Time step in seconds (Newtonian mechanics are discretized)
T= 2000;    % in time steps
mass = 10; % in Kilos
n = 2; % dimensionality of state (one for position one for velocity)
m = 1; % dimensionaliry of U_t (force = U_t * mass)

eta = 0.1; % viscosity coefficient 

% Equation Dynamics
% X_{t+1} = a _t X_t + b u_t + c Z_t, where Z_t multivariate  Gaussian noise


% I'm going to augment X_t to include an integral term also

% augmented X_t will be [ I_t X_t dX_t/dt} where I_t = \int_0^t X_s ds 
%a =[ 1 Dt- eta *Dt^2/2 ; 0 1 - eta*Dt] ;

a = [ 1 Dt Dt^2/2 ; 0 1 Dt; 0 0 1]

% add the viscosity (friction ) forces
a = a - eta *[ Dt^3/6 0 0 ; 0 Dt^2/2 0 ; 0 0 Dt]  

% and now for the forces we can control
b = [Dt^3/(6*mass);  Dt^2/(2*mass) ; Dt/mass];





% The c matrix simulates the application of Gaussian random forces on the
% point mass

sigma=1;
%sigma=10; % standard deviation in Newtons
c = [ 0 0 sigma*Dt^3/(6*mass); 0 0 sigma* Dt^2/(2*mass); 0 0 sigma*Dt/mass];



% Here we need q and g to be stationary

q = eye(size(a)) ; % scales quadratic loss for state 
q(1,1) =0.01;
g  = 0.1; % scales quadratic loss for action

[ tmp1, tmp2, tmp3] = dare(a,b,q,10*g)

x(:,1) = [0 -10 0]'; % Initial location and velocity  in meters and met/sec 

for t=1:T-1
  u(:,t) = -tmp3*x(:,t);
  x(:,t+1) = a*x(:,t) + b*u(:,t) + c*randn(3,1);
end
for t=1:T-1
  g1(t) = tmp3(1);
  g2(t) = tmp3(2);
  g3(t) = tmp3(3);
end



subplot(2,3,1)
hold on 
plot(x(2,:),'r');
ylabel('Position (meters)');
xlabel('Time (centisecs)');
subplot(2,3,2)
hold on 
plot(x(3,:) , 'r');
ylabel('Velocity meters/sec');
xlabel('Time (centisecs)');
subplot(2,3,3)
hold on 
plot(u(:),'r');
ylabel('Control Force in Newtons '); 
xlabel('Time (centisecs)');
subplot(2,3,4)
plot(g2,'r');
hold on 
ylabel('Position Gain');
xlabel('Time (centisecs)');
subplot(2,3,5)
hold on 
plot(g3,'r');
ylabel('Velocity Gain');
xlabel('Time (centisecs)');

subplot(2,3,6)
hold on 
plot(g1,'r');
ylabel('Integral Gain');
xlabel('Time (centisecs)');


