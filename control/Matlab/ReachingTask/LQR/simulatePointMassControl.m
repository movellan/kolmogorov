% Simulate optimal application of force to a mass moving in a viscous
% liquid moving along a line. The 
%
% Here we compare a finite horizon non-stationary solution with an
% infinite horizon stationary solution
% Copyright @ Javier R. Movellan 2007
clear
clf

Dt = 0.01; % Time step in seconds (Newtonian mechanics are discretized)
T= 1000;    % in time steps
mass = 10; % in Kilos
n = 2; % dimensionality of state (one for position one for velocity)
m = 1; % dimensionaliry of U_t (force = U_t * mass)

eta = 0.1; % viscosity coefficient 

% Equation Dynamics
% X_{t+1} = a _t X_t + b u_t + c Z_t, where Z_t multivariate  Gaussian noise
a =[ 1 Dt- eta *Dt^2/2 ; 0 1 - eta*Dt] ;
b = [ Dt^2/(2*mass) ; Dt/mass];

% The c matrix simulates the application of Gaussian random forces on the
% point mass

sigma=0;
%sigma=10; % standard deviation in Newtons
c = [ 0 sigma* Dt^2/(2*mass); 0 sigma*Dt/mass];

%%%%%%%%%%%%%%%%%%%% finite Horizon Non-Stationary Case %%%%%%%%%%%%%%%%

% The loss is of the form 
% x_T' q_T x_T + sum_{t=1}^{T-1} u_t' g_t u_t + x_t' q_t x_t 
% We neeed to specify the state cost matrices q and the action cost
% matrices g


% We let the control function to do its best so that at terminal time we
% are at point 0 with zero velocity

% Here we put running penalties for actions and for not being at the desired
% state as soon as possible
w_x = 0.0; % scales quadratic loss for state 
w_u = 0.1; % scales quadratic loss for action
for t = 1:T-1
    q{t} = w_x*eye(size(a));
    g{t} = w_u * eye(size(b,2));
end

% Here is where we say that by time T  we want to be stationary (zero
% velocity} at terminal time T

q{T} = [ 200 0 ; 0 200000]; 



% This gives us the optimal gain matrices for each time step
gain = lqr(a, b, c, q,g,T);



% Now we simulate  a run with the optimal controller
x(:,1) = [-10 0]'; % Initial location and velocity  in meters and met/sec 

for t=1:T-1
  u(:,t) = -gain{t}*x(:,t);
  x(:,t+1) = a*x(:,t) + b*u(:,t) + c*randn(2,1);
end



for t=1:T-1
  g1(t) = gain{t}(1,1);
  g2(t) = gain{t}(1,2);
end

subplot(2,3,1)
plot(x(1,:));
ylabel('Position (meters)');
xlabel('Time (centisecs)');
subplot(2,3,2)
plot(x(2,:) );
ylabel('Velocity meters/sec');
xlabel('Time (centisecs)');
subplot(2,3,3)
plot(u(:));
ylabel('Control Force in Newtons '); 
xlabel('Time (centisecs)');
subplot(2,3,4)
plot(g1);
ylabel('Position Gain');
xlabel('Time (centisecs)');
subplot(2,3,5)
plot(g2);
ylabel('Velocity Gain');
xlabel('Time (centisecs)');


%%%%%%%%%%% Stationary infinite horizon problem %%%%%%%%%%%%%%%%%%%


% Here we need q and g to be stationary

q = eye(size(a)) ; % scales quadratic loss for state 
g  = 0.1; % scales quadratic loss for action

[ tmp1, tmp2, tmp3] = dare(a,b,q,10*g)

for t=1:T-1
  u(:,t) = -tmp3*x(:,t);
  x(:,t+1) = a*x(:,t) + b*u(:,t) + c*randn(2,1);
end
for t=1:T-1
  g1(t) = tmp3(1);
  g2(t) = tmp3(2);
end



subplot(2,3,1)
hold on 
plot(x(1,:),'r');
ylabel('Position (meters)');
xlabel('Time (centisecs)');
subplot(2,3,2)
hold on 
plot(x(2,:) , 'r');
ylabel('Velocity meters/sec');
xlabel('Time (centisecs)');
subplot(2,3,3)
hold on 
plot(u(:),'r');
ylabel('Control Force in Newtons '); 
xlabel('Time (centisecs)');
subplot(2,3,4)
plot(g1,'r');
hold on 
ylabel('Position Gain');
xlabel('Time (centisecs)');
subplot(2,3,5)
hold on 
plot(g2,'r');
ylabel('Velocity Gain');
xlabel('Time (centisecs)');


