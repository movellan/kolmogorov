% Returns features, feature derivatives and Hessian of state x


function [phi,g,h] =  Phi(x)







nf = 3;

phi = zeros(nf,1);% feature vector
g= zeros(2,nf); % gradient matrix
h = zeros(2,2,nf); % hessian matrices

% quadratic features
    phi(1) = 0.5*x(1)^2; 
    phi(2) = 0.5*x(2)^2;
    phi(3) = x(1)*x(2); 
% gradian matrix   
    g(1,1) = x(1);
    g(1,2) =0; 
    g(1,3) = x(2);
    g(2,1) =0;
    g(2,2) = x(2); 
    g(2,3) = x(1);
% hessians of each feature
    h(:,:,1)= [ 1 0; 0 0]; 
    h(:,:,2) = [0 0; 0 1];
    h(:,:,3) = [0 1; 1 0];
