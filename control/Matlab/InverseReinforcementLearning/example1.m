clear
clf

% inverse reinforcement lear
%dXt = a Xt dt + bU_t dt + c dB_t

% we asssume these re known
a = randn(2);
b= randn(2);
c= 0.05*eye(2);
q = eye(2);


% this is unknown to us. Our goal is to recover from the observed behaviors
p = [1 0 ; 0 10]
gain = lqr(a,b,p,0.5*q,zeros(nx,nu));
k = - gain;  % since the control law is k*x 

% we now assume we've seen examples of x, u pairs and been able to
% figure out the gain matrix k. Our goal is to recover p

phat = (-a'*q*inv(b')*k - 0.5*k'*q*k);
% our estimate of p
phat = (phat+ phat')/2