clear
clf


%dXt = a Xt dt + bU_t dt + c dB_t
alpha =1;
nx=2;
nu=2;

a =0* [ -1 1; 0 -alpha] 
b=eye(nu);
c= 0.05*eye(2);
q = 0.5*eye(nu);
p = [1 0 ; 0 10]

gain = lqr(a,b,p,q,zeros(nx,nu));


dt = 0.01
T = 1 % horizon in seconds

times =0:dt:T
nt = length(times);
nSamples =4; % number of sample trajectories

sqdt=sqrt(dt);
c2=c'*c;
% obtain sample trajectories
for s=1:nSamples
s
  x{s}=zeros(nx, nt);
  x{s}(:,1)= [0;0]; % initial state

  for t=1:nt-1
    % optimal action for p = 10*eye(2)
    u{s}(:,t) = gain*x{s}(:,t);

    dx = a*x{s}(:,t)*dt+ b*u{s}(:,t)*dt + c*sqdt*randn(2,1);
    x{s}(:,t+1)= x{s}(:,t)+ dx;
  end
end

for s=1:nSamples
t = nt-1;
y(s,1)= u{s}(t);
[p,gr,h]= Phi(x{s}(:,t));

z(s,:) = b'*gr; 

end
w= pinv(z'*z+ 0.0000000*eye(length(p)))*z'*y;
subplot(3,1,2)
scatter(y,z*w)

for s=1:nSamples
  g(s,t)=0;
  [p,gr,h]= Phi(x{s}(:,t));
  v(s,t) = p'*w;
  for f=1:length(p);
    g(s,t)= g(s,t)-0.5*trace(c2*h(:,:,f)*w(f));

  end
  g(s,t)= g(s,t)-0.5*u{s}(t)^2/q;
  g(s,t) = g(s,t) - (a*x{s}(:,t))'*gr*w;
  
  x1(s,t) = x{s}(1,t);
  x2(s,t) = x{s}(2,t);
end

% now we plot the cost function

myX = [x1(:,t)  x2(:,t).^2  x1(:,t).*x2(:,t)];
myX = [myX ones(length(myX),1)];
myY = g(:,t);
theta = pinv(myX'*myX)*myX'*myY;
myYhat = myX*theta;




subplot(3,1,1)
for s=1:nSamples
plot(times,x{s}(1,:))
hold on
end
hold off
subplot(3,1,2)

subplot(3,1,3)
scatter(x2(:,t).^2+x1(:,t).^2,g(:,t))
%scatter(myYhat,g(:,t))
