function model =ilqt(model)

nt=ceil(model.T/model.dt); 

if(isfield(model,'epsilonCurrentMax')==0)
  model.epsilonCurrentMax= model.epsilonMax;
end

if(isfield(model,'epsilon')==0)
  model.epsilon= model.epsilonMax;
end


T=nt*model.dt;
sqdt=sqrt(model.dt);
nx = size(model.w2{1},2);
nu = size(model.w2{1},1);

[ r, p1_,p2_,xi_,q_,omega_] =model.reward(zeros(nx,1),zeros(nu,1),model);
for t=1:nt
  p1{t} = p1_;
  p2{t}= p2_;
  xi{t} = xi_; 
  q{t} = q_;
  omega{t}=omega_;
  u{t} = zeros(nu,1);
  openLoopCont{t}= zeros(nu,1);
end







  x{1} = model.x0;

  v =0; % value of initial state
  for t=1:nt
    if t<nt
      du= model.w1{t}+model.w2{t}*x{t};
      
      % the new open loop controller
      
	openLoopCont{t}= du;
     
      % we run the system with the new open loop controller
      u{t} = openLoopCont{t};
      [ r, p1_,p2_,xi_,q_,omega_] =model.reward(x{t},u{t},model);
      
      v = v  + r*exp(-(t-1)*model.dt /model.tau)*model.dt;
      
      p1{t} = p1_;
      p2{t}=p2_;
      xi{t} = xi_;
      q{t} =q_;
      omega{t} = omega_;
      
      
      [f, Jx, Ju] = model.dynamics(x{t},u{t},model);
      
      x{t+1} = x{t}+ f*model.dt;
    end
    omega{t} = omega{t}-u{t};
    k{t} = f - Jx*x{t};
    a{t} = Jx;
    b{t} = Ju;
    c{t} = 0.0*eye(nx);  
    
  end
  v = v +(2*p2{end}'*x{end} -(x{t}-xi{end})'*p1{end}*(x{t}-  xi{end}))*exp(-T/model.tau);

  
  data=  lqt(x,model.w1,model.w2,xi,omega,k,a,b,c,p1,p2, q,model.tau,model.dt);
  v2 = -(x{1}'*data.alpha*x{1}-2*data.beta'*x{1}+ data.gamma);
  model.w1 =data.ow1;
  model.w2=data.ow2;
  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% search for step size
% always test eps =0 and eps = epsilon
% eps =0 is same as not doing anything, so it should result
% on the current value. 

deps = model.epsilonCurrentMax/2;
if(isfield(model,'value') ==0)
  eps = model.epsilonCurrentMax:-deps:0;
else
  eps = model.epsilonCurrentMax:-deps:deps;
end


for k=1 : length(eps)
  for t=1:nt
    test.w1{t}= openLoopCont{t}+eps(k)*model.w1{t};
    test.w2{t}=eps(k)*model.w2{t};
  end
  [v_,x,u]=getExpectedReward(test.w1,test.w2,model); 
  vtest(k) = v_;
end
if(isfield(model,'value') ==1)
  vtest(k+1) = model.value;
  eps(k+1) = 0;
else
  model.value=[];
end



[vbest,im] = max(vtest);

epsBest= eps(im);

if(epsBest ==0)
        model.epsilonCurrentMax = model.epsilonCurrentMax/2;
        model.epsilonCurrentMax
    elseif(epsBest == model.epsilonCurrentMax)
        model.epsilonCurrentMax = model.epsilonCurrentMax*2;
    end
    if(epsBest > model.epsilonMax) epsBest = model.epsilonMax; end
     if(model.epsilonCurrentMax > model.epsilonMax) 
         model.epsilonCurrentMax = model.epsilonMax; 
     end
     model.epsilon = epsBest;
     model.value= vbest;
     
for t=1:nt
  model.w1{t}= openLoopCont{t}+epsBest*model.w1{t};
  model.w2{t}= epsBest*model.w2{t};
end

