function [xdot, Jx, Ju] = dynamicsInfomax(x,u,metaData)
% Original model is 
%dXt = Ut + c dBt
%dYt =  (X_t-\alpha) exp(-0.5*(X_t - alpha)'beta(X_t -alpha))  + h  dWt
%
% From this we get the dynamics of the EKF. The new state x
% includes the posterior expected value (xhat) and the vectorized
% version of the postero variance s
% Thus, if the original state is n dimensional, the state of the
% EKF will have n + n^2 states. 
%
% The input to dyanics Infomax is the n+n^2 dimensional EKF state as a
% vector) and the action vector. The output is the deterministic
% drift for the EKF state, as well as its Jacobian with respect to
% the EKF state and with respect to the action

% We allow the user to choose between two different versions of the
% EKF: 
% Let dY_t =  g(X_t)dt + h dWt
% Version 1 approximates g(x) as follows
% g(x) = g(\hat X_t) + \nabla g(\hat X_t) (x - \hat X_t)
% Version 2 approximates g(x) as follows
% g(x)=(x - alppha) exp(-0.5*(\hat X_t-alpha)'beta(\hat X_t-alpha)) 
% 
% Each version results on different dynamics for the variance of
% the posteior distribution. Thus the value functions provided by
% the two versions are not comparable,i.e, the fact that Version 1
% may produce a controller with a value function larger than
% Version 2, does not mean that Version 1 is better than version
% 2. The reason is that it is possible that Version 1 uses dynamics
% that approximate the true filtering distribution worse, so it
% may not have a good sense of its own uncertainty. The only way to
% compare two solutions is to solve the true non-linear filtering/control
% problem not just find an EKF approximation
%
% Copyright @ Javier R. Movellan, UCSD. 
% MIT stype Open Source License. 




version = metaData.version;
c = metaData.c_dyn;
alpha = metaData.alpha;
beta = metaData.beta;
h = metaData.h_dyn;
n = length(alpha);


hi = inv(h*h')*h;


% we assume the first n elements are the posterior mean, and the
% rest are the vectorized posterior variance
n2 = n*n;
I = eye(n);
xhat = x(1:n);
s = x(n+1:end);
s= reshape(s,n,n);


% first compute the deterministic drift function for xhat and for s
md = (xhat - alpha)'*beta*(xhat-alpha);
eta= kron(I,exp(-md));
nu = (I - (xhat - alpha) *(xhat -alpha)'*beta )* hi;
if (version >1 )
  nu=hi;
end


sdot = c*c' - eta*s*nu*nu'*s;
xhatDot = u;

% vectorize the drift 
xdot = [xhatDot; reshape(sdot,n2,1)];

% The jacobian of the deterministic drift with respect to u
% it should be a (n+n^2) x (n) matrix
Ju= [eye(n,n);zeros(n2,n)];


Jxx =zeros(n,n); % derivative of drift of xhat with respect to xhat
Jxs = zeros(n,n2);% deriv drift of xhat with respect to s

% derivative of drift of s  with respect to s
Jss =  -kron(s'*nu*nu', eta) - kron(I,eta*s*nu*nu');



deta =  - 2*reshape(I,n2,1)*exp(-md)*(xhat-alpha)'*beta;
dnu = -kron(hi*beta*(xhat-alpha),I);
dnu =dnu  - kron(hi*beta,(xhat-alpha));
if (version>1)
  dnu=dnu*0;
end

%%%%%%%% vec transpose matrix Tn
Tn= zeros(n2,n2);
k=1:n2;
rI = 1+n.*(k-1)-(n2-1).*floor((k-1)./n);
I1s = sub2ind([n2 n2],rI,1:n2);
Tn(I1s) =1;
Tn = Tn';
%%%%%%%%%

Jsx = -kron(s*nu*nu'*s,I)* deta;
Jsx = Jsx - kron(s*nu, eta*s)*dnu;
Jsx = Jsx - kron(s,eta*s*nu)*Tn*dnu;


% put it all together into a (n+n^2) x (n+n^2) jacobian matrix
Jx=[Jxx, Jxs;Jsx,Jss];

