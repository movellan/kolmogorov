% iterative linear quadratic tracking for Inverted Pendulum Problem

clear
clf
addpath('..')
%%% begin problem definition
model.dt= 1/100; % sampling time in seconds	       
model.T=13; % simulation time in secs
nt  = ceil(model.T/model.dt); 
model.tau = inf; % discount rate for reward 
model.epsilonMax=1; % max learning rate from 0 to 1. 


ntrials =550; 

nx = 2; % dimensionality of state vector
nu =1; % dimensionality of action vector

model.x0=[pi;0]; % intial state. first is angle, second angular
           % velocity. top is angle zero. angles in radians 

% initial controller
for t=1:nt
  model.w1{t}=0; % open loop part
  model.w2{t}=[0,0]; % closed loop part
end
model.reward =@rewardSolidPendulum;
model.dynamics=@dynamicsSolidPendulum;
%%% end problem definition

%%%% start solving the problem





for trial=1: ntrials % 
  
 		
 if trial==ntrials
   epsilon=1; % run last rial with epsilon=1 to get a large
              % closed loop component
 end
 


%model= ilqt(model)
%model=ilqt2(model);
model= ilqt3(model)





eps(trial) =model.epsilon;

% simulate new controller and display results
x{1} = model.x0;
v(trial)=0;
  for t=1:nt
    u(t)= model.w1{t}+model.w2{t}*x{t};
    [f, Jx, Ju] = model.dynamics(x{t},u(t));
    x{t+1} = x{t}+ f*model.dt +0.0*sqrt(model.dt)*[0 0; 0 1]*randn(2,1);
    r= model.reward(x{t},u(t),model);
    v(trial) = v(trial)+ exp(-t*model.dt/model.tau)*r*model.dt; % value of current trajectory
  end
  r= model.reward(x{t},u(t),model);
  v(trial) = v(trial) +exp(-model.T/model.tau)*r; % add terminal rewrd
    
  % plot results
  ti=[0:nt]*model.dt;  
  
  mx = cell2mat(x);
  subplot(321)
  plot(ti,mx(1,:));
  xlabel('Time (secs)');
  ylabel('Angle in radians: zero upright');
  subplot(322)
  plot(ti,mx(2,:))
  xlabel('Time (secs)');
  ylabel('Angular velocity');
  subplot(323)
  plot([0:nt-1]*model.dt,u)
  xlabel('Time (secs)')
  ylabel('Torque Applied')
  subplot(324)
  plot(mx(1,:),mx(2,:))
  xlabel('Angle');
  ylabel('Angular Velocity');
  
  subplot(325)
  plot(v)
  xlabel('Trial')
  ylabel('Value')
  subplot(326)
  plot(eps)
  xlabel('Trial')
  ylabel('Step Size')
  drawnow
  
end

  

