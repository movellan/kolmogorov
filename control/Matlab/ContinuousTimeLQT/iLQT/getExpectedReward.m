function [v,x,u] = getExpectedReward(w1,w2,model)
reward=model.reward;
dynamics= model.dynamics;

x{1} = model.x0;

v =0;

nt = ceil(model.T/model.dt);
for t=1:nt
  u{t}= w1{t}+w2{t}*x{t};
  [f, Jx, Ju] = dynamics(x{t},u{t},model);
  x{t+1} = x{t}+ f*model.dt;
  x{t+1}(1) = x{t+1}(1) + 0.5*sqrt(model.dt*x{t+1}(3)^40)*randn(1);
   x{t+1}(2) = x{t+1}(2) + 0.5*sqrt(model.dt*x{t+1}(6)^40)*randn(1);

  r= reward(x{t},u{t},model);
  v = v+ exp(-t*model.dt/model.tau)*r*model.dt; % the value of the
end
r= reward(x{t},u{t},model);
v  = v  +exp(-model.T/model.tau)*r; % terminal reward
