% Finite Horizon Optimal Control of 
%  dX_t = k_t + a_t X_t dt  + b_t  U_t  dt + c_t dBt
% with r(x,u,t) = - (x-xi)'*p_t*(x-xi) -  u'*q_t*u;
% r_T(x) = - (x-xi_T)' p_T (x- xi_T)
% v(x,t)  = max_pi {  E[ int_t^T e^{-(s-t)/tau} r(X_s, U_s,t)ds  | X_t= x, pi) 
%              + e^{- (T-t)/tau} r_T(X_T) ] 
% where U_s = pi(X_s)
% 
%  tau =inf  is accepted. 
%
%  The optimal value takes the following form
%  v(x,t) = - x' alpha_t x + 2 beta_t' -  gamma_t
%  and the optimal policy takes the following form
%  u_t = v_t + w_t x_t
%  where
%  v_t = q_t^{-1} b_t' beta_t
%  w_t = -q_t^{-1} b_t' alpha_t
%
% Javier R. Movellan
% UCSD, April 2010, October 2011
%
% 
% xi is a cell array with xi{t} an n_x dimensional vector 
% k is a cell array with k{t} an n_x dimensional vector
% a is a cell array with a{t} an n_x * n_x matrix
% b is a cell array with b{t} an n_x * n_u matrix
% c is a cell array with c{t} an n_x * n_x matrix
% p a cell array with p{t} a symmetric positive definite matrix
% pT is a symetric positive definite matrix
% qinv is a cell array with qinv{t} a symmetric positive definite
% matrix representing the inverse of q_t
% tau in [0, inf], inf is acceptable
% dt >0 
function data= lqt(x,w1,w2,xi,omega,k,a,b,c,p1,p2,q,tau,dt)

  
 
  
  % optimal value function
  oAlpha = p1{end};
  oAlpha=(oAlpha + oAlpha')/2;
  oBeta = p1{end}*xi{end}+p2{end};
  oGamma = xi{end}'*p1{end}*xi{end};

% current value function  
  alpha = p1{end};
  alpha=(alpha + alpha')/2;
  beta = p1{end}*xi{end}+p2{end};
  gamma = xi{end}'*p1{end}*xi{end};

  
  nu = size(b{1},2);
  nx = size(b{1},1);
  
  s = length(xi);
  ow1{s} = zeros(nu,1);
  ow2{s}= zeros(nu,nx);
  dw1{s} =zeros(nu,1);
  dw2{s}= zeros(nu,nx);
  dow1{s} =zeros(nu,1);
  dow2{s}= zeros(nu,nx);
  for t=s:-1: 2
   
   qinv = inv(q{t});
    
    doAlpha=oAlpha/tau-p1{t}+oAlpha*b{t}*qinv*b{t}'*oAlpha-2*oAlpha*a{t};
   
    doBeta=oBeta/tau-p1{t}'*xi{t}- p2{t}+oAlpha*b{t}*qinv*b{t}'*oBeta; 
    doBeta = doBeta +oAlpha*(k{t}+b{t}*omega{t})-a{t}'*oBeta;
    
    
    
    doGamma= oGamma/tau-xi{t}'*p1{t}*xi{t};
    doGamma = doGamma+ oBeta'*b{t}*qinv*b{t}'*oBeta;
    doGamma = doGamma+ 2*oBeta'*(k{t}+b{t}*omega{t})-trace(c{t}*c{t}'*oAlpha);

    
    oAlpha = oAlpha - dt*doAlpha;
    oAlpha=(oAlpha + oAlpha')/2;
	
    oBeta = oBeta - dt*doBeta;

    oGamma = oGamma-dt*doGamma;
    
    dalpha = alpha/tau - p1{t} - w2{t}' *q{t}*w2{t};
    dalpha = dalpha - 2*alpha* (a{t} + b{t}*w2{t});

    
    dbeta = beta/tau - p1{t}*xi{t} -p2{t}+ w2{t}'*q{t}*(w1{t} - omega{t}) ;
    dbeta = dbeta - (a{t}+b{t}*w2{t})' *beta + alpha*(k{t} + b{t}*w1{t});
    
    dgamma = gamma/tau - xi{t}'*p1{t}*xi{t};
    dgamma = dgamma - (w1{t}-omega{t})'*q{t}*(w1{t}-omega{t});
    dgamma = dgamma + 2*beta'*(k{t} + b{t}*w1{t})- trace(c{t}*c{t}'*alpha);

    alpha = alpha - dt*dalpha;
    alpha=(alpha + alpha')/2;
    %alpha
    
    
    beta = beta - dt*dbeta;
    %beta 
    
    
    gamma = gamma-dt*dgamma;
    
    % optimal control law
    % u_t = ow{1,t} + ow_{2,t} x_t
    qinv = inv(q{t-1});

    ow1{t-1} = omega{t-1}+ qinv*b{t-1}'*oBeta;
    ow2{t-1} = - qinv*b{t-1}'*oAlpha;
  
    %gamma
    %W1 = ow1{t-1}
    %W2= ow2{t-1}
    %pause
    
    % gradient of value at current law
    % gradient with respect to w1_t is theta1_t + theta2_t*x
    % gradient with respect to w2_t is (theta1_t + theta2_t*x)*x'
    theta1{t-1} = 2*b{t-1}'*beta + 2*q{t}*(omega{t-1} - w1{t-1});


    
    theta2{t-1} = -2*(q{t}*w2{t-1} + b{t-1}'*alpha);

    
    dw1{t-1}= (theta1{t-1} + theta2{t-1}*x{t-1});
    dw2{t-1} = dw1{t-1}*x{t-1}';
    
    
    dow1{t-1} = ow1{t-1}-w1{t-1};
    dow2{t-1} = ow2{t-1} - w2{t-1};
    
    du(:,t-1) = dw1{t-1} + dw2{t-1}*x{t-1};
    dou(:,t-1) = (ow1{t-1}- w1{t-1})+ (ow2{t-1} - w2{t-1})*x{t-1};


   
  end
  data.dw1=dw1;
  data.dw2=dw2;
  data.ow1=ow1;
  data.ow2=ow2;
  
  data.alpha = alpha;
  data.beta=beta;
  data.gamma=gamma;
  
  data.oAlpha=oAlpha;
  data.oBeta = oBeta;
  data.oGamma=oGamma;
  
  tmp1 = reshape(du,[],1);
  tmp2= reshape(dou,[],1);
  data.corr1 = tmp1'*tmp2/(norm(tmp1)*norm(tmp2));

  t1= cell2mat(dow1);
  t1=reshape(t1,[],1);
    t2= cell2mat(dow2);
  t2=reshape(t2,[],1);
  
  
  tmp1 = [t1;t2];
  t1= cell2mat(dw1);
  t1=reshape(t1,[],1);
  t2= cell2mat(dw2);
  t2=reshape(t2,[],1);
  tmp2=[t1;t2];
  

  data.corr2=tmp1'*tmp2/(norm(tmp1)*norm(tmp2));
  data.scale=norm(tmp2)/norm(tmp1);

 