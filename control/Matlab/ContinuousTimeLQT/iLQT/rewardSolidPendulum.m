function [r, p1,p2,xi,q,omega] = rewardSolidPendulum(x,u,trial)
% we assume reward is of the form 
% r(x,u) = r_1(x) + r_2(u)
% we approximate each component as follows
% r_1(x) approx    -(x - xi)' p1 (x-xi) + 2p2' x
% r_2(x) approx    -(u - omega)' q(u-omega) + c
% 
% p1 = -1/2 H_x 
% xi = 0
% p2 = 0.5*(G_x - H_x *x)
% q = -1/2 *H_u
% omega =  u - (H_u)^{-1} G_u
% where H_x, H_u are Hessians with respect to x,u
%       G_x, G_u are Gradients with respect to x,u 

q_ = 0.04;


p_ = [1 0; 0 0.1];
r = -u'*q_*u - x'*p_*x;

Gx = - 2*p_*x;
Hx = -2 * p_;

Gu = -2*q_*u;
Hu = -2*q_ ;

p1 = -Hx/2;
p2 = 0.5*(Gx- Hx*x);
xi = zeros(size(x));

iHu = inv(Hu);
q= -Hu/2;
omega = u - iHu*Gu;

