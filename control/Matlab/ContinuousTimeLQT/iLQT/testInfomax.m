% Infomax control of partially  observable process
% Consider the process defined by 
%dXt = Ut + c dBt
%dYt =  (X_t- \alpha) exp(-0.5(X_t - alpha)'beta(X_t -alpha)')  + h  dWt
% where X is the unboservable state and dY_t the observation. 
% The reward rate is 
% r(X_t,U_t) = -Xt'p Xt - Ut' q Ut
% 
% Thus the goal is to get X as close as possible to zero utilizing
% minimum energy. 
% 
% The observation model represents a 2D "light" centered at alpha. 
% So in order to get to the target point (0,0) we first need to get
% to the light to figure out where we are. 
%
% We convert the partially observable problem into a fully
% observable EKF where the states are the posterior mean xhat and
% the posterior variance s as a vector.  
% Thus if unboservable X_t has 2 dimensions, the observable EKF
% state has 2 +4 = 6 dimensions.
%
% We then use ilqt to solve the control problem in the
% 6-dimensional state. 
%
% Let dYt = g(X_t) dt + h dWt
% We can try two versions. 
% Version 1 approximates g(x) as follows
% g(x) = g(Xhat) + \nabla_x g(Xhat)' ( x - Xhat)
% where xHat is the posterior mean of the EKF filter
% and \nabla g(Xhat) is the graident of g(x) at Xhat
% Version 2 approximates g(x) as follwos
% g(x) = (x-alpha) exp( -0.5(xHat - alpha) ' beta(xHat - alpha))
%
% The value functions provided by the two versions are not
% comparable since they are different approximations of the true
% value function. So the fact that Version 2 produces a controller
% with a larger value function than Version 1 does not mean
% anything since the approximation used by Version 2 may be worse
% than the approximation used by Vesion 1.
% Overall, Version 1 seems to work better in that the solutions it
% finds appear to make more sense. In addition there are some cases
% where Version 2 clearly fails but Version 1 does not. 

% Copyright Javier R. Movellan, UCSD
% MIT style open source license 

clear; clf

%%%%%% Set model parameters %%%%%%%%%%%%%

model.version =1;
model.epsilonMax=0.2; % Max learning rate from 0 to 1. 
                %Sometimes we get better solutions by using small values

ntrials =500; % number of ilqt iterations

model.dt= 1/100; % sampling time in seconds	       
model.T=6; % simulation time in secs
model.tau = inf; % time scale of discount rate (tested only for tau = inf)
	       
nx = 6; % dimensionality of EKF state vector
nu =2; % dimensionality of action vector

x0=[2;-1]; %mean of prior state distribution 
x0=[-2;2]; %mean of prior state distribution 
s0 = 1*eye(2); % variance of prior state distribution
model.x0=[x0; reshape(s0,4,1)]; % the state is the mean and the vectorized
                          % variance matrix (2 + 4 = 6 dimensions)

model.c_dyn=0.1*eye(2); % dispersion of unobservable state
model.h_dyn = 1*eye(2); 


model.alpha=[1;1]; % location of the light

model.beta=2*[1 0.9; 0.9 1]; % precission matrix of the light

model.p_dyn= 1.5*eye(2); % cost matrix for deviation from origin
model.q_dyn = 0.1*eye(2); % cost matrix for action



model.dynamics=@dynamicsInfomax; % the EKF 6-d state dynamics
model.reward = @rewardInfomax; % the reward function

% if this variable is not set the default is a zero action 
% initial controller
%fileWithInitialController='infomaxController';




fileToSaveFinalController='infomaxController';


%%%%%%%%%%%%%% end of problem parameters %%%%%%%%%%%

			  
	

nt = ceil(model.T/model.dt);
% set initial controller
if( exist('fileWithInitialController')==0)
  for t=1:nt
    model.w1{t}=zeros(nu,1); % open loop input term
    model.w2{t}=zeros(nu,nx); %  closed loop gain matrix
  end
else
  load(fileWithInitialController)
  for t=1:nt
    model.w1{t}=cont.w1{t}; % open loop input term
    model.w2{t}=cont.w2{t}; %  closed loop gain matrix
  end
  
end



%%%%%%%% prepare image for display
kk=0;
for ii= -2:0.01:2
  kk=kk+1;
  ll=0;
  for jj= -2:0.01:2
    ll=ll+1;
    uu =[ii-model.alpha(1);jj-model.alpha(2)];
    yy(kk,ll)= exp(-uu'*model.beta*uu);
  end
end

%%%%%%%%%%%%%%%%%%% 


%%%%%%%%%%%% start using ilqt %%%%%%%%%%
for trial=1:ntrials
  trial
  
  %  test the current  controller
  
 [v_,x,u]=getExpectedReward(model.w1,model.w2,model); 
  v(trial)=v_;
  
  %%%%%%%%%% get new controller
  if trial < ntrials
    % here we use nIterations of ilqt to get the new controller
    model= ilqt(model);
    eps(trial) = model.epsilon;
   
    
    %%%%%%%%%% plot results %%%%%%%%%%
    subplot(221)
    imshow(yy,[])
    hold on
    k1= size(yy,1);
    mx = cell2mat(x);
    mx = mx*k1/4+k1/2;
    mc= k1/2;
    plot(mx(1,:), mx(2,:),'--r','linewidth',2)
    hold on
    scatter([ mc mc],[mc,mc],100,'filled')
    hold off
    
    subplot(222)
    plot([0:length(mx)-1]*model.dt,mx(3,:) +mx(4,:))
    ylabel('Variance (Trace)')
    xlabel('Time');
    drawnow
    display('press key to continue')
    
    subplot(223)
    plot(v)
    xlabel('Trial')
    ylabel('Value')
    subplot(224)
    plot(eps)
    xlabel('Trial')
    ylabel('Step Size')
  end
end

% save final controller
if (exist('fileToSaveFinalController') ==1)
  for t=1:nt
    cont.w1{t}=model.w1{t}; % open loop input term
    cont.w2{t}=model.w2{t}; %  closed loop gain matrix
  end

  save(fileToSaveFinalController,'cont');
end
