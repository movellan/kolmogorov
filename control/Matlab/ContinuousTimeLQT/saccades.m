% Javier R. Movellan
% April 2010
%
% lqt model of saccades

clf
clear


m.tau =inf; % Time constant of the future discount in seconds. 
           % for t = tau seconds, the reward importance decays by
           % 100/e = 36.79 %

m.nx=2;
m.nu=1;

m.T = 40/1000; % in seconds


m.dt = 1/1000; % time step in seconds



s= ceil(m.T/m.dt);

% continuous time version of saccade m in Haiyin Chen-Harris,
% Wilsaan M. Joiner, Vincent Ethier, David S. Zee, and Reza
% Shadmehr
% in discrte time with dt = 1ms and x_{t+1} = a x_t + b u_t
% they use a =[ 0.9998 0.00096; -0.329 0.922] 
% and b =[0.000167; b= 0.33];
% this corresponds to the following continuous time matrices when
% the time units are seconds. 

fa=[ -0.2 0.96 ; -329 -78];
fb=[ 0.1670 ;330];

m.h{1} = 0.1*[fb(1) 0; fb(2) 0];





for t=1:s
  m.xi{t}  =[5;0]; % state cost is (x_t - xi_t)' p_t (xi_t - x_t)
  m.omega{t} =0; % action cost is (u_t - omega_t)' q_t (u_t - omega_t)
  
  m.k{t} = [0;0]; %  a constant external force applied to the system
   m.a{t}=fa;
   m.b{t}=fb;
 
   m.c{t} = 0.5*eye(m.nx);
   m.q{t} =1.5;
  
  if (t> s-1)
    m.p1{t} = 1000*[ 1 0 ; 0 1 ]; % quadratic state cost 
  else
    m.p1{t} = 0*[ 0  0 ; 0 0 ]; % quadratic state cost 
  end
  
  
  m.p2{t} = [0 ;0]; % linear state cost
  x{t} = zeros(m.nx,1);
  m.w1{t} = zeros(m.nu,1);
  m.w2{t}= zeros(m.nu,m.nx);
  
end


m = lqt2(m);

% % Here we run the system with the optimal controller

u = zeros(m.nu,s);
sqdt = sqrt(m.dt);for t=1:s-1

  u(:,t)= m.w1{t}+ m.w2{t} * x{t};
  dx = (m.k{t}+m.a{t}*x{t}  +m.b{t}*u(:,t))*m.dt ;
  x{t+1} = x{t}+ dx;
 end

 subplot(311)
 mxi = cell2mat(m.xi);
 
 mx = cell2mat(x);
 
 plot(1000*[1:s]*m.dt,mx(1,:),1000*[1:s]*m.dt,mxi(1,:))
 
 legend('X_1', '\xi','Location','Best');


 subplot(312)
 mxi = cell2mat(m.xi);
 
 mx = cell2mat(x);
 
 plot(1000*[1:s]*m.dt,mx(2,:))
 

load saccadeData;
hold on
scatter(sdat.x,sdat.y5,'r')
scatter(sdat.x,sdat.y10,'r')
scatter(sdat.x,sdat.y20,'r')
scatter(sdat.x,sdat.y30,'r')
scatter(sdat.x,sdat.y50,'r')
scatter(sdat.x,sdat.y60,'r')
scatter(sdat.x,sdat.y70,'r')
scatter(sdat.x,sdat.y80,'r')

subplot(313)



plot([1:s-1]*m.dt,u(1:end-1));
hold on
plot([0 s*m.dt], [0 0], 'r--');
xlabel('Time (seconds)');
ylabel('Action')



