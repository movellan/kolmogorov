% Linear Quadratic Tracker
%  dX_t = k_t + a_t X_t dt  + b_t  U_t  dt 
%      + (c_t+ \sum_i  X_ig_i + \sum_j U_j h_j) dBt
% with r(x,u,t) = - (x-xi)'*p_t*(x-xi) -  u'*q_t*u;
% v(x,t)  = max_pi {  E[ int_t^T e^{-(s-t)/tau} r(X_s, U_s,t)ds  | X_t= x, pi) 
%              
% where U_s = pi(X_s)
% 
%  tau =inf  is accepted. 
%
%  The optimal value takes the following form
%  v(x,t) = - x' alpha_t x + 2 beta_t' -  gamma_t
%  and the optimal policy takes the following form
%  u_t = w1_t + w2_t x_t
%  
% Javier R. Movellan
% UCSD, April 2010, October 2011
%
% 
% m.xi is a cell array with xi{t} an n_x dimensional vector 
% m.k is a cell array with k{t} an n_x dimensional vector
% m.a is a cell array with a{t} an n_x * n_x matrix
% m.b is a cell array with b{t} an n_x * n_u matrix
% m.c is a cell array with c{t} an n_x * n_b matrix
% m.gg is a cell array with n_x matrices each n_x * n_b
% m.h is a cell array with n_u matrices each n_x * n_b
% m.p a cell array with p{t} a symmetric positive definite matrix
% m.q is a cell array with q{t} a symmetric positive definite
% m.tau in [0, inf], inf is acceptable
% m.dt >0 
% Todo: right now dimensionality of dB_t is same as X_t. Allow nx
% and n_b to be different
function m= lqt2(m) 

  nu = size(m.b{1},2);
  nx = size(m.b{1},1);

  alpha = m.p1{end};
  alpha=(alpha + alpha')/2;
  beta = m.p1{end}*m.xi{end}+m.p2{end};
  gamma = m.xi{end}'*m.p1{end}*m.xi{end};
  
  s = length(m.xi);
  m.w1{s} = zeros(nu,1);  % open loop part of controller
  m.w2{s}= zeros(nu,nx);  % closed loop control gain matrix

  if(isfield(m,'g')==0)
    for i=1:nx 
      m.g{i} = zeros(nx);
    end
  end
  if(isfield(m,'h')==0)
    for i=1:nu
      m.h{i} = zeros(nx);
    end
  end
  

  for t=s:-1: 1
    for i=1:nx
      gbar(i,1) = trace(m.g{i}*m.c{t}'*alpha);
      for j=1:nx
	ghat(i,j) = trace(m.g{i}*m.g{j}'*alpha);
      end
      for j=1:nu
	fhat(j,i) = trace(m.h{j}*m.g{i}'*alpha);
      end
    end
    for i=1:nu
      hbar(i,1) = trace(m.h{i}*m.c{t}'*alpha);
      for j=1:nu
	hhat(i,j) = trace(m.h{i}*m.h{j}'*alpha);
      end
    end

    qbar = m.q{t} + hhat;
    
    
    qhatinv=pinv(qbar);
    
    m.w1{t}= qhatinv*(m.q{t}*m.omega{t}+m.b{t}'*beta- hbar);
    m.w2{t} = -qhatinv*(m.b{t}'*alpha+ fhat);
    if(t>1)
      dalpha=alpha/m.tau-m.p1{t};
      dalpha = dalpha +m.w2{t}'*qbar*m.w2{t};
      dalpha = dalpha -2*alpha*m.a{t};
      dalpha = dalpha -ghat;
      
      dbeta=beta/m.tau-m.p1{t}*m.xi{t}- m.p2{t};
      dbeta = dbeta - m.w2{t}'*qbar'*m.w1{t};
      dbeta = dbeta - m.a{t}'*beta+gbar;
      
      dgamma= gamma/m.tau-m.xi{t}'*m.p1{t}*m.xi{t};
      dgamma = dgamma+ m.w1{t}'*qbar*m.w1{t};
      dgamma = dgamma- m.omega{t}'*m.q{t}*m.omega{t};
      dgamma = dgamma - trace(m.c{t}*m.c{t}'*alpha);
      
      alpha = alpha - m.dt*dalpha;
      alpha=(alpha + alpha')/2;
      
      beta = beta - m.dt*dbeta;
      gamma = gamma-m.dt*dgamma;
    end
  end
  m.alpha=alpha;
  m.beta = beta;
  m.gamma=gamma;
  
