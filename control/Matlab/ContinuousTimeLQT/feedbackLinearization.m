% Test of LQR and LQT based  control of 1df robot arm

% 
% Javier R. Movellan
% April 2010
%

clf
clear

% First we compute the gains for the virtual system

nx =6; % dimensionality of the state
nu=2; % diensionality of action

I = eye(2);
O = 0*I;
a = [ O I O; O O I; O O O];
b= [ O;O;I];



c = 0.001*eye(nx); % noise 

q = 400*eye(nu) ; % virtual action cost



p =  4000*diag([0.1,0.1, 100,100, 0.1,0.1]) ; % virtual state cost

pT=  10*p; %Terminal virtual state cost


tau = inf; % Time constant of the future discount in seconds. 
           % for t = tau seconds, the reward importance decays by
           % 100/e = 36.79 %



dt =0.001; % time step in seconds

T = 20000*dt; % in seconds


sqdt = sqrt(dt);
s= ceil(T/dt);



% Compute desired trajectory

f = 1/4; % frequency in Hz for drawing of circle


T2 = 2;
s2=floor(T2/dt);
s = s2;
T= T2;
xi = zeros(2,s2);
xiV = zeros(2,s2);
xiA = zeros(2,s2);
xiBar = zeros(2,s2);
r= 100/100; % lenght of desired radius in meters 
x1 = 0; x2= -r;
delta = 2*r/(s2/4);
for t=1:s2
    if t<s2/8
        x1 = x1 -delta;
    elseif t< s2/8 + s2/4
        x2 = x2+ delta;
    elseif t< s2/8+s2/2
        x1 = x1+delta;
    else
        x2 = x2 - delta;
    end

    xi(1,t) = sqrt(x1^2+x2^2);
    xi(2,t) = atan2(x1,x2);
    if(x1<0 ) 
        xi(2,t) = 2*pi+xi(2,t);
    end
    
    if(x1>0 && x2>0 ) 
        xi(2,t) = pi+xi(2,t)+pi;
    end

    if(x1>0 && x2<0 && t> s2/2) 
        xi(2,t) = pi+xi(2,t)+pi;
    end
    if(t>1)
        xiV(:,t) = (xi(:,t) - xi(:,t-1))/dt;
        xiBar(:,t) = xiBar(:,t-1) + (xi(:,t)-xi(:,t-1))*dt;
    end
    if(t>2)
        xiA(:,t) = (xiV(:,t) - xiV(:,t-1))/dt;
    end
    
    
end

xi= [ xiBar; xi;xiV];
[omega, k]= oldlqt(xi,a,b,c,p,pT,q,tau,dt);





%keyboard


% Simulate robot


m = 0.5; % mass in kg
g = 9.81; % gravitational acceleration in m/s2
nu1 = 0.1; % radial friction (Newt/m/sec)
nu2 = 0.1;% rotational friction (Newt/(rad/sec))
nu = [nu1 0; 0 nu2]; 
lambda = 0.1; % regularizer 

theta= zeros(2,s2);
torque= zeros(2,s2);
thetaV= zeros(2,1);
thetaA= zeros(2,1);

theta(:,1) = xi(3:4,1)';
thetaBar = theta(:,1);

sigmaA = 0;
sigmaV = 0.0;
sigmaP = 0.0;

for t=1:s2-1
    
    M = m*[ 1, 0; 0, theta(1,t)^2]; % Moment of inertia
    C = m*[ 0, -theta(1,t)*thetaV(2);  theta(1,t)*thetaV(2), ...
            theta(1)*thetaV(1)];
    
    F= - nu * thetaV;
    G = m*g*[ - cos(theta(2,t)) ; theta(1,t)*sin(theta(2,t))];
    N = F+G - C*thetaV;
    
    if (mod(t,150) ==0)
        ep= [ omega(3:4,t) - theta(:,t)];     
        eV= [ omega(5:6,t) - thetaV];
       
        eI = [omega(1:2,t) - thetaBar];
        kI = k(:,1:2,t);
        kP = k(:,3:4,t);;
        kD = k(:,5:6,t);
        torque(:,t)= kP*ep;
        torque(:,t)= torque(:,t)+kD*eV;
        torque(:,t)= torque(:,t)+kI*eI;
    
    end
   
     torque(:,t+1)= torque(:,t);
    thetaA = pinv(M+ lambda*eye(2))*( N -1*N+  +torque(:,t) + sigmaA*sqdt*randn(2,1));

    thetaV = thetaV + thetaA*dt + sigmaV*sqdt*randn(2,1);
    theta(:,t+1) = theta(:,t)+ dt*thetaV + thetaA*dt*dt/2 + sigmaP*sqdt*randn(2,1);
    thetaBar = thetaBar+ (theta(:,t+1)-theta(:,t))*dt;

end

x1= theta(1,:).*sin(theta(2,:));
x2= theta(1,:).*cos(theta(2,:));

d1= xi(3,:).*sin(xi(4,:));
d2= xi(3,:).*cos(xi(4,:));

o1= omega(3,:).*sin(omega(4,:));
o2= omega(3,:).*cos(omega(4,:));

             
subplot(2,2,1)
plot(x1,x2,d1,d2,'r',o1,o2,'r--');


hold on

xlabel('X_1 (meters)')
ylabel('X_2 (meters)')
xlim([-3 3])
ylim([-3 3])



subplot(2,2,2)
plot([1:s2]*dt,theta(1,:),[1:s2]*dt,xi(1,:),'r--');
xlabel('Time (secs)');
ylabel('\theta_1, \xi_1 (meters)');


subplot(2,2,3)
plot([1:s2]*dt,theta(2,:)*360/2/pi,[1:s2]*dt,xi(2,:)*360/2/pi,'r--');
xlabel('Time (secs)');
ylabel('\theta_2, \xi_2 (degrees)');


subplot(2,2,4)
plot([1:s2]*dt,torque(1,:),[1:s2]*dt,torque(2,:));
xlabel('Time (secs)');
ylabel('Torque');
legend('torque_1 (Newtons)', 'torque_2 (Newton meters)')

