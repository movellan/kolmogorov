% Test the continuous time finite horizon linear quadratic tracker
% 
% Javier R. Movellan
% April 2010
%
%  We ask the system to track a step function. Note hos the system
%  anticipates when the step function will occur and starts acting accordingly

clf
clear

% a second order system. 
% x(1,t)= state; 
% x(2,t) = velocity of state
%
% dx_t = (k+ a x + bu) dt + c dBt 
%

nx =2; % dimensionality of the state
nu=1; % diensionality of action (a force)


% we optimize
% v(x,t)  = max_pi {  E[ int_t^T e^{-(s-t)/tau} r(X_s, U_s,t)ds  | X_t= x, pi) 
%              + e^{- (T-t)/tau} r_T(X_T) ] 
% with r(x,u,t) = - (x-xi)'*p_t*(x-xi) -  u'*q_t*u;
% r_T(x) = - (x-xi_T)' p_T (x- xi_T)


tau =inf; % Time constant of the future discount in seconds. 
           % for t = tau seconds, the reward importance decays by
           % 100/e = 36.79 %

T = 4; % in seconds

dt = 1/1000; % time step in seconds




s= ceil(T/dt);

for t=1:s
  xi{t}  =[0;0]; % state cost is (x_t - xi_t)' p_t (xi_t - x_t)
  if(t>s/2) xi{t}= [1;0];end

  omega{t} =0; % action cost is (u_t - omega_t)' q_t (u_t - omega_t)
  
  k{t} = [0;0]; %  a constant external force applied to the system
  a{t} = [ 0 1; 0 0];
  
  b{t} = [0; 1]; % the action is a force. i.e, it has an effect on the
  
  
  c{t} = 0.0*eye(nx);
  q{t} = 0.01;
  
  
  p1{t} = [ 1 0 ; 0 0 ]; % quadratic state cost 
  p2{t} = [0 ;0]; % linear state cost
  x{t} = zeros(nx,1);
  w1{t} = zeros(nu,1);
  w2{t}= zeros(nu,nx);
  
end


data= lqt(x,w1,w2,xi,omega,k,a,b,c,p1,p2,q,tau,dt);

%data = lqt(xi,omega,k,a,b,c,[],[],p1,p2,q,tau,dt);
%datab= lqt3(x,w1,w2,xi,omega,k,a,b,c,p1,p2,q,tau,dt);






% % Here we run the system with the optimal controller

u = zeros(nu,s);
sqdt = sqrt(dt);for t=1:s-1

  u(:,t)= data.ow1{t}+ data.ow2{t} * x{t};
  dx = (k{t}+a{t}*x{t}  +b{t}*u(:,t))*dt + c{t}*randn(nx,1)*sqdt;
   x{t+1} = x{t}+ dx;
 end



 subplot(211)
 xi = cell2mat(xi);
 
 mx = cell2mat(x);
 
 plot([1:s]*dt,mx(1,:),[1:s]*dt,xi(1,:))
 
 legend('X_1', '\xi','Location','Best');
 title('dx = a x dt + b u dt + c dB; a=[0 1; 0 0]; b =[0 1]')
 
 


 subplot(212)



 plot([1:s]*dt,u);
 hold on
 plot([0 s*dt], [0 0], 'r--');
 xlabel('Time (seconds)');
 ylabel('Action')
 title('dx = a x dt + b u dt + c dB; a=[0 1; 0 0]; b =[0 1]')



