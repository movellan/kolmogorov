% lqt model of saccades

clf
clear
q=10; % action cost
p=0.1; % state cost
h=10/1000; % action dependent noise
dt=1/10000; % sampling period 
c=0; % action independent noise
tau = inf; % time constant discount rate
experiment=10; % experimental condition (1 through 9, see below)


% q=60/1000; % action cost
% p=10; % state cost
% h=20/1000; % action dependent noise
% dt=1/10000; % sampling period 
% c=30; % action independent noise
% tau = 8000/1000; % time constant discount rate
% experiment=2; % experimental condition (1 through 9, see below)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
condition{1}.degrees=5;
condition{1}.msecs=40;
condition{2}.degrees=10;
condition{2}.msecs=50;
condition{3}.degrees=20;
condition{3}.msecs=70;
condition{4}.degrees=30;
condition{4}.msecs=100;
condition{5}.degrees=40;
condition{5}.msecs=120;
condition{6}.degrees=50;
condition{6}.msecs=160;
condition{7}.degrees=60;
condition{7}.msecs=180;
condition{8}.degrees=70;
condition{8}.msecs=200;
condition{9}.degrees=80;
condition{9}.msecs=260;

condition{10}.degrees=50;
condition{10}.msecs =200;

m.tau =tau; % Time constant of the future discount in seconds. 
           % for t = tau seconds, the reward importance decays by
           % 100/e = 36.79 %

m.nx=2;
m.nu=1;

m.T = condition{experiment}.msecs/1000; % in seconds


m.dt = dt; % time step in seconds



s= ceil(m.T/m.dt);

% continuous time version of saccade m in Haiyin Chen-Harris,
% Wilsaan M. Joiner, Vincent Ethier, David S. Zee, and Reza
% Shadmehr
% in discrte time with dt = 1ms and x_{t+1} = a x_t + b u_t
% they use a =[ 0.9998 0.00096; -0.329 0.922] 
% and b =[0.000167;  0.33];
% this corresponds to the following continuous time matrices when
% the time units are seconds. 
% discrete time model
fa= ([0.9998 0.00096; -0.329 0.922] - eye(2))/0.001
fb = [0.000167; 0.33]/0.001

% continuous time model 
k1 = 3; % Newton Meters/Rad (elastic force)
k2 = 0.65 %Newton Meters/(rad/sec) (viscous force)
me = 0.004; %(Kg meter/rad)
% convert to degrees
k1= k1*2*pi/360; %Newton meters/degree
k2 = k2* 2*pi/360; % Newton Meters/(deg/sec)
me = me*2*pi/360;% (Kg m)/deg

fa=[ 0 1; - k1/me,-k2/me]
fb = [ 0 ; 1/me]




m.h{1} = h*[fb(1) 0; fb(2) 0];

%m.h{1} = h*[fb(1) 0; 0 fb(2) ];%




for t=1:s
  m.xi{t}  =[condition{experiment}.degrees;0]; % state cost is (x_t - xi_t)' p_t (xi_t - x_t)
  m.omega{t} =0; % action cost is (u_t - omega_t)' q_t (u_t - omega_t)
  
  m.k{t} = [0;0]; %  a constant external force applied to the system
   m.a{t}=fa;
   m.b{t}=fb;
 
   m.c{t} = c*eye(m.nx);
   m.q{t} =q;
  
  if (t*m.dt > 160/1000)
    m.p1{t} = p*[ 1 0 ; 0 1 ]; % quadratic state cost 
  else
    m.p1{t} = p*[ 0  0 ; 0 0 ]; % quadratic state cost 
  end
  
  
  m.p2{t} = [0 ;0]; % linear state cost
  x{t} = zeros(m.nx,1);
  m.w1{t} = zeros(m.nu,1);
  m.w2{t}= zeros(m.nu,m.nx);
  
end


m = lqt3(m);

% % Here we run the system with the optimal controller

u = zeros(m.nu,s);
sqdt = sqrt(m.dt);for t=1:s-1

  u(:,t)= m.w1{t}+ m.w2{t} * x{t};
  dx = (m.k{t}+m.a{t}*x{t}  +m.b{t}*u(:,t))*m.dt ;
  x{t+1} = x{t}+ dx;
 end

 subplot(311)
 mxi = cell2mat(m.xi);
 
 mx = cell2mat(x);
 
 plot(1000*[1:s]*m.dt,mx(1,:),1000*[1:s]*m.dt,mxi(1,:))
xlabel('Time (seconds)');
ylabel('Angle (degrees)')




 subplot(312)
 mxi = cell2mat(m.xi);
 
 mx = cell2mat(x);
 
 plot(1000*[1:s]*m.dt,mx(2,:))
 

load saccadeData;
hold on
% scatter(sdat.x,sdat.y5,'r')
% scatter(sdat.x,sdat.y10,'r')
% scatter(sdat.x,sdat.y20,'r')
% scatter(sdat.x,sdat.y30,'r')
% scatter(sdat.x,sdat.y40,'r')
% scatter(sdat.x,sdat.y50,'r')
% scatter(sdat.x,sdat.y60,'r')
% scatter(sdat.x,sdat.y70,'r')
% scatter(sdat.x,sdat.y80,'r')
xlabel('Time (seconds)');
ylabel('Angular Velocity (degrees/sec)')


subplot(313)



plot([1:s-1]*m.dt,u(1:end-1));
hold on
plot([0 s*m.dt], [0 0], 'r--');
xlabel('Time (seconds)');
ylabel('Action')



