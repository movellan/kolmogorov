% Test the continuous time finite horizon linear quadratic tracker
% 
% Javier R. Movellan
% April 2010
%

clf
clear



nx =2; % dimensionality of the state
nu=2; % diensionality of action

m.tau = inf; % Time constant of the future discount in seconds. 
           % for t = tau seconds, the reward importance decays by
           % 100/e = 36.79 %



m.dt =1/5000; % time step in seconds

m.T = 2; % in seconds
x0=[1;-2];



s= ceil(m.T/m.dt);

% Model is of the form
%  dX_t = k_t + a_t X_t dt  + b_t  U_t  dt 
%      + (c_t+ \sum_i  X_ig_i + \sum_j U_j h_j) dBt
% with r(x,u,t) = - (x-xi)'*p_t*(x-xi) -  u'*q_t*u;
% v(x,t)  = max_pi {  E[ int_t^T e^{-(s-t)/tau} r(X_s, U_s,t)ds  | X_t= x, pi) 
%              
% where U_s = pi(X_s)
% 
%  tau =inf  is accepted. 
%
%  The optimal value takes the following form
%  v(x,t) = - x' alpha_t x + 2 beta_t' -  gamma_t
%  and the optimal policy takes the following form
%  u_t = w1_t + w2_t x_t



for t=1:s
  m.xi{t}  =[0;0];
  m.omega{t}=[0;0];
  m.k{t} = [0;0];
  m.a{t} = 0.1*[ 1 -2; 1 1] ;
  m.b{t} = eye(2);
  m.c{t} = 0*eye(2);
  m.q{t} = 0.2*eye(2);
  m.p1{t} = 1*eye(2);
  m.p2{t}=zeros(nx,1);
  m.w1{t} = zeros(nu,1); 
  m.w2{t} = zeros(nu,nx);
  x{t} = zeros(nx,1);
end



m= lqt3(m);


% now let's compare to Matlab's infinite horizon lqr solution
gain = lqr(m.a{1},m.b{1},m.p1{1},m.q{1},zeros(nx));
% controller is of form u_t = -k* x_t

disp(sprintf('Finite Horizon LQR Initial Gain:'))
-m.w2{1}
disp(sprintf('Matlab Infinite Horizon LQR Gain:'))
gain


if(norm(gain+m.w2{2}) + norm(m.w1{1}) < 0.001)
  disp(sprintf('Unit Test Passed'));
else
  disp(sprintf('Unit Test Failed'));
end
