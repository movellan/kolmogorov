% Test the continuous time finite horizon linear quadratic tracker
% 
% Javier R. Movellan
% April 2010
%
%  We ask the system to track a step function. Note hos the system
%  anticipates when the step function will occur and starts acting accordingly

clf
clear

% a second order system. 
% x(1,t)= state; 
% x(2,t) = velocity of state
%
% dx_t = (k+ a x + bu) dt + c dBt 
%

nx =2; % dimensionality of the state
nu=1; % diensionality of action (a force)


% we optimize
% v(x,t)  = max_pi {  E[ int_t^T e^{-(s-t)/tau} r(X_s, U_s,t)ds  | X_t= x, pi) 
%              + e^{- (T-t)/tau} r_T(X_T) ] 
% with r(x,u,t) = - (x-xi)'*p_t*(x-xi) -  u'*q_t*u;
% r_T(x) = - (x-xi_T)' p_T (x- xi_T)


tau =inf; % Time constant of the future discount in seconds. 
           % for t = tau seconds, the reward importance decays by
           % 100/e = 36.79 %

T = 4; % in seconds

dt = 1/1000; % time step in seconds




s= ceil(T/dt);

for t=1:s
  xi{t}  =[0;0]; % state cost is (x_t - xi_t)' p_t (xi_t - x_t)
  if(t>s/2) xi{t}= [1;0];end

  omega{t} =0; % action cost is (u_t - omega_t)' q_t (u_t - omega_t)
  
  k{t} = [0;0]; %  a constant external force applied to the system
  a{t} = [ 0 1; 0 0];
  
  b{t} = [0; 1]; % the action is a force. i.e, it has an effect on the
  
  
  c{t} = 0.0*eye(nx);
  q{t} = 0.01;
  
  
  p1{t} = [ 1 0 ; 0 0 ]; % quadratic state cost 
  p2{t} = [0 ;0]; % linear state cost
  x{t} = zeros(nx,1);
  w1{t} = zeros(nu,1);
  w2{t}= zeros(nu,nx);
  
end


data= lqt(x,w1,w2,xi,omega,k,a,b,c,p1,p2,q,tau,dt);

ut1(1) = 0.3003;
ut2(1) =  -0.3003;
ut2(2) = -0.0006;

sse= norm(data.ow1{end-3}-ut1)+norm(data.ow2{end-3}-ut2);

if(sse < 0.000001) 
  disp(sprintf('Unit Test Passed'));
else
  disp(sprintf('Unit Test Failed'));
end




