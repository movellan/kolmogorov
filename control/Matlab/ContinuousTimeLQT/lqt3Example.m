% Test the continuous time finite horizon linear quadratic tracker
% 
% Javier R. Movellan
% April 2010
%
%  We ask the system to track a step function. Note hos the system
%  anticipates when the step function will occur and starts acting accordingly

clf
clear

% a second order system. 
% x(1,t)= state; 
% x(2,t) = velocity of state
%
% dx_t = (k+ a x + bu) dt + c dBt 
%

nx =2; % dimensionality of the state
nu=1; % diensionality of action (a force)


% we optimize
% v(x,t)  = max_pi {  E[ int_t^T e^{-(s-t)/tau} r(X_s, U_s,t)ds  | X_t= x, pi)] 
%            
% with r(x,u,t) = - (x-xi)'*p_t*(x-xi) -  u'*q_t*u;
% r_T(x) = - (x-xi_T)' p_T (x- xi_T)


m.tau =inf; % Time constant of the future discount in seconds. 
           % for t = tau seconds, the reward importance decays by
           % 100/e = 36.79 %

m.T = 4; % in seconds

m.dt = 1/320; % time step in seconds

% Model taken by lqt is of the form
%  dX_t = k_t + a_t X_t dt  + b_t  U_t  dt 
%      + (c_t+ \sum_i  X_ig_i + \sum_j U_j h_j) dBt
% with r(x,u,t) = - (x-xi)'*p_t*(x-xi) -  u'*q_t*u;
% v(x,t)  = max_pi {  E[ int_t^T e^{-(s-t)/tau} r(X_s, U_s,t)ds  | X_t= x, pi) 
%              
% where U_s = pi(X_s)
% 
%  tau =inf  is accepted. 
%
%  The optimal value takes the following form
%  v(x,t) = - x' alpha_t x + 2 beta_t' -  gamma_t
%  and the optimal policy takes the following form
%  u_t = w1_t + w2_t x_t



m.h{1} =[ 0 0; 0 0.5];
%m.g{1} =[ 0 0; 0 0.5];
%m.g{2} = [ 0 0; 0 2];
s= ceil(m.T/m.dt);

for t=1:s
  m.xi{t}  =[0;0]; % state cost is (x_t - xi_t)' p_t (xi_t - x_t)
  if(t>s/2) m.xi{t}= [1;0];end

  m.omega{t} =0; % action cost is (u_t - omega_t)' q_t (u_t - omega_t)
  
  m.k{t} = [0;0]; %  a constant external force applied to the system
  m.a{t} = [ 0 1; 0 0];
  
  m.b{t} = [0; 1]; % the action is a force. i.e, it has an effect on the
  
  
  m.c{t} = 0*eye(nx);
  m.q{t} = 0.01;
  
  
  m.p1{t} = [ 1 0 ; 0 0 ]; % quadratic state cost 
  m.p2{t} = [0 ;0]; % linear state cost
  x{t} = zeros(nx,1);
  m.w1{t} = zeros(nu,1);
  m.w2{t}= zeros(nu,nx);
  
end


m = lqt3(m);






% % Here we run the system with the optimal controller

u = zeros(nu,s);
sqdt = sqrt(m.dt);
v=0;
for t=1:s-1
  u(:,t)= m.w1{t}+ m.w2{t} * x{t};
  dv= - (x{t}-m.xi{t})'*m.p1{t}*(x{t}-m.xi{t}) + 2*m.p2{t}'*x{t};
  dv= dv - (u(:,t)-m.omega{t})'*m.q{t}*(u(:,t)-m.omega{t});
  dv = dv*exp(-(t-1)*m.dt/m.tau)*m.dt;
  v = v+dv;
  dx = (m.k{t}+m.a{t}*x{t}  +m.b{t}*u(:,t))*m.dt ;
  x{t+1} = x{t}+ dx;
 end
v 


 subplot(211)
 mxi = cell2mat(m.xi);
 
 mx = cell2mat(x);
 
 plot([1:s]*m.dt,mx(1,:),[1:s]*m.dt,mxi(1,:))
 
 legend('X_1', '\xi','Location','Best');
 title('dx = a x dt + b u dt + c dB; a=[0 1; 0 0]; b =[0 1]')
 
 


 subplot(212)



 plot([1:s]*m.dt,u);
 hold on
 plot([0 s*m.dt], [0 0], 'r--');
 xlabel('Time (seconds)');
 ylabel('Action')
 title('dx = a x dt + b u dt + c dB; a=[0 1; 0 0]; b =[0 1]')



