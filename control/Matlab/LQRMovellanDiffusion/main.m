% Try Movellan diffusion network approach to policy learning
% Here we use a very simple LQR problem

% dynamical sysem is dXt = (Xt + lambda Ut ) dt + sigma dBt 

% Instantaneuous Reward is  R(X_t, U_t) = X_t^2 + \alpha U_t^2

% Reward = (\int_0^T R(X_t, U_t) dt) /T



clear

lambda = 0.01; % Initial guess for control law ( U_t = lambda X_t)
sigma=0.1; % diffusion 
T= 2 ; % Horizon in secs 
epsilon = 1; % step size for learning
dt = 1/100; % time step for discretization in secs

alpha = 10*dt; % Cost of actions



sqdt = sqrt(dt);
sigma2 = sigma^2;

n = 10; % number of sample trajectories

s = ceil(T/dt); % time steps


X = ones(n,s) ;


l(1) = lambda;
for trial =2: 10
  randn('seed',1);
  X(:,1) = ones(n,1);
  Psi = zeros(n,1); 
  dR = zeros(n,1);
  rho = zeros(n,1);
  for t=2:s

    U = lambda* X(:,t-1);
    dI = sigma*sqdt*randn(n,1); % the innovation
    X(:,t) = X(:,t-1)+ (X(:,t-1) + U)* dt + dI;
    gradDrift = X(:,t-1);
    Psi  = Psi+ gradDrift.*dI/sigma2;
    rho = rho -(X(:,t).^2+ alpha*U.^2)*dt/(s*dt);

    dR = dR - 2*alpha*X(:,1).*X(:,1)*lambda*dt/(s*dt);  
  end

  
  J(trial-1)  = mean(rho);


  
  nabla = mean(Psi.*rho) +mean(dR);
  lNabla = 1/(1+exp(-nabla));

  lambda = lambda + epsilon*tanh(nabla);
  l(trial) = lambda;
if(trial <3)
  subplot(3,1,3)
  plot([1:s]*dt,X(1,:),'b')
hold on
end

end
subplot(3,1,1)
plot(l(1:end))
xlabel('Learning Trial')
ylabel('Feedback Gain')
subplot(3,1,2)
plot(J(1:end))
xlabel('Learning Trial')
ylabel('Total Reward')
subplot(3,1,3)
hold on
plot([1:s]*dt,X(1,:),'r')
xlabel('Time in Seconds')
ylabel('State')
hold off

lambda