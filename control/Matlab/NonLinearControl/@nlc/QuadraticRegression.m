% QuadraticRegression(a,b,c,winit, p,tmax)
% e_i = a_i + b_i w + w' c_i w
% find w that mins \sum e_i^2
% p: precission (used to stop iterating)
% tmax; max number of iterations

function w  = QuadraticRegression(obj,a,b,c,winit,p,tmax)


n=max(size(a));
nw= length(b{1});

w= winit;
ow = w+10*p*ones(nw,1); % initialization of old weights. Just make
                        % them different enough from the initial weights.


y= zeros(n,1);
x = zeros(n,nw);

t=0;
d=1;
while( d>p && t<tmax) 
  t=t+1;
  d = max(abs(ow-w));
  for k=1:n
    y(k) = -a{k} +  w'*c{k}*w;
    x(k,:) = b{k}' + w'*(c{k}+c{k}');
  end
  ow =w;
  w=  pinv(x'*x)*x'*y;
   
end

 
    