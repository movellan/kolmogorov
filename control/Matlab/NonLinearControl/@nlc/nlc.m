% Non linear controller class
% dY = A(X) dt + B(X)U dt + c(X) dBt

% Structure: 
% Definitions of features and their corresponding jacobians and
% heesians are treated as cases that belong to the base class
% Definitions of dynamics (A, B,C) are overriden using methods for
% derivative class

classdef nlc
    properties  
        nx; % # state dimensions
        nphi; % dimensions in phi= Phi(x)
        nu; % # action dimensions
        samples; % # of sample trajectories
        xinit; % starting nx-dim state for ns trajectories
        xSamples; % states evaluated for approximating the value
                  % function
        adaptiveSamples; % use states from forward pass to get a
                         % sample of states for evaluating value funcction 
        x; % holds current state for ns trajectory
        phi;% holds feature vector for current state
        JPhi; % Jacobian of phi with respect to x:  dphi/dx'
        v; % value of current states for each sample trajectory
        gradV; % gradient of value of current states for each sample trajectory
        u; % holds current actions for ns trajectories
        dt; % time step in seconds
        approximator;
        T; % horizon in seconds
        xHist; % history of states 

        steps; % horizon in time steps
        t; % current time step;
        q; %  for quadratic cost for the action
        qinv; % q inverse
        k; % action is k *grad Value
        w; % the parameters for the value function
        sqdt;
        tau; % time constant for reward temporal decay
        gamma; %1/tau
        d2phi;% keeps Hessians of each of the state features 
        qh; % holds the H vector for quadratic features case

        vTerminal; % empirical terminal value 

        gMean; % means for Gaussian features
        gPrecission; % precission matrix for Gaussian features
    end % properties
    
    
    %%%%%%%%% specific to default case test. Overrride %%%%%%%%%%%%%%    
    properties
        a = [ 0 1; 0 0 ];
        b = [ 0 1]';

    end
    
    methods
        
        function y = A(o,x)
            y = o.a*x;
        end
        function y = B(o,x)
            y = o.b; 
        end
        function y = C(o,x)
            ns = size(x,2);
            nx = size(x,1);
            for k=1:ns
                y{k} = 0.1*eye(nx);
            end
        end
        function y= gT(o,x) %terminal state reward
            y = - 10*abs(x(1,:)-2).^2;
        end
        function y= g(o,x) %no terminal state reward rate
            y = - 10*abs(x(1,:)-2).^2;
            y=y- 1*abs(x(2,:)-0).^2; % Useful to avoid states that
                                     % are too large 
        end
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function o = nlc(nx, nu, a,xinit,u,b,c,dt,approximator,T)
    if nargin==0 % default system
        
        o.nx =2;
        o.nu=1;
        
        %o.samples=100; % # of sample trajectories
        x= [-4:2:4];
        o.xinit = [x ;zeros(size(x)) ]
        o.samples = size(x,2);
        s1=0;
        for k1 = -4:0.5: 4
            for k2 = -12:1: 12
             s1=s1+1;
             o.xSamples(:,s1) =[k1;k2];
            end
        end
       
        
        
        
        o.dt = 1/20; % time step in seconds
        o.T= o.dt*30; % horizon in seconds
        o.q = 0.1*eye(o.nu); % quadratic action cost
        o.nphi=16;
        o.approximator = 'gaussian';
        % o.approximator = 'quadratic';
        o.adaptiveSamples= false;
        
        
        % we make state reward a linear combination of the
        % features of that state
        

        
        o.tau = inf; % time constant for exponential reward decay
        

        
        
        
        
    end
    o = o.init;
end %constructor

function o = init(o)
    switch o.approximator
      case 'quadratic'
        o.nphi = o.nx*(o.nx+1)/2 +o.nx+1; %all quadratic
                                          %plus all linear
                                          %plus everthing else
                                          % Take care of the derivatives of linear features
                                          % they are a constant so we wont need to update
                                          % them
        
        o.JPhi= zeros(o.nphi,o.nx,o.samples);                   
        k1 = o.nx*(o.nx+1)/2;
        
        for s=1:o.samples
            for iphi=1:o.nx % the linear terms
                for ix=1:o.nx
                    if(ix== iphi)
                        o.JPhi(iphi+k1,ix,s) = 1;
                    end
                end
            end
        end
        
        

      case 'gaussian'
        xmin=[-2 -2 -2]';
        xmax =[2 2 2]';
        o.JPhi= zeros(o.nphi,o.nx,o.samples);                   
        o=o.kmeans(); % initializes means and covariances of
                      % gaussian kernels
        
      otherwise
        error('Approximator must be quadratic or gaussian');
        
        
    end %switch
    
    o.gamma = 1/o.tau; 
    o.sqdt = sqrt(o.dt);
    o.qinv = pinv(o.q);
    
    o.k = 0.5*o.qinv*o.B(o.xinit)';% in general this may be a
                                   % function of x
    o.steps = floor(o.T/o.dt); % horizon in time steps
    o.w =  -ones(o.nphi,o.steps); % initial parameters for estimated
                                  % value function
    o.xHist= zeros(o.nx,o.samples,o.steps);


    o.vTerminal =0;
    o.t= 1; % current time step;
    o.x = o.xinit;
    o.u = zeros(o.nu,o.samples); % actions for ns
                                 % trajectories
    
    %o.phi = zeros(o.nphi,o.samples);
    %o = o.Phi(o.x) ;% compute features of initial state
    [o.phi ] = o.Phi(o.x) ;% compute features of initial state
    o.xHist(:,:,1) = o.x;
    

    
end %init

function o= reinitX(o)
    o.t=1;
    o.x = o.xinit;
    o.xHist(:,:,1) = o.x;
    [o.phi ] = o.Phi(o.x) ;
end


function o= kmeans(o,timeSteps)
% sets the o.gMean and o.Precission parameters for use with
% Gaussian kernel approximators
%
% if o.adaptiveSamples =0 then we do kmeans on the xSamples (this
% is a predefined set of points at which the value function is to
% be evaluated
% if o.adaptiveSamples =1 then we use  a subset of the 
% nx dimensional states in xHist
% we restrict xHist to the time steps specified in the timeSteps vector

% Example: o.kmeans(1:n.steps)
% Example: o.kmeans(1:3)
    
    switch o.adaptiveSamples
      case true
        x=[];
        for k=1:o.samples
            x=[x o.xHist];
        end
        u =[];
        % do k means on each time step
        for k= timeSteps
            k
            [idx, c] =kmeans(x(:,:,k)',o.nphi,'EmptyAction', ...
                             'singleton','Display','off','Replicates',2);
            u = [u; c];    
            
            
        end
        
        clear x;
      otherwise
        u=o.xSamples';
        
    end
    
    % do k means on the combined kmeans obtained for each step;
    [idx, c, sumd] =kmeans(u,o.nphi,'EmptyAction', 'singleton','Display', ...
                     'off','Replicates',2);
       
        

    for k=1:o.nphi % update the gaussian means with the obtained
                   % centroid means 
        o.gMean{k} =  c(k,:)';


    end


    % update the precission matrices (ony works for 1D for now
    %if(o.nx==1)


        for k=1:o.nphi
            n = sum(idx==k);
            sumd(k) = sumd(k)/n;
        end
        
        s= max(sumd)*80;    

        for k=1:o.nphi
            o.gPrecission{k} = pinv(s);
            
        end
        %end

end



function o = backward(o) % entire backwards pass
    o.t = o.steps;
    for t=o.t:-1:1
        
        o =o.stepBackwards;
    end
    
end %backwards

function o = stepBackwards(o) % backwards step
    

    switch o.adaptiveSamples
      case true % requires doing a forward run first
        x= o.xHist(:,:,o.t); % nx x samples with state
      otherwise
        x=o.xSamples;
    end
    

    [phi JPhi d2phi] = o.Phi(x);
    

    
    u = o.updateU(x,JPhi);

    

    

    a = o.A(x); %nx x samples matrix with passive drift

    c= o.C(x);

    if(o.t == o.steps)
        o.vTerminal =0;

    end
    ns = size(x,2);
    for k=1:ns

        phik = phi(:,k);
        

        dphik = JPhi(:,:,k);

        ak= a(:,k);
        xk = x(:,k);
        
        
        
        
        if o.t == o.steps

            g = o.gT(x(:,k));
            a1{k} = g;
            b1{k} = -phik;
            c1{k} = zeros(o.nphi,o.nphi);
            
            o.vTerminal = o.vTerminal + g/o.samples; 
            
            
        else
            
            g= o.g(x(:,k));
            vtp1= o.w(:,o.t+1)'*phi(:,k); % value
                                          %vtp1= o.w(:,o.t+1)'*o.phi(:,k); % value
                                          % at next
                                          % time step
            a1{k} = g + vtp1/o.dt; % Here is where we need the value
                                   % for the next time step and the
                                   % reason why we have to move 
                                   % backwards in time.


            for i=1:o.nphi
                hk(i,1) =0.5*trace(c{k}'*c{k} *d2phi{i,k});
            end
            
            
            b1{k} = dphik*ak+ hk -(o.gamma+1/o.dt)*phik;
            
            
            b = o.B(x);
            c1{k} = 0.25*dphik*b*o.qinv*b'*dphik';

        end
        
        
    end % k
    
    precission=1/10000;
    maxIter=100;
    if o.t == o.steps
        winit = zeros(size(o.w(:,o.t)));
    else
        winit= o.w(:,o.t+1);
    end


    o.w(:,o.t)= o.QuadraticRegression(a1,b1,c1,winit, ...
                                      precission,maxIter);
    

    o.t = o.t-1;
    disp(sprintf('Backwards Pass: t = %d of %d', o.t, o.steps)) 
    
end %stepBackards

function o= forward(o)
    for t=o.t+1:o.steps
        o= o.stepForward;
        
    end
end %forward

function o = stepForward(o)

    [o.phi o.JPhi ]= o.Phi(o.x);
    
    o.u = o.updateU(o.x,o.JPhi);
    o.x
    o.u

    o.t = o.t+1;
    o = o.updateX;

    disp(sprintf('Forward Pass: t = %d of %d', o.t, o.steps)) 
end




function y =computeV(o,x,t)  % compute valueas at t for  states
% x. (size of x is nx by samples)
    

    phi  = o.Phi(x);
    
    y= o.w(:,t)'*phi;

    
end %computeV





function o = updateX(o)
    c = o.C(o.x);
    N=zeros(o.nx,o.samples);
    for k=1:o.samples
        N(:,k) = c{k}*randn(o.nx,1);
    end
    
    
    o.x = o.x+(o.A(o.x) + o.B(o.x)*o.u)*o.dt + N*o.sqdt;
    o.xHist(:,:,o.t) = o.x;
end



function u = updateU(o,x,JPhi)  % compute action for current state
    g= o.updateGradV(x,JPhi);

    u = o.k*g;  % in generl o.k may be a function
                % of x
                % 

end % update U





function y = updateGradV(o,x,JPhi) 
% rows of x are state dimensions cols of x are samples    
    ns = size(x,2);

    y = zeros(o.nx, ns);
    for k=1:ns
        y(:,k) = (o.w(:,o.t)'*JPhi(:,:,k))';
    end
    
end %updateGradV


function JPhi = quadraticJPhi(o,x)  
% compute Jacobian
    ns = size(x,2);
    switch o.approximator
      case 'quadratic'
        JPhi= zeros(o.nphi,o.nx,ns);                   
        f=0;
        for i=1:o.nx % quadratic feature derivatives 
                     % the derivatives of linear and
                     % quadratic features are constant
                     % and have already been precomputed 
            for j=i:o.nx
                f=f+1; % feature index (row)
                for k=1: o.nx % state index (col)
                    for s=1:ns
                        
                        if(i==k && j==k) % this index is the square of a state
                            JPhi(f,k,s) =2*x(j,s);
                        elseif(i==k && j ~= k) 
                            JPhi(f,k,s) = x(j,s);
                        elseif(j==k && i ~= k) 
                            JPhi(f,k,s) = x(i,s);
                        end
                    end %s
                end %k
            end % j
        end %i
        
        
        k1 = o.nx*(o.nx+1)/2;

        for s=1:ns
            for iphi=1:o.nx % the linear terms
                for ix=1:o.nx
                    if(ix== iphi)
                        JPhi(iphi+k1,ix,s) = 1;
                    end
                end
            end
        end
        
        
        
      otherwise
        error('Approximator must be quadratic');
        
    end %switch
end %function
    
    
    
    
% compute the Hessian of all the features for the case
% in which we are using quadratic features
function d2 = quadraticd2Phi(o)
% one hessian per feature 
    k=0; % the only features with non-zero second
         % derivative are the quadratic (productd of pairs
         % of states) features
    for k=1:o.nphi
        d2{k} =zeros(o.nx, o.nx);
    end
    k=0;
    for i=1:o.nx
        for j=i:o.nx
            k=k+1;
            
            
            if(i ==j)
                d2{k}(i,i) = 2;
            else
                d2{k}(i,j) = 1;
                d2{k}(j,i) = 1;
            end
        end
    end
   
end %d2Phi


%xxx
function varargout = Phi(o,x)   % compute features, and jacobian of x
% x has to be o.nx rows by  o.samples cols

    switch o.approximator
      case 'quadratic'
        % For quadratic features first we get all the coproducts,
        % then all the linear and finally the constant
        % e.g., x1*x1, x1*x2, x2*x2, x1, x2, 1
        ns = size(x,2);
        k=0; % first we get all the quadratic terms
        for i=1:o.nx
            for j=i:o.nx
                k=k+1;
                
                phi(k,:) = x(i,:).*x(j,:);
            end
        end
        for i=1:o.nx % second  we get the linear terms
            k=k+1;
            phi(k,:) = x(i,:);
        end
        k=k+1;
        phi(k,:) = ones(1,ns); %the  constant
        
        varargout{1} = phi;
        if nargout > 1
            JPhi=o.quadraticJPhi(x); % Here we compute the
                                     % Jacobian of the features
                                     % with respect to the states
            varargout{2} = JPhi;
            
        end
        if nargout > 2
 
            d2= o.quadraticd2Phi();
            for kp=1:o.nphi
                for ks=1:ns
                    %xxx
                   d2phi{kp,ks} = d2{kp};  
                end
            end
            varargout{3} = d2phi;                    
          
        end
        
      
        
      case 'gaussian'
        
        
        
        % returns value of gaussian rbf, gradient and hessian
        % x = n *1 state
        % mu = n * k mean (one mean per Gaussian}
        % nu = n * n precission matrix
        
        n = o.nphi; 
        m=o.nx;
        


        
        for kp=1:o.nphi
            mu = o.gMean{kp};
            nu = o.gPrecission{kp};
            ns = size(x,2);
            for ks=1:ns
                d = x(:,ks)-mu;
                g= exp( -0.5*d'*nu*d);% feature
                phi(kp,ks) =g;
                if nargout > 1
                    JPhi(kp,:,ks) = g*nu*(-d); %Jacobian
                end
                if nargout >2
                    t= nu*d;
                    d2phi{kp,ks} =  g*(t*t'  - nu);
                    % feature hessian
                    % note we have one Hessian for each combination of
                    % sample and feature within that sample. 
                end
                
            end %ks
        end %kp
        
        varargout{1} = phi;
        if nargout >1
            varargout{2} = JPhi;
        end
        if nargout >2
            varargout{3} = d2phi;
          
        end
        
      otherwise
        error('Approximator must be quadratic or Gaussian');
        
    end %switch
end




    end % methods
end %class