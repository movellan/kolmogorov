% returns value of gaussian rbf, gradient and hessian
% x = n *1 state
% mu = n * k mean (one mean per Gaussian}
% nu = n * n precission matrix

function f =  Phi(x,g)
  n = size(g,1);
  m=length(x);
  
  
  for k=1:n
    mu = g{k,1};
    nu = g{k,2};
    d = x-mu;
    f{k,1} = exp( -0.5*d'*nu*d);
    f{k,2} = f{k,1}*nu*(-d);
    t= nu*d;
    f{k,3} =  (t*t'  - nu)*f{k,1};
   
  end
  