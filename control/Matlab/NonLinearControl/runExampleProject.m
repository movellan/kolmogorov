% Unittest/ example of how to use the nlc class

clear
clf

% the class exampleProjectModel  determines the project dynamics
% dY = A(X) dt + B(X)U dt + c(X) dBt
%the terminal state reward function gT and the instantaneous state reward function g

model = exampleProjectModel; 


model.q = 0.1*eye(model.nu); % quadratic action cost
model.tau = inf; % time constant for exponential reward decay




model.nx= 1; % # state dimensions
model.nu =1; % # action dimensions


x= [-4:2:4];
model.samples =length(x);

model.xinit = x;

model.xSamples= [-4:0.5:4]; % this is the set of states over which
                            % we will approximate the value
                            % function when using the quadratic or
                            % the gaussian approximators. 


model.dt = 1/100; % time step in seconds
model.T= model.dt*25; % horizon in seconds



% choose quadratic or gaussian approximator 
%model.approximator = 'quadratic'; 
model.approximator = 'gaussian'; 

% for the gaussian case the rbf's means and covariance matrices
% are initialized using kmeans
% nphi determines the number of rbfs 
if model.approximator == 'gaussian'
  model.nphi=5;
end








model= model.init;



% here we compute the optimal value function and optimal policy
model=model.backward;
% rerun the model with the optimal policy
model=model.reinitX;
model=model.forward



subplot(2,1,1)
x=model.xSamples;
y = model.gT(x);
plot(x,y);
hold on
plot([-4 4], [0 0])

for t=model.steps:-1:1
    
    t
    y= model.computeV(x,t);
   
    plot(x,y,'r--')


end
subplot(2,1,2)
hold on
for k=1:model.samples
    plot(squeeze(model.xHist(:,k,:)))
end



