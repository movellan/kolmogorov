% Non linear controller class
% dY = A(X) dt + B(X)U dt + c(X) dBt

% Structure: 
% Definitions of features and their corresponding jacobians and
% heesians are treated as cases that belong to the base class
% Definitions of dynamics (A, B,C) are overriden using methods for
% derivative class

classdef mynlc < nlc

    
    methods        
        function y = A(o,x)
            y = x;
        end
        function y = B(o,x)
            y = 1; 
        end
        function y = C(o,x)
            ns = size(x,2);
            nx = size(x,1);
            for k=1:ns
                y{k} = 0*eye(nx);
            end
        end
        
        function y= gT(o,x) %terminal state reward
            y = - 10*abs(x(1,:)-0).^2 ;
        end
        function y= g(o,x) %no terminal state reward rate
            y = - 10*abs(x(1,:)-0).^2 ;
        end

        
  
    
    end %methods

        
end %classdef
