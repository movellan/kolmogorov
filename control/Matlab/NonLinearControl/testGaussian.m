% Unittest/ example of how to use the nlc class

clear
clf
n = mynlc;

n.nx= 1; % # state dimensions
n.nu =1; % # action dimensions


x= [-4:2:4];
n.samples =length(x);
n.xinit = x;

n.xSamples= [-4:0.5:4]; 

n.adaptiveSamples=false
%n.xinit = [1:n.samples;1:n.samples]; 



n.dt = 1/100; % time step in seconds
n.T= n.dt*25; % horizon in seconds
n.q = 0.1*eye(n.nu); % quadratic action cost

n.nphi=5;

n.approximator = 'gaussian';
%n.approximator = 'quadratic'


n.tau = inf; % time constant for exponential reward decay



n= n.init;




%n.w =  zeros(n.nphi,n.steps); % initial parameters for estimated



%n=n.forward;




n=n.backward;
n=n.reinitX;
n=n.forward









subplot(2,1,1)
x=n.xSamples;
y = n.gT(x);
plot(x,y);
hold on
plot([-4 4], [0 0])

for t=n.steps:-1:1
    
    t
    y= n.computeV(x,t);
   
    plot(x,y,'r--')


end
subplot(2,1,2)
hold on
for k=1:n.samples
    plot(squeeze(n.xHist(:,k,:)))
end



