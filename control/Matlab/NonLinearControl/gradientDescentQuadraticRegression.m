% QuadraticRegression(a,b,c,p,tmax)
% e_i = a_i + b_i w + w' c_i w
% find w that mins \sum e_i^2
% p: precission
% tmax; max number of iterations
function [w, ss] = gradientDescentQuadraticRegression(a,b,c,winit,p,tmax,eps)



n=max(size(a));
nw= length(b{1});


w= winit;
ow = w+ones(nw,1);

y= zeros(n,1);
x = zeros(n,nw);

t=0;
d=1;

while( d>p && t<tmax) 
  t=t+1;
  d = max(abs(ow-w));
  grad =0;
  ss=0;
  for k=1:n
      e = a{k} + b{k}'*w + w'*c{k}*w;
      ss = ss+e^2;
      grad = grad+e *(b{k} + 2* c{k}*w);
  end
  ow =w;

  w=  w- eps*grad;

  SS(t) = ss;
 
  
end
plot(SS)
 
    