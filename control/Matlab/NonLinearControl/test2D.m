% Unittest/ example of how to use the nlc class

clear
clf
n = nlc







%n=n.kmeans;
%n.approximator = 'quadratic'
%n.approximator = 'gaussian'
n=n.init
n=n.backward;


n=n.reinitX;
n=n.forward









subplot(2,2,1)
x= [-4:0.05:4];
x= [x;zeros(size(x))];
        



y = n.gT(x);
plot(x(1,:),y);




hold on
for t=n.steps:-1:1
    
    t
    y= n.computeV(x,t);
   
    plot(x(1,:),y,'r--')


end

subplot(2,2,2)
hold on
for k=1:n.samples
    plot(squeeze(n.xHist(1,k,:)))
end


subplot(2,2,3)
hold on
for k=1:n.samples
    plot(squeeze(n.xHist(1,k,1)),squeeze(n.xHist(2,k,1)),'ro')
end

for k=1:n.samples
    plot(squeeze(n.xHist(1,k,:)),squeeze(n.xHist(2,k,:)))
end

if(n.approximator == 'gaussian')
    subplot(2,2,4)
    hold on
    for k=1:n.nphi
        circle([n.gMean{k}(1),n.gMean{k}(2)],(1/n.gPrecission{k})/100,100)
        scatter(n.gMean{k}(1),n.gMean{k}(2),'r')
    end
end




