% Shows a very simple example in which Emo and Alex method fails
% miserably wiiith high probability

% Control dX = X + U + dB
% with r(x,u) = -lambda * x^2 -  0.5*u^2
% v(x)  = E[ int_t^\infty e^{-alpha(s-t)} r(X_s, U_s)ds  | X_t= x, pi) ]
% where U_s = pi(X_s)
%
%
% value function is modeled as v(x) = w_1  + w_2 * x^2
% Javier R. Movellan
% UCSD, December 2009
%
clear

lambda =4; 
alpha = 0.1; % discount rate. 


sample = [-2:0.01:2]'; % number of states in the sample
ns = length(sample);

nf = 2; % dimensionality of the feature function phi(x)

randn('seed',2)
w=randn(nf,1); % Initial weight vector for value function
eps = 0.001; % step size for gradient descent
nIter = 100; % gradient descent iterations
mse = zeros(nIter,1); % mean squared error
for k=1:nIter % Here we start the gradient descent iterations
k
  rho =0;
rhoGrad = zeros(nf,1);
for i=1:ns % iterate over samples of states  
  x = sample(i);
  phi = [ 1;  x^2];
  dotPhi = [ 0; 2*x];
  ddotPhi= [ 0 ;2];
  q = dotPhi*dotPhi';
  e= - lambda*x^2+(0.5*ddotPhi+ dotPhi - alpha*phi)'*w + 0.5*w'*q*w;
  eGrad =  0.5*ddotPhi + dotPhi - alpha*phi+ q*w;
  rhoGrad = rhoGrad +e*eGrad;
  rho = rho+ 0.5*e'*e;
 end
 w = w - eps* rhoGrad/ns; % update the weight parameter
 mse(k) = rho/ns;
end
plot(mse) 

display(sprintf('v(x) = %6.3f + %6.3f * x^2', w(1), w(2)))
display(sprintf('pi(x) =  %6.3f * x', 2*w(2)))
display(sprintf('Bellman  Error at Solution %6.3f',mse(end)));

