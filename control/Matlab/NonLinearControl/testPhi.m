clear



x= [-4:0.01:4];
nx=length(x);
nphi=11;
%x=[x;x;x];

a= -10;
for k=1:nx
    y(k,1)= x(:,k)'*a*x(:,k);
end




%   [idx, c] =kmeans(x',nf,'Replicates',10);
%   c=c';
%   for k=1:nf
%       f = find(idx==k);
%       xc = x(f)- c(k); 
%       nc(k) =length(f);
%       s= xc'*xc/nc(k);
%       phiPar{k,1} = c(:,k);
%       phiPar{k,2} = pinv(s+0.1*eye(3)); %precission matrix
%       phiPar{k,2} =0.001*eye(3); %precission matrix

%   end

for k=1:nphi % create the phi parameters
    mu(k)= -2+4*(k-1)/(nphi-1); % means
    phiPar{k,1} =  mu(k); % means
    phiPar{k,2} =  0.2;%precission matrix
end





for k=1:nx
    
    
    p = Phi(x(:,k),phiPar);
    
    x2(k,:)  = (cell2mat(p(:,1)))';
    grad(k,:)  = (cell2mat(p(:,2)))';
end



w = pinv(x2'*x2)*x2'*y;
vhat= x2*w;
vgradHat= grad*w;
subplot(2,2,1)
for k=1:nphi
    plot(x(1,:),x2(:,k))
    hold on
end
hold off
subplot(2,2,2)
plot(x,y, x,vhat,'r--')

subplot(2,2,3)
plot(x, -20*x,x,vgradHat,'r--')

subplot(2,2,4)
plot(mu,w);


corr(y,vhat)