\documentclass{article} % For LaTeX2e
%\usepackage{nips10submit_e,times}
\usepackage{nips06,times}

\usepackage{amsthm}
\newtheorem{thm}{Theorem}
%\newtheorem{lem}[thm]{Lemma}
%\newtheorem{prop}[thm]{Proposition}
\newtheorem{prop}{Proposition}
\newtheorem{pf}{Proof} 





 \usepackage{array,amsgen,amssymb,amsopn,amsmath}
%\usepackage[round]{natbib}

\DeclareMathOperator*{\plim}{plim}                                               % Probability limit
\title{ Master Equations For Policy Gradient Methods}


\author{
Javier R. Movellan \\
UCSD\\
}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

\newcommand\given{\medspace|\medspace}


%\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}


\maketitle



\begin{abstract}
We present a convergence proof for policy gradient with function
approximation that significantly expands previous proofs. In particular it
allows the use of arbitrary, non-compatible features, as well as
non-markovian processes. To this end we examine the policy gradient
problem at its most basic level, without assuming Markovian dynamics.
We show that by working at this basic level one can gain a new
understanding and generalize key results in the literature .
\end{abstract}






Reinforcement Learning (RL) focuses on finding approximate solutions
to control problems using a combination of Monte-Carlo methods and
function approximation methods. In recent years policy gradient has
become one of the most promising approaches to RL both from a
theoretical and practical point of view
\cite{sutton2000policy,schaalNAC}. Such approaches parameterize a
family of policies and perform gradient descent to search for policy
parameters that are local maximum on a performance measure.  Most work
on these methods assumes Markovian dynamics and reward signals
additive in time. These assumptions, provide useful computational
tricks but these tricks can sometimes obfuscate theoretical progress.
Here we examine policy gradient at its most basic level dispensing of
the standard Markovian assumptions when they are not needed.  Working
at this level one can gain an intuitive sense for some recent
breakthroughs in the literature.  We use this approach to present a
new convergence proof for a very general class of policy gradient
methods that allow the use of non-compatible features for function
approximation.




Throughout the paper we often make use of the mathematical machinery
available for the Hilbert space of random variables with finite
variance \cite{shiryaev1996probability}. In this space scalar random
variables behave in the same manner as standard vectors behave in
Euclidean space only with a different definition of inner product,
norm and distance. In particular the inner product between two random
variables $X,Y$ is the expected value of their product, i.e.., $<X,Y>=
E[XY]$. The norm of a random variable $X$ is its standard deviation
$\|X\| = \sqrt{E[( X- E[X])^2]}$, and the distance between two random
variables is the norm of their difference $d(X,Y) = \| X-Y\|$. This
allows the application of useful geometric concepts such as
orthogonality (zero inner product) and projection. In particularly in
this paper we will make great use of the fact that the projection
$\hat Y$ of a random variable $Y$ onto the linear space induced by a
random vector $Z= [ Z_1, \cdots, Z_n]'$ takes the following form (see
Appendix 2, proof 4)
\begin{align}
\hat Y = Z' E[Z Z']^{-1} E[ Z Y] 
\end{align}
This well known fact will prove helpful for understanding and generalizing recent theoretical breakthroughs in the RL literature \cite{kakade2002natural,sutton2000policy,schaalNAC}


Throughout the rest of the paper we let $X=\{X_t: t\in\mathcal{T}\}$
be a random processes representing state sequences, $U=\{U_t:
t\in\mathcal{T}\}$ a random process representing action sequences, and
$R= f(X,U)$ a scalar random variable representing the reward
associated with state-action sequences. We let $\Theta$ be a random
vector that parameterizes a family of control policies that map states
into actions. All the aforementioned random variables are assumed to
live in a common probability space $(\Omega, \mathcal{F},P)$ and have
finite variance.  The indexing set $\mathcal{T}$ may be finite,
countable (discrete time) or uncountable (continuous time).  The
proofs for the countable and uncountable time are identical but
require using probability densities with respect to Wiener measures
rather than standard Lebesgue measures.  Unless stated otherwise we
make no assumptions about the Markovian structure of $X$ and $U$. We
follow the convention of representing random variables with capital
letters and specific values taken by those variables with small
letters.  We identify probability density function by their
arguments. We name policies by the specific parameter vector $\theta$
that represents them.




Our goal is to find policies, i.e. values of $\theta$,  that locally
maximize the  expected reward
\begin{align}
\rho(\theta) = E[R \given \theta]
\end{align}
In policy gradient methods this is achieved by iteratively changing
$\theta$ in directions determined by the gradient of $\rho$ with
respect to $\theta$. Here we investigate the structure of this
gradient for two cases of interest: (1) probabilistic policies, and
(2) deterministic policies.

\section{Probabilistic Policies}
In this case there is a family of probability densities $p(u \given x, \theta)$
parameterized by $\theta$ . The gradient of $\rho$
with respect to $\theta$ takes the following form (see Appendix 2, proof 1)
\begin{align}\label{eqn:mep} 
\begin{array}{|c|}\hline\\
\nabla_\theta\; \rho(\theta)  = \text{E}[ \Psi  R\given \theta]
\\\\ \hline
\end{array}
\end{align}
where
\begin{align}
\Psi  = \nabla_\theta \;\log p(X,U \given \Theta)
\end{align}
is the Fisher score vector.  We call \eqref{eqn:mep} the {\em master
  equation} for probabilistic policy gradient methods. It tells us
that the gradient of the policy is the vector of projections of the
reward $R$ onto the components of the score $\Psi$.  The goal of
learning is to find a policy $\theta$ with zero gradient, i.e., a
policy with respect to which $R$ is orthogonal to all the elements of
$\Psi$.  Note $\Psi$ has zero mean (see Appendix 2, proof
3). Therefore in this case to be orthogonal to $\Psi$ means to be uncorrelated
(zero covariance) with $\Psi$
\begin{align}
Cov(\Psi, R\given \theta) = E[\Psi R\given \theta] - E[\Psi\given \theta] E[R\given \theta] = E[\Psi R\given \theta]
\end{align}


The most obvious Monte-Carlo algorithm for estimating the policy gradient takes  $n$ i.i.d sample pairs $\Big\{ (\Psi_1, R_1), \cdots
(\Psi_n, R_n)\Big\}$, and averages the products of these pairs
\begin{align}
S_n = \frac{1}{n} \sum_{i=1}^n \Psi_i R_i
\end{align}
It is easy to show that this results in an unbiased estimate of the gradient, i.e.
\begin{align}
E[S_n] = E[\Psi R \given \theta] = \nabla_\theta \; \rho (\theta)
\end{align}

This approach was independently proposed by Williams \cite{REINFORCE} for the
discrete time case and by Movellan \cite{Movellan94} for the continuous time
case. Unfortunately in even the simplest cases this estimate is very
inefficient, i.e., a surprisingly large number of samples is needed to
obtain usable estimates of the gradient. In particular as learning
progresses the score and the reward typically become highly correlated,
making  difficult to estimate the signs of $E[\Psi R\given \theta]$. A
significant improvement can be obtained by using the fact that the Fisher
score has zero mean and therefore expected values of products equal covariances
\begin{align}
\begin{array}{|c|}\hline\\
\nabla_\theta \;\rho(\theta)  = E[\Psi R\given \theta]= \text{Cov}[ \Psi,  R\given \theta]
\\\\\hline
\end{array}
\end{align}                          
This suggests a different Monte-Carlo estimate of the policy gradient 
\begin{align}
&S_n = \frac{1}{n} \sum_i ( \Psi_i -\bar \Psi) ( R_i - \bar R)\\
&\bar \Psi = \frac{1}{n} \sum_{i=1}^n \Psi_i \\
&\bar R = \frac{1}{n} \sum_{i=1}^n R_i 
\end{align}
Typically this estimate works better as it avoids the problems the
previous estimate has when $\Psi$ and $R$ are highly correlated. 

The key here was that we subtracted from $R$ a component $E[R\given
  \theta]$ that was orthogonal to $\Psi$.  We can take this a step
further by decomposing $R$ into a component orthogonal $\Psi$ and a
component in the space induced by $\Psi$, i.e., a linear conbination
of the elements of $\Psi$. We can then safely subtract from $R$ its
component orthogonal to $\Psi$, thus eliminate the source of
variability in $R$ that is irrelevant for the estimation of the
gradient. In other words, rather than working with $R$ we can operate
with $\hat R$, the projection of $R$ onto $\Psi$, i.e.,
\begin{align}
&\hat R = \Psi' w\\
&w=  [ \Psi \Psi' \given \theta]^{-1} E[\Psi  R \given \theta]
\end{align}
The proof that this strategy provides an unbiased estimate of the policy gradient was first
formulated in \cite{sutton2000policy} for the infinite horizon Markovian case. By working at the level of the master equations, the proof reduces to one line and generalizes beyond the Markovian infinite horizon case:
\begin{align}
E[\Psi \hat R \given \theta] = E[\Psi \Psi'\given \theta] E[ \Psi \Psi'\given \theta]^{-1} E[\Psi  R\given \theta] = E[ \Psi R \given \theta] =  \nabla_\theta\;\rho(\theta) 
\end{align}
 In many problems it has been found that pre-multiplying the gradient
 by the inverse of the Fisher Information matrix, results in faster
 convergence , this is known as the natural policy gradient \cite{kakade2002natural,schaalNAC}
\begin{align}
\nabla^N_\theta \;\rho(\theta) = E[\Psi \Psi' \given \theta]^{-1} \nabla_\theta \;\rho(\theta)
\end{align}
It is interesting to observe that
\begin{align}
\nabla^N_\theta \;\rho(\theta) = E[\Psi\Psi'\given \theta]^{-1} \nabla_\theta \;\rho(\theta)
=  E[\Psi\Psi'\given \theta]^{-1} E[ \Psi R \given \theta] = w
\end{align}
Thus the natural gradient of the policy $\theta$ is the vector of coordinates of
the projection of the long term reward $R$ on the space defined
by $\Psi$.


\subsection{Extending Previous Convergence Proofs}
In the RL literature the vector $\Psi$ has been known as the
compatible feature vector for the policy $\theta$.  The name
``compatible'' was used to express the fact that when these features
are used to approximate the average reward for a given policy,
convergence can be guaranteed \cite{sutton2000policy}. This recent
result was a breakthrough in the policy learning literature for it was
the first time that a policy gradient algorithm with function
approximation was shown to converge. However in many cases of interest
the compatible features can be too limiting. For example in some cases
it is useful to apply consistency constraints, like the Bellman
equation, so as to reduce the variance of the estimators. These
constraints may not be applicable to the projection of $R$ onto
$\Psi$, i.e., additional features may be needed that are not
compatible with the family of policies parameterized by $\theta$. Here
we show that in fact it is possible to extend the result in
\cite{sutton2000policy} so as to maintain convergence guarantees while
allowing the use of non-compatible features (see Appendix 1 for a
detailed proof).  In particular consider a feature vector $\Phi$ such
that
\begin{align}
\Phi = \left[ \begin{array}{c} \Lambda\\\Psi\end{array} \right]
\end{align}
where $\Psi$ is the Fisher score vector, i.e. the vector of compatible
features, and $\Lambda$ is an arbitrary random vector, not necessarily
a function of $X$, and such that $E[\Phi \Phi']$ is invertible. Let
$\hat R$ be the projection of $R$ onto $\Phi$, i.e.,
\begin{align}
&\hat R = \Phi' w\\
&w = E[ \Phi \Phi'\given \theta ]^{-1} E[\Phi  R \given \theta]
\end{align}
Then 
\begin{align}
E[\Phi \hat R \given \theta ] = E[\Phi \Phi'\given \theta] E[\Phi\Phi' \given \theta]^{-1} E[\Phi R \given \theta]
= E[\Phi R \given \theta]
\end{align}
Thus 
\begin{align}
E[\Psi \hat R \given \theta] = E[\Psi R \given \theta] = \nabla_\theta \;\rho(\theta)
\end{align}
This suggests a very general approximation algorithm: For a fixed
policy $\theta$ we gather samples $\Big\{ (\Phi_1, R_1),\cdots,
(\Phi_n,R_n)\Big\}$. We then approximate $R_1, \cdots, R_n$ using $\Phi_1, \cdots \Phi_n$
\begin{align}
&\hat R_i = \Phi_i w\\
&w= \Big( \sum_{i=1}^n \Phi_i \Phi_i'\Big)^{-1} \sum_{i=1}^n \Phi_i R_i
\end{align}
and then we estimate the gradient using $\hat R_i$ instead of $R_i$
\begin{align}
S_n = \frac{1}{n} \sum_{i=1}^n \Psi_i \hat R_i
\end{align}
in Appendix 1 we show that $S_n$ converges in probability to the policy gradient. 
\subsection{The Markovian Case}
In most of the work in RL, the states and the actions are Markovian, i.e.,  
\begin{align}
&p(x_{k+1}\given x_{1}\cdots x_k, u_1, \cdots u_k) = p(x_{k+1} \given x_k, u_k) \\
&p(u_k\given x_{1}, \cdots, x_k, u_1, \cdots, u_{k-1}  \theta)= p(u_k \given  x_k,  \theta)
\end{align}
and the total reward is typically the sum of instantaneous rewards, i.e., 
\begin{align}
R = \sum_{t\in \mathcal{T}}  R_t\\
\end{align}
Consider a problem in which $\theta$ controls only the policy at time $t$. To remind us of this fact we add the subindex $t$ in $\theta$ and  the  score function
\begin{align}
\nabla_{\theta_t} \rho(\theta_t) = E[ \Psi_t R\given \theta_t]
\end{align}
where
\begin{align}
\Psi_t = \nabla_{\theta_t} \;\log p(X,U\given \Theta_t) = \nabla_\theta \;\log p(U_t \given X_t, \Theta_t)
\end{align}
In this case we can decompose the long term reward into two
components, the past reward $P_t$ up to time $t-1$ and the future reward $Q_t$ 
\begin{align}
&Q_t  =  E[ \sum_{k=t}^T R_k \given X_t, U_t,\theta]\\
&R= P_t+ Q_t 
\end{align}
It is well known (see Appendix 2, proof 3) that the past reward $P_t$ is orthogonal to $\Psi$ and thus we can safely eliminate it
\begin{align}
\nabla_{\theta_t} \;\rho(\theta_t) = E[ \Psi_t R \given \theta_t] = E[ \Psi_t           Q_t \given \theta_t]
\end{align}

More generally, we can let 
$\theta= (\theta_1', \cdots, \theta_T')'$  where $\theta_t$ represents the policy at time $t$
\begin{align}
\begin{array}{|c|}\hline\\
\nabla_{\theta_t} \;\rho(\theta) =  E[ \Psi_t Q_t\given \theta ]
\\\\\hline
\end{array}
\end{align}
In this case $E[\Psi_t \given x_t] =0$ for all $x_t$. Thus we can safely subtract from $R$ any random variable $c(X_t)$ that is a function of $X_t$
\begin{align}
E[\Psi_t Q_t\given \theta] = E[\Psi_t (Q_t - c(X_t)) \given \theta]
\end{align}

In the Markovian case the Bellman equation provides a consistency condition between the different $Q$ functions. In particular
\begin{align}\label{eqn:qhatt}
&Q_t  =  E[R_t + Q_{t+1} \given X_t, U_t, \theta] 
\end{align}
This constraint can be used to link the different estimates of $Q_t$ through time, thus reducing their variance. In particular let
\begin{align}
&\hat Q_t =  \Phi'_t w_t\\
&\Phi_t =\left[ \begin{array}{c}\Lambda_t\\ \Psi_t\end{array}\right]
\end{align}
 where
\begin{align}
w_t = E[ \Phi_t \Phi_t' \given \theta]^{-1} E[\Phi_t (R_t + \hat Q_{t+1})\given \theta]
\end{align}
and
\begin{align}
E[\hat Q_{t+1} \given X_{t+1}, U_{t+1} ,\theta ] = E[ Q_{t+1} \given X_{t+1}, U_{t+1}, \theta]
\end{align}
Choose the feature set $\Lambda_t$ rich enough so that 
\begin{align}
E[\hat Q_{t} \given X_{t}, U_{t} ,\theta ] = E[ Q_{t} \given X_{t}, U_{t}, \theta]
\end{align}
Thus
\begin{align}
E[ \Psi_t \hat Q_t \given \theta] = E[\Psi_t Q_t \given \theta] = \nabla_{\theta_t} \rho(\theta) 
\end{align}
This suggest choosing a feature $\Lambda_T$ set rich enough to obtain an unbiased estimator of the 
terminal reward $R_T$ 
\begin{align}
&\hat Q_T = \Phi'_T w_T\\
&w_T = E[ \Phi_T \Phi_T' \given \theta]^{-1} E[\Phi_T R_T \given \theta]
\end{align}
Then apply \eqref{eqn:qhatt} recursively to obtain $\hat Q_{T-1},\cdots, \hat Q_1$. For infinite horizon problems we can choose  $T$  large enough then change $\theta_1, \cdots,\theta_T$ in the direction of the gradient until convergence. We can then  choose the resulting $\theta_1$ as the stationary policy for all time steps.  

\section{Deterministic Policies}
In the deterministic case  the action sequence $U$ is a parameterized function of the state
sequence $X$, i.e.,  $U = h(X, \theta)$. Thus  in this case there is no joint density $p(x,u\given \theta)$ and the  reward is a function $g$ of $X$ and $\theta$
\begin{align}
R= f(X,h(X,\Theta)) = g(X,\Theta)
\end{align}
It can be shown that in this case the master equation takes the following form (see Appendix 2, proof 1)
\begin{align}
\begin{array}{|c|}\hline \\
\nabla_\theta \;\rho(\theta) = E[ \dot R \given \theta] + 
 E[ \Psi R \given \theta]
\\\\\hline
\end{array}
\end{align}
where
\begin{align}
\dot R = \nabla_\theta \; g(X,\Theta)
\end{align}
The remarks made about the estimation of $E[\Psi R\given \theta]$  for the probabilistic policy case apply to the deterministic case. The only difference is that to estimate the gradient we need to add an estimate of $E[\dot R\given \theta]$.  For the Markovian case with additive rewards the results are also similar to the results when the policy is probabilistic but with some minor adjustments
\subsection{Markovian Case}
In this case 
\begin{align}
&p(x_{t+1} \given x_1,\cdots, x_t, \theta) = p(x_{t+1}\given x_t, \theta) \\
&\Psi_t  =  \nabla_{\theta_t} \log p(x_{t+1}\given x_t, \theta)\\ 
&\dot R = \nabla_{\theta_t} g(X_t, \Theta_t)
\end{align}
Similarly to the probabilistic case we decompose $R$ into a past reward and a future reward 
\begin{align}
&R= P_t +Q_t\\
&Q_t = E[ \sum_{k=t}^T R_k \given X_t, X_{t+1}]
\end{align}
As in the probabilistic case  he past reward $P_t$ is orthogonal to $\Psi_t$ (see Appendix 2, proof 3), thus
\begin{align}
E[ \Psi_t R \given \theta_t] = E[ \Psi_t Q \given \theta]
\end{align}
and
\begin{align}
\nabla_{\theta_t} \rho(\theta_t) = E[\dot R \given \theta] +  E[ \Psi_t Q_t  \given \theta]
\end{align}
The same scheme proposed for the probabilistic case can be used to obtain unbiased estimates of the gradient for multiple time steps. 


\section*{Appendix 1: New Convergence Proofs}
\begin{prop}{Convergence for Probabilistic Policies}\label{prop1}
{\em Let $\Theta$ be a random vector, let $\mathcal{T}$ an indexing
  set for the finite variance random processes $X=\{ X_t : t\in
  \mathcal{T}\}$ and $U= \{U_t: t\in \mathcal{T}\}$. Let the densities
  $p(u, x\given  \theta)$ exist for all $u,x, \theta$.  Let $R= f(X,U)$ be a
  finite variance scalar random variable. Let $\Psi, \Lambda$ be
  finite variance random vectors, where $\Psi= \nabla_\theta
  \log p(X, U\given \Theta)$ and for
\begin{align}
\Phi = \left[ \begin{array}{c} \Lambda\\\Psi\end{array} \right]
\end{align}
the matrix
\begin{align}
E[  \Phi \Phi'\given \theta] 
\end{align}
is invertible.  Let be $\theta \in \mathcal{R}^p$. Let $\rho(\theta) =
E[R\given \theta]$. Let $\Big\{(\Phi_1, \Psi_1, R_1)\cdots, (\Phi_n,
\Psi_n, R_n)\Big\}$ a set of i.i.d. samples collected from the joint
distribution of $\Phi, \Psi, R$ given $\theta$. Let
\begin{align}
&S_n  =  \Big(\frac{1}{n} \sum_{i=1}^n \Psi_i \Phi_i'\Big) \Big( \frac{1}{n} \sum_{i=1}^n\Phi_i \Phi_i'\Big)^{-1}\Big(  \frac{1}{n}\sum_{i=1}^n  \Phi_i R_i\Big)
\end{align}
For any given tolerance level $\epsilon >0$ and success rate $\delta
\in [0,1)$ there is a sample size $n$ such that $S_n$ is within
  tolerance of $\nabla_\theta \rho(\theta)$ with probability larger
  than $\lambda$, i.e.,
\begin{align}
p(| S_n - \nabla_\theta \rho(\theta)|>\epsilon  \given \theta) <\delta 
\end{align} 
}
\end{prop}
\begin{proof}
Note
\begin{align}
\plim_{n\to \infty} \frac{1}{n} \sum_{i=1}^n\Psi_i \Phi_i' = E[\Psi \Phi'\given \theta]\\
\plim_{n\to \infty} \frac{1}{n} \sum_{i=1}^n\Phi_i \Phi_i' = E[\Phi \Phi'\given \theta]\\
\plim_{n\to \infty} \frac{1}{n} \sum_{i=1}^n\Phi_i R_i = E[\Phi R\given \theta]\\
\end{align}
where $\plim$ stands for ``limit in probability''. Using Slutsky's theorem
\begin{align}
\plim_{n\to \infty} \Big(\frac{1}{n} \Phi_i \Phi_i'\Big)^{-1}  = E[\Phi \Phi'\given \theta]^{-1}
\end{align} 
and
\begin{align}
\plim_{n\to \infty} S_n &= 
\Big[\plim_{n\to\infty}\frac{1}{n} \sum_{i=1}^n \Psi_i  \Phi'_i \Big] \Big[ \plim_{n\to\infty}  \Big( \frac{1}{n} \sum_{i=1}^n\Phi_i \Phi_i'\Big)^{-1}\Big]\Big[ \plim_{n\to \infty}  \frac{1}{n}\sum_{i=1}^n  \Phi_i R_i\Big]\nonumber\\
&= E[\Psi \Phi'\given \theta]\; E[\Phi \Phi'\given \theta]^{-1} \;E[\Phi R\given \theta]  
\end{align}
Let $\hat R$ be the projection of $R$ onto $\Phi$, i.e., 
\begin{align}
\hat R = \Phi ' \;E[ \Phi \Phi']^{-1} E[ \Phi R]
\end{align}
Note
\begin{align}
E[\Phi \hat R] = E[ \Phi \Phi']  \;E[ \Phi \Phi']^{-1} E[ \Phi R] = E[ \Phi R]
\end{align}
Thus, using the master equation for probabilistic policies
\begin{align}
E[\Psi \hat R] = E[ \Psi R]= \nabla_\theta \;\rho(\theta)
\end{align}
i.e.,
\begin{align}
E[\Psi \Phi'\given \theta]\; E[\Phi\Phi' \given \theta]^{-1}\; E[\Phi R \given \theta]
= E[\Psi R \given \theta]= \nabla_\theta \;\rho(\theta)
\end{align}
Thus, 
\begin{align}
\plim_{n\to \infty} S_n =  \nabla_\theta \;\rho(\theta)
\end{align}
i..e, 
for any tolerance level $\epsilon >0$ and success rate $\delta \in [0,1]$ there is a sample size $n$ that guarantees
\begin{align}
 p( | S_n - \nabla_\theta \; \rho(\theta) | < \epsilon \given \theta )  > \lambda
\end{align}
Once convergence to the gradient is proved, standard methods for stochastic gradient descent can be used \cite{bertsekasndp96} to prove convergence to a local minimum of $\rho$
\end{proof}
\begin{prop}{Convergence for Deterministic Policies.}
{\em Let $\Theta$ be a random vector. Let $\mathcal{T}$ an indexing
  set for the finite variance random processes $X=\{ X_t : t\in
  \mathcal{T}\}$ let $h$ be a function such that $U= h(X, \Theta)$ is
  a finite variance process.  Let $R= f(X,U)$ be a finite variance
  scalar random variable. Let $\Psi, \Lambda$ be finite
  variance random vectors, where $\Psi= \nabla_\theta \log p(X\given
  U,\Theta)$ and
\begin{align}
\Phi = \left[ \begin{array}{c} \Lambda\\\Psi\end{array} \right]
\end{align}
is such that 
\begin{align}
E[  \Phi \Phi'\given \theta] 
\end{align}
is invertible.  For a fixed $\theta \in \mathcal{R}^p$ let
$\rho(\theta) = E[R\given \theta]$. Let $\Big\{(\Phi_1, \Psi_1,
R_1)\cdots, (\Phi_n, \Psi_n, R_n)\Big\}$ a set of i.i.d. samples
collected from the joint distribution of $\Phi, \Psi, R$ given
$\theta$. Let
\begin{align}
&S_n  = \Big(\frac{1}{n}\sum_{i=1}^n \dot R_i\Big) +   \Big(\frac{1}{n} \sum_{i=1}^n \Psi_i \Phi_i'\Big) \Big( \frac{1}{n} \sum_{i=1}^n\Phi_i \Phi_i'\Big)^{-1}\Big(  \frac{1}{n}\sum_{i=1}^n  \Phi_i R_i\Big)
\end{align} 
where
\begin{align}
\dot R_i = \nabla_\theta f(X, h(X,\Theta)) 
\end{align}
For any given tolerance level $\epsilon >0$ and success rate $\delta
\in [0,1)$ there is a sample size $n$ such that $S_n$ is within
  tolerance of $\nabla_\theta \rho(\theta)$ with probability larger
  than $\lambda$, i.e.,
\begin{align}
p(| S_n - \nabla_\theta \rho(\theta)|>\epsilon  \given \theta) <\delta 
\end{align} 
}
\end{prop}
\begin{proof}Apply the policy gradient master equation for deterministic policies and follow analogous steps to those used for proving Proposition \ref{prop1}. 
\end{proof}


\section*{Appendix 2: Sketch of proofs for auxiliary facts} 
The symbols used in these sketch proofs are defined in the main body
of the paper.
\begin{pf}{Master Equations.}\\
For probabilistic policies. Let $R= f(X,U)$. Thus 
\begin{align}
\nabla_\theta E[ R \given \theta ] &= \nabla_\theta \int p(x,u\given \theta) f(x,u  ) dx du  = 
 \int \Big(\nabla_\theta p(x,u\given \theta)\Big) f(x,u  ) dx du\nonumber \\&  = \int p(x,u\given \theta) \nabla_\theta \log p(x,u\given \theta) f(x,u) dx du 
= E[ \Psi R]
\end{align}
For the deterministic case there is a function $g$ such that
$U=g(X,\Theta)$. Thus there is no joint density of $X,U$. Moreover
\begin{align}
\nabla_\theta E[ R \given \theta ] &= \nabla_\theta \int p(x\given \theta) h(x, \theta ) dx du  = \int p(x \given \theta)  \nabla_\theta  g(x,\theta) dx du \nonumber\\
&+ \int \Big(\nabla_\theta p(x \given \theta)\Big)   g(x,\theta) dx du \nonumber\\
&= E[\dot R\given \theta] + E[\Psi R]
\end{align}
\end{pf} 
\begin{pf}{The Fisher Score has zero mean.}
\begin{align}
E[\Psi\given \theta] &= \int p(x,u\given \theta) \nabla\log
p(x,u\given \theta) dx du = \int \nabla p(x,u\given \theta) dxdu
\nonumber\\ &= \nabla_\theta \int p(x,u\given \theta)  dx du = 0
\end{align}
\end{pf}
\begin{pf}{Orthogonality of past rewards.}\\
Let $R_k = f(X_k, U_k)$ and let $t>k$.  For the probabilistic policy
case
\begin{align}
E[\Psi_t R_k \given \theta]& =  \int  p(x_k,u_k\given \theta) f(x_k,u_k) 
\int  p(x_t,\given x_k,u_k,\theta )\nonumber\\
&\Big( \int p(u_t \given x_t ,\theta) \nabla_\theta \log( p(u_t\given x_t,\theta)du_t \Big)dx_t du_k  dx_k =0
\end{align}
For the deterministic policy case
\begin{align}
E[\Psi_t R_k \given \theta] =&  \int p(x_k \given \theta) f(x_k,\theta) 
\int  p(x_t,\given x_k, \theta ) \nonumber\\
&\Big(\int p(x_{t+1}  \given x_t ,\theta) \nabla_\theta \log( p(x_{t+1} \given x_t,\theta)dx_{t+1} \Big) dx_t dx_k =0
\end{align}
\end{pf}
\begin{pf}{Projections.}\\
Let $Y$ a random variable and $Z$ an $n$-dimensional random vector such that $E[ZZ']$ is invertible.  For $\hat Y$ to be the projection of $Y$ onto $Z$ we need for the residual $Y-\hat Y$ to be orthogonal to all the elements of $Z$, i.e., 
\begin{align}
E[Z (Y- \hat Y) ] = 0\
\end{align} 
in addition $\hat Y$ lies in the space defined by the components of
$Z$, i.e., there is a $w\in \mathcal{R}^n$ such that 
\begin{align}
\hat Y = Z'w
\end{align}
Thus
\begin{align}
&E[Z(Y - Z'w)]=0 \\
&E[ZY] - E[ZZ'] w=0\\
&w= E[ZZ']^{-1} E[ZY]\\
&\hat Y = Z'E[ZZ']^{-1} E[ZY]
\end{align}
\end{pf}
\bibliographystyle{plain} % plainnat
\bibliography{Nips2010}


\end{document}
