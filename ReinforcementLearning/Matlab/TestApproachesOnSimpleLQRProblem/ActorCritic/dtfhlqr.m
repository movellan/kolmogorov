% X_{t+1} = a _t X_t + b u_t + c Z_t
% R_t = X_t' q_t X_t + U_t' g_t U_t
% Discrete Time Finite Horizon LQR
function [gain, alpha, beta]= dtfhlqr(a, b, c, q,g,T)


alpha{T} = q{T};
beta{T}=0;

for t = T-1:-1:1
  gain{t} = inv(b'*alpha{t+1}* b + g{t} )*b'*alpha{t+1}*a;
  alpha{t} = q{t}+ a'*alpha{t+1}*(a - b*gain{t});
  beta{t} = beta{t+1}+ trace(c'*alpha{t+1} *c);
end

