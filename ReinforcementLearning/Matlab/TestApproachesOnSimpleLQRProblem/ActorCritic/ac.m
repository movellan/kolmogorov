classdef nac 
    properties  
        dt=1 ;
        gamma = 0.99;
        sqdt;
        theta;
        lambda;
        nu;
        t;
        a=0;
        c=0.01;
        ns;
        xinit=[-4:2:4];
        T=1;
        nt;
        x;
        u;
        nx;
        xHist; % cols are time steps, rows are samples
        uHist;
        nphi;
        V;
        Q;
        epsilon =0.1;
        trial=0;
        rho;
        smooth=1;
        p;
        q;
        pT;
    end % properties
    
    methods
        
        function y=rT(o,x)
            y = -o.pT*(x.^2);
        end
        
        function y=r(o,x,u,t)
                y = -(o.p(t)*(x.^2) +o.q(t)*(u.^2)/o.dt/o.dt)*o.dt;
            
        end
        
        
        
        
        function o = nac()
            o.p = 1*ones(o.nt,1);
            o.q = 0.1*ones(o.nt,1);
            o.pT=1;
            o.theta(1,:)=0 *ones(1,o.nt-1);
            o.theta(2,:)= 10*o.dt*ones(1,o.nt-1);
            
            o=o.init;
        end
        
        
        function o = init(o)
            
            o.nt = floor(o.T/o.dt);


            o.sqdt = sqrt(o.dt);
            

            o.x = o.xinit;
            o.nx=1;
            o.ns = length(o.xinit);
            o.nphi=2;
            o.rho=0;
              
        end %init
        

                
                
        function o= initX(o,x)
        % randn('seed',1);
            o.t =1;
            o.x = x;
            o.ns = length(x);
            o.xHist = zeros(o.nt,o.ns);
            o.uHist = zeros(o.nt-1,o.ns);
            o.xHist(1,:)=x;
            o.rho=0; 
        end
        
        function o = forward(o,x)
            o=initX(o,x);
            for  t =1:o.nt-1
                o.t=t;
                o.rho = o.rho + mean(o.r(o.xHist(t,:),o.uHist(t,:),t));
                [o.x,o.u] = o.stepForward(o.x,t);
                o.uHist(t,:) = o.u;
                o.xHist(t+1,:) = o.x;
                if t==o.nt-1
                    o.rho = o.rho + mean(o.rT(o.xHist(t+1,:)));
                end
                
            end
            
        end
        
        function [y, u]  = stepForward(o,x,t)
            u= o.U(x,t); 
            y  = x+o.a*x*o.dt + u + o.c*o.sqdt*randn(size(x));
        end %stepForward
        
        
        function u=  U(o,x,t)
           
            mu = o.theta(1,t)*x*o.dt; % note the what we penalize is
                                      % really mu/dt
            
            sigma = sqrt(o.theta(2,t));
            u = mu+ sigma*randn(size(x));
        end %U
        
        
        
        
        function o=learnVandQ(o);
            
            for t=o.nt:-1:1 % Estimate the value and Q function
                if (t==o.nt)
                    x =  o.xHist(t,:)';
                    v = o.rT(x);

                else
                    vtp1= p*o.nu(:,t+1); % note p is from t+1
                    x =  o.xHist(t,:)';
                    u= o.uHist(t,:)';
                    v = o.r(x,u,t) + o.gamma* vtp1;
                end
                p = o.Phi(x);

                o.nu(:,t) = pinv(p'*p)*p'*v ;
                              
                o.V(:,t) = p*o.nu(:,t);
                o.Q(:,t) = v; % assumes a single action per state
            end %t
            if(o.smooth>1)
                o.nu(1,:) = smooth(o.nu(1,:),o.smooth);
                o.nu(2,:) = smooth(o.nu(2,:),o.smooth);

            

          
            for t=o.nt:-1:1
                if (t==o.nt)
                    x =  o.xHist(t,:)';
                    v = o.rT(x);
                    
                else
                    vtp1= p*o.nu(:,t+1);
                    x =  o.xHist(t,:)';
                    u= o.uHist(t,:)';
                    v = o.r(x,u,t) + o.gamma* vtp1;
                end
                p = o.Phi(x);

                o.V(:,t) = p*o.nu(:,t);
                o.Q(:,t) = v; % assumes a single action per state
            end %t
    
            
            
            end            
            
                
        end % learnVand Q
        
        
        function o=naturalGradient(o)
            a = o.Q - o.V; % the advantage 
            for t=o.nt-1:-1:1
                x = o.Psi(o.xHist(t,:), o.uHist(t,:),t)';
                y = a(:,t); 
                o.lambda(:,t) = pinv(x'*x)*x'*y;
            end
            
                
        end
        
        function o=updatePolicy(o)
            o.lambda(o.lambda>1)=1;
            o.lambda(o.lambda<-1)=-1;
            
            o.theta(1,:) = o.theta(1,:)+o.epsilon*o.lambda(1,:);
            o.theta(2,:) = o.theta(2,:)+o.epsilon*o.lambda(2,:);
            o.theta(2,(o.theta(2,:) <0.0 )) =0.0;
            if(o.smooth>1)
                o.theta(1,:) = smooth(o.theta(1,:),o.smooth);
                o.theta(2,:) = smooth(o.theta(2,:),o.smooth);
            end
            
        end
        
        function p = Psi(o,x,u,t) % the Fisher score function 
                k=o.theta(2,t) + 0;
                p(1,:) = (u - o.theta(1,t)*x*o.dt).*x*o.dt;
                p(2,:) = 0.5*((u-o.theta(1,t)*x*o.dt).^2)/k -ones(size(x));
                p = p./k;
        end
        
            

        function y = Phi(o,x)
            y(:,1) =x.^2;
            y(:,2)= 1;
        end
        
        function displayXHist(o)
            for k=1:o.ns
                plot([1:o.nt]*o.dt, o.xHist(:,k))
                hold on
            end
            hold off
        end %displayXHist
    end %methods
end %classdef
    
        
        