clear
clf
clearall;

n = ac;


n.xinit = [-4:2:4];

n.dt = 0.01;
n.T =100*n.dt;
n.gamma=1;
n.epsilon=0.5;
n.c=0.0;
n.smooth=1;
n=n.init;

n.p = 0.1*ones(n.nt,1); %state cost
n.q = 0.1*ones(n.nt,1); % action cost
n.q(end)=0;
n.pT=2;
for t=1:n.nt-1
    q{t} = n.p(t)*n.dt;
    g{t} = n.q(t)*n.dt;
end
g{n.nt}=0;
q{n.nt}=n.pT;
a = 1;
b = n.dt;
c = n.c;

[gain, alpha, beta] = dtfhlqr(a, b, c, q,g,n.nt);
target= -cell2mat(gain);
target = smooth(target,1);



clear n.theta;
n.theta = ones(2,n.nt-1);
n.theta(1,:)= 0*ones(1,n.nt-1);


n.theta(2,:)= 4*ones(1,n.nt-1);




%load n





for trial=1:100000
    %theta(:,trial) = n.theta;
    n=n.forward(n.xinit);
    rho(trial) = n.rho;
    subplot(3,2,1)
    if (mod(trial,10) ==0) n.smooth=1;
    else
        n.smooth=2;
    end
    
    if(trial>400)
        plot(rho(end-400:end))
    else
        plot(rho)
    end
        
    xlabel('trial')
    ylabel('rho')
    
    
   
    subplot(3,2,2)
     if(mod(trial,10) ==0) n.displayXHist; end
    drawnow
    n=n.learnVandQ;
    n=n.gradient;
    n=n.updatePolicy;
    subplot(3,2,3)
    plot([1:n.nt-1]*n.dt,-n.theta(1,:),'lineWidth',2)
    hold on
    plot([1:n.nt-1]*n.dt,-target,'--r','lineWidth',2)
    hold off
    %   plot(theta(1,:))

    ylabel('Gain')
    subplot(3,2,4)
    plot([1:n.nt-1]*n.dt,n.theta(2,:))
    %  plot(theta(2,:))
    ylabel('Noise')
    subplot(3,2,5)
    plot([1:n.nt]*n.dt,n.nu(1,:))
    ylabel('V')

end



            
