import numpy as np
from matplotlib import pyplot as plt


# linear model with 2 weight parameters

class model:
    def __init__(self, n,mu_0, kappa_0, alpha,x_r=np.zeros((2,1))):
      #n: number of parameters
      # mu_0, kappa_0: mean and covariance of prior
      # alpha =variance of output noise
      # x_r: each column is a reference input vector
      # we want to do well on these

        self.n = n
        self.mu_0 = mu_0
        self.mu = self.mu_0
        self.kappa_0 = kappa_0
        self.kappa =self.kappa_0
        self.alpha = alpha

        self.x_r = x_r

    def initialize(self):
        self.mu= self.mu_0
        self.kappa = self.kappa_0
    def pre_update_covariance(self,x):
        kappa = self.kappa
        u = kappa @ x
        kappa = self.kappa  - u @np.transpose(u)/(self.alpha + np.transpose(x)@u)
        return(kappa)
    def pre_update_mean(self,x,y):
        kappa = self.pre_update_covariance(x)
        mu = self.mu + kappa @ x @(y- np.transpose(x)@self.mu)/self.alpha
        return(mu)
    def update(self,x,y):
        self.mu= self.pre_update_mean(x,y)
        self.kappa= self.pre_update_covariance(x)
    def evaluate_candidate(self,x):
        n_r =  self.x_r.shape[1]
        mse=0
        kappa = self.pre_update_covariance(x)
        for i in range(n_r):
            r = self.x_r[:,i].reshape(2,1)
            mse += np.squeeze(np.transpose(r)@kappa@r)
        return(mse/n_r)
true_w = np.ones((2,1))
# prior for weight parameters
mu_0 = np.zeros((2,1))
beta = 25
kappa_0 = beta* np.eye(2)
mu = mu_0
kappa =kappa_0
# output noise variance
alpha = 10
# reference inputs
# we want the model to do well at these inputs
x_r= np.array([1,0]).reshape((2,1))

m = model(n=2,mu_0 = mu_0, kappa_0 = kappa_0, alpha=alpha,x_r=x_r)
# candidate point
mseList=[]


alphaList = np.arange(0,2*np.pi,0.01)
x_List = []
for alpha in alphaList:
    x = np.array([np.cos(alpha),np.sin(alpha)]).reshape((2,1))
    #x= np.array([1,1]).reshape((2,1))*(alpha+1)
    mse = m.evaluate_candidate(x)
    mseList.append(mse)
    x_List.append(np.squeeze(x[0]))
degreeList  = alphaList*360/(2*np.pi)
x_List=np.array(x_List)
p1=plt.plot(degreeList,mseList,'r')
mseList2=[]
mseTheoryList=[]
for alpha in alphaList:
    x = np.sqrt(2)*np.array([np.cos(alpha),np.sin(alpha)]).reshape((2,1))
    mse2 = m.evaluate_candidate(x)
    mseList2.append(mse2)
    norm_x= np.linalg.norm(x)
    norm_r= np.linalg.norm(m.x_r)
    cosTheta= np.transpose(x)@m.x_r/(norm_x* norm_r)
    mseTheory = beta * norm_r* (1 - cosTheta**2)
    mseTheoryList.append(np.squeeze(mseTheory))
p2=plt.plot(degreeList,mseList2,'g')
p3=plt.plot(degreeList,mseTheoryList,'b')
plt.xlabel(r'$\theta$')
plt.ylabel('MSE')
plt.legend((p1[0],p2[0],p3[0]) ,(r'$a=1$',r'$a=2$',r'$a\to \infty$'))
plt.show(   )
