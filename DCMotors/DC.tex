\documentclass[12pt]{article}
%\usepackage{Mynips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath,amsthm}
\usepackage[pdftex]{graphicx}
\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[square,numbers,sort]{natbib} 
%\usepackage[round,sort]{natbib} 
\usepackage[latin1]{inputenc}  
\usepackage{verbatim}
\usepackage[usenames]{color}


 \title{DC Motors}
\author{ Javier R. Movellan}

\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}
\newcommand\given{\medspace|\medspace}        
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\pfrac}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\ppfrac}[3]{\frac{\partial^2 #1}{\partial #2 \partial #3}}


\begin{document}
\maketitle

\begin{center} 
 Copyright \copyright{}  2010 Javier
R. Movellan. \end{center} 
 
\newpage
The equations of motion for DC motors are as follows
\begin{align}
&V= L \frac{dI}{dt} + R I  + k_b \dot \theta\\
&M \ddot \theta = k_T I - \nu \dot \theta - \tau
\end{align}
where $V$ is the voltage applied to the motor, $L$ is the motor
inductance, $I$ the current through the motor windings, $R$ the motor
winding resistance, $k_b$ the motor's back electro magnetic force
constant, $\dot \theta$ the rotor's angular velocity, $M$ the rotor's moment
of inertia, $k_T$ the motor's torque constant, $\nu$ the motor's
viscous friction constant, and $\tau$ the torque applied to the rotor by
an external load.
\section{Equilibrium Analysis}
 We apply a voltage source to the motor's terminal and a
 mechanical load (a torque ) $\tau$ to its rotor. We let time pass
 until the motor's rate of rotation equilibrates. At that point the
 temporal derivatives of the current and velocity are zero. Thus the
 equilibrium equations are as follows
\begin{align}
&V=  R I  + k_b \dot \theta \label{eqn:VRI}\\
&\tau = k_T I - \nu \dot \theta  \label{eqn:current}
\end{align}
It follows that
\begin{align}
V = \frac{R }{k_T} \tau+ \frac{R \nu}{k_T}\dot \theta + k_b \dot \theta
\end{align}
or equivalently
\begin{align}
\dot \theta =   \Big( \frac{R \nu}{k_T} + k_b\Big)^{-1} \Big( V - \frac{R }{k_T}\tau \Big)
\end{align}
After some algebra we get the equations for the equilibrium velocity
and torque 
\begin{align}
&\dot \theta =   \Big(  \nu  + \frac{k_bk_T}{R}\Big)^{-1} \Big( V \frac{k_T}{R}  - \tau  \Big)\label{eqn:tau2theta}  \\
&\tau = V \frac{K_t}{R} - \Big(  \nu  + \frac{k_bk_T}{R}\Big) \dot \theta \label{eqn:theta2tau} 
\end{align}
Figure~\ref{fig:torquevel} shows the torque/velocity equation for a Maxxon AMax 22 motor running at 6 Volts. The equations represent the load (applied torque) in the vertical axis and the resulting equilibrium velocity of the rotor in the horizontal axis.
\begin{figure}[h]
\begin{center}
\includegraphics[height=3in]{Media/torquepowerspeed.pdf} 
\caption{Torque vs speed (line) and power vs speed (parabola)  for the Maxon AMax 22 motor running at 6 Volts.  \label{fig:torquevel}} 
\end{center}
\end{figure}

 \subsection{Stall Torque and No Load Velocity}
Maximum torque  is achieved when the load is such that the
motor does not move at all. This is called the {\em stall
  torque} $\tau_s$
\begin{align}
&\tau_s = V \frac{k_T}{R}
\end{align}
 Since the current draw is proportional to the torque, then
the {\em stall current} is as follows
\begin{align}
&I_s = \frac{\tau_s}{k_T} = \frac{V}{R}
\end{align}
 Maximum velocity $\dot \theta_n$ is achieved when no load is
 applied. This is called the {\em no load velocity} $\dot \theta_n$
\begin{align}
&\dot \theta_n =\tau_s\; \Big( \nu +
  \frac{k_bk_T}{R}\Big)^{-1}\label{eqn:nlv} \end{align} Thus for a
fixed voltage $V$ the equilibrium torque, velocity, and current can be
expressed as follows
\begin{align}
&\dot \theta =  \dot \theta_n -   \Big(  \nu  + \frac{k_bk_T}{R}\Big)^{-1}\label{eqn:tau2theta} \tau \\
&\tau = \tau_s - \Big(  \nu  + \frac{k_bk_T}{R}\Big) \dot \theta \label{eqn:theta2tau} \\
&I =  I_s - \Big(  \frac{\nu}{k_T}   + \frac{k_b}{R}\Big) \dot \theta \label{eqn:theta2I}
\end{align}

\subsection{Power Curve}
The mechanical power $P$ delivered by the motor is the applied torque $\tau$ times its angular velocity $\dot \theta$. Thus
\begin{align}
P = \tau\;\dot \theta=\tau_s \dot \theta - \Big(  \nu  + \frac{k_bk_T}{R}\Big) \dot \theta ^2
\end{align}
To find the velocity that delivers maximum power we take the gradient with respect to $\dot \theta$
\begin{align}
\nabla_{\dot \theta} P_m = 
\tau_s  - 2 \Big(  \nu  + \frac{k_bk_T}{R}\Big) \dot \theta 
\end{align}
Setting the gradient to zero we obtain the {\em maximum power velocity} $\dot \theta_{mp}$
\begin{align}
\dot \theta_{mp} = \frac{1}{2} \tau_s \Big(  \nu  + \frac{k_bk_T}{R}\Big)^{-1} 
= \frac{1}{2} \dot \theta_n
\end{align}
Thus maximum mechanical power is achieved when the motor is running at half the no load velocity. At that point the torque is  as follows
 \begin{align}
&\tau_{mp} = \tau_s - \Big(  \nu  + \frac{k_bk_T}{R}\Big) \dot \theta_{mp} 
= \frac{1}{2} \tau_s
\end{align}
Thus maximum mechanical power is achieved when the load is one half of the stall torque. Since the current draw is proportional to the torque, then the current at the point of maximum power transfer shall also be one half of the stall current
\begin{align}
I_{mp} = \frac{1}{2} I_s = \frac{V}{2R}
\end{align}  
The maximum mechanical power produced by the motor is as follows
\begin{align}
P_{mp} = \tau_{mp} \; \dot\theta_{mp} = \frac{1}{4} \tau_s \theta_n
\end{align}

\subsection{Motor Efficiency}
The efficiency $\eta$ of a motor is defined as the ratio between the input electrical power, i.e., the product of voltage times current, and the output mechanical power $P$
\begin{align}
\eta = \frac{P}{VI}
\end{align} 
To get the efficiency as a function of the equilibrium velocity we express the current and the torque as a function of the velocity 
\begin{align}
&I = \frac{V - K_b \dot \theta}{R}\\
&\tau = \tau_s - \Big(  \nu  + \frac{k_bk_T}{R}\Big) \dot \theta
\end{align}
Thus
\begin{align}\label{eqn:me1}
\eta = \frac{\tau \dot \theta}{ V I} = \frac{\tau_s\dot \theta - \Big(  \nu  + \frac{k_bk_T}{R}\Big) \dot \theta^2}
{\frac{V^2}{R}  - \tau_s \dot \theta }
\end{align}
The velocity that  maximizes this function, i.e. the most
efficient angular velocity, can be obtained using the following formula
(see derivation in the Appendix).
\begin{align} \label{eqn:me2}
\dot \theta_{me} = \frac{bc - \sqrt{b^2 c^2 - a^2 b c}}{a b}
\end{align}
where
\begin{align}
&a= \tau_s\\
&b= \nu + \frac{k_b k_T}{R}\\
&c = \frac{V^2}{R}
\end{align}
The most efficient velocity occurs for values of $\theta$ larger than
the maximum power velocity, i.e., torques smaller than the maximum
power torque (see Figure~\ref{fig:efficiency}). The maximum motor
efficiency $\eta_{me}$ is found by using $\dot \theta_{me}$ on
equation \eqref{eqn:me1}.

\begin{figure}[h]
\begin{center}
\includegraphics[height=3.1in]{Media/Efficiency.pdf} 
\caption{\it  Efficiency as a function of Angular Velocity  for  a Maxxon Amax 22 running at 6 Volts. \label{fig:efficiency}} 
\end{center}
\end{figure}



\section{Transient Behavior}
When a motor is at rest and we apply a voltage, the current
increases according to the equation below 
\begin{align}
V = L \frac{dI}{dt} + R I + k_T \dot \theta
\end{align}
The changes in the current are much faster than the changes in rotor
velocity so we can treat the rotor velocity $\dot \theta$ as if it
were approximately constant. Thus the current will grow exponentially
with a time constant of $L/R$. This is known as the {\em electrical
  time constant}\footnote{Given an arbitrary initial condition $C>0$
  and a zero input to the system the time constant is the time to
  decay to $C/e = 0.3679 C$. It takes three time constants to get a 95\% decay, 4.6  time constants to get a 99 \% decay. Ten time constants result in  a 99.9955 \% decay.}. In general the electrical time constant
is much larger than the time constant for $\dot \theta$ and thus, when
analyzing the speed dynamics we can approximate $I$ as being at
equilibrium, i.e.,
\begin{align}
V \approx  RI + k_b \dot \theta \label{eqn:approxI}
\end{align}
This approximation is equivalent to assuming a zero electrical time
constant. Figure~\ref{fig:transient} show the current and velocity
dynamics using the differential equations for current and motion
(blue), and the approximation assuming a zero electrical time
constant. We call this the zero inductance (or instantaneous
electrical response) approximation. Under this approximation
\begin{align}
V = \frac{RM}{k_T} \ddot \theta   + \frac{R \nu}{k_T} \dot\theta + \frac{R}{k_T} \tau
+ k_b \dot \theta
\end{align}
\begin{align}
  \ddot \theta   =  \frac{k_T}{RM} V  - \frac{1}{M} \tau  -  \Big( \frac{k_T k_b}{R M}+ \frac{\nu }{M} \Big)\dot \theta\nonumber\\
= \frac{1}{M} \Big( \frac{k_T}{R} V  -  \tau  -  \Big( \frac{k_T k_b}{R}+ \nu \Big)\dot \theta\Big)
\end{align}
Thus the time constant for the motor speed, known
as the {\em mechanical time constant}, is as follows
\begin{align}
c_m= M  \Big( \frac{k_T k_b}{R }+ \nu\Big)^{-1}
\end{align} 


\paragraph{Current Spikes:}
 
When we apply a voltage to a stationary DC motor or when we reverse
voltages we can get large current spikes. There is a misconception
that these spikes are due to the motor's inductance, but that's not
correct. The spikes are due to the mechanical time constant, not the
electrical time constant. In fact for a given winding resistance $R$
and torque constant $k_T$ the smaller the inductance the larger the
current spikes will be. The motor's inductance acts as a low pass
filter for the current, smoothing down the current spikes (see
Figure~\ref{fig:transient}).

\begin{figure}[h]
\begin{center}
\includegraphics[height=2.1in]{Media/current.pdf} 
\includegraphics[height=2.1in]{Media/velocity.pdf} 
\caption{{ \it {\bf Left}: Current (blue) and approximation using a zero electrical time constant (red) . {\bf Right}: Angular velocity (blue) and approximation using a zero electrical time constant (red). } \label{fig:transient}} 
\end{center}
\end{figure}


The magnitude of the current spikes can be easily estimated. When the
motor is stationary and we apply a voltage $V$, the current will jump
to the following start up value 
\begin{align}
I_{start\; up} = \frac{V - k_b \dot \theta}{R} = \frac{V}{R}
\end{align}
as the motor speed catches up, the current will progressively
decrease. For a given voltage range $[ -V, V]$ the largest current
spike occurs when we are running the motor at maximum speed, i.e., no
load speed, and suddenly we reverse the voltage. The no load speed is as follows
\begin{align}
\dot \theta_n = V \frac{k_T}{R} \Big(\nu + \frac{k_b k_T}{R}\Big)^{-1}
\end{align}
An upper limit to this speed can be obtained by  setting the viscous friction to zero,
in which case
\begin{align}
\dot \theta_n \approx \frac{V}{k_b}
\end{align}
Thus, the reversal current can be bounded as follows
\begin{align}
I_{reversal} \approx  -\frac{V}{R} - k_b  \frac{V k_t}{R k_b k_T/R}  = -2 \frac{V}{R}
\end{align}
Thus, in general a useful bound on the current spikes is $\pm 2 V/R$. 


  


\begin{figure}[h]
\begin{center}
\includegraphics[height=3in]{Media/transient.pdf} 
\caption{\it  Simulation of transient behavior of Maxxon Amax 22 motor. A 6 Volt step function  is applied  at  the point in time showing a steep increase in current. The voltage is maintained and then reverse to -6 Volt step. This is done at the point in time showing a steep decline in current.  The figure shows the resulting current and angular velocity for a 100 millisecond period.  \label{fig:transient2}} 
\end{center}
\end{figure}





Because of the large current draws that may occur when suddenly
changing the supply voltage, it is important to have hardware capable
of handling these current spikes. One approach is to avoid abrupt
changes in voltage. For example, to accelerate and decelerate by
slowly changing the voltage (see Figure~\ref{fig:transient3}).



\begin{figure}[h]
\begin{center}
\includegraphics[height=3in]{Media/transient2.pdf} 
\caption{\it  Simulation of transient behavior of Maxxon Amax 22 motor with exponential voltage stepup. Note in this case the terminal speed is achieved with a much smaller current spike than if we had suddenly increased the voltage from zero to 6 Volts.   \label{fig:transient3}} 
\end{center}
\end{figure}



\section{Example: Maxxon Amax 22, 5 Watt motor}

The parameters for the Maxxon Amax 22, 5 Watt motor are as follows. 

\begin{verbatim}
V= 6 Volts %Recommended Voltage. 
% All the parameters are with respect to this voltage . 
L = 0.11/1000  Henrys
R = 1.71;  Ohmns
k_T = 5.9/1000  Newton Meters/Amp
M =  3.88/10^7  % Moment of intertia Kg m^2 
nu =  12/10^7% .Motor damping. New M/(rad/sec) 
\end{verbatim}
It follows that the stall torque, stall current , and no load velocities are as follows 
\begin{align}
&\tau_s = V \frac{k_T}{R} = (20.7018)10^{-3} \; \text{Newton Meters} \\
&I_s = \frac{V}{R} =3.5088\;\text{Amps}\\
&\dot \theta_n = V \frac{k_T}{R} \Big(\nu +\frac{ k_b k_T}{R}\Big)^{-1} = 1008.5\; \text{rads/sec} = 9630.7 \;\text{RPM}
\end{align}
 Figure~\ref{fig:torquevel} shows the torque/velocity function. The
 torque, current and velocity at the points of maximum mechanical
 power follow
\begin{align}
&\tau_{mp} = \frac{1}{2} \tau_s = (10.3509) 10^{-3}\; \text{Newton Meters}\\
&I_{mp} = \frac{1}{2} = I_{s}= 1.7544\;\text{Amps}\\
&\dot \theta_{mp} = \frac{1}{2} \dot\theta_n= 4815.4\; \text{RPM}
\end{align}
Thus the  maximum mechanical power delivered by the motor is as follows
\begin{align}
P_{mp}  = \tau_{mp} \; \dot \theta_{mp}   = 5.2196\; \text{Watts}
\end{align}
For maximum power transfer the input power is 
\begin{align}
V I_{mp} = 10.5263
\end{align}
Thus the efficiency at the point of maximum mechanical power is 
\begin{align}
\eta_{mp} =\frac{ 5.2196}{10.5263} = 0.4959
\end{align}
The efficiency  as a function of the angular velocity 
\begin{align}
\eta = \frac{\tau_s\dot \theta - \Big(  \nu  + \frac{k_bk_T}{R}\Big) \dot \theta^2}
{V^2/R - \tau_s \dot \theta }
\end{align}
is displayed in Figure~\ref{fig:efficiency}. Using equations \eqref{eqn:me1} and \eqref{eqn:me2} we find
                                   
\begin{align}
&\dot \theta_{me} = 8827.38 \;\text{RPMs}\\
&\eta_{me} = 0.8332
\end{align}



The electrical time constant is 6 hundredths of a millisecond
\begin{align}
c_e= \frac{L}{R} = 0.06 \; \text{ Milli secs}
\end{align}
The mechanical time constant is 18.9 milliseconds 
\begin{align}
c_m= M  \Big( \frac{k_T k_b}{R }+ \nu\Big)^{-1} =  18.9\; \text{Milli secs}
\end{align}

The current spikes that would occur when using voltage step functions in the $[-V, V]$ range are bounded by
\begin{align}
\pm \frac{2V}{R} = \pm 7.02 \;\text{Amps}
 \end{align}
We simulated the transient response to a Voltage step function (0 to 6
Volts). At equilibrium we then reverse the voltage (see
Figure~\ref{fig:transient}). We find that the maximum current draw at
start up is $3.4534$ Amps. The maximum current draw when we reverse
voltage is $6.878$ Amps.


\paragraph{Gears}
If we add the Maxxon 110338 gear we get that the reduction is 19:1,
the gear moment of inertia is $0.5 (10^{-7} \; Kg\; M^2$ . There
is no information about the viscous friction. However   the maximum efficiency of the gear said to be $84 \%$. From this I derived that the viscous friction of the gear must be 142 times the viscous friction of the motor. 



\newpage




\section{Appendix}
   
\paragraph{Important Constants for DC Motors (SI Units)}

\begin{itemize}
\item $L$: Inductance. In Henrys.
\item $R$: Resistance. In Ohms.
\item $M$: Moment of inertia. In Kg $m^2$
\item $k_T$:  Torque constant. NewtonMeters/Amp
\item $k_b$: or $k_e$:   Back Emf constant, or Voltage constant: Volts/(radians/sec). When using SI units $k_b = k_T$
\item $K_v$.: Velocity (or speed constant). (radians/sec)/Volts. When using SI units, $k_v= 1/k_e$
\item $\nu$: Viscous Damping (Newton m/ (radians/sec)
\item $c_m$: Mechanical time constant (secs).  
\item $c_e$: Electrical time constant (secs). 
\end{itemize}









\paragraph{SI Units (International System of Units)}
\begin{itemize}
\item Mass: Kg
\item Force: Newton
\item Pressure: Pascal $(Newton/m^2)$
\item Power: Watt
\item Energy: Joule (Newton Meter)
\item Electric Potential: Volt
\item Charge: Coulomb 
\item Capacitance: Farad
\item Resistance: Ohm
\item Inductance: Henry
\item Length: Meter
\item Current: Ampere
\item Time: Second
\item Torque (moment of force): Newton Meter
\item Moment of Inertia: Kg $m^2$
\item Angular Velocity: Radian/Sec
\end{itemize}

\subsection{Maximum Power Efficiency}
We need to optimize 
\begin{align}
\rho(x) = \frac{a x - bx^2}{c - ax}
\end{align}
as a function of $a$. We do so by taking the gradient of the logarithm of $\rho$ and setting it to zero
\begin{align}
\nabla_x \log \rho &= \frac{a - 2bx}{ax - bx^2} + \frac{ a}{c-ax}
= \frac{ ac - 2bcx - a^2 x + 2 ab x^2 + a^2 x - ab x^2}{(ax-bx^2)(c-ax)}\\
&= \frac{ ab x^2 - 2bc + ac}{(ax-bx^2)(c-ax)}
\end{align}
Setting the numerator to zero and solving for $x$ we get
\begin{align}
x = \frac{bc \pm \sqrt{b^2 c^2 - a^2 bc}}{ab}
\end{align}
When applying this to the maximum efficiency problem we find that the solution with the plus sign produces a velocity larger than the no load velocity, so the only value solution is the one with the minus sign. 


\subsection{Actuator Data for the PUMA 560}
Main Source: A Search for Consensus Among Model Parameters Reported for
the PUMA Robot by 
Peter I Corke and Brian Armstrong-Helouvry

\begin{center}\begin{tabular}{|c|c|c|c|c|c|c|c|c|}\hline
Link &M &R &L  &Kb &Kt &GearRatio &VisFric &StatFric\\ \hline
 &Kg-m2 &Henry &Ohm s &V/rad/s &oz in/Amp & &Nm/rad/sec &Nm\\ \hline
1 &200e-6&1.6 &0.0048 &0.19 &0.2611 &62.55& 4.19 &8.35\\ \hline
2 &200e-6&1.6 &0.0048 &0.19 &0.2611 &107.81 &8.1 &12.05\\ \hline
3 &200e06&1.6 &0.0048 &0.19 &0.2611 &53.15 &3.14&5.75\\ \hline
4 &18e-6&3.9 &0.0039 &0.12 &0.0988 &76.04& & \\ \hline
5 &18e-6&3.9 &0.0039 &0.12 &0.0988 &71.92& & \\ \hline
6 &18e-6&3.9 &0.0039 &0.12 &0.0988 &76.65& & \\ \hline
\end{tabular}
\end{center}
\newpage
\subsection{Motor Simulator}
\begin{verbatim}
clear
%Parameters for Maxxon Amax 22, 5 Watt, 6 Volts motor
  R = 1.71; %motor resistance in ohms
  L = 0.11/1000; % motor inductance in Henris
  Kt = 5.9/1000; % torque constant in Newton Meters/Amps
  Kb = Kt; 
  M = 3.88/10000000;
  b = 17/100000000 ; % motor damping in New m/(rad/sec)
  CI = 0.840; % Max current for continuous operation Amps
  CT = CI*Kt; % Torque for continuous operation Newton Meters
  Vs = 6; % Reference Voltage in Volts


taue = L/R;  % electrical time constant
taum = M/(Kb*Kt/R + b); % mechanical time constant 

dt = taue/100; % time step in seconds

T= 20*taum;; % simulation time in secs
s = ceil(T/dt);
V=Vs;
I=zeros(s,1);
I2=zeros(s,1);
Omega= zeros(s,1);
Omega2= zeros(s,1);
V= zeros(s,1);

I(1)=0;
Omega(1)=0;
Omega2(1)=0;

for t=1: s
  V(t) = Vs;
  dI = (V(t) - R*I(t)  - Kb*Omega(t) )*dt/L;
  tau = Kt *I(t);
  dOmega = (tau - b*Omega(t))*dt/M;
  Omega(t+1) = Omega(t) + dOmega;
  I(t+1) = I(t) + dI;
  
end

OmegaNL = Vs*Kt/(R*( b + (Kb*Kt)/R));
stallTorque = Kt*Vs/R;
stallCurrent = Vs/R;
maxPowerVelocity = 0.5*OmegaNL; 
maxPowerTorque = 0.5*stallTorque;
maxPowerCurrent= 0.5*stallCurrent;
maxMechanicalPower = maxPowerTorque*maxPowerVelocity; 
efficiencyAtMaxPower = maxMechanicalPower/(Vs*maxPowerCurrent);

do = OmegaNL/100000;
o = 0:do:OmegaNL;
a1 = stallTorque;
b1 = (b + Kt*Kb/R);
c1 = Vs^2/R;

efficiency = (a1*o - b1.*o.*o)./(c1 - a1*o);
mostEffVelocity = (b1*c1 - sqrt(b1*b1*c1*c1-a1*a1*b1*c1))/a1/b1;
maxEff= (a1*mostEffVelocity 
    - b1*mostEffVelocity*mostEffVelocity)/(c1-a1*mostEffVelocity);

disp(sprintf('Stall Torque: %f milli Newton Meters', stallTorque*1000))
disp(sprintf('Stall Current: %f Amps', stallCurrent))
disp(sprintf('No Load Speed: %f RPM', OmegaNL*60/2/pi))
disp(sprintf('Max Power Torque: %f milli Newton Meters ', ...
             maxPowerTorque*1000))
disp(sprintf('Max Power Current: %f Amps ', ...
             maxPowerCurrent))
disp(sprintf('Max Power Velocity: %f RPM', maxPowerVelocity*60/2/pi))
disp(sprintf('Max Mechanical Power : %f Watts ', maxMechanicalPower))

disp(sprintf('Efficiency at Max Mechanical Power : %f Percent ', ...
             100*efficiencyAtMaxPower))

disp(sprintf('Max Motor Efficiency : %f Percent ', ...
             100*maxEff))

disp(sprintf('Most Efficienct Velocity : %f  RPMs', ...
             mostEffVelocity*60/2/pi))

disp(sprintf('Electrical Time: Constant %f msecs', 1000*taue))
disp(sprintf('Mechanical Time: Constant %f msecs', 1000*taum))
disp(sprintf('Start Up Current: %f Amps', V/R))
disp(sprintf('Reverse Current: %f Amps', 2*V/R))

\end{verbatim}
\end{document}
