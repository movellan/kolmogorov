% The h function in thin plate model for air at 70 degree Farenheit
% Pa: Chamber A pressure: Pascals
% Pb: Chamber B pressure: Pascals
% We assume Pa >= Pb
% y is flow in kg/sec

function y = h(Pa, Pb)

if Pa< Pb
  print 'in h(Pa,Pb) Pa needs to be larger than Pb'
end
if Pb <=0
  print 'in h(Pa,Pb) Pb needs to be larger than 0'
end

M = 0.029; % air molecular mass (Kg/mol)
rho = 1.2; % density of air (Kg/cubic meter)
R = 8.314472; % Universal gas law constant in (Pa m3)/ (mol Kelvin)

c= 0.72 ; % Discharge coefficient  (dimensionless)
k = 1.4 ;% specific heat ratio of air (dimmensionless)
Z = 0.9987; % air compressibility factor at p= 5 bars and T=300K, dimensionless



T = 70; % Temperature in Farenheit
T= 5/9 * (T - 32) + 273 ;% Convert Farenheit to Kelvin

alpha = c *sqrt(2*M*k/(Z*R*T*(k-1)));
beta = c *sqrt( k*M/(Z*R*T) *((2/(k+1))^((k+1)/(k-1))));
theta = ((k+1)/2)^(k/(k-1));
Pr = Pa/Pb;


if Pr >= theta
  y = beta* Pa;
else
  y = alpha* Pa*sqrt( Pr^(2/k) - Pr^((k+1)/k));  
end


