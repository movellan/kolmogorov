% Simulate Cylinder with 2 chambers and 2 valves per chamber. We treat
% the valves indepdendently.
% Each chamber has a reservoir, which is the minimum volume of the
% chamber (when piston is pusehd all the way down against the chamber).
%
% The bore the piston has non-zero width. In practice what matters is the
% difference between the lenght of the cylinder and the width of the
% bore, i.e., the stroke lenght



% [R1| Chamber 1  | Chamber 2 | R2 ]====== Rod==== 
%     <--- x- --->  
%  where Ri = Reservoir for chamber i
%
%
% Each chamber has 2 valves, a supply valve that connects to a compressor
% line, and an exhast valve that connects to the atmosphere
%
% Valve (i,j) is  valve  j in chamber i. 
%
% Valve(., 1) is supply, Valve(.,2) is exhaust.
%
% Copyright @ Javier R. Movellan, UCSD, 2008.
% GNU style license and distribution. 

clear
clf


global alpha beta theta k  

%%%%%%%%%%%%%%%%%%% Cylinder Parameters %%%%%%%%%%%
sP = 250; % supply  (compressor) pressure (Psi)
eP = 14.5; % exhaust (atmospheric) pressure  (Psi)
T = 70; % Temperature in Farenheit
uMax =1; % Maximum  voltage to valve. The radius of the aperture of the
         % valve is proportional to the voltage. 

rMax= 1/16; % max Radius of valve port (orifice) 


kS= 0; %100; % Static friction of piston in Newtons
kV = 100;% 100; % Viscosity friction coefficient of piston  in N/(m/s)
kC = 0*kS/2; % Coulomb friction of piston in Newtons

Br= 1; % Bore radius (inches)
l = 2; % Stroke length (inches)

m = 4; % Mass of bore + load in Kg

x = 0; % location of bore from origin of chamber 1, (in inches)


bl = 1.25; % lenght of bore in inches ( irrelevant if l is known)
cl = l+ bl; %cylinder length (irrelevant if l is known)



rV(1) = 1*pi*(0.6^2); % Reservoir for chamber 1 (cubic inches)
rV(2) =  0.75*pi*(0.35^2); % reservoir for chamber 2 (cubic inches)

% The reservoir is the minimum volume of the chamber when the bore is
% pushed all the way against the chamber. Thus a chamber can never have
% zero volume



dt= 0.00001; % time step in seconds
         % iterate through time

graphDt= 2/300; % time in simulated seconds between graph updates


initGraphs;



%%%%%%%%%%%%%%  Gas Parameters and Conversions  %%%%%%%%%%%%%%%%%%%

sP =  sP*6894.76; %Convert Psi to Pascals 
eP =  eP*6894.76; %Convert Psi to Pascals 
rMax = rMax*0.0254; % Convert inches to meters
Br = Br*0.0254; % Convert inches to meters
l = l *0.0254; % Convert inches to meters
x = x *0.0254; % Convert inches to meters
bl = bl*0.0254; % Convert inches to meters
cl = cl*0.0254; % Convert inches to meters
rV(1) = rV(1)*(0.0254^3); % from cubic inches to cubic meters
rV(2) = rV(2)*(0.0254^3); % from cubic inches to cubic meters
BoreArea = pi*Br^2; % in square meters
T= 5/9 * (T - 32) + 273 ;% Convert Farenheit to Kelvin
M = 0.029; % air molecular mass (Kg/mol)
rho = 1.2; % density of air (Kg/cubic meter)
R = 8.314472; % Universal gas law constant in (Pa m3)/ (mol Kelvin)
c= 0.72 ; % Discharge coefficient  (dimensionless)
k = 1.4 ;% specific heat ratio of air (dimmensionless)
Z = 0.9987; % air compressibility factor at p= 5 bars and T=300K, dimensionless


alpha = c *sqrt(2*M*k/(Z*R*T*(k-1)));
beta = c *sqrt( k*M/(Z*R*T) *((2/(k+1))^((k+1)/(k-1))));
theta = ((k+1)/2)^(k/(k-1));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% State variables are
% r(i,j) % radius of valve j in chamber i
% dotM(i) % air mass flow into chamber i
% dotX % velocity of rod
% PC(i) % pressure of chamber i
% x -> V(i) % location of Bore determines volume of each chamber


loadF=0; % External Load Force
u= 0*ones(2,2); % control signal (voltage) to valve(i,j). The radius of
                % valve (i,j) is rMax*u(i,j)/uMax 


% control voltages to each valve
u(1,1)=1;
u(1,2)= 0;
u(2,1) = 0;
u(2,2) = 0*1.0;




dotM = zeros(2); % dotM(i) is air flow of air out of the chamber (Kg/sec)
dotX=0; % Velocity of the bore
PC= eP*ones(2,1); %Pressure of each chamber
tIndex = 0;
gIndex=0; % for graphics

dirSign(1) = 1; % relates bore velocity into direction of pressure velocity
dirSign(2) = -1; % relates bore velocity into direction of pressure velocity

t=0;


while(t<2) % infinite loop for he simulation
  
  %%%%%%%%%% example sinusoid control signal
  % u(1,1)= 0.5+0.5*sin(2*pi*t);
%   if(u(1,1) >0.5) u(1,1)=1;
%   else u(1,1) =0;
%   end
%   u(1,2)= 1 - u(1,1);
%   u(2,1)= u(1,2);
%   u(2,2)= u(1,1);

  
  tIndex = tIndex+1;


  
  % Each port interfaces two volumes with different pressures. Here we set the
  % pressures of each side of each port P(i,j,k) is the pressure on side k
  % of port j in chamber i For each port, side 1 is external to the chamber,
  % side 2 is the chamber
  for i2=1:2 % chambers
    P(i2,1,1) = sP; 
    P(i2,1,2) = PC(i2); %Pressure of chamber 
    P(i2,2,1) = eP;
    P(i2,2,2) = PC(i2);
  end


  V(1) = BoreArea*x+rV(1);
  V(2) = BoreArea*(l -x)+rV(2);
  r = rMax*u/uMax; % set radius of each valve 

  for i=1:2 % chamber
    dotPC(i)=0;
    for j=1:2  % port 
      dotM(i,j) = thinPort(P(i,j,1), P(i,j,2), r(i,j));
      dotPC(i) = dotPC(i)+ k*R*T*dotM(i,j)/V(i);
    %  dotM(i,j) =dotM(i,j)/rho; % Convert from Kg/sec to m^3/sec
    %  dotM(i,j) = dotM(i,j)* (3.28^3)*60; % Convert from m^3/sec to
    %  cubic feet per minute
    end
    dotPC(i) = dotPC(i) -PC(i)*BoreArea*k*dotX/V(i)*dirSign(i);
    PC(i) = PC(i)+ dt*dotPC(i);
    
  end
  pF = (PC(1) - PC(2))*BoreArea; % Pneumatic Force
  if (abs(dotX) < 0.00001 && abs(pF) < kS)   % static Friction Force
    sF  = - pF; 
  else 
    sF =0;
  end
  vF = -dotX*kV; %Viscous Force
  cF = -sign(dotX) *kC; % Coulomb force

  if(t>1.5) % Put a load 1 sec into the simulation. Just to see what happens 
    loadF =   -00; 
  end 
 

  dotDotX = (pF+sF+vF+cF+loadF)/m; % Newton's law

  dotX = dotX+ dt*dotDotX;
  x = x + dt*dotX + 0.5*dt*dt*dotDotX;
  if( x< 0) x=0;dotX=0; end
  if(x>l) x = l;dotX=0; end
  
  if(rem(tIndex,floor(graphDt/dt)) <1) % graph every  graphDt seconds
    realTimeGraphs;
  end
  t=t+dt; 
end


