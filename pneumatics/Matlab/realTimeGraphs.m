gIndex= gIndex+1;

gXIndex = rem(gIndex,nPoints)+1; %

graphY{1}(gXIndex) =x*39.3701;
graphY{2}(gXIndex)= PC(1)*0.000145038; %convert from pascal to psi
graphY{3}(gXIndex)= PC(2)*0.000145038; %convert from pascal to psi
graphY{4}(gXIndex)= u(1,1)/uMax;
graphY{5}(gXIndex)= u(1,2)/uMax;
graphY{6}(gXIndex)= u(2,1)/uMax;
graphY{7}(gXIndex)= u(2,2)/uMax;

for i=1:nGraphs
  set(lh{i},'ydata',graphY{i});
  set(lb{i},'xdata',[gXIndex gXIndex]*graphDt);
end
pause(nGraphs*0.1/7); % the more graphs the more we need to pause. The
                      % necessary pause may be machine dependent 

vRModel.OB_BoreRod.translation=[0 1.25+x*39.3701  0]; % virtual reality cylinder

