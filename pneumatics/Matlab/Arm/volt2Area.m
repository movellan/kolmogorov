function u = volt2Area(v, maxArea)
    cap = @(x) min(1,max(0,x));
    intercept=0.25;
    delta = 5; %neutral voltage
    slope=(1-intercept)/delta;
    uc = maxArea*cap(intercept + slope*(v-delta));
    ua = maxArea*cap(intercept -slope*(v-delta));
    u=[uc ua];
    return;
    

