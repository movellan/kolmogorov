function [pcal]=asymPressure()
    global param;
    isTesting = 1;
    param=setupConst;
    volts = 3:0.1:7;
    prs = zeros(size(volts));
    us = zeros(length(volts),2);
    for i=1:length(volts)
        i
        if isTesting
            load('volts_prs.mat','pcal');
            v=pcal(volts(i)-5);
        else
            v=volts(i);
        end
        us(i,:)=volt2Area(v, 0.001^2);
        prs(i)=findTerminalPressure(us(i,:),0.1,0.01^2);
    end
    %
    %clf
    subplot(2,1,1)
    plot(volts, us);
    xlabel('control voltage');
    ylabel('valve area');
    legend('compressor','atm');
    subplot(2,1,2)
    plot(volts, prs,'+');
    grid on
    xlabel('control voltage');
    ylabel('asym pressure');
    if ~isTesting
        prs = (prs-min(prs))/(max(prs)-min(prs));
        psig=lsqnonlin(@(x) prs-1./(1+exp(-x(1).*(volts-x(2)))),[0 0]);
        pcal = @(x) (x)./psig(1)+psig(2);
        save('volts_prs.mat', 'volts','prs','pcal');
    end
 return


function newp=findTerminalPressure(u, x, bodeArea)
global param
p=inf;
newp=param.pATM;
dt=0.001;
iter=0;
while abs(newp-p)>1
    p=newp;
    dp=dt*dPressure(p,u,x,0,bodeArea);
    newp=p+dp;
    iter=iter+1;
    if(iter>1e6)
        error('diverge');
    end
end
newp
iter
