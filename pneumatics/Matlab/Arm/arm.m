clear
clf
% arm
param=setupConst; 


%Joint1 Property
j1_l = 0.1;
j1_a = 0.12;
j1_b = 0.05;
j1_s = 0.09;
j1_vL= 0.08;
j1_kVis= 100; 
m1=0.02; % 20g
m2=2; % 100kg
M = m1*j1_b^2 + m2*j1_l^2;
load('volts_prs.mat','pcal');
maxValveArea=0.005^2; %full open oriface


joint = makeJoint(j1_vL, pi*0.005^2, 'linear', 200,-1, j1_a, j1_b);

%simulation -------------------------------
%state space {p1x p1r theta dotTheta}
dt=0.001;
simDur = 5;
simLen = simDur/dt;
theta = zeros(simLen+1,1);
thetaDot = zeros(simLen+1,1);
p1x = zeros(simLen+1,1);
p1r = zeros(simLen+1,1);
u1=zeros(simLen+1,4);
ctrMethod='pidf';
nf=4;
vf = 100*velocityFilter(nf);
lasterr = zeros(1,nf);
erri=0;
% trajectory generation--------------------
trj = ones(simLen+1,1);
trj(:) = 0.4*pi;
trj(0.5/dt:1/dt)=0.6*pi;
trj(2/dt:3.5/dt)=0.4*sin(.01*(0:1.5/dt))+0.4*pi;
%trj = smooth(trj,100);

%initialize -------------------------------
theta(1) = trj(1);
thetaDot(1)=0;
p1x(1) = param.pATM;
p1r(1) = param.pATM;
%% prop-valve to compressor/atm
u1x = [0 0]; 
u1r = [0 0];


u(1,:)=[u1x u1r];

for i=1:simLen
	%valve control, theta, trj, (local vars)
    if mod(i*dt,0.01)==0
        switch(lower(ctrMethod))
            case {'pid','pidf'}
                if strcmp(lower(ctrMethod),'pid')
                    kp=5;
                    ki=0.00;
                    kv=0.01;
                    ku=1.5;
                    kpr=0;
                else
                    kp=30;
                    ki=0.00;
                    kv=0.05;
                    ku=1.5;
                    kpr=-2;
                end
                errp=trj(i)-theta(i); 
                erri=0.99*erri+errp;           
                lasterr(1:end-1)=lasterr(2:end);
                lasterr(end) = errp;
                errv = lasterr*vf;                
                errpr = -1e-5*(p1x(i)-p1r(i));
                errPID = ku*(kp*errp+ki*erri+kv*errv+kpr*errpr);
                %pcal is zero-centered
                u1x = volt2Area(pcal(-errPID), maxValveArea); 
                u1r = volt2Area(pcal(+errPID), maxValveArea);
            case 'ilqt'
                st = [p1x(i) p1r(i) theta(i) thetaDot(i)]';
                uopt = Kt(i)*st;
                u1x = volt2Area(pcal(uopt(1)), maxValveArea);
                u1r =volt2Area(pcal(uopt(2)), maxValveArea);              
            otherwise
                error('no such control method');
        end
    end
    u1(i+1,:) = [u1x u1r];
    
    %% dynamics
	%calculate x1, x1Dot from theta, thetaDot
	x1com=sqrt(j1_a^2+j1_b^2 +2*j1_a*j1_b*cos(theta(i)));
	x1 =  x1com - j1_s;
	x1Dot = -thetaDot(i) * j1_a*j1_b*sin(theta(i)) / x1com;
	%pressure
	p1x(i+1) = p1x(i) + dt* dPressure(p1x(i), u1x, x1, x1Dot, joint(1).bodeArea);
	p1r(i+1) = p1r(i) + dt* dPressure(p1r(i), u1r, joint(1).length-x1, -x1Dot, joint(1).bodeArea);

	%theta
	theta(i+1) = theta(i) + dt*thetaDot(i);

	%thetaDot
	torP = joint(1).arm(theta(i)) * ((p1x(i)-p1r(i)) * joint(1).bodeArea-x1Dot*joint(1).kVis);
    
    %Handle joint limit
	if x1 <0.01
		torP=min(torP,0);
        thetaDot(i)=0;
        warning('hit joint limit max');
	elseif x1>joint(1).length-0.01;

		torP=max(torP,0);
        thetaDot(i)=0;
        warning('hit joint limit min');
    end
    
    torF=0; %assuming no joint friction
    tor = torP - torF;
	thetaDot(i+1) = thetaDot(i) + dt * (M\tor);
end

timePts = (0:dt:simDur)';
subplot(3,1,1);
hold on
ax=plotyy(timePts, theta, timePts, thetaDot); legend('theta','thetaDot');
plot(ax(1),timePts, trj,'r','LineWidth',5);
ylim(ax(1),[0 2]);
hold off
title(['Joints ' num2str(mean((theta-trj).^2))]);
grid on

subplot(3,1,2);
plot(timePts,u1);
legend('u1xc','u1xa','u1rc','u1ra')
title('valves');
grid on
subplot(3,1,3);
plot(timePts,[p1x, p1r]); legend('p1x','p1r');
title('Pressure');
grid on
