function f=velocityFilter(nf)
	% make velocity filter
	w = (1:nf);
	x = (1:nf);
	M = [sum(x.^2.*w) sum(x.*w); sum(x.*w) sum(w)];
	F = M \ [x.*w; w];
	f = F(1,:)';
