%Valve data strcutre

% linear joint
% a: distance from joint-axis to valve-axis
% b: distance from joint-axis to rod-arm junction
% dir: direction of drive wrt to theta (1, -1)

%rotational joint
%dir: direction of drite wrt to theta, also encode arm;

function v = makeJoint(minLength, bodeArea, driveType, kVis, dir, a, b)
	v.length=minLength; %valve rod length in meter
	v.bodeArea = bodeArea; %valve bode area in m^2
	v.driveType = driveType;
    v.kVis = kVis;
	switch(driveType)
		case 'linear'
		v.arm = @(theta) dir*a*b*sin(theta)/sqrt(a^2+b^2+2*a*b*cos(theta));
		case 'rotate'
		v.arm = dir;
	end
