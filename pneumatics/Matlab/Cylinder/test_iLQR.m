
function test_ilqg()
	figure(2)
	cla
	dt=0.01;
	nStep=400;
	xL=10;
	X0=[xL/2 0]';
	U0=[0 0]';%rand(2,1);%[.1 .1]';
	target = [9, 0]';%target point and velocity
	trj = repmat(target,[1, nStep]);
	costt = @(x,u,t) cost(x,u,t, target); 
	[x_ u_ L_ cost]=ilqg_det(@dyn, costt, dt, nStep, X0, U0, [0 0]',[1 1]');
	cost
	if ~isnan(cost)
		figure(1);
		clf
		subplot(2,1,1)
		plot([x_' trj']);
		subplot(2,1,2)
		plot(u_');
		suptitle('iLQR');
	end
end

function [xdot, xdot_x, xdot_u] = dyn(x, u)
	if any(u<0 | u>1)
		keyboard
	end
	M = 1;
	xdot=[x(2)  M\(u(1)-u(2))]';
	if nargout>1
		xdot_x = [0 1;0 0];
		xdot_u = [0 0;1/M -1/M];
	end
end

function [l, l_x, l_xx, l_u, l_uu, l_ux] = cost(x, u, t, target)
	wp=1e3;
	l= u'*u;
	l_x = zeros(2,1);
	l_xx = zeros(2,2);
	l_u = 2*u;
	l_uu = 2*eye(2);
	l_ux = 0;

	if isnan(t) %final cost
		l=wp*(x-target)'*(x-target);
		l_x= 2*wp*(x-target);
		l_xx=2*wp*eye(2);
	end
end

function g=finiteDiff(fn, z, EPS)
	g=zeros(length(z),length(z));
	for i=1:length(z)
		z1=z; z1(i)=z1(i)+EPS;
		z2=z; z2(i)=z2(i)-EPS;
		g(:,i)=(fn(z1)-fn(z2))/2/EPS;
	end
end
