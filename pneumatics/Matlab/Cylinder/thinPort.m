% Simulate thin Plate Port Model
% Pa: Chamber A pressure: Pascals
% Pb: Chamber B pressure: Pascals
% A: Orifice's radius: Meters
% Postive flow describes flow from chamber a to b

function dotM = thinPort(Pa, Pb, A)
global alpha beta theta kk

if(Pa > Pb) 
  num = Pb; den = Pa; dir =1;
else
  num = Pa; den = Pb; dir = -1; 
end
Pr= num/den;
if(den > theta*num)
  dotM = beta;    
else
  dotM = alpha* sqrt( Pr^(2/kk) - Pr^((kk+1)/kk));
end
dotM = dir*A*den*dotM;%flow in kg /sec
