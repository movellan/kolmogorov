function showArm(theta, p1,p2, tgtTheta)
    axis image manual
    xlim([-0.1 0.15]);
    ylim([-0.1 0.15]);
    cla

    j1_l = 0.1;
    j1_a = 0.12;
    j1_b = 0.05;
    j1_s = 0.09; %pistone rod length
    j1_vL= 0.08; %pistone cycle length    
	j1_vW= 0.03;

    %if input is theta
    if ~exist('theta')
        theta=pi/3;
    end
    
    %if input is length
    if theta<0
        lambda=-theta;
        theta = pi-acos((j1_a^2 + j1_b^2 - (lambda*j1_vL+j1_s)^2)/(2*j1_a*j1_b));
    end
    
    
    pwidth = 0.01;
    
    phi = asin(4/13);
	ovec=[0 0];
    avec = j1_a*[sin(-phi) cos(-phi)];
	evec=  j1_a*[0 cos(phi)];
    uvec = [sin(theta-phi) cos(theta-phi)];

    x2vec = j1_l*uvec;
    x1vec = j1_b*-uvec;
	
    
    k1 = (x1vec-avec)./norm(x1vec-avec);
	k2 = [k1(2) -k1(1)];

	%pistone
	cvec=k1*j1_vL+avec;  %bottom
	dvec=-k1*j1_s+x1vec; %bode

	%top-half corners
	p_top=[avec+k2*j1_vW/2;
		 dvec+k2*j1_vW/2;
		 dvec-k2*j1_vW/2;
		 avec-k2*j1_vW/2];
	%bottom half conners
	p_bot=[cvec+k2*j1_vW/2;
		 dvec+k2*j1_vW/2;
		 dvec-k2*j1_vW/2;
		 cvec-k2*j1_vW/2];

	%origin



	%arm skeleton (black part)
    linep(ovec, evec,'k');
	linep(evec, avec,'k');

	%%pistone components
    cmap = colormap('autumn');
    c1=val2color(p1,cmap);
    c2=val2color(p2,cmap);

    patch(p_top(:,1),p_top(:,2),c1,'LineSmoothing','on');
	patch(p_bot(:,1),p_bot(:,2),c2,'LineSmoothing','on');
	%bode
	linep(dvec+k2*j1_vW/2, dvec-k2*j1_vW/2,'b');
	%rod
    linep(-k1*j1_s+x1vec,x1vec,'b');

    %arm skeleton blue part
    linep(x1vec,x2vec, 'b'); % x1-x2
    circle(ovec,pwidth/2, 'k'); %origin
    circle(evec,pwidth/2,'k');
    circle(avec,pwidth/2,'k');  %top of pistone
    circle(x1vec, pwidth/2,'b') %x1
    circle(x2vec, pwidth,'b'); %x2
    
    %moment arm
    marm=avec-avec*k1'*k1;
    linep(marm, ovec, 'r',2);
    
    %target
    if exist('tgtTheta')
        if tgtTheta <0
            tgtLambda=-tgtTheta;
            tgtTheta = pi-acos((j1_a^2 + j1_b^2 - (tgtLambda*j1_vL+j1_s)^2)/(2*j1_a*j1_b));
        end
        tvec = [sin(tgtTheta-phi) cos(tgtTheta-phi)];
        tgtvec= j1_l*tvec;
        circle(tgtvec, pwidth/2,'g')
    end

    %title(num2str(theta));

    function linep(p1,p2,c, w)
        ob=line([p1(1);p2(1)], [p1(2);p2(2)]);
		if ~exist('w','var'); w=5;end
		set(ob, 'LineWidth',w);
		set(ob, 'LineSmoothing','on');
        set(ob,'Color',c);
    
    function circle(xvec,r,c)
        ob=rectangle('Position',[-r+xvec(1) -r+xvec(2) 2*r 2*r],'Curvature',[1 1]);
        if nargin >2
            set(ob,'EdgeColor',c);
            set(ob,'FaceColor',c);
        end

    function c=val2color(p, cmap)   
        mlen=size(cmap,1);
        c = cmap(ceil(p*(mlen-1))+1,:);
        
%animation
% for i=0:0.1:pi,showArm(i),pause(0.2),end
