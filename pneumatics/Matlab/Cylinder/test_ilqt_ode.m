function test_ilqg(m, seed)
	%figure(2);cla
    if exist('seed');
		rand('seed',seed);
	else
		rand('seed',1);
	end

	%initialize 
	simDur=0.5;
	dt=0.0001;
	nStep=simDur/dt;
	xL=0.10; % 10 cm
	if 0
		%step trajectory
		stepWidth=.15/dt;
		trj = smooth(.02+.5*floor((1:nStep)/stepWidth)/(nStep/xL/stepWidth), .01,'lowess')';
		trj_name ='step'
	else
		%step trajectory
		%random
		pad=0.02;
		rndstep=simDur/50/dt; %create 10 steps in (simDur) secs.
		trj=repmat(rand(1,floor(nStep/rndstep)), rndstep, 1);
		trj=pad + (xL-2*pad)*[trj(:)'];
		trj = smooth(trj, .02, 'lowess')';
		trj_name ='rnd'
	end

    p=setupConst;
	X0 = [trj(1); 0;5*1e5;5*1e5];
	costt = @(x,u,t) cost(x,u,t, trj, [0 xL]);
	dynl = @(x,u) dyn(x,u, [0 xL]);
	uL=[0 0 0 0]';
	uH=[1 1 1 1]';
	%obtain p-trj
	kopt_ = [10 0 .001];
	switch(m)
		case {'pid','pidf'}
			minval=inf;
			if strcmp(m,'pid');
				kopt=[   10.9867    0.0001    0.2463         0]; 
				%kseeds = rand(20,3)*diag([60 1 1]);
			else
		  		kopt=[ 267.8465    0.2531    1.1750    0.5413]; %
				%kseeds = rand(20,4)*diag([400 1 1 1]);
			end
			if exist('kseeds');
				tic
				oopt = optimset('TolX',0.1, 'TolFun',0.001,'Display','iter');
				for i=1:length(kseeds)
					if strcmp(m,'pid')
						[kopt_ fval_]=fminsearch( @(x) pid(dynl, trj,dt, nStep, X0, uL, uH, xL, [x 0]), kseeds(i,:),oopt);
						if fval_<minval
							kopt=[kopt_ 0]
							minval=fval_
						end
					else
						[kopt_ fval_]=fminsearch( @(x) pid(dynl, trj,dt, nStep, X0, uL, uH, xL, x), [kseeds(i,:)],oopt);
						if fval_<minval
							kopt=kopt_
							minval=fval_
						end
					end
				end
				toc
			end
			[cost_ x_ u_ ] = pid(dynl, trj, dt, nStep, X0, uL, uH, xL, kopt);
			kopt
		case 'ilqg'
			%use pid trajectory to initialize ILQG
			if 1
				u_=rand(4,1);%[.1 .1]';
			else
				kopt=[6.8261    0.0000    0.1711 0];
				%kopt=[  435.2609    1.0361    1.1712   1.1946];
				[cost_ x_ u_] = pid(dynl, trj, dt, nStep, X0, uL, uH, xL, kopt);
			end
			[x_ u_ L_ cost_]=ilqg_det(dynl, costt, dt, nStep, X0, u_, uL, uH);
		otherwise
			error('no such method');
	end
	save(sprintf('log_%s_%s.mat', trj_name, m));
	se=sum( (x_(1,:)-trj).^2)
    showplot(x_, trj, u_, xL);
    showArmAni(x_, trj, xL,dt);
end

function showArmAni(x, trj,xL,dt)
    p=setupConst;
    aniDt=0.002;
    figure(2)
    pcolor(randn(2,2));
    colorbar
    colormap ('autumn');
    zlim([-1 1]);
    for i=1:aniDt/dt:length(trj)
        lambda = x(1,i)/xL;
        p1 = (x(3,i)-0.9*p.pATM)./(1.1*p.pCompressor-.9*p.pATM);
        p2 = (x(4,i)-0.9*p.pATM)./(1.1*p.pCompressor-.9*p.pATM);
        tlambda = trj(i)/xL;
        showArm(-lambda, p1, p2, -tlambda);
        pause(aniDt);       
    end
end

function showplot(x_, trj, u_, xL)
    % state trajectory
    % desired position trajectory
    % action trajectory
    % length
		figure(1)
		clf
		subplot(3,1,1)
		ax=plot([trj' x_(1,:)']);
		set(ax(1),'LineWidth',3,'Color',[.9 .9 .9]);
		ylim([0 xL]);
        title('trajectory')

		subplot(3,1,2)
		plot([x_(3:4,:)']);
        legend('p1','p2','Location','West');
        title('pressure');

		subplot(3,1,3)
		%plot([u_' upid_'+2]);
        plot((repmat((0:3)',1,size(u_,2))+u_)')
        title('control');
end

function [cost x u]= pid(fnDyn, trj, dt, n, X0, uL, uH, xL, kgain)
	u=zeros(length(uL), n-1);
	x=zeros(length(X0), n);
	x(:,1)=X0;
	cost=0;
	if any(kgain<0)
		cost=cost+1e3;
	end
	kp=kgain(1);
	ki=kgain(2);
	kd=kgain(3);
	kpp=-kgain(4);
	trjv = [0 diff(trj)];
	ei=0;
	pidt=0.001;
	epid=0;
	for i=1:n-1
		if mod(dt*i,pidt)==0
			ep = trj(1,i)-x(1,i);
			ei = .99*ei + ep;
			ed=trjv(i)-x(2,i);
			epp = 1e-5 * (x(3,i)-x(4,i));
			epid = kp*ep + ki*ei  + kd*ed + kpp*epp;
		end
		u(1,i) = +epid;
		u(2,i) = -epid;
		u(3,i) = -epid;
		u(4,i) = +epid;
		%obey to limit
		u(:,i) = max(uL, u(:,i));
		u(:,i) = min(uH, u(:,i));

		% simulate one step
		use_ode = 0;
		if use_ode
			odeopt=odeset('RelTol',1e-2);
			[t y]=ode45( @(t_, x_) fnDyn(x_, u(:,i)), [0 dt], x(:,i)', odeopt);
			x(:,i+1)=y(end,:)';
		else
			x(:,i+1) = x(:,i) + dt*fnDyn(x(:,i), u(:,i));
		end
	end
	if min(x(1,:))<0 || max(x(1,:))>xL
		warning('hitlimit in pid');
		cost=cost+1e4;
	else
		cost=cost+sum( (x(1,:)-trj).^2);
	end
end

function [xdot, xdot_x, xdot_u hitThisTime] = dyn(x, u, xL)
	persistent isHitlimit
	hitThisTime=0;
	if any(u<-1e-4 | u>10)
		keyboard
	end
	bA = 0.02^2; %bodeArea^2; 2cm x 2cm
	M = .1; % .2kg
    vA=5*0.01^2; %maximal oriface area
	fric =-200; %friction N/(m/s)
	xdot=[x(2);
		  inv(M)*bA*(x(3)-x(4)) + fric*x(2); %Mi*BodeArea*(p1-p2)
		  %dP(x(3), u(1:2), x(1), x(2), bA);
		  %dP(x(4), u(3:4), 10-x(1), -x(2), bA);
		  dPressure(x(3),vA*u(1:2), 0.01+x(1), x(2), bA);
		  dPressure(x(4),vA*u(3:4), 0.11-x(1), -x(2), bA);
		  ];
	
	if x(1)<xL(1) || x(1)>xL(2) 
		hitThisTime=1;
		if isempty(isHitlimit)
			warning('hit limit');
			isHitlimit=1;
		end
	end
	if nargout>1
		EPS=1e-5;
		%4x4
		%xdot_x = [0 1 0 0;
		%		  0 0 0 0;
		%		  0 0 0 0;
		%		  0 0 0 0];
		xdot_x = finiteDiff(@(z) dyn(z,u, xL), x, EPS);
		%4x2
		%xdot_u = [0 0;
		%		  1/M -1/M;
		%		  0 0;
		%		  0 0];
		xdot_u = finiteDiff(@(z) dyn(x,z, xL), u, EPS);
	end
end

%poor tingfan's valve model
function dp=dP(p, u, x, xdot, bA)
	pL=1e5; %compressed air (pascal)
	pH=6e5; %compressed air (pascal)
    vA=0.01^2; %maximal oriface area
	p = max(min(pH,p),pL);
	dp=1000000*vA*((pH-p)*u(1) + (pL-p)*u(2));
end

function [l, l_x, l_xx, l_u, l_uu, l_ux] = cost(x, u, t, trj, xL)
	wp=1e6;
	wl=1e3; %physical boundary constraints
	isLow=x(1)<xL(1);
	isHigh=x(1)>xL(2);
	if isnan(t)
		l= wp*(x(1)-trj(1,end))'*(x(1)-trj(1,end)) ...
			+ wl*(isLow*(x(1)-xL(1))^2 + isHigh*(x(1)-xL(2))^2);

		%4x1
		l_x= 2*wp*[(x(1)-trj(1,end));0;0;0]; 
		l_x(1) = l_x(1) + 2*wl*(isLow*(x(1)-xL(1))+isHigh*(x(1)-xL(2)));

		%4x4
		l_xx=diag([2*wp 0 0 0]);
		l_xx(1,1) = l_xx(1,1) + 2*wl*(isLow+isHigh);

		%2x1
		l_u = zeros(4,1);

		%2x2
		l_uu =zeros(4,4);

		%2x4
		l_ux = zeros(4,4);
	else
		%scalar
		l=u'*u + wp*(x(1)-trj(1,t))'*(x(1)-trj(1,t)) ...
			+ wl*(isLow*(x(1)-xL(1))^2 + isHigh*(x(1)-xL(2))^2);
		%4x1
		l_x= 2*wp*[(x(1)-trj(1,t));0;0;0];
		l_x(1) = l_x(1) + 2*wl*(isLow*(x(1)-xL(1))+isHigh*(x(1)-xL(2)));

		%4x4
		l_xx=diag([2*wp 0 0 0]);
		l_xx(1,1) = l_xx(1,1) + 2*wl*(isLow+isHigh);

		%2x1
		l_u = 2*u;

		%2x2
		l_uu = 2*eye(4);

		%2x4
		l_ux = zeros(4,4);
	end
end

function g=finiteDiff(fn, z, EPS)
	g=zeros(length(z),length(z));
	for i=1:length(z)
		z1=z; z1(i)=z1(i)+EPS;
		z2=z; z2(i)=z2(i)-EPS;
		g(:,i)=(fn(z1)-fn(z2))/2/EPS;
	end
end
