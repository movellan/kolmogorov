function test_ilqg()
	clear all
	figure(2);cla
	dt=0.001;
	nStep=500;
	xL=10;
	trj = 2+.5*floor((1:nStep)/200)/(nStep/xL/200);
	X0 = [trj(1); 0];
	costt = @(x,u,t) cost(x,u,t, trj, [2 xL-2]);
	dynl = @(x,u) dyn(x,u, [0 xL]);
	uL=[0 0]';
	uH=[1 1]';
	%obtain p- trj
	[xpid_ upid_ ] = pid(dynl, trj, dt, nStep, X0, uL, uH);
	if 0
		U0=[0 0]';rand(2,1);%[.1 .1]';
	else
		U0=upid_;
	end
	%use pid trajectory to initialize ILQG
	[x_ u_ L_ cost_]=ilqg_det(dynl, costt, dt, nStep, X0, U0, uL, uH);
	cost_
	if ~isnan(cost_)
		figure(1)
		clf
		subplot(3,1,1)
		plot([x_(1,:)' xpid_(1,:)' trj']);
		legend('iLQG','PID','desired','Location','EastOutside');
		subplot(3,1,2)
		plot([x_(2,:)' xpid_(2,:)']);
		legend('iLQG','PID','Location','EastOutside');
		subplot(3,1,3)
		plot([u_' upid_'+2]);
		legend('iLQG+','iLQG-','PID+','PID-','Location','EastOutside');
	end
end

function [x u]= pid(fnDyn, trj, dt, n, X0, uL, uH)
	u=zeros(length(uL), n-1);
	x=zeros(length(X0), n);
	x(:,1)=X0;
	kp=0.05;
	for i=1:n-1
		e = trj(1,i)-x(1,i);
		u(1,i) = +kp*e;
		u(2,i) = -kp*e;
		u(:,i) = max(uL, u(:,i));
		u(:,i) = min(uH, u(:,i));
		x(:,i+1) = x(:,i) + dt*fnDyn(x(:,i), u(:,i));
	end
end

function [xdot, xdot_x, xdot_u hitThisTime] = dyn(x, u, xL)
	persistent isHitlimit
	hitThisTime=0;
	if any(u<0 | u>10)
		keyboard
	end
	M = .001;
	xdot=[x(2)  M\(u(1)-u(2))]';
	
	if x(1)<xL(1) || x(1)>xL(2) 
		hitThisTime=1;
		if isempty(isHitlimit)
			warning('hit limit');
			isHitlimit=1;
		end
	end
	if nargout>1
		xdot_x = [0 1;0 0];
		xdot_u = [0 0;1/M -1/M];
	end
end

function [l, l_x, l_xx, l_u, l_uu, l_ux] = cost(x, u, t, trj, xL)
	% t : 1..399 NaN
	wp=1e3;
	wl=1e4; %non-physical boundary constraints
	isLow=x(1)<xL(1);
	isHigh=x(1)>xL(2);
	if isnan(t)
		l= wp*(x(1)-trj(1,end))'*(x(1)-trj(1,end)) ...
			+ wl*(isLow*(x(1)-xL(1))^6 + isHigh*(x(1)-xL(2))^6);
		l_x= 2*wp*[(x(1)-trj(1,end));0]; 
		l_x(1) = l_x(1) + 6*wl*(isLow*(x(1)-xL(1))^5+isHigh*(x(1)-xL(2))^5);
		l_xx=2*wp*[1 0;0 0];
		l_xx(1,1) = l_xx(1,1) + 30*wl*(isLow*(x(1)-xL(1))^4+isHigh*(x(1)-xL(2))^4);
		l_u = zeros(2,1);
		l_uu =zeros(2,2);
		l_ux = zeros(2,2);
	else
		l=u'*u + wp*(x(1)-trj(1,t))'*(x(1)-trj(1,t)) ...
			+ wl*(isLow*(x(1)-xL(1))^6 + isHigh*(x(1)-xL(2))^6);
		l_x= 2*wp*[(x(1)-trj(1,t));0];
		l_x(1) = l_x(1) + 6*wl*(isLow*(x(1)-xL(1))^5+isHigh*(x(1)-xL(2))^5);
		l_xx=2*wp*[1 0;0 0];
		l_xx(1,1) = l_xx(1,1) + 30*wl*(isLow*(x(1)-xL(1))^4+isHigh*(x(1)-xL(2))^4);
		l_u = 2*u;
		l_uu = 2*eye(2);
		l_ux = zeros(2,2);
	end
end

function g=finiteDiff(fn, z, EPS)
	g=zeros(length(z),length(z));
	for i=1:length(z)
		z1=z; z1(i)=z1(i)+EPS;
		z2=z; z2(i)=z2(i)-EPS;
		g(:,i)=(fn(z1)-fn(z2))/2/EPS;
	end
end
