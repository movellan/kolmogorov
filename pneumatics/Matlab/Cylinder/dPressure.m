% p - current pressure
% u = [uCmpu Atm]; Valve area
% xDot - current extending velocity
% x - currentExtension
% 
% Return
% dp = change in pressure
function dp=dPressure(p, u, x, xDot, bodeArea);
	global param

	%in-flow-rate
	mAlpha = param.k*param.R*param.T ./ bodeArea;
	dotM = thinPort(param.pCompressor, p,u(1)) - thinPort(p, param.pATM, u(2));
	dp=mAlpha*dotM/x - param.k*p*xDot/x;
