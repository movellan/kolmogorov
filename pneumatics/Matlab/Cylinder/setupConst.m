function param=setupConst()
global param alpha beta theta kk
PaMax = 90; % Max Pressure in chamber A (Psi)
Pb = 14.5; % Pressure in chamber B ( Psi)
Rp = 1/16; % Radius of port (hole) in inches 
T = 70; % Temperature in Farenheit
%%%%%%% Internal Parameters %%%%%%%%%%%
PaMax =  600000; %Convert Psi to Pascals 
Pb =  Pb*6894.76; %Convert Psi to Pascals 
Rp = Rp*0.0254; % Convert inches to meter
T= 5/9 * (T - 32) + 273 ;% Convert Farenheit to Kelvin

M = 0.029; % air molecular mass (Kg/mol)
rho = 1.2; % density of air (Kg/cubic meter)
R = 8.314472; % Universal gas law constant in (Pa m3)/ (mol Kelvin)

c= 0.72 ; % Discharge coefficient  (dimensionless)
kk = 1.4 ;% specific heat ratio of air (dimmensionless)
Z = 0.9987; % air compressibility factor at p= 5 bars and T=300K, dimensionless

alpha = c *sqrt(2*M*kk/(Z*R*T*(kk-1)));
beta = c *sqrt( kk*M/(Z*R*T) *((2/(kk+1))^((kk+1)/(kk-1))));
theta = ((kk+1)/2)^(kk/(kk-1));

param.pCompressor = 600000; %pascal
param.pATM = 100000; %pascal
param.k = kk; 
param.R=R;
param.T=T;
