function test_ilqg()
	clear all
	figure(2);cla
	dt=0.001;
	nStep=1000;
	xL=.1;
	trj = .02+.05*floor((1:nStep)/200)/(nStep/200);
    p=setupConst;
	X0 = [trj(1); 0;p.pATM;p.pATM];
	costt = @(x,u,t) cost(x,u,t, trj, [0 xL]);
	dynl = @(x,u) dyn(x,u, [0 xL]);
	uL=[0 0 0 0]';
	uH=[1 1 1 1]';
	%obtain p- trj
	[xpid_ upid_ ] = pid(dynl, trj, dt, nStep, X0, uL, uH);
	if 0
		U0=[0 0]';rand(2,1);%[.1 .1]';
	else
		U0=upid_;
	end
	%use pid trajectory to initialize ILQG
    if 1
        [x_ u_ L_ cost_]=ilqg_det(dynl, costt, dt, nStep, X0, U0, uL, uH);
    else
        x_=xpid_;
        u_=upid_;
        cost_ = -1;
    end
	cost_
	if ~isnan(cost_)
		figure(1)
		clf
		subplot(3,1,1)
		plot([x_(1,:)' trj']);
        title('trj')
		legend('x','desired','Location','West');
		subplot(3,1,2)
		plot([x_(3:4,:)']);
        legend('p1','p2','Location','West');
        title('pressure');
		subplot(3,1,3)
		%plot([u_' upid_'+2]);
        plot((repmat((0:3)',1,size(u_,2))+u_)')
        title('control');
	end
end

function [x u]= pid(fnDyn, trj, dt, n, X0, uL, uH)
	u=zeros(length(uL), n-1);
	x=zeros(length(X0), n);
	x(:,1)=X0;
	kp=.1;
	for i=1:n-1
		e = kp*(trj(1,i)-x(1,i))
		u(1,i) = e;
        u(2,i) = -e;
		u(3,i) = -e;
        u(4,i) = e;
        
        %obey to limit
		u(:,i) = max(uL, u(:,i));
		u(:,i) = min(uH, u(:,i));
		x(:,i+1) = x(:,i) + dt*fnDyn(x(:,i), u(:,i));
	end
end

function [xdot, xdot_x, xdot_u hitThisTime] = dyn(x, u, xL)
	persistent isHitlimit
	hitThisTime=0;
	if any(u<-1e-4 | u>1)
		keyboard
	end
	bA = 0.02^2; %bodeArea^2; 
	M = .005;
    vA = 1e-4;
	x(1)
    if x(1)<=xL(1) || x(1)>=xL(2)
        x(1)=min(max(xL(1),x(1)),xL(2));
        x(2)=0;
		hitThisTime=1;
		if isempty(isHitlimit)
			warning('hit limit');
			isHitlimit=1;
		end
	end

    
    xdot=[x(2);
		  inv(M)*bA*(x(3)-x(4)); %Mi*BodeArea*(p1-p2)
		  dPressure(x(3), vA*u(1:2), x(1), x(2), bA);
		  dPressure(x(4), vA*u(3:4), x(1), x(2), bA);
		  ];
	
	if nargout>1
		EPS=1e-8;
		%4x4    
		%xdot_x = [0 1 0 0;
		%		  0 0 0 0;
		%		  0 0 0 0;
		%		  0 0 0 0];
		xdot_x = finiteDiff(@(z) dyn(z,u, xL), x, EPS);
		%4x2
		%xdot_u = [0 0;
		%		  1/M -1/M;
		%		  0 0;
		%		  0 0];
		xdot_u = finiteDiff(@(z) dyn(x,z, xL), u, EPS);
	end
end

function dp=dP(p, u, x, xdot, bA)
	pL=1e5;
	pH=6e5;
    vA=100;
	p = max(min(pH,p),pL);
	dp=vA*((pH-p)*u(1) + (pL-p)*u(2));
end

function [l, l_x, l_xx, l_u, l_uu, l_ux] = cost(x, u, t, trj, xL)
	% t : 1..399 NaN
	wp=1e3;
	wl=1e4; %non-physical boundary constraints
	isLow=x(1)<xL(1);
	isHigh=x(1)>xL(2);
	if isnan(t)
		l= wp*(x(1)-trj(1,end))'*(x(1)-trj(1,end)) ...
			+ wl*(isLow*(x(1)-xL(1))^2 + isHigh*(x(1)-xL(2))^2);

		%4x1
		l_x= 2*wp*[(x(1)-trj(1,end));0;0;0]; 
		l_x(1) = l_x(1) + 2*wl*(isLow*(x(1)-xL(1))+isHigh*(x(1)-xL(2)));

		%4x4
		l_xx=diag([2*wp 0 0 0]);
		l_xx(1,1) = l_xx(1,1) + 2*wl*(isLow+isHigh);

		%2x1
		l_u = zeros(4,1);

		%2x2
		l_uu =zeros(4,4);

		%2x4
		l_ux = zeros(4,4);
	else
		%scalar
		l=u'*u + wp*(x(1)-trj(1,t))'*(x(1)-trj(1,t)) ...
			+ wl*(isLow*(x(1)-xL(1))^2 + isHigh*(x(1)-xL(2))^2);
		%4x1
		l_x= 2*wp*[(x(1)-trj(1,t));0;0;0];
		l_x(1) = l_x(1) + 2*wl*(isLow*(x(1)-xL(1))+isHigh*(x(1)-xL(2)));

		%4x4
		l_xx=diag([2*wp 0 0 0]);
		l_xx(1,1) = l_xx(1,1) + 2*wl*(isLow+isHigh);

		%2x1
		l_u = 2*u;

		%2x2
		l_uu = 2*eye(4);

		%2x4
		l_ux = zeros(4,4);
	end
end

function g=finiteDiff(fn, z, EPS)
	g=zeros(length(z),length(z));
	for i=1:length(z)
		z1=z; z1(i)=z1(i)+EPS;
		z2=z; z2(i)=z2(i)-EPS;
		g(:,i)=(fn(z1)-fn(z2))/2/EPS;
	end
end
