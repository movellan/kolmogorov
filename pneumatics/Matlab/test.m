clear
m= vrworld('cylinder.wrl')
open(m)
view(m)
node =get(m,'Nodes');
x =0;
dt = 0.05;
f = 1; % freq in hertz

t=0;
while(1<2)
  t=t+dt;
  x =  1.25+ 2*(1+sin(2*pi*f*t))/2;
  
  
  m.OB_BoreRod.translation=[0 x  0]

  pause(dt);
end
