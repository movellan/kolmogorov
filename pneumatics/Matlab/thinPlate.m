% Simulate thin Plate Port Model
% We input US units and convert into metric Kg meter sec units
% then report back flow in US standard Cubic Feet per Minute
clear
clf

global alpha beta theta k 
PaMax = 90; % Max Pressure in chamber A (Psi)
Pb = 14.5; % Pressure in chamber B ( Psi)
Rp = 1/16; % Radius of port (hole) in inches 
T = 70; % Temperature in Farenheit


%%%%%%% Internal Parameters %%%%%%%%%%%


PaMax =  PaMax*6894.76; %Convert Psi to Pascals 
Pb =  Pb*6894.76; %Convert Psi to Pascals 
Rp = Rp*0.0254; % Convert inches to meter
T= 5/9 * (T - 32) + 273 ;% Convert Farenheit to Kelvin

 M = 0.029; % air molecular mass (Kg/mol)
rho = 1.2; % density of air (Kg/cubic meter)
R = 8.314472; % Universal gas law constant in (Pa m3)/ (mol Kelvin)

c= 0.72 ; % Discharge coefficient  (dimensionless)
k = 1.4 ;% specific heat ratio of air (dimmensionless)
Z = 0.9987; % air compressibility factor at p= 5 bars and T=300K, dimensionless

alpha = c *sqrt(2*M*k/(Z*R*T*(k-1)));
beta = c *sqrt( k*M/(Z*R*T) *((2/(k+1))^((k+1)/(k-1))));
theta = ((k+1)/2)^(k/(k-1));

for j=1:4
  radius = Rp*j/4 ;
  radius*2000
  for i=0:1:100
    Pa(i+1) = PaMax*i/100; % Vary supply pressure to see effect
                                    % on flow
    
    dotM(i+1) = thinPort(Pa(i+1), Pb, radius); 
  end
  dotM =dotM/rho; % Convert from Kg/sec to m^3/sec
  dotM = dotM* (3.28^3)*60; % Convert from m^3/sec to  cubic feet per minute
  Pa = Pa./6894.76; %Convert  Pascals to Psi  
  plot(Pa, dotM,'LineWidth',2)
  hold on
end

xlabel('Pressure in Chamber A (PSI)','FontSize',18);
ylabel('Air Flow  (CFPM)','FontSize',18);
%title('Thin Plate Port Model')
legend('0.8 mm', '1.6 mm', '2.4 mm', '3.2 mm')
set(gca,'FontSize',18);