nPoints=300; % number of points in graph

graphX = 1:nPoints;
graphX = graphX*graphDt;

nGraphs=7;

for i=1:nGraphs
  graphY{i} = inf*ones(size(graphX));
  subplot(nGraphs,1,i)
  lh{i}=line(graphX,graphY{i},...
           'marker','.',...
           'markersize',5);
end

subplot(nGraphs,1,1);
lb{1} = line([0 0 ], [0 l ]);
ylabel('Stroke (Inches)');


 subplot(nGraphs,1,2);
 lb{2} = line([0 0 ], [eP sP])
 ylabel('Pressure Chamber 1 PSI');;
 subplot(nGraphs,1,3);
 lb{3} = line([0 0 ], [eP sP]);
 ylabel('Pressure Chamber 2 PSI');;
 subplot(nGraphs,1,4);
 ylabel('Supply Port 1');;
 lb{4} = line([0 0 ], [0 1]);
 subplot(nGraphs,1,5);
 ylabel(' Exhaust Port 1 ');;
 lb{5} = line([0 0 ], [0 1]);
 subplot(nGraphs,1,6);
 ylabel('Supply Port 2');;
 lb{6} = line([0 0 ], [0 1]);
 subplot(nGraphs,1,7);
 ylabel(' Exhaust Port 2 ');;
 lb{7} = line([0 0 ], [0 1]);


xlabel('Time (Seconds)')



% virtual reality cylinder
vRModel= vrworld('cylinder.wrl')
open(vRModel)
view(vRModel)
vRModel.OB_BoreRod.translation=[0 1.25+x*39.3701  0]; 





