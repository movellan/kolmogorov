% Get graphs for the f_a and f_c functions. They discrive flow into
% a chamber with pressure Pchamber from the atmosphere port and
% from the compressor port
% We input US units and convert into metric Kg meter sec units
% then report back flow in US standard Cubic Feet per Minute
clear
clf

global alpha beta theta k 
pCompressor = 90; % Max Pressure in chamber A (Psi)
PAtmosphere = 14.5; % Pressure in chamber B ( Psi ). Atmospheric Pressure


T = 70; % Temperature in Farenheit


%%%%%%% Internal Parameters %%%%%%%%%%%


pCompressor =  pCompressor*6894.76; %Convert Psi to Pascals 
pAtmosphere =  PAtmosphere*6894.76; %Convert Psi to Pascals 


T= 5/9 * (T - 32) + 273 ;% Convert Farenheit to Kelvin

M = 0.029; % air molecular mass (Kg/mol)
rho = 1.2; % density of air (Kg/cubic meter)
R = 8.314472; % Universal gas law constant in (Pa m3)/ (mol Kelvin)

c= 0.72 ; % Discharge coefficient  (dimensionless)
k = 1.4 ;% specific heat ratio of air (dimmensionless)
Z = 0.9987; % air compressibility factor at p= 5 bars and T=300K, dimensionless

alpha = c *sqrt(2*M*k/(Z*R*T*(k-1)));
beta = c *sqrt( k*M/(Z*R*T) *((2/(k+1))^((k+1)/(k-1))));
theta = ((k+1)/2)^(k/(k-1));

radiusa=[0.5,1,2]/1000
for j=1:3

  radiusc = 1/1000;
  for i=0:1:100
    PChamber(i+1) = PAtmosphere+(pCompressor-PAtmosphere)*i/100; 
    % Vary supply pressure to see effect
    % on flow
    dotMa(i+1) = thinPort(PChamber(i+1), pAtmosphere,radiusa(j)); 
    dotMc(i+1) = thinPort(pCompressor,PChamber(i+1), radiusc); 
  end
  dotMa =dotMa/rho; % Convert from Kg/sec to m^3/sec
  dotMc = dotMc/rho;
  dotMa = dotMa* (3.28^3)*60; % Convert from m^3/sec to  cubic feet
                              % per minute
  dotMc = dotMc* (3.28^3)*60; % Convert from m^3/sec to  cubic feet per minute
  PChamber = PChamber./6894.76; %Convert  Pascals to Psi  
  plot(PChamber, dotMa,'LineWidth',1)
  
  hold on
 
  plot(PChamber, dotMc,'r','LineWidth',1)
  
 
end
xlim([14.5 90])
xlabel('Pressure in Chamber','FontSize',14);
ylabel('Air Flow  (CFPM)','FontSize',14);
%legend('0.5','1','2')
%title('Thin Plate Port Model')
