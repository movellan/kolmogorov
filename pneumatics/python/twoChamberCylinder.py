from numpy import *
from matplotlib.pyplot import *
from chamber import *

class Piston():
	def __init__(self):
		self.c1=Chamber()
		self.c2=Chamber()
		
		self.mass=0.5 # in Kg
		self.viscosity=17000.
		self.dt =0.0001 # in secs
		self.length=0.04 # cylinder length in meters
		self.radius =0.01 # cylinder radius  meters
		self.area = pi*self.radius**2.
		self.minX=0.01*self.length
		self.maxX= 0.99*self.length
		self.x=(self.minX+self.maxX)/2. #  bore location.  for x =0 zero chamber1 volume is minimum
		self.dx =0. # rod velocity
		self.ddx =0. # rod acceleration
		self.g = 9.8 # gravity m/sec^2
		
		self.c1.dt = self.dt
		self.c2.dt=self.dt
		self.t =0 # time in seconds
		v= self.x*self.area
		self.c1.setVolume(v)
		v= (self.length-self.x)*self.area
		self.c2.setVolume(v)

		# these may be useful 
		self.dotx =0.
		self.dotdotx =0.

	def update(self,v1,v2):
		#v1 v2 are volts sent to valves for chamber 1 and 2. 
		self.t = self.t + self.dt
		# integrate chamber pressures
		self.c1.update(v1)
		self.c2.update(v2)

		f1= self.c1.p * self.area
		f2=self.c2.p * self.area

		# experimental simulation of contact forces
		contactForce=0.
		
		nonContactForce= f1-f2- self.mass*self.g - self.dx*self.viscosity 
		

		ddx = (nonContactForce+contactForce)/self.mass 
		
		self.x = self.x + self.dt*self.dx +0.5*(self.dt**2.)*ddx

		self.dx = self.dx + self.dt*ddx
		


		if self.x >self.maxX:
			self.x=self.maxX
			self.dx =0. 
		if self.x <self.minX:
			self.x=self.minX
			self.dx =0.

		self.c1.length= self.x
		self.c2.length = self.length-self.x
		self.c1.dx=self.dx
		self.c2.dx = -self.dx


		self.setX(self.x)	
		
	def update2(self,v1,v2): # experimental, to be deprecated
		# implements the third order equation
		#v1 v2 are volts sent to valves for chamber 1 and 2. 
		self.t = self.t + self.dt
		self.c1.update(v1)
		self.c2.update(v2)

		# gather current states
		uc1= self.c1.valve.cPort.u
		ua1 = self.c1.valve.aPort.u
		uc2= self.c2.valve.cPort.u
		ua2 = self.c2.valve.aPort.u
		# print 'uc1',uc1
		# print 'uc2',uc2
		# print 'ua1',ua1
		# print 'ua2',ua2
		x = self.x
		
		p1 = self.c1.p
		p2 = self.c2.p
		m1 = self.c1.m
		m2 = self.c2.m
		# print'x', x*100.
		# print 'dotx',self.dotx*100.
		# print 'dotdotx',self.dotdotx*100.
		# print 'p1',pascalToPSI(p1)
		# print 'p2',pascalToPSI(p2)
		# print 'm1',m1
		# print 'm2',m2
		# gather intervening variables
		inFlux1 = self.c1.valve.cPort.inFlux(p1)
		inFlux2 = self.c2.valve.cPort.inFlux(p2)
		outFlux1 = self.c1.valve.cPort.outFlux(p1)
		outFlux2 = self.c2.valve.cPort.outFlux(p2)
		print 'inFlux1',inFlux1
		print 'inFlux2',inFlux2
		print 'outFlux1',outFlux1
		print 'outFlux2',outFlux2

		inFlow1 = uc1*inFlux1
		inFlow2 = uc2*inFlux2
		outFlow1 = ua1*outFlux1
		outFlow2=ua2*outFlux2
		# print 'inFlow1',inFlow1
		# print 'inFlow2',inFlow2
		# print 'outFlow1',outFlow1
		# print 'outFlow2',outFlow2

		# gather constants
		n = self.c1.n
		Rs = self.c1.Rs
		T = self.c1.T
		a = self.area
		v = x *a
		dotv1 = self.dotx *self.area 
		dotv2 = -self.dotx *self.area 
		# print 'n',n
		# print 'Rs',Rs
		# print 'T',T
		# print 'a',a
		# print 'v',v
		# print 'dotv1',dotv1
		# print 'dotv2',dotv2

		# get the state transitions

		dx = self.dotx *self.dt
		ddotx = self.dotdotx *self.dt
		dm1 = (inFlow1 - outFlow1)*self.dt
		dm2 = (inFlow2 - outFlow2)*self.dt
		dp1 =(n *(Rs*T * dm1 - p1*dotv1)/v)*self.dt
		dp2 = (n*(Rs*T *dm2 - p2*dotv2)/v)*self.dt
		dFriction = (-self.viscosity*self.dotx)**self.dt
		# note we need to initialize grav forces to non-zero
		dGravity=0
		df = (a*(dp1 - dp2) +dFriction+dGravity)*self.dt

		ddotdotx = df/self.mass


		# print 'dx',dx
		# print 'ddx',ddotx
		# print 'dm1', dm1
		# print 'dm2', dm2
		# print 'dp1', dp1
		# print 'dp2', dp2
		
		# update states
		self.x = self.x + dx
		self.dotx = self.dotx + ddotx
		self.dotdotx = self.dotdotx + ddotdotx
		self.p1 = p1+dp1
		self.p2 = p2+dp2
		self.m1 = m1+dm1
		self.m2 = m2+dm2

		# print 'x',self.x
		# print 'dotx',self.dotx
		# print 'dotdotx',self.dotdotx
		# print 'p1',self.p1
		# print 'p2',self.p2
		# print 'm1',self.m1
		# print 'm2',self.m2

		
		
	def setX(self,x):
		# x meters
		if x >self.maxX:
			x=self.maxX
		if x <self.minX:
			x=self.minX
		self.x =x
		v= self.x*self.area
		self.c1.setVolume(v)
		v= (self.length-self.x)*self.area
		self.c2.setVolume(v)
	
	def setRadius(self,r):
		# in meters
		self.area = pi*r**2.
		v = self.x*self.area
		self.c1.setVolume(v)
		v= (self.length-self.x)*self.area
		self.c2.setVolume(v)


	def PControler(self,tpd):
		# proportional controller to achieve a target pressure difference
		# tpd in pascal
		k=0.001	 	
		pd = self.c1.p -self.c2.p
		v1=5.0 +k*(tpd-pd)
		v2=5.0-k*(tpd-pd)
		if v1 <0.:
			v1=0.
		if v1>10.:
			v1=10.
		if v2 <0.:
			v2=0.
		if v2>10.:
			v2=10.
		return [v1,v2]

