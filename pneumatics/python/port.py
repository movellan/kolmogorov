from numpy import *


# simulates pneumatic chamber with compressor and atmoshperic ports

class Port(): 	
	def __init__(self):
		self.qa = 14.5 # atmoshperic pressure in psi
		self.qc = 100. # compressor pressure in psi
		self.qa = psiToPascal(self.qa)
		self.qc= psiToPascal(self.qc)
		self.T =70.
		self.T= farenheitToKelvin(self.T)
		self.Rs= 287.0574 # gas constant for air Jules/(Kg, Kelvin Degrees)	
		self.k = 1.4 # specific heat of air
		self.Z=0.99 # air  compresibility factor
		self.c=0.72 #  air discharge coefficient
		self.n =self.k# index for polytropic temperature model
		self.alpha = self.c*sqrt(2.*self.k/(self.Z*self.Rs*self.T*(self.k-1.)))
		self.beta= (2./(self.k+1.))**((self.k+1.)/(self.k-1.))
		self.beta = self.c*sqrt(self.k/(self.Z*self.Rs*self.T)*self.beta)	
		self.theta =  ((self.k+1.)/2.)**(self.k/(self.k-1.))
		self.u = 0 # port width
	def thinPort(self,p1,p2):
		# p1 is upstream pressure in Pascals 
		#  p2 downstream pressure in Pascals
		# we assume p1 > p2		
		if p1 <= self.theta*p2:
			h= (p2/p1)**(2./self.k) - (p2/p1)**((self.k+1.)/self.k)
			h = self.alpha*p1*sqrt(h)
		else:
			h = self.beta*p1
		return h
	def inFlux(self,p):
		# p is chamber pressure in pascals
		# output flow per port unit area from compressor to chamber
		# in Kg/(meter^2 sec )	
		return(self.thinPort(self.qc,p))
	def outFlux(self,p):
		#p is chamber pressure in pascals
		# output is flow per port unit area  from chamber into atmosphere
		# in Kg/(meter^2 sec )
		return(self.thinPort(p,self.qa))
	def portRatioForTargetPressure(self, tp):
		# pressure in pascals
		if tp >self.qc*0.99999999999999:
			tp = self.qc*0.99999999999999
		if tp <= self.qa:
			return 0.
		return self.outFlux(tp)/self.inFlux(tp)		
	
def psiToPascal(p):
	return 	6894.76 *p
def pascalToPSI(p):
	return 	p/6894.76 
def farenheitToKelvin(T):
	return 5. *(T+459.67) /9. 
def kelvinToFarenheit(T):
	return 9./5.*T -459.67
def ccToCubicMeter(cc):
	return cc/10**6.
def squaremmToSquareMeter(smm):
	return smm/10**6.
def squareMetersToSquareInches(sm):
	return sm*1550.0

