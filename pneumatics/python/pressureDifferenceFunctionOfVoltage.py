from numpy import *
from matplotlib.pyplot import *
from twoChamberCylinder import *


# #############
# # Example: 
# #############
radius = 0.01 # cylinder radius in meters
l =0.04 # length of cylinder in meters
x0=l/2 # initial position of bore

p = Piston()
p.setRadius(radius)
p.setX(x0)
p.mass=1.0 #Kg

# let's find pressure needed to compensate for gravity
pre = p.mass*9.8/p.area



p1Series=[]
p2Series=[]
dpSeries=[]
m1=-5
m2=5

vSeries=arange(m1,m2,(m2-m1)/100.)

# first lets get the theoretical curve for target pressure as a function of voltage
for v in vSeries:
	print v
	v1= 5+v
	v2=5-v

	p1 = p.c1.valve.targetPressureForVoltage(v1)
	p2 = p.c2.valve.targetPressureForVoltage(v2)
	dp = p1-p2
	p1=pascalToPSI(p1)
	p2=pascalToPSI(p2)
	dp=pascalToPSI(dp)
	p1Series.append(p1)
	p2Series.append(p2)
	dpSeries.append(dp)
subplot(2,1,2)
plot(vSeries,dpSeries)
xlabel('Voltage',fontsize=16)
ylabel('Pressure Difference',fontsize=16)
subplot(2,1,1)
plot(vSeries,p1Series,label='Chamber 1')
plot(vSeries,p2Series,label='Chamber 2')

ylabel('Pressure',fontsize=16)
legend(loc=1	)
show()

# lets simulate now

p1bSeries=[]
p2bSeries=[]
dpbSeries=[]
v1Series=[]
v2Series=[]
tSeries=[]
xSeries=[]
trialTime = 1.
endTime=trialTime
p.setX(0.)
k=0
for v in vSeries:
	k=k+1
	print k

	v1= 5+v
	v2=5-v
	
	while p.t < endTime:
		print p.t
		p.update(v1,v2)
		tSeries.append(p.t)

	p1 = p.c1.valve.targetPressureForVoltage(v1)
	p2 = p.c2.valve.targetPressureForVoltage(v2)
	dp = p1-p2
	p1=pascalToPSI(p1)
	p2=pascalToPSI(p2)
	dp=pascalToPSI(dp)
	p1bSeries.append(p1)
	p2bSeries.append(p2)
	dpbSeries.append(dp)
	xSeries.append(100.*p.x)
		 

	endTime=endTime+trialTime
	

