from numpy import *
from matplotlib.pyplot import *

from valve import *


# simulates pneumatic chamber with compressor and atmoshperic ports

class Chamber:
	def __init__(self):
		# default parameters
		self.dt = 0.01 # time interval for discrete time simulation. use twice bandwidth of valve (100Hz)
		self.radius = 0.01#  chamber radius in m
		self.length =0.1 # chamber lenght in m
		self.area = pi*(self.radius)**2.
		self.v = self.area*self.length
		maxPortArea=1. # in square mm
		self.valve = Valve(maxPortArea)
		self.qc = self.valve.cPort.qc # compressor pressure
		self.qa = self.valve.aPort.qa # atmospheric pressure
		# state variables
		
		self.p = self.valve.qa # pressure in pascals. initialize to atmosphere
		self.inFlow =0.
		self.outFlow=0.

		self.n = self.valve.aPort.n
		self.Rs = self.valve.aPort.Rs
		self.T= self.valve.aPort.T

		self.m =  self.p*self.v/(self.Rs*self.T) # initial mass	
		self.t =0 # current time
		self.dx =0. # derivative of the chamber's length
	def setVolume(self,v):
		# v in cubic meters
		self.v =v 
		self.m =  self.p*self.v/(self.Rs*self.T) # initial mass	
	def setMaxPortArea(self,mpa):
		# mpa in square mm
		self.maxPortArea=mpa
		self.valve.maxPortAreaInSquareMm=mpa
		self.valve.portHeight = sqrt(mpa)
		self.valve.gain =self.valve.portHeight/5.0 # mm per volt

	def update(self,V):
		self.valve.setVoltage(V)
		dv = self.area*self.dx
		self.v = self.length*self.area
		inFlux = self.valve.cPort.inFlux(self.p)
		uc = self.valve.cPort.u
		self.inFlow = inFlux*uc
		
		outFlux = self.valve.aPort.outFlux(self.p)
		ua = self.valve.aPort.u
		self.outFlow = outFlux*ua

		mdot= self.inFlow-self.outFlow
		self.m = self.m + self.dt*mdot
		
		
		pdot = self.n*( self.Rs*self.T*mdot -self.p*	dv)/self.v		
		self.p = self.p+self.dt*pdot
	
		if self.p > self.qc:
			self.p = self.qc
		if self.p < self.qa:
			self.p = self.qa
		self.t = self.t+self.dt
	#### 
	def clipV(self,v):
		if v<0.0: 
			v=0.0
		if v>10.0:
			v=10.0
		return v
	def unitTest(self):
		# get a10cc chamber to 90psi using a 1smm port
		c=Chamber()
		c.dt=0.0001
	
		c.setVolume(10./10**6.)
		c.setMaxPortArea(1.) # squared mm
		desiredPressure = 0.9*c.qc
		c.valve.Leakage =0.0 
		V=10. # send a 10 volt signal to open port maximally
		tSeries=[]
		pSeries=[]
		inFlowSeries=[]
		outFlowSeries=[]
		while c.p < desiredPressure:
			tSeries.append(c.t)
			pSeries.append(pascalToPSI(c.p))
			inFlowSeries.append(c.inFlow)
			outFlowSeries.append(c.outFlow)
			c.update(V)
		e = c.t -0.0402 
		print " It took ",c.t, ' seconds to get a 10 cc chamber to 90PSI with a 1 smm port'
		if e >2.*c.dt:
			print 'too long. Test Failed'
		elif e<-2.*c.dt:
			print 'too  short. Test Failed'
		else:
			print 'Test Passed'
		
		print 'chamber volume in cubic cm ',c.v*10**6.
		print 'compressor port in squared mm ',c.valve.cPort.u*10**6.
		print 'atmospheric port in squared mm ',c.valve.aPort.u*10**6.
		print 'tempreature in Farenheit ',kelvinToFarenheit( c.T)
		print 'alpha ',c.valve.aPort.alpha
		print 'beta ',c.valve.aPort.beta
		print 'theta ',c.valve.aPort.theta


def example1():
	# send a voltage signal and see how it affects the 
	# chamber pressure
	c = Chamber()
	simulationTimeInSecs =1000.

	timeSeries=[]
	vSeries=[]
	pSeries=[]
	cPortSeries=[]
	aPortSeries=[]
	tpSeries=[]

	# an interesting phenomenon is intensity dependent bandwith
	# for maxV=5 minv=1 the sinuoid is greatly atenuated
	# for maxV=10, minV=0 it is not
	
	# parameters of the voltage signal sent to the valve
	freq=0.05#Hz
	maxV =5.
	minV=3.


	while c.t <simulationTimeInSecs:
		print c.t ,'of ', simulationTimeInSecs
		timeSeries.append(c.t)
		p = pascalToPSI(c.p)
		pSeries.append(p)
		# sinusoid or square wave
		V= minV+(maxV-minV)*(1. + cos(2*pi*freq*c.t))/2.
		V= minV+(maxV-minV)*(1. + sign(cos(2*pi*freq*c.t)))/2.
		vSeries.append(V)
		cp = c.valve.cPort.u*10**6.
		cPortSeries.append(cp)
		ap=c.valve.aPort.u*10**6.
		aPortSeries.append(ap)
		uc = c.valve.cPort.u
		ua = c.valve.aPort.u
		tp = c.valve.targetPressureForPortAreas(uc,ua)
		tpSeries.append(pascalToPSI(tp))

		c.update(V)
	plot(timeSeries,pSeries,'b')
	plot(timeSeries,tpSeries,'r--')
	show()

	uc = c.valve.cPort.u
	ua = c.valve.aPort.u
	print pascalToPSI(c.valve.targetPressureForPortAreas(uc,ua))
	
def testControllers():

	c1=Chamber()
	c2=Chamber()
	c3=Chamber()
	simulationTimeInSecs =3.

	tSeries=[]
	v1Series=[]
	v2Series=[]
	v3Series=[]
	p1Series=[]
	p2Series=[]
	p3Series=[]
	tpSeries=[]

	freq=1.#Hz
	pMax =psiToPascal(100.)
	pMin=psiToPascal(14.5)


	tp = psiToPascal(50.) # target pressure
	kp2=0.0001 # controller gain
	kp3=0.000005 # differential controller gain
	k=0
	V1=0.
	V2=0.
	V3=0.
	while c1.t <simulationTimeInSecs:
		print c1.t,'of ',simulationTimeInSecs
		tSeries.append(c1.t)
		p1 = pascalToPSI(c1.p)
		p2 = pascalToPSI(c2.p)
		p3 = pascalToPSI(c3.p)

		p1Series.append(p1)
		p2Series.append(p2)
		p3Series.append(p3)

		tp= pMin+(pMax-pMin)*(1. + cos(2*pi*freq*c1.t))/2.
		#tp= pMin+(pMax-pMin)*(1. + sign(cos(2*pi*freq*c1.t)))/2.
		tpSeries.append(pascalToPSI(tp))
		k=k+1
		if k%10==0: # run controller at 10 Hz
			# feedforward control
			# this version would set the volage so that it produces 
			# the desired target pressure. requires precise knowledge
			# of the relationship between voltage and target pressure
			V1= c1.valve.voltageForTargetPressure(tp)
			# feedback control version 1
			V2= 5.0 +kp2*(tp - c2.p)
			# feedback control version 2
			V3=V3+kp3*(tp-c3.p)
			V1=c1.clipV(V1)
			V2=c2.clipV(V2)
			V3=c3.clipV(V3)
		v1Series.append(V1)
		v2Series.append(V2)
		v3Series.append(V3)
		c1.update(V1)
		c2.update(V2)
		c3.update(V3)
	subplot(2,1,1)
	plot(tSeries,tpSeries,'r--',label='Target')
	plot(tSeries,p1Series,'b',label='Control 1')
	plot(tSeries,p2Series,'k',label='Control 2')
	plot(tSeries,p3Series,'g',label='Control 3')
	legend(loc=4)
	ylabel('PSI')
	subplot(2,1,2)
	plot(tSeries,v1Series,'b',label='Control 1')
	plot(tSeries,v2Series,'k',label='Control 2')
	plot(tSeries,v3Series,'g',label='Control 3')
	legend(loc=4)
	xlabel('Secs')
	ylabel('Volts')
	show()



############### old stuff #####################

####################################################
	# def setCompresorPortInSquaremm(self,uc):
	# 	# input in squared mm
	# 	self.uc = uc/10**6. # convert to squared meters
		

	# def setAtmosphericPortInSquaremm(self,ua):
	# # input in squared mm		
	# 	self.ua = ua/10**6. # convert to squared meters
	# def setVolumeInCubicCm(self,v):
	# # input in cubic centimeters
	# 	self.v=v/10**6. # convert to cubic meters

	# def setVolumeInCubicMeters(self,v):
	# # input in cubic meters
	# 	self.v=v # convert to cubic meters
	

	# def getPressureInPascals(self):
	# 	return 	self.p

	# def getTimeInSecs(self):
	# 	return self.t
	# def getPressureInPSI(self):
	# 	return 	pascalToPSI(self.p)
	

	# def setPressureInPSI(self,p):
	# 	p = psiToPascal(p)
	# 	self.p =p
	# def getTimeConstantInSeconds(self,meanAtmospherePortAreaInSquaredMeters):
	# 	# approximate time constant of the controller
	# 	tau=self.v/(self.n*self.Rs*self.T*self.beta*meanAtmospherePortAreaInSquaredMeters)
	# 	return tau
	# def getBandwidthInHertzs(self,meanAtmospherePortAreaInSquaredMeters):
	# 	# approximate time constant of the controller
	# 	tau=self.getTimeConstantInSeconds(meanAtmospherePortAreaInSquaredMeters)
	# 	return (1/(2.*pi*tau))
	# 	return tau

# 	def setRadiusInMeters(self,r):
# 		self.r =r
# 		self.area = 2.*pi*(r**2.)
# 		self.v = self.area*self.length
# 	def setLengthInMeters(self,l):
# 		self.length=l
# 		self.v=self.area*self.length
# 	def portRatioForEquilbriumPressure(self, p):
# 		#finds the port ratio that produces an equilibrium pressure in PSI
# 		# change  p to Pascals
# 		qc =psiToPascal(100.)
# 		qa=psiToPascal(14.5)
# 		# avoid numerical issues
# 		if p>0.99999999*self.qc:
# 			p=0.99999999*self.qc
# 		if p <self.qa:
# 			p=self.qa
# 		return self.outFlux(p)/self.inFlux(p)	
	
# 	def inFlux(self,p):
# 		# p is chamber pressure in pascals
# 		# output flow per port unit area from compressor to chamber
# 		# in Kg/(meter^2 sec )	
# 		return(self.thinPort(self.qc,p))
# 	def outFlux(self,p):
# 		#p is chamber pressure in pascals
# 		# output is flow per port unit area  from chamber into atmosphere
# 		# in Kg/(meter^2 sec )
# 		return(self.thinPort(p,self.qa))
# 	def thinPort(self,p1,p2):
# 		# p1 is upstream pressure in Pascals 
# 		#  p2 downstream pressure in Pascals
# 		# we assume p1 > p2		
# 		if p1 <= self.theta*p2:
# 			h= (p2/p1)**(2./self.k) - (p2/p1)**((self.k+1.)/self.k)
# 			h = self.alpha*p1*sqrt(h)
# 		else:
# 			h = self.beta*p1
# 		return h
	
# 	def setTargetPressureInPascal(self,tp):
# 		# sets ports so that equilibrium pressure equals tp
# 		pr = self.portRatioForEquilbriumPressure(tp)
# 		v=self.valve.voltageForPortRatio(pr)
# 		self.uc,self.ua = self.valve.portAreasForVoltage(v)

# 	def equilbirumPressureForPortAreas(self,uc,ua):
# 			p = (self.qc+self.qa)/2. 
# 			dt = 1.
# 			if (ua ==0. ) & (uc >0.):
# 				return self.qc
# 			if (ua>0. ) & (uc ==0):
# 				return self.qa
# 			if (ua ==0.) & (uc ==0.):
# 				return 'error in equilbirumPressureForPortAreas'
# 			#normalize ports 
# 			s = uc+ua
# 			uc = uc/s
# 			ua = ua/s
# 			for k in range(100):
# 				dp = uc*self.inFlux(p)-ua*self.outFlux(p)
# 				p = p+dt*dp
# 				if p>self.qc:
# 					p=self.qc
# 				if p<self.qa:
# 					p = self.qa
# 			return p


# def psiToPascal(p):
# 	return 	6894.76 *p
# def pascalToPSI(p):
# 	return 	p/6894.76 
# def farenheitToKelvin(T):
# 	return 5./9. *(T-32.) +273 # convert to Kelvin
# def ccToCubicMeter(cc):
# 	return cc/10**6.
# def squaremmToSquareMeter(smm):
# 	return smm/10**6.
# def squareMetersToSquareInches(sm):
# 	return sm*1550.0
# def targetPresure(uc,ua):
# 	# uc, ua, areas of compressor and atmospheric ports in squaremm
# 	# gets the target pressure corresponding to portAreas
# 	p = 1


# #############################
# ## Testing the Valve Model
# ############################
# ucSeries=[]
# uaSeries=[]
# pSeries=[]
# portRatioSeries=[]
# predictedVSeries=[]
# equilibriumTimeSeries=[]
# voltSeries=arange(0.,10.,0.1)
# c= Chamber() # used only for helper functions

# maxPortAreaInSquaredM=1/10**6.
# valve = FestoValve(maxPortAreaInSquaredM)

# for v in voltSeries:
# 	uc,ua = valve.portAreasForVoltage(v)			
# 	ucSeries.append(uc*10**6.)
# 	uaSeries.append(ua*10**6.)
# 	pr = uc/ua
# 	portRatioSeries.append(pr)
# 	predictedV=valve.voltageForPortRatio(pr)
# 	predictedVSeries.append(predictedV)
# 	tp = c.equilbirumPressureForPortAreas(uc,ua)
# 	pSeries.append(pascalToPSI(tp))

# 	eqTime=0.0
# 	# for p0 in [c.qa,c.qc]:
# 	# 	c.p=p0
# 	# 	c.t=0
# 	# 	c.setTargetPressureInPascal(tp)
# 	# 	#print pascalToPSI(c.p,), pascalToPSI(tp) ,'pressures'
# 	# 	while abs(tp-c.p)/(c.qc-c.qa) >0.001:
# 	# 		c.update()
# 	# 		#print pascalToPSI(p0),pascalToPSI(c.p),pascalToPSI(tp),c.t
# 	# 	eqTime=eqTime +c.t/2.0
# 	equilibriumTimeSeries.append(eqTime)
# 	print eqTime






# subplot(2,2,1)
# plot(voltSeries,ucSeries,'b',label='compressor')
# plot(voltSeries,uaSeries,'b--',label='atmosphere')
# ylabel('Port Area square mm')
# xlabel('Volts')
# legend(loc=2)
# subplot(2,2,2)
# plot(voltSeries,portRatioSeries)
# ylabel('Port Ratio (uc/ua)')
# xlabel('Volts')
# subplot(2,2,3)
# plot(voltSeries,pSeries)
# xlabel('Volts')
# ylabel('Target Pressure')
# show()
# subplot(2,2,4)
# plot(voltSeries,equilibriumTimeSeries)
# xlabel('Volts')
# ylabel('Time In Seconds')
# show()




