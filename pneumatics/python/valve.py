from numpy import *
from  matplotlib.pyplot import *
from port import *


#simulate festo  valve used in Diego
class Valve:
	# input is a voltage.
	# states are voltage, compressor port and atmosphere port areas, target pressure
	def __init__(self,maxPortAreaInSquareMm=1.):
		self.maxPortAreaInSquareMm=maxPortAreaInSquareMm
		self.portHeight = sqrt(self.maxPortAreaInSquareMm)
		# gain and offset parameters convert voltage into spool displacement
		self.offset =5 # volts
		self.gain =self.portHeight/5.0 # mm per volt
		# with Leakage, ports do not completey close	
		self.Leakage =0.1# in square mm
		

		self.aPort=Port() # atmospheric port
		self.cPort = Port() # compressor port
		self.qa = self.aPort.qa #atmospheric pressure
		self.qc =self.cPort.qc # compressor
		self.V=5. # signal to valve in volts
		self.x =0 # spool location. 
		self.setVoltage(self.V)
		

	def setVoltage(self,v):
		# given a signal in volts, it sets the valve port areas
		self.V =v
		[cu,au,x]= self.portAreasForVoltage(v)
		self.cPort.u=cu
		self.aPort.u=au
		self.x =x
	


	def targetPressureForPortAreas(self,uc,ua):
		# returns equilibrium pressure achived by having a fixed chamber
		# with compressed port area uc and atmospheric port area ua
		# p in pascals, uc, ua in cubic meters
		qc=self.cPort.qc
		qa=self.aPort.qa
		p = (qc+qa)/2. 
		dt = 10.
		if (ua ==0. ) & (uc >0.):
			return qc
		if (ua>0. ) & (uc ==0):
			return qa
		if (ua ==0.) & (uc ==0.):
			return 'error in targetPressureForPortAreas'
		#normalize ports 
		s = uc+ua
		uc = uc/s
		ua = ua/s
		for k in range(1000):
			dp = uc*self.cPort.inFlux(p)-ua*self.aPort.outFlux(p)
			p = p+dt*dp
			if p>qc:
				p=qc
			if p<qa:
				p = qa
		return p

	def portAreasForVoltage(self,v):
		#v is input signal in volts. Assumed from 0 to 10 V
		# 5 keeps the ports maximally closed
		if v <0.0:  v =0.0
		if v> 10.0: v =10.0
		x = self.gain*(v -self.offset)
		uc = self.Leakage
		ua = self.Leakage
		if x> 0:
			uc = uc+ x*self.portHeight 
		if x <0:
			ua = ua  -x*self.portHeight
		# convert to square meters
		uc = uc/10**6.
		ua = ua/10**6.
		# compressor port area m^2, atmosphere port area m^2, spool displacement in mm
		return uc,ua,x
	def targetPressureForVoltage(self,v):
		#target pressure in pasca
		[uc,ua,x] = self.portAreasForVoltage(v)
		pr = uc/ua
		tp = self.targetPressureForPortAreas(uc,ua)
		return tp
	def voltageForPortRatio(self,pr):
		a = self.gain*self.portHeight
		if pr >1.:
			v = 5+self.Leakage*(pr-1.)/a
		else:
			v = 5 - self.Leakage*(1.-pr)/(a*pr)
		return v
	def portRatioForTargetPressure(self,tp):
		#finds the port ratio that produces an equilibrium pressure in Pascal
		
		maxP = self.targetPressureForVoltage(10.)
		minP = self.targetPressureForVoltage(0.)
		if tp> maxP:
			tp = maxP
		if tp < minP:
			tp = minP
		return self.aPort.outFlux(tp)/self.cPort.inFlux(tp)	
	def voltageForTargetPressure(self,tp):
		# tp in Pascal
		pr = self.portRatioForTargetPressure(tp)
		V=self.voltageForPortRatio(pr)
		
		if V>10.:
			V=10.
		if V< 0.:
			V=0.
		return V


def testValve(): 
	
	ucSeries=[]
	uaSeries=[]
	tpSeries=[]
	portRatioSeries=[]
	predictedVSeries=[]
	voltSeries=arange(0.,10.,0.1)
	xSeries=[]


	maxPortAreaInSquaredMm=1.
	valve = Valve(maxPortAreaInSquaredMm)


	for v in voltSeries:
		valve.setVoltage(v)	
		uc=valve.cPort.u
		ucSeries.append(uc*10**6.)
		ua = valve.aPort.u
		uaSeries.append(ua*10**6.)
		xSeries.append(valve.x)
		pr = uc/ua
		portRatioSeries.append(pr)
		
		
		tp=valve.targetPressureForVoltage(v)
		predictedV=valve.voltageForTargetPressure(tp)
		predictedVSeries.append(predictedV)	

	     	tpSeries.append(pascalToPSI(tp))


	subplot(2,2,1)
	plot(voltSeries,ucSeries,'b',label='Compressor')
	plot(voltSeries,uaSeries,'b--',label='Atmosphere')
	ylabel('Port Area square mm')
	xlabel('Volts')
	legend(loc=2)
	subplot(2,2,2)
	plot(voltSeries,portRatioSeries,'b',label='Obtained')
	plot(predictedVSeries,portRatioSeries,'r--',label='Predicted')
	legend(loc=2)
	ylabel('Port Ratio (uc/ua)')
	xlabel('Volts')
	subplot(2,2,3)
	plot(voltSeries,tpSeries)

	xlabel('Volts')
	ylabel('Target Pressure')
	show( )
	subplot(2,2,4)
	plot(voltSeries,xSeries)
	xlabel('Volts')
	ylabel('Spool Displacement mm')
	show()
