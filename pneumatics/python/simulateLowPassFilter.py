from numpy import *
from  matplotlib.pyplot import *

dt = 0.01

simulationTime=10# in seconds
timeSeries = arange(0,simulationTime,dt)
pSeries=[]
p=0

desiredP=[]

tau=0.1 
bandWidth = 1/(tau*2*pi)

for t in timeSeries:	
	
	freq = bandWidth
	dP= sin(2*pi*freq*t)
	desiredP.append(dP)
	p = p +dt*(dP-p)/tau
	pSeries.append(p)

clf()
plot(timeSeries,desiredP,timeSeries,pSeries)
show()
