#!/usr/bin/python

from pylab import *

def setupThinPlateParams():
    prm = {};
    prm['R'] = 8.314472 #Universal Gas Law constant (Pa m^3)/(mol degreesKelvin)
    prm['M'] = 0.029 # Gas Molecular Mass, Kg/mol.  Value for air
    prm['c'] = 0.72 #Discharge coefficient, dimensionless
    prm['Z'] = .99 #Gas compressibility factor, dimensionless.  Value for air
    prm['k'] = 1.4 #Specific Heat Ratio, dimensionless.  Value for air
    prm['T'] = 295 #Temperature, degreesKelvin.  Value ~70 Fahrenheit
    prm['alpha'] = prm['c']*sqrt( ((2*prm['M'])/(prm['Z']*prm['R']*prm['T']))*(prm['k']/(prm['k']-1)));
    prm['beta'] = prm['c']*sqrt( ((prm['k']*prm['M'])/(prm['Z']*prm['R']*prm['T']))*(2/(prm['k']+1))**((prm['k']+1)/(prm['k']-1)));
    prm['theta'] = ((prm['k']+1)/2)**(prm['k']/(prm['k']-1));
    prm = addCylinderParams(prm);
    return prm;

def addCylinderParams(prm):
    prm['pc']=600000#1723690#600000 #supply pressure in pascal
    prm['pa']=  99974#101000 #atmosphere pressure in pascal
    prm['Br']=.0254 #bore radius in m
    prm['m'] =4 #mass of bore in kg
    prm['l'] =.0508 #stroke length in m
    prm['a'] = pi*prm['Br']**2 #area of bore
    prm['valveOpenArea'] = 0.00001;
    prm['valveClosedArea'] = 0.0000001;
    prm['maxVolt'] = 7;
    prm['minVolt'] = 3;
    return prm;

def totalChamberFlow(uc,ua,p,prm):
    return uc*fc(p,prm) - ua*fa(p,prm)

def fc(p,prm):
    return thinplate(prm['pc'],p,prm)

def fa(p,prm):
    return thinplate(p,prm['pa'],prm)

def thinplate(pu,pd,prm):
    if(pu >= pd):
        return h(pu,pd,prm)
    else:
        return -h(pd,pu,prm)

def h(pu,pd,prm):
    if ((pu/pd) <= prm['theta']):
        ratio = pd/pu;
        retval = prm['alpha']* pu * sqrt(ratio**(2/prm['k']) - ratio**((prm['k']+1)/prm['k']));
    else:
        retval = prm['beta']*pu
    return retval

def pressToVolt(ps,prm):
    #pressure sensors from .5 to 4.5 V
    minV = .5;
    maxV = 4.5;
    psRat = (ps-prm['pa']) / (prm['pc'] - prm['pa'])
    #  add noise?
    v = psRat*(maxV-minV)+minV
    return v
    
def posToVolt(x,prm):
    #potentiometers from .5 to 4.5 V
    minV = .5
    maxV = 4.5
    posRat = (x)/(prm['l']);
    v = posRat*(maxV-minV)+minV
    return v


def voltToArea(volt1,volt2,prm):
    #assumes linear volt-area relationship
    
    p1 = (volt1 - prm['minVolt']) / (prm['maxVolt']-prm['minVolt'])
    p2 = (volt2 - prm['minVolt']) / (prm['maxVolt']-prm['minVolt'])

    p11 = min(1,max(0,(p1-.5)/.5));
    p12 = min(1,max(0,(((1-p1)-.5)/.5)));
    p21 = min(1,max(0,(p2-.5)/.5));
    p22 = min(1,max(0,(((1-p2)-.5)/.5)));

    areaRange = prm['valveOpenArea'] - prm['valveClosedArea'];

    u = np.zeros((4,1));
    u[0,0] = prm['valveClosedArea'] + areaRange*p11
    u[1,0] = prm['valveClosedArea'] + areaRange*p12
    u[2,0] = prm['valveClosedArea'] + areaRange*p21
    u[3,0] = prm['valveClosedArea'] + areaRange*p22

    return u


def limit(state,prm):
    if(state[1][0] > (prm['l']-prm['l']*.01)):
        state[1][0] = (prm['l']-prm['l']*.01)
        state[0][0] = 0;
    if(state[1][0] < prm['l']*.01):
        state[1][0] = prm['l']*.01
        state[0][0] = 0;
    return state;

""" test the thin plate flow function """
def tptest():
    prm = setupThinPlateParams();
    flow = np.zeros(500000);
    for p1 in range(1,500000):
        flow[p1] = .0008*thinplate(p1,100000,prm);

    plot(flow)
    show();
       
def updateState(state,u,extForce,dt,prm):
    # extract state
    dx = state[0][0];
    x = state[1][0];
    p1 = state[2][0];
    p2 = state[3][0];

    #first find pressure velocities
    dm1 = totalChamberFlow(u[0,0],u[1,0],p1,prm) #fc(p1,prm)*u[0][0] + fa(p1,prm)*u[1][0];
    dm2 = totalChamberFlow(u[2,0],u[3,0],p2,prm)  #fc(p2,prm)*u[2][0] + fa(p2,prm)*u[3][0];
    alpha = prm['k']*prm['R']*prm['T']/prm['a'];
    dp1 = alpha*dm1/x - prm['k']*p1*dx/x
    dp2 = alpha*dm2/(prm['l']-x) + prm['k']*p2*dx/(prm['l']-x)

    ddx = (prm['a']*(p1-p2) + extForce)/prm['m'];
    state[0][0] = dx + ddx*dt
    state[1][0] = x + dx*dt
    state[2][0] = p1 + dp1*dt
    state[3][0] = p2 + dp2*dt
    return state;

    

def dyntest():
    prm = setupThinPlateParams();
    midP = (prm['pc']-prm['pa'])/2 + prm['pa']
    # state X is [dx x p1 p2]
    X = np.array([[0],[0.1*prm['l']],[prm['pa']],[prm['pa']]],dtype=float64);
   
    T = 2; #simulation time in seconds
    dt = .00001; #timestep
    nsteps = T/dt;
    u = np.array([[.0000100173],[0],[0],[0.0000059173]]) #Area of ports

    x = np.zeros(nsteps+1);
    p1 = np.zeros(nsteps+1);
    p2 = np.zeros(nsteps+1);

    for t in range(int(round(nsteps))):
        x[t] = X[1][0];
        p1[t] = X[2][0];
        p2[t] = X[3][0];
        
        if t>(nsteps/2):
            L = -00+X[0][0]*-100;
        else:
            L = X[0][0]*-100;
        
        X = updateState(X,u,L,dt,prm)
        X = limit(X,prm);

    plot(p2);
    plot(p1);
    figure()
    plot(x)
    show()


def v2a():
    prm = setupThinPlateParams();
    a1 = zeros(1000);
    a2 = zeros(1000);
    for i in range(1000):
        vlt = 3+(i/1000)*4;
        vlt2 = 7-(i/1000)*4;
        u = voltToArea(vlt,vlt2,prm) 
        a1[i] = u[0,0];
        a2[i] = u[1,0];
    plot(a1)
    plot(a2)
    show()

def pidTest():
    prm = setupThinPlateParams();
    X = np.array([[0],[0.1*prm['l']],[prm['pa']],[prm['pa']]],dtype=float64);

    xDes = posToVolt(prm['l']/2,prm)

    T = 5
    dt = .0001
    controlDT = .01;

    nsteps = T/dt;
    nstepsBetweenControl = controlDT/dt;
    sinceLastControl = nstepsBetweenControl;
    
    x = np.zeros(nsteps+1);
    p1 = np.zeros(nsteps+1);
    p2 = np.zeros(nsteps+1);
    V1 = zeros(nsteps+1);
    erri = 0
    errpPrev = 0

    Kp = .75;
    Ki = 0;
    Kd = .10;
    Ku = 1;

    for t in range(round(nsteps)):

        if(sinceLastControl==nstepsBetweenControl):
            sinceLastControl = 0

            #PID position
            errp = xDes - posToVolt(X[1,0],prm)
            erri = 0.999*erri + errp
            errv = (errpPrev-errp)/controlDT
            errpPrev = errp;

            DP = errp*Kp + erri*Ki + errv*Kd;

            #P pressure
            dp = pressToVolt(X[3,0],prm) - pressToVolt(X[2,0],prm)

            prErr = DP-dp
            
            prCmd = prErr*Ku

            v1 = 5+prCmd
            v2 = 5-prCmd


            u = voltToArea(v1,v2,prm)

        V1[t] = errp
        L = -X[0,0]*10;
        updateState(X,u,L,dt,prm)
        X = limit(X,prm);
        x[t] = X[1][0];
        p1[t] = X[2][0];
        p2[t] = X[3][0];
        sinceLastControl = sinceLastControl+1;
    plot(x)
    #plot(V1)
    show()
    print(errp)







if __name__ == "__main__":
    pidTest()
