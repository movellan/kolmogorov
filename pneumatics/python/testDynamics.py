from numpy import *
from matplotlib.pyplot import *
from twoChamberCylinder import *
import time


radius = 0.01 # cylinder radius in meters
l =0.04 # length of cylinder in meters
x0=l/2 # initial position of bore

p = Piston()
p.setRadius(radius)
p.setX(x0)
p.mass=1.0 #Kg

# let's find pressure needed to compensate for gravity
pre = p.mass*9.8/p.area



p1Series=[]
p2Series=[]
dpSeries=[]
m1=0.
m2=5.

vSeries=arange(m1,m2,(m2-m1)/3.)



p1Series=[]
p2Series=[]
p1bSeries=[]
p2bSeries=[]
dpbSeries=[]
v1Series=[]
v2Series=[]
tSeries=[]
xSeries=[]
vtSeries=[]
trialTime = 5
endTime=trialTime
p.setX(0.)
k=0
startClock=time.clock()
for v in vSeries:
	k=k+1
	print k

	v1= 5+v
	v2=5-v
	p1t = p.c1.valve.targetPressureForVoltage(v1)
	p2t = p.c2.valve.targetPressureForVoltage(v2)
	p1t = pascalToPSI(p1t)
	p2t = pascalToPSI(p2t)
	while p.t < endTime:
		print p.t
		p.update(v1,v2)
		tSeries.append(p.t)
		p1Series.append(p1t)
		p2Series.append(p2t)
		p1 = p.c1.p
		p2 = p.c2.p
		dp = p1-p2
		p1=pascalToPSI(p1)
		p2=pascalToPSI(p2)
		dp=pascalToPSI(dp)
		p1bSeries.append(p1)
		p2bSeries.append(p2)
		dpbSeries.append(dp)
		xSeries.append(100.*p.x)
		vtSeries.append(v)
		 

	endTime=endTime+trialTime
endClock=time.clock()

print 'simultation took ',endClock-startClock, ' seconds'	
clf()
plot(tSeries,p1Series,'b',tSeries,p1bSeries,'b--')
plot(tSeries,p2Series,'r',tSeries,p2bSeries,'r--')
plot(tSeries,array(xSeries)*100./max(xSeries),'k')
show()


