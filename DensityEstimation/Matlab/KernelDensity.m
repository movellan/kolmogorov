%KernelDensity:
% pdf = KernelDensity(h,k,D,W,X)
%
% Uses the Nadaraya-Watson kernel weighted average method
%pdf: the estimated density at X
%D: vector of data points
%W: Vector of weights for each data point
%X: vector of points, of which the pdf's are to be estimated
%h: kernel width

% In practice the type of kernel is not that important
% the most important variable is the width of the kernel
% Javier R. Movellan; April 2002

function pdf=KernelDensity(h,D,W,X)

N=length(D);
L=length(X);

W = W/sum(W); % normalize the weights so that we have a distriubution
for i=1:L
   U=(X(i)-D)./h;
      %Gaussian
         KU=exp(-(U.^2)/2).*W;
  			pdf(i,1)=(2*pi)^(-1/2)*sum(KU)/(h);

end

      
