clear 
clf
%Here we show how to use the facilities for probabililty density
%estimation. We are given samples from a random variable and our
%goal is to estimate the underlying probability density function
%(histogram).  We do so by putting a Gaussian Kernel over each sample. The
%best  width of the kernelis  estimated using leave-one-out crossvalidation
%methods.
% Copyright @ Javier R. Movellan, UCSD, 2008.

nsamples = 200;
D = 10+4*randn(nsamples,1); % The observed samples. 


%D: vector of data points
%W: Vector of weights for each data point
%X: vector of points, of which the pdf's are to be estimated
%h: kernel width

W = ones(nsamples,1); % weight given to each data point



sd = std(D); % standard deviation of the data
mn = mean(D)

X = [mn-4*sd:0.1*sd:mn+4*sd]; % the range upon which we want a probability
                         % density model



% number of times to do the leave-one-out crossvalidation. Do as many as
% you can. 

crossValSamples = nsamples;

 

% Here use crossvalidation to find the best kernel width to calculate the
% underlying probability density function 
i=0;
for h= 0.01*sd:0.01*sd:1*sd % put here the kernel widths you want to
                              % try. The more samples you have, the
                              % smaller the optimal width is likely to be
  h
  i= i+1;
   x(i) = h;
   % average log-likelihood in leave-one-out crossvalidation
  logLikelihood(i) = CrossValidateKernelDensity(h,D,W,crossValSamples);
end


[m, am] = max(logLikelihood);
bestKernelWidth = x(am)



subplot(3,1,1)
plot(x,logLikelihood);
xlabel('Kernel Width')
ylabel('Log Likelihood')


subplot(3,1,2)
Y=KernelDensity(bestKernelWidth,D,W,X);
plot(X,Y,'--r'); % plot probability density estimate



xlabel('X')
ylabel('Prob Density')
hold on
g =pdf('norm', X,10,4); 


; % Plot  the true density
plot(X, g, 'b');
legend('Estimated Density','True Density'); 






% Computing the pdf of new data may in principle take a lot of time. Here
% we show how to accelerate things by creating a lookup table



% First you should save your X and Y, since they will define a
% lookuptable anytime you may need it in the future

save 'X', X;
save 'Y', Y;
clear % Just to make the point that we can use this in the future 
      % without having to recompute Y
load X
load Y




newX = mn-4*sd +8*sd*rand(100000,1); % We want to obtain the pdf of 100,000
                                    % new points
  




newY = interp1(X,Y,newX,'linear','extrap');  % We do it by creating a
                                             % lookup table and
                                             % interpolating in between
  
  
 subplot(3,1,3)
   scatter(newX, newY,'.')
  xlabel('X')
  ylabel('Prob Density');
  
  title('Estimated Density Using Lookup Table')

