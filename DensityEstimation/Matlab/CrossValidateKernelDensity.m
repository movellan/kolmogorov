%CrossValidateKernelDensity:
%cv=CrossValidateKernelDensity(h,k,D,W,nsamples)
%
%cv: The average  log-likelihood of samples using leave-one-out crossvalidation
%h: width of the kernel
%D: Column vector of data points
%W: Column vector of weights assigned to each data point
%nsamples: Number of random samples used for estimating the average neg log-likelihood
%
% Javier R. Movellan; April 2002.


function cv=CrossValidateKernelDensity(h,D,W,nsamples)

  
N=length(D);
W = W/sum(W); % make sure weights add up to 1;
CW = cumsum(W);
	
rand('state',1); % Make sure we get the same sequence of random numbers
                 % every time we call this function.  
fd=0.0;
for i=1:nsamples
   % take a random sample from distribution defined by W
   j = min(find(CW>rand(1,1)));
   Ds=D;
   Ws = W;
   Ds(j)=[]; %leave the j-th element out%
   Ws(j) =[];
   % compute log-likelihood of the j-th element
    
   fd = fd + log(KernelDensity(h,Ds,Ws,D(j))); 
 
end
cv =  sum(fd)/nsamples; 
 



