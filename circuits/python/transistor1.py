import numpy as np
from matplotlib import pyplot as plt
#Input operating characteristics of NPN transistor
Vt= 0.026
Ies=14.34*(10**(-15)) # from spice model
Ics=Ies*2.116
Ics=Ies
alphaF=0.9884815
# beta = 196 # can be between 35 and 300
# alphaF = beta/(1.0+beta)
#alphaF = 0.9949238
alphaR=alphaF
alphaR = alphaF*Ies/Ics

Vbe= np.arange(0.6,0.8,0.001)
#Vbe=np.array([0.8])
vceVals=[0,0.1,12]

for Vce in vceVals:

    Vbc = -(Vce-Vbe)


    If= Ies*(np.exp(Vbe/Vt)-1)
    Ir = Ics*(np.exp(Vbc/Vt)-1)


    Ib= (1-alphaF)*If + (1-alphaR)*Ir
    Ic = alphaF*If - Ir
    print Ib*1000
    #
    #plt.semilogy(Vbe,Ib*1000)
    plt.plot(Vbe,Ib*1000)
    plt.xlabel('$V_{BE}$ Volts')
    plt.ylabel('$I_{B}$ m Amps')

plt.ylim([0,5])
plt.xlim([0.6,0.8])
plt.legend(['$V_{CE}$=0 V','$V_{CE}$=0.1 V','$V_{CE}$=12 V'])
plt.show()
plt.savefig('tin.pdf')
