# voltage transfer characteristicsself. of npn transistor
# collector connects to Rc that connects to V that connects to ground
# base connects to Vbe that coonects to ground
# emitter connects to ground
# goal is to plot the voltage transfer function
# Vce as a function of Vbe
import numpy as np
from  scipy.optimize import golden as golden
from matplotlib import pyplot as plt

V_T= 1/40.0
I_ES=14.34*(10**(-15)) # from spice model

I_CS=I_ES
alpha_F=0.9884815
alpha_R = alpha_F*I_ES/I_CS
V=12.0
R_C= 1000.0



db=0.0001

V_BEList= np.arange(0.5,0.75,db)
V_CEList = np.arange(0.5,0.75,db)
V_CEList2 = np.arange(0.5,0.75,db)
V_BCList = np.arange(0.5,0.75,db)
rhoList = np.arange(0.5,0.75,db)
I_RList = np.arange(0.5,0.75,db)


for k in range(len(V_BEList)):
    V_BE=V_BEList[k]
    I_F = I_ES * np.exp(V_BE/V_T -1.0)

    def loss(V_bc):
        I_R = I_CS*(np.exp(V_bc/V_T)-1)
        I_C = alpha_F * I_F - I_R
        V_CE1 = V_BE- V_bc
        V_CE2 = V - I_C * R_C
        rho = np.abs(V_CE1-V_CE2)
        return rho

    V_BC = golden(loss,brack=(-11,1.0))
    V_BCList[k]= V_BC
    rhoList[k] = loss(V_BC)
    I_RList[k]=  I_CS*(np.exp(V_BC/V_T)-1)

    V_CE = V_BE- V_BC
    V_CEList[k] = V_CE
    V_CEList2[k] = V- alpha_F* I_F*R_C

# plt.plot(V_BEList,V_CEList)
# plt.show()

V_BE=V_BEList[-1]
I_F = I_ES * np.exp(V_BE/V_T -1.0)
V_BC = golden(loss,brack=(-11,1.0))
I_R = I_CS*(np.exp(V_BC/V_T)-1)
rho = loss(V_BC)

print V_BC, I_R,'rho=',rho
# This one assumes I_R =0 but does not let voltage less than 0
V_CEList2[V_CEList2<0]=0
plt.plot(V_BEList,V_CEList,'b',V_BEList,V_CEList2,'r--')
plt.ylim([-0.5,12.5])
plt.show()
