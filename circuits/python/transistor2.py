import numpy as np
from matplotlib import pyplot as plt
#Output operating characteristics of NPN transistor
Vt= 0.026
Ies=14.34*(10**(-15)) # from spice model
Ics=Ies*2.116
Ics=Ies
alphaF=0.9884815
# beta = 196 # can be between 35 and 300
# alphaF = beta/(1.0+beta)
#alphaF = 0.9949238
alphaR=alphaF
alphaR = alphaF*Ies/Ics




Vce=np.arange(0,1,0.01)
IbVals = np.array([0.5,1,1.5,2])/1000.0

for Ib in IbVals:


    numer= Ib+ (1-alphaF)*Ies+ (1- alphaR)*Ics
    denom= (1-alphaF)*Ies*np.exp(Vce/Vt) +(1-alphaR)*Ics
    Vbc = Vt * np.log(numer/denom)
    Vbe = Vce + Vbc

    If= Ies*(np.exp(Vbe/Vt)-1)
    Ir = Ics*(np.exp(Vbc/Vt)-1)
    Ic = alphaF*If - Ir

    plt.plot(Vce,Ic*1000)
#
#plt.semilogy(Vbe,Ib*1000)
# plt.plot(Vce,Ic*1000)
# plt.plot()
plt.xlabel('$V_{CE}$ Volts')
plt.ylabel('$I_{C}$ m Amps')

plt.ylim([0,200])
# plt.xlim([0.6,0.8])
plt.legend(['$I_B$=0.5 mA','$I_B$=1 mA','$I_B$=1.5 mA','$I_B$=2 mA'])
plt.show()
plt.savefig('tout.pdf')
