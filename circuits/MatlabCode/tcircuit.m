function F = tcircuit(x)
  
  If=x(1);
  Ir=x(2);
  Vbe=x(3);
  Vbc=x(4);
  
  Ies= 50*10^(-9);
  Ics= 64*10^(-9);
  alphaF = 0.9901;
  alphaR= 0.7735;
  vT= 0.026;
  Vc= 10;
  Vb=10;
  R1=2000;
  R2=100000;
  
  Ifhat = Ies *(exp(Vbe/vT) -1);
  Irhat = Ics *(exp(Vbc/vT) -1);
  Vbhat = R2*(If + Ir - alphaF*If - alphaR*Ir)+Vbe;
  Vchat = R1*(alphaF*If-Ir) - Vbc + Vbe;
  
  F = (If - Ifhat)^2 + (Ir-Irhat)^2+(Vb-Vbhat)^2+ (Vc-Vchat)^2;
  
  Ie = (If - alphaR*Ir)*1000
  Ic = (alphaF*If - Ir)*1000
  Ib =  Ie - Ic
 
  

  