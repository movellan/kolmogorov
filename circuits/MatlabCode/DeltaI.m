
% Computes the current  change for 
% Iterative method to solve transistor equations
%Ic = collector current
%Ie = emitter current
%Vc = collector voltage source
%Vb = base  voltage source
%Rc = collector resistance
%Rb = base resistance
% alpha_F = Forward transport factor
% alpha_R = resverse transport factor
% Ies; Saturation current base emitter junction
% Ics: Saturation current base collector junction
% Temperature in centigrades;

% dIc= change in collecror current
% dIe = change in emitter current

function [dIc, dIe] = DeltaI(Ic, Ie,Vc,Vb,Rc, Rb,alphaF,alphaR,Ies,Ics,T)
  
  k= 1.38*10^(-23); %Boltzmann Constant in Joules/Kelvin Degree
  q= 1.6*10^(-19); %Charge of electron in Coulombs
  T = T+ 273.16; %convert to Kelvin
  Vt= k*T/q; %Thermal voltage

  
  Vbe=  Vb+ Ic*Rb - Ie*Rb;
  Vbc= Vbe + Ic*Rc - Vc;
  gF = Ies*(exp(Vbe/Vt)-1);
  gR = Ics*(exp(Vbc/Vt)-1);
  
  dIe = gF- alphaR*gR - Ic;
  dIc = - gR + alphaF*gF - Ie;
  