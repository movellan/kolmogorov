%Simulate Resistor/Diode Circuit
clear V; clear I; clear I2;
SiliconProp=1;
GermaniumProp=1-SiliconProp;

eta=2*SiliconProp + 1*GermaniumProp ; % emission coefficient 1 for silicon 2
                                       % for germanium
T=27+273.16; %Room temperature in Kelvin

% Typical saturation currents at 27 degrees centigrades. 
Is = SiliconProp*10^(-8)+ GermaniumProp*10^(-6);

k= 1.38*10^(-23); %Boltzmann Constant in Joules/Kelvin Degree
q= 1.6*10^(-19); %Charge of electron in Coulombs
VT= k*T/q; %Thermal voltage


R= 220; % resistor
V= 55;% Voltage source

i=0;


L=0.534; % Voltage for which current is 1 Amp
L= -log(Is) *eta*VT
for X= [-10:0.001:L]
  i=i+1;
    V(i) = X;
    I(i) = Is*(exp(V(i)/(eta*VT)) -1);
    if (X<0)
        I(i) = Is*(exp(-10) -1)+ (Is*exp(-10)/(eta*VT))*(V(i) + 10 *eta*VT);
    end
   
 end
%h=plot(V,I, V,I2,'--','LineWidth',2);
h=plot(V,I);

Xlabel('Volts')
 ylabel('Amps')

