% Simple simulation of Diode Resistor Capacitor series circuit.


C = 1/100000; % capacitance in Farads
R = 200; % resistance in ohms



eta=1.4;
Is= 10^(-10);
 
 
  VT= 0.026; %Thermal voltage
  
  PIV = -10; %Peak Inverse Voltge
    


VcInit = 0; 



% Time Constant




dt = 0.0001 % interstep time interval in secs ;

nt=300; % number of time steps


V=3*ones(nt,1); %independent voltage source

% First we find the intial current

Vd=0 % Voltage accross diode
for n=0:100
  Iinit = (V(1) - VcInit-Vd)/R;
  Vd = VT*eta*log((Iinit+Is) /Is);  
end






Vc=zeros(nt,1);
I=zeros(nt,1);


t=1;
Vc(t) = VcInit;
I(t) = Iinit;

  

for t=1:nt-1
  h = VT*eta*log((I(t) + Is)/Is);
  hprime(t) = VT*eta/(I(t) + Is);
  Re = R + hprime(t);
  Ve = V(t) - h + I(t)*hprime(t);
  dVc = dt* (Ve - Vc(t))/Re/C;
  Vc(t+1)= Vc(t) + dVc;
  I(t+1) = C*(Vc(t+1) - Vc(t))/dt;
 
end
hprime(t+1) = hprime(t);
subplot(1,2,1)
l=plot((1:nt)*dt,Vc,'LineWidth',2); % shows the capacitor charging
xlabel('Seconds','FontSize',14)
ylabel('Volts')
l2 = gca;
set(l2,'FontSize',14);

subplot(1,2,2)
plot((1:nt)*dt,hprime,'LineWidth',2); % shows the dynamic impedance of the diode
xlabel('Seconds')
ylabel('Ohms')
l2 = gca;
set(l2,'FontSize',14);


