  format long g
  
  
  eta=1.4;
                                          % for germanium
  T=27+273.16; %Room temperature in Kelvin
  
  
  
  % Typical saturation currents at 27 degrees centigrades.
  % I've seen all sorts of numbes here are some
  % Silicon 10^-10, Germanijm 10^-4
  % Silicon 10^-8 Germanium 10^-6
  
  Is = 10^(-10);
    
  k= 1.38*10^(-23); %Boltzmann Constant in Joules/Kelvin Degree
  q= 1.6*10^(-19); %Charge of electron in Coulombs
  VT= k*T/q; %Thermal voltage
  
  PIV = -10; %Peak Inverse Voltge
  
  thres=0.7; %Idealized Threshold
  Imax = 0.5; % Maximum forward current 
  
  
  % First we do regression for negative voltage
  clear V; clear I;
  i2=0;
  for X= [PIV:0.001:0]
    i2=i2+1;
    V(i2) = X;
    I(i2) = Is*(exp(V(i2)/(eta*VT)) -1); % note current is negative when
                                         % voltage is negative
  end
 
b1=   regress(I',V')
resistance = (1/b1) /10^(6)

% Now we do regression for positve voltage up to the threshold

clear V; clear I;
 i2=0;
  for X= [0:0.001:thres]
    i2=i2+1;
    V(i2) = X;
    I(i2) = Is*(exp(V(i2)/(eta*VT)) -1); % note current is negative when
                                         % voltage is negative
  end
 
b2=   regress(I',V')
resistance = 1/b2 

% Now we do regression above theshold

L= log((Imax +Is)/Is) *eta*VT

clear V; clear I;
 i2=0;
  for X= [thres:0.001:L]
    i2=i2+1;
    V(i2) = X;
    I(i2) = Is*(exp(V(i2)/(eta*VT)) -1); % note current is negative when
                                         % voltage is negative
        
  end
 
% We make the regression line at threshold to make the same predictions
% than the previous line, i.e., we want an equation of the form

% I = b3*(V- thres) + b2* threshold ... thus

V = V - thres;
I = I - b2*thres;


  
b=   regress(I',V')
resistance = 1/b 

