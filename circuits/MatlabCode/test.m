function F = tcircuit(If,Ir, Vbe,Vbc)
  
  Ies= 50*10^(-9);
  Ics= 64*10(-9);
  alphaF = 0.96;
  alphaR= 0.75;
  vT= 0.026;
  Vc= 5;
  Vb=5
  R1=100;
  R2=1000;
  
  Ifhat = Ies *(exp(Vbe/vT) -1);
  Irhat = Ics *(exp(Vbc/vT) -1);
  Vbhat = R2*(If + Ir - alphaF*If - alphaR*Ir)+Vbe;
  Vchat = R1*(alphaf*If-Ir) - Vbc + Vbe;
  
  F = (If - Ifhat)^2 + (Ir-Irhat)^2+(Vb-Vbhat)^2+ (Vc-Vchat)^2;
  

  