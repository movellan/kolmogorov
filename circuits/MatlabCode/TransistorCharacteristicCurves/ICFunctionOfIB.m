% The relationship between IB and IC is linear but the gain and offset
% depend on VCE



% Parameters:
clear
IES = 50*10^(-9); % Base-emitter saturation current.
ICS = 64*10^(-9); % Base-collector saturation current.
alphaF =0.96; % Forward transport Factor
alphaR = IES*alphaF/ICS; %Reverse transport factor
VT = 0.026; % Thermal voltage

G = alphaF/(1-alphaF);
theta = log((1-alphaR)*ICS/((1-alphaF)*IES));
i=0
for X = 0:0.001: 1
  i=i+1;
  VCE(i) = X;

  
Gainnum = alphaF*IES*exp(VCE(i)/VT) - ICS;
Gainden = (1- alphaR)*ICS + (1-alphaF)*IES*exp(VCE(i)/VT);
Gain(i) = Gainnum/Gainden;

GainApprox(i) = G/(1+ exp(- (VCE(i)/VT - theta)));


(Gainnum+ICS)/Gainden;
Offnum = (1-alphaF)*IES + (1-alphaR)*ICS;
Offnum = Offnum*Gainnum;
Offset(i) = Gain(i)* ((1-alphaF)*IES + (1-alphaR)*ICS)+ ICS - alphaF*IES;
GainDerivative(i) = IES*exp(VCE(i)/VT)*(alphaF - Gain(i)*(1-alphaF))/(VT* ...
                                                  Gainden);
GainDerivativeApprox(i) = GainApprox(i)*(1-GainApprox(i)/G)/VT;
end
subplot(1,2,1)
plot(VCE,Gain, VCE,GainApprox,'--','LineWidth',2)
xlabel('VCE (Volts)','Fontsize',14)
ylabel('Gain = IC/IB','Fontsize',14)
 legend('Exact', 'Approximation')
set(gca,'FontSize',14);
set(gca,'XTick',[0 0.2 0.4 0.6 0.8 1])
subplot(1,2,2)
plot(VCE,GainDerivative,VCE,GainDerivativeApprox,'--','LineWidth',2)
xlabel('VCE (Volts)','Fontsize',14)
ylabel('Derivative of Gain','Fontsize',14)
 legend('Exact', 'Approximation')
set(gca,'XTick',[0 0.2 0.4 0.6 0.8 1])

set(gca,'FontSize',14);

