% Transisotr Common Emitter Curves
% Given collector emitter voltage and base current  compute collector current

clear;
clf;

% Parameters:

IES = 50*10^(-9); % Base-emitter saturation current.
ICS = 64*10^(-9); % Base-collector saturation current.
alphaF =0.96; % Forward transport Factor
alphaR = IES*alphaF/ICS; %Reverse transport factor
VT = 0.026; % Thermal voltage

% State Variables:
IF = 0; %Forward current
IR= 0; %Reverse current
VBE =0; % Base emitter voltage
VCB =0; % Collector base voltage
VCE =0; % Collector emitter voltage
IB=0; % Base current
IC=0; % Collector current
IE=0; % Emitter Current




% 
% IF = IES*(e^(VBE/VT) -1);
% IR = ICS*(e^(VBC/VT) -1);
% IB = (1-alphaF)*IF + (1- alphaR)*IR
% IB = IF + IR
% VBE = VCE+ VBC

% Input is VCE and IB output is IC, IE, VBC, VBE

format long g

for u = 0:2.5:10
IB = u* 10^(-5);
i=0;
for X = 0:0.0001: 1
  i=i+1;
  VCE(i) = X;
  
  tmp1 = (1-alphaF)*IES + (1-alphaR)*ICS;
 tmp2 = (1-alphaF)*IES*exp(VCE(i)/VT) + (1-alphaR)*ICS;
  VBC(i) = VT*log((IB+tmp1)/tmp2); 
  VBE(i) = VBC(i) + VCE(i);
  IF = IES*(exp(VBE(i)/VT) -1);
  IR = ICS*(exp(VBC(i)/VT) -1);
  
  IC(i) = 1000*alphaF*IF - IR; % miliamps
  IE(i) = IB+IC(i);
  end

subplot(2,2,1)
hold on
plot(VCE,IC)  
subplot(2,2,2)
hold on
plot(VCE,IE)  
subplot(2,2,3)
hold on
plot(VCE,VBE)
subplot(2,2,4)
hold on
plot(VCE,VBC)
end
subplot(2,2,1)
xlabel('VCE (Volts) for different IB','Fontsize',14)
ylabel('IC (mA)','Fontsize',14)

%legend('IB 0 mA', 'IB 0.025 mA' ,'IB  0.050 mA', 'IB 0.075 mA','IB 0.1 mA','location','best')


subplot(2,2,2)
xlabel('VCE (Volts) for different IB','Fontsize',14)
ylabel('IE (mA)','Fontsize',14)

%legend('IB 0 mA', 'IB 0.025 mA' ,'IB  0.050 mA', 'IB 0.075 mA','IB 0.1 mA','location','best')


subplot(2,2,3)
xlabel('VCE (Volts) for different IB','Fontsize',14)
ylabel('VBE (Volts)','Fontsize',14)

%legend('IB 0 mA', 'IB 0.025 mA' ,'IB  0.050 mA', 'IB 0.075 mA',['IB 0.1 ' ...
%                    'mA'],'location','best')




subplot(2,2,4)
xlabel('VCE (Volts) for different IB','Fontsize',14)
ylabel('VBC (Volts)','Fontsize',14)

%legend('IB 0 mA', 'IB 0.025 mA' ,'IB  0.050 mA', 'IB 0.075 mA','IB 0.1 mA','location','best')

