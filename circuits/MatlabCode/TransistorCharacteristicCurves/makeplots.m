% Each file describes I_C as a function of V_{CE} for 
% a fixed value of base current
I0= load( 'Ibase0.0mA');
I04=load( 'Ibase0.4mA');
I08=load( 'Ibase0.8mA');
I12=load( 'Ibase1.2mA');
I16=load( 'Ibase1.6mA');
I20=load( 'Ibase2.0mA');


I0n= I0(find(I0(:,2)>0.4),:);
I04n= I04(find(I04(:,2)>0.4),:);
I08n= I08(find(I08(:,2)>0.4),:);
I12n= I12(find(I12(:,2)>0.4),:);
I16n= I16(find(I16(:,2)>0.4),:);
I20n= I20(find(I20(:,2)>0.4),:);

I0n(:,2) = 2 *(I0n(:,2) -1);
I04n(:,2) = 2* (I04n(:,2) -1);
I08n(:,2) = 2 *(I08n(:,2) -1);
I12n(:,2) = 2 *(I12n(:,2) -1);
I16n(:,2) = 2 *(I16n(:,2) -1);
I20n(:,2) = 2 *(I20n(:,2) -1);

I0n(:,3) = I0n(:,3)*1000;
I04n(:,3) = I04n(:,3)*1000;
I08n(:,3) = I08n(:,3)*1000;
I12n(:,3) = I12n(:,3)*1000;
I16n(:,3) = I16n(:,3)*1000;
I20n(:,3) = I20n(:,3)*1000;

h = plot(I0n(:,2), I0n(:,3),I04n(:,2),I04n(:,3),I08n(:,2),I08n(:,3),I12n(:,2),I12n(:,3),I16n(:,2),I16n(:,3),I20n(:,2),I20n(:,3))

set(h,'LineWidth',2)

xlabel('Base Collector Voltage (Volts)','Fontsize',16)
ylabel('Collector Current (mA)','Fontsize',16)




figure

clear
% This file has the base current as a function of the base,emitter
% voltage for a collector emitter voltage of 0.2 (saturation)
 x= load('VIBaseCurveVC0.1');
 
 
  i02 = x(:,3)*1000; % base current in mAmps
 v02 = x(:,4); %base-emitter voltage in volts


 
 h = plot(v02,i02,'LineWidth',2)


 
 
 xlabel('Base Emitter Voltage (Volts)','Fontsize',16)
ylabel('Base Current (mA)','Fontsize',16)
