% The relationship between IC and VCE for a fixed IB




% Parameters:
clear
IES = 50*10^(-9); % Base-emitter saturation current.
ICS = 64*10^(-9); % Base-collector saturation current.
alphaF =0.96; % Forward transport Factor
alphaR = IES*alphaF/ICS; %Reverse transport factor
VT = 0.026; % Thermal voltage

G = alphaF/(1-alphaF);
theta = log((1-alphaR)*ICS/((1-alphaF)*IES));

set(gca,'FontSize',14);
i=0;

GainApprox(1) = G/(1+ exp( theta));

for X=GainApprox(1):0.01:0.99999*G
  i= i+1;
  Gain(i) = X;
  VCE(i) =VT*( theta - log(G/X-1));
  dVCE(i) = VT/(Gain(i)* ( 1 - Gain(i)/G));
end
subplot(1,2,1)
plot(Gain,VCE,'LineWidth',2)
xlabel('I_C/I_B','Fontsize',14)
ylabel('V_{CE}','Fontsize',14)
set(gca,'FontSize',14);
subplot(1,2,2)
plot(Gain,dVCE,'LineWidth',2)

xlabel('I_C/I_B','Fontsize',14)
ylabel('I_C (dV_{CE}/d_{IC})','Fontsize',14)
set(gca,'FontSize',14);