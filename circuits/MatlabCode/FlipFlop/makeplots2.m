
clear
load FlipFlop2

%1- Time in secs
%2-Current through Collector2
%3- Current through Base 2
%4- Current through R12
%5-  Current through R21
%6- Current through Base1 
%7 - Current through R11
%8- Current throught R22
%9- Current through Collector 1
%10- Voltage through Capacitor 1
%11- Voltage at  Base1 
%12- Voltage through Capacitor 2
%13 -Voltage at Base2
%14 -Voltage at Collector 2



clf

%tmin = 1.108;
%tmax = 1.188;
%nmin=min(find(x(:,1) > tmin)) ; 
%nmax=max(find(x(:,1) < tmax))  ;
DT = 1:size(x,1);

h = plot(x(DT,1),1000*x(DT,3));
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Current Through Base_2  (mAmps)','Fontsize',16)

figure
h = plot(x(DT,1),1000*x(DT,2));
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Current Through Collector_2  (mAmps)','Fontsize',16)

figure
h = plot(x(DT,1),1000*x(DT,4));
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Current Through R_{12}  (mAmps)','Fontsize',16)
figure

h = plot(x(DT,1),1000*x(DT,5));
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Current Through R_{21}  (mAmps)','Fontsize',16)

figure

h = plot(x(DT,1),1000*x(DT,6));
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Current Through Base_{1} (mAmps)','Fontsize',16)

figure

h = plot(x(DT,1),1000*x(DT,7));
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Current Through R_{11} (mAmps)','Fontsize',16)

figure


h = plot(x(DT,1),1000*x(DT,8));
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Current Through R_{22} (mAmps)','Fontsize',16)

figure

h = plot(x(DT,1),1000*x(DT,9));
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Current Through Col_{1} (mAmps)','Fontsize',16)


figure


h = plot(x(DT,1),x(DT,10));
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Voltage Through Capacitor_{1} (Volts)','Fontsize',16)



figure

h = plot(x(DT,1),x(DT,11));
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Voltage at Base_{1} (Volts)','Fontsize',16)



figure

h = plot(x(DT,1),x(DT,12));
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Voltage Through Capacitor_{2} (Volts)','Fontsize',16)


figure

h = plot(x(DT,1),x(DT,13));
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Voltage at Base_{2} (Volts)','Fontsize',16)


figure

h = plot(x(DT,1),x(DT,14));
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Voltage at Collector_{2} (Volts)','Fontsize',16)





%axis([1.1 1.2  -2.5 1])
