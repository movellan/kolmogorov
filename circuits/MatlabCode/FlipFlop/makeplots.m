clear
load FlipFlop

%1- time in secs
%2- source voltage
%3- voltage base1
%4- voltage base2
%5-  voltage accross capacitor connecting to base1
%6- voltage accross capacitor connecting to base2
%7- voltage at collector 1 
%8 - voltage at collector 2
%9- current at collector 1
%10- current at collector 2
%11- current at base1
%12 current at base2



clf

tmin = 1.108;
tmax = 1.188;
nmin=min(find(x(:,1) > tmin)) ; 
nmax=max(find(x(:,1) < tmax))  ;

DT= nmin:nmax;
h = plot(x(DT,1),x(DT,3), x(DT,1),x(DT,4),'--')
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Voltage (Volts)','Fontsize',16)
legend('Base1 ', 'Base 2','Location','Best');
axis([1.1 1.2  -2.5 1])
figure

h = plot(x(DT,1),x(DT,5), x(DT,1),x(DT,6),'--')
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Voltage (Volts)','Fontsize',16)
legend('Capacitor 1', 'Capacitor 2','Location','Best');
axis([1.1 1.2  1.8 2.4])


figure
h = plot(x(DT,1),x(DT,7), x(DT,1),x(DT,8),'--')
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Voltage (Volts)','Fontsize',16)

legend('Collector 1', 'Collector 2','Location','Best');
axis([1.1 1.2  0 3.2])

figure
h = plot(x(DT,1),1000*x(DT,9), x(DT,1),1000*x(DT,10),'--')
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Current (mAmps)','Fontsize',16)
axis([1.1 1.2  -1 30])
legend('Collector 1', 'Collector 2','Location','Best');


figure
h2 = plot(x(DT,1),1000*x(DT,11), x(DT,1),1000*x(DT,12),'--')
set(h,'LineWidth',2)
set(gca,'FontSize',16);
xlabel('Time (Secs)','Fontsize',16)
ylabel('Current (mAmps)','Fontsize',16)
axis([1.1 1.2  -5 5])
% note the active base (base1)  current asymptotes to 0.05 mAmps which
% corresponds to 5mAmps for the collector
% The 0.05 asymptote must be due to the R21-> C1 rapid charge.
% This means that capacitor 2, which connects to the active collector
% must discharge at a constant rate. The voltage drop accross the
% capacitor should be determined by the resistor R11 (100 Ohms), 
% This will also determine the voltage drop across collector 1
% which shall be approx 3 - 100*5/1000 volts = 2.5


legend('Base 1', 'Base 2','Location','Best');