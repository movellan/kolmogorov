clear
R=1000; %Resistance in Ohms
C = 1/1000000; % Capacity in Farads
f= 0:10:1000;
omega = 2*pi*f;
H = 1./sqrt(1+ (R.*C.*omega).^2);
plot(f,H,'LineWidth',2)
breakout= 1/(2*pi*R*C)
Xlabel('Vin/Vout')
 ylabel('Frequency (Hertzs)')
