% Simple simulation of Transistor Resistor Capacitor 
% The base is current clamped to IB;
% There is a resistor R and capacitor Q in series with collector.
% resistor and emitter are voltage clamped to V
% Transistor Parameters:
clear
clf
IES = 50*10^(-9); % Base-emitter saturation current.
ICS = 64*10^(-9); % Base-collector saturation current.
alphaF =0.96; % Forward transport Factor
alphaR = IES*alphaF/ICS; %Reverse transport factor
VT = 0.026; % Thermal voltage



C = 1/100000; % capacitance in Farads
R = 160; % resistance in ohms
VQInit=0; % Initial voltage accross capacitor
IB= 0.0001; % Clamped base current







dt = 0.000001; % interstep time interval in secs ;


nt=10000;
V=1*ones(nt,1); %independent voltage source from collector to emitter



% First we find the intial current by iterating over base emitter equation
% We do this by succesively iterating over the two constraints. Needs to
% be done slowly (gamma =0.99) otherwise we don not converge



ICInit=0;
oldIC = ICInit;
oldVE= V(1);
gamma=0.999; 
VQ(1) = VQInit;


for t=1:nt
  n=0;
  di=1; % when to stop iteratin
  while di > (1-gamma)*0.0000001*dt % loops solves nonlinear equation
    n=n+1;
    VCEInit  = gamma*oldVE + (1-gamma)*(V(t) - R*ICInit - VQInit);
    oldVE = VCEInit;
    Gainnum = alphaF*IES*exp(VCEInit/VT) - ICS;
    Gainden = (1- alphaR)*ICS + (1-alphaF)*IES*exp(VCEInit/VT);
    Gain = Gainnum/Gainden;
    ICInit = gamma*oldIC+ (1-gamma)* IB*Gain;
    di =abs( ICInit - oldIC);
    oldIC = ICInit;
  end
  % initialize for next time step
  VCE(t) = VCEInit;
  IC(t) = ICInit;
  VBC(t) = VT*log( (IB+ (1-alphaR)*ICS+(1-alphaF)*IES)/((1-alphaR)*ICS+(1-alphaF)*IES*exp(VCE(t)/VT)));
  VBE(t) = VCE(t) + VBC(t);
  
  if t<nt
    VQ(t+1) = VQ(t) + IC(t)*dt/C;
    VQInit= VQ(t+1);
  end
  
end


nt=10000;

subplot(3,5,11,'replace')
plot((1:nt)*dt*1000,VQ(1:nt))
xlabel('MilliSecs')
%title('V_Q (Volts)')

subplot(3,5,12,'replace')
plot((1:nt)*dt*1000,VCE(1:nt))
xlabel('MilliSecs')
%title('V_{CE} (Volts)')

subplot(3,5,13,'replace')
plot((1:nt)*dt*1000,IC(1:nt)/IB)
xlabel('MilliSecs')
%title('I_{C}/I_B ')

subplot(3,5,15,'replace')
plot(sort(VCE(1:nt)),1000*sort(IC(1:nt)));
xlabel('V_{CE} (Volts)')
%title('I_{C} (mAmps)')


subplot(3,5,14,'replace')
plot((1:nt)*dt*1000,VBE(1:nt));
xlabel('MilliSecs')
%title('V_{BE} (Volts)')
%set(gca,'YTickLabel',{'22','26','30'})


%---------------------------
u = diff(IC);
uu = find(u<0);
nt= uu(1);

subplot(3,5,1,'replace')
plot((1:nt)*dt*1000,VQ(1:nt))
xlabel('MilliSecs')
title('V_Q (Volts)')

subplot(3,5,2,'replace')
plot((1:nt)*dt*1000,VCE(1:nt))
xlabel('MilliSecs')
title('V_{CE} (Volts)')
%set(gca,'YTickLabel',{'0.61','0.61','0.62'})  
subplot(3,5,3,'replace')
plot((1:nt)*dt*1000,IC(1:nt)/IB)
xlabel('MilliSecs')
title('I_{C}/I_B')

subplot(3,5,5,'replace')
plot(sort(VCE(1:nt)),1000*sort(IC(1:nt)));
xlabel('V_{CE} (Volts)')
title('I_{C} (mAmps)')
%set(gca,'XTickLabel',{'61','62','62'})  
subplot(3,5,4,'replace')
plot((1:nt)*dt*1000,VBE(1:nt));
xlabel('MilliSecs')
title('V_{BE} (Volts)')
%set(gca,'YTickLabel',{'28','28','28'}) 





nt= uu(1)+80;
 



subplot(3,5,6,'replace')
plot((1:nt)*dt*1000,VQ(1:nt))
xlabel('MilliSecs')
%title('V_Q (Volts)')

subplot(3,5,7,'replace')
plot((1:nt)*dt*1000,VCE(1:nt))
xlabel('MilliSecs')
%title('V_{CE} (Volts)')

subplot(3,5,8,'replace')
plot((1:nt)*dt*1000,IC(1:nt)/IB)
xlabel('MilliSecs')
%title('I_{C}/I_B')

subplot(3,5,10,'replace')
plot(sort(VCE(1:nt)),1000*sort(IC(1:nt)));
xlabel('V_{CE} (Volts)')
%title('I_{C} (mAmps)')
%set(gca,'XTickLabel',{'61','62','63'})  

subplot(3,5,9,'replace')
plot((1:nt)*dt*1000,VBE(1:nt));
xlabel('MilliSecs')
%title('V_{BE} (Volts)')
%set(gca,'YTickLabel',{'28','28','28'})  



 


 
 






