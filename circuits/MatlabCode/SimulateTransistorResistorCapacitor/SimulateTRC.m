% Simple simulation of Transistor Resistor Capacitor 
% Resistor and capacitor are in series with base.
% Base and Collector-Emmitter are voltage clamped
% Transistor Parameters:
clear

IES = 50*10^(-9); % Base-emitter saturation current.
ICS = 64*10^(-9); % Base-collector saturation current.
alphaF =0.96; % Forward transport Factor
alphaR = IES*alphaF/ICS; %Reverse transport factor
VT = 0.026; % Thermal voltage



C = 1/100000; % capacitance in Farads
R = 2000; % resistance in ohms
VCAInit=0; % Initial voltage accross capacitor








dt = 0.0001 % interstep time interval in secs ;

nt=2000; % number of time steps

VCE=3*ones(nt,1); %independent voltage source from collector to emitter



% First we find the intial current by iterating over base emitter equation



VBEinit=0;
for n=0:100
  IBinit = (VCE(1) - VCAInit-VBEinit)/R 
  tmp1 = (1-alphaF)*IES + (1-alphaR)*ICS;
  tmp2 = (1-alphaF)*IES*exp(VCE(1)/VT) + (1-alphaR)*ICS;
  VBCinit = VT*log((IBinit+tmp1)/tmp2); 
  VBEinit = VBCinit + VCE(1);
end






VCA=zeros(nt,1);
VBE = zeros(nt,1);
IB=zeros(nt,1);


t=1;
VBE(t) = VBEinit;
IB(t) = IBinit;
VCA(t) = VCAInit;
  

for t=1:nt-1
  tmp1 = (1-alphaF)*IES + (1-alphaR)*ICS;
  tmp2 = (1-alphaF)*IES*exp(VCE(t)/VT) + (1-alphaR)*ICS;
  tmp3 = VT*log((IB(t)+tmp1)/tmp2); 
  h = tmp3 + VCE(t); % VBE = h(IB)
  hprime(t) = VT/(IB(t) + tmp1);

  Re = R + hprime(t);
  Ve = VCE(t) - h + IB(t)*hprime(t);
  dVCA = dt* (Ve - VCA(t))/Re/C;
  VCA(t+1)= VCA(t) + dVCA;
  IB(t+1) = C*(VCA(t+1) - VCA(t))/dt;
 
end
hprime(t+1) = hprime(t);

subplot(1,3,1)
l=plot((1:nt)*dt,VCA,'LineWidth',2); % shows the capacitor charging
xlabel('Seconds','FontSize',14)
ylabel('V_{CA} (Volts)','FontSize',14)
l1 = gca;
set(l1,'FontSize',14);


subplot(1,3,2)
plot((1:nt)*dt,VCE-VCA,'LineWidth',2); % shows the dynamic impedance of the diode
xlabel('Seconds','FontSize',14)
ylabel('V_{CE} (Volts)','FontSize',14)
l2 = gca;
set(l2,'FontSize',14);



subplot(1,3,3)
plot((1:nt)*dt,hprime,'LineWidth',2); % shows the dynamic impedance of the diode
xlabel('Seconds','FontSize',14)
ylabel('BE Dynamic Impedance (Ohms)','FontSize',14)
l3 = gca;
set(l3,'FontSize',14);



