Vc=5;
Vb=5;
Rc=2000;
Rb=100000;
Ies = 50*10^(-9);
Ics=  64*10^(-9);
beta = 100;
alphaF= beta/(1+beta);
alphaR = alphaF*Ies/Ics;
T=27;



for t=1:1000
  
   k= 1.38*10^(-23); %Boltzmann Constant in Joules/Kelvin Degree
  q= 1.6*10^(-19); %Charge of electron in Coulombs
  T = T+ 273.16; %convert to Kelvin
  Vt= k*T/q; %Thermal voltage

  
  Vbe=  Vb + Ic*Rb - Ie*Rb;
  If Vbe> 0.8
    Vbe=0.8
  end
  Vbc= Vbe + Ic*Rc - Vc;
  
  if Vbc> 0.8
    Vbc=0.8
  end
  
  
  gF = Ies*(exp(Vbe/Vt)-1);
  gR = Ics*(exp(Vbc/Vt)-1);
  
  dIe = gF- alphaR*gR - Ic;
  dIc = - gR + alphaF*gF - Ie;
 
 % [dIc, dIe] = Deltai(Ic, Ie,Vc,Vb,Rc, Rb,alphaF,alphaR,Ies,Ics,T)
 
  if (dIc>0.001) 
    dIc=0.001; 
  end

  if (dIc<-0.001) 
    dIc=-0.001; 
  end

  if (dIe>0.001) 
    dIe=0.001; 
  end

  if (dIe<-0.001) 
    dIe=-0.001; 
  end

  Ic = Ic+dIc;
  Ie = Ie+dIe;
end
  