clear
R=1000; %Resistance in Ohms
C = 1/200000; % Capacity in Farads
peakF= 60;
L = 1/((2*pi*peakF)^2*C);
f= 0:1:300;
omega = 2*pi*f;
H = 1./sqrt(1+ (R.*(C .*omega - 1./(L.*omega))).^2);
plot(f,H,'LineWidth',2)
Dfhalf= (sqrt((1/R^2 + 4 *C/L))/(2*C));
omegamin=- 1/(2*R*C) + Dfhalf;
fmin = omegamin/(2*pi)
omegamax =1/(2*R*C) + Dfhalf;
fmax = omegamax/(2*pi)
fpeak = 1/(2*pi*sqrt(L*C))
clear omega;

omega= omegamin;
H = 1./sqrt(1+ (R.*(C .*omega - 1./(L.*omega))).^2)

omega= omegamax;
H = 1./sqrt(1+ (R.*(C .*omega - 1./(L.*omega))).^2)
omega= fpeak*2*pi;
H = 1./sqrt(1+ (R.*(C .*omega - 1./(L.*omega))).^2)

Xlabel('Vin/Vout')
 ylabel('Frequency (Hertzs)')
