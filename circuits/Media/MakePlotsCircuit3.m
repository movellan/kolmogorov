load Circuit3
 plot(t,v4/10,t,vbe,':',t,vce/10,'-.',t,vdiod/10,'--')
 legend('V4/10','V_{BE}','V_{CE}/10','V_{Diode}/10')
 ylabel('Volts')
 xlabel('Seconds') 
 
 figure
 
 plot(t,ib*1000,t,ic*10,'--')
 ylabel('mAmps')
 xlabel('Seconds') 
 legend('I_B','I_C/100')
 