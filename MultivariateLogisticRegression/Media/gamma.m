clear 
clf
set( 0,'DefaultAxesFontSize',18,'DefaultTextFontSize',18)     




x = [ -3:0.0001:3];
beta =10;

k = [ 0.8 1 2 3]
rows = length(k)
c=0
for p = k
c=c+1;

subplot(rows,3,1+(c-1)*3)
  y1 = (log((exp(beta*x) + exp(-beta*x))/2)/beta).^p;

  plot(x,y1)


 if(c==4)  xlabel('x');end
 if(c ==1)  title('\Gamma(x)'); end

  subplot(rows,3,2+(c-1)*3)
  
  yg = tanh(beta*x) .* (log((exp(beta*x) + exp(-beta*x))/2)/beta).^(p-1);
  

  plot(x,yg)
  

 if(c==4)  xlabel('x');end
 

if(c==1)   title('d \Gamma(x) /dx'); end
  
  subplot(rows,3,3+(c-1)*3)
  
  yh = (p-1) * (log((exp(beta*x) + exp(-beta*x))/2)/beta).^(p-2) .* tanh(beta*x).^2 + beta*(1 - (tanh(beta*x).^2)).*(log((exp(beta*x) + exp(-beta*x))/2)/beta).^(p-1);
  
  
  

  plot(x,yh)
  


  
   if(c==4)  xlabel('x');end
   

if(c==1)   title('d2 \Gamma(x)/dx^2'); end
  
  end
  