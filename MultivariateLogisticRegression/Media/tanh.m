clear 
clf
set( 0,'DefaultAxesFontSize',18,'DefaultTextFontSize',18)     




x = [ -3:0.01:3];
beta =4
p=1;
subplot(2,1,2)
  y1 = (log((exp(beta*x) + exp(-beta*x))/2)/beta).^p;
  y2 = (abs(x)).^p

  plot(x,y1, x, y2)

legend('Integral of tanh','abs')  
  xlabel('x')
  ylabel('h(x)')

  subplot(2,1,1)
  
  y1 = (exp(2*beta*x) -1) ./(exp(2*beta*x)+1);
  y2 = (sign(x))
  plot(x,y1,x,y2)
  

  legend('tanh','sign')      
  xlabel('x')

  ylabel('dh(x)/dx')