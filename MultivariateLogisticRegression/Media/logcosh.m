clear
clear 
clf
set( 0,'DefaultAxesFontSize',16,'DefaultTextFontSize',16)     




p=1; beta =4;
x= -4:0.01:4;

subplot(2,1,1)

  y = log(2.*cosh(beta*x))./beta;
  v = abs(x);

plot(x,y,'--',x,v)
legend('logCosh','abs')      
  xlabel('x')

  ylabel('h(x)')

  
subplot(2,1,2)

y = tanh(beta*x)
v= sign(x)

plot(x,y,'--',x,v)

  legend('tanh','sign')      
  xlabel('x')

  ylabel('dh(x)/dx')
  
  
% y = (p-1)*(((log(cosh(beta*x)))/beta).^(p-2)).*(tanh(beta*x).^2);
       
%        y = y+  (beta*((log(cosh(beta*x)))/beta).^(p-1)).*(1 - (tanh(beta*x)).^2);
        
%   plot(x,y)

