\documentclass[12pt]{article}

\usepackage{array,amsgen,amssymb,amsopn,amsmath,amsthm}
\usepackage[pdftex]{graphicx}
\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[square,numbers,sort]{natbib} 
%\usepackage[round,sort]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}


\newcommand\R{{\mathbb R}}
\title{Tutorial on Multivariate Logistic Regression}
\author{ Javier R. Movellan}

\begin{document}

\maketitle

    
\newpage


\section{Motivation}

The last few years have seen a resurgence of interest in the Machine
Learning community for main-effect models, i.e., models without
interaction effects. This is due to the emergence of learning
algorithms, like SVMs and ADABoost that work well with very high
dimensional representations, avoiding the need for modeling
interaction effects. The goal of this tutorials is to introduce the
most important main effect regression models, how they relate to each other and common algorithms used for training them. 



\section{General Model}
The inputs are $n$-dimensional vectors the outputs are $c$-dimensional
vectors. The training sample consist of $m$ input output pairs. We
organize the example inputs as an $m \times n$ matrix $x$. The
corresponding example outputs are organized as a $c \times m$ matrix
$y$.  The models under consideration make predictions $\hat y = h(u)$
where $u$ is a linear transformation of the data $u = x \theta$, and
$\theta$ is a $c \times n$ weight matrix.
\begin{figure}[hb]
\begin{center}
\includegraphics[width=.8\textwidth]{Media/matrices.pdf}
\end{center}
\caption{{\it There are $m$ examples, $n$ input variables and $c$ output variables. }} \label{matrices}
\end{figure}
We  evaluate the optimality of $\hat y$, and thus of $\theta$,
using the  following criterion 
\begin{equation}
\Phi(\theta) = - \sum_{j=1}^m w_j \rho_j(y, \hat y) 
+ \frac{\alpha}{2} \sum_{k=1}^c  \theta_k^T \theta_k
\end{equation}
$\rho_j$ is a function that measures the mismatch between the $j^{th}$
rows of $y$ and $\hat y$.  For example 

\begin{align}
\rho_j(y, \hat y) =
\sum_{k=1}^c (y_{j,k} - \hat y_{j,k})^2
\end{align}
We let $\rho = (\rho_1,
\rho_m)^T$.  The terms $w_1, \ldots, w_m$ are positive weights that
capture the relative importance of each of the $m$ input-output pairs,
and $\alpha$ is a positive constant.  Informally, the first term can
be seen as a negative log-likelihood function, capturing the degree of
match between the data and the model, the second term can be
interpreted as a negative log prior over $\theta$.

Our goal is to find a matrix $\theta$ tha minimizes $\Phi$.  A
standard approach for minimizing $\Phi$ is the Newton-Raphson
algorithm (See Appendix B), which calls for computation of the
gradient and the Hessian matrix (See Appendix A).

Let $\theta_i$ represent the $i^{th}$ row of $\theta$. The gradient
can be computed using the chain rule (See Appendix A)
\begin{align}
&\nabla_{\theta_i} 
\frac{\alpha}{2} \sum_{k=1}^c  \theta_k^T \theta_k
= \alpha \theta_i\\
&\nabla_{\theta_i} \sum_{j=1}^m w_j \rho_j(y, \hat y) =
\nabla_{\theta_i} \hat y_i \nabla_{\hat y_i} \rho \nabla_\rho
\sum_{j=1}^m w_j \rho_j(y, \hat y) = x^T  w  \Psi_i  
\end{align}
where ${w}$ is a diagonal matrix with diagonal elements $w_1,
\ldots, w_m$ and
\begin{equation}
\Psi_i= 
\left( 
\begin{array}{c}
\frac{\partial \rho_1}{\partial \hat y_{1i}}\\
\vdots\\
\frac{\partial \rho_m}{\partial \hat y_{mi}}
\end{array}
\right)
\end{equation}
Thus
\begin{equation}
\nabla_{\theta_i} \Phi = x^T  w \Psi_i  + \alpha \theta_i
\end{equation}
The overall gradient vector follows $\nabla_\theta \Phi = 
\left( \nabla_{\theta_1} \Phi,  \ldots, \nabla_{\theta_n}
\Phi\right)^T$. The chain rule can  be used to compute the
component matrices of the overall Hessian matrix
\begin{align}
&\nabla_{\theta_j} \nabla_{\theta_i} \Phi = 
\nabla_{\theta_j} \alpha \theta_i + \nabla_{\theta_j} x^T w \Psi_i\\
&\nabla_{\theta_j} \alpha \theta_i = \delta_{ij} \alpha I_n
\end{align}
where $\delta_{ik}$ is the Kronecker delta function (0 valued
unless $i=k$ in which case it takes value 1) and $I_n$ is the $n\times
n$ identity matrix.
\begin{equation}
\nabla_{\theta_j}  x^T w \Psi_i  
= \nabla_{\theta_j} \hat y_j
\nabla_{\hat y_j} \Psi_i \nabla_{\Psi_i} x^T w \Psi_i
= x^T   \Psi'_{ij}  w  x 
\end{equation}
 and
\begin{equation}
\Psi'_{ij}= 
\left( 
\begin{array}{cccc}
\frac{\partial^2 \rho_1}{\partial \hat y_{1j} \partial \hat y_{1i}} &0 &\cdots &0\\
0 &\frac{\partial^2 \rho_2(y, \hat y)}{\partial \hat y_{2j} \partial
\hat y_{2i}}  &\ddots &0\\
\vdots  &\cdots &\ddots  &\vdots\\
0 &0 &\cdots  &\frac{\partial^2  \rho_m}{\partial \hat y_{mj}
\partial \hat y_{mi}} \\
\end{array}
\right)
\end{equation}
Thus 
\begin{equation}
\nabla_{\theta_j} \nabla_{\theta_i} \Phi = 
x^T \Psi'_{ij} w x + \delta_{ij} \alpha I_n
\end{equation}
The overall Hessian matrix follows
\begin{equation}
\nabla_\theta  \nabla_\theta \Phi = 
\left(\begin{array}{ccc}
\nabla_{\theta_1}  \nabla_{\theta_1} \Phi &\cdots &\nabla_{\theta_1} \nabla_{\theta_1} \Phi\\
\vdots &\ddots &\vdots\\
\nabla_{\theta_n}  \nabla_{\theta_1} \Phi &\cdots &\nabla_{\theta_n}  \nabla_{\theta_n} \Phi\\
\end{array}
\right)
\end{equation}

\section{Linear Ridge Regression}
Without loss of generality we will examine the case with a single
response variable, i.e.,  $c=1$.  Our error function will be the sum
of squared errors, i.e,
\begin{equation}
\rho_j(y, \hat y) = \frac{1}{2}(y_j - \hat y_j)^2
\end{equation}
Thus
\begin{equation}
\Psi_i= -
\left( 
\begin{array}{c}
\frac{\partial \rho_1}{\partial \hat y_{1i}}\\
\vdots\\
\frac{\partial \rho_m}{\partial \hat y_{mi}} \\
\end{array}
\right)
= \left(
\begin{array}{c}
y_1 - \hat y_1\\
\vdots\\
y_m - \hat y_m \\
\end{array}
\right) = y - x\theta
\end{equation}
The gradient follows 
\begin{equation}
\nabla_{\theta} \Phi = x^T  w (y - x \theta)  + \alpha \theta
\end{equation}
Setting the equation to zero results on a linear equation on
$\theta$, that can be solved analytically
\begin{equation}
\hat \theta =  \left(x^T w x + \alpha I_n \right)^{-1} x^T w y
\end{equation}
where $\hat \theta$ represents the optimal value of $\theta$.

\section{Robust Regression}

 \begin{equation}
\frac{\alpha}{2} \theta^T \theta + \sum_{j=1}^m w_j \rho ( y_j - \hat y_j)
\end{equation}
where  $\hat y = x \theta$. Where $\rho$ typically penalizes extreme
deviations  between $y$ and $\hat y$ less than the squared error function does. It is easy to show that in this case
\begin{equation}\label{eqn:robust1}
\nabla_\theta \Phi = - x^T w \Psi + \alpha \theta
\end{equation}
where $\Psi_j = \rho'(y_j - \hat y_j)$ the derivative of the error function. This derivative is known
as the {\em influence function} for it controls the influence of each
example on the final solution.  Note if $\rho(y_j \hat y_j) = (y_h -
\hat y_j)^2$ then $\Psi = y - \hat y$. 
If $\rho$ has no second derivative, a common procedure to find an optimal $\theta$ involves the use of the following heuristic. Note that \eqref{eqn:robust1} can be expressed as follows:
\begin{equation}
\nabla_\theta \Phi = - x^t \tilde{w} (y - \hat y) + \theta
\end{equation}
where $\tilde{w}$ is an $m \times m$  diagonal matrix such that
$w_{jj} =  w_j \Psi_j/ (y_j - \hat y_j)$. Setting this gradient to zero, and disregarding the fact that $w$ is a function of $\theta$, results on a linear weighted least squares problem whose solution we have already seen. This iterative version of weighted least squares we start with an arbitrary value $\theta$ and apply the following iteration: 
\begin{equation}
\theta_{t+1} = (x^t \tilde{w}_t x + \alpha I_n)^{-1} x^t \tilde{w}_t y
\end{equation}
where $w_t$ is the matrix of weights obtained using $\theta_t$. 

If $\rho$ has second derivative, then the Hessian matrix exists and
has the following form
\begin{equation}
\nabla_\theta \nabla_\theta \Phi =  x^T \Psi' w x + \alpha I_n
\end{equation}
Where $\Psi$ is an $m \times m$ diagonal matrix such that $\Psi_{jj} = \rho^{''} (y_j - \hat y_j)$, the second derivative of the error function. The Newton-Raphson algorithm calls for the following iteration
\begin{equation}
\theta_{t+1} = \theta_t + (x^t \Psi'  x)^{-1} x^t \Psi
\end{equation}
which can also be casted as weighted least squares solution with weight matrix $\Psi'$ and desired response vector $z = x \theta_t + \Psi^{-1} x^T \Psi$ 



\section{Multinomial Logistic Regression}
Let
\begin{equation}
\rho_j(y,\hat y) =
\sum_{k=1}^c y_{jk} \log h_k(\hat y_{j}) 
\end{equation}
where
\begin{equation}
h_k(\hat y_{j}) =
\frac{e^{\hat y_{jk}}}{\sum_{i=1}^c  e^{\hat y_{ji}}} 
\end{equation}
Note for any $u =(u_1,\ldots,u_c)^T \in \R^c$ 
\begin{equation}
\frac{\partial h_k(u) }{\partial u_i}
= h_k(u) \frac{\partial \log h_k(u) }{\partial u_i}
=
h_k(u) (\delta_{ik} - h_i(u))
\end{equation}
Thus
\begin{equation}
\frac{\partial \rho_j(y,\hat y) }{\partial \hat y_{ji}}
= \sum_{k=1}^c \frac{y_{jk}}{h_k(\hat y_{jk})}
h_k(\hat y_{jk})(\delta_{ik} - h(\hat y_{ji}))
 = y_{ji} - h_i(\hat y_{ji})
\end{equation}
\begin{equation}
\Psi_i = y_i - h_i( \hat y)
\end{equation}
and
\begin{equation}
\Psi'_{ki} = 
\left( 
\begin{array}{cccc}
\hat y_{1k} (\delta_{ki} - \hat y_{1i}) &0 &\cdots &0\\
0 &\hat y_{2k} (\delta_{ki} - \hat y_{2i})  &\ddots &0\\
\vdots  &\cdots &\ddots  &\vdots\\
0 &0 &\cdots  &\hat y_{mk} (\delta_{ki} - \hat y_{mi}) \\
\end{array}
\right)
\end{equation}

\subsection{Iteratively Reweighted Least Squares}
When there are only two response alternatives and $\alpha = 0$  the Newton-
Raphson algorithm simplifies as follows (See Appendix B)
\begin{equation}
\theta(t+1) = \theta(t) +  \left(x^T \tilde{w}(t) x \right)^{-1} x^T w (y - \hat{y}(t))
\end{equation}
where $\theta(t)$ represents the $n$-dimensional vector of weights at iteration $t$, $y$ and $\hat y(t)$ are $m$-dimensional vectors containing the desired and predicted probabilities for the first response alternative and $w(t)$ is an $m \times m$ matrix $ w =    \nabla_{x \theta} \hat y$, i.e., $w_{ii} = \hat y_i (1 - \hat y_i)$. Some simple algebra shows that this iteration can be casted as the solution to a weighted linear regression problem
\begin{equation}
\theta(t+1) = \left(x^T \tilde{w}(t) x\right)^{-1} x^T \tilde{w}(t) z(t) 
\end{equation}
where $z(t) = x \theta(t) + w \tilde{w}^{-1}(t) (y - \hat{y})$ is a
modified target variable.

It is common practice to start the iterations at the zero vector, i.e.,  $\theta_0 = 0$. It is easy to show that in such case the matrix $w$ has constant diagonal terms and the solution obtained after the first iteration is the standard least squares solution. Successive iterations change the weight assigned to each example. 


\section{Robust Regression}
In robust regression the criterion function is of the form
\begin{equation}
\Phi(\theta) = \frac{\alpha}{2} \theta^T \theta + \sum_{j=1}^m w_j \rho ( y_j - \hat y_j)
\end{equation}
where  $\hat y = x \theta$. Where $\rho$ typically penalizes extreme
deviations  between $y$ and $\hat y$ less than the squared error function does. It is easy to show that in this case
\begin{equation}\label{eqn:robust1}
\nabla_\theta \Phi = - x^T w \Psi + \alpha \theta
\end{equation}
where $\Psi_j = \rho'(y_j - \hat y_j)$ the derivative of the error function. This derivative is known
as the {\em influence function} for it controls the influence of each
example on the final solution.  Note if $\rho(y_j \hat y_j) = (y_h -
\hat y_j)^2$ then $\Psi = y - \hat y$. 
If $\rho$ has no second derivative, a common procedure to find an optimal $\theta$ involves the use of the following heuristic. Note that \eqref{eqn:robust1} can be expressed as follows:
\begin{equation}
\nabla_\theta \Phi = - x^t \tilde{w} (y - \hat y) + \theta
\end{equation}
where $\tilde{w}$ is an $m \times m$  diagonal matrix such that
$w_{jj} =  w_j \Psi_j/ (y_j - \hat y_j)$. Setting this gradient to zero, and disregarding the fact that $w$ is a function of $\theta$, results on a linear weighted least squares problem whose solution we have already seen. This iterative version of weighted least squares we start with an arbitrary value $\theta$ and apply the following iteration: 
\begin{equation}
\theta_{t+1} = (x^t \tilde{w}_t x + \alpha I_n)^{-1} x^t \tilde{w}_t y
\end{equation}
where $w_t$ is the matrix of weights obtained using $\theta_t$. 

If $\rho$ has second derivative, then the Hessian matrix exists and
has the following form
\begin{equation}
\nabla_\theta \nabla_\theta \Phi =  x^T \Psi' w x + \alpha I_n
\end{equation}
Where $\Psi$ is an $m \times m$ diagonal matrix such that $\Psi_{jj} = \rho^{''} (y_j - \hat y_j)$, the second derivative of the error function. The Newton-Raphson algorithm calls for the following iteration
\begin{equation}
\theta_{t+1} = \theta_t + (x^t \Psi'  x)^{-1} x^t \Psi
\end{equation}
which can also be casted as weighted least squares solution with weight matrix $\Psi'$ and desired response vector $z = x \theta_t + \Psi^{-1} x^T \Psi$ 



\section{Support Vector Machines}
In support vector machines (SVMs) there typically is a single response variable $c=1$ and the error function has the following form
\begin{equation}
\rho(y_i, \hat y_i) =  - \max \{ 1- y_i  \hat y, 0\} 
\end{equation}
where $y \in \{0,1\}$. It is easy to show that as $\alpha \to 0$, the solution is the hyperplane that maximizes the minimum distance from the training examples to the hyperplane. 

It is interesting to note tha tthe SVM error function and the logistic
error function are are very similar, the logistic error function being
a smooth version of the SVM error function.

\section{ADABoost and the Backfitting algorithm}
Under construction.

\appendix

\section{Vector Calculus}

{\bf Definition Gradient Matrix:} Let $y = f(x)$ where $y \in \R^n$, $x \in \R^m$.
The gradient of $y$ with respect to $x$, symbolized $\nabla_x \; y$, is
an $m \times n$ matrix defined as follows
\begin{equation}
\nabla_x y = \left(\begin{array}{ccc}
\frac{\partial y_1}{\partial x_1} &\cdots &\frac{\partial y_n}{\partial x_1}\\
\vdots &\ddots &\vdots\\
\frac{\partial y_1}{\partial x_m} &\cdots &\frac{\partial y_n}{\partial x_m}\\
\end{array} 
\right)
\end{equation}

 {\bf Gradient of quadratic functions:} Let $y = x^T a x + b x + c$ where $x \in \R^n$. It can be shown that
\begin{equation}
\nabla_x \; y =  b^T+ ( a + a^T) x
\end{equation}
where $a^T$ is the transpose of $a$. Note $y \in \R^n$. If $a$ is symmetric then $a + a^T = 2a$.
 
{\bf Chain Rule}: Let $y = f(x)$, $x \in \R^m$,  $y \in \R^n$ and $z = h(y)$, $z \in \R^o$. Then
\begin{equation}
\nabla_x \; z = \nabla_x \: y \; \nabla_y \: z
\end{equation}

{\bf Example}: Let $y = (x - \mu)^T a (x - \mu)$ where $x, \in \R^n$ and $\mu$ is a constant vector. Then 
\begin{equation}
\nabla_y \; x = \nabla_x \;  (x - \mu) \; \nabla_{(x - \mu)}\; y = 
I_n a (x - \mu) = a (x - \mu).
\end{equation}
where $I_n$ is the $n\times n$ identity matrix.

{\bf Definition Hessian Matrix:} Let $y = f(x)$, $y \in \R$, $x \in \R^n$. The matrix 
\begin{equation}
\nabla_x \nabla_x f(x) = 
\left(\begin{array}{ccc}
\frac{\partial^2  y}{\partial x_1 \partial x_1} &\cdots &\frac{\partial^2 y}{\partial x_1 \partial x_n}\\
\vdots &\ddots &\vdots\\
\frac{\partial^2 y}{\partial x_n \partial x_1} &\cdots &\frac{\partial^2 }{\partial x_n \partial x_n}\\
\end{array}
\right)
\end{equation}
is called the Hessian matrix of $y$.
{\bf Example}: Let $ y = x^T a x + b x + c$ then
\begin{equation}
\nabla_x \: \nabla_x y = \nabla_x a x + b^T = a
\end{equation}

\section{Newton-Raphson Optimization Method}
Let $y = f(x)$, for $y \in \R$, $x \in \R^n$. The Newton-Raphson algorithm is an iterative method for optimizing $y$. We start the process at an arbitrary point $x_0 \in \R^n$ .  Let $x_t \in \R^n$ represent the state of the algorithm at iteration $t$. We approximate the function $f$ using the linear and quadratic terms of the Taylor expansion of $f$ around $x_t$. 
\begin{equation}
\hat{f}_t(x) = f(x_t) +   \nabla_x f(x_t) (x - x_t)^T +   
\frac{1}{2} (x- x_t)^T \left( \nabla_x \nabla_x f(x_t)\right) (x - x_t)
\end{equation}
and then we then find the extremum of $\hat{f}_t$ with respect to $x$ and move directly to that extremum. To do so note that
\begin{equation}
\nabla_x \hat{f}_t(x) = \nabla_x f(x_t) + \left( \nabla_x \nabla_x f(x_t)\right) (x - x_t)
\end{equation}
We let $x(t+1)$ be the value of $x$ for which $\nabla_x \hat{f}_t(x) =0$
\begin{equation}
x_{t+1} = x_t + \left( \nabla_x \nabla_x f(x_t) \right)^{-1} \nabla_x f(x_t)
\end{equation}
It is useful to compare the Newton-Raphson method with the standard method of gradient ascent. The gradient ascent iteration is defined as follows
\begin{equation}
x_{t+1} = x_t + \epsilon I_n \nabla_x f(x_t)
\end{equation}
where $\epsilon$ is a small positive constant. Thus gradient descent can be seen as a Newton-Raphson method in which the Hessian matrix is approximated by $\frac{1}{\epsilon} I_n$.

\section{Gauss-Newton Optimization Method}
Let $f(x) = \sum_{i=1}^n r_i(x)^2$ for $r_i: \R^n \to \R$. We start
the process with an arbitrary point $x_0 \in \R^n$. Let $x_t \in \R^n$ represent the state of the algorithm at iteration $t$. We approximate the functions $r_i$ using the linear  term of their Taylor expansion  around $x_t$. 
\begin{align}
&\hat{r}_i(x_t) = r_i(x_t) + (\nabla_x r_i(x_t))^T (x - x_t)\\
&\hat{f}_t(x) = \sum_{i=1}^n (\hat{r}_i(x_t))^2  = \sum_{i=1}^n
(r_i(x_t)- (\nabla_x r_i(x_t))^Tx_t  + (\nabla_x r_i(x_t))^T  x)^2\\
\end{align}
Minimizing $\hat{f}_t(x)$ is a linear least squares problem of well
known solution. If we let $y_i = (\nabla_x r_i(x_t))^T x_t -r_i(x_t)$
and $u_i = \nabla_x r_i(x_t)$ then
\begin{equation}
x_{t+1} = (\sum_{i=1}^n u_i u_i^T)^{-1} (\sum_{i=1}^n u_i y_i)
\end{equation}

\section*{History}
\begin{itemize}
\item The first version of this document was written by Javier
R. Movellan, May 2002. It was 7 pages long. 
\item The document was made open source under GNU FDL license 1.1 as part of the Kolmogorov
project on August 9 2002.
\item Javier Movellan added Gauss-Newton Method on March 14, 2003.

\item October 9, 2003. Javier R. Movellan changed the license to GFDL 1.2 and included an endorsement section.

\end{itemize}
\bibliographystyle{apa}
\bibliography{movellan.mypapers}

\end{document}
