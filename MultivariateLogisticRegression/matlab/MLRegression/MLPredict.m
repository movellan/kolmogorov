% MLPredict Make prediction using Multivariate Logistic Regression
% with parameter matrix B and input X 
% There are c dependent variables, n independent variables and m examples.
% X is an m x n  matrix. 
% Yhat is a m x c matrix of response measures  in [0,1] range. Rows add up to 1.
% B is an n x c matrix of initial weight values. B = 0 is recommended. 

% There is a short tutorial on this topic at the 
% Machine Perception Laboratory's Web site and the Kolmogorov project site.


% Copyright (C) Javier R. Movellan April 2002
% Copyright (C)  Machine Perception Laboratory
% This program is free software; 
% you can redistribute it and/or modify it under the terms 
% of the GNU General Public License 


function Yhat = MLPredict(X,B)

   Yhat = exp(X*B);
   Yhat = Yhat./repmat(sum(Yhat,2),1,size(Yhat,2)); 
   
   