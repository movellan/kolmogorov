% Checks the weighting of examples functionality

clear
X = [0 1 0 1]';
X = [X ones(4,1)]; % add a constant for the bias term
Y= [ 0 1 1 0]';
Y = [Y 1-Y];

% First we check what happens when we weight all the examples
% equally and when we double the weight of the first two examples
W1 = [ 1 1 1 1]';
W2 =[ 2 2 1 1]';

Binit = zeros(2,2); % Initial estimate of B
alpha = 0;

[Bhat1, Yhat1]= MLRegress(X,W1,Y,Binit,alpha);
[Bhat2, Yhat2]= MLRegress(X,W2,Y,Binit,alpha);


Bhat1
Bhat2


% Now we check what happens when replicate the first two examples
X=[ 0 1 0 1 0 1]';
X = [X ones(6,1)]; % add a constant for the bias term

Y=[ 0 1 0 1 1 0]';
Y = [ Y 1-Y];

W4=[ 1 1 0 0 1 1]'; % Should replicate the results with W1

[Bhat4, Yhat4]= MLRegress(X,W4,Y,Binit,alpha);

Bhat4


W5=[ 1 1 1 1 1 1]'; % Should replicate the results with W2

[Bhat5, Yhat5]= MLRegress(X,W5,Y,Binit,alpha);

Bhat5