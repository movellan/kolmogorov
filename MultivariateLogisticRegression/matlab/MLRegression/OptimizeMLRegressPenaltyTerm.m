%Find optimal value of penalty term for Multivariate Logistic Regression:
%alpha  = OptimizePenaltyTerm(X,W,Y,Binit,nsamples,min, max)
%alpha is the optimal value of quadratic penalty term (weight decay)
%that maximizes generalization to new sampls. 
% X is an m x n  matrix. 
% W is an m x 1 vector specifying the relative importance of each example
% Y is a m x c matrix of response measures  in [0,1] range. Rows add up to 1.
% Binit is an n x c matrix of initial weight values. Binit = 0 is recommended.  
% nsamples is the number of leave-one-out samples used for obtaining
% the expected generalization likelihood
% min and max bracket the values of alpha being searched.
%
% This program requires CrossValidateMLRegress and MLRegress
% There is a short tutorial on this topic at the 
% Machine Perception Laboratory's Web site.
% Copyright (C) Javier R. Movellan April 2002
% Copyright (C)  Machine Perception Laboratory
% This program is free software; 
% you can redistribute it and/or modify it under the terms 
% of the GNU General Public License 

function alpha  = OptimizeMLRegressPenaltyTerm(X,W,Y,Binit,nsamples,min, max)
alpha = fmin('CrossValidateMLRegress', min, max,[],X,W,Y,Binit,nsamples);

