clear
m = 4000; %number of examples
n = 3;
X = randn(m,n); % n indep variables, m examples
X = [X ones(m,1)]; % add a constant for the bias term
c = 3; % cumber of outputs
TrueB =  [ 1 -1 0 ; 2 -2 0; 3  -3  0; 4 -4 0 ];

%TrueB =  randn(n+1,c);
        
sigma = 0.0;

Y = MLSample(X,TrueB,sigma); % Generate a random sample of data from the
                             % true model. We will use it to estimate B

W = ones(size(X,1),1); % All data points are given equal weight


Binit = zeros(size(TrueB)); % Initial estimate of B
alpha =0.000001;


%[Bhat, Yhat]= MLRegressTmp(X,W,Y,Binit,alpha);

[Bhat, Yhat]= MLRegress(X,W,Y,Binit,alpha);

Bhat
TrueB
norm(Bhat(:,1) - TrueB(:,1))

%Y
%Yhat
