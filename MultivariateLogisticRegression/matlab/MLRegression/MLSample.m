% MLSample
% function Y = MLSample(X,B,sigma)
% Sample from noisy Multivariate Logistic Model
% X is an m x n  matrix. 
% Y is a m x c matrix of response measures  in [0,1] range. Rows add up to 1.
% B is an n x c matrix of  weight values.



function Y = MLSample(X,B,sigma)
Y = exp(X*B);
Y = Y + sigma*randn(size(Y)); % dangerous. what if Y is negative?
Y = Y./repmat(sum(Y,2),1,size(Y,2)); % normalize rows to add up to 1 
