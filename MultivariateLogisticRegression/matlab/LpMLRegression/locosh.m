function y=logcosh(x)
  y = zeros(size(x));
  for i=1: length(x)
    if(abs(x))<10
      y(i) = log(2*cosh(x(i)));
    else
      y(i) = abs(x(i));
    end
  end
  

  