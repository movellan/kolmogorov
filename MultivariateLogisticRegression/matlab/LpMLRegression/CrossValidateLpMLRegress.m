%Cross Validate Multivariate Logistic Regression:
%LMean= CrossValidateMLRegress(alpha,X,W,Y,Binit,nsamples)
%
% Uses leave-one-out method to estimate the average
% likelihood achieved by a Multivariate Logistic Regression Model,  
% on data not used for training. 
%
% It relies on the program MLRegress.
% There are c dependent variables, n independent variables and m
% examples.
%
% Lmean is the average negative log-likelihood obtained accross
% leave-one-out samples
% Bmean is the average of the B parameters obtained accross 
% the leave-one-out cross-validation trials
% alpha is the weight penalty term. It controls the strenght of a
% Gaussian prior on the B matrix favoring small magnitudes of B. 
% X is an m x n  matrix. 
% W is an m x 1 vector specifying the relative importance of each example
% Y is a m x c matrix of response measures  in [0,1] range. Rows add up to 1.
% Binit is an n x c matrix of initial weight values. Binit = 0 is recommended.  
% nsamples is the number of leave-one-out samples used for obtaining
% the expected generalization likelihood
%
% There is a short tutorial on this topic at the 
% Machine Perception Laboratory's Web site.
% Copyright (C) Javier R. Movellan April 2002
% Copyright (C)  Machine Perception Laboratory
% This program is free software; 
% you can redistribute it and/or modify it under the terms 
% of the GNU General Public License 


function [LMean, BMean]  = CrossValidateLpMLRegress(X,W,Y,Binit,alpha,p,tolerance,nsamples)



m=size(X,1);

Wtmp = abs(W);
Wtmp = W/sum(W); % make sure weights are positive and add up to 1;

CW = zeros(m,1); % cumulative weights
tmp =0.0;
for i=1: m
   tmp = Wtmp(i)+ tmp;
   CW(i) = tmp;
end
 clear Wtmp;	

 rand('state',1); % Make sure we get the same sequence of random numbers
                 % every time we call this function.  
 
 L=zeros(nsamples,1);  
 BMean = zeros(size(Binit));
for sample=1:nsamples
  sample
   % take a random sample from distribution defined by W
   j = min(find(CW>rand(1,1)));
   Xs=X;
   Ys = Y;
   Ws = W;
   Xs(j,:)=[]; %leave the j-th element out%
   Ws(j) =[];
   Ys(j,:) = [];
   % compute log-likelihood of the j-th element
  
   Bhat = LpMLRegress(Xs,Ws,Ys,Binit,alpha,p,tolerance);
   BMean = BMean+ Bhat;
   Yhat = exp(X(j,:)*Bhat);
   Yhat = Yhat/sum(Yhat); 
   L(sample) = sum(Y(j,:).*log(Yhat));
end
LMean = - mean(L); % give the negative log-likelihood so that we can use fmin
BMean = BMean/nsamples;


