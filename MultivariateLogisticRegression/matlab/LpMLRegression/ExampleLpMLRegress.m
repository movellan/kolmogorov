% This illustrates how to use the LpMLRegress.m
% and the potential benefits of using non Euclidean regularizers


clear


%%%%%%%%%%%%%%%%%%%
% Create an input matrix that approximates a common situation we encouter
% in machine learning problems. One of the input variables is the better
% one but all the input variables are highly correlated
sigma = 0.1;
m = 1000; 
X = randn(m,1); 
n = randn(m,5);
 X=[ X X(:,1)+(n(:,1)*sigma) X(:,1)+(n(:,1)+n(:,2))*sigma X(:,1)+ ...
     (n(:,1)+n(:,2)+n(:,3))*sigma X(:,1)+(n(:,1)+n(:,2)+n(:,3)+n(:,4))*sigma X(:,1)+(n(:,1)+n(:,2)+n(:,3)+n(:,4)+n(:,5))*sigma];

 
 
X = [X ones(m,1)]; % add a constant for the bias term

% Onlyt the first input variable is relevant
TrueB =  [ 1 0  ; 0 0; 0 0 ; 0 0 ;  0 0 ; 0 0 ; 0 0 ];

Y = MLSample(X,TrueB,sigma); % Generate a random sample of data from the
                             % true model. We will use it to estimate B

W = ones(size(X,1),1); % All data points are given equal weight
Binit = 1*randn(size(TrueB)); % Initial estimate of B


Binit(:,end) = 0; 

% Try and see how things changes when using p =2, which regularizes using
% the Euclidean norm, and p =0.7, which is non Eucledian.

% uncomment the one you want to use
%p = 2;
p= 0.7;



alpha=1/1000 ;
tolerance = 0.0001

[Bhat, Yhat]= LpMLRegress(X,W,Y,Binit,alpha,p,tolerance);

%In this particular problem, using hundreds of simulations it can be
%shown that p=0.7 produces much better estimates of the true B parameters
%
% alpha=0 -> average error in B estimate = 0.24
%
% alpha=1/1000, p =2 -> average error  =0.1459
%
% alpha = 1/1000 p = 1 -> average error  =  0.075
%
% alpha = 1/1000 p = 0.7 -> average error = 0.06
%
% alpha = 1/1000 p= 0.1 -> average error = 0.22











