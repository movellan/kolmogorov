%Ridge Regression on Model y = xb + constant
% the constant does not get ridged
% Javier R. Movellan
% Feb 2007
function b = RidgeRegression(y,x,alpha)
  n = size(x,1);
  x = [x ones(n,1)];
  d = n*ones(size(x,2),1);
  d(end)=0;
  c = x'*x +alpha*alpha*diag(d) ;
  b = pinv(c)*x'*y;
