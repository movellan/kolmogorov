function y=logcosh(x,beta)
  y = zeros(size(x));
  for i=1: length(x)
    if(abs(x*beta))<10
      y(i) = (log(2*cosh(x(i)*beta)))./beta;
    else
      y(i) = abs(x(i));
    end
  end
  

  