function A=ROC_area(Y,X)

       % A=ROC_area(Y,X)
       % A is a 1-by-m vector (areas under ROCs)
       % Y is an n-by-m matrix of real values (outputs)
       % X is an n-by-m matrix of binary values (0,1 or +1,-1) (labels)
       %
       % Gwen 11/11/2006
       %

       if size(Y,2)>size(Y,1), Y=Y'; end;
       if size(X,2)>size(X,1), X=X'; end;
       %keyboard;
       n=size(Y,1);
       for i=1:size(Y,2)
        [s t]=sort(Y(:,i));
        f=find(X(t,i)==1);
        s=size(f,1);
        A(i)=(mean(f)-(s+1)/2)/(n-s);
       end
