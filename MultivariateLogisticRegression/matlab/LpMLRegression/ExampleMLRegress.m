%rand('seed', 1);
%randn('seed', 1);
m = 100

sigma = 0.1
X = randn(m,1); % 2 indep variables, m examples
n = randn(m,5);
 X=[ X X(:,1)+(n(:,1)*sigma) X(:,1)+(n(:,1)+n(:,2))*sigma X(:,1)+ ...
     (n(:,1)+n(:,2)+n(:,3))*sigma X(:,1)+(n(:,1)+n(:,2)+n(:,3)+n(:,4))*sigma X(:,1)+(n(:,1)+n(:,2)+n(:,3)+n(:,4)+n(:,5))*sigma];

X = [X ones(m,1)]; % add a constant for the bias term
%TrueB =  [ 3 0 ; 2 0; -1 0];

 TrueB =  [ 1 0  ; 0 0; 0 0 ; 0 0 ;  0 0 ; 0 0 ; 0 0 ];
        
sigma = 0.1;

Y = MLSample(X,TrueB,sigma); % Generate a random sample of data from the
                             % true model. We will use it to estimate B

W = ones(size(X,1),1); % All data points are given equal weight
Binit = 1*randn(size(TrueB)); % Initial estimate of B


Binit(:,end) = 0; 

 p  = 1/100
p = 2
p=0.5



 alpha = 1/100
alpha=1/1000 
%alpha=0
Binit


[Bhat, Yhat]= MLRegress(X,W,Y,Binit,alpha);

Bhat
TrueB