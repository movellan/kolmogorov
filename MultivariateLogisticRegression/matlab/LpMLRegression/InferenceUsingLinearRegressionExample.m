% Example of how to do classic hypothesis testing with linear regression
% models. 
% Javier R. Movellan, 2006
clear;


% First we are going to create a dataset. You can substitute x and y with
% actual empirical data.

x = rand(1000,9); % the independent variables

n = size(x,1); % number of examples
x = [ones(n,1) x ];

b = [ 1 2 3 0 0 0 0 0 0 0]';

sigma = 2; % True standard deviation of the noise
y = x*b + sigma*randn(n,1); % the dependent variable


ridge =0.01; % add a small ridge term to avoid numereical problems

% Note only the first 3 columsn of x have an effect on y.  Suppose you
% dont know b and your taks as an analyst is to decide which variables
% are important. In this example we will assume that  you already decided
% the first three variables are important and you try to check whether
% the other 7 variables are.

% First we create a full model with all the variables
xFull = x;

% We estimate b by using the standard least squares equations
bhatFull = pinv(xFull'*xFull+ridge*eye(size(xFull,2))) *xFull'*y;

% model predictions
yhatFull = xFull*bhatFull;

% error and sum  of squares errors for the full model
e = y - yhatFull;
sseFull = sum( e.^2);
% degrees of freedom for full model
dfFull = n - size(xFull,2);



nvars =3;
% Second we create a restricted model with a subset of variables


xRest = x(:,1:nvars);

% Compute the estimate of b for the restricted model 

bhatRest = pinv(xRest'*xRest+ridge*eye(size(xRest,2))) *xRest'*y;



% prediction, error and sum of squared errors for restricted model
yhatRest = xRest*bhatRest;
e = y - yhatRest;
sseRest = sum( e.^2);

% degrees of freedom for restricted model
dfRest = n - size(xRest,2);


% Mean squared error, an estimate of the variance  of the noise (the
% standard dev of the noise squared)
MSE = sseFull/dfFull


% Now we are going to test whether augmenting the Restricted model to the
% full model produces a statistically significant reduction in error
% F statistic
F = (sseRest - sseFull)/(( dfRest -dfFull)*MSE)

F = abs(F); % sometimes negative because of very very small numerical error

dfNum = dfRest - dfFull
dfDenominator = dfFull


pValue = 1- fcdf(F,dfNum,dfDenominator)


% Exercise. Try running the example with nvars =1 and nvars= 2  and see what happens


