% LpMLRegress Lp  Regulirazid Multinomial Logistic Regression
% [Bhat Yhat] = MLRegress(X,W,Y,B,alpha,p)
% Fits model of form: Yhat proportional to  exp(X*Bhat)
% The objective function is multinomial log-likelihood with a 
% L-p penalty term on the Bhat parameters
% This function is optimized using a  Newton-Raphson with an adaptive
% damping parameter. 
% There are c dependent variables, n independent variables and m examples.
% X is an m x n  matrix. 
% W is an m x 1 vector specifying the relative importance of each example
% Y is a m x c matrix of response measures  in [0,1] range. Rows add up to 1.
% B is an n x c matrix of initial weight values. 
% Small random weights are recommended. 
% alpha is the weight of the regularization term term. 
% It controls the strenght of a prior that favors small magnitudes of B. 
% p controls the order of the regularization term
%  R(B) = (\sum_i \sum_j |B_ij|^p) / p
% 
% tolerance: determines when to stop learning process.If change in
% parameters < tolerance then stop 
% 
% A bias term can be introduced by having a column of  X be all "ones"
% The program automatically detects this fact and does not apply the
% decay term alpha to that column.
% There are many possible solutions to Bhat, we choose the one for which
% the last column is all zeros.
% There is a short tutorial on this topic at the 
% Machine Perception Laboratory's Web site. 



% Copyright (C) Javier R. Movellan  June 2009
% Copyright (C)  Machine Perception Laboratory
% This program is free software; 
% you can redistribute it and/or modify it under the terms 
% of the GNU General Public License 


function [B, Yhat]= LpMLRegress(X,W,Y,B,alpha,p,tolerance)
  if(nargin < 6)
    error('MLRegress requires 6  arguments')
  end

  plotResults =1;
  
  n = size(X,2); % number of input variables
  m = size(X,1); % number of examples
  c = size(Y,2); % number of output variables
  
  % we approximate the absolute value function with a smooth version
  % abs(x) approx f(x) = (log cosh(beta*x))/beta. 
  % as beta -> infty f(x) converges to abs(x)
  % We use beta = 10
  
  beta =10;

  
  % We use a damped Newton-Raphson algorithm for learning the B coefficients
  %  Delta B = - (Hessian(B) - \lambda I)^{-1} Gradient(B)
  % For lamabda = 0 we have a pure Newton-Raphson method. It is very
  % aggressive and has a tendency to become stable. As lambda increases
  % we have gradient descent with smaller and smaller step size. It is
  % more stable but slower. We start with lambda= lambdaInit and
  % progressively adapt it to optimze performance. We dont let lambda
  % become smaller than lambdaMin or larger than LambdaMax. The current
  % values seem to work well but it may be possible to find better values
  
  lambdaInit = 10; 
  lambdaMin =0.1; % If this is small  we tend to get stuck in local min
  lambdaMax = 100;
  
  
  
  maxIterations = 1000;
  
  
  lambda=lambdaInit;

  
  bias_term = find(((max(X) ==1) + (min(X) == 1)) ==2); % finds whether
                                                        % there is a
                                                        % column of
                                                        % ones which
                                                        % represents a
                                                        % bias term.  
  
  
  
  % We first normalize all the inputs (except the column of ones) to have
  % equal variance. This is important when using a parameter
  % regularization term. After we compute the optimal B parameters we
  % undo the effects of the input normalization

  Istd = 1./std(X);
  Istd(bias_term) = 1;
  Istd = diag(Istd);
  X = X*Istd;

  alphaeye  = alpha*eye(n);
  alphaeye(bias_term,bias_term) = 0; % gets rid of decay term for the bias


  MyHess = eye(n*(c-1));
  MyGrad = zeros(n*(c-1),1);
  delta =1;
  Yhatold = Y;
  iteration = 0;
  W = abs(W); W = m*W/sum(W);
  Wd = sparse(m,m);
  for i=1:m
    Wd(i,i) = W(i);
  end
  

  iteration =1;
  

  Yhat = exp(X*B);
  Yhat = Yhat./repmat(sum(Yhat,2),1,size(Yhat,2)); % normalize rows to add up to 1 
  cost(1) = costFunction(Y,Yhat,m,B,p,beta,alpha);

  stopNow =0;
  
  Bchange =0;  
  
  while(stopNow <1) 
    if (iteration > 20 && Bchange < tolerance)
      stopNow = 1;
    end
    if (iteration > maxIterations)
      stopNow = 1;
    end
    
    
    stepSize(iteration) = 1/lambda;
    iteration = iteration+1;
    
    Yhat = exp(X*B);
    Yhat = Yhat./repmat(sum(Yhat,2),1,size(Yhat,2)); % normalize rows to add up
                                                     % to 1

    
    delta = max(max((abs(Yhatold - Yhat))));   
    % construct the Hessian matrix  and the gradient vector
    % without loss of generality we fix the last column of B to zero.
    for i=1:c-1 
      for j=1:c-1         
        temp = Yhat(:,i).* (kdelta(i,j) - Yhat(:,j)).*W;
        myn = size(temp,1);
        L = sparse(1:myn, 1:myn,temp); % creates sparse diagonal
      	MyHess((i-1)*n+1:i*n, (j-1)*n+1: j*n) =  X'*L*X/m + kdelta(i,j)* ...
            alphaeye*fHess(B(:,i),p,beta);    
        
      end;
      MyGrad((i-1)*n+1:i*n) = X'*Wd*(Yhat(:,i) - Y(:,i))./m +alphaeye* fGrad(B(:,i),p,beta);  

    end

    Bold = B;
    
    % On every iteration we run the Newton-Raphson algorithm with three
    % different lambda dampening parameter. This allows us to adapt
    % lambda on the fly 
    
    % First we try with the current lambda
    BTmp1 = B;
    BTmp1(:,1:c-1) = B(:,1:c-1) - reshape(pinv(MyHess+lambda*eye(size(MyHess)))*MyGrad,n,c-1);
    YhatTmp1 = exp(X*BTmp1) ;
    YhatTmp1 = YhatTmp1./repmat(sum(YhatTmp1,2),1,size(YhatTmp1,2)); 
    costTmp(1) = costFunction(Y,YhatTmp1,m,BTmp1,p,beta,alpha);
    
    % Second we try with lambda 20 % larger than current
    BTmp2 = B;
    BTmp2(:,1:c-1) = B(:,1:c-1) - reshape(pinv(MyHess+lambda*1.2*eye(size(MyHess)))*MyGrad,n,c-1);
    YhatTmp2 = exp(X*BTmp2) ;
    YhatTmp2 = YhatTmp2./repmat(sum(YhatTmp2,2),1,size(YhatTmp2,2)); 
    costTmp(2) = costFunction(Y,YhatTmp2,m,BTmp2,p,beta,alpha);

   % Third we try with lambda 20 % smaller than current 
    BTmp3 = B;
    BTmp3(:,1:c-1) = B(:,1:c-1) - reshape(pinv(MyHess+(lambda/1.2)*eye(size(MyHess)))*MyGrad,n,c-1);
    YhatTmp3 = exp(X*BTmp3) ;
    YhatTmp3 = YhatTmp3./repmat(sum(YhatTmp3,2),1,size(YhatTmp3,2)); 
    costTmp(3) = costFunction(Y,YhatTmp3,m,BTmp3,p,beta,alpha);

    
    costTmp(4) = cost(iteration-1);
    [junk best] = min(costTmp);

    % Pick the lambda that reduced the error the most, then adapt the
    % current lambda accordingly
    if best ==1 % Current lambda is good
      B = BTmp1;
      cost(iteration) = costTmp(1);
      Yhat = YhatTmp1;
    elseif best ==2 % We need to increase lambda. 
                    % ie., reduce step size and behave more like gradient descent
      lambda = lambda*1.2;
      if lambda > lambdaMax
        lambda = lambdaMax;
      end
      
      B = BTmp2;
      cost(iteration) = costTmp(2);
      Yhat = YhatTmp2;
    elseif best ==3 % We can decrease lambda, ie., behave more like
                    % Newton-Raphson
      lambda = lambda/1.2;
      if lambda < lambdaMin
        lambda = lambdaMin;
      end
      
      B = BTmp3;
      cost(iteration) = costTmp(3);
      Yhat = YhatTmp3;
    else % we did not improve. Increase lamda even more
      lambda = lambda*(1.2^2);
      if lambda > lambdaMax
        lambda = lambdaMax;
      end
      cost(iteration) = costTmp(4);
    end
    tmp = B;
    if iteration ==1
      Bchange = 1
      Bchange  = (max(max(abs(Bold - B))))/((c-1)*n);
    else
      Bchange = 0.8*Bchange+ 0.2*(max(max(Bold - B)))/((c-1)*n);
    end
          
    Bplot(iteration,:) = reshape(tmp, size(tmp,1)*size(tmp,2),1);
  end
  
  
  if (plotResults  ==1)
    iteration
    cost(end)
    B= Istd*B;
    subplot(3,1,1)
    plot(cost)
    xlabel('Iteration')
    ylabel('- Log Posterior')
    subplot(3,1,2)
    plot(stepSize)
    xlabel('Iteration')
    ylabel('step Size')
    subplot(3,1,3)
    plot(Bplot)
    xlabel('Weight Parameters')
    ylabel('step Size')

  end
  
  
  
function y = fGrad(x,p,beta)

  if p==200
    y = x;
  else
    y = ((softAbs(x,beta)).^(p-1)).*tanh(beta*x);
  end

function y = fHess(x,p,beta)
  tmp = length(x);
  if p==200
    y = sparse(1:tmp, 1:tmp,ones(tmp,1));
  else 
    for i=1: length(x)
      tmp2(i) = (p-1)*((softAbs(x(i),beta)).^(p-2)).*tanh((beta*x(i)).^2);
      tmp2(i) = tmp2(i) + ((softAbs(x(i),beta)).^(p-1)).*beta.*(1 - ...
                                                        (tanh(beta*x(i)).^2));
      if(isinf(tmp2(i)))
        keyboard
      end
      
    end
    y = sparse(1:tmp, 1:tmp,tmp2);
  end
  
  
function k = kdelta(i,j)
  if i == j 
    k = 1;
  else
    k=0;
  end


function y = costFunction(Y, Yhat, m, B,p,beta,alpha)
  
  y = - sum(sum(Y.*log(Yhat)))/m;
  y = y+ alpha*(sum(sum((softAbs(B,beta)).^p)))/p;
  
  
function y=softAbs(x,beta)
  y = zeros(size(x));
  for i=1: size(x,1)
    for j=1: size(x,2)
      if(abs(x(i,j))*beta)<10
        y(i,j) = (log(2*cosh(beta*x(i,j))))/beta;
      else
        y(i,j) = abs(x(i,j));
      end
    end
  end
  
  