import numpy
from pylab import *
import theano
import theano.tensor as T
import time
rng = numpy.random
# logistic regression
def logisticRegress(dx,dy,winit,binit,learningRate,trials):    
    N = shape(dx)[0]
    feats=shape(dx)[1]
    training_steps = trials
    print N, feats
    # Declare Theano symbolic variables
    x = T.matrix("x")
    y = T.vector("y")
    w = theano.shared(winit, name="w")
    b = theano.shared(binit, name="b")
    print "Initial model:"
    print w.get_value(), b.get_value()
    
    
    # build symbolic expression (aka expression graph)
    p_1 = 1 / (1 + T.exp(-T.dot(x, w) - b))   # Probability that target = 1


    prediction = p_1>0.5
    xent = -y*T.log(p_1) -(1-y) *T.log(1-p_1)
    cost = xent.mean()
    gw,gb = T.grad(cost,[w,b])

    #compile
    print "start compiling"
    train = theano.function(
        inputs=[x,y],
        outputs =[prediction,cost],
        updates={w:w-learningRate*gw, b:b-learningRate*gb})
    predict=theano.function(
        inputs=[x],
        outputs=p_1)
    print "start training"
    # Train
    Cost = numpy.zeros(training_steps)
    timeBefore = time.clock()
    for i in range(training_steps):
        pred,err=train(dx,dy)
        Cost[i] = err.sum()
    timeAfter =time.clock()    

    print "Final model:"
    print w.get_value(), b.get_value()
    print "target values for Y:", dy
    print "prediction for Y:", predict(dx)
    print "Train Time=", timeAfter-timeBefore
    return [w.get_value(),b.get_value(),Cost]
