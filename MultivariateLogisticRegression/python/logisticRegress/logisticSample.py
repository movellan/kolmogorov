from pylab import *
def logisticSample(x,w,b,sigma):
    c= shape(w)[1]  # number of outputs (categories)
    m = shape(x)[0] # number of examples
    y = x*w+ b+ sigma*normal(0,1,[m,c])
    y = 1.0/(1.0+exp(-y))
    return y
