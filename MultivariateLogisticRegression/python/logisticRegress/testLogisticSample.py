from pylab import *
from  logisticSample import *


m = 10 # number of samples
n = 3 # number of variables
c=1 # number of outputs
sigma=0.0
trueW = mat([[-1],[1],[0]])
trueB=0
X=normal(0,1,[m,n])
trueY=logisticSample(X,trueW,trueB,0)
Y=logisticSample(X,trueW,trueB,sigma)

print sum(Y,0)/m
print sum(trueY,0)/m
