\documentclass[12pt]{article}

\usepackage{array,amsgen,amssymb,amsopn,amsmath,amsthm}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[square,numbers,sort]{natbib} 
%\usepackage[round,sort]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}


\newcommand\R{{\mathbb R}}
\title{Tutorial on Multinomial Logistic Regression}
\author{ Javier R. Movellan}

\begin{document}

\maketitle

    
\newpage



\section{General Model}
The inputs are $n$-dimensional vectors the outputs are $c$-dimensional
vectors. The training sample consist of $m$ input output pairs. We
organize the example inputs as an $m \times n$ matrix $x$. The
corresponding example outputs are organized as a $m \times c$ matrix
$y$.  The models under consideration make predictions 
\begin{align}
&\hat y = h(u)\\
&u = x \theta
\end{align}
where 
$\theta$ is a $n \times c$ weight matrix. Note $\theta_{ij}$ can be
seen as the connection strength from  input variable $X_i$ to
output variable $U_j$.
\begin{figure}[hb]
\begin{center}
\includegraphics[width=.8\textwidth]{Media/matrices.pdf}
\caption{{\it There are $m$ examples, $n$ input variables and $c$ output variables. }} \label{matrices}
\end{center}
\end{figure}
We  evaluate the optimality of $\hat y$, and thus of $\theta$,
using the  following criterion 
\begin{align}
&L(\theta) = \Phi(\theta) + \Gamma(\theta)\\
&\Phi(\theta) =  \sum_{j=1}^m w_j \rho_j 
\end{align}
where $\Gamma(\theta)$ is a prior penalty function of the parameters, 
\begin{align}
\rho_j = f(y_{j\cdot}, \hat y_{j\cdot})
\end{align}
measures the mismatch between the $j^{th}$ rows of $y$ and $\hat y$.
The terms $w_1, \ldots, w_m$ are positive weights that capture the
relative importance of each of the $m$ input-output pairs.

Our goal is to find a matrix $\theta$ that minimizes $L$.  A
standard approach for minimizing $L$ is the Newton-Raphson
algorithm  which calls for computation of the
gradient and the Hessian matrix. 
\subsection{Gradient Vector}
Let $\theta_{\cdot i} \in \R^n$ be the $i^{th}$ column of $\theta$,
i.e., the set of connection strengths from the $n$ input units to the $i^{th}$ output unit. 
The overall gradient of $\Phi$ with respect to $\theta$ is as follows
\begin{align}
\nabla_{\text{vec}[\theta]} \Phi = 
\left( 
\begin{array}{c}
\nabla_{\theta_{\cdot 1}} \Phi\\
\nabla_{\theta_{\cdot 2}} \Phi\\
  \vdots\\
 \nabla_{\theta_{\cdot c}} \Phi
\end{array}
\right)
\end{align}
where ${\text{vec}[\theta]}$ is the vectorized version of $\theta$ and
the gradient of a vector $q$ with respect to a vector $p$ is defined
as follows
\begin{align}
\nabla_p q = \frac{\partial q'}{\partial p}
\end{align}
Using the chain rule for gradients we get 
\begin{align}
&\frac{\partial \Phi'}{\partial  \theta_{\cdot i}}
 = \frac{\partial u_{\cdot i}'}{\partial  \theta_{\cdot i}}
\frac{\partial \rho' }{\partial  u_{\cdot i}}
\frac{\partial \Phi '}{\partial  \rho}
\end{align}
Note
\begin{align}
u_{\cdot i} = (x \theta)_{\cdot i} = x \theta_{\cdot i}
\end{align}
Thus
\begin{align}
\frac{\partial u_{\cdot i}'}{\partial  \theta_{\cdot i}}
= 
\frac{\partial \theta_{\cdot i}' x'}{\partial  \theta_{\cdot i}} = x'
\end{align}
Moreover
\begin{align}
\frac{\partial \rho'}{\partial  u_{\cdot i}}
=
\left( 
\begin{array}{ccc}
\frac{\partial \rho_1}{\partial  u_{1i}} &\cdots &\frac{\partial \rho_m}{\partial  u_{1i}} \\ 
\vdots &\vdots &\vdots\\
\frac{\partial \rho_1}{\partial  u_{mi}} &\cdots &\frac{\partial \rho_m}{\partial  u_{mi}} \\ 
\end{array}
\right)
=
\left( 
\begin{array}{cccc}
\frac{\partial \rho_1}{\partial  u_{1i}} &0& \cdots &0\\
0 & \frac{\partial \rho_2}{\partial  u_{2i}} & \cdots &0\\
\vdots &\vdots &\vdots &\vdots \\
0 &0 &0  &\frac{\partial \rho_m}{\partial  u_{mi}} \\ 
\end{array}
\right)
\end{align}
and
\begin{align}
\frac{\partial \Phi'}{\partial  \rho}
=
\left( 
\begin{array}{c}
w_1\\
\vdots\\
w_m
\end{array}
\right)
\end{align}
Thus
\begin{align}
&\frac{\partial \Phi'}{\partial  \theta_{\cdot i}}
= x'
\left( 
\begin{array}{cccc}
\frac{\partial \rho_1}{\partial  u_{1i}} &0& \cdots &0\\
0 & \frac{\partial \rho_2}{\partial  u_{2i}} & \cdots &0\\
\vdots &\vdots &\vdots &\vdots \\
0 &0 &0  &\frac{\partial \rho_m}{\partial  u_{mi}} \\ 
\end{array}
\right)
\left( 
\begin{array}{c}
w_1\\
w_2\\
\vdots\\
w_m
\end{array}
\right)
\end{align}
Equivalently
\begin{align}
&\nabla_{\theta_{\cdot i}} \Phi =  \frac{\partial \Phi'}{\partial  \theta_{\cdot i}}
= x' w \Psi_{i} 
\end{align}
where
\begin{align}
w = 
\left( 
\begin{array}{cccc}
w_1 &0& \cdots &0\\
0 & w_2 & \cdots &0\\
\vdots &\vdots &\vdots &\vdots \\
0 &0 &0  &w_m
\end{array}
\right)
\end{align}
and
\begin{equation}
\Psi_{i}= 
\left( 
\begin{array}{c}
\frac{\partial \rho_1}{\partial  u_{1i}}\\
\\
\frac{\partial \rho_2}{\partial u_{2i}}\\
\vdots\\
\frac{\partial \rho_m}{\partial  u_{mi}}
\end{array}
\right)
\end{equation}
Thus
\begin{align}
\nabla_{\text{vec}[\theta]} \Phi= 
 \left( \begin{array}{c} x' w \Psi_1\\ \vdots\\ x'w \Psi_c\end{array}\right) 
\end{align}
\subsection{Hessian Matrix}
The Hessian of a scalar $v$ with respect to a vector $u$ is defined as  follows
\begin{align}
 \nabla_u^2 v = \nabla_u \Big(\nabla_u v\Big) = \frac{\partial}{\partial u}  \Big(\nabla_u v\Big)'
\end{align}
Thus
\begin{align}
\nabla^2_{\text{vec}[\theta]} \Phi&= 
\nabla_{\text{vec}[\theta]} 
 \left( \begin{array}{c} x' w \Psi_1\\ \vdots\\ x'w \Psi_c\end{array}\right) \\
&=
  \left( \begin{array}{ccc}  \nabla_{\text{vec}[\theta]} x' w  \Psi_1,&  \cdots&  ,x' w \Psi_c\end{array}\right) 
\end{align}
where
\begin{align}
\nabla_{\text{vec}[\theta]} x' w  \Psi_i&=
\Big(\nabla_{\text{vec}[\theta]} \Psi_i\Big) \nabla_{\Psi_i} \Big(x' w\Psi_i \Big)\nonumber\\
&= (\nabla_{\text{vec}[\theta]} \Psi_i ) w x
\end{align}
Thus 
\begin{align}
\nabla^2_{\text{vec}[\theta]} \Phi&= 
 \Big( \begin{array}{ccc} (\nabla_{\text{vec}[\theta]}  \Psi_1) wx, &\cdots,&(\nabla_{\text{vec}[\theta]}   \Psi_c)wx \end{array}\Big) 
\end{align}

\begin{align}
\nabla^2_{\text{vec}[\theta]} \Phi&= 
 \left( \begin{array}{ccc} \left(\begin{array}{c} \nabla_{\theta_{\cdot 1}} \Psi_1\\\vdots\\\nabla_{\theta_{\cdot c}} \Psi_1 \end{array}  \right)   wx, &\cdots,&
 \left(\begin{array}{c} \nabla_{\theta_{\cdot1}} \Psi_c\\\vdots\\\nabla_{\theta_{\cdot c}} \Psi_c \end{array}  \right)wx \end{array}\right)\\
&= \left( \begin{array}{ccc} \begin{array}{c} (\nabla_{\theta_{\cdot1}} \Psi_1) wx\\\vdots\\(\nabla_{\theta_{\cdot c}} \Psi_1) wx \end{array}  &\cdots&
 \begin{array}{c} (\nabla_{\theta_{\cdot1}} \Psi_c) wx \\\vdots\\(\nabla_{\theta_{\cdot c}} \Psi_c) wx \end{array} \end{array}\right)
\end{align}

Note
\begin{align}
\nabla_{\theta_{\cdot i}} \Psi_j= \nabla_{\theta_{\cdot i}} u_{\cdot j} \nabla_{u_{\cdot j}} \Psi_j
= x' \Lambda_{ij}
\end{align}

where
\begin{align}
\Lambda_{ij} =\nabla_{u_{\cdot j}} \Psi_j=  
\left(\begin{array}{cccc}
\frac{\partial \Psi_{1j}}{\partial u_{1i}} &0 &\cdots &0\\
0&\frac{\partial \Psi_{2j}}{\partial u_{2i}} &\cdots &0\\
\vdots &\vdots &\vdots &\vdots\\
0&0&\cdots&\frac{\partial \Psi_{mj}}{\partial u_{mi}}
\end{array}
\right)
\end{align}

Thus



\begin{align}
\nabla_{\theta_{\cdot i}} \Psi_j = \frac{\partial \Psi_{j}' }{\partial \theta_{\cdot i}}=
\left(\begin{array}{ccc}
x_{11}\frac{\partial \Psi_{1j}}{\partial u_{1i}} &\cdots &x_{m1}\frac{\partial \Psi_{mj}}{\partial u_{mi}}\\
\vdots &\vdots &\vdots\\
x_{1n}\frac{\partial \Psi_{1j}}{\partial u_{ni}} &\cdots &x_{mn}\frac{\partial \Psi_{mj}}{\partial u_{mi}}\\
\end{array}
\right)
= x' \Lambda_{ij}
\end{align}
where
\begin{align}
\Lambda_{ij} =
\left(\begin{array}{cccc}
\frac{\partial \Psi_{1j}}{\partial u_{1i}} &0 &\cdots &0\\
0&\frac{\partial \Psi_{2j}}{\partial u_{2i}} &\cdots &0\\
\vdots &\vdots &\vdots &\vdots\\
0&0&\cdots&\frac{\partial \Psi_{mj}}{\partial u_{mi}}
\end{array}
\right)
\end{align}
Thus

\begin{align}
 \nabla^2_{\text{vec}[\theta]}\Phi =
\left(\begin{array}{ccc}
x' \Lambda_{11} wx &\cdots& x' \Lambda_{1c} w x\\
\vdots&\cdots &\vdots\\
x' \Lambda_{c1} wx &\cdots& x' \Lambda_{cc} w x
\end{array}
\right)
\end{align}

\section{Quadratic Priors}
Let
\begin{align}
\Gamma(\theta) = - \frac{1}{2} (\text{vec}[\theta] - \text{vec}[\mu])' \sigma^{-1} (\text{vec}[\theta] -\text{vec}[\mu])'
\end{align}
then
\begin{align}
\nabla_{\text{vec}[\theta]} \Gamma(\theta) = -\sigma^{-1} (\text{vec}[\theta] - \text{vec}[\mu])
\end{align}
\begin{align}
\nabla^2_{\text{vec}[\theta]} = \nabla_{\text{vec}[\theta]} \nabla_{\text{vec}[\theta]} 
= - \sigma^{-1}
\end{align}
\section{ Newton-Raphson Optimization }
\begin{equation}
\text{vec} [\theta^{(k+1)}]  =   \text{vec}[\theta^{(k)}] - \left( \nabla^2_{\text{vec}[\theta]} L(\theta^{(k)} \right)^{-1} \nabla_{\text{vec}[\theta]} L(\theta^{(k)})
\end{equation}
where $\theta^{(k)}$ is the value of $\theta$ after $k$ iterations of the optimization algorithm. 


\section{Multivariate Linear Regression}
In this case
\begin{align}
&\hat y_{i\cdot} = u_{i\cdot}\\
&\rho_i = \sum_k (\hat y_{ik} - y_{ik})^2
\end{align}
Thus
\begin{align}
&\Psi_i = \hat y_{\cdot i} - y_{\cdot i}\\
&\Lambda_{ij} = I_m
\end{align}
where $I_m$ is the $m\times m$ identity matrix. 
Thus 
\begin{align}
\frac{\partial }{\partial \theta_{\cdot i}} \frac{\partial \Phi}{\partial \theta'_{\cdot j}} = x'wx
\end{align}
\section{Multinomial Logistic Regression}
Let 
\begin{equation}
\hat y_{ij} = f_j(u_{i\cdot}) =
\frac{e^{u_{ij}}}{\sum_{k=1}^c  e^{u_{ik}}} 
\end{equation}
and $\rho_i$ the negative log-likelihood of the output vcector $y_{i\cdot}$ given the input vector $ x_{i\cdot}$
\begin{equation}
\rho_i =-
\sum_{k=1}^c  y_{ik} \log \hat y_{ik} 
\end{equation}
Note 
\begin{equation}
\frac{\partial \hat y_{ij} }{\partial u_{ik}}
= \hat y_{ij} \frac{\partial \log \hat y_{ij} }{\partial u_{ik}}
=
\hat y_{ij}  (\delta_{jk} - \hat y_{ik})
\end{equation}
Thus
\begin{equation}
\frac{\partial \rho_i }{\partial u_{ik}}
= -\sum_{j=1}^c \frac{y_{ij}}{\hat y_{ij}}
\frac{\partial \hat y_{ij}}{u_{ij}}
= \sum_{j=1}^c y_{ij} (\delta_{jk} - \hat y_{ik})
= \hat y_{ik} -  y_{ik}
\end{equation}
where we used the fact that $\sum_j y_{ij} =1$. Thus

\begin{equation}
\Psi_{i}= 
\left( 
\begin{array}{c}
\frac{\partial \rho_1}{\partial  u_{1i}}\\
\\
\frac{\partial \rho_2}{\partial u_{2i}}\\
\vdots\\
\frac{\partial \rho_m}{\partial  u_{mi}}
\end{array}
\right)
= 
\left( 
\begin{array}{c}
y_{1i} -\hat y_{1i}\\
\\
y_{2i} -\hat y_{2i}\\
\vdots\\
y_{mi} -\hat y_{mi}\\
\end{array}
\right) = \hat y_{\cdot i} -  y_{\cdot i}
\end{equation}
Moreover
\begin{align}
\Lambda_{ij} =
\left(\begin{array}{cccc}
\frac{\partial \Psi_{1j}}{\partial u_{1i}} &0 &\cdots &0\\
0&\frac{\partial \Psi_{2j}}{\partial u_{2i}} &\cdots &0\\
\vdots &\vdots &\vdots &\vdots\\
0&0&\cdots&\frac{\partial \Psi_{mj}}{\partial u_{mi}}
\end{array}
\right)
\end{align}
where
\begin{align}
\frac{\partial \Psi_{kj}}{\partial  u_{ki}}
=  \frac{\partial \hat y_{kj}}{\partial  u_{ki}}
=  \hat y_{kj}( \delta_{ij} - \hat y_{ki})
\end{align}

\subsection{Relationship to Linear Regression}
Note that the gradient in multinomial logistic regression is identical to the gradient in multivariate linear regression. 
\begin{align}
\nabla_{\theta_{\cdot i}} \Phi = \hat y_{\cdot i} - y_{\cdot i}
\end{align}
The Hessians would are also very simmilar. In linear regression
\begin{align}
\frac{\partial}{\partial \theta_{\cdot i}} \frac{\partial \Phi}{\partial \theta'_{\cdot j}} = x'w x
\end{align}
and in logistic regression 
\begin{align}
\frac{\partial}{\partial \theta_{\cdot i}} \frac{\partial \Phi}{\partial \theta'_{\cdot j}} = x'w\Lambda_{ij} x
\end{align}
which can be seen as special case of linear regression where the weight matrix $w$ is substituted by the $w\Lambda_{ij}$ matrix.
\subsection{Summary}

\begin{itemize}
\item{\bf Training Data}
\begin{itemize}
\item{\bf Inputs}  $x \in \R^m \times R^n$. Rows are examples, columns are input variables.
\item {\bf Outputs}  $y \in \R^m \times  R^c$. Rows are examples, columns are labels or label probabilities. All entries are non-negative and each row add up to 1. 
\item {\bf Example Weights}  $w \in \R^m \times  R^r$. Diagonal matrix. Each term is positive and represents the relative importance of each example. 
\end{itemize}

\item{\bf Gradients}
\begin{align}
\nabla_{\text{vec}[\theta]} L = 
\left( 
\begin{array}{c}
\nabla_{\theta_{\cdot 1}} \Phi \\
\nabla_{\theta_{\cdot 2}} \Phi \\\\
  \vdots\\
 \nabla_{\theta_{\cdot c}} \Phi  
\end{array}
\right)
+
\nabla_{\text{vec}[\theta]} \Gamma
\end{align}
where
\begin{align}
&\nabla_{\theta_{\cdot i}} \Phi =  \frac{\partial \Phi'}{\partial  \theta_{\cdot i}}
= x' w \Psi_{i} 
\end{align}
and
\begin{equation}
\Psi_{i}= 
\left( 
\begin{array}{c}
\frac{\partial \rho_1}{\partial  u_{1i}}\\
\\
\frac{\partial \rho_2}{\partial u_{2i}}\\
\vdots\\
\frac{\partial \rho_m}{\partial  u_{mi}}
\end{array}
\right) = \hat y_{\cdot i} - y_{\cdot i}
\end{equation}
and
\begin{align}
\nabla_{\text{vec}[\theta]} \Gamma = -\sigma^{-1} (\text{vec}[\theta] - \text{vec}[\mu])
\end{align}

\item{\bf Hessian Matrices}
\begin{align}
\nabla^2_{\text{vec}[\theta]} L = 
\nabla^2_{\text{vec}[\theta]} \Phi +\nabla^2_{\text{vec}[\theta]} \Gamma 
\end{align}
\begin{align}
\nabla^2_{\text{vec}[\theta]} \Phi= 
\left(\begin{array}{ccc}
\frac{\partial }{\partial \theta_{\cdot 1}} \frac{\partial \Phi}{\partial \theta'_{\cdot 1}} &\cdots
&\frac{\partial }{\partial \theta_{\cdot 1}} \frac{\partial \Phi}{\partial \theta'_{\cdot c}}\\
\vdots &\vdots &\vdots\\
\frac{\partial }{\partial \theta_{\cdot c}} \frac{\partial \Phi}{\partial \theta'_{\cdot 1}} &\cdots
&\frac{\partial }{\partial \theta_{\cdot c}} \frac{\partial \Phi}{\partial \theta'_{\cdot c}}
\end{array}
\right)
\end{align}
where
\begin{align}
\frac{\partial }{\partial \theta_{\cdot i}} \frac{\partial \Phi}{\partial \theta'_{\cdot j}} = x' \Lambda_{ij} w x
\end{align}
where 
\begin{align}
\Lambda_{ij} =
\left(\begin{array}{cccc}
\frac{\partial \Psi_{1j}}{\partial u_{1i}} &0 &\cdots &0\\
0&\frac{\partial \Psi_{2j}}{\partial u_{2i}} &\cdots &0\\
\vdots &\vdots &\vdots &\vdots\\
0&0&\cdots&\frac{\partial \Psi_{mj}}{\partial u_{mi}}
\end{array}
\right)
\end{align}
and
\begin{align}
\frac{\partial \Psi_{kj}}{\partial  u_{ki}}
=  \frac{\partial \hat y_{kj}}{\partial  u_{ki}}
=  \hat y_{kj}( \delta_{ij} - \hat y_{kj})
\end{align}
and

\begin{align}
\nabla^2_{\text{vec}[\theta]}  \Gamma = -\sigma^{-1}
\end{align}




\item{\bf Learning Rule (Newton-Raphson) }
\begin{equation}
\text{vec} [\theta^{(k+1)}]  =   \text{vec}[\theta^{(k)}] - \left( \nabla^2_{\text{vec}[\theta]} L(\theta^{(k)} \right)^{-1} \nabla_{\text{vec}[\theta]} L(\theta^{(k)})
\end{equation}
where $\theta^{(k)}$ is the value of $\theta$ after $k$ iterations of the learning rule. 

\end{itemize}

\section{Appendix: L-p priors}


From a Bayesian point of view the $\Phi$ function can be seen as a  
log-likelihood term. At times it may be useful to add a log prior term
over $\theta$, also known as a regularization term.  Most prior terms
of interest have the following form

\begin{align}
\Gamma = \alpha \sum_{i=1}^n \sum_{j=1}^c \frac{1}{p} \Big(h(\theta_{i,j})\Big)^p
\end{align}
where $\alpha$ is a non-negative constant. If $h$ is the absolute
value function then $\Gamma$ is the $L$-$p$ norm of $\theta$. Two
popular options are $p=2$ and $p=1$. Note the absolute value function
is not differentiable, and thus when $p$ is odd, it useful to
approximate it with a differentiable function. Here we will use the
log hyperbolic cosine function. 
\begin{align}
h(x) = \frac{1}{\beta} \log(2 \text{cosh}(\beta x))
\end{align}
where $\beta \geq 0$ is a gain parameter and
\begin{align}
\text{cosh}(x) = \frac{e^{x} + e^{-x}}{2}
\end{align}
 Note
\begin{align}
\lim_{\beta \to \infty} h(x)=  \text{abs}(x)
\end{align}
\begin{align}
h'(x) = \frac{d h(x)}{dx } =   \frac{ e^{\beta x} - e^{-\beta x} }{e^{\beta x} + e^{-\beta x}}= \text{tanh}(x) 
\end{align}
\begin{align}
\lim_{\beta \to \infty} \frac{d h(x)}{dx } = \text{sign}(x)
\end{align}
and 
\begin{align}
&h''(x) = \frac{d^2 h(x)}{dx^2} = \frac{d \;\text{tanh}(\beta x)}{d x} = \beta\; ( 1 - \text{tanh}^2(\beta x)) 
\end{align}


\begin{figure}[h]
\begin{center}
\includegraphics[width=.9\textwidth]{Media/logCosh.pdf}
\caption{{\it The absolute value function can be approximated by the log of a hyperbolic cosine. The derivatie of the absolute value is the sign function. It can be approximated by the derivative of the log cosh function, which is the hyperbolic tangent.  In this figure we used gain $\beta = 4$.  }} \label{fig:tanh}
\end{center}
\end{figure}

\subsection{Gradient and Hessian}

\begin{align}
\frac{\partial \Gamma}{\partial \theta_{ij}} &= \alpha \Big(h^{p-1}(\theta_{ij})\;  h'(\theta_{ij}) \Big)\\
&=  \alpha \;\Big((\frac{1}{\beta}\log(2 \text{cosh}(\beta x)))^{p-1} \;\text{tanh}(x)  
\Big)
\end{align}


The $nc \times nc$ Hessian matrix is diagonal. Each term in the diagonal has the following form
\begin{align}
\frac{\partial^2 \Gamma}{\partial \theta_{ij}^2}
= \alpha \Big((p-1) h(\theta_{ij})^{p-2} \;( h'(\theta_{ij}))^2 +  h(\theta_{ij})^{p-1} h''(\theta_{ij})  \Big)
\end{align}

Thus
\begin{align}
\frac{\partial^2 \Gamma}{\partial \theta_{ij}^2}
&= \alpha \Big((p-1) 
(\frac{1}{\beta}\log(2 \text{cosh}(\beta x)))^{p-2} \; (\text{tanh}(\beta x))^2\\
&\;\;+ (\frac{1}{\beta}\log(2 \text{cosh}(\beta x)))^{p-1} \; \beta \; (1 - \text{tanh}^2(\beta x) \Big)
\end{align}



Note for $p=1$, we get
\begin{align}
&\frac{\partial \Gamma}{\partial \theta_{ij}} = \alpha \frac{1}{\beta}\; \text{tanh}(\beta \theta_{ij})\\
&\frac{\partial^2 \Gamma}{\partial \theta_{ij}^2} = \alpha \;\beta \;(1 - \text{tanh}^2(\beta \theta_{ij}))\\
\end{align}
and for $p=1$, and  $\beta \to \infty$  we get
\begin{align}
&\frac{\partial \Gamma}{\partial \theta_{ij}} = \alpha\; \theta_{ij}\\
&\frac{\partial^2 \Gamma}{\partial \theta_{ij}^2} = \alpha 
\end{align}



\bibliographystyle{apa}
\bibliography{movellan.mypapers}

\end{document}
