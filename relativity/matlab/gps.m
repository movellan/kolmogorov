clear
v = 14000; % tangential velocity of satelite in Km/h
v = 14000/60/60; % velocity in Km/sec

v=v/2
c = 299792.458; % speed of light in Km sec
beta = v/c;

gamma = 1/sqrt(1-beta^2);

% delta t in microseconds (1/1000000 sec)
24*60*60*60*(gamma-1)*10000000 