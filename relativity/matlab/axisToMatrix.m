function r = axisToMatrix(v)
% function r = axisToMatrix(v)
% get matrix r that rotates  |v| radians about the axis of rotation
% vector v

% input is a 3d vector v. The lenght of v is the angle of rotation
% in radians. The axis of rotation is v


theta = norm(v);
if theta==0
  r=eye(3);
else
  
u = v/theta;
r = eye(3) + sin(theta)*R(u)+ (1- cos(theta))*R(u)*R(u);
end

function y = R(x)

y=[0, -x(3), x(2); x(3),0,-x(1);-x(2),x(1),0];
