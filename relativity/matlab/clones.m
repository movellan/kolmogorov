% simulation of the clone profs problem

beta = rand(1); 
tau =1;
c=1;

p = [c *tau; 0];
qPrime=[c*tau;0];

gamma = 1/sqrt(1 - beta^2);

lambda=[gamma, - gamma*beta; - gamma*beta, gamma];
lambdaInv= [gamma,  gamma*beta; gamma*beta, gamma];

pPrime= lambda*p;
q = lambdaInv*qPrime;
delta = q -p;
deltaPrime = qPrime-pPrime;

m = [ 1,0; 0, -1];

s2 = delta'*m*delta;
s2Prime = deltaPrime'*m*deltaPrime;