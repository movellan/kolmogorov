function l=lTransform(v,c)


beta = v/c;
gamma = 1/sqrt(1 - beta'*beta/c^2);

l = zeros(4);
l(1,1) = gamma;
l(1,2:4) = -gamma*beta';

l(2:4,1) = -gamma*beta;
l(2:4,2:4) = eye(3) +(gamma-1)*beta*beta'/(beta'*beta);
