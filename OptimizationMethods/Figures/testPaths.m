% Test paths given by gradient descent and by Newton Raphson
function dummy()
clear all
clf
a = [ 1 2 ; 2  1];
i=0;
for x1=-1:0.01:1
  j=0;
  i = i+1;
  for x2=-1:0.01:1
    j = j+1;
    y(j,i)  = [ x1 x2] *a'*a *[x1 x2]';
  end
end
contour(y,100)
hold on



x(:,1) = [ -0.5 1.2]'/2;
x(:,1) = [ 1.4 -0.9]'/2;
y1(:,1) = a*x(:,1);
g(:,1) = x(:,1);
eps = 0.01;
dg =0;
dx =0;
dx2=0;
dg2=0;
a

for t = 2:500
  x(:,t) = x(:,t-1) -  eps* a'*a*x(:,t-1);
  y1(:,t) = a*x(:,t);
  ex = x(:,t) - x(:,t-1);
  dx = dx+ sqrt(ex'*ex);
  dx2 = dx2+ sqrt(ex'*a'*a*ex);
  g(:,t) = g(:,t-1) -  eps* g(:,t-1);
  y2(:,t) = a*g(:,t);
  eg = g(:,t) - g(:,t-1);
  dg = dg+ sqrt(eg'*eg);
  dg2 = dg2+ sqrt(eg'*a'*a*eg);
end

dx
dg
dx2
dg2

x = convert(x);
g = convert(g);
y1 = convert(y1);
y2 = convert(y2);
plot(x(1,:), x(2,:),'r--','LineWidth',2)

 plot(g(1,:), g(2,:),'b','LineWidth',2)

% plot(y1(1,:), y1(2,:),'r--','LineWidth',2)

 %plot(y2(1,:), y2(2,:),'b','LineWidth',2)
set(gca, 'Xtick', []);
set(gca, 'Ytick', []);
 
function y = convert(x)
min = -1;
max = 1;
n = 201;
y = zeros(size(x));
for i=1:2
  y(i,:)  = (x(i,:) - min)./(max-min)*n;
end
