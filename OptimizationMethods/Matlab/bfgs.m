% Quasi Newton BFGS algorithm to minimize a function f
% 
% Input: 
%        f = objective function,
%        should be of form [fc,gc]=f(x) where fc=f(x) is a scalar
%              and gc = grad f(x) is a column vector
%        x0 = initial state
%        tol. Tolerance. algorithm stops when root mean square of change
%        in  parameter vector is less than tol 
%
% Output: 
%       amin: argument that minimizes f
%       fmin: minimum value of f
%       B: BFGS estimate of the inverse hessian
%       fcalls: number of function calls
%
% Copyright Javier R. Movellan
% Licensed under open source MIT style license 
%
function [amin fmin,B,fcalls]  = bfgs(f, x0,tol)

fcalls =0;  
% First we initialize the Hessian estimate.
% To do so we take a very small step in the direction of the
% gradient

epsilon = 0.000000000000001;; % Very small initial step size

[ f0, g0] = feval(f,x0); fcalls = fcalls+1;
x1 = x0 - epsilon*g0;
[ f1, g1] = feval(f,x1); fcalls = fcalls+1;
if (f1 > f0)
  display('My initial step size was too large')
  return
end

p = x1 - x0;
q = g1 - g0;
epsilon = (p'*p)/(p'*q) % Optimal step size in the direction of the
                         % gradient assuming quadratic model
keyboard
n = length(x0);
I = eye(n);
B = epsilon*I;  % Initial estimate of the inverse Hessian

 
xc = x0;
wolfeFlag =1;

k =1;

fVal(1) = f0;
dx =1; % keeps track of change in parameters

while wolfeFlag > 0 && dx > tol
  fVal(k)
  k= k+1
  
  [fc,gc]=feval(f,xc); fcalls = fcalls+1;
  d = -B*gc; % descent direction
    display('Entering LineSearch');
    [ xc, yc, wolfeFlag,ncalls] = lineSearch('quadraticFunction',xc,d);
    
    
    ncalls
    display('Calls made by LineSearch');

    fcalls = fcalls+ ncalls;
    [fc,gc]=feval(f,xc); fcalls = fcalls+1;
    fVal(k) = yc;
    
    dx = norm(xc -x0)/n 
    if wolfeFlag >0   % We only continue if Line Search could satisfy the 
                      % Wolfe Conditions
                      % We may want to change this and have some provisos
                      % for the case in which linesearch fails 
      
      p = xc - x0;
      q = gc - g0;
      gamma = 1/(p'*q);
      if( p'*q > p'*B*p/1000000) % standard numerical safegard       
        B = (I - gamma*p*q')*B;
        B = B*(I - gamma*q*p');
        B = B+ gamma*p*p';
      else
        display('Numerical Safegard: Did not apply BFGS update')
      end
      x0 = xc;
      g0 = gc;
    end
end
plot(fVal)
amin = xc;
fmin = fc;