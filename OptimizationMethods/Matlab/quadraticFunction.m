function [ y, gy] = quadraticFunction(x)
% TODO: Modify so it does not compute gradient if not required
n= length(x);
%a = diag([1:n]);
%x0 = [ 1:n]';
 a= diag(n:-1:1);
 
x0 = zeros(n,1);

y = (x-x0)'*a*(x-x0)/2 ;
gy = (a*(x-x0))/(y+0.1);
y= log(y+0.1)- log(0.1);





