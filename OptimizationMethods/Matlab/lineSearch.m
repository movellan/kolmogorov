% Given a function f, a state x0, and a direction d find a stepisze that
% satisfies the Wolfe conditions for minimizing f 

% Input:
%        f = objective function,
%              should be of form [fc,gc]=f(x) where fc=f(x) is a scalar
%        x0 = initial state
%        d= descent direction
%
% Output: x1 = state that satisfies Wolfe conditions
%         y1 = f(x1)
%         flag =1 if Wolfe conditions were satisfied
%         flag =-1 if we could not satisfy the Wolfe conditions
%         fcalls = number of function calls
% Copyright Javier R. Movellan
% Licensed under open source MIT style license 
%
function  [x1, y1, flag,fcalls] = lineSearch(f, x0,d)
  alpha = 1;  % initial step size
  c1 = 1/10000; % Typical parameter for wolfe conditions
  c2 = 0.9; % Typical parameter for wolfe conditions  
  beta2 = 0.5; % shrink factor 

  
  
  
  alpha = alpha/beta2;
  wolfe=0;
  fcalls =0;
  [ f0, g0] = feval(f,x0); fcalls= fcalls+1;
  dg0 = d'*g0;
  t =0;
  nIter=10;
  while wolfe <1  && t <nIter
    t=t+1;
     if t == floor(nIter/2)  % If after nIter/2 trials reducing alpha we do not
                             % satisfy the
              % Wolf Conditions try 10 more trials increasing alpha
      alpha = 1;
      beta2=2;
    end
    
    alpha = alpha*beta2;
    
    x1 = x0 + alpha*d;
    [ f1, g1] = feval(f,x1); fcalls=fcalls+1; 
    
    if f1 < f0 + c1*alpha*dg0; % armijo sufficient descent condition
      if  d'*g1 > c2* d'*g0; % curvature condition
        wolfe =1;
        y1 = f1;
        flag =1;
          return 
        end
    end
  end
% Here we handle the case in which we could not satisfy the Wolfe conditions
x1 = x0;
y1 = f0;
flag = -1;