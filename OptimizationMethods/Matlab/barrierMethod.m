% Illustrates barrier method for constrained optimization

% target: minimize f
% f(x) = (x1-6)^2 + (x2 - 7)^2
% subject to constraints
% g1(x) = -3x1 - 2 x2 + 6 \leq 0
% g2(x) = -x1+x2-3 \leq 0
% g3(x) = x1+x2 -7 \leq 0
% g4(x) = 2/3 x1 - x2 - 4/3 \leq 0


% Here we illustrate the case where we search through a sequence of
% points outside  the feasibility region

% We augment the original objective by adding a barrier function

% h(x,c) = f(x) +  c \sum_{i=1}^m 0.5* max(0, gi(x))^2 


% this barrier function for  points outside the feasible region
clear all
clf
epsilon = 0.001;

c=200;
x = [ 3 4  ]';
nabla = [ 0 0]';
grow = 1.001;
for k=1:1000
  c = c*grow;
  nabla(1) = 2 *(x(1) - 6);
  nabla(2) = 2* (x(2) - 7);
 % nabla = [ 0 0 ]';
  g(1) = -3*x(1) -2 *x(2) +6;
  g(2) = -x(1)+ x(2) -3;
  g(3) = x(1)+x(2) -7;
  g(4) = 2*x(1)/3- x(2) - 4/3;
  for i =1:4
    if g(i) <0
      g(i) =0;
    end
    
  end
  
  
  if g(1) >0
    nabla(1) = nabla(1)  +2*c*g(1)*(-3);
    nabla(2) = nabla(2) +2*c*g(1)*(-2);
  end
  
  if g(2) >0
    nabla(1) = nabla(1)  +2*c*g(2)*(-1);
    nabla(2) = nabla(2) +2*c*g(2)*(1);    
  end
  
  if g(3) >0
    nabla(1) = nabla(1)  +2*c*g(3)*(1);
    nabla(2) = nabla(2) +2*c*g(3)*(1);
  end
  
  if g(4) >0
    nabla(1) = nabla(1)  +2*c*g(4)*(2/3);
    nabla(2) = nabla(2) +2*c*g(4)*(-1);
g
  end



  
  F(k) =  (x(1)-6)^2 + (x(2) - 7)^2+ c*sum(g.^2);
  G(k) = x(1);
  x = x - epsilon*nabla;  
  
end

plot(F)


