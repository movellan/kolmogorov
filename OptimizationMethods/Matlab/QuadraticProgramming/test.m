n=3;

h = randn(n);
h = h*h';
f = randn(n,1);
% find x that mins 0.5*x'*h*x + f'*x
x = quadprog(h,f)


a= -eye(n);
b= zeros(n,1);
% find x that mins 0.5*x'*h*x + f'*x 
% subject to x \geq 0

x = quadprog(h,f,a,b)

% find x that mins 0.5*x'*h*x + f'*x 
% subject to x \geq 0
% and sum x =1

a2 =ones(1,n);
b2=1

x = quadprog(h,f,a,b,a2,b2)