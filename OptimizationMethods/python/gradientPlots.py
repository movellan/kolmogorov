#simulate stochastic gd as ou process

from numpy import *
from numpy.random import *
from matplotlib.pyplot import *

x,y = meshgrid( arange(-1,1.1,0.02),arange(-1,1.1,0.02) )
r=25
a = array([[1.,r],[r,1.]])
a = dot(a,a.T)
a = eye(2)
a[0,0]=r

u = zeros(shape(x))
v = zeros(shape(x))
rho = zeros(shape(x))
cosAngle=[]
for i in range(len(x)):
    for j in range(len(y)):
        z = array([x[i,j],y[i,j]])
        gr= dot(a,z)
        zNorm= sqrt(dot(z.T,z))
        grNorm=sqrt(dot(gr.T,gr))
        if (zNorm*grNorm>0.):
        	cosAngle.append(dot(z.T,gr)/(zNorm*grNorm))
        u[i,j]=-gr[0]
        v[i,j]=-gr[1]
        rho[i,j]= dot(z.T,gr)
        m =sqrt(u**2.+v**2.)
lw = 5.*m/m.max()
streamplot(x, y, u, v, color='k', linewidth=lw, cmap=get_cmap('Greys'))
contour(x, y, rho,40)
plot([-0.05,0.05],[0,0],'r',lw=2)
plot([0,0],[-0.05,0.05],'r',lw=2)
axes().set_aspect('equal')
cosAngle=array(cosAngle)
show()
