import numpy as np
from  scipy.linalg import *

x = np.random.randn(100,2)
a  =  np.random.randn(2,2)
aT =a.transpose()

c = np.matmul(a,aT)

s =np.sqrt(inv(np.diag(np.diag(c))))

sc = np.matmul(s,np.matmul(c,s))
ec=eig(c)
esc=eig(sc)

rc =ec[0][0]/ec[0][1]
rsc =esc[0][0]/esc[0][1]

print rc, rsc
