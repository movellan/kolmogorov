% Copyright \copyright{} 1996,,1998, 2002 Javier R. Movellan. 
% This is an open source document.  Permission is granted
% to copy, distribute and/or modify this document under the terms of the
% GNU Free Documentation License, Version 1.1 or any later version
% published by the Free Software Foundation; with no Invariant Sections,
% no Front-Cover Texts, and no Back-Cover Texts.






\chapter{High Order Designs}
 


In practice most experimental designs include more than 2 factors. The
basic ideas for high order factorial designs are the same as in 2
factor designs but some new concepts appear.  Consider a case in which
we have 3 factors $A$, $B$, and $C$. The number of treatment levels on
each factor is represented as $a$, $b$, and $c$, and the number of
subjects per group as $n$. The population mean of subjects treated
with the combination of $A_iB_jC_k$ is represented as
$\mu_{A_iB_jC_k}$. In this case we can have the following types of
effects:

% Need to make this factors meaningfull with an experiment.

\begin{itemize}


\item {\bf Main effect} of treatment $A_i$ is the deviation of the
population mean of all groups treated with treatment $A_i$ from the
overall population mean of all the groups

\begin{equation}
\delta_{A_i} = \mu_{A_i} - \mu
\end{equation}


\item {\bf Conditional main effect} of treatment $A_i$ given $B_j$ is
equivalent to the definition of main effect but restricted only to
groups that receive $B_j$. In such case the overall mean becomes
$\mu_{B_j}$ and the mean of groups that also receive $A_i$ becomes
$\mu_{A_iB_j}$, thus

\begin{equation}
\delta_{A_i|B_j} = \mu_{A_iB_j} - \mu_{B_j}
\end{equation}

Similarly we can condition the effect of $A_i$  to groups that receive a combination treatment $B_jC_k$, for example,

\begin{equation}
\delta_{A_i|B_jC_k} = \mu_{A_iB_jC_k} - \mu_{B_jC_k}
\end{equation}

\item {\bf Joint effect} of treatment $A_i$, $B_j$, $C_k$ is the
deviation between the population mean of the group treated with this
combination of treatments, and the overall population mean,

\begin{equation}
\delta_{A_iB_jC_k} = \mu_{A_iB_jC_k} - \mu
\end{equation}

 
\item {\bf Average joint effects} of treatment $A_i$ and $B_j$ is the
deviation of the population mean of all group jointly treated with
$A_iB_j$ from the overall population mean of all groups

\begin{equation}
\delta_{A_iB_j} = \mu_{A_iB_j} - \mu
\end{equation}

\item {\bf Conditional joint effects} of treatment $A_i B_j$
given $C_k$ is equivalent to the definition of average main effect but
restricted only to groups that receive $C_k$. In such case the overall
mean becomes $\mu_{C_k}$ and the mean of groups that also receive
$A_iB_j$ becomes $\mu_{A_iB_jC_k}$, thus

\begin{equation}
\delta_{A_iB_j|C_k} = \mu_{A_iB_jC_k} - \mu_{C_k}
\end{equation}


\item {\bf Two way interaction effects} capture the difference between
main effects and conditional main effects. For example,

\begin{equation}
\delta_{A_i \times B_j} = \delta_{A_i|B_j} - \delta_{A_i}
\end{equation}

If the two way interaction effect between $A_i$ and $B_j$ is zero then
the average effect of $A_i$ is the same for all treatments
$B_j$. Similarly we can define the two way interaction between $A$ and
$C$ and the two way interaction between $B$ and $C$.

\begin{equation}
\delta_{A_i \times C_k} = \delta_{A_i|C_k} - \delta_{A_i}
\end{equation}
and
\begin{equation}
\delta_{B_j \times C_k} = \delta_{B_j|C_k} - \delta_{B_j}
\end{equation}

\item {\bf Conditional two way interaction} effects measure whether
there are two way interactions when restricted to particular
treatments of the other factor. For example,

\begin{equation}
\delta_{A_i \times B_j|C_k} = \delta_{A_i|B_jC_k} - \delta_{A_iC_k}
\end{equation}

measures to what degree $A_i$ has the same effect in all treatments of
factor $B_j$ when restricted to the set of groups that have treatment
$C_k$.


\item {\bf Three way interaction effects} measure whether two way
interaction effects are the same for all treatments of the other
factor.

\begin{equation}
\delta_{A_i \times B_j \times C_k} = \delta_{A_i \times B_j|C_k} -
\delta_{A_i \times B_j}
\end{equation}

It is easy to verify that the formula above is equivalent to the following

\begin{equation}
\delta_{A_i \times B_j \times C_k} = \delta_{B_j \times C_k|A_i} -
\delta_{B_j \times C_k} = 
 \delta_{A_i \times C_k|B_j} -
\delta_{A_i \times C_k}
\end{equation}

Thus three way interactions effects us that the interactions look
different for the different levels of the other factor.

\item {\bf Two-way single-joint interaction} effects measure whether
the main effect of a treatment is the same to the effect of a
treatment restricted to subjects that have a combination of
treatments. For example,

\begin{equation}
\delta_{A_i\times(B_jC_k)} = \delta_{A_i|B_jC_k} - \delta_{A_i}
\end{equation}

It is easy to show that main-joint interactions decompose as follows,

\begin{equation}
\delta_{A_i\times(B_jC_k)} = \delta_{A_i\times B_j} + \delta_{A_i\times C_k}
+ \delta_{A_i \times B_j \times C_k}
\end{equation}


\end{itemize}

\section{Decomposition of joint effects}


Amongst all the effects described above, the most useful and commonly used
are the main effects, two-way interaction effects, and the three-way
interaction effects. These effects are particularly important because
the joint effect of any combination of treatments can be decomposed as
the sum of main effects and interaction effects. To see why, first  note
that the joint effects can be decomposed into main effects and
conditional effects

\begin{equation}
\delta_{A_iB_jC_k} = \delta_{A_iB_j} + \delta_{C_k|A_iB_j}
\end{equation}

The equation above simply tells us that a joint effect is the sum of
the effect of $A_iB_j$ plus the effect of $C_k$ on groups that receive
a combination of $A_i$ and $B_j$.


From 2-factor designs we know that the 2-way joint effects can be
decomposed into main effects and interactions,
\begin{equation}
\delta_{A_iB_j} = \delta_{A_i} + \delta_{B_j} + \delta_{A_i \times B_j}
\end{equation}

Moreover, from the definition of three way interaction, we know that

\begin{equation}
\delta_{C_k|A_iB_j} = \delta_{A_i\times (B_j  C_k)} + \delta_{C_k}
\end{equation}

And from the definition of single-joint interaction,  we know that
\begin{equation}
\delta_{A_i\times (B_j C_k)} = \delta_{C_k} + \delta_{B_j \times C_k} + 
\delta_{A_i \times B_j \times C_k}
\end{equation}

Putting it together, we get the classic decomposition of effects in
3-way factorial designs,



\bigskip
\fbox{$\delta_{A_i B_j C_k} =  \delta_{A_i}+  \delta_{B_j}+ \delta_{C_k} + \delta_{A_i\times B_j} + \delta_{A_i \times C_k} + \delta_{B_j \times C_k} + \delta_{A_i \times B_j \times C_k}$}
\bigskip
 
The joint effect of three treatments can be decomposed into main
effects, three two-way interaction effects and a three-way interaction
effect.


\section{Interpretation of joint effects}

If there are joint effects we know that some combination of treatments
is making a difference. Not all treatment groups have the same
population mean.

\section{Interpretation of 3 way interactions}

If three way interactions are present we know that in some cases the
two-way interactions between two factors are different at different
levels of the other factor. For example, drugs A and B may interact
when given to men and they may no interact when given to women. This
tells us that we should be careful when making claims about the
interaction between 2 factors, since this interaction changes
depending on a third factor. If there is no 3 way interaction effects
then it is safe to analyze 2 way interactions since these interactions
do not change depending of the levels of the other factor. If there
are no three way interactions the design can be safely decomposed into
2-way factorial experiments.


\section{Interpretation of average joint effects}

The average joint effect of $A_iB_j$ is obtained by averaging the
effects of $A_iB_jC_k$ across  treatments of factor $C$. For
example, if $A_i$ represents a drug, $B_j$ is another drug, and factor
$C$ represents whether the subject is a man or a woman, the average
joint effect of $A_iB_j$ is the combined effect of the drug when
averaged across men and women.

In general this averaging operation is meaningful only if the joint
effects of $AB$ do not depend on $C$. In other words, the averaging is
safe if

\begin{equation}
\delta_{A_iB_j|C_k} = \delta_{A_iB_j}
\end{equation}

in which case the single-joint interactions $\delta_{(A_iB_j)\times
C_k}$ are zero. From the definition of single-joint interaction we know
that

\begin{equation}
\delta_{(A_iB_j)\times C_k} = \delta_{A_i \times C_k} +
\delta_{B_j \times C_k} + \delta_{A_i \times B_j \times C_k}
\end{equation}

Thus, if the three way interactions are zero, and the two way
interactions between C and the two other factors are zero, it is safe
to average across C.  Similar arguments can be used for the case of
averaging across A and across B.



\section{Interpretation of 2 way interactions}

Two way interactions tell us that the average-joint effects are not
additive. For example if there are two way interaction effects between
A and B, we know that the effect of A when averaging across C depends
on B. In our example, the effect of a drug when averaging across men
and women, depends on the second drug with which it combines.


\section{Interpretation of main effects}

The main effects of factor $A$ represent the average effect of a
treatment related to factor A when averaged across all levels of
factor B and C. This averaging operation is meaningful if the effects
of $A$ are the same for all combination of $B$ and $C$,

\begin{equation}
\delta_{A_i|B_jC_k} = \delta_{A_i}
\end{equation}

which is the same as saying that the single-joint interactions between
$A$ and $BC$ are zero $\delta_{A_i \times (B_j C_k)} =0$. We have
already seen that this interaction is zero if the three-way
interaction is zero and the two way interactions $A\times B$ and
$A\times C$ are zero. The two way interactions $B \times C$ are not
relevant when averaging across $B$ and $C$.


\end{document}


\head{References} \bibliography{cs14}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End: 
