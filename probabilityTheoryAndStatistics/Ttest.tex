% Copyright \copyright{} 1996,,1998, 2002 Javier R. Movellan. 
% This is an open source document.  Permission is granted
% to copy, distribute and/or modify this document under the terms of the
% GNU Free Documentation License, Version 1.1 or any later version
% published by the Free Software Foundation; with no Invariant Sections,
% no Front-Cover Texts, and no Back-Cover Texts.

\chapter{Introduction to Classic Statistical Tests} In the previous
chapter we saw that the current standards of empirical science use
classical statistical tests with a type I error specification of 5 \%.
In this chapter we see two classic statistical tests: The Z test and
the T test. Of the two the second one is more general and by far more
important. The Z test is introduced for historical reasons and because
it serves as a nice steping stone to  the T test.
\section{The Z test}
This test is {\bf used when the null hypothesis specifies the mean and
  the variance of the observations}. Let's see how the test works
using the following example. We toss a coin 100 times and we obtain 55
heads. Is this enough evidence to say that the coin is loaded?


\subsection{Two tailed Z test}
\begin{enumerate}
\item We have $n$ random variables 
\begin{equation}
X_1, \ldots, X_n
\end{equation}
where $X_i$ commonly represents a measurements from subject $i$ in a sample
of $n$ subjects.  We assume the following:
\begin{itemize}
\item The random variables are independent and identically distributed.
\item  If $n<30$ the random variables are 
Gaussian. 
\end{itemize}

{\it In our example $X_1, \ldots, X_n$ are Bernoulli random variables. If the outcome of
toss number $i$ is heads then $X_i$ takes value 1 otherwise it takes
value 0.}

\item Formulate a null hypothesis that specifies the mean and standard deviation of the mean. We represent these as $E(\bar{X}\given H_n \:\text{true})$ and $\Sdev(\bar{X} \given H_n \:\text{true})$

{\it In our example the null hypothesis $H_n$ is that the coin is fair. In such case $E(\bar{X} \given H_n \:\text{true}) = E(X_i \given H_n \: \text{true}) = 0.5$ and 

\begin{equation}
\Sdev(\bar{X} \given H_n \: \text{true}) = \frac{\Sdev(X_i \given H_n \: \text{true})}{\sqrt{n}} = \frac{0.5}{10} = 0.05
\end{equation}

}
\item Define the random variable $Z$ as follows
\begin{equation}
Z = \frac{\bar{X} - E(\bar{X} \given H_n \: \text{true} )}{\Sdev(\bar{X} \given H_n \: \text{true})}
=\frac{\bar{X} - E(\bar{X} \given H_n \: \text{true} )}{\Sdev(X_i \given H_n \: \text{true})/\sqrt{n} }
\end{equation}

\item Compute, the value taken by the random variable  $Z$,

{\it In our example

\begin{equation}
Z = \frac{0.55 - 0.5}{0.05} = 1
\end{equation}
}
\item If $Z \nin [-1.96,1.96]$ reject $H_n$ and report that
  the results were {\bf statistically significant}.  Otherwise
  withhold judgment and report that the resuts {\bf were not
    statistically significant}.

{\it In our example $Z \in [-1.96,1.96]$ so we withhold judgment. We do not have enough evidence to say that the coin is loaded.}
\end{enumerate}

\paragraph{Proof:}
Since this is a classical test we just need to show that with this
procedure  the type I error specification is no larger than 5 \%. In
other words $P(H_n \: \text{rejected} \given H_n \: \text{true}) \leq  0.05$. To do
so first we will show that if $H_n$ is true then $Z$ is an standard
Gaussian random variable. First note that $Z$ is just a linear combination of $\bar{X}$


\begin{align}
Z &= a + b \bar{X}\\
a &= - \frac{E(\bar{X} \given  H_n \: \text{true})}{ \Sdev(\bar{X} \given H_n \: \text{true})}\\
b &=  \frac{1}{ \Sdev(\bar{X} \given H_n \: \text{true})}
\end{align}
If $X_1, \ldots, X_n$ are Gaussian then $\bar{X}$ is also Gaussian
(because the sum of Gaussian is Gaussian). If $X_1, \ldots, X_n$ are not
Gaussian but $n > 30$, by the Central limit theorem, then the
cumulative of $\bar{X}$ is approximately Gaussian. Under these
conditions $Z$ is also Gaussian (because $Z$ is a linear transformation of $\bar{X}$ and a linear transformation of a Gaussian rav is also Gaussian).  Moreover 

\begin{align}
E(Z \given H_n \: \text{true}) &= a + b E(\bar{X}  \given H_n \;\text{true}) = 0\\
\Var(Z \given H_n \: \text{true}) &=  b^2 \Var(\bar{X} \given H_n \:\text{true}) = 1
\end{align}
Thus, if the null hypothesis is true and the assumptions of the $Z$
test are met, then $Z$ is a standard Gaussian random variable and 

\begin{equation}
P(H_n \: \text{rejected}
\given H_n \: \text{true}) = P (Z \nin [-1.96, 1.96]) = 2 \Phi(-1.96) = 1/20
\end{equation}
\begin{flushright} $\square$ \end{flushright}



\subsection{One tailed Z test}

In this case the null hypothesis includes an entire range of values
for the sample mean. For example, the null hypothesis could be that
the expected value of the mean is smaller than 0. Or that the expected
value of the mean is smaller than 3. The first thing we do is to pick
the extreme case proposed by the null hypothesis and which is
finite. For example if the null hypothesis is that the expected value
of the sample mean is larger than 3, then the extreme case is that the
expected value is exactly 3. I'm going to represent this extreme case
of the null hypothesis as $H_x$ to distinguish it from the more
general null hypothesis that we want to reject. For example if the
null hypothesis says $E(\bar{X} \given H_n \: \text{true}) \leq 3$ the
the extreme case of the null hypothesis would claim $E(\bar{X} \given
H_x \: \text{true}) = 3$.


\begin{enumerate}
\item Formulate the null hypothesis and the extreme hypothesis.
\item Compute the value taken by the random variable $Z$ as in the two-tailed case, only in this case use expected values given by $H_x$ instead of $H_n$.  
\item If positive values of $Z$ are the only ones inconsistent
  with $H_n$ and if $Z > 1.64$ reject $H_n$. Report that the
  results were {\bf statistically significant}. If negative values of
  $Z$ are the only ones inconsistent with $H_n$ and if
  $Z < -1.64$ then reject the null hypothesis. Report that the 
  results are {\bf statistically significant}. Otherwise
  withhold judgment. Report that the results {\bf were not
    statistically significant}.
\end{enumerate}
To see that this test also has the desired type I error specification note that
\begin{align}
P(H_n \: \text{rejected}
\given H_n \: \text{true}) &\leq P(H_n \: \text{rejected} \given H_x \text{true}) \\&=  P (Z > 1.64) =  \Phi(-1.64) = 1/20
\end{align}
Thus we have proven that the type I error specification is not larger
than 1/20, making the test acceptable from a classical point of view.

\paragraph{Example}{\it  We want to show that bees prefer red flowers rather than yellow flowers. We construct 100 plastic flowers 50 of which are yellow and 50 red. Other than the color the flowers are indistinguishable in appearance. We then measure for 200 bees the amount of time they spend in yellow versus red flowers. We find that 160 out of the 200 bees spent more time in the red flowers. Is this enough evidence to say that bees prefer red flowers?}

We can model the preference of each bee as independent Bernoulli
random variables $X_1, \ldots, X_{200}$, where $X_i = 1$
represents the event that bee number $i$ in the sample spent more
time on the red flowers.  In our case
the sample mean takes the following value  $\bar{X} = 160/200=0.8$. The null hypothesis says that
$E(\bar{X} \given H_n \: \text{true}) \leq 0.5$. This hypothesis covers an entire range of values so a one tailed test
is appropriate. The extreme case of the null hypothesis, symbolized as
$H_x$ says that $E(\bar{X} \given H_x \: \text{true}) = 0.5$. Thus,

\begin{align}
\bar{X}&= 0.8\\
E(\bar{X} \given H_x) &= 0.5\\
\Sdev(\bar{X} \given H_x) &= \Sdev(X_i \given H_x)/\sqrt{200} = 0.035\\
Z &= \frac{0.8- 0.5}{0.035} = 8.57
\end{align}
Since positive values of $Z$ contradict $H_n$ and $Z >
1.64$ we reject the null hypothesis and conclude that bees prefer red
flowers over yellow flowers.


\section{Reporting  the results of a classical statistical test}


First let us concentrate on the things you should avoid when reporting results:

\begin{itemize}

\item{Avoid statements like   ``The null hypothesis was'' or ``The alternative hypothesis was''. These terms are just used to understand the logic underlying  statistical tests. You should assume that the readers already know about this logic.}

\item{Avoid statements like ``The results prove the idea that'', or ``The results disprove the idea that''. As we know, statistical test  are not infallible so scientists avoid strong statements like ``prove'' and ``disprove''.}
  
\item{Avoid statements like ``The data shows that very probably there
    is a difference between the experimental and the control
    groups...''. You cannot say this because we do not know the
    probability of the null hypothesis. Remember we are using a
    classical test and thus the null hypothesis is either true or
    false.}

\item{Avoid statements like ``The results are  statistically insignificant''. The word insignificant is misleading. Simply say that the results are not statistically significant. }

\item{Avoid statements like ``The results were very significant''. This suggests that your results are very important or that your type I error specification is   different from that of other scientists. This is misleading since you would have published your results if your test passed with your type I error specification. }


\end{itemize}


\subsection{Interpreting the results of a classical statistical test}

If the results are not significant, it basically means that we do not have enough evidence, for now, to reject the null hypothesis. Here is an example interpretation: 

\begin{itemize}

\item {There is not enough evidence to support the idea  that the drug has an effect on the subjects' reaction time.}

\end{itemize}


If the results  are significant we can say that the data
are not consistent with the null hypothesis.  Here are some standard
interpretations of significant results. Note how we avoided strong words
like ``prove'' in favor of softer words like ``support''

\begin{itemize}

\item {The data support the idea  that the drug has an effect on the subjects' reaction time.}

\item{The results of this  experiment support the idea that drug X has an effect on reaction time.}

\end{itemize}

\section{The T-test}
One major problem with the $Z$ test is that the null hypothesis needs
to specify the value of $\Var(\bar{X})$. This value is known for
Bernoulli random variables, but in general it is unknown. In such a
case, we may estimate the variance of the mean using our sample
information. To do so first define the sample variance as

\begin{equation}
S^2_X = \frac{1}{n-1} \sum_{i=1}^n (X_i - \bar{X})^2
\end{equation} 
note that the sample variance is a random variable. In the Appendix to
this Chapter we prove that the sample variance is an unbiased estimate
of the variance of the observations, i.e.,
\begin{equation}
E(S^2_X) = \Var(X_i) 
\end{equation}
Since $S^2_X$ is an unbiased estimate of the variance of the observations, we can divide it by $n$ to get an unbiased estimate of the variance of the sample mean. We represent this estimate as $S^2_{\bar{X}}$
\begin{equation}
S^2_{\bar{X}} = \frac{S^2_{X_i}}{n}
\end{equation}
Now remember the $Z$ random variable was defined as follows
\begin{equation}
Z = \frac{\bar{X} - E(\bar{X} \given H_n \: \text{true} )}{\Sdev(\bar{X} \given H_n \: \text{true})}
=\frac{\bar{X} - E(\bar{X} \given H_n \: \text{true} )}{\Sdev(X \given H_n \: \text{true}) /\sqrt{n}}
\end{equation}
The $T$ random variable is very similar to the $Z$ random variable except for the fact that in the denominator it uses an estimate of the variance of the mean instead  of the true variance

\begin{equation}
T = \frac{\bar{X} - E(\bar{X} \given H_n \: \text{true} )}{S_{\bar{X}}}= 
\frac{\bar{X} - E(\bar{X} \given H_n \: \text{true} )}{S_X/\sqrt{n} }
\end{equation}

\subsection{ The distribution of T}

The distribution of the $T$ random variable was discovered by William
Sealy Gosset (1876--1937). Gosset was a chemist and mathematician hired
by the Guinees brewery in 1899. Brewing is a process in which
statistical analysis had great potential since brewers have to deal
with variable materials, temperature changes and so on.  The story
goes that at the end of the $19^{th}$ century scientific methods and
statistical modeling were just beginning to be applied to brewing but
the methods available at the time required large numbers of
observations.  Gosset had to work with experiments with small samples,
for which the $Z$ test did not work very well. As a statistician
Gosset liked to do everything starting from first principles and
disliked the use of recipes and tabulations. This gave him great
flexibility and power when tackling new problems. He once said ``Doing
it from first principles every time preserves mental
flexibility''. Gosset was also a very good carpenter, an activity to
which he also applied his devotion to first principles; he disliked
the use of complicated tools and liked doing as much as possible with
a pen-knife. In 1908 Gosset published a paper entitled ``The probable
error of the mean'', where he described the distribution of the $T$
random variable, as a statistic applicable to experiments with small
numbers of observations. He published this paper under the name
``Student''. His desire to remain anonymous gave him a romantic,
unassuming reputation.  His theoretical distribution became known as
the Student-distribution, or simply the T-distribution.  In fact there
is an infinite number of \emph{T-distributions}, with each member of the
family identified by a parameter known as the \textbf{degrees of freedom}
($df$).  The probability density function of $T$ is given by the following
formula:

\begin{align}
f_T(u) = \frac{1}{K(df)} (1 + \frac{u^2}{df})^{-(df+1)/2}\\
K(df) = \int_{-\infty}^{\infty} (1 + \frac{x^2}{df})^{-(df+1)/2} \; dx
\end{align}
In practice, we do not need to worry about the formula for $f_T(u)$
since the values for the cumulative distribution 
\begin{equation}
F_T(v) = \int_{\-\infty}^v f_T(x) dx
\end{equation}
appears in most statistics textbooks.  Figure 3 shows the shape of the
T-distribution with 1 df. The distribution is bell shaped but it has
longer tails than the Gaussian distribution. When the number of
degrees of freedom is very large, the T-distribution closely
approximates the Gaussian distribution.

\begin{figure}[t]\label{f3}
\centerline{\psfig{file=figures/l4.f4.eps,height=2in}}
\caption{The t-distribution with one degree of freedom.}
\end{figure}



\subsection{Two-tailed T-test}
Here is the procedure to perform a two-tailed T-test
\begin{enumerate}
\item Figure out the expected value of the sample mean if the null hypothesis is true $E(\bar{X} \given H_n \: \text{true})$

\item Compute $T$ the value taken by $T$ for your specific sample.

\item Get the degrees of freedom (i.e., number of independent observations) in $S_{\bar{X}}$. In our case the number of degrees of freedom is $n-1$.


\item Go to $T$ tables for $n-1$ degrees of freedom, and compute the value $c$ such that 
\begin{equation}
P(T > c \given H_n \:\text{true}) = 1/40
\end{equation}
The number $c$  is called the critical value. 
\item If $T \nin [-c,c]$ reject the null hypothesis, otherwise withhold judgment. 
\end{enumerate}

\paragraph{Example:}
We get 3 bags of potato chips from a specific brand. The company that
makes these chips says that on average there are 40 chips per bag. Let
$X_i$ measure the number of chips in bag $i$. In our sample 
we have $X_1 = 30$, $X_2 = 10$ and $X_3 =
20$. Do we have enough evidence to reject the idea that on average
there are 40 chips per bag?

The null hypothesis is that there are 40 chips per bag. Thus $E( \bar{X} \given H_n \: \text{true}) = 40$. Moreover
\begin{gather}
\bar{X} = (30+10+20)/3 = 20\\
S^2_X  = \frac{(30-20)^2 +(10-20)^2 +(20-20)^2)}{3-1} = 100\\
S_{\bar{X}} = \sqrt{S^2_X/n} = \sqrt{100/3} = 5.77\\
T = \frac{20 - 40}{5.77} = - 3.46\\
df = n-1 = 2\\
c  = 4.30 \;\;
\end{gather}
I obtained the critical value $c=4.30$ using the tables of the $T$
statistic with $df=2$. Since $T \in [-4.30,4.30]$ we withhold
judgment. We do not have enough evidence to reject the null
hypothesis.

\paragraph{Proof:}
This procedure has the correct type I error specification since
\begin{align}
P(H_n \: \text{rejected}
\given H_n \: \text{true}) = P (T \nin &[-c,c])\\ &=  2 P(T > c \given 
H_n \: \text{true}) = 1/20
\end{align}

\paragraph{Procedure for one-tailed T-test}
In this case the null hypothesis takes an entire range. For example,
it could say that the expected value of the mean is smaller than 20,
not just 20. As with the $Z$ test I will use $H_n$ to represent the null
hypothesis and $H_x$ to represent the extreme case in this null
hypothesis. For example if the null hypothesis says that the expected
value of the mean is smaller than 20, then $H_x$ says that the
expected value of the mean is exactly equal to 20. The procedure to
test one-tailed hypotheses is as follows

\begin{enumerate}
\item Formulate the null hypothesis and the extreme hypothesis.
\item Compute the value of $T$ and $df$ as in the two-tailed case, only in this case use expected values given by $H_x$ instead of $H_n$.  
\item Using tables find the value $c$ such that $P(T > c \given H_x) = 1/20$
\item If positive values of $T$ are the only ones inconsistent with $H_n$  and if $T > c$ reject $H_n$. If negative values of $T$ are the only ones inconsistent with $H_n$  and if $T < -c$ reject the hypothesis. Otherwise withhold judgment. 
\end{enumerate}
For example if we want to show that the consumers are being
cheated. Then we want to reject the hypothesis that the average number
of chips is larger or equal to 40. In such case the null hypothesis is $E(\bar{X} \given \: H_n) \geq 40$. The extreme case of the null hypothesis says $E(\bar{X} \given \: H_x) = 40$. Moreover only negative values
of $T$ are inconsistent with the null hypothesis. We go to tables
and find that for 2 degrees of freedom $P(T > -2.91) = 1/20$. Since
$T = -3.46$ is smaller than $-2.91$, we have enough evidence to
reject the null hypothesis.
\paragraph{Proof:}
This procedure has the desired type I error specification since
\begin{align}
P(H_n \: \text{rejected}
\given H_n \: \text{true}) &\leq P (T \nin [-c,c] \given H_x \:
\text{true} )\\ &=  2 P(T > c \given  H_x \: \text{true}) = 1/20
\end{align}


\subsection{A note about LinuStats}

LinuStats, and some T-tables provide probabilities of the absolute value of $T$, i.e.,
\begin{equation}
P(|T| \geq c) = P(\{-T\leq c \}\union \{T \geq c\}) = 2 P(T \geq c)
\end{equation}
Thus, if you want to obtain the critical value $c$ such that $P(T \geq
c) = x$ then you need to give LinuStats the value $2x$. If you do
that LinuStats will give you the value $c$ such that
\begin{equation}
P(|T| \geq c) = 2x 
\end{equation}
from which it follows that
\begin{equation}
P(T \geq c) = 2x/2 = x
\end{equation}
which is what you wanted to begin with.

\section{Exercises}

\begin{enumerate}

\item Which of the following  is more likely not to belong
to a population with $\mu= 0$ and $\sigma^2=100$. Explain why. (5 points)

\begin{enumerate}
\item A single subject with a value of 20.
\item A random, independent sample of 100 subjects with a sample mean of -2.0
\item A random, independent sample of 10000 subjects with a sample mean of 1.0
\end{enumerate}


\item
{\sl A research team is testing whether newborns have color vision.
Ten infants were tested at the UCSD hospital within their first 3
hours of life.  Experimenters showed the infants a card with 2 color
patches: red and green. The card was positioned so that the distance
between the two color patches was about 15 degrees of visual
angle. Each color patch was circular with a diameter of 7 degrees of
visual angle.  For half the infants red was on the right side, of the
card and for the other half red was on the left side. The two colors
were calibrated to have equal luminance, so that they could not be
distinguishable by a black-and-white vision system.  After parental
consent was granted, the infants were tested as follows. Once it was
decided that the infant was in an alert state, the card was presented
in front of him/her and an observer recorded, during a 30 second
period how long infants looked to the left and right side of the
card. The observer did not know whether left corresponded to red or to
the green patch. The dependent variable was the total time in seconds
looking to the red patch minus the time looking to the green
patch. The results were as follows: $\{-2,-1,10,8,4,2,11,1,-3,4\}$.
}

\begin{enumerate}
\item{Formulate the null hypothesis}
\item{Should the test be 1-tail or 2-tails?}
\item{Do you have enough evidence to reject the null hypothesis?}
\item{What is the probability that you made a type I error?}
\item{What is the probability that you made a type II error?}
\item{What is the type I error specification?}
\item{What is the power specification?}
\item{What are the assumptions needed to guarantee a 5\% type I error specification?}
\end{enumerate}


\item You are given 3 randomly selected kittens of the same age.
Their weights are 3, 3 and 2 pounds.  Your biology book says that 1
year old kittens weigh 7.0 pounds. Your goal is to prove that these
kittens are not 1 year old.

\begin{enumerate}
\item{What is the null hypothesis?}
\item{Do you have enough evidence to say that the kittens are not 1 year old?}
\item{What is the probability that you made a type I error?}
\item{What is the probability that you made a type II error?}
\item{What is the type I error specification?}
\item{What is the power specification?}
\item{What are the assumptions needed to guarantee a 5\% type I error specification?}
\end{enumerate}





\end{enumerate}
\section{Appendix: The sample variance is an umbiased estimate of the population variance}


Let $X_1, \ldots, X_n$ be i.i.d.\ random variables with mean $\mu_X$
and variance
$\sigma^2_X$. To ease the presentation I will prove that $E(S^2_X) =
\sigma^2_X$ for the case in which $\mu_X=0$. Generalizing the proof
for arbitrary values of $\mu_X$ is mechanical once you know the proof
for $\mu_X=0$. First note if $\mu_X=0$ then
\begin{equation}
\Var(X_i) = E(X_i- \mu_X)^2 = E(X_i^2) = \sigma^2_X
\end{equation}
\begin{equation}
\Var(\bar{X}) = E[(\bar{X} - E(\bar{X}))^2] = E(\bar{X}^2) = \frac{\sigma^2_X}{n}
\end{equation}
and since $X_i, X_j$ are independent when $i \neq j$
\begin{equation}
E(X_i X_j) = E(X_i) E(X_j) = 0 
\end{equation}
Using the definition of $S^2_X$, and considering that $X_1, \ldots, X_n$ have the same distribution, we get
\begin{equation}
E(S^2_X) = \frac{1}{n-1} \sum_{i=1}^n E(X_i - \bar{X})^2 = \frac{n}{n-1}E(X_1 - \bar{X})^2
\end{equation}
Moreover
\begin{equation}
E(X_1 - \bar{X})^2 = E(X_1^2) + E(\bar{X}^2) - 2 E(X_1 \bar{X}) 
= \sigma^2_x + \frac{\sigma^2_X}{n} - 2 E(X_1 \bar{X}) 
\end{equation}
Finally
\begin{align}
E(X_1 \bar{X}) = E(X_1 \frac{1}{n} &\sum_{i=1}^n X_i) \\  &=\frac{1}{n}\big( E(X_1^2) + \sum_{i=2}^n E(X_1 X_j) \big) = \frac{\sigma^2_X}{n}
\end{align}
Thus
\begin{equation}
E(S^2_Y) = \frac{n}{n-1}\bigl(\sigma^2_X + \frac{\sigma^2_X}{n} - 2 \frac{\sigma^2_X}{n}\bigr)
=  \sigma^2_X
\end{equation}
\begin{flushright} $\square$ \end{flushright}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End: 
