% Copyright \copyright{} 1996,,1998, 2002 Javier R. Movellan. 
% This is an open source document.  Permission is granted
% to copy, distribute and/or modify this document under the terms of the
% GNU Free Documentation License, Version 1.1 or any later version
% published by the Free Software Foundation; with no Invariant Sections,
% no Front-Cover Texts, and no Back-Cover Texts.

\chapter{Blocked and Within Subject Designs}


Good experiments are designed so that if the null hypothesis is false,
we have a good chance of rejecting it (statistical power). The three
most common ways to increase the power of an experiment are:

\begin{itemize}
\item To use a large number of
subjects. 

\item To use extreme treatments (e.g., large dosages of a drug) 

\item To reduce random variability.

\end{itemize}

In many cases running subjects is expensive, and large dosages may be
dangerous, thus the special importance of reducing random variability
as much as possible. The smaller the randomized variablity the less
subjects are needed to obtain a desired statistical power. One way to
reduce within group variability is to treat intervening variables as
if they were factors, thus removing their effect from the denominator
of the F statistic.  This technique is known as {\bf blocking}.

\section{Blocked designs}


Blocked designs can be seen as a particular case of factorial
designs. In a blocked experiment, one of the factors is an intervening
variable suspected of having an important influence on the dependent
variable. The effect of the variable is eliminated from the error term
by classifying subjects into groups, technically called blocks,
according to the intervening variable and by treating these groups as
if they were levels of an additional independent variable.  For
convenience I'll use the letter $A$ for the original independent
variable and $B$ for the blocking variable.

Contrary to standard factorial designs, in blocked designs we are not
particularly interested on testing whether $B$ has a significant
effect, we just want to remove its influence from the error term
(i.e., the denominator of the F-statistic).  For example if we are
testing the effect of a drug on memory and we think that age is an
important intervening variable, we could use classify subjects into
different age groups. The factor of interest ($A$) would be the drug
vs the placebo, and the other factor ($B$) would be the age category
of the subject. Since all the members of a particular group have the
same age, the effect of age is removed from the within groups
variability potentially increasing the power of the F-test. At the
same time we have a reasonable range of ages so that we do not lose
generalization.


\subsection{An Example}

Let me analyze a hypothetical experiment first using a standard
unblocked design and then a blocked design: There is a placebo and a
drug group and subjects are tested on a memory task. There are 4
subjects per group randomly assigned to one of the two
conditions. Subjects were blocked into two age named ``younger'' and
``older''.

Table \ref{b.data} shows the hypothetical results of
the experiment, and Table \ref{b.means} shows the
corresponding sample means.


\begin{table}[h]
\begin{center} 
\begin{tabular}{r|cc|cc|} \cline{2-5}
\multicolumn{1}{c|}{} 
&\multicolumn{2}{c|}{\sl Younger} 
&\multicolumn{2}{c|}{\sl Older} \\ \cline{2-5}
{\sl Drug}    &11 &13 &5 &7 \\ \cline{2-5}
{\sl Placebo} &7  &9  &1 &3 \\ \cline{2-5}
\end{tabular}  
\caption{Raw Data}
\label{b.data}
\end{center} 
\end{table}



\begin{table}[h]
\begin{center} 
\begin{tabular}{r|c|c|c} \cline{2-3}
\multicolumn{1}{c|}{} 
&\multicolumn{1}{c|}{\sl Younger} 
&\multicolumn{1}{c|}{\sl Older} 
&\multicolumn{1}{c}{\sl Row Means} \\ \cline{2-3}
{\sl Drug}    &12 &6 &9\\ \cline{2-3}
{\sl Placebo} &8  &2 &5 \\ \cline{2-3}
\multicolumn{1}{c}{\sl Column Means} 
&\multicolumn{1}{c}{10}
&\multicolumn{1}{c}{4}
&\multicolumn{1}{c}{7}
\end{tabular}  
\caption{Sample Means}
\label{b.means}
\end{center} 
\end{table}


First let will analyze the results as an unblocked design. In this
case we simply ignore the age group. We have a single factor design
where the independent variable $A$, has two treatment levels: drug and
placebo. There is 4 subjects per group (two of them are young and two
old, but we do not care about it since age is treated as a random
variable). The sum of square statistics follow:

\begin{equation}
SS_A = (4)(9-7)^2 + (4) (5-7)^2  = 32
\end{equation}

\begin{equation}
SS_{R/A} = (11-9)^2 + (13-9)^2 +(5-9)^2 + (7-9)^2 +
\end{equation}
\begin{equation}
+(7-5)^2 + (9-5)^2 +(1-5)^2 + (3-5)^2 = 80
\end{equation}

The mean square statistics follow

\begin{equation}
MS_A = \frac{SS_A}{a-1} = \frac{32}{1} = 32
\end{equation}

with $df_1 = 1$, and 

\begin{equation}
MS_{R/A} = \frac{SS_{R/A}}{N-a} = \frac{80}{6} 
\end{equation}

with $df_2 = 6$. Therefore,



\begin{equation}
F_{A} = \frac{MS_A}{MS_{R/A}} = 2.4
\end{equation}

Using F-tables we find that for F(1,6) = 2.4, the p-value is 0.17,
which is not significant.

 There is nothing wrong with this analysis, other than it is not very
powerful. The problem is that this analysis  considers age as a
random intervening variable and thus the within groups variability is
very large, reducing the power of the experiment.

Let us now do the analysis blocking by age. In this case we have a $2 \times 2$
factorial design. Factor $A$ is the drug condition and factor $B$ the age
condition.  The main effects of the drug are the same as in the
previous analysis: $SS_A = 32$. However the within group variability
is now reduced due to the fact that each group now is made of members
of approximately the same age.

\begin{equation}
SS_{R/AB} = (11-12)^2 + (13-12)^2 + (5-6)^2 +(7-6)^2 +
\end{equation}

\begin{equation}
+(7-8)^2 +(9-8)^2 +(1-2)^2 +(3-2)^2 = 8
\end{equation}

Unfortunately there is no free lunch here. Blocking by age reduces the
error, but it also costs us in complexity. The previous model had
complexity 2 since there were only two treatments, and the blocked
model has complexity 4, since now we divide subjects into 2 treatments
times 2 age groups. Since the complexity of the data is $8$, the
difference in complexity is now $df_2 = 8-4= 4$.  The denominator of
the F-statisic follows

\begin{equation}
MS_{R/AB} = \frac{SS_{R/AB}}{N-(a)(b)} = \frac{8}{4}  = 2
\end{equation}
with $df_2 = 4$

So blocking by age seems worth the cost. The old denominator of the
F-test was $8/6$ and now it is just 2.  The F-statistic is now

\begin{equation}
F_{A} = \frac{MS_A}{MS_{S/AB}} = \frac{32}{2} = 16
\end{equation}

For F(1,4) = 16, the p-value is 0.016, which is significant. 

I want to point out that blocking may not always be a good idea. The
key question here is whether the extra complexity we get by blocking
is compensatted by a reduction in our estimate of random
variability. So in a way by using a blocked analysis, we are gambling
that the blocking variable has an important intervening effect: The
price we pay by blocking is an increment in the complexity of our
model and thus a reduction in $df_2$, which is bad. Our gamble is that
this reduction in $df_2$ will be compensated by a large reduction in
the sum of squares due to random effects. In other words, since we do
not treat age as a random variable anymore, its effect will not show
in the denominator of the F-statistic. The moral of the story is that
for bloking to be benefitial, the increment in the model complexity
due to blocking should be compensated by an important reduction in
within groups error.


\section{Within Subject Designs}

Up to know we have controlled subject differences by randomly
assigning subjects to the treatment conditions. Designs that treat
subjects as random variables are called {\bf between subjects}, or
more generally, {\bf completely randomized}. The main advantage of
between subject designs is their simplicity. We do not need to worry
about intervening subject differences because they are statistically
controlled by random assignment. Moreover we do not need to worry
about order effects because each subject is only tested in one
condition. 

The price payed by the simplicity of between subject designs is a loss
of statistical power: The effects due to subject differences are
randomized and thus become part of the error term, the denominator of
the F-test. We know that large random variability reduces the power of
the experiment and thus subjects designs generally need a relatively
large number of subjects, to compensate for the large denominator in
the F test.


An alternative way to control for subject differences is to use the
subject's identity as a blocking variable.  This type of blocked
designs are so important that they are given a special name: {\bf
within subjects}, or more generally {\bf repeated measures} designs.
Consider for concreteness the following example. We want to study the
effect of a drug on memory and we have a sample of 4 subjects. Our
independent variable is the drug condition (drug vs placebo). Given
our limited set of subjects we decide to use a within subjects
design. Each subject is run on the two experimental treatments (drug
and placebo), and the subject identity is used as an additional,
blocking, independent factor.  Thus we use a $2\times4$ factorial
design where one of the independent variables $(A)$ is the drug
condition (drug or placebo) and the other factor, which for
convenience I will call $S$ instead of $B$, is the subject's identity
(1 to 4).

 Note that in this design we no longer randomly assign subjects to
conditions. What we randomize is all intervening variables other than
subject identity (e.g., subject emotional state, external room
conditions,etc.). Our observations are random samples of all the
different observations that we could have obtained {\bf with the same
subjects tested} at different times, different conditions.

The objective in within subjects designs is to remove from the error
term that part of the variance that is due to the additive effects of
internal subject differences.  For example, if a drug has the same
effect in all subjects, we would like to say that it has a very
predictable effect (low error). It does not really matter to us
whether subjects differ from as long as the drug has a consistent
effect on each subject (i.e., raising or lowering the performance by a
constant amount). In the next section we will formulate a model to
capture this type of error term. For concreteness I'll use the
hypothetical results shown in Table~\ref{tab:within}.


\section{General  Model of the Observations}

We model observations as the result of three additive factors: 1) an
underlying baseline specific to each subject, 2) the effect of the
treatment, and 3) Gaussian variability,


\begin{equation}
Y_{A_iS_j} = \mu_{S_j} + \delta_{A_i} + E_{ij}
\end{equation}

where $\mu_{S_j}$ is the baseline for subject $S_j$ and $\delta_{A_i}
= \mu_{A_i} - \mu$ is the effect of treatment $A_i$.  Thus, the error
term in the model is as follows

\begin{equation}
E_{ij} = Y_{A_iS_j} -  \mu_{S_j} -\delta_{A_i}  
\end{equation}



This general model requires estimate of the effects and of the
subjects' baselines:


\begin{equation}
\hat{\mu}_{S_j} = \bar{Y}_{S_j} 
\end{equation}

and 

\begin{equation}
\hat{\delta}_{A_i}   = \bar{Y}_{A_i} - \bar{Y}
\end{equation}

Note that to get these estimates we need the row means, column means
and the overall mean.  At first sight it may seem like the complexity
is $s+a+1$ but in fact is less than that. If you give me the $s$
subject means I can calcutate the overall mean. In addition, if you
give me $a-1$ row means, I can calculatethe last one, since I already
know the overall mean. Thus the complexity is in fact $a+s-1$, where
$s$ is the number of subjects.

\begin{table}[t]\label{tab:within}
\caption{A hypothetical within subjects experiment.} 
\begin{center} 
\begin{tabular}{r|c|c|c|c|c} \cline{2-5}
\multicolumn{1}{c|}{} 
&\multicolumn{1}{c|}{\sl Subject 1} 
&\multicolumn{1}{c|}{\sl Subject 2} 
&\multicolumn{1}{c|}{\sl Subject 3} 
&\multicolumn{1}{c|}{\sl Subject 4}  
&\multicolumn{1}{|c}{\sl Treatment Means}  \\ \cline{2-5}

{\sl Drug}    &2 &4 &4 &2 &3\\ \cline{2-5}
{\sl Placebo} &0 &2 &0 &2 &1\\ \cline{2-5}
\multicolumn{1}{r}{\sl Subject Means} 
&\multicolumn{1}{c}{$1$}
&\multicolumn{1}{c}{$3$}
&\multicolumn{1}{c}{$2$}
&\multicolumn{1}{c}{$2$}
&\multicolumn{1}{c}{Overall = 2}\\
\end{tabular}  
\end{center} 
\end{table}


Note that since we only have an observation per cell, the cell means
equal the observations themselves, and thus the residual corresponds
to the estimate of the interaction effect

\begin{equation}
\hat{E}_{ij} = Y_{A_iS_j} - \hat{\mu}_{S_j} -  \hat{\delta}_{A_i} =  
 Y_{A_iS_j} - \bar{Y}_{S_j} - \bar{Y}_{A_i} + \bar{Y} =
 \hat{\delta}_{A_i\times S_j}
\end{equation}


Thus the loss of this model
is the sum of squares of $A \times S$ interaction,

\begin{equation}
SS_{H_a} = \sum_{i=1}^a \sum_{j=1}^n ( Y_{A_iS_j} -  \bar{Y}_{S_j} - \bar{Y}_{A_i} + \bar{Y}  )^2 = SS_{A \times S}
\end{equation}



\section{Model of the Null Hypothesis}

The null hypothesis postulates that $\delta_{A_i}=0$ for all the
treatment groups.  Thus, according to this model,

\begin{equation}
E_{ij} = Y_{A_iS_j} -  \mu_{S_j} 
\end{equation}

The residuals are obtained by estimating the $\mu_{S_j}$ using the  sample means,

\begin{equation}
\hat{E}_{ij} = Y_{A_iS_j} -  \bar{Y}_{S_j} 
\end{equation}

The complexity of this model is $s$, the number of subjects, since we
have to estimate  a mean $(\bar{Y}_{S_j})$ for each subject.


The loss of this model is
\begin{equation}
SS_{H_n} = \sum_{i=1}^a \sum_{j=1}^s \hat{E}_{ij}^2 =   
\sum_{i=1}^a \sum_{j=1}^s (Y_{A_iS_j} -  \bar{Y}_{S_j} )^2
\end{equation}



It is easy to show that the loss of this model is $SS_{A}+
SS_{A\times S}$. First note that

\begin{equation}
\hat{E}_{ij} = Y_{A_iS_j} -  \bar{Y}_{S_j}  = \hat{\delta}_{A_i\times S_j} + \hat{\delta}_{A_i}
\end{equation}

Thus

\begin{equation}
SS_{H_n} = \sum_{i=1}^a \sum_{j=1}^s \hat{E}_{ij}^2 =   
\sum_{i=1}^a \sum_{j=1}^s  (\hat{\delta}_{A_i\times S_j} + \hat{\delta}_{A_i})^2 =
\end{equation}


\begin{equation}
\sum_{i=1}^a \sum_{j=1}^s  
(\hat{\delta}_{A_i\times S_j})^2 +  \sum_{i=1}^a \sum_{j=1}^s  (  \hat{\delta}_{A_i})^2 +
\end{equation}
\begin{equation}
2 \sum_{i=1}^a \sum_{j=1}^s  (\hat{\delta}_{A_i\times S_j}  \hat{\delta}_{A_i})^2 
\end{equation}

The first term is $SS_{A\times S}$, the second term is $SS_{A}$ and
the third term can be easily shown to be zero. Since the loss of the
alternative hypothesis is $SS_{A\times S}$ then the loss difference
between the null and the alternative hypothesis is $SS_A$. The increment in complexity is $(a+s-1) - (s) = a-1$. 


We are now ready to formulate the F-statistic to test the hypothesis
of no effects of Factor A. First, for the numerator,

\begin{equation}
MS_A = \frac{SS_A}{a-1}
\end{equation}

with $df_1 = a-1$

For the denominator, we compare loss and complexities with respect to
the data itself. The loss difference is $SS_A$, and the difference in complexity is $(a)(s) - (a+s-1) = (a-1)(s-1)$. Thus, the denominator of the F-test is

\begin{equation}
MS_{A\times S} = \frac{SS_{A \times S}}{(a-1)(s-1)}
\end{equation}

with $df_2 = (a-1)(s-1)$

The F-statistic follows


\begin{equation}
F_A = \frac{MS_A}{MS_{A\times S}}
\end{equation}

In our example, 

\begin{equation}
SS_A = (s) \sum_{i=1}^a  (\bar{Y}_{A_i} - \bar{Y})^2 = 
\end{equation}
\begin{equation}\nonumber
= 4 [(3-2)^2 + (1-2)^2] = 8
\end{equation}



and
\begin{equation}
SS_{A\times S} = \sum_{i=1}^a \sum_{j=1}^s (\bar{Y}_{ij} -  \bar{Y}_{Ai} - \bar{Y}_{S_j} + \bar{Y})^2 = (2 - 3 -1 + 2) ^2 + (4 -3 -3 +2)^2+
\end{equation}
\begin{equation}\nonumber
+ (4 - 3 -2 +2)^2 + (2-3-2+2)^2 +(0-1-1+2)^2+
\end{equation}
\begin{equation}\nonumber
 +(2-1-3+2)^2 +(0-1-2+2)^2 +(2 -1 -2+2)^2  = 4
\end{equation}

\begin{equation}
MS_A = \frac{SS_A}{a-1} = \frac{8}{2-1} = 8
\end{equation}

and

\begin{equation}
MS_{A\times S}=   \frac{SS_{A\times S}}{(a-1)(s-1)} = \frac{4}{(2-1)(4-1)} = \frac{4}{3}
\end{equation}

and

\begin{equation}
F_A = \frac{MS_A}{MS_{A\times S}} = \frac{24}{4} = 6
\end{equation}

Using tables we see that for $F(1,3) = 6$, the p-value is 0.09, which
is not significant. Thus we do not have enough evidence against the
hypothesis that the drug has no influence on these subjects. 

\section{Assumptions}

The assumptions are the same as in between subjects designs. We assume
that the true error terms $E_{i,j}$ are independent, and Gaussian with
identical variance for all subjects and treatments. However, contrary
to between subjects designs, in within subjects designs the F-test is
not very robust to violations of the assumptions of equal
variance. Thus we need to be extra careful about this assumption. In
general if the underlying variances are different, the true type I
error rate may be larger than 5 \%. Diagnosis methods and corrections
to this problem have been proposed but they are beyond the scope of
this book.

\section{A comment on why the interaction is a measure of error}

Note the $MS_{A \times S}$ plays the same role in within subjects
designs as $MS_{R/A}$ in between subjects designs. It estimates the
variance of the error terms in the model and thus it gives us an idea
of how much the observations are expected to vary. Figure \ref{fig:within.int},
illustrates why the interaction is a good measure of error in this
case. On the left side, we have a case in which subjects differ from
each other but they are practically identical in their response of the
drug. In this case the interaction will be very small, reflecting the
fact that the drug has very predictable effects within each
subject. On the right side we have a case in which subjects do not
differ in their average response but differ a lot in their response
to the drug. The drug is beneficial to some, neutral to others and
detrimental to others. This is a case where the effect of the drug is
hard to predict, a situation captured by the interaction between
subjects and treatments.


\begin{figure}[t]
\centerline{\psfig{file=figures/l19.f1.eps}}
\caption{Case 1 has small within subjects error, the effect of the
drug is predictable; Case 2 has large within subjects error, since the
effect of the drug is unpredictable}
\label{fig:within.int}
\end{figure}







\section{Order Effects}

A crucial assumption for the F-test to work is that all intervening
variables, other than the subject identity, are distributed in a
random, unsystematic manner. An important complication with within
subjects designs is that the mere fact of observing subjects more than
one time, creates new intervening variables.  For a review of methods
to control order effects take a look at Section
%\ref{sec:control-methods}



\section{Analytical Comparisons}

Analytical comparisons can be performed as in between subjects design
but substituting the $MS_{R/A}$ error term by the $MS_{A \times S}$
error term. In both cases these error terms are pooled estimates of
the error variance so they behave equivalently.


\section{Exercises}

\begin{enumerate}
\item {
{\sl A drug company tested whether vitamin B1 has a positive effect on
memory. Subjects were tested in two consecutive days. The first day,
by random assignment, half the subjects were given either 70 mgs of B1
and the other half were given a placebo for breakfast. Three hours
later they were asked to memorize for 5 minutes a list of 30
words. Immediately after the 5 minutes they were tested. A week later
the same procedure wa repeated. Those subjects who received B1 the
first time now receive a placebo and vice-versa. Moreover, on the
second week, all the words in the list were changed. The results were
as follows.}\\ 1 18 10\\ 2 12 7\\ 3 20 13\\ 4 16 13\\

The first column is the subject, second is the number of words remembered in the B1 condition, and third the number of words remembered in the placebo condition. 
\begin{enumerate}

\item Describe how the experimenters controlled for possible intervening variables.  

\item What is the population ?

\item Formalize your model of the observations.

\item Test the hypothesis that vitamin B1 has no effect. 

\item What assumptions did you make to do this test?




\end{enumerate}
}
\end{enumerate}


=========== This is stuff I cut out of chapter 1 =============

are
generally very efficient since they do require less subjects to obtain
the same number of observations. Unfortunately, there is no free lunch
here. Matching by subjects introduces {\bf order effects}, a new
intervening variable that needs to be controlled. If all our subject
receive treatment 1 first followed by treatment 2, it could well be
that our results are just due to practice or to lingering effects of
treatment 1 into treatment 2. Order effects are usually classified
into specific and unspecific.


{\bf Specific order effects} are order effects due to the
particular sequence of treatments a subject goes through.  This
differentiates them from general practice effects, which depend only
on position, regardless of the previous and posterior
treatments. Specific order effects are particularly important in
experiments involving the use of drugs.  For example, if we have two
treatments one involving a drug and one involving a placebo, we have
two possible arrangements: the drug goes first, or the placebo goes
first. Specific order effects occur because when the drug is
administered first, it may have carryover when the subject is
later given the placebo. This effect will be very different from the
effect of the placebo on the drug, if the placebo is administered
first.  In general specific order effects can only be controlled
by leaving sufficient time between the different treatments. In some situations the necessary lag between observations may be so large that it just more economical to use a between subjects design. 

{\bf Unspecific order effects} refer to systematic changes in performance
due to the fact that subjects repeat the same task several
times. Contrary to specific order effects, they depend only on the
position in which a treatment occurs, regardless which particular
treatments preceded it. The most common of these effects are
performance improvements due to practice and performance decline due
to fatigue and boredom. One approach to reduce practice effects is to
give enough pre-experiment trials so that there is not much room to
improvement due to practice. {\bf Boredom and fatigue effects} are
reduced by making the tasks as interesting as possible and by
providing enough resting periods. The most common form of control of
unspecific order effects is by {\bf Counterbalancing}.


\item{\bf Counterbalancing:} In repeated measures designs the order of presentation is an additional
intervening variable we need to control. One approach to control is to
hold constant. For example we could test all our subjects with the
neutral condition first followed by the placebo.  This may reduce
variability but it also reduces the generalizability of our
results. We will not be able to generalize to the case in which
subjects are given a placebo first followed by the drug.  Thus it is
important that a wide variety of orders of treatment be represented in
our studies. The methods used to maintain variety of orders while
controlling their effects are known as {\bf counterbalancing}. One way
to counterbalance presentation order is to make sure all orders occur
with equal frequency and are randomly assigned to the different
subjects. This is known as {\bf complete counterbalance}.  In general
the number of possible orderings of $a$ treatments is a-factorial
($a!$). Thus if there with 3 treatment conditions: $A_1, A_2, A_3$
there are $3! = 6$ treatment orders: $(A_1,A_2, A_3)$, $(A_1, A_3,
A_2)$, $(A_2,A_3,A_1)$, $(A_2,A_1,A_3)$,$(A_3,A_1,A_2)$,
$(A_3,A_2,A_1)$. Unfortunately, in many cases the number of possible
sequences of treatments is huge and we simply cannot use all of
them. For example with 10 different treatments the number of orders
goes up to $10!= 3,628,800$.

 When the number of possible sequences is very large, we may randomly
choose randomly assigning the order of presentation to subjects. This
is known as {\bf random counterbalancing}. Another approach is to
chose a set of representative sequences so that each treatment
condition occurs an equal number of times in each position. This type
of counterbalancing is known as {\bf incomplete}, or {\bf latin
square}. For example, if we have 3 treatment conditions, we could
randomly assign to subjects one of the 3 following sequences:
$(A_1,A_2,A_3)$, $(A_3,A_1,A_2)$, $(A_2,A_1,A_3)$. These sequences
were chosen so that each treatment occurs an equal number of times in
the first, second, and third positions. This particular
counterbalancing is also known as {\bf cyclical}, since the different
conditions are organized as in a loop: $A_1$ then $A_2$ then $A_3$ and
back to $A_1$. Cyclical counterbalancing assures that each condition
occurs an equal number of times in each position but it does not
balance the pairwise sequences. For example, in our case $a_1$ is
always followed by $A_2$, it is never followed by $A_3$ or $A_4$. In
general, if pairwise order is important, random counterbalancing may
be preferable.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End: 
