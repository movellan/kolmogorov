% Copyright \copyright{} 1996,,1998, 2002 Javier R. Movellan. 
% This is an open source document.  Permission is granted
% to copy, distribute and/or modify this document under the terms of the
% GNU Free Documentation License, Version 1.1 or any later version
% published by the Free Software Foundation; with no Invariant Sections,
% no Front-Cover Texts, and no Back-Cover Texts.

\chapter{Introduction to Statistical Hypothesis  Testing}

The goal of statistical hypothesis testing is to provide a rational
basis for making inferences and decisions about hypotheses based on
uncertain data.  For example based on the results of a experiment with
a limited sample of individuals we may want to decide whether there is
enough evidence to say that smoking causes cancer.  There are two
major approaches to statistical hypothesis testing: 1) The classic
approach, 2) The Bayesian approach. The classic approach is the
standard used when analysing scientific experiments. The Bayesian
approach is dominant in machine perception, artificial intelligence
and engineering applications.

\section{The Classic Approach}
Classic statisticians (also known as frequentists) view scientific
hypotheses as propositions with a fixed truth value. Within this
approach a hypothesis is either true or false and thus it makes no
sense to talk about its probability.  For example, for a frequentist
the hypothesis ``Smoking causes cancer'' is either true or false. It
makes no sense to say that there is a 99 \% chance that smoking causes
cancer. In this approach we perform experiments, gather data and if
the data provide sufficient evidence against a hypothesis we reject
the hypothesis.


Here is an example that illustrates how the classic approach works
in practice.  Suppose we want to disprove the hypothesis that a
particular coin is fair.  We toss the coin 100 times and get 74 \%
tails. We model our observations as a random variable $\bar{X}$ which
is the average of 100 iid Bernoulli ravs with unknown parameter $\mu$
\begin{equation}
\bar{X} = \frac{1}{100} \sum_{i=1}^{100} X_i
\end{equation}
The statement that we want to reject is called {\bf the null
  hypothesis}. In this case the null hypothesis is that $\mu=0.5$,
i.e., that the coin is fair.  We do not know $E(\bar{X})$ nor
$\Var(\bar{X})$. However we know what these two values would be if the null
hypothesis were true. We represent the conditional expected value of
$\bar{X}$ assuming that the null hypothesis is true as
$E(\bar{X}\given H_n \:\text{true})$. The variance of
$\bar{X}$ assuming that the null hypothesis is true is represented as
$\Var( \bar{X} \given H_n \:\text{true})$. In our example
\begin{align}
&E(\bar{X}\given H_n \:\text{true} ) =0.5\\
&\Var(\bar{X}\given H_n \:\text{true} )= 0.25/100
\end{align}
Moreover, due to the central limit theorem the cumulative distribution
of $\bar{X}$ should be approximately Gaussian. We note that a
proportion of 0.74 tails corresponds to the following standard score
\begin{equation}
Z = \frac{0.74-0.5}{\sqrt{0.25/100}} = 4.8
\end{equation}
Using the standard Gaussian tables we find 
\begin{equation}
P(\{ \bar{X} \geq 4.8\} \given H_n \:\text{true}) < 1/10^6
\end{equation}
If the null hypothesis were true the chances of obtaining 74 \% tails or
more are less than one in a million. We now have two alternatives: 1)
We can keep the null hypothesis in which case we would explain our
observations as an extremely unprobable event due to chance. 2) We can
reject the null hypothesis in view of the fact that if it were correct
the results of our experiment would be an extremely improbable event.
Sir Ronald Fisher, a very influential classic statistician, explained these
options as follows:

{\sl The force with which such conclusion is supported logically is
that of a simple disjunction: Either an exceptionally rare chance
event has occurred, or the theory or random distribution [the null
hypothesis] is not true } \cite{fisher56}

In our case obtaining 74 \% tails would be such a rare event if the
null hypothesis were true that we feel compelled to reject the null
hypothesis and conclude that the coin is loaded. But what if we had
obtained say 55 \% tails? Would that be enough evidence to reject the
null hypothesis? What should we consider as enough evidence? What
standards should we use to reject a hypothesis? The next sections
explain the classic approach to these questions. To do so it is
convenient to introduce the concept of Type I and Type II errors.



\section{Type I and Type II errors}


Jerzy Neyman and E.S. Pearson developed the bulk of the classic
approach to hypothesis testing between 1928 and 1933. Neyman and
Pearson emphasized hypothesis testing as a procedure to make decisions
rather than as a procedure to falsify hypothesis. In their own words

{\sl Without hoping to know whether each separate hypothesis is true
or false, we may search for rules to govern our behavior with regard
to them, in following which we insure that, in the long run
experience, we shall not be too often wrong.}  \cite{np33}


They viewed the  task of statistical hypothesis
testing as similar to the detection of signals in the presence of
noise. Let me illustrate this concept with the following
analogy. Suppose you are a fire detector. You are hanging up there in
the ceiling of a house and your task is to decide whether the house is
on fire.  Once in a while you measure the amount of smoke passing
through your sensors. If the amount is beyond a critical value you
announce the house residents that the house is on fire.  Otherwise you
stay quiet.


\begin{figure}[h!]
\centerline{\psfig{file=figures/l6.f1.eps}}
\caption{An illustration of the process of statistical hypothesis testing. The upper figure shows the distribution of smoke when there is no fire. The lower figure shows the distribution when there is fire. It can be seen that on average there is more smoke when there is fire but there is overlap between the two conditions. The task of hypothesis testing is to decide whether there is a  fire  based only on the amount of smoke  measured by the sensor. }
\end{figure}\label{f1}


In this analogy the null hypothesis is the theoretical possibility
 that the house is not on fire and the alternative hypothesis is that
 the house is on fire.  Measuring how much smoke there is out there is
 the equivalent of conducting an experiment and summarizing the results
 with some statistic.  The information available to us is imperfect
 and thus we never know for sure whether the house is or is not on
 fire. There is a myriad of intervening variables that may randomly
 change the amount of smoke.  Sometimes there is a lot of smoke in the
 house but there is no fire, sometimes, due to sensor failure, there
 may be fire but our sensors do not activate. Due to this fact we can
 make two types of mistakes:


\begin{enumerate}
\item We can have {\bf false alarms}, situations where there is no fire
but we announce  that there is
a fire. This type of error is known as {\bf a Type I
error}. In scientific research, type I errors occur when scientists
reject null hypotheses which in fact are true.

\item We can also {\bf miss} the fire. This type of error as {\bf Type
II error}. Type II errors occur when scientists do not reject null
hypothesis which in fact are false.

\end{enumerate}

Note that for type I errors to happen two things must occur:
\begin{center}
\begin{equation}
\begin{array}{|l|}\hline
 \\
 \mbox{(1) \emph{The null hypothesis must be true.}}\\
\\
2)\; \mbox{\emph{We reject it.}} \\
\\
\hline
\end{array}
\end{equation}
\end{center}


and for type II errors to happen two things must occur:

\begin{center}
\begin{equation}
\begin{array}{|l|}\hline
 \\
 \mbox{(1) \emph{The null hypothesis must be false.}} \\
\\
 \mbox{(2) \emph{We do not reject it.}} \\
\\
\hline
\end{array}
\end{equation}
\end{center}



\subsection{Specifications of a decision system}

The performance of a decision system can be specified in terms of its
potential to make errors when the null hypothesis is true and when the
null hypothesis is false.

\begin{itemize}
\item The {\bf type I error specification} is the probability of
  making errors when the null hypothesis is true. This specification
  is commonly represented with the symbol $\alpha$. For example if we
  say that a test has $\alpha \leq 0.05$ we guarantee that if the null
  hypothesis is true the test will not make more than 1/20 mistakes. 

  
\item The {\bf type II error specification } is the probability of
  making errors when the null hypothesis is false. This specification
  is commonly represented with the symbol $\beta$. For example if we
  say that for a test $\beta$ is unknown we say that we cannot
  guarantee how it will behave when the null hypothesis is actually
  false.


\item The {\bf Power specification} is the probability of correctly rejecting the null hypothesis when it is false. Thus the power specification is 1.0 - $\beta$. 
\end{itemize}
The current standard in the empirical sciences dictates that {\bf for a
scientific test to be acceptable the type I error specification has to
be smaller or equal to 1/20} (i.e., $\alpha \leq 0.05$). The standard
does not dictate what the type II error specification should be. In
the next chapter we will see examples of statistical tests that meet
this type I error specification. 



\section{The Bayesian approach}

The essence of the Bayesian approach can be summarized as follows:

\begin{itemize}

\item{A recognition of the fact that humans not only make binary
decisions about hypotheses but also  want to assign degrees of belief to
the different hypotheses}

\item{An assumption that probability is useful to describe  beliefs not just relative frequencies. }

\item{An emphasis on the problem of how to combine external data with prior knowledge to modify beliefs. }

\end{itemize}
To Fisher's statement that
\begin{quotation}
{\sl
``It should be possible to draw
conclusions from the data alone, without apriori assumptions.''
}
\cite{fisher34}, 
\end{quotation}
L. Savage, a well known Bayesian replies that
\begin{quotation}
``{\sl We had a slogan about letting the data speak for themselves, but
when they do, they tell us how to modify our opinions, not what
opinion is justifiable.}''  \cite{savage62}
\end{quotation}
In practice the main difference between Bayesians and frequentists is
that Bayesians treat hypotheses as if they were probabilistic events
and thus are willing to assign them probability values.  Here is an
example of how the Bayesian approach would work in practice.
\paragraph{Example:}
A doctor believes that a patient has a 10\% chance of having Lyme
disease.  She gives the patient a blood test and the test comes out
positive. The manual for this test says that that out of 100 patients
with Lyme disease, 80 \% test positive.  Moreover, out of 100 patients
with no Lyme disease 30 \% test positive. What is the probability that
the patient has Lyme disease?
\paragraph{Answer:} If you were a pure classic you would be unwilling
to answer this question. You would simply say that probabilities should
not be applied to empirical hypotheses. This person either has Lyme
disease or he does not. You would simply say that your tools do not
apply to this problem. If you were Bayesian you would be willing to
use probability theory to play with knowledge and internal beliefs so
this question makes sense to you. You could represent the hypothesis
that the patient has Lyme disease as an event $H_1$ which has a prior
probability of 10 \%.
\begin{align}
&P(H_1) = 0.1\\
&P(H_0) = 1 - P(H_1) = 0.9
\end{align}
where $H_0$ represents the hypothesis that the patient does not have
Lyme disease. The positive test is a data event $D$ with the following
characteristics
\begin{align}
&P(D \given H_1) = 0.8\\
&P(D \given H_0) = 0.3
\end{align}
 Now you could apply Bayes' theorem to compute the
probability of the hypotheses given the data
\begin{equation}
P(H_1 \given D) = \frac{ (0.8)(0.1)}{(0.8)(0.1)+(0.3)(0.9)} = 0.23
\end{equation}
After seeing the results of the test, it would be rational for the
Doctor to update her beliefs and give her patient a 23 \% probability
of having Lyme disease. Note the emphasis here is on upgrading beliefs
based on empirical data. The emphasis is not on deciding whether a
hypothesis is true or false. Of course it is now up to the doctor and
her patient to use the 23 \% probability figure to perhaps get a
better test or to evaluate the costs and benefits of treatments.

% I chopped a section on the limitations of the current classic standard

\section{Exercises} 
\begin{enumerate} 
\item Urn A contains 50\% black balls 50 \% white balls. Urn B contains 45 \% black balls 55 \% white balls.  You get a sample of n randomly selected balls. All balls in the sample belong to the same urn but you do not know which one. Your task is to decide which urn the sample belongs to. Let the null hypothesis be the idea that the sample comes from Urn A. 

\begin{enumerate}
\item Suppose there are 10 balls in the sample (i.e., n = 10). 

\begin{enumerate}
\item What would  the critical value be for a Type I error specification of 1/20 ?
\item What would the power specification be? NOTE: in this case we can calculate the power because it is possible to find the distribution of the mean assuming the alternative hypothesis is true. In general this may not be possible.

\item What would the critical value be for a Type I error specification of 1/100 ?

\item What would the power specification be if we use a Type I error specification of 1/100?

\end{enumerate}


\item Same as the previous 4 questions but using $n=20$ instead of $n=10$. What did you learn out of this exercise?

\end{enumerate}




\item True or false (justify your response): 
\begin{enumerate}

\item Due to the current standards we should expect one out of 20 scientific
studies to be wrong.

\item An experiment results in a p-value of 0.03, another experiment in a p-value of 0.0001.  Using the current 0.05 standard in both experiments we reject the null hypothesis. Moreover, in the second experiment there is a smaller chance of being wrong by rejecting the null hypothesis.


\item An experiment results in a p-value of 0.00001. Therefore the type I error specification   is 0.00001.

\item An experiment results in a p-value of 0.5.   The probability of making a type I error in this experiment is unknown.

\item An experiment results in a p-value of 0.5.   The power specification of this experiment is 0.5.


\item An experiment results in a p-value of 0.01. The probability of making a type II error in this experiment is unknown.

\item An experiment results in a p-value of 0.01. The probability of making a type I error in this experiment is unknown.

\item An experiment results in a p-value of 0.01. The type I error specification is 0.05.


\item An experiment results in a p-value of 0.01. The probability of making a type II error in this experiment zero. 


\item An experiment results in a p-value of 0.01. The probability of making a type I error in this particular experiment is either 1 or 0. 

\end{enumerate}




\end{enumerate}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End: 
