% Copyright \copyright{} 1996,,1998, 2002 Javier R. Movellan. 
% This is an open source document.  Permission is granted
% to copy, distribute and/or modify this document under the terms of the
% GNU Free Documentation License, Version 1.1 or any later version
% published by the Free Software Foundation; with no Invariant Sections,
% no Front-Cover Texts, and no Back-Cover Texts.

\chapter{\bf Random variables}

Up to now we have studied probabilities of sets of outcomes. In
practice, in many experiment we care about some numerical property of
these outcomes. For example, if we sample a person from a particular
population, we may want to measure her age, height, the time it takes
her to solve a problem, etc. Here is where the concept of a random
variable comes at hand. Intuitively, we can think of a random variable
(rav) as a numerical measurement of outcomes. More precisely, a random
variable is a rule (i.e., a function) that associates numbers to
outcomes. In order to define the concept of random variable, we first
need to see a few things about functions.

\paragraph{ Functions:}

Intuitively a function is a rule that associates members of two
sets. The first set is called the {\bf domain} and the second set is
called the {\bf target} or {\bf codomain}. This rule has to be such
that an element of the domain  should not be associated to more
than one element of the codomain. Functions are described using the
following notation
\begin{equation}
f \st A \to B
\end{equation}
where $f$ is the symbol identifying the function, $A$ is the domain
and $B$ is the target. For example, $h \st \R \to \R$ tells us that $h$
is a function whose inputs are real numbers and whose outputs are also
real numbers. The function $h(x) = (2)(x) + 4$ would satisfy that
description. Random variables are {\bf functions} whose domain is the 
outcome space and whose codomain is the real numbers. In practice we can
think of them as numerical measurements of outcomes. The input to a
random variable is an elementary outcome and the output is a number.

\paragraph{Example:}
Consider the experiment of tossing a fair coin twice.  In this case
the outcome space is as follows:
\begin{equation}
\Omega = \{(H,H), (H,T), (T,H), (T,T)\} \:.
\end{equation}
 One possible way to assign numbers to
these outcomes is to count the number of heads in the outcome. I will
name such a function with the symbol $X$, thus $X \st \Omega \to \R$
and
\begin{equation}\label{eqn:erava}
X(\omega) = \begin{cases}
0 & \text{if $\omega = (T,T)$}\\
1 & \text{if $\omega = (T,H)$ or $\omega = (H,T)$}\\
2 & \text{if $\omega = (H,H)$}
\end{cases}
\end{equation}
In many cases it is useful to  define sets of $\Omega$ using the outcomes
of the random variable $X$. For example the set $\{\omega \st
X(\omega) \leq 1\}$ is the set of outcomes for which $X$ associates a
number smaller or equal to $1$. In other words

\begin{equation}
\{\omega \st X(\omega) \leq 1\} = \{(T,T), (T,H), (H,T)\}
\end{equation}
 Another possible random variable for this experiment may measure whether the first element of an  outcome is ``heads''. I will denote this random variable with the letter $Y_1$. Thus $Y_1 \st \Omega \to \R$ and
\begin{equation}
Y_1(\omega) = \begin{cases}
0 &\text{if $\omega = (T,T)$ or $\omega = (T,H)$}\\
1 & \text{if $\omega = (H,H)$ or $\omega = (H,T)$}\\
\end{cases}
\end{equation}
Yet another random variable, which I will name $Y_2$ may tell us whether the second element of an outcome is heads. 
\begin{equation}
Y_2(\omega) = \begin{cases}
0 & \text{if $\omega = (T,T)$ or $\omega = (H,T)$}\\
1 & \text{if $\omega = (H,H)$ or $\omega = (T,H)$}\\
\end{cases}
\end{equation}
We can also describe relationships between random variables. For example,  for all outcomes $\omega$ in $\Omega$ it is true that 
\begin{equation}
X(\omega) = Y_1(\omega) + Y_2(\omega) 
\end{equation}
This relationship is represented  succinctly as 
\begin{equation}
X = Y_1 + Y_2
\end{equation}
\paragraph{Example:}
Consider an experiment in which we select a sample of 100 students
from UCSD using simple random sampling (i.e., all the students have
equal chance of being selected and the selection of each students does
not constrain the selection of the rest of the students).  In this
case the sample space is the set of all possible samples of 100
students. In other words, each outcome is a sample that contains 100
students.\footnote{The number of possible outcomes is $n!/(100!\,(n-100)!)$
  where $n$ is the number of students at UCSD.} A possible
random variable for this experiment is the height of the first student
in an outcome (remember each outcome is a sample with 100 students).
We will refer to this random variable with the symbol $H_1$. Note
given an outcome of the experiment, (i.e., a sample of 100 students)
$H_1$ would assign a number to that outcome. Another random variable
for this experiment is the height of the second student in an outcome.
I will call this random variable $H_2$. More generally we may define
the random variables $H_1, \ldots, H_{100}$ where $H_i \st \Omega \to
\R$ such that $H_i(\omega)$ is the height of the subject number $i$ in
the sample $\omega$. The average height of that sample would also be a
random variable, which could be symbolized as $\bar{H}$ and defined as
follows
\begin{equation}
\bar{H}(\omega) = \frac{1}{100}( H_1(\omega) + \cdots + H_{100}(\omega))
 \; \text{for all $\omega \in \Omega$}
\end{equation}
or more succinctly 
\begin{equation}
\bar{H} = \frac{1}{100}( H_1 + \cdots + H_{100})
\end{equation}
I want you to remember  that all these {\bf random variables
are not numbers}, they are functions (rules) that assign numbers to
outcomes. The output of these functions may change
with the outcome, thus the name random
variable.
 
\paragraph{Definition}
A random variable $X$ on a probability space $(\Omega, \F, P)$ is a
function $X:
\Omega \to \R$. The domain of the function  is the outcome space and the target is the real numbers.\footnote{Strictly
speaking the function has to be Borel measurable (see Appendix), in practice the
functions of interest to empirical scientists are Borel
measurable so for the purposes of this book we will not worry about
this condition.}

\paragraph{Notation:}
By convention random variables
are represented with capital letters. For example $X: \Omega \to
\R$, tells us that $X$ is a random variable. Specific values of a random variable are represented with small letters. For example,  $X(\omega) = u$ tells us that the ``measurement'' assigned to the outcome $\omega$ by the random variable $X$ is $u$.  Also I will represents sets like 
\begin{equation}
\{\omega : X(\omega) =  u\}
\end{equation}
with the simplified notation 
\begin{equation}
\{X = u \}
\end{equation}
I will also denote probabilities of such sets in a simplified, yet misleading, way. For example, the simplified notation
\begin{equation}
P( X= u) 
\end{equation}
or 
\begin{equation}
P(\{X = u\}) 
\end{equation}
will stand for 
\begin{equation}
P(\{\omega : X(\omega) =  u\}) 
\end{equation}
Note the simplified notation is a bit misleading since for example $X$
cannot possibly equal $u$ since the first is a function and the second is a
number. 

\paragraph{Definitions:}
\begin{itemize}
\item A random variable $X$ is {\bf discrete} if there is a countable
  set or real numbers $\{x_1, x_2,  \ldots \}$ such that $P(X \in
  \{x_1, x_2, \ldots \})$ = 1.

\item A random variable $X$ is {\bf continuous} if for all real numbers $u$ the probability that $X$ takes that value is zero. More formally, for all $u \in \R$, $P(X = u) = 0$.

\item A random variable $X$ is {\bf mixed} if it is not continuous and it is not discrete.
\end{itemize}

\section{Probability mass functions.}

A probability mass function is a function $p_X: \R \to [0,1]$ such
that

\begin{equation}
p_X(u) = P(X =u) \;\text{for all $u \in \R$}.
\end{equation}
\paragraph{Note:} If the random variable is continuous, then $p_X(u)=0$ for all values of $u$. Thus, 
\begin{equation}
\sum_{u \in \R} p_X(u) = \begin{cases}
0 &\text{if $X$ is continuous}\\
1 &\text{if $X$ is discrete}\\
\text{neither $0$ nor $1$} &\text{if $X$ is mixed}
\end{cases}
\end{equation}
where the sum is done over the entire set of real numbers. What
follows are some important examples of discrete random variables and
their probability mass functions.
\paragraph{Discrete Uniform Random Variable:}
A random variable $X$ is discrete uniform is there is a finite set of
real numbers $\{x_1,\ldots, x_n\}$ such that
\begin{equation}
p_X(u) = \begin{cases}
1/n &\text{if $u \in \{x_1, \ldots, x_n\}$}\\
0           &\text{else}
\end{cases}
\end{equation}
For example a uniform random variable that assigns probability 1/6 to the numbers $\{1,2,3,4,5,6\}$ and zero to all the other numbers could be used to model the behavior of fair dies. 
\paragraph{Bernoulli Random Variable:}
Perhaps the simplest random variable is the so called {\bf Bernoulli}
random variable, with parameter $\mu \in [0,1]$. The Bernoulli random
variable has the following probability mass function

\begin{equation}
p_X(y)= 
\begin{cases}
\mu& \text{ if $y = 1$ } \\
1 - \mu& \text{ if $y= 0$ } \\
0& \text{ if $y \neq 1$ and $y \neq 0$ } 
\end{cases}
\end{equation}
For example, a Bernoulli random variable with parameter $\mu=0.5$ could
be used to model the behavior of a random die. Note such variable
would also be discrete uniform.
\paragraph{Binomial Random Variable:}
A random variable $X$ is binomial with parameters $\mu\in[0,1]$ and $n \in \N$ if its probability mass function is as follows
\begin{equation}
p_X(y) = \begin{cases}
 \binom{n}{y} \mu^y(1- \mu)^{n-y} &\text{if $u \in \{0,1,2,3, \ldots\}$}\\
0 &\text{else}
\end{cases}
\end{equation} 
Binomial probability mass functions are used to model the probability
of obtaining $y$ ``heads'' out of tossing a coin $n$ times. The
parameter $\mu$ represents the probability of getting heads in a
single toss. For example if we want to get the probability of getting
9 heads out of 10 tosses of a fair coin, we set $n=10$, $\mu=0.5$
(since the coin is fair).
\begin{equation}
p_X(9) = \binom{10}{9} (0.5)^9 (0.5)^{10-9} = \frac{10!}{9! (10-9)!} (0.5)^{10} = 0.00976
\end{equation}
\paragraph{Poisson Random Variable}
A random variable $X$ is Poisson with parameter $\lambda >0$ if its
probability mass function is as follows
\begin{equation}
p_X(u) = \begin{cases}
\frac{\lambda^u e^{-\lambda}}{u!} &\text{if $u\geq0$}\\
0 &\text{else}
\end{cases}
\end{equation} 
Poisson random variables model the behavior of random phenomena that
occur with uniform likelihood in space or in time. For example,
suppose on average a neuron spikes 6 times per 100 millisecond. If
the neuron is Poisson then the probability of observing 0 spikes in a
100 millisecond interval is as follows
\begin{equation}
p_X(0) = \frac{6^0  e^{-6}}{0!} =0.00247875217
\end{equation}

\section{Probability density functions.}




The probability density of a random variable $X$ is a function $f_X:
 \R \to \R$ such that for all real numbers $a>b$ the probability that
 $X$ takes a value between $a$ and $b$ equals the area of the function
 under the interval $[a,b]$. In other words

\begin{equation}
P(X \in [a,b] ) = \int_a^b f_X(u) du
\end{equation}

Note if a random variable has a probability density function (pdf) then
\begin{equation}
P(X=u) = \int_{u}^u f_X(x) dx =0\; \text{for all values of $u$}
\end{equation}
and thus the random variable is continuous. 


\paragraph{\bf Interpreting probability densities:} 
If we take an interval very small the area under the interval can be approximated as a rectangle, thus, for small $\Delta x$
\begin{align}
&P({X \in (x , x+ \Delta x]})  \approx f_X(x) \Delta x\\
&f_X(u) \approx \frac{P({X \in (x , x+ \Delta x]})}{\Delta x}
\end{align}
Thus the probability density at a point can be seen as the amount of
probability per unit length of a small interval about that point. It is
 a ratio between two different ways of measuring a small
interval: The probability measure of the interval and the length (also
called Lebesgue measure) of the interval. What follows are examples of important continuous random variables and their probability density functions. 

\paragraph{Continuous Uniform Variables:}
A random variable $X$ is continuous uniform in the interval $[a,b]$, where $a$ and $b$ are real numbers such that $b>a$, if its pdf is as follows;
\begin{equation}
f_X(u) = \begin{cases}
1/(b-a) &\text{if $u \in [a,b]$}\\
0 &\text{else}
\end{cases}
\end{equation}
Note how a probability density function can take values larger than 1. For example, a uniform random variable in the interval $[0,0.1]$ takes value 10 inside that interval and 0 everywhere else.

\paragraph{Continuous Exponential Variables:}

A random variable $X$ is called exponential if it has the following pdf
\begin{equation}
f_X(u) =
\begin{cases}
0  \text{ if $u < 0$ } \\
\lambda \exp(-\lambda x) & \text{ if $u \geq 0$ } 
\end{cases}
\end{equation}
we can calculate the probability of the interval $[1,2]$ by integration
\begin{equation}
P(\{X \in [1,2]\} ) = \int_1^2 \lambda \exp(- \lambda x) dx = \Big[ - \exp(-\lambda x ) \Big]_1^2
\end{equation}
if $\lambda=1$ this probability equals $\exp(-1) - \exp(-2) = 0.2325  $.

\paragraph{Gaussian Random Variables:}
A random variable $X$ is Gaussian, also known as {\bf normal}, with parameters $\mu \in \R$ and $\sigma^2>0$ if its pdf is as follows
\begin{equation}
f(x) = \frac{1}{\sqrt{2 \pi \sigma^2} } \exp( -\frac{1}{2} ( \frac{x - \mu}{\sigma})^2)
\end{equation}
where $\pi = 3.1415 \cdots$, $\mu$ is a parameter that controls the location of the center of the function  and $\sigma$ is a parameter than controls the spread of the function.. Hereafter whenever we want to say that a
random variable $X$ is normal with parameters $\mu$ and  $\sigma^2$
we shall write it as $X \sim  N(\mu, \sigma^2)$. If a Gaussian random
variable $X$ has zero mean and standard deviation equal to one, we say
that it is a {\bf standard Gaussian random variable}, and represent it 
$X  \sim  N(0,1)$.

%  Figure \ref{fig:gaussians} shows two Gaussian
%density functions with different means and standard deviation.

%\begin{figure}[h]\label{fig:gaussians}
%\centerline{\psfig{file=figures/l4.f2.eps,height= 2in}}
%\caption{Two different Gaussian distributions} 
%\end{figure}
The Gaussian pdf is very important because of its ubiquitousness  in nature thus  the name ``Normal''. The underlying reason why this distribution is so widespread in nature is explained by an important theorem known as {\bf the central limit theorem}. We will not prove this theorem here but it basically says that observations which are the result of a sum of a large number of  random and independent influences have a cumulative distribution function closely approximated by that of a Gaussian random variable.  Note this theorem applies to many natural observations: Height, weight, voltage fluctuations, IQ... All these variables are the result of a multitude of effects which when added up make the observations distribute approximately Gaussian.


One important property of Gaussian random variables is that linear
combinations of Gaussian random variables produce Gaussian random
variables. For example, if $X_1$ and $X_2$ are random variables, then
$Y=2+ 4 X_1 +6 X_2$ would also be a Gaussian random variable.




\section{Cumulative Distribution Function.}

The cumulative distribution function, of a random variable $X$ is a
function $F_X: \R \to [0,1]$ such that

\begin{equation}
F_X(u) = P(\{X \leq u\}) 
\end{equation}

For example, for the random variable described in \eqref{eqn:erava} 

\begin{equation}
F_X(u)= 
\begin{cases}
0.0& \text{ if $u < 0.0$ } \\
1/4& \text{ if $u \geq 0.0$ and $u < 1$} \\
3/4& \text{ if $u \geq 1$ and $u < 2$ } \\
1.0& \text{ if $u \geq 2$ } 
\end{cases}
\end{equation}
The relationship between the cumulative distribution function, the probability mass function and the probability density function is as follows:

\begin{equation}
F_X(u) = \begin{cases}
\sum_{x \leq u} p_X(u) &\text{if $X$ is a discrete random variable}\\
\int_{-\infty}^u f_X(x) dx  &\text{if $X$ is a continuous random variable}
\end{cases}
\end{equation}

\paragraph{Example:}

To calculate the  cumulative distribution function of a continuous exponential
random variable $X$ with parameter $\lambda >0$ we integrate the exponential pdf
\begin{equation}
F_X(u)= \begin{cases}
\int_{0}^u \lambda \exp(-\lambda x) dx &\text{if $u\geq 0$}\\
0 &\text{else} 
\end{cases}
\end{equation}
And solving the integral
\begin{equation}
\int_{0}^u \lambda \exp(-\lambda x) dx = \biggl[ - \exp(-\lambda x)\biggl]^u_0 
= 1 - \exp(- \lambda u)
\end{equation}
Thus the cumulative distribution of an exponential random variable is
as follows
\begin{equation}
F_X(u)= \begin{cases}
 1 - \exp(- \lambda u) &\text{if $u\geq 0$}\\
0 &\text{else} 
\end{cases}
\end{equation}
\paragraph{Observation:}
We have seen that if a random variable has a probability density function then the cumulative density function can be obtained by integration. Conversely we can differentiate the cumulative distribution function to obtain the probability density function
\begin{equation}
f_X(u) = \frac{d F_X(u)}{du}
\end{equation}

\paragraph{A Property of Cumulative distribution Functions:}
Here is a property of cumulative distribution which has important
applications. Consider a random variable $X$ with cumulative
distribution $F_X$ now suppose we define a new random variable $Y$
such that for each outcome $\omega$
\begin{equation}
Y(\omega) = a + b X(\omega)
\end{equation}
where $a$ and $b\neq 0$ are real numbers. More succinctly we say
\begin{equation}
Y = a + b X
\end{equation}
If we know the cumulative distribution of $Y$ we can easily derive the cumulative distribution of $Y$.
\begin{equation}\label{eqn:cdftransform}
F_Y(u) = P(Y \leq u) = P(a+ bX \leq u ) = P(X \leq \frac{ u - a}{b}\}) = F_X(\frac{ u - a}{b})
\end{equation}



\paragraph{Example:}
Let $X$ be an exponential random variable with parameter $\lambda$, i.e.,  
\begin{equation}
F_X(u) = 
\begin{cases}
0 &  \text{ if $u < 0$ } \\
1 - \exp( - \lambda u )& \text{ if $u \geq 0$ } 
\end{cases}
\end{equation}
Let $Y= 1+ 2X$. In this case $a= 1$ and $b=2$. Thus the cumulative distribution of $Y$ is
\begin{equation}
F_Y(u) = F_X((u -1)/2)   =
\begin{cases}
0 &  \text{ if $(u-1)/2 < 0$ } \\
1 - \exp( - \lambda (u-1)/2 )& \text{ if $(u-1)/2 \geq 0$ } 
\end{cases}
\end{equation}





  





\section{Exercises}
\begin{enumerate}

\item Distinguish the following standard symbols $P$, $p_X$, $F_X$, $f_X$, $X$, $x$

\item Find the cumulative distribution function of a Bernoulli random variable  $X$ with parameter $\mu$.


\item  Find the cumulative distribution of a uniform random variable with parameters $a,b$.

\item Consider a uniform random variable in the interval $[0,1]$. 
\begin{enumerate}
\item Calculate the probability of the interval $[0.1, 0.2]$
\item Calculate the probability of 0.5.
\end{enumerate}


\item Let $X$ be a random variable with probability mass function
\begin{equation}
p_X(u) = \begin{cases}
1/5 & \text{if $u \in \{1,2,3,4,5\}$}\\
0   & else
\end{cases}
\end{equation}

\begin{enumerate}

\item Find $F_X(4)$ 
\item Find $P(\{X \leq  3\} \inter \{X \leq 4\})$ 
\item Plot the function $h_X(t) = P(\{X = t\} \given \{X \geq t\})$
for $t=1,2,3,4,5$.  This function is commonly known as the ``hazard
function'' of $X$. If you think of $X$ as the life time of a system,
$h_X(t)$ tells us the probability that the system fails at time $t$
given that it has not failed up to time $t$. For example, the hazard
function of human beings looks like a U curve with a extra bump at the
teen-years and with a minimum at about 30 years. 

\end{enumerate}

\item Let $X$ be a continuous random variable with pdf

\begin{equation}
f_X(u) = \begin{cases}
0.25 &\text{if $u \in [-3, -1]$}\\
0.25 &\text{if $u \in [1,3]$}\\
0 &\text{else}
\end{cases}
\end{equation}
\begin{enumerate}
\item Plot $f_X$. Can it be a probability density function? Justify your response. 

\item Plot $F_X$ 


\end{enumerate}

\item Show that if $X$ is a continuous random variable with pdf $f_X$ and $Y = a + bX$ where  and $a,b$ are real numbers. Then 
\begin{equation}
f_Y(v) = (1/b) f_X(\frac{u-a}{b})
\end{equation}
hint: Work with cumulative distributions and then differentiate them to get densities. 

\item Show that if $X \sim N(\mu, \sigma^2)$ then $Y = a + bX$ is Gaussian. 

\end{enumerate}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End: 
