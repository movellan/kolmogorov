% Copyright \copyright{} 1996,,1998, 2002 Javier R. Movellan. 
% This is an open source document.  Permission is granted
% to copy, distribute and/or modify this document under the terms of the
% GNU Free Documentation License, Version 1.1 or any later version
% published by the Free Software Foundation; with no Invariant Sections,
% no Front-Cover Texts, and no Back-Cover Texts.


%We need a chapter on sampling
\chapter{The precision of the arithmetic  mean  } 

Consider taking the average of a number of  observations
all of which are randomly sampled from a large population.
Intuitively it seems that as the number of observations increases this
average will get closer and closer to the true population mean.  In this
chapter we quantify how close the sample mean gets to the
population mean as the number of observations in the sample
increases. In other words we want to know how good  the mean of a sample of noisy observations is as an estimate of the ``true'' value underlying those observations. 

Let's formalize the problem using our knowledge of probability
theory. We start with $n$ {\em independent} random variables variables $X_1,
\ldots, X_n$. We will also assume that all these random variables have the same mean, which we will represent as $\mu$  and the same variance, which we will represent as $\sigma^2$. In other words 
\begin{equation}
E(X_1) = E(X_2) = \cdots = E(X_n) = \mu
\end{equation}
and
\begin{equation}
\Var(X_1) = \Var(X_2) = \cdots = \Var(X_n) = \sigma^2
\end{equation}
This situation may occur, when our experiment consists of randomly
selecting $n$ individuals from a population (e.g., 20 students from
UCSD). In this case the outcomes of the random experiment consists of
a sample of $n$ subjects. The outcome space is the set of all possible
samples of $n$ subjects.  The random variable $X_i$ would represent a
measurement of subject number $i$ in the sample. Note that in this
case all random variables have the same mean, which would be equal to
the average observation for the entire population. They also have the
same variance, since all subjects have an equal chance of being in any
of the $n$ possible positions of the samples.

Given a sample $\omega$ we can average the $n$ values taken by the random variables for that particular sample. We will represent this average as $\bar{X}_n$,
\begin{equation}\label{eqn:meanrav}
\bar{X}_n(\omega) = \frac{1}{n} \sum_{i=1}^n X_i(\omega); \;\;\text{for all $\omega \in \Omega$}
\end{equation}
More concisely,
\begin{equation}
\bar{X}_n = \frac{1}{n} \sum_{i=1}^n X_i
\end{equation}
Note that $\bar{X}_n$ is itself a random variable (i.e., a function from
the outcome space to the real numbers). We call this random variable the
sample mean. 

One convenient way to measure the precision of the sample mean is to
compute the expected squared deviation of the sample mean from the
true population mean, i.e., 
\begin{equation}\label{eqn:efficomean}
\sqrt{E[( \bar{X}_n - \mu)^2]}
\end{equation}
For example, if $\sqrt{E[( \bar{X}_n - \mu_Y)^2]}=4.0 secs$ this would
tell us that on average the sample mean deviates from the population
mean by about 4.0 secs. 

\section{The sampling distribution of the mean}
We can now use the properties of expected values and variances to
derive the expected value and variance of the sample mean .

\begin{equation}
E(\bar{X}_n ) = \frac{1}{n} \sum_{i=1}^n E(X_i) = \mu
\end{equation}
thus the expected value of the sample mean is the actual population
mean of the random variables.  A consequence of this is that
\begin{equation}
E[( \bar{X}_n - \mu)^2] = E[( \bar{X}_n - E(\bar{X}_n) )^2] = \Var(\bar{X}_n) 
\end{equation}
This takes us directly to our goal. Using the properties of the
variance, and considering that the random variables $X_1, \ldots, X_n$
are independent we get
\begin{equation}
Var (\bar{X}_n ) = \frac{1}{n^2} \sum_{i=1}^n \Var(X_i)  = \frac{\sigma^2}{n}
\end{equation}
The standard deviation of the mean is easier to interpret than the
variance of the mean. It roughly represents how much the means of
independent randomly obtained samples typically differ from the population
mean. Since the standard deviation of the mean is so important, many
statisticians give it a special name: the {\bf standard error of the
mean}. Thus the standard error of the mean simply is the square root
of the variance of the mean, and it is represented as
$\Sdev({\bar{X}_n})$ or $Se(\bar{X}_n)$. Thus,
\begin{equation}\label{eqn:semean}
\Sdev(\bar{X}_n) = \frac{\sigma}{\sqrt{n}} = \sqrt{E[( \bar{X}_n - \mu)^2]}
\end{equation}
Finally we are done! Equation \ref{eqn:semean} tells us that the
uncertainty of the sample mean (its standard deviation) increases
proportionally to the uncertainty about  individual observations
($\sigma$) and
decreases proportionally to the square root of the number of
observations. Thus, if we want to double the precision of the sample
mean (reduce its standard deviation by half) we need to quadruple the
number of observations in the sample.  

\paragraph{Example:}
Let $X_1$ and $X_2$ be independent Bernoulli random variables with
parameter $\mu = 0.5$. Thus $E(X_1) = E(X_2) = \mu=0.5$ and $\Var(X_1) =
\Var(X_2) = \sigma^2=0.25$, moreover
\begin{equation}
E(\bar{X}_2) = (0)(0.25)+(0.5)(0.5)+(1)(0.25) = 0.5 = \mu
\end{equation}
\begin{equation}
\Var(\bar{X}_2) = \sigma^2/2 = 0.125
\end{equation}


\subsection{Central Limit Theorem}
This is a very important theorem which we will not prove here. The
theorem tells us that  as $n$ goes to infinity, the {\sl cumulative distribution} of $\bar{X}_n$ is closely approximated by that of a Gaussian random variable with mean $E(\bar{X_n})$ and standard deviation $\Sdev(\bar{X}_n)$. In practice for $n\geq 30$ the Gaussian cumulative distribution provides very good approximations. 
\paragraph{Example:} We toss a fair coin 100 times. What is the
probability that the proportion of ``heads'' be smaller or equal to
0.45? 
\paragraph{Answer:} Tossing a fair coin 100 times can be modeled using 100
independent identically distributed Bernoulli random variables each of
which has parameter $\mu=0.5$. Let's represent these 100 random
variables as $X_1, \ldots, X_{100}$ where $X_i$ takes the value 1 if the
$i^{th}$ time we toss the coin we get heads. Thus the proportion of
heads is the average of the 100 random variables, we will represent it
as $\bar{X}$.
\begin{equation}
\bar{X} = \frac{1}{100}(X_1 + \cdots + X_{100})
\end{equation}
We know 
\begin{equation}
\mu = E(X_1) = \cdots = E(X_{100}) = 0.5
\end{equation}
\begin{equation}
\sigma^2 = \Var(X_1) = \cdots = \Var(X_{100}) = 0.25
\end{equation}
Thus
\begin{align}
&E(\bar{X}) = 0.5\\
&\Var(\bar{X}) = \sigma^2/100 = 0.0025
\end{align}
Since $n=100 > 30$ the cumulative distribution of $\bar{X}$ is
approximately Gaussian. Thus
\begin{equation}
P( \bar{X} \leq 0.45) = F_{\bar{X}}(0.45) \approx
\Phi(\frac{0.45-0.5}{\sqrt{0.0025}}) = \Phi(-1) = 0.1586
\end{equation}


\section{Exercises}

\begin{enumerate}


\item {\bf The Central Limit Theorem}

\begin{enumerate}

\item Goto the book Web site and click on LinuStats $\rightarrow$ Coin Simulator

\item Do 1000 replications of a coin tossing experiment. Each
experiment should consists of 10 coin tosses (10 observations per
experiment). The result should be 1000 numbers each of which
represents the proportion of tails obtained in a particular
experiment.  If we encoded Heads as ``0'' and tails as ``1'', the
outcome of a coin toss is an outcome from a Bernoulli 0,1 random
variable with parameter $\mu=0.5$. The proportion of tails obtained
in 10 tosses is the average of 10 independent identically distributed
Bernoulli random variables.

\begin{equation}
\bar{X} = \frac{ \sum_{i=1}^{10} X_i}{10}
\end{equation}
This average is itself a random variable. Moreover the central limit
theorem tell us that it should be approximately Gaussian. We will
check that now:

\item Use the Edit menu in your browser to copy the 1000 numbers
obtained in the coin tossing experiments.

\item Go back to LinuStats and choose the descriptive statistics page.

\item Use the Edit menu in your browser to paste the 1000 numbers into
the data window.

\item Choose a minimum value of 0, maximum value of 1, and window of 0.1

\item Analyze the data by clicking the appropriate button. 

\item Copy the resulting relative frequency table and transform it into a cumulative distribution function


\item Copy the Mean and SD (Standard Deviation) of the 1000
numbers. Explain what this standard deviation means. I'll call these
numbers $\mu_{\bar{X}}$ and $\sigma_{\bar{X}}$

\item Compare the obtained cumulative distribution function with that predicted by a Gaussian distribution with mean $\mu_{\bar{X}}$ and standard deviation $\sigma_{\bar{X}}$. 


\end{enumerate}

\item{\bf The sampling distribution of the mean}
\begin{enumerate}

\item Go to the coin simulator and do 1000 experiments each with 1
toss (i.e., one observation per experiment).  Using the same procedure
as in the previous exercise, calculate the mean and standard deviation
of the 1000 experiments. Plot the relative frequency density
polygon. Interpret your results.

\item Same as the previous question but now toss the coin 2 times per
experiment. Obtain the new mean and standard deviation and interpret
what they mean.

\item Same with 4, 16 and with 32 observations per experiment. 


\item Plot a graph with the obtained means as a function of the number
of tosses.

\item Plot a graph with the obtained standard deviations as a function
of the number of tosses.

\item Plot a graph with the obtained variances as a function of the
number of tosses.

\item Explain your results. How does the mean, variance, and standard
deviation change as the number of observations per experiment
increases. How does the relative frequency density polygon change as
the number of observations per experiment increases? Does it make
sense?

\end{enumerate}

\item What is less probable: 1) to get 80 or more tails out of tossing
  a fair coin 100 times, 2) to get 30 or more tails out of tossing a
  fair coin 50 times. NOTE: Since $n>30$ you can assume that the
  cumulative distribution of the mean is approximately Gaussian. 



\item An experimenter inserts intracellular electrodes on a large
sample of randomly selected neurons in primary visual cortex
(V1). Intracellular electrodes are designed to measure the response of
single neurons. The researcher finds the average response of the
neurons is 10 mVolts and the standard deviation 1 mVolt. On a
subsequent experiment the researcher inserts extracellular electrodes
on randomly selected location in V1. Extracellular electrodes compute
the average response of a number of neurons around the tip of the
electrode. The researcher finds that when the electrode is
extracellular the average response is still 10 mVolts but the standard
deviation goes down to 0.01 mVolts.  Assume the neurons in V1 are
independent and have identical response distributions. 
\begin{enumerate}

\item Explain how the standard deviation of the mean of n independent
random variables changes as the number of random variables
increases. 

\item Estimate how many neurons are having an effect on the
extracellular electrode. Justify your response. 

\end{enumerate}


\end{enumerate}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End: 
