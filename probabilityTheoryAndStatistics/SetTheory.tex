% Copyright \copyright{} 1996,,1998, 2002 Javier R. Movellan. 
% This is an open source document.  Permission is granted
% to copy, distribute and/or modify this document under the terms of the
% GNU Free Documentation License, Version 1.1 or any later version
% published by the Free Software Foundation; with no Invariant Sections,
% no Front-Cover Texts, and no Back-Cover Texts.
\chapter{Set Theory}
Intuitively we think of sets as collections of elements. The crucial
part of this intuitive concept is that we are willing to treat sets as
entities distinguishable from their elements. Sometimes we identify
sets by enumeration of their elements. For example, we may talk about
the set whose elements are the numbers $1$, $2$ and $3$. In
mathematics such sets are commonly represented by embracing the
elements of the set using curly brackets. For example, the set $\{
1,2,3\}$ is the set whose elements are the numbers $1$, $2$ and
$3$. Sometimes sets are defined using some property that identifies
their elements. In such case it is customary to represent the sets
using the following formula
\begin{equation}
\{ x \st x
\:
\text{has a particular property}\}
\end{equation}
 For example, the set $\{1,2,3\}$ can be represented as  

\begin{equation}
\{ x \st x
\; \text{ is a natural number smaller than $4$}\}.
\end{equation}
The  intuitive concept of sets as collections of
 elements is useful but it can only take us so far.  You may complain
 that we have not really defined what a set is since we have not
 defined collections and we have not specified what qualifies as an
 element. We have not specified either what qualifies as a property.
 Consider for example the proposition
 $\{x \st x
\; \text{is not an element of $x$}\}$, i.e., the set of sets which are
not elements of themselves. We can prove by  contradiction that such
a set does not exist. Let's assume that this set exists and lets
represent it with the symbol $y$. If $y$ is an element of $y$ then,
since all the elements of $y$ are not an element of themselves it
follows that $y$ is not an element of $y$. Moreover, if $y$ is not an
element of $y$ then, $y$ must be an element of $y$.  In other words,
if we assume the set $y$ exists we get a contradiction. Therefore we
have to conclude that $y$ does not exist. Using similar reasoning one
can also show that the set of all sets does not exist either (see
proof later in this document). But this raises deep questions:


\begin{enumerate}

\item What does it mean to say that a set exists or does not exist? For example Leopold Kronecker, a German mathematician born in 1823, claimed that the only numbers that assuredly exist are the natural numbers (1,2,3 ...). According to him the set of real numbers are just a fantasy that does not exist. But think about it, what criteria, other than authority, can we use to decide whether  the natural numbers or the real numbers exist? 

\item How can we tell whether something is or is not a set?

\item What are valid elements of a set? 

\end{enumerate}

Axiomatic set theory was developed to provide answers to such 
 questions. In axiomatic set theory:
\begin{enumerate}

\item A set exists if the proposition that asserts its existence is logically true. Moreover within this theory there are only sets so if a formal object is not a set, it does not exist.

\item If the assumption that an object  exists leads to a contradiction we can assert that that object does not exist, or equivalently, that it is not a set. 

\item There are no atomic elements: An object exists if and only if it is a set. Of course sets can have elements but those elements must be sets themselves otherwise they would not exist.

\end{enumerate}
  
One ``annoying'' aspect of axiomatic set theory is that sets become a
logical abstraction detached from our everyday experience with
collections of physical objects.  You should think of mathematical
sets as logical ``objects'' which are part of a formal
structure. Within this theory to say that an object exists is the same
as saying that it is a set. To say that an object does not exist is
the same as saying that it is not a set. Everyday collection of
physical objects are no longer sets in this theory and 
thus they do not exist. While this approach may
strike you as peculiar, it turns out to be extremely powerful and in
fact it has become the foundation of mathematics. The formal structure
of set theory while independent from the physical world provides very
useful tools to model the world itself. The key is to develop set
structures constrained in ways that mirror essential properties of the
physical world. For example, the properties of the set of natural
numbers (i.e., 1,2,3, $\ldots$) mirrors our experience counting
collections of physical objects.


Axiomatic set theory is a first order logical structure. First order
logic works with propositions, i.e., logical statements constructed
according to the rules of logic and that can take two values. For
convenience we call these two values ``True'' and
``False''. Set theory, and thus the entire body of mathematics reduces
to logical propositions that use the following elements:
\begin{enumerate}
\item Variables (e.g., $a,b, \ldots x, y,z$) which stand for sets.  
\item The predicate $\in$, which stands for element inclusion. For example, if the  proposition  $(x \in y)$ takes the value true, we know that both $x$ and $y$ are sets and that $x$ is an element of $y$.  For example, the proposition
\begin{equation}
\{1,2,3\} \in \{\{1,2\}, \{4,5\}, \{1,2,3\}\}
\end{equation}
takes the value ``True''. 


\item  Logical operators
\begin{enumerate}
\item $\neg P$, where $\neg$ is the logical ``negation'' operator. 
\item $P \land P$, where $\land$ is the logical ``and'' operator.
\item $P \lor P$, where $\lor$ is the logical ``or'' operator.
\item $P \to P$, where $\to$ is the logical ``implication'' operator.
\item $P \ifff P$, where $\ifff$ is the logical ``bijection'' operator.
\item $\forall x P$ is the logical ``for-all'' quantifier.
\item $\exists x P$ is the logical ``exists'' quantifier.
\end{enumerate}
\end{enumerate}
The names of the different operators (i.e., ``negation'', ``and'',
``or'', ``implication'' ...) are selected for convenience.  We could
have given them completely different names, all we really need to know
is how they operate on propositions.


All propositions in set theory are built out of atomic propositions of
the form $(x \in y)$ connected using the logical operators. If $P$ and
$Q$ are propositions, e.g., $P$ could be  $(x \in y)$ and $Q$ could be $(y
\in z$ ) then $\neg P$, $P \land P$, $P \lor Q$ $P \to Q$, $P \ifff Q$,
$\forall x P$ and $\exists x P$ are also propositions.

The effect of the connectives on the truth value of propositions is
expressed in Table \ref{logicalOperators}
\begin{table}[h]\label{logicalOperators}
\begin{center} 
\begin{tabular}{|c|c|c|c|c|c|c|}
$P$ & $Q$ &$\neg P$ &$P \land Q$ &$P \lor Q$ &$P \to Q$ &$P \ifff Q$\\ \hline
T   &  T  &F        &T           &T          &T         &T\\
T   &  F  &F        &F           &T          &F         &F\\
F   &  T  &T        &F           &T          &T         &F\\
F   &  F  &T
        &F           &F          &T         &T\\
\end{tabular}
\caption{The truth tables of the logical operators. T stands for ``True'' and F for ``False''.}
\end{center}
\end{table}
. Thus, if the
proposition $P$ takes value ``True'' and the proposition $Q$ takes the
value ``False'' then the proposition $(P \land Q)$ takes the value
``False''.  The propositions $\forall x P$ and $\exists x P$ tell us
that $x$ is a variable that can take as value any formal object that
qualifies as a set.  It also tells us that $P$ is a proposition whose
truth value depends on $x$. For example, $P$ could be $(x \in y) \lor
(x
\in z)$, where $y$ and $z$ are fixed sets and $x$ acts as a variable.  
The proposition $\forall x P$ takes the value ``True'' if $P$ takes
the value ``True'' for all sets.  The proposition $\exists x P$ takes
the value ``True'' if there is at least one set for which $P$ takes
the value ``True''. Remember when we say for all sets we do not mean
sets of physical objects. In fact we still have to define what we mean
by set. 



\section{Proofs and Logical Truth}

Sometimes we treat propositions as formulas whose truth value depends
on the truth values taken by variables in the proposition. For
example if $P$ and $Q$ are propositional variables then the $P \land Q$
is a propositional formula whose truth value depends on the specific
truth values taken by $P$ and $Q$. We say that a propositional formula
is {\bf logically true} if for all valid combinations of truth values of
the elements in the formula, the formula can only take the value
``True''. For example for the formula $(P \lor \neg P)$ there is only
two valid combination of truth values for $P$ and $\neg P$: ``True,
False'' and ``False, True''. In both case the formula $(P \lor (\neg
P))$ takes the value ``True'' and thus we say that it is  logically true. Similarly if a propositional formula can only take ``False''
values we say that it is {\bf logically false}. For example $(P \land (\neg
P))$ is logically false. A {\bf proof} is a process that shows a propositional formula is logically true.




\section{The Axioms of Set Theory}

To simplify the presentation of axiomatic set theory I will use
``pseudocode'', i.e., a combination of logical propositions,
mathematical symbols, and English statements. I do so under the
understanding that all these statements can be written as pure logical
propositions.

I will use the symbol $\nin$ in propositions of the form $(x \nin y)$
as an alternative notation to $\neg (x \in y)$. I will use the formula 
\begin{equation}
\exists  \{x \st P\}
\end{equation}
as an alternative notation to the propositional formula
\begin{equation}
\exists y \forall x P
\end{equation}
This formula simply says that there is a set of elements that satisfy
the proposition $P$. If the formula takes the value ``True'' then the
symbols $\{x \st P\}$ refers to a set that make the proposition
$\forall x P$ ``True''. When a set $x$ makes the proposition $P$ true,
I will say that $x$ satisfies $P$. For example the set $1$ satisfies
the propositional formula $(x \in \{1,2\})$.

 In set theory all existing objects are sets. If an object exists it
is a set otherwise it does not exist. To remind us of the fact that
sets include elements we sometimes refer to sets as a collection of
sets, or as a families of sets. This is just a ``human factors'' trick
since the theory makes no distinction between sets, families,
collections or elements. In axiomatic set theory elements,
collections, and families are just sets.


 Axiomatic set theory is
commonly presented using 9 redundant axioms, which are the foundation
of all mathematical statements. 


\subsection{Axiom of Existence:}
 An axiom is a restriction in the truth value of a proposition.
The axiom of existence  forces the proposition
\begin{equation}
\exists y \forall x (x \nin y) 
\end{equation}
to take the value ``True''. We call call the sets that satisfy
$\forall x (x \nin y)$ {\bf empty sets}. Thus the axiom of existence
tells us that there is at least one empty set, we will see later that
in fact there is only one empty set.

\subsection{Axiom of Equality:}
This axiom is also called the axiom of extensionality and it defines
the predicate ``$=$''. For mnemonic convenience when the proposition
$(x=y)$ takes the value ``True''  we say that the sets $x$ and $y$ are equal.  In order
to define how the symbol ``$=$'' works it is convenient to create a
new predicate, which we will symbolize as $\subset$. The new predicate
works as follows: For all sets $u$ and $v$ if the proposition
\begin{equation}
\forall x ( x \in u) \to (x \in v)
\end{equation}
is true then the proposition $(u \subset v)$ is true.  For mnemonic
convenience if the proposition $(u \subset v)$ takes the value
``True'' we say that $u$ is a subset of $v$.

The axiom of equality says that if the proposition $(u \subset v)
\land ( v \subset u)$ is true then the proposition $(u = v)$ is
true. In other word, the proposition
\begin{equation}
\forall u (u \in x \ifff u \in y) \to (x = y )
\end{equation}
takes the value ``True''.  The formula $(x \neq y)$ is used as an
alternative notation to $\neg (x =y)$.  We will now use the axiom of
equality to prove that there is only one empty set.
\paragraph{Theorem:} The empty set is unique.
\paragraph{Proof:}
Let $x$ and $y$ be empty sets, then $u \in y$ and $u \in x$ are always false for all sets $u$. Thus $(u \in y \ifff u \in x)$ is true for all sets $u$ and 
since by the axiom of equality
\begin{equation}
(\forall u (u \in x \ifff u \in y)) \to (x = y)
\end{equation}  
is true then it follows that $(x=y)$ must be true. Hereafter we
identify the empty set with the symbols $\emptyset$ or alternatively
with the symbol $\{\}$.
\begin{flushright} $\square$ \end{flushright}




\subsection{Axiom of Pair:}
So far set theory has only given us one set: the empty set. The axiom of pair brings new sets
to life. The axiom of pair says that if $x$ and $y$ exist (i.e., if they are
sets) there also exists a set whose only elements are $x$ and $y$. We
will represent such set as $\{x,y\}$. The axiom of pair
forces  the proposition
\begin{equation}
\forall x \forall y \exists \{x,y\} 
\end{equation}
to take the value ``True''.  The set made out of the sets $a$ and $a$ is symbolized as
$\{a,a\}$ or $\{a\}$ and is called the singleton whose only element is
$a$. So starting with the empty set $\emptyset$, it follows that the
set $\{\emptyset\}$ exists. Note that $\emptyset$ and $\{\emptyset\}$
are different since the first has no element and the second has one
element, which is the empty set.


\paragraph{Ordered pairs:} The ordered pair of the
sets $x$ and $y$ is symbolized $(x,y)$ and defined as follows
\begin{equation}
(x,y) \defined \{\{x\}, \{x,y\}\}
\end{equation}
where $\defined$ stands for ``equal by definition''. 
\paragraph{Exercise:} Prove that two ordered pairs $(a,b)$ and $(c,d)$ are equal if and only if $a=b$ and $c=d$.

\paragraph{Ordered sequences:}  Let $x_1, \ldots, x_n$ be sets. The ordered sequence $(x_1, \ldots , x_n)$ is recursively defined as follows
\begin{equation}
(x_1, \cdots, x_n) = ((x_1,\cdots,x_{n-1}),x_n) \end{equation}
\paragraph{Exercise:} Prove that two n-tuples pairs $(a_1,\ldots,a_n)$
and $(b_1,\ldots,b_n)$ are equal if and only if $a_1=b_1$ and
$a_2=b_2$ and ... $a_n=b_n$.



\subsection{Axiom of Separation:}
This axiom tells us how to generate new sets out of elements of an
existing set. To do so we just choose elements of an existing set that
 satisfy a proposition. Consider a  proposition $P$
whose truth value depends on the sets $u$ and $v$, for example,
$P$ could be $(u\in v$). The axiom of separation forces 
the proposition
\begin{equation}
\exists \{ x \st (x \in u) \land P\}
\end{equation}
to take the value ``True'' for all sets $u$, $v$ and for all propositions $P$ with truth values dependent on $u$ and $v$. 



\paragraph{Fact:} There is no set of all sets. \\
The proof works by contradiction. Assume there is a
set of all sets, and call it $u$. Then by the axiom of separation
the following set $r$ must exist
\begin{equation}
r = \{x \st (x \in u) \land (x \nin x)\}
\end{equation}
and since $(x\in u)$ is always true, this set equals the set
\begin{equation}
\{x \st  x \nin x\} 
\end{equation}
Then $(r \in) r \ifff (r \nin r)$ which is a logically false
proposition. Thus the set of all sets does not exist (i.e., it is not
a set).



\paragraph{Intersections:} The intersection of all the sets in the set $s$, or simply the intersection of $s$ is symbolized as $\inter s$ and defined as follows:

\begin{equation}
\inter s = \{ x \st \forall y (\:(y \in s) \to (x \in y)\:)\}
\end{equation}
For example, if $s=\{\{1,2,3\}, \{2,3,4\}\}$ then $\inter s = \{2,3\}$. The
axiom of separation tells us that if $s$ exists then $\inter s$ also
exists. We can then use the axiom of equality to prove that $\inter s$
is in fact unique.  For any two sets $x$ and $y$, we  represent
their intersection as $x\inter y$ and define it as follows
\begin{equation}
x \inter y \defined \inter\{x,y\} \end{equation} 
For example, if $x=\{1,2,3\}$ and $y=\{2,3,4\}$ then 
\begin{equation}
x \inter y = \inter \{\{1,2,3\}, \{2,3,4\}\} = \{2,3\}
\end{equation}

\subsection{Axiom of Union:} It tells us that for any set $x$ we can
make a new set whose elements belong to at least one of the elements
of $x$. We call this new set the union of  $x$ and we represent it as $\union x$. For example, if $x= \{\{1,2\}, \{2,3,4\}\}$ then $\union x = \{ 1,2,3,4\}$.  More formally, the axiom of union forces the  proposition
\begin{align}
&\forall s \exists \union s
\end{align}
to be true. Here $\union s$ is defined as follows
\begin{equation}
\union s \defined \{ x \st \exists y (y \in s) \land (x \in y)\}
\end{equation}
 For example, if $x=\{\{1,2,3\}, \{2,3,4\}\}$ then $\union x=
\{1,2,3,4\}$.  Using the axiom of equality $\union x$ can be shown to
be unique. For any two sets $x$ and $y$, we define the union of the
two sets as follows
For example,
\begin{equation}
\{1,2,3\} \union \{2,3,4\} = \{1,2,3,4\}
\end{equation}
\begin{equation}
x \union y \defined \union\{x,y\} \end{equation} 

\subsection{Axiom of Power:}
This axiom tells us that for any set $x$ the   set of all subsets of $x$ exists. We call this set the power set of $x$ and represent it as $\Ps(x)$. More formally, the axiom of power forces the proposition
\begin{equation}
\forall s \exists \{ x \st  x \subset u\}
\end{equation}
to take the value ``True''. For example, if $s = \{1,2\}$ then 
\begin{equation}
\Ps(s) = \{\{1\}, \{2\}, \emptyset, \{1,2\}\}.
\end{equation}



\paragraph{Cartesian Products:} The Cartesian product of two sets $u$ and $v$, is 
symbolized $a \times b$ and defined as follows
\begin{equation}
a \times b = \{(x,y) \st (x \in a) \land (y \in b)\} 
\end{equation}
Using the axioms of separation, union and  power, we can show that $x\in
y$ exists because it is a subset of $\Ps(\Ps(x \union y))$. Using the
axiom of identity we can show that it is unique.


\paragraph{Functions:} A function $f$ with domain $u$ and target $v$ is a subset of $u \times v$ with the following property: If  $(a,c)$ and $(b,c)$ are elements of $f$ then $a=b$. More formally, if the proposition
\begin{equation}
\forall a \forall b \forall c ( ((a,c) \in f) \land ((b,c) \in f) \to (a=b))
\end{equation}
takes the value ``True'' then we say that the set $f$ is a function.

The following formulae are alternative notations for the same proposition:

\begin{align}
&(x,y) \in f \\
& y = f(x)\\
& x \mapsto f(x)\\
& x \mapsto y
\end{align}
\subsection{Axiom of Infinity:} This axiom forces the  proposition 
\begin{equation}
\exists s \forall x (x \in s) \to (\{x , \{x\}\} \in s)
\end{equation}
to take the value ``True''. In English  this axiom  tells us that there is a set $s$ such that  if $x$ is an element of $s$  then the pair $\{x , \{x\}\}$ is also an element of $s$.  Sets that satisfy the proposition 
\begin{equation}
\forall x (x \in s) \to (\{x , \{x\}\} \in s)
\end{equation}
 are called inductive (or infinite) sets.

\paragraph{Natural numbers:} The natural
numbers plus the number zero are defined as the intersection of all
the inductive sets and are constructed as follows:
\begin{eqnarray}
0 &\defined& \{\} \\
1 &\defined& \{0,  \{0\}\} = \{\{\},\{\{\}\} \}\\
2 &\defined& \{1 ,\{1\}\} = \{ \{\{\},\{\{\}\} \}, \{\{\{\},\{\{\}\} \}\}\}\\
\vdots \nonumber
\end{eqnarray}

The axiom of existence in combination with the axiom of infinity
guarantee that these sets exist. Note that the symbols $1,2, \cdots$
are just a mnemonic convenience. The bottom line is that numbers, and
in facts all sets, are just a bunch of empty curly brackets!

\subsection{Axiom of Image:} Let $f: u \to v$ be a function (i.e, a subset of $u \times b$). Define the image of $u$ under $f$ as the set of elements  for which there is an element of $u$ which projects into that element. We represent that set as $I_f(u)$. More formally
\begin{equation}
I_f(u) = \{y \st \exists x (x \in u) \land (f(x) = y) \}
\end{equation}
The axiom of image, also called the axiom of replacement, tells us that
for all sets $u$ and for all functions $f$ with domain $u$ the set
$I_f(u)$ exists. 
\subsection{Axiom of Foundation:} This axiom prevents the existence of sets who are elements of themselves. 

\subsection{Axiom of Choice:} This axiom tells us that every set with
no empty elements has a choice function. A choice function for a set
 $s$ is a function with domain $s$ and such that for each $x\in s$
the function takes a value $f(x) \in x$ which is an element of $x$.
In other words, the function $f$ picks one element from each of the
sets in $s$, thus the name ``choice function''. For example, For the
set $s=\{\{1,2,3\}, \{2,5\},\{2,3\}\}$ the function $f\st s \to
\{1,2,3\}$ such that 
\begin{align}
f(\{1,2,3\}) = 3\\
f(\{2,5\}) = 2\\
f(\{2,3\}) = 2
\end{align}
is a choice function since  for each set in $s$ the function $f$  picks an
element of that set. The axiom of choice is independent of the other
axioms, i.e., it cannot be proven right or wrong based on the other
axioms. The axiomatic system presented here is commonly symbolized as
ZFC (Zermelo-Fraenkel plus axiom of Choice), the axiomatic system
without the axiom of choice is commonly symbolized as ZF.


\section*{History}
\begin{itemize}
\item The first version of this document was written by Javier R. Movellan
in 1995. The document was 8 pages long.

\item The document was made open source under the GNU Free
Documentation License Version 1.1 on August 9 2002, as part of the
Kolmogorov project.
\end{itemize}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End: 
