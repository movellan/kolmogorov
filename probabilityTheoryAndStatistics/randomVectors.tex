
% Copyright \copyright{} 1996,,1998, 2002 Javier R. Movellan. 
% This is an open source document.  Permission is granted
% to copy, distribute and/or modify this document under the terms of the
% GNU Free Documentation License, Version 1.1 or any later version
% published by the Free Software Foundation; with no Invariant Sections,
% no Front-Cover Texts, and no Back-Cover Texts.

\chapter{Random Vectors}

In many occasions we need to model the joint behavior of more than one
variable. For example, we may want to describe whether two different
stocks tend to fluctuate in a somewhat linked manner or whether high levels of
smoking covary with high lung cancer rates.  In this chapter we examine the
joint behavior of more than one random variables. To begin with, we
will start working with pairs of random variables.

\section{Joint probability mass functions}
The joint probability mass function of the random variables $X$ and $Y$ is a function $p_{X,Y}\st \R^2 \to [0,1]$, such that for all $(u,v) \in \R^2$
\begin{equation}
p_{X,Y}(u,v) = P(\{X=u\} \inter \{Y=v\})
\end{equation}
hereafter, we use the simplified notation $P(X=u, Y=u)$ to represent
$P(\{X=u\} \inter \{Y=v\})$, i.e., the probability measure of the set
\begin{equation}
\{ \omega \st (X(\omega) = u) \text{and} (Y(\omega) = v)\} 
\end{equation}
\section{Joint probability density functions}
The joint probability density function of the continuous random variables $X$ and $Y$ is a function $f_{X,Y}\st \R^2 \to [0, \infty)$ such that for all $(u,v) \in \R^2$ and for all $(\Delta u, \Delta v) \in R^2$
\begin{equation}
P(X \in [u, u+\Delta u], Y \in [v, v+ \Delta v]) 
= \int_u^{u+\Delta u} \int_v^{v+\Delta v} f_{X,Y}(u,v) \:dv\: du
\end{equation}
\paragraph{Interpretation}
Note if $\Delta u$ and $\Delta v$ are so small that $f_{X,Y}(u,v)$ is approximately constant over the area of integration then
\begin{equation}
P(X \in [u, u+\Delta u], Y \in [v, v+ \Delta v]) \approx f_{X,Y}(u,v) \Delta u \: \Delta v 
\end{equation}
In other words, the probability that $(X,Y)$ take values in the rectangle $[u, u+\Delta u] \times [v, v+\Delta v]$ is approximately the area of the rectangle times the density at a point in the rectangle. 

\section{Joint Cumulative Distribution Functions}
The joint cumulative distribution of two random variables $X$ and $Y$ is a function $F_{X,Y} \st \R^2 \to [0,1]$ such that 

\begin{equation}
F_{X,Y}(u,v) = P(\{X \leq u\} \inter \{Y \leq v\})
\end{equation}
note if $X$ and $Y$ are discrete then
\begin{equation}
F_{X,Y}(u,v) = \sum_{x \leq u} \sum_{ y \leq v} p_{X,Y}(u,v)
\end{equation}
and if $X$ and $Y$ are continuous then 
\begin{equation}
F_{X,Y}(u,v) = \int_{-\infty}^u \int_{-\infty}^v  f_{X,Y}(u,v) \:dv \:du 
\end{equation}
\section{Marginalizing}
In many occasions we know the joint probability density or probability
mass function of two random variables $X$ and $Y$ and we want to get
the probability density/mass of each of the variables in
isolation. Such a process is called marginalization and it works as follows: 
\begin{equation}
p_{X}(u) = \sum_{v \in \R} p_{X,Y}(u,v)
\end{equation}
and if the random variables are continuous
\begin{equation}
f_{X}(u) = \int_{-\infty}^{\infty} f_{X,Y}(u,v) \: dv
\end{equation}
\paragraph{Proof:}
Consider the function $h\st \R \to \R$
\begin{equation}
h(u) = \int_{-\infty}^\infty f_{X,Y}(u,v) \: dv
\end{equation}
We want to show that $h$ is the probability density function of $X$. Note for all $a,b \in \R$ such that  $a \leq b$
\begin{equation}
P(X\in [a,b]) = P(X\in [a,b], Y\in \R) = \int_a^b f_{X,Y}(u,v) \: dv \: du
= \int_a^b h(u) \: du
\end{equation}
showing that $h$ is indeed the probability density function of $X$. A similar argument can be made for the discrete case.

\section{Independence}
Intuitively, two random variables $X$ and $Y$ are independent if
 knowledge about one of the variables gives us no information
 whatsoever about the other variable.  More precisely, the random variables $X$ and $Y$ are independent if and only if all events of
 the form $\{ X \in [u, u+\Delta u]\}$ and all events of the form $\{Y
 \in [v, v+ \Delta v]\}$ are independent.  If the random variable is
 continuous, a necessary and sufficient condition for independence is
 that the joint probability density be the product of the densities of
 each variable
\begin{equation}
f_{X,Y} (u,v) = f_{X}(u)  f_{Y}(v)
\end{equation}
for all $u,v \in \R$.
If the random variable is discrete, a necessary and sufficient condition for independence is that the joint probability mass function be the product of the probability mass for each of the variables
\begin{equation}
p_{X,Y} (u,v) = p_{X}(u)  p_{Y}(v)
\end{equation}
for all $u,v \in \R$.
% I need an example. 

\section{Bayes' Rule for continuous data and discrete hypotheses}
Perhaps the most common application of Bayes' rule occurs when the data are represented by a continuous random variable $X$, and the hypotheses by a discrete random variable $H$. In such case, Bayes' rule works as follows
\begin{equation}
p_{H|X}(i\given u) = \frac{f_{X|H}(u|i)\: p_H(i)}{f_X(u)}
\end{equation} 
where $p_{H|X} \st \R^2 \to [0,1]$ is known as the conditional probability mass function of $H$ given $X$ and it is defined as follows
\begin{align}
p_{H|X}(i\given u) = \lim_{\Delta u \to 0} P(H=i \given X \in [u, u+\Delta u]) 
\end{align}
$f_{X|H} \st \R^2 \to \R_+$ is known as the conditional probability density function of $X$ given $H$ and it is defined as follows: For all $a, b \in \R$ such that $a\leq b$, 
\begin{equation}
P( X \in [a,b] \given H=i) = \int_a^b f_{X|H}(u|i) \: du 
\end{equation}
\paragraph{Proof:}
Applying Bayes' rule to the events $\{H = i\}$ and $\{ X \in [u,
u+\Delta u]\}$

\begin{align}
P(H=i &\given X \in [u, u+ \Delta u]) = \frac{ P(X \in [u, u+ \Delta u] \given H=i) \: P(H =i)}{P(X \in [u, u+ \Delta u])} \\
&= 
\frac{\int_u^{u+\Delta u} f_{X|H}(x|i) \: dx\: P(H=i)}{\int_u^{u+\Delta u}f_X(x) \: dx}
\end{align}
Taking limits and approximating areas with rectangles

\begin{equation}
 \lim_{\Delta u \to 0} P(H=i \given X \in [u, u+ \Delta u]) 
= \frac{ \Delta u f_{X|H}(u|i) p_H(i)}{\Delta u f_{X}(u)} 
\end{equation}
The $\Delta u$ terms cancel out, completing the proof.
\subsection{A useful version of the LTP}
In many cases we are not given $f_X$ directly and we need to compute it using $f_{X|H}$ and $p_H$. This can be done using the following version of the law of total probability:
\begin{equation}
f_X(u) = \sum_{j \in \text{Range}(H)}\: p_H(j)\: f_{X|H}(u|j)
\end{equation}
\paragraph{Proof:}
Note  that for all $a,b \in \R$, with $a \leq b$  
\begin{align}
\int_a^b &\sum_{j \in \text{Range}(H)} p_H(j) f_{X|H}(u|j) \: du =
\sum_{j \in \text{Range}(H)} p_H(j) \int_a^b f_{X|H}(u|j) \: du \\= 
&\sum_{j \in \text{Range}(H)} p_H(j) P(X \in [a,b]\given H=j) = P(X \in [a,b])
\end{align}
and thus 
$
\sum_{j \in \text{Range}(H)} p_H(j) f_{X|H}(u|j)
$
is the probability density of $X$.
\paragraph{Example:}
Let $H$ be a Bernoulli random variable with parameter $\mu = 0.5$. Let $Y_0 \sim N(0,1)$, $Y_1 \sim N(1,1)$  and 
\begin{equation}
X = (1- H) (Y_0) + (H) (Y_1) 
\end{equation}
Thus,
\begin{align}
&p_H(0) = p_H(1) = 0.5\\
&f_{X|H}(u\given i) = \frac{1}{\sqrt{2 \pi}} e^{-(u - i)^2/2}\;\;\; \text{for $i \in \{1,2\}$}.
\end{align}
Suppose we are given the value $u\in \R$ and we want to know the
posterior probability that $H$ takes the value 0 given that $X$ took
the value $u$. To do so, first we apply LTP to compute $f_X(u)$
\begin{equation}
f_X(u) = (0.5) \frac{1}{\sqrt{2 \pi}} \biggl( e^{-u^2/2} + e^{-(u-1)^2/2} \biggl)
= (0.5) \frac{1}{\sqrt{2 \pi}}  e^{u^2/2} \biggl( 1  + e^{2u -1}\biggl)
\end{equation}
Applying Bayes' rule, 
\begin{equation}
p_{H|X}(0\given u) = \frac{1}{1 + e^{-(1-2u)}}
\end{equation}
So, for example, if $u=0.5$ the probability that $H$ takes the value
$0$ is 0.5. If $u=1$ the probability that $H$ takes the value $0$
drops down to $0.269$.
\section{Random Vectors and Stochastic Processes}
A random vector is a collection of random variables organized as a vector. For example if $X_1, \ldots, X_n$ are random variables then 
\begin{equation}
X = (X_1, Y, \ldots, X_n)' 
\end{equation}
is a random vector. 
A stochastic process is an ordered set of random vectors. For example, if $I$ is an indexing set and $X_t$, is a random vector for all $t \in I$ then 
\begin{equation}
X = (X_i \st i \in I ) 
\end{equation} 
is a random process. When the indexing set is countable, $Y$
 is called a ``discrete time'' stochastic process. For example,
\begin{equation}
X=(X_1, Y, X_3, \ldots)
\end{equation}
is a discrete time random process. If the indexing set are the real numbers, then $Y$ is called a ``continuous time'' stochastic process. For example, if $X$ is a random variable, the ordered set of random vectors
\begin{equation}
X = (X_t\st t \in \R)
\end{equation}
with 
\begin{equation}
X_t(\omega)  =  \sin( 2 \pi t X(\omega))
\end{equation}
is a continuous time stochastic process. 

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End: 
