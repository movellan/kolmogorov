% Copyright \copyright{} 1996,,1998, 2002 Javier R. Movellan. 
% This is an open source document.  Permission is granted
% to copy, distribute and/or modify this document under the terms of the
% GNU Free Documentation License, Version 1.1 or any later version
% published by the Free Software Foundation; with no Invariant Sections,
% no Front-Cover Texts, and no Back-Cover Texts.

\chapter{Probability}


Probability theory provides a mathematical foundation to concepts such
as ``probability'', ``information'', ``belief'', ``uncertainty'',
``confidence'', ``randomness'', ``variability'', ``chance'' and
``risk''.  Probability theory is important to empirical scientists
because it gives them a rational framework to make inferences and test
hypotheses based on uncertain empirical data.  Probability theory is
also useful to engineers building systems that have to operate
intelligently in an uncertain world. For example, some of the most
successful approaches in machine perception (e.g., automatic speech
recognition, computer vision) and artificial intelligence are based on
probabilistic models.  Moreover probability theory is also proving very
valuable as a theoretical framework for scientists trying to
understand how the brain works.  Many computational neuroscientists
think of the brain as a probabilistic computer built with unreliable
components, i.e., neurons, and use probability theory as a guiding
framework to understand the principles of computation used by the
brain. Consider the following examples:

\begin{itemize}
\item You need to decide whether a coin is loaded (i.e., whether it
  tends to favor one side over the other when tossed).   You toss the
  coin 6 times and in all cases you get ``Tails''. Would you say that
  the coin is loaded?

  
\item You are trying to figure out whether newborn babies can
  distinguish green from red. To do so you present two
  colored cards (one green, one red) to 6 newborn babies. You make
  sure that the 2 cards have equal overall luminance so that they are
  indistinguishable if recorded by a black and white camera. The 6
  babies are randomly divided into two groups. The first group gets
  the red card on the left visual field, and the second group on the
  right visual field. You find that all 6 babies look longer to the
  red card than the green card. Would you say that babies can
  distinguish red from green?

\item A pregnancy test has a 99 \% validity (i.e., 99 of of 100
  pregnant women test positive) and 95 \% specificity (i.e., 95 out of
  100 non pregnant women test negative). A woman believes she has a 10
  \% chance of being pregnant. She takes the test and tests
  positive. How should she combine her prior beliefs with the results
  of the test?


\item You need to design a system that detects a sinusoidal tone of
  1000Hz in the presence of white noise. How should design the system to solve this task optimally? 

\item How should the photo receptors in the human retina be 
  interconnected to maximize information transmission to the brain?
  

\end{itemize}
While these tasks appear different from each other, they all share a
common problem: The need to combine different sources of uncertain
information to make rational decisions. Probability theory provides a
very powerful mathematical framework to do so. Before we go into
mathematical aspects of probability theory I shall tell you that there
are deep philosophical issues behind the very notion of probability.
In practice there are three major interpretations of probability,
commonly called the frequentist, the Bayesian or subjectivist, and the
axiomatic or mathematical interpretation.

\begin{enumerate}

\item{\bf Probability as a relative frequency}

This approach interprets the probability of an event as the proportion
of times such ane event is expected to happen in the long run.
Formally, the probability of an event $\E$ would be the limit of the
relative frequency of occurrence of that event as the number of
observations grows large
\begin{equation}
P(\E) = \lim_{n \to \infty} \frac{n_\E}{n}
\end{equation}
where $n_E$ is the number of times the event is observed out of a
total of $n$ independent experiments.  For example, we say that the
probability of ``heads'' when tossing a coin is 0.5. By that we mean
that if we toss a coin many many times and compute the relative
frequency of ``heads'' we expect for that relative frequency to
approach 0.5 as we increase the number of tosses.

This notion of probability is appealing because it seems objective and
ties our work to the observation of physical events. One difficulty
with the approach is that in practice we can never perform an
experiment an infinite number of times. Note also that this approach
is behaviorist, in the sense that it defines probability in terms of
the observable behavior of physical systems. The approach fails to
capture the idea of probability as internal knowledge of cognitive
systems.


\item{\bf Probability as uncertain knowledge. }

This notion of probability is at work when we say things like ``I will
probably get an A in this class''. By this we mean something like
``Based on what I know about myself and about this class, I would not
be very surprised if I get an A.  However, I would not bet my life on
it, since there are a multitude of factors which are difficult to
predict and that could make it impossible for me to get an A''.  This
notion of probability is ``cognitive'' and does not need to be
directly grounded on empirical frequencies. For example, I can say
things like ``I will probably die poor'' even though I will not be
able to repeat my life many times and count the number of lives in
which I die poor.

This notion of probability is very useful in the field of machine
intelligence. In order for machines to operate in natural environments
they need knowledge systems capable of handling the uncertainty of the
world. Probability theory provides an ideal way to do so. Probabilists
that are willing to represent internal knowledge using probability
theory are called ``Bayesian'', since Bayes is recognized as the first
mathematician to do so.

\item{\bf Probability as a mathematical model.}  Modern mathematicians
avoid the frequentist vs. Bayesian controversy by treating probability
as a mathematical object. The role of mathematics here is to make sure
probability theory is rigorously defined and traceable to first
principles. From this point of view it is up to the users of
probability theory to apply it to whatever they see fit. Some may want
to apply it to describe limits of relative frequencies. Some may want
to apply it to describe subjective notions of uncertainty, or to build
better computers.  This is not necessarily of concern to the
mathematician. The application of probability theory to those domains
will be ultimately judged by its usefulness.



\end{enumerate}


\section{Intuitive Set Theory}

We need a few notions from set theory before we jump into probability
theory. In doing so we will use intuitive or ``naive''
definitions. This intuitive approach provides good mnemonics and is
sufficient for our purposes but soon runs into problems for more
advanced applications. For a more rigorous definition of set
theoretical concepts and an explanation of the limitations of the
intuitive approach you may want to take a look at the Appendix.

\begin{itemize}
\item{\bf Set}: A set is a collection of elements. Sets are commonly represented using curly brackets containing a collection of elements separated by commas. For example 
\begin{equation}
A = \{1,2,3\}
\end{equation}
tells us that $A$ is a set whose elements are the first 3 natural
numbers. Sets can also be represented using a rule that identifies the
elements of the set. The prototypical notation is as follows
\begin{equation}
\{ x \st x \; \text{follows a rule}\}
\end{equation}
For example,
\begin{equation}
\{ x \st x\; \text{is a natural number and $x$ is smaller than 4 }\}
\end{equation}

\item{\bf Outcome Space}: The outcome space is a set  whose elements
  are all the possible basic outcomes of an experiment.\footnote{The empty
    set is not a valid sample space.}  The sample space is also called
  {\bf sample space}, {\bf reference set}, and {\bf universal set}
  and it is commonly represented with the capital Greek letter
  ``omega'', $\Omega$.  We call the elements of
  the sample space ``outcomes'' and represent them symbolically 
  with the small Greek letter ``omega'', $\omega$.
  \paragraph{Example 1:}  If we roll a die, the outcome
  space could be
\begin{equation}
\Omega = \{1,2,3,4,5,6\}
\end{equation}
 In this case the symbol $\omega$ could be used to represent either
1,2,3,4,5 or 6.

\paragraph{Example 2:} If we toss a coin twice, we can observe 1 of 4
outcomes: (Heads, Heads), (Heads, Tails), (Tails, Heads), (Tails,
Tails). In this case we could use the following outcome space
\begin{equation}
\Omega = \{(H,H), (H,T), (T,H), (T,T) \}
\end{equation}
and the symbol $\omega$ could be used to represent either $(H,H)$, or
$(H,T)$, or $(T,H)$, or $(T,T)$. Note how in this case each basic
outcome contains 2 elements. If we toss a coin $n$ times each basic
outcome $\omega$ would contain $n$ elements. 

\item{\bf Singletons: } A singleton is a set with a single element.
  For example the set $\{4\}$ is a singleton, since it only has one
  element. On the other hand $4$ is not a singleton since it is an
  element not a set.\footnote{The distinction between elements and
    sets does not exist in axiomatic set theory, but it is useful
    when explaining set theory in an intuitive manner.}

  
\item {\bf Element inclusion:} We use the symbol $\in$ to represent
   element inclusion.  The expression $\omega \in
  A$ tells us that $\omega$ is an element of the set $A$. The
  expression $\omega \nin A$ tells us that $\omega$ is {\bf not} an element
  of the set $A$. For example, $1 \in \{1,2\}$ is true since $1$ is an
  element of the set $\{1,2\}$. The expression $\{1\} \in \{\{1\},
  2\}$ is also true since the singleton $\{1\}$ is an element of the set
  $\{\{1\}, 2\}$. The expression $\{1\} \nin \{1,2\}$ is also
  true, since the set $\{1\}$ is not an element of the set $\{1,2\}$.

  
\item{\bf Set inclusion:} We say that the set $A$ is included in the
  set $B$ or is a {\bf subset} of $B$ if all the elements of $A$ are
  also elements of $B$. We represent set inclusion with the symbol
  $\subset$. The expression $A \subset B$ tells us that both $A$ and
  $B$ are sets and that all the elements of $A$ are also elements of
  $B$. For example the expression $\{1\} \subset \{1,2\}$ is true
  since all the elements of the set $\{1\}$ are in the set $\{1,2\}$.
  On the other hand $1 \subset \{1,2\}$ is not true since $1$ is an
  element, not a set.\footnote{For a more rigorous
    explanation see the Appendix on axiomatic set theory.}

 % This is an example where intuitive set theory starts to differ from 
 % axiomatic set theory. In axiomatic set theory all elements are also 
 % sets. 


  
\item{\bf Set equality:} Two sets $A$ and $B$ are equal if all
  elements of $A$ belong to $B$ and all elements of $B$ belong to $A$.
In other words, if $A\subset B$ and $B\subset A$. 
  For example the sets $\{1,2,3\}$ and $\{3,1,1,2,1\}$ are equal. 


\item{\bf Set Operations:} There are  3 basic  set operations:

\begin{enumerate}

\item{\bf Union}: The union of two sets $A$ and $B$ is another set  that includes all elements of $A$ and all elements of $B$. We represent the union operator with this symbol $\union$ 

For example, if $A=\{1,3,5\}$ and $B=\{2,3,4\}$, then $A \union B
=\{1,2,3,4,5\}$.  More generally
\begin{equation}
A \union B = \{ \omega: \omega \in A \;\text{or}\; \omega \in B\}
\end{equation}
In other words, the set $A\union B$ is the set of elements with the
property that they either belong to the set $A$ or to 
the set $B$. 

\item{\bf Intersection}: The intersection of two sets $A$ and $B$ is another set $C$ such that all elements in $C$ belong to $A$ and to $B$. The intersection operator is symbolized as $\inter$. If  $A=\{1,3,5\}$ and $B=\{2,3,4\}$ then $A \inter B = \{3\}$.
More generally
\begin{equation}
A \inter B = \{ \omega: \omega \in A \;\text{and}\; \omega \in B\}
\end{equation}

\item{\bf Complementation}: The complement of a set $A$ with respect to a
  reference set $\Omega$ is the set of all elements of $\Omega$ which
  do not belong to $A$. The complement of $A$ is represented as $A^c$.
  For example, if the universal set is $\{1,2,3,4,5,6\}$ then the
  complement of $\{1,3,5\}$ is $\{2,4,6\}$.   More generally
\begin{equation}
A^c = \{ \omega: \omega \in \Omega \: \text{and} \: \omega \nin A \}
% Note we cant we define the complement with no reference to a
 % universal set. Otherwise when we define sigma algebras we say that 
 % the complement of emptyset should be there. But the complement of
 % the emptyset does not exist unless we define complementation in
 % reference to a given set.
\end{equation}
\end{enumerate}



\item{\bf Empty set}: The empty set is a set with no elements.  We
  represent the null set with the symbol $\emptyset$. Note $\Omega^c = 
  \emptyset$, $\emptyset^c = \Omega$, and for any set $A$
\begin{align}
 &A
  \union \emptyset = A\\
&A \inter \emptyset = \emptyset
\end{align}
% Note this is the absolute one and only empty set, not an empty set
 % with respect to a reference set. Otherwise, things get ugly. 

\item{\bf Disjoint sets}: Two sets are disjoint if they have no
  elements in common, i.e., their intersection is the empty set. For
  example, the sets $\{1,2\}$ and $\{1\}$ are not disjoint since they
  have an element in common.


\item{\bf Collections}: A collection of sets is a set of sets, i.e., a set whose elements are sets. For example, if $A$ and $B$ are the sets defined above, the set $\{A,B\}$ is a collection of sets. 


\item{\bf Power set}: The power set of a set $A$ is the a collection of all possible sets of $A$. We represent it as $\Ps(A)$.  For example, if $A = \{1,2,3\}$ then 
\begin{equation}
\Ps(A) = \{ \emptyset, \{1\},\{2\},\{3\}, \{1,2\},\{1,3\}, \{2,3\}, A\}
\end{equation}
Note that $1$ is not an element of $\Ps(A)$ but $\{1\}$ is. This is
because $1$ is an element  of $A$, not a set of $A$. 

\item{\bf Collections closed under set operations}: A collection of sets is closed under set operations if any set operation on the sets in the collection results in another set which still is in the collection. If  $A=\{1,3,5\}$ and $B=\{2,3,4\}$,  the collection $\C= \{A,B\}$ is not closed because the set $A \inter B =\{3\}$ does not belong to the collection. The collection $\C= \{\Omega, \emptyset\}$ is closed under set operations, all set operations on elements of $\C$ produce another set that belongs to $\C$. The power set of a set is  always a closed collection.


\item{\bf Sigma algebra}: A sigma algebra is  a
  collection of sets which is closed when set operations are applied
  to its members a countable number of times. The power set of a set
  is always a sigma algebra. 

  
\item{\bf Natural numbers}: We use the symbol $\N$ to represent the
  natural numbers, i.e., $\{1,2,3, \ldots \}$. One important property
  of the natural numbers is that if $x \in \N$ then $x+1 \in \N$.
   
\item{\bf Integers}: We use the symbol $\Z$ to represent the set of
  integers, i.e., $\{\ldots,$ $-3,$ $-2,$ $-1,$ $0,$ $1,$ $2,$ $3,$
  $\ldots\}$. Note $\N \subset \Z$. One important property of the
  natural numbers is that if $x \in \Z$ then $x+1 \in \Z$ and $x-1 \in
  \Z$ .

\item{\bf Real numbers}: We use the symbol $\R$ to represent the real
  numbers, i.e., numbers that may have an infinite number of decimals.
  For example, $1$, $2.35$, $-4/123$, $\sqrt{2}$, and $\pi$, are real
  numbers. Note $\N \subset \Z \subset \R$.  

\item{\bf Cardinality of sets}: 
\begin{itemize}
\item We say that a set is {\bf finite} if it can be put in one-to-one
  correspondence with a set of the form $\{1,2, \ldots, n\}$, where
  $n$ is a fixed natural number. 

\item We say that a set is {\bf infinite countable} if it can be put in
  one-to-one correspondence with the natural numbers. 

\item We say that a set is {\bf countable} if it is either finite or
  infinite countable.

\item We say that a set is {\bf infinite uncountable} if it has a
  subset that can be put in one-to-one correspondence with the natural
  numbers, but the set itself cannot be put in such a correspondence.
  This includes sets that can be put in one-to-one correspondence with
  the real numbers.
\end{itemize}
\end{itemize}


\section{Events}
We have defined outcomes as the elements of a reference set $\Omega$.
In practice we are interested in assigning probability values not only
to outcomes but also to sets of outcomes.  For example we may want to
know the probability of getting an even number when rolling a die. In
other words, we want the probability of the set $\{2,4,6\}$. In
probability theory set of outcomes to which we can assign
probabilities are called {\bf events}.  The collection of all events
 is called the {\bf event space} and is commonly
represented with the letter $\F$.  Not all collections of sets qualify
as event spaces. To be an event space, the collection of sets has to
be a sigma algebra (i.e., it has to be closed under set operations).
Here is an example:

\paragraph{Example:}
Consider the sample space $\Omega= \{1,2,3,4,5,6\}$. Is the collection
of sets $ \{\{1,2,3\},\{4,5,6\}\}$ a valid event space?\\{\bf Answer:}
No, it is not a valid event space because the union of $\{1,2,3\}$ and
$\{4,5,6\}$ is the set $\Omega = \{1,2,3,4,5,6\}$ which does not
belong to $\F$. On the other hand the set $ \{\emptyset,
\{1,2,3\},\{4,5,6\}, \Omega\}$ is a valid event space. Any set
operation using the sets in $\F$ results into another set which is in
$\F$.
\paragraph{Note:} The outcome space $\Omega$ and the event space $\F$
are different sets. For example if the outcome space were $\Omega =
\{H,T\}$ a valid event space would be $\F=\{\Omega, \emptyset, \{H\},
\{T\} \}$. Note that $\Omega \neq \F$. The outcome space contains
the basic outcomes of an experiments. The event space contains
sets of outcomes. 

\section{Probability measures}

When we say that the probability of rolling an even number is 0.5, we
can think of this as an assignment of a number (i.e., 0.5) to a set
,i.e., to the set $\{2,4,6\}$.  Mathematicians think of probabilities
as function that ``measures'' sets, thus the name {\bf probability
measure}. For example, if the probability of rolling an even number on
a die is 0.5, we would say that the probability measure of the set
$\{2,4,6\}$ is 0.5. Probability measures are commonly represented with
the letter $P$ (capitalized).  Probability measures have to follow
three constraints, which are known as Kolmogorov's axioms:

\begin{enumerate}
\item The probability measure of events has to be larger or equal to zero: $P(A) \geq 0$ for all $A\in \F$. 

\item The probability measure of the reference set is 1 
\begin{equation}
P(\Omega) = 1
\end{equation}


\item If the sets $A_1 , A_2, \ldots \in \F$ are disjoint then 
\begin{equation}
P(A_1 \union A_2 \union \cdots) = P(A_1) + P(A_2) + \cdots
\end{equation}
\end{enumerate}


\paragraph{Example 1: A fair coin.}\label{sec:faircoin}

We can construct a probability space to describe the behavior of a
coin. The outcome space consists of 2 elements, representing heads and
tails $\Omega=\{H,T\}$. Since $\Omega$ is finite, we can use as the event
space the set of all sets in $\Omega$, also known as the power set of
$\Omega$. In our case, $\F = \{ \{H\}, \{T\}, \{H,T\},
\emptyset\}$. Note $\F$ is closed under set operations so we can use it as an event space.

The probability measure $P$ in this case  is totally defined if we  simply say $P(\{H\}) = 0.5$. The outcome of $P$ for all the other elements of $\F$ can be inferred: we already know $P(\{H\}) = 0.5$ and $P(\{H,T\}) = 1.0$. Note the sets $\{H\}$ and $\{T\}$ are disjoint, moreover $\{H\} \union \{T\} = \Omega$, thus using the probability axioms 
\begin{equation}
P(\{H,T\}) = 1 = P(\{H\}) + P(\{T\}) = 0.5 + P(\{T\})
\end{equation}
from which it follows $P(\{T\}) = 0.5$. Finally we note that $\Omega$ and $\emptyset$ are disjoint and their union is $\Omega$, using the probability axioms it follows that 
\begin{equation}
1 = P(\Omega) = P(\Omega \union \emptyset) = P(\Omega) + P(\emptyset) 
\end{equation}
Thus $P(\emptyset) = 0$. Note $P$ qualifies as a probability measure:
for each element of $\F$ it assigns a real number  and the
assignment is consistent with the three axiom of probability.

\paragraph{Example 2: A fair die.}
In this case the outcome space is $\Omega= \{1,2,3,4,5,6\}$, the event
space is the power set of $\Omega$, the set of all sets of $\Omega$,
$\F = \Ps(\Omega)$, and $P(\{i\}) = 1/6,\; \text{for}\; i=1,\ldots, 6$. I will refer to this as the fair die probability space.

\paragraph{Example 3: A loaded die.} We can model the behavior of a
loaded die by assigning non negative weight values to each side of the
die. Let $w_i$ represent the weight of side $i$. In this case the
outcome space is $\Omega= \{1,2,3,4,5,6\}$, the event space is the
power set of $\Omega$, the set of all sets of $\Omega$, $\F =
\Ps(\Omega)$, and 
\begin{equation}
P(\{i\}) = w_i/(w_1 + \cdots + w_6),
\end{equation}
Note that if all weight values are equal, this probability space is
the same as the probability space in Example 2. 



\section{\bf Joint Probabilities}
The joint probability of two or more events is the probability of the
intersection of those events. For example consider the events
$A_1=\{2,4,6\}, \:A_2=\{4,5,6\}$ in the fair die probability space. Thus,
$A_1$ represents obtaining an even number and $A_2$ obtaining a number
larger than 3. 



\begin{equation}
P(A_1) = P(\{2\} \union \{4\} \union \{6\})  = 3/6
\end{equation}

\begin{equation}
P(A_2) = P(\{4\} \union \{5\} \union \{6\})  = 3/6
\end{equation}

\begin{equation}
P(A_1 \inter A_2) = P(\{4\}  \union \{6\})  = 2/6
\end{equation}
Thus the joint probability of $A_1$ and $A_2$ is $1/3$.

\section{\bf Conditional Probabilities}

The conditional probability of event $A_1$ given event $A_2$ is defined as follows
\begin{equation}
P(A_1 \given A_2) = \frac{P(A_1 \inter A_2)}{P(A_2)}
\end{equation}
Mathematically this formula amounts to making $A_2$ the new reference
set, i.e., the set $A_2$ is now given probability 1 since
\begin{equation}
P(A_2 \given A_2) = \frac{P(A_2 \inter A_2}{P(A_2)} = 1
\end{equation}
  Intuitively,
Conditional probability represents a revision of the original
probability measure $P$. This revision  takes into consideration
the fact that we know the event $A_2$ has happened with probability
1.   In the fair die example,
\begin{equation}
P(A_1 \given A_2) = \frac{1/3}{3/6} = \frac{2}{3}
\end{equation}
in other words, if we know that the toss produced a number larger than
3, the probability that the number is even is 2/3.

\section{\bf Independence of 2 Events}
The notion of independence is crucial. Intuitively two events $A_1$
and $A_2$ are independent if knowing that $A_2$ has happened does not
change the probability of $A_1$. In other words

\begin{equation}
P(A_1 \given A_2) = P(A_1)
\end{equation}
More generally we say that the events $A$ and $A_2$ are independent if and only if 
\begin{equation}
P(A_1 \inter A_2) = P(A_1) P(A_2)
\end{equation}
In the fair die example, $P(A_1 \given A_2) = 1/3$ and $P(A_1) = 1/2$, thus
the two events are not independent.

\section{\bf Independence of n Events}
We say that the events $A_1, \ldots, A_n$ are independent if and only
if the following conditions are met:

\begin{enumerate}
\item All pairs of events with different indexes are independent,
  i.e.,
\begin{equation}
P(A_i \inter A_j) = P(A_i) P(A_j)
\end{equation}
for all $i, j \in \{1,2, \ldots, n\}$ such that $i \neq j$. 

\item For all triplets of events with different indexes
\begin{equation}
P(A_i \inter A_j \inter A_k) = P(A_i) P(A_j) P(A_k)
\end{equation}
for all $i,j,k \in \{1, \ldots, n\}$ such that $i \neq j \neq k$.

\item Same idea for combinations of 3 sets, 4 sets, $\ldots$
\item For the n-tuple of events with different indexes
\begin{equation}
P(A_1 \inter A_2 \inter \cdots \inter A_n) = P(A_1) P(A_2)
  \cdots P(A_n)
\end{equation}
\end{enumerate}
You may want to verify that $2^n -n -1$ conditions are needed to check
whether $n$ events are independent. For example, $2^3- 3 -1 = 4$
conditions are needed to verify whether 3 events are independent.  
\paragraph{Example 1:} Consider the fair-die probability space and let 
$A_1=A_2=\{1,2,3\}$, and $A_3 =\{3,4,5,6\}$. Note 

\begin{equation}
P(A_1 \inter A_2 \inter A_3) = P(\{3\}) = P(A_1)P(A_2)P(A_3) = 1/6 
\end{equation}
However
\begin{equation}
P(A_1 \inter A_2) = 3/6 \neq P(A_1) P(A_2) = 9/36
\end{equation}
Thus $A_1, A_2, A_3$ are not independent.
\paragraph{Example 2:} Consider a probability space that models the
behavior a  weighted die with 8 sides: $\Omega = (1,2,3,4,5,6,7,8)$, $\F = 
\Ps(\Omega)$ and the die is weighted so that
\begin{align}
&P(\{2\}) = P(\{3\}) = P(\{5\}) = P(\{8\}) = 1/4\\
&P(\{1\}) = P(\{4\}) = P(\{6\}) = P(\{7\}) = 0
\end{align}
Let the events $A_1, A_2, A_3$ be as follows
\begin{align}
&A_1= \{1,2,3,4\}\\
&A_2 = \{1,2,5,6\}\\
&A_3 = \{1,3,5,7\}
\end{align}
Thus $P(A_1) = P(A_2) = P(A_3) = 2/4$. Note
\begin{align}
&P(A_1 \inter A_2) = P(A_1)P(A_2) = 1/4 \\
&P(A_1 \inter A_3) = P(A_1)P(A_3)= 1/4\\
&P(A_2 \inter A_3) = P(A_2)P(A_3)=1/4
\end{align}
Thus $A_1$ and $A_2$ are independent, $A_1$ and $A_3$ are independent
and $A_2$ and $A_3$ are independent.  However
\begin{equation}
P(A_1 \inter A_2 \inter A_3) = P(\{1\}) = 0 \neq P(A_1) P(A_2)P(A_3) = 
1/8
\end{equation}
Thus $A_1, A_2, A_3$ are not independent even though $A_1$ and $A_2$
are independent, $A_1$ and $A_3$ are independent and $A_2$ and $A_3$
are independent. 
\section{\bf The Chain Rule of Probability}
Let $\{A_1, A_2, \ldots, A_n\}$ be a collection of events. The chain
rule of probability tells us a useful way to compute the joint probability of the entire collection


\begin{align}
P(A_1 \inter A_2 \inter \cdots \inter A_n) =
\nonumber\\
 P(A_1) P(A_2\given A_1) &
 P(A_3 \given A_1\inter A_2) \cdots
 P(A_n \given A_1 \inter A_2 \inter \cdots \inter A_{n-1}) 
\end{align}
\paragraph{Proof:}
Simply expand the conditional probabilities and note how the denominator of the term $P(A_k \given A_1\inter  \cdots \inter A_{k-1})$ cancels the numerator of the previous conditional probability, i.e., 
\begin{align}
P(A_1) P(A_2 \given A_1) &P(A_3 \given A_1 \inter A_2) \cdots P(A_n \given A_1 \inter \cdots \inter A_{n-1}) = \\
&P(A_1) \frac{P(A_2 \inter A_1)}{P(A_1)} \frac{P(A_3 \inter A_2 \inter A_1)}{P(A_1 \inter A_2)}
\cdots \frac{P(A_1 \inter\cdots \inter A_n)}{P(A_1 \inter \cdots \inter A_{n-1})} \\
&= P(A_1 \inter \cdots \inter A_n)
\end{align}
\paragraph{Example:}
{A car company has 3 factories. 10\% of the cars are produced
in factory 1, 50\% in factory 2 and the rest in factory 3. One out of
20 cars produced by the first factory are defective. 99\% of the
defective cars produced by the first factory are returned back to the
manufacturer. What is the probability that a car produced by this
company is manufactured in the first factory, is defective and is not
returned back to the manufacturer.}\\
Let $A_1$ represent the set of cars produced by factory 1, $A_2$ the set
of defective cars and $A_3$ the set of cars not returned. We know 
\begin{align}
P(A_1) &= 0.1\\
P(A_2 \given A_1) &= 1/20\\
P(A_3 \given A_1 \inter A_2) = 1 - 99/100
\end{align}
Thus, using the chain rule of probability
\begin{align}
P(A \inter A_2 \inter A_3) = P(A_1) &P(A_2 \given A_1) P(A_3 \given A_1 \inter A_2) =\\
&(0.1)(0.05)(0.01)= 0.00005
\end{align}


\section{\bf The Law of Total Probability}

Let $\{H_1, H_2, \ldots\}$ be a countable collection of sets which is a partition of $\Omega$. In other words
\begin{align}
&H_i \inter H_j = \emptyset, \; \text{for $i \neq j$},\\
&H_1 \union H_2 \union \cdots = \Omega.
\end{align}
In some cases it is convenient to compute the probability of an event
$D$ using the following formula,

\begin{equation}
P(D) = P(H_1 \inter D) + P(H_2 \inter D) + \cdots
\end{equation}
This formula is commonly known as the law of total probability (LTP)
\paragraph{Proof:}  
First convince yourself that $\{ H_1 \inter D, H_2 \inter D, \ldots\}$
is a partition of $D$, i.e.,
\begin{align}
&(H_i \inter D)  \inter (H_j \inter D) = \emptyset, \; \text{for}\; i \neq j,\\
&(H_1 \inter D) \union (H_2 \inter D) \union \cdots = D.
\end{align}
Thus
\begin{align}
P(D) = P((H_1& \inter D) \union (H_2 \inter D) \union \cdots) = \\
&P(H_1 \inter D) + P(H_2 \inter D) + \cdots
\end{align}
We can do the last step because the partition is countable.
\begin{flushright} $\square$ \end{flushright}


\paragraph{Example:}

A disease called pluremia affects 1 percent of the population. There is
a test to detect pluremia but it is not perfect. For people with
pluremia, the test is positive 90\% of the time. For people without
pluremia the test is positive 20\% of the time. Suppose a randomly
selected person takes the test and it is positive. What are the
chances that a randomly selected person tests positive?:\\


Let $D$ represent a positive test result, $H_1$ not having pluremia,
$H_2$ having pluremia. We know  $P(H_1) = 0.99$, $P(H_2) = 0.01$. The test specifications
tell us: $P(D
\given H_1) = 0.2$ and  $P(D \given H_2) =0.9$. Applying the LTP 
\begin{align}
P(D) &= P(D \inter H_1) + P(D \inter H_2)\\ &= P(H_1) P(D \given H_1) +
 P(H_2) P(D \given H_2)\\ &= (0.99)(0.2)+(0.01)(0.9) = 0.207
\end{align}




\section{\bf Bayes' Theorem}
This theorem, which is attributed to Bayes (1744-1809), tells us how
  to revise probability of events in light of new data. It is
  important to point out that this theorem is consistent with
  probability theory and it is accepted by frequentists and Bayesian
  probabilists. There is disagreement however regarding whether the
  theorem should be applied to subjective notions of probabilities
  (the Bayesian approach) or whether it should only be applied to
  frequentist notions (the frequentist approach).

Let $D \in \F$ be an event with non-zero probability, which we will name . Let $\{H_1, H_2, \ldots\}$ be a countable collection of disjoint events, i.e,  
\begin{align}
&H_1 \union H_2 \union \cdots = \Omega\\
&H_i \inter H_j = \emptyset \; \text{if $i \neq j$} 
\end{align}
We will refer to $H_1, H_2, \ldots$ as ``hypotheses'', and $D$ as
``data''. Bayes' theorem says that
\begin{equation}
P(H_i \given D) = \frac{P( D \given H_i) P(H_i)}{P( D \given H_1) P(H_1) + P(D \given H_2) P(H_2) + \cdots}
\end{equation}
where
\begin{itemize}
\item $P(H_i)$ is known as the prior probability of the hypothesis $H_i$. It evaluates the chances of a hypothesis prior to the collection of  data.

\item $P(H_i \given D)$ is known as the posterior probability of the  hypothesis $H_i$ given the data.  

\item $P(D \given H_1), P(D\given H_2), \ldots$ are known as the likelihoods.
\end{itemize}
\noindent {\bf Proof:}
Using the definition of conditional probability 
\begin{equation}
P(H_i \given D) = \frac{ P(H_i \inter D)}{P(D)} 
\end{equation}
Moreover, by the law of total probability
\begin{align}
P(D) = 
P(D \inter H_1) + &P(D \inter H_2) +  \cdots   =\\& P(D \given H_1) P(H_1) + P(D \given H_2) P(H_2)
+ \cdots
\end{align}
\begin{flushright} $\square$ \end{flushright}



\paragraph{Example:}

A disease called pluremia affects 1 percent of the population. There is
a test to detect pluremia but it is not perfect. For people with
pluremia, the test is positive 90\% of the time. For people without
pluremia the test is positive 20\% of the time. Suppose a randomly
selected person takes the test and it is positive. What are the
chances that this person has pluremia?:\\


Let $D$ represent a positive test result, $H_1$ not having pluremia,
$H_2$ having pluremia. Prior to the the probabilities of $H_1$ and
$H_2$ are as follows: $P(H_2) = 0.01$, $P(H_1) = 0.99$. The test specifications
give us the following likelihoods: $P(D \given H_2) =0.9$ and $P(D
\given H_1) = 0.2$. Applying Bayes' theorem

\begin{equation}
P(H_2 \given D) = \frac{ (0.9)(0.01)}{(0.9)(0.01) + (0.2)(0.99)} = 0.043
\end{equation}
Knowing that the test is positive increases the chances of having
pluremia from 1 in a hundred to 4.3 in a hundred.


\section{Exercises}
\begin{enumerate}

\item Briefly describe in your own words the difference between the frequentist, Bayesian and mathematical notions of probability.

\item Go to the web and find more about the history of 2 probability theorists mentioned in this chapter.

\item Using diagrams, convince yourself of the rationality of  De Morgan's law:
\begin{equation}
(A\union B)^c = A^c\inter B^c
\end{equation}

\item Try to prove analytically De Morgan's law. 


\item Urn A has 3 black balls and 6 white balls. Urn B has 400 black balls and 400 white balls. Urn C has 6 black balls and 3 white balls. A person first randomly chooses one of the urns and then grabs a ball randomly from the chosen urn. What is the probability that the ball be black? If a person grabbed a black ball. What is the probability that the ball came from urn B?

\item 

The probability of catching Lyme disease after on day of hiking in the
Cuyamaca mountains are estimated at less than 1 in 10000. You feel bad
after a day of hike in the Cuyamacas and decide to take a Lyme disease
test. The test is positive. The test specifications say that in an
experiment with 1000 patients with Lyme disease, 990 tested
positive. Moreover. When the same test was performed with 1000
patients without Lyme disease, 200 tested positive. What are the
chances that you got Lyme disease.

\item This problem uses Bayes' theorem to combine probabilities as subjective beliefs with probabilities as relative frequencies. A friend of yours
believes she has a 50\% chance of being pregnant. She decides to take
a pregnancy test and the test is positive. You read in the test
instructions that out of 100 non-pregnant women, 20\% give false
positives.  Moreover, out of 100 pregnant women 10\% give false
negatives. Help your friend upgrade her beliefs.

\item In a communication channel a zero or a one is transmitted. The
probability that a zero is transmitted is 0.1. Due to noise in the
channel, a zero can be received as one with probability 0.01, and a
one can be received as a zero with probability 0.05. If you receive a
zero, what is the probability that a zero was transmitted? If you
receive a one what is the probability that a one was transmitted?


\item Consider a probability space $(\Omega, \F,P)$. Let $A$ and $B$ be sets of $\F$, i.e., both $A$ and $B$ are  sets whose elements belong to $\Omega$. Define the set operator $''-''$ as follows
\begin{equation}
A - B = A \inter  B^c
\end{equation}
Show that $P(A-B) = P(A) - P(A \inter B)$

\item Consider a probability space whose sample space $\Omega$ is the natural numbers (i.e., 1,2,3, $\ldots$). Show that not all the natural numbers can have equal probability. 

\item Prove that any event is  independent
of the universal event $\Omega$ and of the null event $\emptyset$.

\item Suppose $\omega$ is an elementary outcome, i.e., $\omega \in \Omega$. What is the difference between $\omega$ and $\{\omega\}$?.  How many elements does $\emptyset$ have?. How
many elements does $\{ \emptyset \}$ have?

\item You are a contestant on a television game show. Before you are three
closed doors. One of them hides a car, which you want to win; the other
two hide goats (which you do not want to win).


First you pick a door. The door you pick does not get opened
immediately. Instead, the host opens one of the other doors to reveal
a goat. He will then give you a chance to change your mind: you can
switch and pick the other closed door instead, or stay with your
original choice. To make things more concrete without losing
generality concentrate on the following situation
 \begin{enumerate}
\item You have chosen the first door.
\item The host opens the third door, showing a goat.
\end{enumerate}
If you dont switch doors, what is the probability of wining the car?
If you switch doors, what is the probability of wining the car? Should
you switch doors? 
 


\item Linda is 31 years old, single, outspoken and very bright. She majored in philosophy. As a student she was deeply concerned with issues of discrimination and social justice, and also participated in anti-nuclear demonstrations. Which is more probable?
\begin{enumerate}
\item Linda is a bank teller?
\item Linda is a bank teller who is active in the feminist movement?
\end{enumerate}

\end{enumerate}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End: 
