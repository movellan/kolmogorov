% Copyright \copyright{} 1996,,1998, 2002 Javier R. Movellan. 
% This is an open source document.  Permission is granted
% to copy, distribute and/or modify this document under the terms of the
% GNU Free Documentation License, Version 1.1 or any later version
% published by the Free Software Foundation; with no Invariant Sections,
% no Front-Cover Texts, and no Back-Cover Texts.

\chapter{Intro to Experimental Design}




The goal of experimentation is to study causal relationships between
physical events. Behind every scientific experiment there are
individuals and indirectly entire societies trying to figure out
whether two or more phenomena are related (e.g., are lung cancer and
smoking related?, does a drug improve the life-expectancy of
patients?, does an education program help economically disadvantaged
students?). Different experimenters have to tackle issues specific to
their disciplines but generally speaking empirical scientists share
many deal of common problems: 1) They need to deal with the
variability and uncertainty inherent in natural data, 2) They need to
organize and communicate the obtained data in an efficient fashion, 3)
They need to make inferences based on limited samples of data, 4) They
need to design experiments carefully to leave as few possible interpretations of the results as possible.



\section{An example experiment}

In this chapter we will discuss general concepts of experimental
design.  For concreteness, I will introduce these concepts in relation
to the following experiment which was part of J. Ridley Stroop's~
\cite{Stroop35} doctoral dissertation. Stroop was intrigued by the
well known fact that it takes longer to name colors than to read color
names (i.e., it takes longer to say that a red patch is red than to
read the word ``RED''). Theories of the time proposed explanations
based on interference effects and thus Stroop decided to study the
effect of interfering color names upon naming colors. The results,
which were rather spectacular, are nowadays known as the ``Stroop
effect''. To accommodate our needs I have modified Stroop's original
experiment while maintaining the overall spirit of his work. You can
read his original experiment at the 1935 issue of the Journal of
Experimental Psychology \cite{Stroop35}.


\begin{itemize}
\item {\bf Materials}: 

There were 2 lists of stimuli:
\begin{itemize}
\item One list of 100 stimuli each of
which consisted of four capital X letters ``XXXX''. The color of the
ink varied randomly on each stimulus. These lists were used for the
``Neutral'' test condition.  

\item  One  list of 100 stimuli
consisting of color names (e.g. ``GREEN'') but with each word printed
with ink of color different from that of the color named by the
word. These lists were used for the ``Interference'' test
condition. The colors used were red, blue, green, brown, and
purple. The colors were arranged randomly while making sure that no
color would immediately follow itself. The words were printed an equal
number of times on each color (except for the color they named).
\end{itemize}

\item {\bf Subjects and Procedure}: 

Twenty volunteer college undergraduate students (10 males and 10
females) participated in the experiment. All participants were tested
individually. They were seated near the window so as to have good
daylight illumination from the left side.  A ten-word sample of each
test was read before reading the test the first time. The instructions
were to name the colors as they appeared in regular reading line as
quickly as possible and to correct all errors. On the signal ``Ready!
Go!'' the sheet which the subjects held face down was turned by the
participant and read aloud. The words were followed on another sheet
by the experimenter and the time was taken with a stop watch to a
fifth of a second.  Within each sex category half of the participants
were randomly assigned to the Neutral condition and half to the
Interference condition.
\item {\bf Results}

Means and standard deviations of the time it takes to name 100 stimuli appear in Table I.

\end{itemize}

\begin{table}[t]\label{stroop}
\begin{center} 
\begin{tabular}{ccccc} \toprule
\multicolumn{1}{c}{\textbf{Sex}}
 & \multicolumn{2}{c}{\textbf{Neutral}}
 & \multicolumn{2}{c}{\textbf{Interference}}
\\ \cmidrule(r){1-1}\cmidrule(lr){2-3}\cmidrule(l){4-5}
male   & 111.1 & [21.6] & 69.2 & [10.8] \\
female & 107.5 & [17.3] & 61   & [10.5] \\ \bottomrule
\end{tabular}
\caption{Mean Time and Standard Deviation [in square brackets] per 100 stimuli. Time measured in seconds.}
\end{center} 
\end{table}


\section{Independent, Dependent and Intervening Variables}


By convention the variables manipulated by the experimenter are known
as the {\bf independent variables} (e.g, the interference level).  The
different values of the independent variable are called {\bf treatment
  levels}, or simply {\bf treatments}. For example, in Stroop's
experiment the treatments were the ``Neutral'' and ``Interference''.
Experiments are designed to test whether the different levels of the
independent variable have an effect on another variable known as the
{\bf dependent variable}. In Stroop's experiment the dependent
variable is the reaction time.






\begin{figure}[h]\label{f1}
\centerline{\includegraphics{figures/l1f1.\gformat}}
\caption{Independent and intervening variables have a potential effect on the dependent variable.} 
\end{figure}



\paragraph{Intervening variables} are variables other than the independent
variable that have a potential effect on the dependent variable. Even
though we are interested on the relationship between the independent
and the dependent variables, in practice there are many other
variables that are also having an effect on the dependent variable.
For example, the temperature of the room, the age of the subject, the
particular mood of the subject at test time, may also have an effect
on the dependent variable. Some experimenters emphasize the fact that
they are not interested in the effects of some intervening variables,
by calling them {\bf nuisance variables}.

Good experimental designs control for all possible intervening
variables so that the only systematic source of variation that can
explain the results is the independent variable. If a study is poorly
designed and intervening variables may actually explain our results we
say that the design lacks {\bf internal validity}, or that it has {\bf
confounding variables}. For example, if we choose all the subjects in
the neutral condition to be males and all the subjects in the
interference condition to be females, we would say that sex is a
confounding variable.  The design lacks internal
validity and we would not be able to tell whether the effects on the
dependent variable are due to the variable ``sex'' or to the treatment
conditions (Neutral vs. Interference). 

The art of experimental design consists of making sure intervening
variables distribute in a ``fair'' fashion amongst the different
treatment conditions so that the only systematic differences between
the different treatment groups can be attributed to the independent
variable. The concept of ``fair'' distribution of the intervening
variables is very tricky and we should return to it later. For now
we'll just use the concept intuitively and say that when an
intervening variable is distributed in a ``fair'' fashion amongst the
treatment conditions we say that the variable has been {\bf
controlled}. If an intervening variable has been controlled then it
cannot be used to explain away our results. For example, if all our
subjects are females, and we still find a difference between the two
neutral and the interference conditions, clearly sex cannot explain
this difference. By holding the sex of all subjects constant we have
controlled its possible effect. In scientific parlance we'd say that
sex is a controlled variable.  Only when all possible intervening
variables are controlled we can proceed to analyze whether the
independent variable $X$ has an effect on the dependent variable
$Y$. In the next section we discuss the most common methods of control
available to scientists.

\section{Control Methods}\label{sec:control-methods}

\begin{enumerate}

\item {\bf Holding constant:} The idea is simple. We make sure  an intervening variable is held constant in all our observations. This disqualifies it as a potential explanation of
our results. For example if all subjects were treated in the same room
at the same temperature, it follows that temperature cannot explain
the obtained differences between treatment conditions. Holding
constant is a rather dramatic method of control but it has its
problems. For example, if we decide to hold ``sex'' constant by
studying only male subjects, we no longer know whether the results are
also applicable to female subjects. In the lingo of experimental
design, we say that holding things constant reduces the {\bf external
validity} of the experiment. External validity simply means the power
to generalize our results to populations larger than the specific set
of observations obtained in the experiment (e.g., our ability to say
that the Stroop effect occurs to subjects other than the 20 subjects
investigated in our experiment).


\item{\bf Blocking:} The idea in blocking methods is to basically
  replicate the study with different levels of the blocked intervening
  variable. For example we may ``block'' age by categorizing subjects
  into less than 15, 15-30, 30-40, 40-50, 50-60, more than 60). Then we can systematically study the experimental results within
each of the different blocks.  An important form of blocking is {\bf
blocking by subject}, which indicates that we actually block the
variable ``subject identity'', thus controlling all intervening
variables, known and unknown, that make subjects different from each
other. When we block by subject, each subject is considered as a block
and he/she goes through all the treatment conditions in the
experiment. The idea is to study the effect of a treatment within each
subject (e.g. study the difference between the neutral and
interference conditions on a subject by subject basis). In this case
the experiment is said to be {\bf within subjects}, or {\bf repeated
measures} as opposed to {\bf between subjects}. Within subject designs
are a bit trickier to analyze and thus we will later dedicate them a
special chapter. For now we will concentrate on between subject
designs. 








\item {\bf Probabilistic Control:} 
This is by far the most important methods of control and it is
arguably the one form of control that made rigorous experiments in the
social and biological sciences possible. The fact is that even though
we may try to control for known intervening variables, there will
always be intervening variables unknown to us that may have a
potential effect on our results. Hey, may be the position of Jupiter
at the time of the experiment has an effect on some people's capacity
to complete the Stroop task. Who knows?

So here is the problem: How can we possibly control for intervening
variables we cannot even think of? Scientists have found a very
ingenious way to solve this problem. The trick is to give up equal
distribution of intervening variable in {\em specific experiments},
instead what we maintain equal amongst the treatment conditions is the
{\bf probability} distribution of the variable.  We think of an
ensemble of replications of the same experiment and guarantee that
over this entire ensemble, the intervening variables will be
distributed evenly across the different treatment conditions. When we
do that, we say that the intervening variables have been {\bf
randomized}.

We have actually seen control by randomization in previous methods
even though I avoided calling it by its name. For example, in control
by matching we first clustered subjects in attempt to distribute
intervening variable equally. But since we had differences between the
subjects within each cluster, we then randomly assigned subjects to each
treatment condition.  In {\bf completely randomized designs} we simply
assign subjects randomly to each condition without any previous
matching. 

 We can think of the subject identity as an intervening variable that
includes all the peculiarities, known and unknown to us, that make
each subject different from everybody else at test time. We then
randomize the effects of all these known and unknown variables by
assigning each subject randomly across the different experimental
conditions. For example, we could put as many cards in a hat as the
number of different treatment conditions. Then we can ask each subject
in our experiment to draw a card identifying his/her treatment
condition. Note that by using probabilistic control, experiments
become {\bf a game of chance}, thus the importance of probability
theory and statistics in the analysis of experiments. 

Note again that randomization does not guarantee equal distribution of
intervening variables in single experiments, however it guarantees
that on average, if we were to repeat the same experiment many many
times, the intervening variables will be equally distributed among the
different treatment groups.  Technically speaking, when a variable is
randomized, we say that it is a {\bf random variable}, a mathematical
object studied in probability theory and statistics. We will finish up
this chapter with some useful concepts.

\item {\bf Matching:} You can think of matching as controlled randomization. For example, if we simply randomly assign subjects to different treatment conditions, it is always possible that in specific experiments exceptional subjects accumulate disproportionally in some treatment conditions. Matching techniques try to control for these possible imbalances in the following way. First  we  cluster  cluster our subjects in terms of their similarity with respect to  a particular variable.  Each cluster must has as many
subjects as treatment conditions. For example, if we wanted to ``match
by age'' in the Stroop experiment, we would put together groups of 2
people of similar age. If we had 4 subjects with ages 18, 49, 30, and
16, we would make a cluster with the younger subjects (18, 16) and
another cluster with the older subjects (49,30). Once the clusters are
formed, subjects within each cluster are {\bf randomly assigned} to
different treatment groups. 



\item{\bf Statistical Control:} 
In many cases we may not be able to guarantee a uniform distribution
of the intervening variables but we may at least try to make some
aspect of the distribution of variables be the same in all the
treatment groups.  For example, it would be impossible for the room
temperature to be exactly the same all the time but at least we may
want to guarantee than on average the room temperature was the same
across treatment conditions.  We may also want to make sure that the
average age of the subjects is about the same across all treatment
groups.  Note that equalizing averages does not guarantee equal
distributions. For example, treatment group 1 could have 50 \% 5 year
old subjects and 50\% 35 year old subjects. Group 2 could have just 20
year old subjects. Both groups have the same average age but obviously
the distribution is completely different. Although this form of
control is rather weak, in some cases it may be all we can do. In such
cases we report in a crystal clear way our method of control and leave
it it is up to the scientific community to decide on the validity of
our results.

\end{enumerate}
\section{Useful Concepts}

\begin{enumerate}
\item {\bf Experiments vs. Observational studies:} 

Scientific studies are commonly classified into observational and
experimental depending on whether the independent variable is
observational or experimental. An independent variable is {\bf
Experimental} if its value is assigned by the experimenter to the
subjects.\footnote{Note that being ``experimental'' is a property of the
independent variable, not a property of the dependent variable.  If you
want to know whether a study is experimental you need to focus on the
independent variable; do not worry about the dependent variable.}  For
example if experimenters assign different subjects different dosages
of a drug then the drug's dose is an experimental variable. The
crucial point here is that the experimenter, not the subjects, decides
which treatment each subject will receive.

If an independent variable is not experimental then it is called {\bf
observational}. For example, age does not qualify as an experimental
variable since age cannot be assigned to subjects. All experimenters
can do is to classify subjects according to their age.

The goal of experimental independent variables is to study cause and effect
relationships (e.g., to establish whether a drug is or is not
a beneficial to treat  a particular sickness). Observational
variables on the other hand can only be used to study relationships,
without  establishing causes and effects.  For example, early
observational studies about the relationship between smoking and
cancer showed that smokers tended to have a higher cancer rate than
non-smokers. The dependent variable was whether the subject had
cancer. The independent variable was whether the person smoked.  Note
in this case the independent variable is observational, since it would
be unethical for experimenters to force a group of subject to
smoke. The experimenters could only classify subjects into smokers or
non-smokers. Unfortunately, the results of observational studies like
this were inconclusive since they could be due to an intervening
variable that is systematically different between the two groups. For
example highly stressed subjects could have a propensity to smoke and
a higher cancer risk. According to this view, stress, and not smoking
per se, could cause cancer. It could also be the case that subjects
with cancer have a higher propensity to smoke just because cancer
stresses you out. According to this view it is cancer that causes
smoking!  

A classic example of observational studies involves the analysis of
differences between men and women on some dependent variable of
interest.  Note that since these these studies are observational. We
can investigate whether men and women are different on some dependent
variable (e.g., memory, IQ, or income) but we cannot assess what the
causes are for the obtained differences. These causes could be due to
trivial variables, such as the fact that on average men tend to be
heavier, or they could be due to complex socio-political variables
(e.g., the underlying cause for the observed differences could be that
the educational system treats men and women differently causing).



\item {\bf Exploratory vs. Confirmatory Studies:}  The distinction
between these two different types of research is somewhat subtle, yet
important. Confirmatory studies tend to have only a small and well
defined set of treatments and behaviors under study. Clear a-priori
hypotheses are made about what to expect in the data and only those
a-priori hypothesis are tested. For example, a confirmatory study may
investigate whether taking an aspirin a day reduces the chances of heart
attack. In exploratory studies the experimenters may have a general
idea about what to expect in the data but they are willing to analyze
the data in a very wide variety of ways with the hope of finding
potentially interesting results.  Exploratory studies are extremely
important in science and they are the source in many cases of new
unexpected results. However, the results in exploratory studies tend
to be inconclusive basically because by analyzing the data in many
different ways it is always possible to find some sort of pattern that
may just be due to chance. You can see this effect in sports analysis
when all sorts of different statistics are used to explain after the
fact why a team has lost or won.  In general, results of exploratory
analysis are treated with caution and attempts are made to replicate
these results with well defined confirmatory experiments.


\item {\bf Random Assignment vs. Random Selection:} 
This is a very important distinction that novices sometimes
confuse. Random selection refers to the process of selecting a sample
from a population of subjects. In many experiments this selection does
not need to be random. For example, in psychophysical experiments may
times the experimenter himself and his colleagues are the subjects of
the study, and thus they are not ``randomly selected''. However these
non-randomly selected subjects may still be randomly assigned to the
different treatment conditions. Random selection has an effect on the
external validity of the experiment, our power to generalize to a
larger population. Random assignment has an effect on the internal
validity of the experiment, our power to infer that the independent
variable is the one causing the observed results. 

\end{enumerate}

\section{Exercises}

\begin{enumerate}


\item The following questions refer to the experiment outlined at the beginning of this chapter:


\begin{enumerate}
\item What are the independent and dependent variables?

\item Find intervening variables that where held constant, matched and
randomized.

\item Is the  experiment  experimental or observational? Explain why.

\item Is the experiment within subjects or within subjects? 

\end{enumerate}

\item Design a between subject experiment to test whether bees can distinguish red
from green.

\item Design a between subject experiment to test whether human newborns can tell the
difference between male and female faces.


\item Design  an experiment  to test
whether caffeine has an effect on the subjective feeling of
Nervousness. Make sure you specify the following:
\begin{enumerate}

\item Independent variable and its implementation in the different
treatment groups.

\item Dependent variable and how you are going to measure it.

\item Intervening variables and how you will control them. 



\end{enumerate}


\item Consider the following hypothetical experiment inspired on
research by Baker and Theologus \cite{BakerAndTheologus72} on the
effects of caffeine on visual monitoring.


{\sl 

The purpose of the was to asses the effect of caffeine on a visual
monitoring task that simulated automobile night driving.  Subjects sat
in a semi-darkened room approximately 12 oft from a visual display
that consisted of two 1-inch red lights spaced 6 in. apart.  At random
intervals ranging from 1.5 to 3.5 min., the red lights were driven
apart at a rate of 12.56 in/min for 30 seconds.  The geometry of the
viewing condition corresponded to a driving situation in which one
vehicle followed 60 yd. behind a lead vehicle at night driving. The
speed at which the lights separated simulated what would be perceived
if the lead vehicle reduced its velocity by 0.92 mph.  Subjects had to
continuously monitor the two red-lights and press a button whenever
they detected any separation of the two lights. The reaction time,
from the beginning of the light separation to the initiation of the
subject;s response was automatically recorded.  Testing was conducted
over a 4-hr period with 20 trials being administered each hour.  for a
total of 80 responses per subject.  During the 4 hour testing period
an FM radio tuned to a local station was played to avoid excessive
sensory deprivation.  There were 10 paid male volunteers drawn from
universities in the Washington DC area. Average age was 29.91
yrs. Subjects were matched by age (e.g. divided into 5 couples with no
more than a year difference). Each member of the age-matched couple
was randomly assigned to a different treatment group. There were two
such treatment groups: 1) A control group of subjects given placebo
tablets just before testing began and 2 hours before the end of
testing. 2) An experimental group of subjects that were given a tablet
with 200 mg. of caffeine just before testing began, and another tablet
with 200 mg. of caffeine 2 hours before the end of testing.  Drug and
placebo tablet administration was conducted according to a
double-blind paradigm, with tablets placed in coded envelopes in a
manner unknown to the subjects and test administrators.
}
\begin{enumerate}
\item What are the independent and dependent variables?

\item Find intervening variables that where held constant, matched and
randomized.

\item Is the  study  experimental or observational? Explain why.

\item Is the study within subjects or within subjects? 

\end{enumerate}


\item What is the difference between external and internal validity?

\item A friend of yours observes that swimmers  nicer bodies than other athletes. She concludes that swimming helps have a good body. What's wrong with this conclusion? Design an experiment to test your friend's hypothesis.

\item Read the Abstract, Introduction and Methods sections of the article in Reference  \cite{aspirin89}. Describe the control methods used in the study.
 

\end{enumerate}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End: 
