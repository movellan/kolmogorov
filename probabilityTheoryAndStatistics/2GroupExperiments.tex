% Copyright \copyright{} 1996,,1998, 2002 Javier R. Movellan. 
% This is an open source document.  Permission is granted
% to copy, distribute and/or modify this document under the terms of the
% GNU Free Documentation License, Version 1.1 or any later version
% published by the Free Software Foundation; with no Invariant Sections,
% no Front-Cover Texts, and no Back-Cover Texts.

\chapter{Experiments with 2 groups} In this chapter we learn how to
apply the T test to experiments with 2 groups. For example one group
may receive a drug and another group may receive a placebo. The form
of the test depends on whether the experiment is performed with
different subjects in each group or whether the same subjects are used 
for the two groups. The first type of experiment is called {\bf between
subjects} or {\bf randomized}. The second is called {\bf within subjects} or
{\bf repeated measures}. 
\section{Between Subjects Experiments}

In this case we have two sets of random variables. The first set
describes measurements in a group of subjects, the second set
describes measurements in a different group of subjects. The number of
subjects per group is the same, and we will represent it with the
letter $n$. Thus, the total number of subjects in the experiment is
$2n$. Let $X_{1,1}, \ldots, X_{1,n}$ represent the measurements for the
first group and $X_{2,1}, \ldots, X_{2,n}$ represent the measurements
for the second group.  Let $\bar{X}_1$ represent the average of the
observations in the first group and $\bar{X}_2$ the average of the
observations in group 2. We thus have
\begin{align}
\bar{X}_1 &= \frac{1}{n} \sum_{i=1}^n X_{1,i}\\
\bar{X}_2 &= \frac{1}{n} \sum_{i=1}^n X_{2,i}\\
S^2_1 &=  \frac{1}{n-1} \sum_{i=1}^n (X_{1,i} - \bar{X}_1)^2\\
S^2_{\bar{X}_1} &=  \frac{1}{n} S^2_1\\
S^2_2 &=  \frac{1}{n-1} \sum_{i=1}^n (X_{2,i} - \bar{X}_2)^2\\
S^2_{\bar{X}_2} &=  \frac{1}{n} S^2_2
\end{align}
The null hypothesis specifies the expected value of the
difference between the sample means of the two groups, i.e,
$E(\bar{X}_2 - \bar{X}_1 \given H_n \:\text{true})$. In most
experiments the null hypothesis says, that this expected value is
zero, i.e., that on average the two groups are no different. The
variable of interest in this case is $\bar{X}_2 - \bar{X}_1$.  We
assume that the random variables $X_{1,1}, \ldots, X_{1,n}$ and the
random variables $X_{2,1}, \ldots, X_{2,n}$ are independent and
Gaussian. Moreover we assume that within each group all the random
variables have the same expected value
\begin{gather}
E(X_{1,1}) = E(X_{1,2}) = \cdots = E(X_{1,n})\\
E(X_{2,1}) = E(Y_{2,2}) = \cdots = E(Y_{2,n})\\
\end{gather}
and that all the random variables have the same variance
\begin{equation}
\Var(X_{1,1}) = \cdots = \Var(X_{1,n}) = \Var(X_{2,1}) = \cdots = \Var(X_{2,n})  = \sigma^2_X
\end{equation}
where we do not the value of $\sigma^2_X$. Note in single group 
experiments the variable of interest is the sample mean for that group
and we use an estimate of the standard deviation of the sample
mean. In two group experiments the random variable of interest is
$\bar{X}_2 - \bar{X}_1$, i.e, the difference between two group means,
and thus we need to come up with an estimate of the standard deviation
of $(\bar{X}_2 - \bar{X}_1)$. Note that since $\bar{X}_1$ and
$\bar{X}_2$ are independent ravs
\begin{align}
\Var(\bar{X}_2 - \bar{X}_1) &= \Var(\bar{X}_2) + \Var(- \bar{X}_1) =
\Var(\bar{X}_2) + \Var(\bar{X}_1) \\ &= 2 \Var(\bar{X}_1) = \frac{2}{n} \Var(X_{1,1})
\end{align}
The last two steps are valid because we assume $\Var(X_{1,1}) = \cdots =
\Var(X_{2,n})$. Thus $\Var(\bar{X}_1 ) = \Var(\bar{X}_2)$. We can get an umbiased estimate of the variance of the observations by averaging $S^2_1$ and $S^2_2$. We represent this pooled estimate as $S^2$
\begin{equation}
S^2 = \frac{1}{2} (S^2_1 + S^2_2)
\end{equation}
 An unbiased estimate of the variance of the difference between sample
means is as follows
\begin{equation}
S^2_{\bar{X}_1 - \bar{X}_2} = \frac{2}{n} S^2 = S^2_{\bar{X}_2} + S^2_{\bar{X}_1}
\end{equation}
and the  $T$ random variable is defined as follows
\begin{equation}
T = \frac{\bar{X}_2 - \bar{X}_1 - E(\bar{X}_2 - \bar{X}_1 \given H_n true)}{S_{\bar{X}_2 - \bar{X}_1}} = 
\frac{\bar{X}_2 - \bar{X}_1 - E(\bar{X}_2 - \bar{X}_1 \given H_n true)}{\sqrt{ S^2_{\bar{X}_2} + S^2_{\bar{X}_1}}}
\end{equation}
In this case the number of degrees of freedom is $2(n-1)$ since
$S_{\bar{X}_2 - \bar{X}_1}$ is based on $2(n-1)$ independent
observations, i.e., $n-1$ independent observations in $S^2_1$
and $n-1$ independent observations in $S^2_2$.

If $H_n$ is true and the assumptions are met, then $T$ follows the
distribution described by Gosset, which is available in tables. The
procedure to do one tailed and two tailed tests is identical as for
one group experiments provided the new formulas for $T$ and for $df$
are used.

\paragraph{Example}
{\it 
A group of 6 migrane patients was gathered and randomly assigned to
one of two conditions. In the experimental group subject were given
500 mg of a new migrane drug whenever they had a migrane access. In
the control group subjects were given two aspirins. One hour after
the access, subjects were asked to rank from 0 to 100 how bad the
headache was. 0 meaning, ``headache? what headache?'', and 100 meaning ``I cant
take it anymore''. The results were as follows

\noindent Experimental Group =$[10,30,20]$\\ 
Control Group = $[56,64,60]$\\ 
}
First of all we compute the values taken by the relevant random variables in our experiment
\begin{gather}
\bar{X}_1 = (10+30+20)/3 = 20\\
\bar{X}_2 = (56+64+60)/3 = 60\\
S^2_1 = ((10-20)^2+(30-20)^2+(20-20)^2)/2 = 100\\
S^2_2 = ((56-60)^2+(64-60)^2+(60-60)^2)/2 = 16\\
S_{\bar{X}_1 - \bar{X}_2} = \sqrt{100/3 + 16/3} = 6.21
\end{gather}
In our case it makes sense to use a one tailed test, since we are
trying to reject the hypothesis that the aspirin works better or
equal to the new drug. Thus the null hypothesis tells us that
$E(\bar{X}_2 - \bar{X}_1 \given H_n \:\text{true}) \leq 0$. The extreme
case, i.e., what we call $H_x$ says $E(\bar{X}_1 - \bar{X}_2 \given H_x
\:\text{true}) = 0$. Thus,

\begin{equation}
T = \frac{\bar{X}_2 - \bar{X}_1 - E(\bar{X}_2 - \bar{X}_1 \given H_x\: \text{ true})}{S_{\bar{X}_2 - \bar{X}_1}}  =
\frac{(60-40) - 0}{6.21} = 3.22
\end{equation}
The number of degrees of freedom is $(2)(3-1) = 4$. We go to tables
and find that for 4 degrees of freedom $P(T > 2.13 \given H_x
\;\text{true}) = 0.05$. Thus the critical value is 2.13. Since the
value taken by $T$ is larger than 2.13 we have enough evidence to
reject the null hypothesis. We can say that the new drug works better than aspirin. 

\subsection{Within Subjects Experiments}

In the previous experiments we assumed all the measurements were
independent. In within subjects  experiments this assumption is clearly
incorrect: if we test the same subjects in the two
groups, then the random variables in the first group will be related
to the random variables in the second group. This type of experiments
are called ``repeated measurements'' or within subjects. In this case
we work with  the
differences between the observations obtained in the two groups on a
subject by subject basis
\begin{equation}
D_{j} = X_{2,j} - X_{1,j} 
\end{equation}
and the sample average of the difference would be
\begin{equation}
D = \frac{1}{n} \sum_{j=1}^n D_j = \bar{X}_2 - \bar{X}_1
\end{equation}
In most cases the null hypothesis says that there is no differences in the expected values of the two groups, i.e., $E(\bar{D} \given H_n \: \text{true}) =0$. The $T$ random variable follows
\begin{equation}
T = \frac{\bar{D} - E( \bar{D} \given H_n \: \text{true}) }{S_{\bar{D}}}
\end{equation}
This effectively transforms a 2 group experiment into a single group
experiment that can be analyzed with the techniques we saw in the
previous chapter.  
\paragraph{Example}
{\it Three undergraduate students from UCSD volunteered to participate
  on an experiment to test the effect of caffeine on reaction time.
  Each student was tested in two different conditions with the tests
  being separated in time by one week. In one condition the subjects
  drunk a double espresso. In the other conditions the subjects drunk
  a double decaffeinated espresso. The order of the two conditions was
  randomly assigned. On each condition, the subjects were tested 15
  minutes after drinking the espresso. Subjects were tested on a task
  that required rapid reaction to a stimulus. The average reaction
  time in milliseconds of each subject was recorded.  The results were
  as follows

\noindent Caffeine Condition =$[10,30,20]$\\ 
Decaf Condition = $[56,64,60]$\\ 
The first number on each condition corresponds to the first subject,
second number to second subject and third number to the
third subject. Is  the difference in reaction time between the two conditions 
statistically significant?}

This is a repeated measures experiment so the  T-test would be based on
the difference measurements obtained for each of the three subjects.
\begin{gather}
D_1   = 10 - 56 = -46\\
D_2 = 30 - 64 = - 34\\
D_3  = 20 - 60 = -40\\
\bar{D} = (-46-34-40)/3 = -40\\
S^2_D = \frac{1}{2}((6^2)+(6^2)+0^2) = 18\\
S^2_{\bar{D}} = 18/3 = 6\\
T = \frac{18 -0}{\sqrt{6}} =  7.35\\
df = (3-1)8 = 2\\
P(T \given H_x > 2.91 \: \text{True}) = 1/20 \; \;\text{Obtained using T-tables}
\end{gather}
Thus the critical value is 2.91. Since 7.35 is larger than 2.91. The
difference in reaction time between the two groups is statistically
significant. 
\section{Exercises}
\begin{enumerate}
\item
{\sl A San Diego based biotek company is testing whether a new drug
has a good effect on amnesia, the inability to remember recently
presented materials.  The sample consisted of 8 amnesic patients that
volunteered to participate in the experiment. The subjects were randomly
assigned to either the experimental group (drug) or the control group
(placebo). Subjects were treated with 125 mg a day of the drug or the
placebo for 30 days. After one month subjects were given a list of 100
training pictures one at a time at 2 second intervals. Half an hour
later subjects were presented 200 new pictures: the 100 training
pictures plus 100 new ones.  Subjects were asked to discriminate
whether the pictures were old or new. The dependent variable was the
percentage of correctly discriminated pictures. The results in the
experimental group were as follows:$\{61,60,59,60\}$. The results in
the control group were as follows: $\{50,51,50,49\}$.}

\begin{enumerate}

\item Is the experiment randomized or within subjects?


\item Specify the  null hypothesis if you were to use a two tailed test. 

\item Do you have enough evidence to reject this null hypothesis?

\item Specify the null hypothesis if you were to use a one tailed test. 

\item Do you have enough evidence to reject this null hypothesis?

\item What assumptions did you make? 

\item A replication of the same experiment with a different random sample of subjects results in a control group mean of 10  instead of 50? Do you find this surprising? Justify your response.

\item Repeat the analysis assuming the same subjects are used in both groups. For example, Subject 1 gets a score of 61 when he/she takes a drug and  a score of 50  when he/she takes the placebo. 
\end{enumerate}
\end{enumerate}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End: 
