% Copyright \copyright{} 1996,,1998, 2002 Javier R. Movellan. 
% This is an open source document.  Permission is granted
% to copy, distribute and/or modify this document under the terms of the
% GNU Free Documentation License, Version 1.1 or any later version
% published by the Free Software Foundation; with no Invariant Sections,
% no Front-Cover Texts, and no Back-Cover Texts.

\chapter{Expected Values} The expected value (or mean) of a random
variable is a generalization of the notion of arithmetic mean. It is a
number that tells us about the ``center of gravity'' or average value
we would expect to obtain is we averaged a very large number of  observations from that random variable.  The expected
value of the random variable $X$ is represented as $E(X)$ or with the
Greek letter $\mu$, as in $\mu_X$ and it is defined as follows
\begin{equation}
E(X) = \mu_X=
\begin{cases}
 \sum_{u \in \R} p_X(u) u  & \text{for discrete ravs} \\
  \int_{-\infty}^{+\infty} p_X(u) u \; du & \text{for continuous ravs}
\end{cases}
\end{equation}
We also define the expected value of a number as the number itself,
i.e., for all $u \in \R$
\begin{equation}
E(u) = u 
\end{equation}


\paragraph{Example:}
The expected value of a Bernoulli random variable with parameter $\mu$ is as follows

\begin{equation}
E(X) = (1-\mu)(0.0) + (\mu) (1.0) = \mu
\end{equation}
\paragraph{Example:}
A random variable $X$ with the following pdf 
\begin{equation}
p_X(u)= 
\begin{cases}
\frac{1}{b-a} & \text{ if $u \in [a,b]$ } \\
0 &\text{ if $u \nin [a,b]$ } 
\end{cases}
\end{equation}
is known as a uniform $[0,1]$ continuous random variable. Its expected
value is as follows
\begin{multline}
E(X) =\int_{- \infty}^{\infty} p_X(u) u\;du = \int_{a}^b \frac{1}{b-a} u\; du
\\= \frac{1}{(b-a)} \Big[ \frac{u^2}{2}\Big]_a^b = 
\\= \frac{1}{(b-a)} (\frac{b^2 - a^2}{2}) = (a+b)/2
\end{multline}

\paragraph{Example:}
The expected value of an exponential random variable with parameter
$\lambda $is as follows (see previous chapter for definition of an
exponential random variable)

\begin{equation}
E(X) = \int_{-\infty}^\infty u p_X(u) du  = \int_{0}^\infty u \lambda \exp( - \lambda u)  du 
\end{equation}
and using integration by parts
\begin{equation}
E(X) =\Big[ -u \exp(-\lambda u) \Big]_0^{\infty} + \int_{0}^\infty \lambda \exp( - \lambda u) du =  - \Big[ \lambda \exp( - \lambda u ) \Big]_0^{\infty} = \frac{1}{\lambda}
\end{equation}

\section{Fundamental Theorem of Expected Values}
This theorem is very useful when we have a random variable $Y$ which
is a function of another random variable $X$. In many cases we may
know the probability mass function, or density function, for $X$ but
not for $Y$. The fundamental theorem of expected values allows us to
get the expected value of $Y$ even though we do not know the
probability distribution of $Y$. First we'll see a simple version of
the theorem and then we will see a more general version.
\paragraph{Simple Version of the Fundamental Theorem:}
Let $X \st \Omega \to \R$ be a rav and $h \st \R \to \R$ a function. Let $Y$ be a rav defined as follows: 
\begin{equation}
Y(\omega) = h(X(\omega)) \;\text{for all $\omega \in \R$}
\end{equation}
or more succinctly
\begin{equation}
Y = h(X)
\end{equation}
Then it can be shown that
\begin{equation}
E(Y) =
\begin{cases}
 \sum_{u \in \R} p_X(u) h(u)  & \text{for discrete ravs} \\
  \int_{-\infty}^{+\infty} p_X(u) h(u) \; du & \text{for continuous ravs}
\end{cases}
\end{equation}
\paragraph{Example:}
Let $X$ be a Bernoulli rav and $Y$ a random variable defined as $Y =
(X - 0.5)^2$. To find the expected value of $Y$ we can apply the fundamental theorem of expected values with $h(u) = (u - 0.5)^2$. Thus,
\begin{multline}
E(Y) = \sum_{u \in \R} p_X(u) (u - 0.5)^2 =p_X(0)(0 -0.5)^2 + p_X(1)(1 - 0.5)^2\\ = (0.5)(0.5)^2 + (0.5)(-0.5)^2 = 0.25
\end{multline}

\paragraph{Example:}
The average entropy, or information value (in bits) of a random
variable $X$ is represented as $H(X)$ and is defined as follows
\begin{equation}
H(X) = - E( \log_2 p_X(X))
\end{equation}
To find the entropy of a Bernoulli random variable $X$ with parameter
$\mu$ we can apply the fundamental theorem of expected values using
the function $h(u) = \log_2 p_X(u)$ . Thus,

\begin{multline}
H(X) = \sum_{u \in \R} p_X(u) \log_2 p_X(u) = p_X(0) \log_2 p_X(0) +
p_X(1) \log_2 p_X(1) \\ = (\mu) \log_2(\mu) + (1- \mu) \log_2(1-\mu)
\end{multline}
For example, if $\mu =0.5$, then 
\begin{equation}
H(X)= (0.5) \log_2(0.5) + (0.5)\log_2(0.5) = 1 \; \text{bit}
\end{equation}
\paragraph{General Version of the Fundamental Theorem:}
Let $X_1, \ldots, X_n$ be random variables,  let $Y = h(X_1, \ldots, X_n)$, where $h\st \R^n \to \R$ is a function. Then
\begin{equation}
E(Y) = 
\begin{cases}
 \sum{(u_1,\ldots,u_n) \in \R^n} p_{X_1,\ldots,X_n}(u_1,\ldots,u_n) h(u_1,\ldots,u_n)  & \text{for discrete ravs} \\
  \int_{-\infty}^{+\infty} \cdots \int_{-\infty}^{+\infty} p_{X_1,\ldots,X_n}(u_1,\ldots,u_n) h(u_1,\ldots,u_n) du_1 \cdots du_n\; & \text{for continuous ravs}
\end{cases}
\end{equation}



\section{Properties of Expected Values}
Let $X$ and $Y$ be two random variables and $a$ and $b$ two real numbers. Then
\begin{equation}
E(X+Y) = E(X) + E(Y)
\end{equation}
and
\begin{equation}
E(a+ bX) = a + bE(X)
\end{equation}
Moreover if $X$ and $Y$ are independent then
\begin{equation}
E(XY) = E(X)E(Y)
\end{equation}
We will prove these properties using  discrete ravs. The proofs
are analogous for continuous ravs but substituting sums by
integrals. 

\paragraph{ Proof:}
By the fundamental theorem of expected values
\begin{multline}
E(X+Y) = \sum_{u \in \R} \sum_{v \in \R}  p_{X,Y}(u,v) (u+v) \\=
 (\sum_{u \in \R}  u \sum_{v\in \R} p_{X,Y}(u,v) ) + (\sum_{v \in \R} v \sum_{u \in \R}  p_{X,Y}(u,v) )\\ = 
\sum_{u \in R} u p_X(u) + \sum_{v \in \R} v p_Y(v)= E(X) + E(Y) 
\end{multline}
where we used the law of total probability in the last step.


\paragraph{ Proof:}
 Using the fundamental theorem of expected values
\begin{multline}
E(a+bX) = \sum_{u}  p_X(u)  (a + b u )
\\= a \sum_{u\in \R}  p_X(u) + b\sum_{u}u  p_X(u)
= a   +b E(X) 
\end{multline}

\paragraph{ Proof:} 
By the fundamental theorem of expected values
\begin{equation}
E(XY) =  \sum_{u\in \R} \sum_{v\in \R}  p_{X,Y}(u,v) uv 
\end{equation}
if $X$ and $Y$ are independent then $p_{X,Y}(u,v) = p_X(u)
p_Y(v)$. Thus
\begin{equation}
E(XY) =  [\sum_{u} u p_X(u)]\: [  \sum_{v}  v  p_{Y}(v)] = E(X) E(Y) 
\end{equation}
\begin{flushright} $\square$ \end{flushright}




\section{Variance}
The variance of a random variable $X$ is a number that represents the
amount of variability in that random variable.  It is defined as the
expected value of the squared deviations from the mean of the random
variable and it is represented as $\Var(X) $ or as  $\sigma^2_X$

\begin{equation}
\Var(X) = \sigma^2_X =  E[ (X - \mu_X)^2 ]= 
\begin{cases}
 \sum_{u \in \R}  p_X(u) (u - \mu_X)^2 & \text{for discrete ravs} \\
  \int_{-\infty}^{+\infty} p_X(u) (u - \mu_X)^2 du & \text{for continuous ravs}
\end{cases}
\end{equation}
The {\bf standard deviation} of a random variable $X$  is
represented as $\Sdev(X)$ or $\sigma$ and it is  the square root
of the variance of that random variable
\begin{equation}
\Sdev(X) = \sigma_X = \sqrt{\Var(X)}
\end{equation}
The standard deviation is easier to interpret than the variance for it
uses the same units of measurement taken by the random variable. For
example if the random variable $X$ represents reaction time in
seconds, then the variance is measured in seconds squares, which is
hard to interpret, while the standard deviation is measured in
seconds. The standard deviation can be interpreted as a ``typical''
deviation from the mean. If an observation deviates from the mean by
about one standard deviation, we say that that amount of deviation is
``standard''. 

\paragraph{Example:}
The variance of a Bernoulli random variable with parameter $\mu$ is as follows

\begin{equation}
\Var(X) = (1-\mu)(0.0 - \mu)^2 + (\mu)(1.0 - \mu)^2 = (\mu)(1 - \mu)
\end{equation}


\paragraph{Example:}
For a continuous uniform $[a,b]$ random variable $X$, the pdf is as follows 
\begin{equation}
p_X(u)= 
\begin{cases}
\frac{1}{b-a} & \text{ if $u \in [a,b]$ } \\
0 &\text{ if $u \nin [a,b]$ } 
\end{cases}
\end{equation}
which we have seen as expected value   $\mu_X= (a+b)/2$. Its variance is as follows
\begin{equation}
\sigma^2_X  = \int_{-\infty}^{+\infty} u p_X(u) du = \frac{1}{b-a} \int_a^b (u - (a+b)/2)^2 du 
\end{equation}
and doing a change of variables $y = (u - (a+b)/2)$
\begin{equation}
\sigma^2_X  = \frac{1}{b-a}  \int_{(a-b)/2}^{(b-a)/2} y^2 dy = \frac{1}{b-a} \Big[ y^3/3\Big]_{(a-b)/2}^{(b-a)/2}  = \frac{b-a}{12}
\end{equation}

\paragraph{Exercise:}
We will show that in a Gaussian random variable the parameter $\mu$ is the mean and $\sigma$ the standard deviation. First note

\begin{equation}
E(X) = \int_{-\infty}^{\infty} u \frac{1}{\sqrt{2 \pi \sigma^2} } \exp( -\frac{1}{2} ( \frac{u - \mu}{\sigma})^2) du
\end{equation}
changing to the variable $y = (u- \mu)$
\begin{multline}
E(X) =   \frac{1}{\sqrt{2 \pi \sigma^2} } \int_{-\infty}^{\infty} y  \exp( -\frac{1}{2} ( \frac{y}{\sigma})^2) dy \\ + \mu [ \frac{1}{\sqrt{2 \pi \sigma^2} } \int_{-\infty}^{\infty}   \exp( -\frac{1}{2} ( \frac{y}{\sigma})^2)] = \mu 
\end{multline}
The first term is zero because the integrand is an odd function (i.e., $g(-x) = - g(x)$.  For the variance

\begin{equation}
\Var(X) = \frac{1}{\sqrt{2 \pi \sigma^2} } \int_{-\infty}^{\infty} (u - \mu)^2   \exp( -\frac{1}{2} ( \frac{u - \mu}{\sigma})^2) du
\end{equation}
changing variables to $y = (u - \mu)$
\begin{equation}
\Var(X) = \frac{1}{\sqrt{2 \pi \sigma^2} } \int_{-\infty}^{\infty} y \;  y \exp( -\frac{1}{2} ( \frac{y}{\sigma})^2) dy
\end{equation}
and using integration by parts
\begin{multline}
\Var(X) = \frac{1}{\sqrt{2 \pi \sigma^2} } \Big( -\sigma^2 \Big[ y \exp( - \frac{1}{2} (y/\sigma)^2) \Big]_{- \infty}^{+\infty} \\ + \sigma^2  \int_{-\infty}^{\infty}  \exp( -\frac{1}{2} ( \frac{y}{\sigma})^2) dy \Big)= \sigma^2
\end{multline}

\subsection{Properties of the Variance}

Let $X$ and $Y$ be two random variables and $a$ and $b$ two real numbers then
\begin{equation}
\Var(a X + b) = a^2 \Var(X)
\end{equation}
and
\begin{equation}
\Var(X+Y) = \Var(X) + \Var(Y) + 2 \Cov(X,Y)
\end{equation}
where $\Cov(X,Y)$ is known as the covariance between $X$ and $Y$ and
is defined as
\begin{equation}
\Cov(X,Y) = E[(X - E(X))(Y- E(Y))]
\end{equation}
\paragraph{ Proof:}
\begin{multline}
\Var(aX+b ) = E[(aX +b - E(aX+b))^2] = E[(aX+b - aE(X) -b)^2] \\= E[(a(X -E(X))^2]= a^2 E[(X- E(X))^2] = a^2 \Var(X)
\end{multline}


\paragraph{ Proof:} 

\begin{multline}
\Var(X+Y) = E(X+Y - E(X+Y))^2 = E ((X- \mu_X)+ (Y - \mu_Y))^2\\ =
 E[(X - \mu_X)^2] + E[(Y- \mu_Y)^2] +  2E[(X- \mu_X)(Y - \mu_Y)]
\end{multline}

\begin{flushright} $\square$ \end{flushright}
If $\Cov(X,Y) =0$ we say that the random variables $X$ and $Y$ are {\bf
  uncorrelated}. In such case the variance of the sum of the two
random variables equals the sum of their variances.

It is easy to show that if two random variables are independent they
are also uncorrelated. To see why note that 
\begin{align}
\Cov(X,Y) = E[(X- &\mu_X)(Y - \mu_Y)] = E(XY) - \mu_Y E(X) \\&- \mu_X E(Y)
+ \mu_X \mu_Y = E(XY) - E(X)E(Y)
\end{align}
 we have already seen that if  $X$ and $Y$ are independent then $E(XY) =
E(X) E(Y)$ and thus $X$ and $Y$ are uncorrelated. 


However two random variables may be uncorrelated and still be
dependent. Think of correlation as a linear form of dependency. If two
variables are uncorrelated it means that we cannot use one of the
variables to linearly predict the other variable. However in
uncorrelated variables there may still be non-linear relationships
that make the two variables non-independent.
\paragraph{Example:}
Let $X,Y$ be random variables with the following joint pmf
\begin{equation}
p_{X,Y}(u,v) = \begin{cases}
1/3 &\text{if $u = -1$ and $v= 1$}\\
1/3 &\text{if $u = 0$ and $v= 0$}\\
1/3 &\text{if $u = 1$ and $v= 1$}\\
0 &\text{else}
\end{cases}
\end{equation}
show that $X$ and $Y$ are uncorrelated but are not independent. 
\paragraph{Answer:} Using the fundamental theorem of expected values
we can compute $E(XY)$
\begin{equation}
E(XY) = (1/3)(-1)(1) +(1/3)(0)(0) + (1/3)(1)(1) = 0
\end{equation}
From the joint pmf of $X$ and $Y$ we can
marginalize to obtain the pmf of $X$ and of $Y$
\begin{equation}
p_{X}(u) = \begin{cases}
1/3 &\text{if $u = -1$}\\
1/3 &\text{if $u = 0$}\\
1/3 &\text{if $u = 1$}\\
0 &\text{else}
\end{cases}
\end{equation}
\begin{equation}
p_{Y}(v) = \begin{cases}
1/3 &\text{if $v = 0$}\\
2/3 &\text{if $v = 1$}\\
0 &\text{else}
\end{cases}
\end{equation}
Thus 
\begin{align}
&E(X) = (1/3)(-1)+(1/3)(0)+(1/3)(1) = 0\\
&E(Y) = (1/3)(0) + (2/3)(1) = 2/3\\
&\Cov(X,Y) = E(XY) - E(X)E(Y) = 0 - (0)(2/3) = 0
\end{align}
Thus $X$ and $Y$ are uncorrelated. To show that $X$ and $Y$ are
not independent note that
\begin{equation}
p_{X,Y}(0,0) = 1/3 \neq p_X(0) p_Y(0) = (1/3)(1/3) 
\end{equation}
\section{Appendix: Using Standard Gaussian Tables}

Most statistics books have tables for the cumulative distribution of
Gaussian random variables with mean $\mu=0$ and standard deviation
$\sigma=1$. Such a cumulative distribution is known as the standard
Gaussian cumulative distribution and it is represented with the
capital Greek letter ``Phi'', i.e., $\Phi$. So $\Phi(u)$ is the
probability that a standard Gaussian random variable takes values
smaller or equal to $u$. In many cases we need to know the probability
distribution of a Gaussian random variable $X$ with mean $\mu$
different from zero and variance $\sigma^2$ different from 1. To do so
we can use the following trick

\begin{equation}
F_X(u) = \Phi( \frac{x - \mu}{\sigma})
\end{equation}
For example, the probability that a Gaussian random variable with mean
$\mu=2$ and standard deviation $\sigma=4$ takes values smaller than
$6$ can be obtained as follows

\begin{equation}
P(X \leq 6) = F_X(6) = \Phi(\frac{6 - 2}{4})= \Phi(1)
\end{equation}
If we go to the standard Gaussian tables we see that $\Phi(1) = 0.8413$. 
\paragraph{Proof:}
Let $Z$ be a standard Gaussian random variable, and $X = \mu +
\sigma Z$. Thus,
\begin{equation}
E(X) = \mu+ E(Z) = \mu
\end{equation}
\begin{equation}
\Var(X) = \sigma^2 \Var(Z) = \sigma^2
\end{equation}
Thus, $X$ has the desired mean and variance. Moreover since $X$ is a linear transformation of  $Z$ then $X$ is also Gaussian. Now, using the the properties of cumulative distributions that we saw in a previous chapter
\begin{equation}
F_X(u) = F_Z( (u - \mu)/\sigma) = \Phi( (u - \mu)/\sigma))
\end{equation}
\begin{flushright} $\square$ \end{flushright}

\paragraph{Example:}
Let $Y$ be a Gaussian rav with $\mu_Y = 10$ and $\sigma^2_Y =
100$. Suppose we want to compute $F_Y(-20)$. First we compute $z = (-20
-\mu_Y)/\sigma_Y =-1$. We go to the standard Gaussian tables and find
$F_Z(-1)=0.1587$ Thus $F_Y(20) = F_Z(-1) = 0.1587$.







\section{Exercises}
\begin{enumerate}

\item Let a random variable $X$ represent the numeric outcome of rolling a fair die, i.e., $p_X(u) = 1/6$ for $u \in \{1,2,3,4,5,6\}$. Find the expected value , standard deviation, and average information value of  $X$.

\item Consider the experiment of tossing a fair  coin twice. Let $X_1$ be a random variable taking value 1 if the first toss is heads, and 0 otherwise. Let $X_2$ be a random variable taking value 1 if the second toss is heads, and 0 else. 
\begin{enumerate}
\item Find $E(X_1)$, $E(X_2)$
\item Find $\Var(X_1)$, $\Var(X_2)$
\item Find $\Var(X_1/2)$ and $\Var(X_2/2)$
\item Find $\Var(X_1+X_2)$ and $\Var[(X_1+X_2)/2]$ 
\end{enumerate}

\item Find the information value of a continuous random variable in the interval $[a,b]$. 




\item Show that the variance of an exponential random variable with parameter $\lambda$ is $1/\lambda^2$.


\item Let $X$ be a standard Gaussian random variable. Using Gaussian
  tables  find:
\begin{enumerate}
\item $F_X(-1.0)$
\item $F_X(1.0)$
\item $P( -1.0 \leq X \leq 1.0)$
\item $P(X = 1.0)$
\end{enumerate}

\end{enumerate}














%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.ltx"
%%% End: 
