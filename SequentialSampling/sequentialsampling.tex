\documentclass{article}
\usepackage{nips00e}
\usepackage{kolmogorovmath}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
 \title{Tutorial On Sequential Sampling Methods}
\author{ Javier R. Movellan}








\begin{document}
\maketitle
    
\newpage



\newcommand{\baro}{\bar{o}}
\newcommand{\barh}{\bar{h}}






Let $H =(H_0,H_1,\cdots)$ a stochastic process representing some
(hidden) state dynamics, $O=(O_0,O_1, \cdots)$ represent some
observable dynamics. Let $\bar{H}_t=(H_0, \cdots, H_t)$,
$\bar{O}_t=(O_0,\cdots,O_t)$ for $t=1,2,\cdots$. For a fixed sequence
$x=(x_1, x_2, \cdots)$ we let $\bar{x}_t = (x_1, \cdots, x_t)$. To
simplify the presentation we identify probability density functions by
their arguments. For example the notation $p(o_t\given h_t)$ stands
for $p_{O_t|H_t}(o_t\given h_t)$. Moreover we gloss over differences
between continuous and discrete random variables by accepting` delta
functions as proper probability density functions.  The joint process
$(H,O)$ is assumed to have the following Markovian properties:
\begin{itemize}
\item System dynamics:
\begin{equation}
p(h_t \given \barh_{t-1}, \baro_{t-1}) = p( h_t \given h_{t-1})\; \;
\text{for all $\barh_t \in \R^t, \baro_{t-1} \in \R^{t-1}$},
\end{equation}
\item Observation dynamics:
\begin{equation}
p(o_t \given \barh_t, \baro_{t-1}) = p(o_t \given h_t)\;\;
\text{for all $\barh_t \in \R^t, \baro_{t} \in \R^{t}$}.
\end{equation}
\end{itemize}
\subsection{Forward Recursion Equation}
Suppose we are given an observation sequence $\baro = (o_1,o_2,
\cdots)$. Our goal is to get an estimate of $p(h_t \given \baro_t)$
for $t=0,1,\cdots$. This would allow us to make inferences about the
hidden process based on the observed sequence. First suppose that we
know $p(h_{t-1} \given \baro_{t-1})$, the following recursion equation allows
us to get $p(h_t \given \baro_t)$
\begin{equation}
p(h_t \given \baro_t) = \frac{p(\baro_{t-1})}{p(\baro_t)}p(o_t\given h_t) \int dh_{t-1}\: p(h_{t-1} \given \baro_{t-1}) p(h_{t-1} \given h_t) .
\end{equation}
\paragraph{Proof:}
\begin{align}
&p(h_t \given \baro_t) = \frac{p(h_t, o_t, \baro_{t-1})}{p(\baro_t)} = \frac{p(\baro_{t-1})}{p(\baro_t)} p(h_t, o_t \given \baro_{t-1}) \\
&
=\frac{p(\baro_{t-1})}{p(\baro_t)} \int dh_{t-1}\; p(h_t, h_{t-1}, o_t \given \baro_{t-1}) \\
&
=\frac{p(\baro_{t-1})}{p(\baro_t)} \int dh_{t-1}\; p(h_{t-1} \given \baro_{t-1}) p(h_t  \given \baro_{t-1}, h_{t-1}) p(o_t \given \baro_{t-1},h_{t-1},h_t) \\
&
=\frac{p(\baro_{t-1})}{p(\baro_t)} \int dh_{t-1}\; p(h_{t-1} \given \baro_{t-1}) p(h_t  \given h_{t-1}) p(o_t \given h_t) .
\end{align}
where in the last step we used the Markovian properties of the process.
\begin{flushright} $\square$ \end{flushright}

\subsection{Sequential Sampling}
We will now use the forward recursion equation to devise a sequential
Monte-Carlo sampling scheme that will give us estimates of $p(h_t
\given o_t)$ for all $t$. We represent probability estimates using hats (\^{}).

\paragraph{Initialization:}
We get an estimate of $p(h_0 \given o_0)$ by obtaining $n$
i.i.d. random samples $h^{(1)}_0, \cdots, h^{(n)}_0$ from $p_{H_0}$  and defining
\begin{equation}
\hat{p}(h_0 \given o_0) = \frac{\sum_{i=1}^n  \delta(h_0 - h^{(i)}_0) \; p(o_0 \given h^{(i)}_0 )}{\sum_{j=1}^n p(o_0 \given h^{(j)}_0)} \;\; \text{for all $h_0 \in \R$}.
\end{equation}
Note we are modeling the probability density function $p_{H_0|O_0}$ as
a sum of delta functions (spikes) centered at the $n$
i.i.d. samples. Each spike has strength proportional to the posterior
probability of the observation given the sampled hidden state.

\paragraph{Recursion:}
Assuming we have $\hat{p}_{H_{t-1}|\bar{O}_{t-1}}$ we can get an estimate of $\hat{p}_{H_{t}|\bar{O}_{t}}$ using the forward recursion equation:
\begin{itemize}
\item Get $n$ i.i.d. samples $\tilde{h}^{(1)}_{t-1}, \cdots,
\tilde{h}^{(n)}_{t-1}$ from $\hat{p}_{H_{t-1}|\bar{O}_{t-1}}$.
\item For each $\tilde{h}^{(i)}_{t-1}$ get a sample $h^{(i)}_t$ from $p_{H_t|H_{t-1}}(\cdot \given \tilde{h}^{(i)}_{t-1})$. This results in $n$ samples $h^{(1)}_{t}, \cdots,
h^{(n)}_{t}$ from $\hat{p}_{H_t|\bar{O}_{t-1}}$.
\item The estimate of  $p_{H_t|\bar{O}_t}$ is defined as follows
\begin{equation}\label{eqn:ssampler}
\hat{p}(h_t \given \bar{o}_t) = \frac{\sum_{i=1}^n  \delta(h_t - h^{(i)}_t) \; p(o_t \given h^{(i)}_t )}{\sum_{j=1}^n p(o_t \given h^{(j)}_t)} \;\; \text{for all $h_t \in \R$}.
\end{equation}
\end{itemize}
\paragraph{Notes:}
The sampling scheme requires we weight  delta functions centered at $h_t^{(i)}$  by the the value of $p(o_t \given h^{(i)}_t)$. In practice we just need a number proportional to that value. Let $w(h_t, o_t) = k(o_t) p(o_t \given h_t)$ , where the proportionality constant $k(o_t)$ is independent of $h_t$. Then \eqref{eqn:ssampler} can be modified as follows:
\begin{equation}
\hat{p}(h_t \given \bar{o}_t) = \frac{ \sum_i  \delta(h_t - h^{(i)}_t) \;w( h^{(i)}_t ,o_t)}{\sum_{j=1}^n w(h^{(j)}_t, o_t)} \;\; \text{for all $h_t \in \R$}.
\end{equation}
This is of interest since in some cases it is easier to obtain a model
of $p(h_t \given o_t)$ than a model of $p(o_t \given h_t)$. For
example, neural networks can be trained to provide estimates
of $p(h_t \given o_t)$, i.e., for a given input $o_t$ to the neural network the output can be interpreted as an estimate of the posterior probability of the state given the observation $o_t$. Using Bayes rule we have
\begin{equation}
p(o_t \given h_t) = k(o_t) w(h_t,o_t) ,
\end{equation}
where
\begin{align}
&k(o_t) = p(o_t),\\
&w(h_t,o_t) = p(h_t \given o_t)/ p(h_t).
\end{align}
Here $p(h_t \given o_t)$ is provided by the neural network and
$p(h_t)$ can be interpreted as a model of the prior probability of the states.
\subsection{Importance Sampling}
In the previous sampling scheme the samples $h^{(1)}_t,\cdots,
h^{(n)}_t$ are taken from $\hat{p}_{H_t|\bar{O}_{t-1}}(\cdot\given \baro_{t-1})$. To increase the efficiency of our estimates we may want to sample from another distribution $g_t(\cdot)$  and compensate by multiplying each sample by $\hat{p}_{H_t|\bar{O}_{t-1}}(\cdot\given \baro_{t-1})/g_t(\cdot)$.
In particular let
\begin{equation}
\hat{p}(h_{t-1} \given \baro_{t-1} ) = \sum_{i=1}^n \delta(h_{t-1} - h^{(i)}_{t-1})\: w_{t-1}(h^{(i)}_{t-1}, o_{t-1}).
\end{equation}
Then
\begin{equation}
\hat{p}(h_{t} \given \baro_{t-1} ) = \int dh_{t-1}\: \hat{p}(h_{t} \given \baro_{t-1} ) p(h_t\given h_{t-1}) =  \sum_{i=1}^n  w(h^{(i)}_{t-1}, o_{t-1}) p(h_t \given h^{(i)}_{t-1}).
\end{equation}
Now we sample  $h^{(1)}_t, \cdots , h^{(n)}_t$ from $g_t(\cdot)$ to get
\begin{equation}
\hat{p}(h_{t} \given \baro_t) = \sum_{i=1}^n \delta(h_t - h^{(i)}_t) w_t(h_t^{(i)},o_t)
\end{equation}
where
\begin{equation}
w_{t}(h_t, o_t) = p(o_t \given h_t) \hat{p}(h_t \given \baro_{t-1}) / g_t(h_t).
\end{equation}


\section{History}
\begin{itemize}
\item The first version of this document was written by Javier R. Movellan
in 1996 and used in one of the courses he taught at the Cognitive Science Department at UCSD. 

\item The document was made open source under the GNU Free
Documentation License Version 1.2 on October 9  2003, as part of the
Kolmogorov project.


\end{itemize}


\end{document}



