% Westheimer Model of saccades

% step response of second order system
clear; clf

xi=0.7; %unitless damping ratio
omega=120; % undamped natural frequency rads per sec
%%%%%%%%%
%xi = 0.1185
%omega
% The model takes the form
% a(t) + 2xi* omega v(t) + omega^2 x(t) = u(t)
% where x is position of eye in degrees, v velocity and a
% acceleration.
% u(t) is the driving signal. It is a step function times
% k*omega^2, where k is the final destination of the saccade


dt=1/1000; % time step of simulation
k =[10 20];  % saccade amplitudes in degrees
T=0.1; % simulation time in secs

%%%%%%%%%%%%%%
load saccadeData
t=0:dt:T;
s= length(t);
phi = acos(xi);

% theoretical step response
x1 = k(1)*(1 - exp(-xi*omega*t).*(sin(omega*sqrt(1-xi^2)*t+phi))./ ...
	   sqrt(1-xi^2));
x2 = k(2)*(1 - exp(-xi*omega*t).*(sin(omega*sqrt(1-xi^2)*t+phi))./sqrt(1-xi^2));

v1= diff(x1)/dt;
v1(end+1) = v1(end);
v2=diff(x2)/dt;
v2(end+1) = v2(end);
% now we simulate it using a discrte time approximation
x(1)=0;
v(1)=0;
for i=1:s-1
  a(i) =k(1)*omega^2  - 2*xi*omega*v(i) - omega^2*x(i);
  v(i+1) = v(i) + a(i)*dt;
  x(i+1) = x(i) + v(i)*dt;
end

% some interesting statistics of the response
dampedNaturalFrequency = omega*sqrt(1-xi^2)
resonantFrequency = omega*sqrt(1-2*xi^2)
timePeakOvershoot = 1000*pi/(omega*sqrt(1-xi^2))  
Overshoot= k(1)*(1+ exp(-(pi*xi/sqrt(1-xi^2))))
PercentOvershoot= 100 *exp(-(pi*xi/sqrt(1-xi^2)))
timeOfMaximumVelocity= 1000*atan((1-xi^2)/xi^2)/(omega*sqrt(1-xi^2))


subplot(211)

plot(1000*t,x1,'k-','LineWidth',2)
hold on
plot(1000*t,x2,'k-','LineWidth',2)
plot([0 100],[10,10],'k--','LineWidth',1)
plot([0 100],[20,20],'k--','LineWidth',1)
plot([9.398 9.398], [ 0 600],'--k','LineWidth',1)
plot([36.66 36.66], [ 0 600],'--k','LineWidth',1)
set(gcf,'PaperPositionMode','auto');
set(gcf,'PaperOrientation','landscape');
set(gca,'FontSize',14);
set(gca,'XTick',[0 10 20 30 40 50 60 70 80 90 100]);
set(gca,'YTick',[0 5 10 15 20 25])
xlabel('Time (ms)') 
ylabel('Degrees')
axis([0 80 0 24]);


subplot(212)
set(gcf,'PaperPositionMode','auto');
set(gcf,'PaperOrientation','landscape');
set(gca,'FontSize',14);
set(gca,'XTick',[0 10 20 30 40 50 60 70 80 90 100]);
set(gca,'YTick',[0 100 200 400 600]);
xlabel('Time (ms)') 
ylabel('Velocity (Degrees/sec)')
axis([0 100 -100 600]);
plot(1000*t,v1,'-k','LineWidth',2)
hold on
plot(1000*t,v2,'-k','LineWidth',2)
plot([9.398 9.398], [ 0 1200],'--k','LineWidth',1)
plot([36.66 36.66], [ 0 1200],'--k','LineWidth',1)
plot([0 100], [ 0 0],'--k','LineWidth',1)
xlabel('msecs')
ylabel('velocity (deg/sec)')
axis([0 80 -100 1200]);