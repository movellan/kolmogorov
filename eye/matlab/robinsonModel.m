% this is Robison (1964) 'smodel for the 10 degree saccade. 
% i get the expected shape but the saccade only gives me 6.5
% degrees

clear;clf;
% parameters for 10 degree saccade
p1 = 43.5;
p2=40/1000;
p3=18.9;
p4=3.9;


% parameters for 5 degree saccade
p1 = 32.9;
p2=29/1000;
p3=18.9;
p4=3.9;


e0=0.072;
e2=1.81;
e3=0.025;
k1=3.6;
k2=6.36;
k3=2.06;
m= 0.677/10000;

a1 = [0, 1,0,0];
a2 = [0,0, 1/m,1/m];
a3=[0 -k1, -k1/e0,0];
a4=-[k2*k3, (e2*k3+e3*k2), e2*e3/m, e2*e3/m+  k2+k3]/(e2+e3);
a=[a1;a2;a3;a4];


b=[0 ;0; k1/e0;0];

dt =1/20000;
T=100/1000;
s = ceil(T/dt);

x(:,1)=[0;0;0;0];
for t=1:s
  if t*dt <p2
    u(t) = p1;
  else
    u(t) = p3;
  end
  x(:,t+1) = x(:,t)+(a*x(:,t)+b*u(t))*dt;
end

subplot(211)
set(gcf,'PaperPositionMode','auto');
set(gcf,'PaperOrientation','landscape');
set(gca,'FontSize',14);
set(gca,'XTick',[0  20  40  60  80  ]);
set(gca,'YTick',[0 2 4 6 8 10 12]);
plot(1000*(0:s)*dt,x(1,:),'-k','LineWidth',2)
xlabel('Time (ms)') 
ylabel('Degrees')
axis([0 80  0 12]);

subplot(212)
set(gcf,'PaperPositionMode','auto');
set(gcf,'PaperOrientation','landscape');
set(gca,'FontSize',14);
set(gca,'XTick',[0  20  40  60  80  ]);
set(gca,'YTick',[0 100 200 300 ]);
plot(1000*(0:s)*dt,x(2,:),'-k','LineWidth',2)

xlabel('Time (ms)') 
ylabel('Velocity (Degrees/sec)')
axis([0 80  -50 350]);


