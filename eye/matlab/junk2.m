clear;clf
m =10; %kg
eta=1;
kappa=1;



x(1)=0;
v(1)=0;

dt =1/1000;
T=10;
s =ceil(T/dt);
t = (0:s+1)*dt;
fs= T/2;
f = 1./(1+ exp(-2*(t-fs)));
vf = diff(f)/dt;
af = diff(vf)/dt;
f(end-1:end)=[];
vf(end)=[];
x(1) =0;
vx(1)=0;
x1(1)=0;
vx1(1)=0;
ax1(1)=0;
x2(1)=0;
vx2(1)=0;
ax2(1)=0;

x3(1)=0;
vx3(1)=0;
ax3(1)=0;


for k=1:s
  ax(k) = f(k)/m+ vf(k)/eta+ af(k)/kappa;
  vx(k+1) = vx(k) + ax(k)*dt;
  x(k+1) = x(k)+vx(k)*dt;
  ax1(k) = f(k)/m;
  vx1(k+1) = vx1(k)+ ax1(k)*dt;
  x1(k+1) = x1(k)+vx1(k)*dt;
  x3(k) = f(k)/kappa;
  
end

plot([0:s-1]*dt,f)