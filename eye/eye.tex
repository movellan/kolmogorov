\documentclass{article}    \usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}

 \title{Eye Movement Models}
\author{Copyright \copyright Javier R. Movellan}

\newenvironment{frcseries}{\fontfamily{frc}\selectfont}{}
\newcommand{\mathfrc}[1]{{\frcseries#1}}

\newcommand{\vc}{\text{Vec}}
\newcommand{\vct}{\mathcal{T}}
\newcommand{\Rl}{\mathcal{R}}


\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}
\newcommand\vx{\nabla_x v(x,t)}
\newcommand\vt{\dot v(x,t)}
\newcommand\vxx{\nabla_x^2 v(x,t)}
\newcommand\tr{\text{Tr}}



\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}

\newtheorem{prop}{Proposition}[section]
\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{lem}{Lemma}[section]
\theoremstyle{definition}
\newtheorem{rem}{Remark}[section]
\theoremstyle{definition}
\newtheorem{ex}{Example}[section]






\begin{document}
\maketitle


Please cite as

{ 
Movellan J. R. (2011) {\em Eye Movement Models} MPLab Tutorials, University of California San Diego
}

    
\newpage
\section{History of the Literature}
\begin{itemize}
\item Descartes (1664) principle of reciprocal innervation in eye model. Muscles thought of as ``air muscle'' balloon-like mechanism.

\item Dodge (1901, 1903,1907). Pioneer in the systematic study and classification of eye movements. 

\item Westheimer (1954). First mathematical model. Second order linear
  model. Models agonistic and antagonistic pair as single force
  generator. Input signal is a step function. Works reasonably well
  for 20 degree saccades.

\item Robinson (1964). 4 state linear model. agonistic and antagonistic pairs model as single force generator. Input signal is a short pulse followed by a sustained signal. Describes the trajectories of saccades 5 to 40 degree but the velocity profiles show significant deviations from empirical data. 

\item Cook \& Stark (1966,1967). Use non-linear muscle model. Model
  agonistic and antagonistic muscles independently. Matches saccade
  velocities well.

\item Clark \& Stark (1975) show that the saccade trajectories are
  time optimal with respect to the Cook \& Stark eye model.

\item Clark \&Stark 1974
\item Hsu Bahil and Stark (1976) popular non-linear reciprocal innervation model.

\item Bahill 1980. 4th order linear model 


\end{itemize}
\section{Mathematical Background}
You need the tutorial on Linear Systems and the chapter on visco-elastic models from the Physics for Robotics and Animation tutorial.

\section{Westheimer 1954 model}
 Westheimer recorded saccade movements of 2, 4,6,8,10,12,14,16,18,20,
 25, 30 degrees.

Westheimer models the saccade system as a second order linear system
\begin{align}
a_2 \ddot \theta_t + a_1 \dot \theta_t + a_0\theta_t  = \tau_t
\end{align}
where $a_2>0$ is the moment of inertia, $a_1>0$ is the viscosity coefficient, $a_0>0$ is the elasticity coefficient $\tau_t$ is the torque. 
Westheimer uses the following standard parameterization of second order systems
\begin{align}
\ddot \theta_t + 2 \xi\omega  \dot \theta + \omega^2 \theta = \omega^2 \tau_t
\end{align}
where
\begin{align}
&\omega = \sqrt{c/a}\\
&\xi = \frac{2}{\sqrt{ca}}
\end{align}
$\omega$ represents the undamped natural frequency, and $\xi$ is
called the damping ratio and is dimensionless.  Note the model is
underdamped $(\xi<1)$. The driving signal $\tau_t$ is assumed to be a
step function (0 before the initiation of the saccade and a constant
$k \omega^2$ until the end of the saccade. Thus the saccade trajectory
is the step response of the system. It is well known that with zero
initial conditions $x_0= \dot x_0 =0$ the step response to a second
order system to a step signal 
\begin{align}
\tau_t = \begin{cases}
0&\text{for $t<0$}\\ 
k&\text{for $t\geq 0$}
\end{cases}
\end{align}
is as follows
\begin{align}
x_t = k\Big( 1 - \frac{e^{-\xi\omega t}}{\sqrt{1-\xi^2}} \sin(\omega\sqrt{1-\xi^2} t +\phi)\Big)
\end{align}
Westheimer reported the best fitting parameters to be 
\begin{align}
&\omega  = 120\;\; radians/sec\\
&\xi =   0.7 (dimensionless)
\end{align}
Fig~\ref{fig:wes} shows the predicted trajectories for a 10 degree and a 20 degree saccades. 
\begin{figure}[h] \label{fig:wes}
\begin{center}
\includegraphics[width=.8\textwidth]{Media/Westheimer.pdf}
\end{center}
\caption{\it Westheimer (1954) simulation of 10 and 20 degree saccades. Vertical lines show the time of maximum velocity and the time of first overshoot}
\end{figure}
The following statistics for the step response of second order systems are useful and well known 
\begin{itemize}
\item Damped Natural Frequency: $ \omega \sqrt{1-\xi^2}= 85.7$ rads/sec $= 13.64$ Hz. 
\item Resonant Frequency: $\omega \sqrt{1-2\xi^2} = 19.97$ rads/sec $= 2.7$ Hz
\item Time of Maximum Velocity: $\tan^{-1}(\frac{ 1-\xi^2}{\xi^2} )/ (\omega \sqrt{1-\xi^2})=9.4$ msecs
\item Time of Peak Overshoot: $\pi/(\omega\sqrt{1-\xi^2})= 36.66$ msecs 
\item Percent Overshoot:$100 \exp( -\pi \xi/\sqrt{1-\xi^2})=  4.6 \%$ 
\end{itemize}
Westheimer model predicts: (1) The time of maximum velocity is
constant $9.4$ msecs and independent of saccade magnitude. We know
that's not the case. (2) By evaluating the derivative of $x_t$ at the
time for peak velocity, it is easy to see that the peak velocity is
proportional to the magnitude of the saccade. We know that that's not
the case in natural saccades. (3) The time to the first overshoot can
be taken as a measure of the duration of the saccade. In Westheimer
model is a constant (36.66 msecs) independent of the magnitude of the
saccade. We know in natural saccades the duration of the saccade
increases with its magnitude. Finally the driving signal in Westheimer
model is a step function. We know now that in natural saccades the
driving signal is the sum of a pulse and a step. Thus, while
Westheimer model provides a reasonable model for 10 degrees saccades,
it does not do well with a wide range of saccade magnitudes. One way
to fix this  problem is to use different parameters for each
saccade amplitude, but then the model would lose its physical
interpretation.  Another way is to use different driving
signals. According to J. D. Enderle (models of Horizontal Eye
Movements, Part1. Morgan \&Claypool publishers, 2010) it turns out
that the Westheimer model it a reasonable model for saccades of all
sizes if a pulse step function rather than a step function is used and
if the pulse step function can change depending on the saccade, like
in Robinson's 1954 model.
\section{Robinson (1964) model}
 Robinson proposed a model to fit saccade ranging from 5 to 40
 degrees. He combined the agonistic and antagonistic muscle into a
 single pulling mechanism.  As the eye rotates by $\theta$ degrees,
 the contracting mechanism moves to $v$. We let $x$ represent the
 contraction involved in the rotation (see Figure~\ref{fig:rob} Top). Thus
\begin{align}
&v= r \left(\begin{array}{c}\sin(\theta)\\\cos (\theta)\end{array}\right)
-  \left(\begin{array}{c}l \\r\end{array}\right)\\
&x = l - |v|   = l - \sqrt{(r \sin(\theta) - l)^2 +(r \cos(\theta) - r)^2} 
\end{align}
Note
\begin{align}
&\nabla_\theta x = -\frac{ 2(r\sin(\theta) -l)r\cos(\theta) + 2(r\cos(\theta) - r)(-r \sin(\theta))}{2\sqrt{(r \sin(\theta) - l)^2 +(r \cos(\theta) - r)^2}  }\\
\end{align}
Robinson uses a first order Taylor expansion  about $\theta=0$, i.e.
\begin{align}
x \approx r \theta
\end{align}
\begin{figure}[h] \label{fig:rob}
\begin{center}
\includegraphics[width=.5\textwidth]{Media/EyeMechanics.pdf}
\includegraphics[width=.7\textwidth]{Media/Robinson.pdf}
\end{center}
\caption{\it {\bf Top:} Mechanics of the eye rotation. As the eye
  rotates, the agonistic muscle moves to $v$. {\bf Bottom:} Robinson's
  model. }\label{fig:rob}
\end{figure}
 We adopt the convention that a positive force is a contracting
 force. Under the model the total force exerted on the eye is the sum
 of the active forces $f_a$ and the the passive forces $f_p$ produced
 by the muscles and surrounding tissue. The parameters for the
 spring-like elements of the model are $\kappa_1, \kappa_2,
 \kappa-3$. The parameters of the viscous elements are
 $\eta_0,\eta_2,\eta_3$. The contractions of the corresponding
 elements are $x_0,x_2,x_3$. We let $x$ represent the total
 contraction, i.e.,
\begin{align}
x_t = x_{0,t} + x_{1,t} = x_{2,t} + x_{3,t}
\end{align}
 Newton's law relates the acceleration of the eye to the forces
 applied to it, i.e.
\begin{align}
m \ddot x_t= f_{a,t} + f_{p,t}
\end{align}
where $m$ is the moment of inertia, $x_t$ the eye rotation in degrees,
and $f_a$, $f_t$ are active and passive muscle forces. The active force
is based on Hill's (1938) muscle model.  It is known that for a given
neural signal, the muscle produces a force that decreases with the
velocity of contraction. This is modeled by putting a contracting
source force $f_{c,t}$ which depends on the neural signal, in parallel
with a viscous dashpot.  The force produced by the dashpot is $-\eta_0
\dot x_{0,t}$. Thus the total active force is
\begin{align}
f_{a,t} = f_{c,t} - \eta_0 \dot x_{0,t}
\end{align}
The active muscle force is also modulated by a series elastic element
between the muscle and the eye. This element produces a force $f_{a,t}
= - \kappa_1 x_{1,t}$, i.e., it opposes contraction. Moreover $x_t =
x_{0,t} + x_{1,t}$. Note
\begin{align}
x_t &= x_{0,t} + x_{1,t}\\
\dot x_t &= \dot x_{0,t} + \dot x_{1,t} \nonumber\\
\dot x_t & = \frac{1}{\eta_0} (f_{c,t} - f_{a,t})  - \frac{1}{\kappa_1} \dot f_{a,t}   
\end{align}
In addition to the active force $f_a$ there are passive viscoelastic
forces $f_p$ produced by the agonistic and antagonistic muscle
tissue. These forces are modeled as two Voight elements in series,
corresponding to fast and slow viscoelastic tissue forces. A Voight element is a spring in parallel with a dashpot. Thus
\begin{align}
&f_{p,t} = - \kappa_2 x_{2,t} - \eta_2 \dot x_{2,t}\\
&f_{p,t} = -\kappa_3 x_{2,t} - \eta_3 \dot x_{2,t}\\
\end{align}
and
\begin{align}
\dot x_{2,t} &= -\frac{1}{\eta_2} f_{p,t} - \frac{\kappa_2}{\eta_2} x_{2,t}\\
\dot x_{3,t} &= -\frac{1}{\eta_3} f_{p,t} - \frac{\kappa_3}{\eta_3} x_{3,t}\\
\dot x_t&= \dot x_{2,t} + \dot x_{3,t} = -(\frac{1}{\eta_2} + \frac{1}{\eta_3}) f_{p,t} - (\frac{\kappa_2}{\eta_2} x_{2,t} + \frac{\kappa_3}{\eta_3} x_{3,t})\\
\eta_2\eta_3 \dot x_t &= - (\eta_2 +\eta_3)f_{p,t} - \kappa_2\eta_3 x_{2,t} - \kappa_3 \eta_2 x_{3,t}\nonumber\\
&=  -(\eta_2 +\eta_3)f_{p,t} - (\kappa_2\eta_3 + \kappa_3 \eta_2) x_t
+ \kappa_2 \eta_3 x_{3,t} + \kappa_3 \eta_2 x_{2,t}
\end{align}
Differentiating the last equation
\begin{align}
\eta_2 \eta_3 \ddot x_t = -(\eta_2 + \eta_3) \dot f_{p,t} - (\kappa_2 \eta_3 + \kappa_3 \eta_2) \dot x_t -(\kappa_2+\kappa_3) f_{p,t} -\kappa_2\kappa_3 x_t
\end{align}

\begin{figure}[t] \label{fig:saccadesignal}
\begin{center}
\includegraphics[width=.4\textwidth]{Media/SaccadeSignal.pdf}
\end{center}
\caption{\it The shape of the active force signal $f_a$ during a saccade.}
\end{figure}
Summarizing, the equations of motion are as follows
\begin{align}
& m \ddot x_t= f_{a,t} + f_{p,t}\\
&\dot x_t  = \frac{1}{\eta_0} (f_{c,t} - f_{a,t})  - \frac{1}{\kappa_1} \dot f_{a,t}   \\
&\kappa_2 \kappa_3 x_t + (\eta_2 \kappa_3 + \eta_3 \kappa_2) \dot x_t 
+ \eta_2 \eta_3 \ddot x_t = -(\kappa_2+ \kappa_3) f_{p,t} - (\eta_2+\eta_3)\dot f_{p,t}
\end{align}
Rearranging terms 
\begin{align}
&\ddot x_t = \frac{f_{a,t} + f_{p,t}}{m}\\
&\dot f_{a,t} = \frac{\kappa_1}{\eta_0} (f_{c,t} - f_{a,t}) - \kappa_1 \dot x_t\\
&\dot f_{p,t} = -\frac{\kappa_2 \kappa_3}{\eta_2 + \eta_3} x_t \nonumber\\
&- \frac{\eta_2 \kappa_3 + \eta_3 \kappa_2}{\eta_2 + \eta_3} \dot x_t\nonumber\\
&- \frac{\eta_2 \eta_3/m}{\eta_2 + \eta_3} f_{a,t} \nonumber\\
&- \frac{\eta_2 \eta_3/m +  \kappa_2 +  \kappa_3 }{\eta_2 + \eta_3} f_{p,t} 
\end{align}
In matrix form
\begin{align}
\frac{d}{dt}\left[ \begin{array}{c} x_t\\ \dot x_t\\f_{a,t}\\ f_{p,t}\end{array}\right]
=&
\left[ \begin{array}{cccc}
0 &1&0&0\\
0&0&1/m&1/m\\
0&-\kappa_1&  -\kappa_1/\eta_0&0\\
-\frac{\kappa_2 \kappa_3}{\eta_2 + \eta_3} 
&-\frac{\eta_2 \kappa_3+ \eta_3 \kappa_2}{\eta_2 + \eta_3} 
&-\frac{\eta_2 \eta_3/m}{\eta_2 + \eta_3}
&-\frac{\eta_2 \eta_3/m + \kappa_2+ \kappa_3}{\eta_2 + \eta_3}  
\end{array}
\right]
\left[ \begin{array}{c} x_t\\ \dot x_t\\f_{a,t}\\ f_{p,t}\end{array}\right]\nonumber\\
&+ \left[ \begin{array}{c} 0\\0\\ \kappa_1/\eta_0 \\0\end{array}\right]
f_{c,t}
\end{align}
The parameter values for Robinson's (1954) model are as follows
\begin{itemize}
\item $\eta_0$ 0.072 $g \cdot sec /deg$
\item $\eta_2$ 1.81  $g \cdot sec /deg$
\item $\eta_3$ 0.025 $g \cdot sec /deg$
\item $\kappa_1$ 3.6 $g/deg$
\item $\kappa_2$ 6.36 $g/deg$
\item  $\kappa_3$  2.06 $g/deg$
\item $m$ 0.677 $\cdot 10^{-4}$ $g \cdot sec^2/deg$
\end{itemize}
The $f_c$ signal has three parameters (see
Figure~\ref{fig:saccadesignal}). $a$ is the initial force. $b$ is the
time the initial force is sustained (a pulse-step function). Both $a$
and $b$ change with the magnitude of the saccade (see
Table~\ref{tab:saccadesignal}).  $c$ is the force after the initial
burst. $d$ is the final asymptote. Robinson did not specify the slope
for the decay of the ``step'' function from 18.9 down to 3.9, but in a
graph in his paper, it seems to happen in the order of 300 msecs.
\begin{figure}[h] \label{fig:robs}
\begin{center}
\includegraphics[width=.8\textwidth]{Media/RobinsonSaccade.pdf}
\end{center}
\caption{\it Robinson (1964) simulation of 10 degree saccade. }
\end{figure}

\begin{table} \label{tab:saccadesignal}
\begin{center}
\begin{tabular}{c|cccccccc}
saccade magnitude (deg) &5 &10 &15 &20 &25 &30 &35 &40\\\hline
a (grams)&32.9 &43.5 &44.9 &44.1 &44.1 &41.9 &41.9 &40.9 \\
b (msecs)&29 &40 &55 &65 &74 &87 &95 &100\\
c (grams)&18.9 &18.9 &18.9 &18.9 &18.9 &18.9 &18.9 &18.9\\
d (grams)&3.9 &3.9&3.9&3.9&3.9&3.9&3.9&3.9
\end{tabular}
\end{center}
\end{table}
Figure~\ref{fig:robs} shows the location and velocity profile predicted by Robinson's model for a 10 degree saccade. The main problem of the model is the velocity profile, which has a strong discontinuity that does not appear in natural saccades. \mynote{I have only been able to replicate the 10 degree saccade with Robinson's model.}

\section{Bahill 1980 Model}

\begin{figure}[h] \label{fig:bahilmus}
\begin{center}
\includegraphics[width=.5\textwidth]{Media/Bahill.pdf}
\end{center}
\caption{\it Bahill (1980) muscle model.}
\end{figure}
\paragraph{Muscle Model}
Figure~\ref{fig:bahilmus} displays Bahill's muscle model. It consists
of a component made of an active contraction source, which is
controlled by the neural input, in parallel with a spring and a
dashpot. This is intended to model the fact that for a given level of
muscle activity the resulting source decreases with muscle length and
with muscle velocity. Lhe lenght-force relationship is typically
studied in isometric experiments in which tension is measured for a
muscle at different fixed lenghts. The force velocity relationship is
described by the Hill equation
\begin{align}
f_t= f_{o,t} - \frac{f_{0,t} + \alpha }{\beta + \dot x_t} \dot x_t
\end{align}
where $f_o$ is the muscle force at zero velocity. We can think of this
as the equation of a non-linear dashpot, whose viscosity coefficient
is a functin of $f_0 and \dot x$. The resulting force depends on the
level of muscle activation. $\dot x_t$ is the velocity of muscle
contraction, and $\alpha,\beta$ are fixed parameters. Different
parameters are used if the muscle is contracting or extending. Bahill
found that this relationship could be modeled well using a linear
function.  In Bahill's muscle model the input is a force $f_c$ caused
by the neural activation. The output is a contraction $x$ and a force
$f$. Note

\begin{align}\label{eqn:bahil1}
&x_t = x_{0,t} + x_{1,t}
\end{align}
Regarding the forces we get the following relationships
\begin{align}
&f_{t} = - \kappa_1 x_{1,t}\\
&f_{t} = f_{c,t} - \eta_0  \dot x_{0,t} - \kappa_0 x_{0,t} \\
\end{align}
Differentiating \eqref{eqn:bahil1}
\begin{align}
\dot x_t = \dot x_{0,t} + \dot x_{1,t}
\end{align}
where
\begin{align}
\dot x_{0,t} &= \frac{1}{\eta_0}\Big( f_{c,t} - f_t -\kappa_0 x_{0,t}\Big)\nonumber\\
&= \frac{1}{\eta_0}\Big( f_{c,t} - f_t -\kappa_0 (x_t - x_{1,t}) \Big)\nonumber\\
&= \frac{1}{\eta_0}\Big( f_{c,t} - f_t -\kappa_0 x_t - \frac{\kappa_0}{\kappa_1} f_t \Big)\nonumber\\
&= \frac{1}{\eta_0}\Big( f_{c,t} - f_t \frac{\kappa_1+\kappa_0}{\kappa_1} -\kappa_0 x_t  \Big)
\end{align}
and
\begin{align}
\dot x_{1,t} = - \frac{\dot f_t}{\kappa_1}
\end{align}
Thus 
\begin{align}
\dot x_t = \frac{1}{\eta_0}\Big( f_{c,t} - f_t \frac{\kappa_1+\kappa_0}{\kappa_1} -\kappa_0 x_t  \Big) - \frac{\dot f_t}{\kappa_1}
\end{align}
Equivalently
\begin{align}
\dot f_t  = \frac{\kappa_1}{\eta_0} f_{c,t} - \frac{\kappa_0 +\kappa_1}{\eta_0} f_t   -\frac{\kappa_0 \kappa_1}{\eta_0}  x_t -\kappa_1 \dot x_t 
\end{align}
It is interesting to note that at equilibrium ( $\dot f_t =0$)
\begin{align}
f_t= \frac{\kappa_1}{\kappa_0+\kappa_1} \Big( f_{c,t} - \kappa_0 x_t -\eta_0 \dot x_t\Big)
\end{align}
\paragraph{Oculomotor Model}
Figure~\ref{fig:bahileye} shows the schematic of Bahill's model of the
oculomotor system. First we use Newton's law of motion for rotational
coordinates
\begin{align}
m \ddot \theta_t = \tau_{1,t} +\tau_{0,t} - \rho \theta_t - \nu \dot \theta_t 
\end{align}
where $m$ is the eye's moment of inertia, $\tau_1,\tau_0$ are the
torques produced by the agonistic and antagonistic muscles, $\rho
\theta$ is a passive elastic rotational torque and $\nu \dot \theta$
is a passive viscous force. The force $g_{1}$ produced by the
agonistic muscle is converted into a force at the connection between
tendon and eyeball. This force is tangential to the eyeball, thus the
resulting torque $\tau_{1}$ is as follows 
\begin{align}
\tau_{1,t} =  r g_{1,t} 
\end{align}
Similarly the contracting force $g_0$ from the antagonistic muscle
results on the corresponding torque $\tau_0$
\begin{align}
\tau_{0,t} = - r g_{0,t} 
\end{align}
Thus Newton's law becomes as follows 
\begin{align}
m \ddot \theta_t = r (g_{1,t} - g_{0,t}) - \nu \dot \theta_t - \rho \theta_t
\end{align}
\paragraph{Agonistic and antagonistic Forces}
\begin{figure}[h] \label{fig:bahileye}
\begin{center}
\includegraphics[width=.9\textwidth]{Media/BahillEye.pdf}
\end{center}
\caption{\it Bahill (1980)  model of the oculomotor system.}
\end{figure}
Using the Bahill muscle equations for the agonistic muscle 
\begin{align}
\dot g_{1,t}  = \frac{\xi}{\eta_1} f_{1,t} - \frac{\kappa +\xi}{\eta_1} g_{1,t}   -\frac{\kappa \xi}{\eta_1}  x_{1,t} -\xi \dot x_{1,t} 
\end{align}
and for the antagonistic muscle
\begin{align}
\dot g_{0,t}  = \frac{\xi}{\eta_0} f_{0,t} - \frac{\kappa +\xi}{\eta_0} g_{0,t}   -\frac{\kappa \xi}{\eta_1}  x_{0,t} -\xi \dot x_{0,t} 
\end{align}
Note the lenght of the muscle tendon when $\theta=0$ is as follows
\begin{align}
l = l_0 + \frac{\alpha_2 - \alpha_1 -\theta}{2\pi}  2\pi r = l_0+ (\alpha_2- \alpha_1 -\theta) r
\end{align}
where $l_0$ is the distance between the point where the muscle/tendon
first contacts the eyeball, $\alpha_2$ is the angle (in radians) of
that first contact point, and $\alpha_1+\theta$ the angle of the
connection between the eyeball and the tendon. Sionce $x_{1}$ is the contraction with respect to the rest point $\theta=0$, then
\begin{align}
x_{1,t} = \theta_t r
\end{align}
Equivalently, for the contraction of the antagonistic muscle is as follows
\begin{align}
x_{0,t}= - \theta_t r
\end{align}
Bringing it all together
\begin{align}
\ddot \theta &= \frac{r}{m} g_{1,t} - \frac{r}{m} g_{0,t} -\frac{\nu}{m} \dot \theta_t -\frac{\rho}{m}\theta_t\\
\dot g_{1,t} & = \frac{\xi}{\eta_1} f_{1,t} - \frac{\kappa +\xi}{\eta_1} g_{1,t}   -\frac{\kappa \xi}{\eta_1}  r \theta_{t} -\xi r \dot \theta_{t} \\
\dot g_{0,t}  &= \frac{\xi}{\eta_0} f_{0,t} - \frac{\kappa +\xi}{\eta_0} g_{0,t}  +\frac{\kappa \xi}{\eta_0}  r \theta_t  + \xi r  \dot \theta_t 
\end{align}
In matrix form
\begin{align}
\frac{d}{dt}\left[ \begin{array}{c} \theta_t\\ \dot \theta_t\\g_{1,t}\\ g_{0,t}\end{array}\right]
=&
\left[ \begin{array}{cccc}
0 &1&0&0\\
-\frac{\rho}{m}&-\frac{\nu}{m}&\frac{r}{m}&-\frac{r}{m}\\
-\frac{r\kappa\xi}{\eta_1} 
&-\xi r
&-\frac{\kappa+\xi}{\eta_1}
&0\\
\frac{r\kappa\xi}{\eta_0} 
&\xi r
&0
&-\frac{\kappa+\xi}{\eta_0}
\end{array}
\right]
\left[ \begin{array}{c} \theta_t\\ \dot \theta_t\\g_{1,t}\\ g_{0,t}\end{array}\right]\nonumber\\
&+ \left[ \begin{array}{cccc}
0&0&\frac{\xi}{\eta_1}&0\\
0&0&0&\frac{\xi}{\eta_0}
\end{array}\right]
\left[ \begin{array}{c}f_{1,t}\\f_{0,t}\end{array}\right]
\end{align}

\paragraph{Equilibrium solution}
To get the equilibrium solution we let $\dot \theta_t=\ddot \theta_t =
\dot g_{1,t} = \dot g_{0,t}=0$. Thus
\begin{align}
&g_{1,t} - g_{0,t}=0\\
&g_{1,t} = \frac{\xi}{\kappa+\xi} f_{1,t} -\frac{\kappa \xi}{\kappa+\xi} r \theta\\
&g_{0,t} = \frac{\xi}{\kappa+\xi} f_{0,t} +\frac{\kappa \xi}{\kappa+\xi} r \theta\\
\end{align}
Thus, at equilibrium
\begin{align}
\frac{\xi}{\kappa+\xi}( f_{1,t} - f_{0,t}) = 2\frac{\kappa \xi}{\kappa+\xi} r \theta
\end{align}
and
\begin{align}
\theta_t = \frac{f_{1,t} - f_{0,t}}{r \kappa}
\end{align}
\paragraph{Activation and Deactivation}
The relationship between neural signals and resulting active muscle tensions is modeled using low pass filters with differnt time constants for the activation $\tau_1$ (raising) and deactivation $\tau_2$.
\paragraph{Model Parameters}
\begin{itemize}
\item $r = 11 \times 10^{-3}$ $meters$
\item  $m = 2.662 \times 10^{-7}$ $Newtons/(rads/sec^2)$
\item  $\xi = 125 $ $Newton/meter$.
\item $\kappa = 60$ $ Newton/meter$ (Bahill ) $\kappa = 32$ $Newton/meter$ (Enderle 84).
\item $\eta_1 = 2.36$ $Newton/(meter/sec)$ (Bahill)  $\eta_1 = 3.5$ $Newton/(meter/sec)$ (Enderle 84)  
\item $\eta_0 = 1.12 $ $Newton/(meter/sec)$ (Bahill)  $\eta_1 = 1.2$ $Newton/(meter/sec)$ (Enderle 84)  
\item $\rho = 0.275$ $Newton/radian$ (Bahill ), $\rho = 0.7304$ $Newton/radian$ (Enderle 84) 
 \item $\nu = 0.0341$ $Newton/(radian/sec)$ 
\item $\tau_1 = 0.009$ $secs$. same for agonistic and antagonistic muscles
\item $\tau_2 = 0.0054$ $secs$. same for agonistic and antagonistic muscles.
\item 
\begin{align}
F_1 = \begin{cases} 0.14+ 0.0185\; \theta \;\; Newtons &\text{for $\theta < 14.23$ degrees}\\
0.002830\; \theta \;\;Newtons &\text{for $\theta \geq 14.23$ degrees}
\end{cases}
\end{align}
\item 
\begin{align}
F_0 = \begin{cases} 0.14-  0.0098 \;\theta \;\; Newtons &\text{for $\theta < 14.23$ degrees}\\
0  \;\;Newtons &\text{for $\theta \geq 14.23$ degrees}
\end{cases}
\end{align}
\end{itemize}
\section{Properties of the OculoMotor System}
\begin{itemize}
\item Moment of inertia of solid sphere: $\frac{ 2 m r^2}{5}$
\item Volume of sphere: $4 \pi r^3/3$
\item Diameter of adult human eye: 25 $\pm$ 2 mm.
\item Mass of adult human eye: 7.5 $\pm$ 1 gm. 
\item Density of water at 4 degrees centigrade: 1000 Kg per cubic meter. 
\item The moment of inertia of an 11 mm sphere of water at 4 degrees centigrade is as follows 
\begin{align}
m = \frac{2}{5} \frac{4}{3} \pi (0.011)^3 1000 (0.011)^2 = 2.6984 \times 10^{-7}\;Newtons\; meter^2 
\end{align}
In practice the vitrious body is in greater part left behind during a saccade to the effective moment of inertia is less than this.  Bahill's model uses  a moment of inertia of $2.662 \times 10^{-7}$ Newtons meter^2.

\item Momen of interia of head 0.0148 $Kg m^2$ (Peng et all 1996) 
\end{itemize}




\bibliographystyle{abbrvnat}



\end{document}

