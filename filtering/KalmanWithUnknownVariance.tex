\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}

 \title{A Simple Kalman Filter Modification To Provide Adaptive Variance Estimates}
\author{Copyright 2011 \copyright Javier R. Movellan}




\newcommand{\ve}[1]{\text{vec}[#1]}
\newcommand\given{\medspace|\medspace}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\R}{\mathcal{R}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}





\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}



\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{lem}{Lemma}[section]
\theoremstyle{definition}
\newtheorem{rem}{Remark}[section]
\theoremstyle{definition}
\newtheorem{ex}{Example}[section]




\usepackage{verbatim}

\begin{document}
\maketitle In the standard Kalman filter the uncertainty about the
state is a function of time, i.e., it does not change with the
observations. This is basically due to the fact that in the standard
filter the amount of noise in the system dynamics and observations are
assumed to be known. Here we propose a simple approach that can
automatically detect changes in the amount of noise in the world, and
adapt its beliefs accordingly. Let's consider a Kalman filter solution
to a problem of the form

\begin{align}
&X_{t+1} = a X_t +  \sqrt{G_{t+1}} Z_{t+1} \\
&Y_{t+1} = b X_{t+1} +  \sqrt{H_{t+1}}  W_{t+1}\\
&\log G_{t+1} = \log G_t + U_{t+1}\\
&\log H_{t+1} = \log H_t + V_{t+1}\\
\end{align}
where $Z_t,W_t,U_t,V_t$ are iid Gaussian random vectors with zero mean
and covariance $\sigma_z, \sigma_w,\sigma_u,\sigma_v $, and
$G_t,H_t\in \R$ are random variables controlling the overall
volatility in the system dynamics and observation dynamics. .

Given $p(x_{t} \given y_{1:t}, \hat g_{1:t},\hat h_{1:'t})$ and given $y_{t+1}$ our
goal is to find find
\begin{align}
(\hat g_{t+1}, \hat h_{t+1})  = \argmax_{g_{t+1}, h_{t+1}} p(g_{t+1}, h_{t+1} \given y_{1:t+1}, \hat g_{1:t},\hat h_{1:t})
\end{align}  
and
\begin{align}
p(x_{t+1} \given y_{1:t+1}, \hat g_{1:t+1},\hat h_{1:t+1})
\end{align}
Regarding the first problem note
\begin{align}
 p(g_{t+1},h_{t+1}& \given y_{1:t+1}, \hat g_{1:t},\hat h_{1:t}) \propto  p(g_{t+1} \given \hat g_t) p(h_{t+1} \given \hat h_{t})\; p(y_{t+1} \given y_{1:t}, \hat g_{1:t} ,\hat h_{1:t}) \\
&= p(g_{t+1}\given \hat g_t) p(h_{t+1}\given \hat h_t)\;\phi(y_{t+1} \given m_{t+1},s_{t+1})
\end{align}
where
\begin{align}
&p(g_{t+1}\given \hat g_t) = \frac{1}{g_{t+1}  \sqrt{2 \pi \sigma_v}} e^{- \frac{1}{2 \sigma_u} (\log \frac{g_{t+1}}{\hat g_t} )^2 }\\
&p(h_{t+1}\given \hat h_t) = \frac{1}{h_{t+1}  \sqrt{2 \pi \sigma_v}} e^{- \frac{1}{2 \sigma_b} (\log \frac{h_{t+1}}{\hat h_t} )^2 }
\end{align}
\begin{align}
&m_{t+1} = a b \mu_t^t\\
&s_{t+1}=  b a \sigma_t^t a' b' + g_{t+1} b   \sigma_z b' + h_{t+1} \sigma_w \\
&\mu_t^t = E[X_t \given y_{1:t}, \hat g_{1:t},\hat h_{1:t}]\\
&\sigma_t^t = Var[X_t \given y_{1:t}, \hat g_{1:t},\hat h_{1:t}]
\end{align}
 We note that $g_{t+1}, h_{t+1}$ affect the probability of $y_{t+1}$
 only through the variance term $s_{t+1}$, not the mean $m_{t+1}$.  If
 $p(g_{t+1},h_{t+1} \given g_t,h_t)$ has very low variance, it means
 that we can only search for values of $(g_{t+1},h_{t+1})$ that are
 very close to $(g_{t},h_{t})$ while maximizing the likelihood of the
 data $\phi(y_{t+1}\given m_{t+1}, s_{t+1})$, i.e., we want to move in
 the direction of the gradient of the likelihood (or gradient of the
 log likelihood). From the Apendix we get that 


\begin{align}
&\frac{d \log \phi(y_{t+1} \given m_{t+1},s_{t+1})}{d g_{t+1}}  = \frac{1}{2}\Big(  (y_{t+1} - m_{t+1})' s_{t+1}^{-1} b  \sigma_z  b' s_{t+1}^{-1} (y_{t+1} - m_{t+1}) - \text{trace}[s_{t+1}^{-1}b  \sigma_z b] \Big) \\
&\frac{d \log \phi(y_{t+1} \given m_{t+1},s_{t+1})}{d h_{t+1}}  = \frac{1}{2}\Big( (y_{t+1} - m_{t+1})' s_{t+1}^{-1}  \sigma_w  s_{t+1}^{-1} (y_{t+1} - m_{t+1})- \text{trace}[s_{t+1}^{-1} \sigma_w] \Big) 
\end{align}
Note that if $\sigma_w = ba \sigma_z a'b'$ the two derivatives are identical. In general we think the problem may be ill-posed when both $h$ and $g$ are made adaptable. We recommend to use models where only one of the two is treated as a random variable and the other one is fixed. Thus we get the following update equations
\begin{align}
&\hat g_{t+1} = \hat g_t + \epsilon \Big(  (y_{t+1} - m_{t+1})' s_{t+1}^{-1} b \sigma_z  b' s_{t+1}^{-1} (y_{t+1} - m_{t+1}) - \text{trace}[s^{-1}_{t+1}b\sigma_z b'] \Big) \\
&\hat h_{t+1} = \hat h_t + \epsilon \Big(  (y_{t+1} - m_{t+1})' s_{t+1}^{-1} \sigma_w as_{t+1}^{-1} (y_{t+1} - m_{t+1}) - \text{trace}[s^{-1}_{t+1}\sigma_w] \Big) \\
\end{align}
where $\epsilon \geq 0$ is the adaptation rate.

Regarding the second problem
\begin{align}
p(x_{t+1} \given y_{1:t+1}, \hat g_{1:t+1},\hat h_{1:t+1}) = \phi( x_{t+1}\given \mu_{t+1}^{t+1} , \sigma_{t+1}^{t+1})
\end{align}
where
\begin{align}
&\mu_{t+1}^{t+1} = E[X_{t+1}\given y_{1:t+1}, \hat g_{1:t+1},\hat h_{1:t+1}] = \mu_t^t + \alpha_{t+1} (y_{t+1} - b a \mu_t^t)\\
&\sigma_{t+1}^{t+1} = E[X_{t+1}\given y_{1:t+1}, \hat g_{1:t+1},\hat h_{1:t+1}] = \left(I - \alpha_{t+1} b \right) \sigma_{t}^{t+1}
\end{align}
and
\begin{align}
\sigma_{t}^{t+1} &= Var[X_{t+1}\given y_{1:t},\hat g_{1:t+1},\hat h_{1:t+1}] =  a\sigma_t^t a' +\hat  g_{t+1}\sigma_z\\
\alpha_{t+1} &= \sigma_t^{t+1} b'(b\sigma_t^{t+1} b' +\hat  h_{t+1} \sigma_w)^{-1} 
\end{align}
Consider the simple case in which $a=b=1$. In this case the Kalman
gain takes the following form
\begin{align}
\alpha_{t+1} = ( \hat g_{t+1} \sigma_z +\sigma_t^t)/( \hat g_{t+1} \sigma_z + \sigma_t^t + \hat h_{t+1} \sigma_w) 
\end{align}

We note that as the system noise $\hat g_{t+1}$ increasea, the gain
$\alpha_{t+1}$ increases thus paying more attention to the new data
$y_{t+1}$. On the other hand as the observation noise $\hat h_{t+1}$
increases, the gain decreases, thus paying less attention to the new
observation and more to the past beliefs.  From the update equations
of the Kalman filter we have that

\begin{align}
(\sigma_{t+1}^{t+1})^{-1} =  ((\sigma_t^t)^{-1} + (\hat g_{t+1}
  \sigma_z)^{-1})^{-1} + \hat h_{t+1}^{-1} \sigma^{-1}_w
\end{align}
Thus if $\hat g_{t+1}$ or $\hat h_{t+1}$ increases, the certainty
(inverse variance) about the state decreases.

\section{Appendix}
Let 
\begin{align}
p = \phi(y\given \mu, \sigma)
\end{align}
where $\phi$ is the $n$-dimensional Gaussian distribution with mean
$\mu$ and co-variance $\sigma$. We let
\begin{align}
\sigma=\alpha + k \beta
\end{align}
where $k\in \R$ is a scalar. We note 
\begin{align}
&\frac{d |\sigma|}{dk} = |\sigma| \text{trace}[\sigma^{-1} \beta ]\\
&\frac{d \log |\sigma|}{dk} = \text{trace}[\sigma^{-1} \beta]\\
&\frac{d  \sigma^{-1}}{dk} = - \sigma^{-1} \beta \sigma^{-1} \\
&\frac{d  (y -\mu)' \sigma^{-1} (y-\mu)}{dk} = (y-\mu)' \frac{d \sigma^{-1}}{dx} (y-\mu) = - y' \sigma^{-1} \beta \sigma^{-1} y
\end{align}
Thus
\begin{align}
\frac{d \log \phi(y,\given \mu,\sigma)}{dk} = \frac{1}{2} \Big(  (y-\mu)' \sigma^{-1} \beta \sigma^{-1}(y-\mu) - \text{trace}[\sigma^{-1} \beta])\Big)
\end{align}

\end{document}

