'''
Created on Aug 1, 2011

@author: nick
'''

import matplotlib
matplotlib.use('macosx')
import matplotlib.pyplot as pyplot
pyplot.ion()

from numpy import *
from numpy.random import *
from numpy.linalg import *
from matplotlib.patches import Ellipse

def gravity_model(timescale       = 0.1, 
                  xyuv_prior_mean = (0.,5.,10.,10.),
                  xyuv_prior_var  = (0., 1., 1., .5),
                  drag            = .5,
                  system_var      = .001,
                  obs_var         = .01  
                  ):
  '''
  Initializes and returns a linear Gaussian system (LGS) model with  
  specified parameters. Also returns the force of gravity at the 
  specified timescale.
  
  Parameters:
  timescale: length of one simulation tic, in seconds.
  xyuv_prior_mean: prior on xy position and uv velocity, in meters and meters/second
  xyuv_prior_var: variance on the above (assume each is i.i.d., no cross terms)
  drag: Fraction of the speed that will be lost in one second. Must be in [0,1]
  system_var: noise of the physical system
  obs_var: noise of the observations  
  ''' 
  assert drag <= 1. and drag >= 0., 'Drag must be between 0 and 1.'

  #exp(log(keep)/invscale)
  if not (drag == 1):
    drag = 1.-exp(log(1.-drag)*timescale)
  
  
  lgs_model = LGS()
  lgs_model.mu_x0 = array([[xyuv_prior_mean[0]],
                           [xyuv_prior_mean[1]],
                           [xyuv_prior_mean[2]],
                           [xyuv_prior_mean[3]]])
  lgs_model.sigma_x0 = array([[xyuv_prior_var[0], 0., 0., 0.],
                              [0., xyuv_prior_var[1], 0., 0.],
                              [0., 0., xyuv_prior_var[2], 0.],
                              [0., 0., 0., xyuv_prior_var[3]]])
  lgs_model.a = array([[1., 0., timescale, 0.       ],
                       [0., 1., 0.       , timescale],
                       [0., 0., 1.-drag  , 0.       ],
                       [0., 0., 0.       , 1.-drag  ]])
  lgs_model.b = array([[0., 0.],
                       [0., 0.],
                       [1., 0.],
                       [0., 1.]]) # Force causes acceleration
  lgs_model.sigma_R = system_var*eye(4) # Physical noise
  lgs_model.c = array([[1., 0., 0., 0.],
                       [0., 1., 0., 0.]]) # Can only observe position
  lgs_model.sigma_Q = .1*eye(2)
  
  u = array([[0.],[-9.8 * timescale]]) # Gravity, .1s timescale  
  
  return (lgs_model, u)

class LGS:
  ''' 
  A class that represents a linear-Gaussian system's parameters,
  and common operations such as simulation, filtering, and 
  smoothing. The notation used in this class is modeled after
  the Kalman filter derivation in Thrun et al.
  
  The system can be modeled by 
  X_t = aX_{t-1}+bu_t+R_t
  Z_t = cX_t+Q_t
  
  The model parameters are:
  mu_x0    - the prior x mean              (nx1)
  sigma_x0 - the prior x covariance matrix (nxn)
  a        - the dynamics matrix           (nxn)
  b        - the control matrix            (nxm)
  c        - the observation matrix        (oxn)
  sigma_R  - the system noise              (nxn)
  sigma_Q  - the observation noise         (oxo) 
  
  '''
  
  def __init__(self):
    self.mu_x0 = None
    self.sigma_x0 = None
    self.a = None
    self.b = None
    self.c = None
    self.sigma_R = None
    self.sigma_Q = None
  
  def _check_consistency(self):
    assert (len(self.a.shape)==2 
            and self.a.shape[0]==self.a.shape[1]
            ), "Expected 2d square a matrix (nxn)"
            
    n = self.a.shape[0]
    
    assert (len(self.mu_x0.shape)==2 
            and self.mu_x0.shape[0]== n
            and self.mu_x0.shape[1] == 1
            ), "Expected 2d column mu_x0 matrix (nx1)"
    assert (len(self.sigma_x0.shape)==2 
            and self.sigma_x0.shape[0]==n
            and self.sigma_x0.shape[1] == n
            ), "Expected 2d square sigma_x0 matrix (nxn)"
    assert (len(self.b.shape)==2
            and self.b.shape[0]==n
            ), "Expected 2d b matrix (nxm)"
    assert (len(self.sigma_R.shape)==2
            and self.sigma_R.shape[0] == n
            and self.sigma_R.shape[1] == n
            ), "Expected 2d square sigma_R matrix (nxn)"
    assert (len(self.c.shape)==2
            and self.c.shape[1] == self.a.shape[0]
            ), "Expected 2d c matrix (oxn)"
            
    o = self.c.shape[0]
    assert (len(self.sigma_Q.shape)==2
            and self.sigma_Q.shape[0] == o
            and self.sigma_Q.shape[1] == o
            ), "Expected 2d square sigma_Q matrix (oxo)"
            
  
  def simulate(self, x = None, u = None):
    '''
    Compute the next (state,observation) tuple given a current state.
    If no x is given, one is drawn from the prior. If no u is given,
    it is assumed to be a zero-matrix of appropriate size.
    '''
    
    self._check_consistency()
    
    n = self.mu_x0.shape[0]
    m = self.b.shape[1]
    o = self.c.shape[0]
    
    if x==None:
      x = multivariate_normal(self.mu_x0.reshape(n), self.sigma_x0).reshape(n,1)
    if u == None:
      u = zeros((m,1))
    
    assert (len(x.shape)==2 
            and x.shape[0] == n
            and x.shape[1] == 1
            ), "Expected 2D column matrix state x (nx1)."
            
    assert (len(u.shape)==2 
            and u.shape[0] == m
            and u.shape[1] == 1
            ), "Expected 2D column matrix control u (mx1)."
    
    new_x = (dot(self.a,x)
             +dot(self.b,u)
             +multivariate_normal(zeros(n),self.sigma_R).reshape(n,1)
             )
    
    z = dot(self.c,new_x)+multivariate_normal(zeros(o),self.sigma_Q).reshape(o,1)
    
    return (new_x, z)
  
  def filter(self, z, mu_x = None, sigma_x = None, u = None):
    '''
    Compute the posterior (mean,cov) tuple given the current
    observation and current prior.
    If no prior is given, the initial is used. If no u is given,
    it is assumed to be a zero-matrix of appropriate size.
    '''
    
    self._check_consistency()
    
    m = self.b.shape[1]
    o = self.c.shape[0]
    
    if mu_x == None:
      mu_x = self.mu_x0
    if sigma_x == None:
      sigma_x = self.sigma_x0
    if u == None:
      u = zeros((m,1))
            
    assert (len(u.shape)==2 
            and u.shape[0] == m
            and u.shape[1] == 1
            ), "Expected 2D column matrix control u (mx1)."
    assert (len(z.shape)==2 
            and z.shape[0] == o
            and z.shape[1] == 1
            ), "Expected 2D column matrix observation z (ox1)."
    assert (mu_x.shape == self.mu_x0.shape
            ), "Expected 2d column mu_x matrix (nx1)"
    assert (sigma_x.shape==self.sigma_x0.shape
            ), "Expected 2d square sigma_x matrix (nxn)"
    
    mu_hat = dot(self.a,mu_x)+dot(self.b,u)
    sigma_hat = dot(dot(self.a, sigma_x), self.a.transpose())+self.sigma_R
    
    kalman_gain_den = inv(dot(dot(self.c,sigma_hat),self.c.transpose())+self.sigma_Q)
    kalman_gain = dot(dot(sigma_hat,self.c.transpose()), kalman_gain_den)
    
    new_mu = mu_hat + dot(kalman_gain, z-dot(self.c,mu_hat))
    new_sigma = dot(eye(kalman_gain.shape[0])-dot(kalman_gain,self.c),sigma_hat)
    return (new_mu,new_sigma)
  
   
  def smooth(self, zs, us = None):
    '''
    Compute the posterior (means,covs) tuple given all
    observations. If no u is given,
    it is assumed to be a zero-matrix of appropriate size.
    '''
    
    self._check_consistency()
    
    n = self.a.shape[0]
    m = self.b.shape[1]
    o = self.c.shape[0]
    
    assert (len(zs.shape)==2 
            and zs.shape[0] == o
            ), "Expected 2D column matrix observation zs (oxd)."
            
    d = zs.shape[1]
            
    if us == None:
      us = zeros((m,d))
            
    assert (len(us.shape)==2 
            and us.shape[0] == m
            and us.shape[1] == d
            ), "Expected 2D column matrix control us (mxd)."
    
    mus = zeros((n, d))
    sigmas = zeros((n,n,d))
    
    mu = None
    sigma = None
    
    #Forward Pass  
    for i in range(0,d):
      mu, sigma = self.filter(zs[:,i].reshape(o,1),mu,sigma,us[:,i].reshape(m,1))
      mus[:,i] = mu[:,0]
      sigmas[:,:,i] = sigma[:,:]
    
    #Backward Pass    
    for i in range(numpts-2,-1,-1):
      oldmu0 = reshape(mus[:,i],(n,1))
      newmu1 = reshape(mus[:,i+1],(n,1))
      oldsigma0 = reshape(sigmas[:,:,i],(n,n))
      newsigma1 = reshape(sigmas[:,:,i+1],(n,n))
      P = dot(dot(self.a,oldsigma0),self.a.transpose())+self.sigma_R
      J = dot(dot(oldsigma0,self.a.transpose()),inv(P))
      newmu0 = oldmu0 + dot(J,newmu1-(dot(self.a,oldmu0)+dot(self.b,us[:,i].reshape(m,1)))) 
      newsigma0 = oldsigma0 + dot(dot(J,newsigma1-P),J.transpose())
      mus[:,i] = newmu0[:,0]
      sigmas[:,:,i] = newsigma0[:,:]
      
    return (mus,sigmas) 

if __name__ == '__main__':
  lgs_model, u = gravity_model()
  
  n = lgs_model.a.shape[0]
  m = lgs_model.b.shape[1]
  o = lgs_model.c.shape[0]
  
  x = None
  mu = None
  sigma = None
  numpts = 21
  
  
  states = zeros((n, numpts))
  state_ests = zeros((n, numpts))
  obs = zeros((o, numpts))
  acts = zeros((m, numpts))
  sigmas = zeros((n,n,numpts))
  
  for i in range(0,numpts):
    x, z = lgs_model.simulate(x,u)
    states[:,i] = x[:,0]
    obs[:,i] = z[:,0]
    acts[:,i] = u[:,0]
    mu, sigma = lgs_model.filter(z,mu,sigma,u)
    state_ests[:,i] = mu[:,0]
    sigmas[:,:,i] = sigma[:,:]
    
  sm_state_ests, sm_sigmas = lgs_model.smooth(obs,acts)
  
  fig = pyplot.figure()
  ax = fig.add_subplot(111, aspect='equal')
  for i in range(0,states.shape[1]):
    ax.add_artist(Ellipse(xy=state_ests[0:2,i], 
                          width=6*sqrt(sigmas[0,0,i]),
                          height=6*sqrt(sigmas[1,1,i]),
                          facecolor='g',
                          alpha=.3,
                          edgecolor='k')
                  )
  for i in range(0,states.shape[1]):
    ax.add_artist(Ellipse(xy=sm_state_ests[0:2,i], 
                          width=6*sqrt(sm_sigmas[0,0,i]),
                          height=6*sqrt(sm_sigmas[1,1,i]),
                          facecolor='m',
                          alpha=.3,
                          edgecolor='k')
                  )
    
  ax.scatter(obs[0,:],obs[1,:])
  ax.plot(states[0,:],states[1,:],'r-')
  ax.plot(state_ests[0,:],state_ests[1,:],'g-')
  ax.plot(sm_state_ests[0,:],sm_state_ests[1,:],'m-')

  pyplot.show()
  print 'Click on the figure to quit: > '
  x = pyplot.ginput(1)