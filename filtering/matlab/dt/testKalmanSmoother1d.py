from pylab import *
from kalmanSmoother1d import *
t = range(5*60*30) # five mins at 30 frames per second
t = array(t)/30.0

qt = linspace(0,5*60,5*60) #query times


f= 1/60.0 # frequency in hertz
y=(sin(2.0*pi*f*t)+rand(len(t))) # observations 


s=0.000001 #smoothing constat very small, this pretty much gets the observations at the 
f= kalmanSmoother1d(y,t,s)
ys = f.smoothAt(qt)

s=1000 # try another smoothing constant
ys2 = kalmanSmoother1d(y,t,s).smoothAt(qt)
plot(qt,ys,'b',linewidth=0.5)
plot(qt,ys2,'r',linewidth=2)
show()
