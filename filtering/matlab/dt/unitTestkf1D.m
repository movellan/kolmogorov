clear
%%%% Initialize filter
f.a = [1 ] ; % state dynamics
f.b= [ 1]; % observation matrix
f.nx = size(f.a,1);
f.ny = size(f.b,1);
f.priorMeanX = [1]; % prior state mean
f.priorVarX= eye(f.nx); % prior state variance
f.varZ = 1*eye(f.nx); % state noise variance matrix
f.varW= eye(f.ny);  % observation noise variance matrix

f.T=4; % number of time Steps
f.y = []; % observation


x= zeros(f.nx,f.T);
x(:,1)= f.priorMeanX;


sqrtz= sqrtm(f.varZ);
sqrtw = sqrtm(f.varW);

% fix the noise so we can unitest accross platforms
noisex= repmat([1 -1],f.nx,f.T/2+1);
noisey = repmat([-1 1],f.ny,f.T/2+1);



f.y=[0 3 0 3]
f = kf(f);



f.fm % filterMean
% should be     0.5000    2.0000    0.7692    2.1471
f.sm % smootherMean
% should be     0.9118    1.7353    1.2941    2.1471
f.filterVar(:,:,4) 
% should be 0.6176

tolerance =0.001;
testFailed=0;
for k=1:4
  expect=[0 3 0 3];
  if abs(f.y(k) - expect(k))> tolerance
    testFailed=1;
  end
end
if testFailed ==0
  display('Test 1 Passed')
else
  display('Test 1 Failed')
end

testFailed=0;
for k=1:4
  expect=[0.5; 2.0; 0.7692; 2.1471];
  if abs(f.fm(k) - expect(k))> tolerance
    testFailed=1;
  end
end
if testFailed ==0
  display('Test 2 Passed')
else
  display('Test 2 Failed')
end

testFailed=0;
for k=1:4
  expect=[0.9118; 1.7353; 1.2941; 2.1471];
  if abs(f.sm(k) - expect(k))> tolerance
    testFailed=1;
  end
end
if testFailed ==0
  display('Test 3 Passed')
else
  display('Test 3 Failed')
end

if abs(f.filterVar(:,:,4) -0.6176) > tolerance
  display('Test 4 Failed')
else
  display('Test 4 Passed')
end

