% multivariate Gaussian (normal) probability density function
function y=mvgpdf(o,m,s)

d= (o-m);
y = 0.5*d'*inv(s)*d;
y = exp(-y);
n = length(m);
z = (2*pi)^(n/2);
z = z*sqrt(det(s));
y = y/z;