%f.sm(1,:)' should look like this
%     1.2444
%     2.1903
%     1.6374
%     1.5902
%     0.1046
%    -0.6871
%    -2.5015
%    -2.9965
%    -3.7694
%    -2.5264
   
% f.fm(1,:)'  should be like this
%     0.8333
%     2.8454
%     0.8237
%     2.5048
%    -0.8920
%     0.1702
%    -3.6607
%    -2.3764
%    -5.3294
%    -2.5264
   
% f.y(1,:)' should look like this
%   -2.0000
%   4.5000
%   1.7500
%   7.6250
%   3.3125
%   6.8438
%   -0.2031
%   0.7891
%   -7.9180
%   -7.0723

clear
f.a = [1 -.5 ; .5 1] ; % state dynamics
f.b= [ 1 2]; % observation matrix
f.nx = size(f.a,1);
f.ny = size(f.b,1);
f.mx0 = [1 ; -1];
f.sx0= eye(f.nx);
t=10;
f.bufferSize=10;
f.y = [];
f.fx =[]; %forward mean
f.fm=[]; % forward mean prior time step
f.fs = []; % forward var prior time step
f.sm=[];
f.ss=[];

f.adaptswRate=1/2;
f.adaptszRate=1/2;

x= zeros(f.nx,t);
x(:,1)= f.mx0;

f.sz = 1*eye(f.nx);
f.sw= eye(f.ny);
f.szInit = f.sz;
f.swInit = f.sw;

sqrtz= sqrtm(f.sz);
sqrtw = sqrtm(f.sw);
k1=1;
k2=1;

noisex= repmat([1 -1],f.nx,t/2+1);
noisey = repmat([-1 1],f.ny,t/2+1);
for i=1:t
  y(:,i) = f.b*x(:,i) + sqrtw*noisey(:,i);
  x(:,i+1) = f.a*x(:,i) +sqrtz*noisex(:,i);
  %f = myKf(f,y(:,i));
  f = kf(f,y(:,i));
  sw(i) = f.sw;
end

plot(f.sm(1,1:t),'b--')
hold on

plot(f.sm(2,1:t),'r--')
plot(x(1,1:t),'b')
plot(x(2,1:t),'r')
plot(f.fm(1,1:t),'b+')
plot(f.fm(2,1:t),'r+')
f.sm(1,:)'
f.fm(1,:)'
f.y'

