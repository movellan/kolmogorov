% Kalman filter.
% Model is of the form 
% X_{t+1} = a X_t +  Z_t
% Y_{t+1} = b X_{t+1} +  W_t
%
% where X_t in R^{nx} is the system's state
% a in R^{nx} x R^{nx} is the state transition matrix
% Z_t \in R^n is a Gaussian random vector with zero mean and
% covariance matrix sz
% Y_t \in R^{ny} is the observation vector
% b \in R^{ny} x R^{nx} is the observation model
% W_t \in \R^{ny} is a Gaussian vector with zero mean and
% covariance matrix sw
%
% function f = kf(f)
%
% f is a structure with the following fields

% f.T: integer. time steps of observation buffer
% f.nx: integer. dimensionality of state 
% f.ny: integer. dimensionality of observation
% f.b:  ny * nx matrix. observation model matrix
% f.a:  nx * nx matrix. state transition matrix
% f.priorMeanX: nx * 1  vector. initial state mean 
% f.priorVarX: nx * nx matrix. initial state variance matrix 
% f.varZ: nx * nx matrix. variance of Z (state transition)
% f.varW: ny * ny. variance matrix of W (observation noise)
% f.y: ny * T matrix. Observations 
% f.fm: nx * T matrix. Filter state  estimates mean
% f.filterVar: nx * nx*  T matrix. Filter state  estimates covariance
% f.sm: nx * T matrix: Smoother state estimates mean 
% f.smootherVar: nx * nx* T matrix: Smoother state estimates covariance
% f.logLikelihood: scalar: log likelihood of observation buffer.

% Copyright @ Javier R. Movellan 2011
% IMT style open source licensed. 


function f = kf(f)

f.fm=[]; % forward mean prior time step
f.filterVar = []; % forward var prior time step
f.model='Xt+1 = a Xt+ Zt; Yt+1 = bYt + Wt';

mx = f.priorMeanX; % prior state mean 
vx = f.priorVarX; % prior state variance 
f.logLikelihood=0;
%%%%%%% forward  filtering pass %%%%%

for t=1:f.T
  my = f.b*mx; % Expected value for Yt
  vy = f.b*vx*f.b'+ f.varW; % variance matrix of y

  display(t)  
  display('vy')
  display(vy)
  cxy = vx*f.b';% covariance xy
  f.logLikelihood = f.logLikelihood+ log(mvgpdf(f.y(:,t),my,sqrtm(vy)));  
  % filter correction step
  k= cxy*inv(vy);
  display('k');
  display(k)
  mx = mx+ k*(f.y(:,t)- my);
  vx =  vx- k*vy*k';
  % filter mean (expected state) E[X_t | y_{1:t}]
  f.fm(:,t) = mx;
  % filter covariance matrix Cov(X_t | y_{1:t}]
  f.filterVar(:,:,t) = vx;
  % forward prediction step
  mx = f.a*mx; 
  vx= f.a*vx*f.a' +f.varZ;
end



%%%%%%% backwards smoothing pass %%%%%
% initialize smoother with last time step of filter
f.sm(:,f.T) = f.fm(:,f.T);
f.smootherVar(:,:,f.T)=f.filterVar(:,:,f.T);

for t = f.T-1:-1:1
  v = f.filterVar(:,:,t);
  % filter variance time t plus 1
  vp1 = f.a*v*f.a'+f.varZ;
  % covariance time Xt, Xt+1
  cttp1 = v*f.a';
  % smoother gain
  g= cttp1*inv(vp1);
  m = f.fm(:,t);
  mp1= f.a*m;  
  % smoother mean E[X_t | y_{1:T}]
  f.sm(:,t) = m+g*(f.sm(:,t+1) - mp1);
  % smooter cov matrix Cov[X_t | y_{1:T}]
  f.smootherVar(:,:,t) = v - g*(vp1- f.smootherVar(:,:,t+1))*g';
end




