% Adaptive kalman smoother. At every time step we compute the
% kalman filter estimates, kalman smoother estimate and, optionally
% adapt the model parameters based on the observations kept in a
% running buffer
% Model is of the form 
% X_{t+1} = a X_t + sqrt(Kz_t) Z_t
% Y_{t+1} = b X_{t+1} + sqrt(Kw_t) W_t
%
% where X_t in R^{nx} is the system's state
% a in R^{nx} x R^{nx} is the state transition matrix
% Z_t \in R^n is a Gaussian random vector with zero mean and
% covariance matrix sz
% Y_t \in R^{ny} is the observation vector
% b \in R^{ny} x R^{nx} is the observation model
% W_t \in \R^{ny} is a Gaussian vector with zero mean and
% covariance matrix sw
%
% function f = akf(f,o,sw)
%
% f is a structure with the following fields

% f.buffersize: integer. size of observation buffer
% f.nx: integer. dimensionality of state 
% f.ny: integer. dimensionality of observation
% f.b:  ny * nx matrix. observation model matrix
% f.a:  nx * nx matrix. state transition matrix
% f.mx0: nx * 1  vector. initial state mean 
% f.sx0: nx * nx matrix. initial state covariance matrix 
% f.sz: nx * nx matrix. covariance of Z (state transition)
% f.kz: positive scalar. estimate of stqte transition noise  level
% f.sw: ny * ny * bufferSize. observation covariance. note each
% individual observation comes with its own covariance
% f.kw: positive scalar. estimate of  observation noise level
% f.adaptkzRate: scalar in [0 1] interval. EM adaptation rate for kz
% f.adaptkwRate: scalar in [0 1] interval. EM adaptation rate for kw
% f.adaptbRate: scalar in [0 1] interval. EM adaptation rate for b
% f.y: ny * bufferSize matrix. Observations 
% f.fm: nx * bufferSize matrix. Filter state  estimates mean
% f.fs: nx * nx*  bufferSize matrix. Filter state  estimates covariance
% f.sm: nx * bufferSize matrix: Smoother state estimates mean 
% f.ss: nx * nx* bufferSize matrix: Smoother state estimates covariance
% f.L: scalar: log likelihood of observation buffer/time steps in buffer

% Copyright @ Javier R. Movellan 2011
% IMT style open source licensed. 


function f = akf(f,o,sw)
bs = size(f.y,2); % buffer size
bs=bs+1;
f.y(:,bs) =o; % add observation and observation covariance to the buffer
f.sw(:,:,bs) =sw;
for cycle=1:1 % EM iterations per time step
  m = f.mx0; % prior state mean 
  s = f.sx0; % prior state variance 
  f.L = log(mvgpdf(o,m,s));% is this right? why? 
  SSW=0;
%%%%%%% forrward stochastic filtering pass %%%%%
  for t=1:bs
    if t>1
      % forward prediction step
      m = f.a*m; 
      s= f.a*s*f.a' +f.sz*f.kz;
      f.L = f.L+ log(mvgpdf(o,m,sqrtm(s)));
    end
    
    % forward correction step
    k= s*f.b'*inv(f.b*s*f.b'+f.sw(:,:,t)*f.kw);
    m = m+ k*(f.y(:,t)- f.b*m);
    s=  s- k*f.b*s;
    % filter mean (expected state) E[X_t | y_{1:t}]
    f.fm(:,t) = m;
    % filter covariance matrix Cov(X_t | y_{1:t}]
    f.fs(:,:,t) = s;

  end

  %%%%%%% backwards stochastic smoothing pass %%%%%
  m = f.fm(:,bs);
  s = f.fs(:,:,bs);
  % initialize smoother with last time step of filter
  f.sm(:,bs) = m;
  f.ss(:,:,bs)=s;
  % initialize EM statistics
  if f.adaptkwRate + f.adaptbRate + f.adaptaRate + f.adaptkzRate >0
    yy=f.y(:,bs)*f.y(:,bs)';
    xy =m*f.y(:,bs)';
    xx= s +m*m';
    xxp1= zeros(f.nx,f.nx);
    etaw = inv(f.sw(:,:,bs)*f.kw);
    SSW= SSW+ trace(f.y(:,bs)*f.y(:,bs)'*etaw) ;
    SSW = SSW+ trace((s+m*m') *f.b'*etaw*f.b);
    SSW= SSW - 2*trace(m*f.y(:,bs)'*etaw*f.b);
  end
  
  for t = bs-1:-1:1
    s = f.fs(:,:,t);
    % filter variance time t plus 1
    sp1 = f.a*s*f.a'+f.sz*f.kz; 
    m = f.fm(:,t);
    mp1= f.a*m;  
    % smoother gain
    g= s*f.a'*inv(sp1);
    % smoother mean E[X_t | y_{1:T}]
    f.sm(:,t) = m+g*(f.sm(:,t+1) - mp1);
    % smooter cov matrix Cov[X_t | y_{1:T}]
    f.ss(:,:,t) = s - g*(sp1- f.ss(:,:,t+1))*g';
    % collect statistics needed for EM learning 
    if f.adaptkwRate + f.adaptbRate + f.adaptaRate+f.adaptkzRate >0
      xy = xy + f.sm(:,t)*f.y(:,t)';
      xxp1 =xxp1+ f.fm(:,t)*f.sm(:,t+1)' ;
      xxp1 = xxp1+ g*(f.ss(:,:,t+1) +(f.sm(:,t+1) - mp1)*f.sm(:,t+1)');
      
      etaw = inv(f.sw(:,:,t)*f.kw);
      SSW= SSW+ trace(f.y(:,t)*f.y(:,t)'*etaw) ;
      SSW = SSW+ trace((f.ss(:,:,t) + f.sm(:,t)*f.sm(:,t)') *f.b'*etaw*f.b);
      SSW= SSW - 2*trace(f.sm(:,t)*f.y(:,t)'*etaw*f.b);
    end
  end
  if f.adaptkwRate + f.adaptbRate + f.adaptaRate+f.adaptkzRate >0
    yy = f.y* f.y'; 
    xx = sum(f.ss,3) + f.sm*f.sm'; 
  end
  
  f.L = f.L/bs; 

  % EM algorithm on observation matrtix a
  if f.adaptaRate>0 & bs>10
    tmp = (xxp1)'*pinv(xx);
    % we smooth the EM found new value and the old value
    f.a = (1-f.adaptaRate)*f.a + f.adaptaRate*tmp;
  end
  
  
  % EM algorithm on observation matrtix b
  if f.adaptbRate>0 & bs>10
    tmp = (xy)'*pinv(xx);
    % we smooth the EM found new value and the old value
    f.b = (1-f.adaptbRate)*f.b + f.adaptbRate*tmp;
  end
  
  %EM algorithm on gain of Cov(W). Note we do not learn the entire
  %matrix but a scalar to multiply the original matrix by. 
  if f.adaptkwRate>0 & bs >10
    tmp = SSW/(bs*f.ny);
    % we smooth the EM found new value and the old value
    f.kw = (1- f.adaptkwRate) *f.kw +f.adaptkwRate* f.kw*tmp;
  end
  
  if f.adaptkzRate>0 & bs> 10
    %EM algorithm on gain of Cov(Z). Note we do not learn the entire
    %matrix but a scalar to multiply the original matrix by. 
    xp1xp1 = xx - f.ss(:,:,bs) - f.sm(:,bs)* f.sm(:,bs)'; 
    xx = xx - f.ss(:,:,1) - f.sm(:,1)* f.sm(:,1)'; 
    etaz = inv(f.sz*f.kz);
    tmp = trace(xp1xp1*etaz)+ trace(xx*f.a'*etaz*f.a) -2*trace(xxp1*etaz*f.a);
    tmp = tmp/((bs-1)*f.nx);
    % we smooth the EM found new value and the old value
    f.kz = (1- f.adaptkzRate) *f.kz +f.adaptkzRate* f.kz*tmp;
  end
end

% Prune buffer
if (bs > f.bufferSize)
  f.y(:,1)=[];
  f.fm(:,1)=[];
  f.fs(:,:,1)=[];
  f.sm(:,1)=[];
  f.ss(:,:,1)=[];
end
