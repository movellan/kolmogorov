% Test a running version of an adaptive kalman smoother. At every
% time step we add a new observation to the buffer and compute the
% kalman filter, kalman smoother and change the model parameters
% using the EM algorithm
% Copyright @ Javier R. Movellan 2011
% IMT style open source licensed. 
%
% Model is of the form 
% X_{t+1} = a X_t + Z_t
% Y_{t+1} = b X_{t+1} + W_t
%
% where X_t in R^{nx} is the system's state
% a in R^{nx} x R^{nx} is the state transition matrix
% Z_t \in R^n is a Gaussian random vector with zero mean and
% covariance matrix sz
% Y_t \in R^{ny} is the observation vector
% b \in R^{ny} x R^{nx} is the observation model
% W_t \in \R^{ny} is a Gaussian vector with zero mean and
% covariance matrix sw
% see akf.m for instructions

clear
clf

%%%%%%%%%%%% Model Parameters. You can play with this
T=300; % number of time steps in simulation
f.bufferSize=51; % size of the running window over which we do the
                 % filtering, smoothing and EM adaptation

f.nx=20; % dimensionality of the state
f.ny=20; % dimensionality of the observation
f.b= eye(f.ny); % observation matrix
f.a = -0.5*eye(f.nx); % state transition matrix
f.mx0 = zeros(f.nx,1); % mean of initial state distribution
f.sx0= eye(f.nx); % covariance matrix of initial state distribution

f.sz = 0.5*eye(f.nx); % covariance matrix of Z

% we let the observation covariance be a function of time, we set
% it later in the simulation. It will be f(:,:,t)

f.kz=1;  % scalar multiplier (gain) of system noise
f.kw =1; % scalar multiplier (gain) of observation noise


% adaptation rates. Between 0 and 1. Zero means no adaptation. 1
% give the output of EM on the current buffer. Values between Zero
% and 1 smooth the ouput of EM over buffers
f.adaptkzRate=1/2; % adapt gain of sz matrix (state noise)
f.adaptkwRate=0/1; % adapt gain of sw matrix (observation noise)
f.adaptbRate=0/4;  % adapt b matrix
f.adaptaRate=0/4;  % adapt a matrix (untested)
% I need to work on  the adaptation of the a matrix
%%%%%%%%%%%%%%%%%%% Initialize internal filter variables %%%%%

f.y = [];
f.fm=[]; % forward mean prior time step
f.fs = []; % forward var prior time step
f.sm=[];
f.ss=[];
f.L=[];
f.sw=[];
%%%%%%%%%%%%%%%%%%%%%%%%% Start simulation
%
% In this simulation we change the actual noise level of the state
% transitions or, optionally of the observation model. We study how
% the adaptive kalman filter tracks these changes


x= zeros(f.nx,T); 
x(:,1)= f.mx0; % initialize state





for t=1:T
  % we use kw and k2 to change the state and observation noise levels
  if t <= T/3
    kw(t)=1 ;
    kz(t)=1;
  elseif t>T/3 & t< 2*T/3
    kw(t) =1; 
    kz(t)= 10; 
  else
    kw(t) =1;
    kz(t)= 5; 
  end
    t
 
  
 
  % generate observation. We allow each observation to come
  % with its own sense of uncertainty in the form of a covariance
  % matrix sw
  sw= exp(0.2*randn(1))*eye(f.ny);
  sqrtw = sqrtm(sw);
  % note how kw controls the general observation noise level
  y(:,t) =  f.b*x(:,t) +  sqrt(kw(t))*sqrtw*randn(f.ny,1);

  % send observation to adaptive kalman filter and update it
  f = akf(f,y(:,t),sw);
  
  % generate next state
  sqrtz= sqrtm(f.sz);
  % note how kz controls the general system noise level
  x(:,t+1) = f.a*x(:,t) + sqrt(kz(t))*sqrtz*randn(f.nx,1);
  ekw(t) = f.kw; % estimate of kw(t)
  ekz(t) = f.kz; % estimate of kz(t)
  L(t) = f.L; % loglikelihood of observation buffer/ bu
  
end



subplot(131)
hold on
% plot one state dimension, the filter estimate and the smoother estimate
plot(f.fm(1,1:f.bufferSize),'b.')
plot(f.sm(1,1:f.bufferSize),'b+')
plot(x(1,t-f.bufferSize+1:t),'ro')
plot(x(1,t-f.bufferSize+1:t),'r--')
legend('Filter Estimate','Smoother Estimate','True State')
xlabel('Time')

subplot(132)
hold on
% plot the true and estimated noise levels
plot(ekw,'b-')
plot(kw,'b--')
plot(ekz,'r-')
plot(kz,'r--')
legend('Kw Estimate', 'True Kw', 'Kz Estimate','True Kz')
xlabel('Time')
ylabel('Noise Levels')
%plot(kb)
subplot(133)
hold on
% plot log likelihood rate of observations in buffer
plot(L,'b')
plot(L,'r','lineWidth',2)
xlabel('Time')
ylabel('Average Log Likelihod Of Buffered Observations')