   
% f.y(1,:)' should look like this
%   -2.0000
%   4.5000
%   1.7500
%   7.6250

%
% f.filterMean'  should look like this
%  0.8333   -1.3333
%  2.8454    0.5284
%  0.8237    0.7109
%  2.5048    2.3258


%f.smootherMean' should look like this
%     1.3602   -1.3682
%     2.4797    0.4091
%     2.1846    0.2965
%     2.5048    2.3258
    
%f.filterVar(:,:,4) should look as follows
% 2.3040   -0.9447
% -0.9447    0.5948


clear
%%%% Initialize filter
f.a = [1 -.5 ; .5 1] ; % state dynamics
f.b= [ 1 2]; % observation matrix
f.nx = size(f.a,1);
f.ny = size(f.b,1);
f.priorMeanX = [1 ; -1]; % prior state mean
f.priorVarX= eye(f.nx); % prior state variance
f.varZ = 1*eye(f.nx); % state noise variance matrix
f.varW= eye(f.ny);  % observation noise variance matrix

f.T=4; % number of time Steps
f.y = []; % observation


x= zeros(f.nx,f.T);
x(:,1)= f.priorMeanX;


sqrtz= sqrtm(f.varZ);
sqrtw = sqrtm(f.varW);

% fix the noise so we can unitest accross platforms
noisex= repmat([1 -1],f.nx,f.T/2+1);
noisey = repmat([-1 1],f.ny,f.T/2+1);


for i=1:f.T
  f.y(:,i) = f.b*x(:,i) + sqrtw*noisey(:,i);
  x(:,i+1) = f.a*x(:,i) +sqrtz*noisex(:,i);
end

f = kf(f);

f.y
f.filterMean
f.smootherMean
f.filterVar(:,:,4) 