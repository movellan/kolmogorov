from pylab import *
class kalmanSmoother1d:
#kalman  smoother for sequences of 1D observarions
#input is a sequence y of observations, a sequence t of time stamps, a smoothing parameter s
# and a sequence qt of query times, that may be different from t
# output are the smooth observations at the query times
# math model:
# X_{t+1} = X_t + Z_t state model. Zt, is Gaussian zero mean variance 1
# Y_{t+1} = X_{t+1} + W_t observationModel. Wt is Gaussian zero mean variance s
# Copyright: @ Emotient, 2014
# Developer: Javier R. Movellan, September, 2014

    def __init__(self,y,t,s):
        #y is a list of 1D observations
        #t is a non-decreasing list of times (in seconds) at which the observations occurred
        #qt is a non-decreasing list of query times in seconds
        # s is the smoothing parameter. The larger the more we smooth
        if type(y) is ndarray:
            y=y.tolist()
        if type(t) is  ndarray:
            t=t.tolist()
        if type(y) is float:
            y =[y]
        if type(t) is float:
            t=[t]
        self.priorMean =0
        self.varW=s
        self.v = (ones(len(t))*s).tolist()
        self.varZ=1.0
        self.priorVar = self.varW*10000000.0 # complete uncertainty about prior state
        self.y= y
        self.t=t
        self.qIn=[]
        self.qs=[]

    def filter(self):
    # forward filtering
        self.T = len(self.y)
        self.fm=zeros(self.T) #filter mean
        self.fv=zeros(self.T) # filter var
        self.sm=zeros(self.T) #smoother mean
        self.sv=zeros(self.T) #smoother var
        mx=self.priorMean
        vx=self.priorVar
        for k in range(self.T):
            vy=vx +self.v[k] # variance of Y
            cxy = vx # covariance X,Y
            g = cxy/(vy*1.0) # Kalman gain
            # filter correction step
            mx = mx + g*(self.y[k] - mx)
            vx = vx - g*vy*g
            self.fm[k] = mx #filter mean
            self.fv[k] = vx #filter variance
            # filter prediction step
            if k<self.T-1:
                dt = self.t[k+1] -self.t[k]
                vx= vx+ self.varZ*sqrt(dt) #verify this is the right way to handle time


    def smoother(self):
        # backwards smoothing
        self.sm[self.T-1] = self.fm[self.T-1]
        self.sv[self.T-1] = self.fv[self.T-1]
        for k in range(self.T-2,-1,-1):
            vt=self.fv[k]
            dt=self.t[k+1]-self.t[k]
            vp1= vt +self.varZ*sqrt(dt) #verify this is the right way to handle time
            cttp1 = vt # cov(Xt,Xt+1)
            g = cttp1/(vp1*1.0) #smoother gain
            m = self.fm[k]
            mp1 = m
            #smoother mean
            self.sm[k] = m+g*(self.sm[k+1] - mp1)
            self.sv[k] = vt - g*(vp1 - self.sv[k+1])*g
    # grab the query times. This is the most important function
    def smoothAt(self,qt2):
        if type(qt2) is ndarray:
            qt2=qt2.tolist()
        if type(qt2) is float:
            qt2 =[qt2]
        self.mergeQueryTimes(qt2)
        self.filter()
        self.smoother()
        qq=[]
        for k in range(len(qt2)):
            j = self.qIn[k]
            qq.append(self.sm[j])
        return(qq)

    def mergeQueryTimes(self,qt2):
        if type(qt2) is ndarray:
            qt2=qt2.tolist()
        if type(qt2) is float:
            qt2 =[qt2]
        for k1 in range(len(qt2)):
            if qt2[k1] >= self.t[-1]: # qt2[k1] larger that the largest t
                self.t.append(qt2[k1])
                self.y.append(0.0)
                self.v.append(self.priorVar)
                self.qIn.append(len(self.t)-1)
            else:
                # this can be made more efficient since the times are always increasing
                for k2  in range(len(self.t)):
                    if self.t[k2] >qt2[k1]:
                        self.t.insert(k2,qt2[k1])
                        self.y.insert(k2,0.0)
                        self.v.insert(k2,self.priorVar)
                        self.qIn.append(k2)
                        break





