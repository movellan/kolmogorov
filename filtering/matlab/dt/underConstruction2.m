%f.sm(1,:)' should look like this
%     1.2444
%     2.1903
%     1.6374
%     1.5902
%     0.1046
%    -0.6871
%    -2.5015
%    -2.9965
%    -3.7694
%    -2.5264
   
% f.fm(1,:)'  should be like this
%     0.8333
%     2.8454
%     0.8237
%     2.5048
%    -0.8920
%     0.1702
%    -3.6607
%    -2.3764
%    -5.3294
%    -2.5264
   
% f.y(1,:)' should look like this
%   -2.0000
%   4.5000
%   1.7500
%   7.6250
%   3.3125
%   6.8438
%   -0.2031
%   0.7891
%   -7.9180
%   -7.0723


clear
%%%% Initialize filter
f.a = [1 -.5 ; .5 1] ; % state dynamics
f.b= [ 1 2]; % observation matrix
f.nx = size(f.a,1);
f.ny = size(f.b,1);
f.priorMeanX = [1 ; -1];
f.priorVarX= eye(f.nx);
f.T=10;
f.y = [];
f.fx =[]; %forward mean
f.fm=[]; % forward mean prior time step
f.fs = []; % forward var prior time step
f.sm=[];
f.ss=[];

f.varZ = 1*eye(f.nx); % state noise variance matrix
f.varW= eye(f.ny);  % observation noise variance matrix


x= zeros(f.nx,f.T);
x(:,1)= f.priorMeanX;


sqrtz= sqrtm(f.varZ);
sqrtw = sqrtm(f.varW);

% fix the noise so we can unitest accross platforms
noisex= repmat([1 -1],f.nx,f.T/2+1);
noisey = repmat([-1 1],f.ny,f.T/2+1);


for i=1:f.T
  f.y(:,i) = f.b*x(:,i) + sqrtw*noisey(:,i);
  x(:,i+1) = f.a*x(:,i) +sqrtz*noisex(:,i);
end

f = kf(f);


load UnitTestKF2D

tolerance = 0.0001;
t(1) = norm(f.fm - expectedF.fm);
t(2) = norm(f.sm - expectedF.sm);
t(3) = norm(f.filterVar(:) - expectedF.filterVar(:));
t(4) = norm(f.smootherVar(:) - expectedF.smootherVar(:));

for k=1:4
  
  if t(k)<tolerance
    display(sprintf('Unit Test %d  Passed',k))
  else
    display(sprintf('Unit Test %d  Failed',k))
  end
end