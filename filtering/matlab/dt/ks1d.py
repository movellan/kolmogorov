from pylab import *

class ks1d:
    #kalman  smoother for sequences of 1D observarions
    # example use
    # k = ks1d(y,t,s)
    # y is a
    #  X_{t+1} = X_t + Z_t state model. Zt, is Gaussian zero mean variance 1
    # Y_{t+1} = X_{t+1} + W_t observationModel. Wt is Gaussian zero mean variance s

    def __init__(self,y,t,qt,s):
        #y is a list of 1D observations
        #t is a list of times (in seconds) at which the observations occurred
        #qt is query times in seconds
        # s is the smoothing parameter. The larger the more we smooth
        self.priorMean =0

        self.varW=s
        self.tmp= ones(len(t))*s
        self.v = self.tmp.tolist()
        self.varZ=1.0
        self.priorVar = self.varW*10000000.0
        self.y= y
        self.t=t
        self.qt=qt
        self.qIn=[]
        self.qs=[]
        self.mergeQueryTimes()
        self.T = len(self.y)
        self.fm=zeros(self.T) #filter mean
        self.fv=zeros(self.T) # filter var
        self.sm=zeros(self.T) #smoother mean
        self.sv=zeros(self.T) #smoother var

        # forward filtering
        mx=self.priorMean
        vx=self.priorVar
        for k in range(self.T):
            vy=vx +self.v[k] # variance of Y
            cxy = vx # covariance X,Y
            g = cxy/(vy*1.0) # Kalman gain
            # filter correction step
            mx = mx + g*(self.y[k] - mx)
            vx = vx - g*vy*g
            #    print k, k,mx,vx
            self.fm[k] = mx #filter mean
            self.fv[k] = vx #filter variance
            # filter prediction step
            if k<self.T-1:
                dt = self.t[k+1] -self.t[k]
                vx= vx+ self.varZ*sqrt(dt) #verify this is the right way to handle time
    
        self.sm[self.T-1] = self.fm[self.T-1]
        self.sv[self.T-1] = self.fv[self.T-1]
        for k in range(self.T-2,-1,-1):
            vt=self.fv[k]
            dt=self.t[k+1]-self.t[k]
            vp1= vt +self.varZ*sqrt(dt) #verify this is the right way to handle time
            cttp1 = vt # cov(Xt,Xt+1)
            g = cttp1/(vp1*1.0) #smoother gain
            m = self.fm[k]
            mp1 = m
            #smoother mean
            self.sm[k] = m+g*(self.sm[k+1] - mp1)
            self.sv[k] = vt - g*(vp1 - self.sv[k+1])*g
        # grab the query times
        for k in range(len(self.qt)):
            j = self.qIn[k]
            self.qs.append(self.sm[j])


    def mergeQueryTimes(self):
        for k1 in range(len(qt)):
            for k2  in range(len(t)):
                if self.t[k2] >self.qt[k1]:
                    self.t.insert(k2,self.qt[k1])
                    self.y.insert(k2,0.0)
                    self.v.insert(k2,self.priorVar)
                    self.qIn.append(k2)
                    break

    def printName(self):
        print 'Hello world'
    def unitTest(self):
        y=[0,3,0,3] #observation sequence
        t=[0,1,2,3] # time sequence in seconds
        fm=[]
        w=1
        f = ks1d(y,t,w)
        sm=[ 0.857142803772, 1.71428569376, 1.2857142775, 2.14285713875]
        tolerance=0.00001
        if sum(abs(f.sm-sm)>tolerance) ==0:
            print 'Unit Test Passed'
        else:
            print 'Unit Test Failed'





y=[-1,1,-1,1,-1] #observation sequence
t=[0.0,1.0,2.0,3.0,4.0] # time sequence in seconds
qt = [0.5,1.5,1.6,1.7,1.8,1.9,2.0]
s=0.1 # smoothing parameter. The larger the more you smooth.
f = ks1d(y,t,qt,s)


for k in range(f.T):
    print 'time=%1.2f raw signal=%3.2f, smooth signal=%3.2f'%(f.t[k], f.y[k], f.sm[k])
