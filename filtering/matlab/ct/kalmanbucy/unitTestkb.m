% Test the continuous time finite horizon linear quadratic regulator
% 
% Javier R. Movellan
% April 2010
%

clf
clear



nx =2; % dimensionality of the state
nu=2; % diensionality of action

tau = inf; % Time constant of the future discount in seconds. 
           % for t = tau seconds, the reward importance decays by
           % 100/e = 36.79 %



dt =1/100; % time step in seconds

T = 10; % in seconds
x0=[1;-2];



s= ceil(T/dt);



for t=1:s
  xi{t}  =[0;0];
  
  omega{t}=[0;0];
  k{t} = [0;0];
  a{t} = 0.1*[ 1 -2; 1 1] ;
  b{t} = eye(2);
  c{t} = 0*eye(2);
  q{t} = 0.2*eye(2);
  p{t} = 1*eye(2);
  w1{t} = zeros(nu,1);
  w2{t} = zeros(nu,nx);
  ow1{t}=zeros(nu,1);
  ow2{t}=zeros(nu,nx);
  newow1{t}=zeros(nu,1);
  newow2{t}=zeros(nu,nx);
  x{t} = zeros(nx,1);
end

pT= 10* p{s};
xiT =1*xi{s};




data= lqt(x,w1,w2,xi,xiT,omega,k,a,b,c,p,pT, q,tau,dt);
alpha = data.alpha;
beta=data.beta;
gamma=data.gamma;

  oAlpha=data.oAlpha;
  oBeta = data.oBeta;
  oGamma=data.oGamma;
  
newow1=data.ow1;
newow2= data.ow2;
dw1=data.dw1;
dw2=data.dw2;

corr1(1) = data.corr1;
corr2(1) = data.corr2;


% controller is of the form u_t = w1_t + w2_t* x_t

% now let's compare to Matlab's infinite horizon lqr solution
gain = lqr(a{1},b{1},p{1},q{1},zeros(nx));
% controller is of form u_t = -k* x_t

disp(sprintf('Finite Horizon LQR Initial Gain:'))
-data.ow2{1}
disp(sprintf('Matlab Infinite Horizon LQR Gain:'))
gain


if(norm(gain+data.ow2{2}) + norm(data.ow1{1}) < 0.0001)
  disp(sprintf('Unit Test Passed'));
else
  disp(sprintf('Unit Test Failed'));
end
disp('press any key to continue');

pause
epsilon2=1;
epsilon=0.001;
epsilon0=epsilon;

deltaW(1)=0;
for trial=1:1000000
  trial
  
   x{1}=x0;
   ox{1} = x{1};
   oval=0;
   val=0;

  for t=1:length(w1)-1;
    u = w1{t}+w2{t}*x{t};
    ow1{t} = epsilon2*newow1{t}+ (1-epsilon2)*w1{t};
    ow2{t} = epsilon2*newow2{t}+(1-epsilon2)*w2{t};
    ou = ow1{t}+ow2{t}*ox{t};
    
    val = val - (x{t}-xi{t})'*p{t}*(x{t}-xi{t})*dt;
    oval = oval - (ox{t}-xi{t})'*p{t}*(ox{t}-xi{t})*dt;
    
      oval=oval - (ou-omega{t})'*q{t}*(ou-omega{t})*dt;
      val=val - (u-omega{t})'*q{t}*(u-omega{t})*dt;
    
    
    

    dow1{t} = newow1{t}-w1{t};
    dow2{t} = newow2{t} - w2{t};
    du(:,t) = dw1{t} + dw2{t}*x{t};
    
    dou(:,t) = (newow1{t}- w1{t})+ (newow2{t} - w2{t})*x{t};


    w1{t}=w1{t}+ deltaW(trial)*dw1{t}; 
    w2{t} = w2{t} + deltaW(trial)*dw2{t};

    
    dx = (a{t}*x{t}+ b{t}*u)*dt;
    x{t+1} = x{t}+ dx;

    dox= (a{t}*ox{t}+ b{t}*ou)*dt;
    ox{t+1} = ox{t} + dox;
    
    
  end
  oval = oval-(ox{t+1}-xi{t+1})'*pT*(ox{t+1}-xi{t+1});
  val = val-(x{t+1}-xi{t+1})'*pT*(x{t+1}-xi{t+1});
  

  deltaW(trial+1) = epsilon;
  
    
   
    
   
    subplot(411)
    
    mx= cell2mat(x);
    mox = cell2mat(ox);
    plot(mx(1,:))
    hold on
    plot(mox(1,:),'r--');
    hold off

    subplot(412)
    plot(mx(2,:))
    hold on
    plot(mox(2,:),'r--');
    hold off
    

   
    

  v(trial) = - (x{1}'*alpha*x{1}-2*beta'*x{1}+gamma);
  ov(trial) = - (ox{1}'*oAlpha*ox{1}-2*oBeta'*ox{1}+oGamma);
  
  
  data=lqt(x,w1,w2,xi,xiT,omega,k,a,b,c,p,pT,q,tau,dt);

  alpha = data.alpha;
beta=data.beta;
gamma=data.gamma;

  
  oAlpha=data.oAlpha;
  oBeta = data.oBeta;
  oGamma=data.oGamma;
 newow1=data.ow1;
newow2= data.ow2;
dw1=data.dw1;
dw2=data.dw2;

corr1(trial+1) = data.corr1;
corr2(trial+1) = data.corr2;


 subplot(414)
 i1= max(1,trial-20);

 

  plot(corr1(i1:trial),'b--');hold on; plot(corr2(i1:trial),'r--');
  hold off; 

 
  
  subplot(413)
   i1= max(1,trial-20);
  plot(i1:trial,v(i1:trial),i1:trial,ov(i1:trial))

  val
  v(trial)
  oval
  ov(trial)
  if(trial >2)
    if v(trial) > v(trial-1)
      epsilon = epsilon*1;
    else epsilon=epsilon0;
    end
  end
  
   drawnow
   
end





