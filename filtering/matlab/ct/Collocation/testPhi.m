clear

o.nd = 1;% dimensionality of input
o.nphi=100; % number of features
o.ns = 20; % number of samples
for k=1:o.nphi
  o.gMean{k} = randn(o.nd,1);
  o.gPrecission{k} = 10*rand(1)*eye(o.nd);
end

%x = randn(o.nd,o.ns);
x=-10:0.2:10;



o.ns=size(x,2);



[f, gf,hf]=Phi(o,x);


y = 0.5*exp(-0.5*4*(x-2).^2)*sqrt(4)/sqrt(2*pi) + 0.5*exp(- 0.5*4*(x+1).^2)*sqrt(4)/sqrt(2*pi);

%y = exp(sin(2*pi*0.1*x));
w = pinv(f*f')*f*y';
[w2, lambda] = nnRegress(f',y');
yhat = w'*f;
yhat2 = w2'*f;



plot(x,y,'k', x,yhat,'-g',x,yhat2,'--r')
corr(y',yhat')
corr(y',yhat2')