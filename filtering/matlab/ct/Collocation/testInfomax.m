clear
clf
% contol following partially observable process
%dXt = Ut + c dBt
%dYt =  exp((X_t - alpha)'beta(X_t -alpha)') X_t + h  dWt
%with reward rate
%r(X_t,U_t) = -Xt'p Xt - Ut' q Ut

% the control policy should be a function of Y_{0:t}, U_{0,t}
 
dt= 1/100; % sampling time in seconds	       
T=5; % simulation time in secs
nt  = ceil(T/dt); 
tau = inf; % time horizon parameter for controller
epsilon=0.4; % learning rate from 0 to 1. 

nIterations=1; % ilqt iterations before displaying results
ntrials =60
nx = 1; % dimensionality of state vector
nu =1; % dimensionality of action vector

x0=[0]; %mean of prior state distribution 
s0 = 1*eye(nx); % variancer of prior state distribution



% initial controller
for t=1:nt
  w1{t}=zeros(nu,1); % open loop term
  w2{t}=zeros(nu,nx); %  closed loop gain matrix
end

systemDynamics=@systemDynamicsTest;

%%%%%%%%%%%%%% end of problem definition %%%%%%%%%%%




% now we test the new controller and plot results
x{1} = x0;


for t=1:nt
    u{t}= w1{t}+w2{t}*x{t};
    [dx, Jx, Ju] = systemDynamics(x{t},u{t},dt);
    x{t+1} = x{t}+ dx;

                                              % current run
end


