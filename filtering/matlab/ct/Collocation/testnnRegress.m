% find constrained least squares solution 
n=100;
p=3;
x = randn(n,p);
y = x*[1; -1; 1]+ randn(n,1);

iqc = ones(p,1)/3;
eqc= 0;
% solve regression problem 
% y approx x*w
% subject to w(i) +iqc(i)  >=0 and sum w(i) = eqc


[w, lambda] = nnRegress(x,y,iqc,eqc)
% soultion is of form y approx x*w
% lambda are the lagrange multipliers due to the constraints
% can be shown that the solution w takes the followin form
w2 = inv(x'*x)*(x'*y - lambda) 