function [dy] = observationDynamicsTest(x,dt)
% dY_t =  X_t dt + h* dB_t
h=0.1;
g=1;

% make the dfrift function into a 6x1 vector
dy =  g*x *dt +  h*sqrt(dt)*randn(1);




