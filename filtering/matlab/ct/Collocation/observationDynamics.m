function [dy] = observationDynamics(x,dt)
% dY_t =  X_t dt + h* dB_t
h=0.0;


% make the dfrift function into a 6x1 vector
dy =  x *dt+  h*sqrt(dt)*randn(1);




