function [dx, Jx, Ju] = systemDynamicsTest(x,u,dt)
% dX_t = - X_t dt + U_t dt + c dB_t
c=1;
b=0;
a=-1;

% make the dfrift function into a 6x1 vector
dx = a*x*dt + b*u*dt +c*sqrt(dt)*randn(1);

% The jacobian of the drift with respect to u and x

Ju= b;

Jx= a;



