% linear regression with equality an inequality constraints 
% each weight w(i) has to be smaller than p(i)
% and they have to sum to q

function [w, lambda] = nnRegress(x,y,p,q,w0)
% our goal is to find x that minimizes 0.5(xw -y)'(xw-y)
% with constraint x(i) \geq p(i), \sum x(i) =q
d = size(x,2);


%first put 0.5 (xw - y)' (xw-u) in the form 0.5 w' h w + f'w 
% thus h = x'x, f = -x'y

h = x'*x ;
%h = h + eye(length(h)); %ridge term
f = - x'*y;

% next we put the inequality constraints in the form a x <= b
a = -eye(d);
b= p;



% next we put the equality constraint in the form c x = d
c = ones(1,d);
d = q;

opts = optimset('Display','off','LargeScale','off');
w  = quadprog(h,f,a,b,c,d,[],[],w0,opts);


% the lagrange multipliers added for equality and inequality constraints
lambda = x'*y -x'*x*w ;
% the actual solution is a standard least squares on 
% (xw -y )'(xw-y) + lambda'*x
