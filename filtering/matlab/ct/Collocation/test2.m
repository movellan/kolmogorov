clear
clf

%%%%%%%%%%%% Begin Problem Description %%%%%%%%%%%%%%
% use collocation method to solve Kushner Filtering equation
% System is of the form
% dXt = - X_t dt + cdBt 
% dY_t = X_t dt + h dW_t

% We simulate the case in which dY_t follows a sinusoid and examine
% the resulting filtering distribution. 
% Recommended activity: play with the number of features by
% changing o.nphi. 

dt= 1/500; % sampling time in seconds	       
T=20; % simulation time in secs
c=1;
h=0.002;


mx0=[0]; %prior mean
s0 = 1; % prior variance
xi= -6:0.05:6; % collocation  points


%%%%%% features for approximator 
o.nd = 1;% dimensionality of input
o.nphi=2; % number of features. Play with this it is fun.

for k=1:o.nphi
  o.gMean{k}=0;
  if o.nphi>1
    o.gMean{k} =-2+ (k-1)*4/(o.nphi-1);
  end
  
  o.gPrecission{k} = 1;
end


w = rand(o.nphi,1);
w=ones(o.nphi,1);
w = w/sum(w);

%%%%%%%%%%% End of Problem Description %%%%%%%%%%%%%



%xi=xi';
nt  = ceil(T/dt); 
for t=1: nt
  t*dt
  [f, gf,hf]=Phi(o,xi);
  
  f=f';
  gf = squeeze(gf);
  gf = gf';
  hf =cell2mat(hf);
  hf = hf';


  p{t} = f*w;
  gp{t} = gf*w;
  hp{t} = hf*w;
  m(t) = o.gMean{1}*w(1)+o.gMean{2}*w(2); 
  
  x= -(gf.*repmat(w',length(xi),1))*dt; % derivative with respect to m is - deriviative with
          % respect to x
  y=(p{t} + xi'.*gp{t} +0.5*c*c*hp{t})*dt;
  dy = 2*sin(5*pi*2*t*dt)*dt;
  dI= (xi'*dt - m(t)*dt).*(dy - m(t)*dt)/(h^2);
  y = y+dI.*p{t};
  dotTheta = inv(x'*x+0.0001*eye(o.nphi))*x'*y;
  for k=1:o.nphi
    o.gMean{k} = o.gMean{k} + dotTheta(k)*dt;
    o.gMean{k} = o.gMean{k} + dotTheta(k)*dt;
  end
  for k=1:o.nphi
    fw(:,k) = f(:,k)*w(k);
  end
  
  plot(xi,fw,'r--')
  hold on
  plot(xi,p{t},'b','lineWidth',4)
  ylim([0 0.5])
  hold off
  drawnow
end
