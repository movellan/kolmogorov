function varargout = Phi(o,x)   % compute features, and jacobian of x
% x has to be o.nx rows by  o.samples cols
      
        % returns value of gaussian rbf, gradient and hessian
        % x = n *1 state
        % mu = n * k mean (one mean per Gaussian}
        % nu = n * n precission matrix
        
        n = o.nphi; 

        


        
        for kp=1:o.nphi
            mu = o.gMean{kp};
            nu = o.gPrecission{kp};
	   
	    z = sqrt(det(o.gPrecission{kp}))/(sqrt(2*pi)^o.nd);
            ns = size(x,2);
            for ks=1:ns
                d = x(:,ks)-mu;
                g= exp( -0.5*d'*nu*d);% feature
		g=g*z;
                phi(kp,ks) =g;
                if nargout > 1
                    JPhi(kp,:,ks) = g*nu*(-d); %Jacobian
                end
                if nargout >2
                    t= nu*d;
                    d2phi{kp,ks} =  g*(t*t'  - nu);
                    % feature hessian
                    % note we have one Hessian for each combination of
                    % sample and feature within that sample. 
                end
                
            end %ks
        end %kp
        
        varargout{1} = phi;
        if nargout >1
            varargout{2} = JPhi;
        end
        if nargout >2
            varargout{3} = d2phi;
          
        end
        

end

