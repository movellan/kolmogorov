clear
clf

%%%%%%%%%%%% Begin Problem Description %%%%%%%%%%%%%%
% use collocation method to solve Kushner Filtering equation
% System is of the form
% dXt = - X_t dt + cdBt 
% dY_t = X_t dt + h dW_t

% We simulate the case in which dY_t follows a sinusoid and examine
% the resulting filtering distribution. 
% Recommended activity: play with the number of features by
% changing o.nphi. 

dt= 1/1000; % sampling time in seconds	       
T=2000; % simulation time in secs
c=sqrt(2);
h=0.005;


mx0=[-3]; %prior mean
s0 = 1; % prior variance
xi= -10:0.1:10; % collocation  points


%%%%%% features for approximator 
o.nd = 1;% dimensionality of input
o.nphi=20; % number of features. Play with this it is fun.
for k=1:o.nphi
  o.gMean{k} =-3+ (k-1)*6/(o.nphi-1);
  o.gPrecission{k} = 10;
end

%%%%%%%%%%% End of Problem Description %%%%%%%%%%%%%

gMean= cell2mat(o.gMean)';
gMean2 = gMean.^2;
gVar = cell2mat(o.gPrecission)';
gVar = 1./gVar;

[f, gf,hf]=Phi(o,xi);

f=f';
gf = squeeze(gf)';
hf =cell2mat(hf);
hf = hf';



p0 = (xi-mx0);
p0 = exp(-0.5*p0.*p0/s0);
p0=p0/sqrt(2*pi*s0);

p0=p0';
xi=xi';


[w{1}, lambda] = nnRegress(f,p0,zeros(o.nphi,1),1,zeros(o.nphi,1));

m2(1) = mx0;
v2(1) = s0;
nt  = ceil(T/dt); 
for t=1: nt
  
  p{t} = f*w{t};
  gp{t} = gf*w{t};
  hp{t} = hf*w{t};
  m(t) = gMean'*w{t};
  v(t) = gVar'*w{t}+ gMean2'*w{t} - m(t)^2;

  for k=1:o.nphi
    fw(:,k) = f(:,k)*w{t}(k);
  end
  
  plot(xi,p{t},'b','lineWidth',2);
  hold on
  plot(xi,fw,'r--');
  ylim([0 0.6])
  xlim([-5 5])
  xlabel('X')
  ylabel('p(X)')
  hold off
  drawnow

  
  x= f;
  y=(p{t} + xi.*gp{t} +0.5*c*c*hp{t})*dt;
  dy = 2*sin(5*pi*2*t*dt)*dt;
%  dI= (-xi*dt + m(t)*dt).*(dy + m(t)*dt)/(h^2);
  dI= (xi*dt - m(t)*dt).*(dy - m(t)*dt)/(h^2);
  y = y+dI.*p{t};
  [dw, lambda] = nnRegress(x,y, w{t},0,zeros(o.nphi,1));

    
  w{t+1} = w{t}+dw;

  m2(t+1) = m2(t) - m2(t)*dt;
  v2(t+1) = v2(t) - 2*v2(t)*dt +c*c*dt;
  
end
