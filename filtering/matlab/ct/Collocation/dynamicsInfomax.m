function [xdot, Jx, Ju] = dynamicsInfomax(x,u)
% Original model is 
%dXt = Ut + c dBt
%dYt =  exp(-0.5*(X_t - alpha)'beta(X_t -alpha)') X_t + h  dWt
% This result on following EKF model
% d m = u dt  + K dIt
% ds=(cc' - exp(-(m - alpha)' beta (m-alpha))  s f (hh')^{-1} f's
% where m = xhat is the posterior mean
% s is the posterior covariance matrix
% f = I - beta (m-alpha)m'

c=0.1*eye(2);
alpha=[1;1];
beta=2*[1 0.2; 0.2 1];
h = eye(2);
hi = inv(h*h');
% brake the 6 dimensional vector into a 2d xhat vector
% and a 2x2 variance matrix s
xhat = x(1:2);
s = x(3:6);
s= reshape(s,2,2);

% linear approximation for observation drift
f= eye(2) - beta*(xhat -alpha)*xhat';
% simpler than linear approximation. works as well?
% f= eye(2);

% the drift function for xhat and for s
xhatDot = u;
g= exp(-(xhat - alpha)'*beta*(xhat-alpha));
sdot = c*c' - g*s*f*hi*f'*s';
% make the dfrift function into a 6x1 vector
xdot = [xhatDot; reshape(sdot,4,1)];

% The jacobian of the drift with respect to u
% it should be a 6x2 matrix
Ju= [eye(2,2);zeros(4,2)];

% ensemble the jacobian of the drift with respect to x
Jxx =zeros(2,2); % the xhat part with respect to xhat
Jxs = zeros(2,4);% the xhat part with respect to s 
Jsx= 2*g*reshape(s*s',4,1)*((xhat-alpha)'*beta); 
% here we use some nice matrix calculus to help us out
Jss = - g*2*kron(eye(2),s); % s with respect to s
% put it all together into a 6x6 jacobian matrix
Jx=[Jxx, Jxs;Jsx,Jss];



