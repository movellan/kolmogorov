function [r, p1,p2,xi,q,omega] = rewardInfomax(x,u,trial)
% we assume reward is of the form 
% r(x,u) = r_1(x) + r_2(u)
% we approximate each component as follows
% r_1(x) approx    -(x - xi)' p (x-xi) +c
% r_2(x) approx    -(u - omega)' q(u-omega) + c
% p = -1/2 H^{-1}_x
% xi = x - H^{-1}_x G_x
% q = -1/2 H^{-1}_u
% omega =  u - H_u G_u
% where H_x is Hessian of r_1 with respect to x
%       G_x is the gradient of r_1 with respect to x

q = 0.5*eye(2);
p= 1*eye(2);

p1= [p, zeros(2,4); zeros(4,6)];
p2 = reshape(p,4,1);
p2=-0.5*[0;0;p2];

r = -u'*q*u - x'*p1*x +2* p2'*x;


xi=zeros(6,1);
omega=zeros(2,1);


