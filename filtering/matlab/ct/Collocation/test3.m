clear
clf

%%%%%%%%%%%% Begin Problem Description %%%%%%%%%%%%%%
% use collocation method to solve Kushner Filtering equation
% System is of the form
% dXt = - X_t dt + cdBt 
% dY_t = X_t dt + h dW_t

% We simulate the case in which dY_t follows a sinusoid and examine
% the resulting filtering distribution. 
% Recommended activity: play with the number of features by
% changing o.nphi. 

dt= 1/1000; % sampling time in seconds	       
T=2000; % simulation time in secs
c=1;
h=0.01;


mx0=[0]; %prior mean
s0 = 1; % prior variance
xi= -6:0.1:6; % collocation  points


%%%%%% features for approximator 
o.nd = 1;% dimensionality of input
o.nphi=5; % number of features. Play with this it is fun.
for k=1:o.nphi
   o.gMean{k} = 0;
  if o.nphi>1
    o.gMean{k} =  -4+ (k-1)*8/(o.nphi-1);
  end
  
  o.gPrecission{k} = 6;
end

%%%%%%%%%%% End of Problem Description %%%%%%%%%%%%%

Mean= cell2mat(o.gMean)';
Mean2 = Mean.^2;
gVar = cell2mat(o.gPrecission)';
gVar = 1./gVar;

[f, gf,hf]=Phi(o,xi);

f=f';
if(size(gf,1)< size(gf,2))
  gf = gf';
end

hf =cell2mat(hf);
hf = hf';



p0 = (xi-mx0);
p0 = exp(-0.5*p0.*p0/s0);
p0=p0/sqrt(2*pi*s0);

p0=p0';
%xi=xi';


[w{1}, lambda] = nnRegress(f,p0,zeros(o.nphi,1),1,zeros(o.nphi,1));

m2(1) = mx0;
v2(1) = s0;
nt  = ceil(T/dt); 
dy=0;
for t=1: nt
  [f, gf,hf]=Phi(o,xi);
  f=f';
  gf = squeeze(gf);
  if(size(gf,1)< size(gf,2))
    gf = gf';
  end
  
  hf =cell2mat(hf);
  hf = hf';
  
  p{t} = f*w{t};

  gp{t} = gf*w{t};
  hp{t} = hf*w{t};
  Mean= cell2mat(o.gMean)';
  m(t) = Mean'*w{t};
  v(t) = gVar'*w{t}+ Mean2'*w{t} - m(t)^2;

  for k=1:o.nphi
    fw(:,k) = f(:,k)*w{t}(k);
  end
  
  plot(xi,p{t},'b','lineWidth',2);

  hold on
  plot(xi,fw,'r--');
  ylim([0 1])
  xlim([-7 7])
  xlabel('X')
  ylabel('p(X)')
  hold off
  drawnow

  
  x= f;
  y=(p{t} + xi'.*gp{t} +0.5*c*c*hp{t})*dt;

  dy = 0.9*dy+0.1*randn(1)*20*dt;
%  dI= (-xi*dt + m(t)*dt).*(dy + m(t)*dt)/(h^2);
  dI= (xi'*dt - m(t)*dt).*(dy - m(t)*dt)/(h^2);
  y = y+dI.*p{t};
%  [dw, lambda] = nnRegress(x,y, w{t},0,zeros(o.nphi,1));

  
%%%%%%%%%%%%%
dd = size(x,2);
  x2= -(gf.*repmat(w{t}',length(xi),1))*dt; % derivative with respect
                                        % to m is - deriviative
                                        % with

x=[x x2];
%pause
opts = optimset('Display','off','LargeScale','off');
%dw2 =quadprog(x'*x,-x'*y,[-eye(dd) zeros(dd)] ,w{t},[ones(1,dd)
%zeros(1,dd)])
hh = x'*x + 0.00000000000*[zeros(o.nphi) zeros(o.nphi); zeros(o.nphi) eye(o.nphi)];
dw2 =quadprog(hh,-x'*y, [-eye(dd) zeros(dd)] ,w{t},[ones(1,dd) zeros(1,dd)],0,[],[],[],opts);
%dw2


for k=1: o.nphi
  mm(k,t) = o.gMean{k};
  o.gMean{k} = o.gMean{k} + 1*dw2(o.nphi+k)*dt;
  
  
end


%%%%%%%%%%%%%

    
  w{t+1} = w{t}+dw2(1:o.nphi);

  m2(t+1) = m2(t) - m2(t)*dt;
  v2(t+1) = v2(t) - 2*v2(t)*dt +c*c*dt;
  
end
