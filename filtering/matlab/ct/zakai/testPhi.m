clear

o.nd = 1;% dimensionality of input
o.nphi=20; % number of features
o.ns = 10; % number of samples
for k=1:o.nphi
  o.gMean{k} = randn(o.nd,1);
  o.gPrecission{k} = 20*eye(o.nd);
end

%x = randn(o.nd,o.ns);
x=-10:0.1:10;



o.ns=size(x,2);


[f, gf,hf]=Phi(o,x);


y = exp(-4*(x-2).^2) + exp(- 4*(x+1).^2);

%y = exp(sin(2*pi*x));
w = pinv(f*f')*f*y';

yhat = w'*f;
grad = zeros(o.nd,o.ns);
for ks=1:o.ns
  grad(:,ks) = gf(:,:,ks)'*w;
  hess(:,ks) =0;
  for kp=1:o.nphi
    hess(:,ks) = hess(:,ks)+hf{kp,ks}*w(kp);
  end
end
plot(x,y, x,yhat,'--r')