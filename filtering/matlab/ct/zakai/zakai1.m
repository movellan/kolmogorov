% Computes Zakai's Equation doing Brute force sampling

% dXt = a(X_t) dt + c(X_t) dB_t
% dYt = g(X_t) dt + h(Y_t) dW_t
% 

clear; clf

a =10;
c=0.1;
h =1;
freq = 1;
T=2;

particles =40000;
dt =0.05;

% Initialize variables
sqdt = sqrt(dt);
nt = ceil(T/dt);
h2=h*h;

dB= sqdt*randn(particles,nt);
dW= sqdt*randn(particles,nt);
X= zeros(particles,nt);
Z = zeros(particles,nt);

t=[1:nt]*dt;
X(:,1)=2*pi*rand(particles,1)*0;
Z(:,1) = 0;
XTarget = zeros(1,nt);
YTarget = zeros(1,nt);
%note for the no noise case
% Y(t) = \int_0^t sin(2 *pi *freq* s) ds = [ cos(0) - cos(2*pi*freq*t]/(2*pi)

% First let's generate a target sequence
for n=2:nt
  dXTarget = a*dt + c*sqdt*randn;
  XTarget(n) = XTarget(n-1)+ dXTarget;
  dYTarget= sin(2*pi*freq*XTarget(n))*dt + h*sqdt*randn;
  YTarget(n) = YTarget(n-1) + dYTarget;
end
XTarget= mod(XTarget,2*pi);
% Now we do the filtering
for n=2:nt
  dX = a*dt + c*dB(:,n);
  X(:,n) = X(:,n-1)+ dX;
  s = sin(2*pi*freq*X(:,n-1));
  dZ=  (s*(YTarget(n)-YTarget(n-1)) -0.5*s.*s*dt)/h2;
  Z(:,n) = Z(:,n-1)+dZ;
end
L = exp(Z);
Lmean = mean(L,1);
Xprob = mod(X,2*pi);
Xhat = mean(Xprob.*L,1)./Lmean;
nbins=100;
for i=1:nbins
 tmp= ceil(Xprob/(2*pi/nbins));
 tmp(find(tmp==0))=1;
  Phat{i} = (tmp==i);
  PHat(i,:) = mean(Phat{i}.*L,1)./Lmean;
end

  
  
%Xhat = mod(Xhat,2*pi);




subplot(221)
plot(t,YTarget,t,XTarget,'b','lineWidth',2)
hold on 
subplot(222)
%plot(t,Z')
subplot(223)
plot(t,XTarget,t,Xhat)
subplot(224)
imshow(PHat.^0.1)