% Computes Zakai's Equation doing discrete time and space
% approximation 


% dX_t = a(X_t) dt + c(X_t) dB_t
% dY_t = g(X_t) dt + h(Y_t) dW_t
% 
% note at time t I observe dY_t (which basically means I observe Y_{t+dt})
clear; clf

a =-1;
c=1;

h =0.03;

T=4;


dt =0.001;

% Initialize variables
sqdt = sqrt(dt);
nt = ceil(T/dt);


XTarget = ones(1,nt);
YTarget = zeros(1,nt);
dYTarget= zeros(1,nt-1);

% First let's generate a target sequence
for n=2:nt
  dXTarget = a*dt + c*sqdt*randn;
  XTarget(n) = XTarget(n-1)+ dXTarget;
%  dYTarget(n-1)= pi*dt;
  dYTarget(n-1)= XTarget(n-1)*XTarget(n-1)*dt + h*sqdt*randn;
  YTarget(n) = YTarget(n-1) + dYTarget(n-1);
end

% Now we do the filtering
nbins=101;
lowerLimit=-6;
upperLimit=6;
dx = (upperLimit-lowerLimit)/nbins;
x = lowerLimit+dx/2+ [0:nbins-1]*dx; % midpoints


% transition probability matrix of discretized system
q= zeros(nbins,nbins);% transition prob matrix row: target col:
                      % source 
for i=1:nbins
  if i==1
    q(i,i) = 1 - dt*c*c/dx/dx+ dt*(c*c/dx - a)/(2*dx);
    q(i+1,i) = dt*(c*c/dx + a)/(2*dx);
  elseif i == nbins
    q(i,i) = 1 - dt*c*c/dx/dx+dt*(c*c/dx + a)/(2*dx);
    q(i-1,i) = dt*(c*c/dx - a)/(2*dx);
  else
    q(i-1,i) =  dt*(c*c/dx - a)/(2*dx);
    q(i,i) = 1 - dt*c*c/dx/dx;
    q(i+1,i)= dt*(c*c/dx + a)/(2*dx);
  end
  
end

% Initial distribution 
p= zeros(nbins,nt);
tmp = find(x<0);
tmp = tmp(end);
p(tmp+1,1) = 1;
p(tmp,1) = 1;
p(tmp-1,1) = 1;
p(:,1)=1;
p(:,1) = p(:,1)/sum(p(:,1));


L = zeros(nbins,nbins);
u = x.*x*dYTarget(1) - 0.5*x.*x.*x.*x*dt;
u = u/(h*h);
L = diag(exp(u));

%p(:,1) = L*p(:,1);
p(:,1) = p(:,1)/sum(p(:,1));

for t=2:nt-1
  p(:,t) = q*p(:,t-1);


  u = x.*x*dYTarget(t) - 0.5*x.*x.*x.*x*dt;
  u = u/(h*h);
  L = diag(exp(u));
  
%  plot(x,diag(L)); pause
  p(:,t) = L*p(:,t);
  p(:,t) = p(:,t)/sum(p(:,t));
end
p(:,end) = [];

xHat = x*p;
colormap(gray)
imagesc(p)
hold on
plot((XTarget-1)*101/12+60,'r')
plot([1 size(p,2)], [101/2, 101/2])
xlabel('Time','FontSize',24)
ylabel('-X','FontSize',24)
title('Filtering Distribution','FontSize',24)
set(gca,'XTick',[],'YTick',[])