
import numpy as np
import scipy.linalg as la
from kalmanMachines  import *
import matplotlib.pyplot as pl
import math

"""
====
Use a Kalman Filter to solve a simple 2D on-line linear regression problem
get aggregate performance statistics and visualization
===
"""


def ang(v1, v2):
    """ Returns the angle in degrees between 2D vectors 'v1' and 'v2'    """
    ang=math.atan2(v1[1],v1[0]) - math.atan2(v2[1],v2[0])     
    if np.cos(ang)< 0:
        print "FOUND NEGATIVE COSINE"  
    ang=ang*360/2./np.pi
    if ang>270and ang<360.:
        ang = ang-360.
    if ang<-270and ang>-360.:
        ang = ang+360.
    return(ang)



# generate the inputs they are 2-D vectors

inputDim=2
#nExamples=50
nExamples=20
nExperiments=1000
inputMean=np.zeros(inputDim)

# r for pearson correlation coefficent between the two inputs
r=0.85
inputCov=np.array([[1.,r],[r,1.]])





# true wegiths
# beta = np.array([1,2])
beta = np.array([-1,4])
YNoiseVar=0.01*np.array([[1.]])



# create Kalman model
transition_matrix = np.eye(inputDim)
transition_offset = np.zeros([inputDim,1])
observation_matrix = np.zeros([1,inputDim])
observation_offset = [0]
transition_covariance = 0.00*np.eye(inputDim)
observation_covariance = YNoiseVar
initial_state_mean = np.zeros([inputDim,1])

initial_state_covariance = 1.*np.eye(inputDim)



kf=KalmanFilter()


learningRateList= np.zeros((nExperiments,nExamples))
learningAngleList =np.zeros((nExperiments,nExamples))
costFunctionKalmanList=np.zeros((nExperiments,nExamples))
costFunctionKalmanList2=np.zeros((nExperiments,nExamples))
costFunctionSGDList=np.zeros((nExperiments,nExamples))
costFunctionSGDList2=np.zeros((nExperiments,nExamples))
betaEstimateKalmanList=np.zeros((nExperiments,nExamples+1,inputDim))
betaEstimateSGDList=np.zeros((nExperiments,nExamples+1,inputDim))


for experiment in range(nExperiments):
    print experiment

    X=np.random.multivariate_normal(
                inputMean,
                inputCov,nExamples)
    trueY=np.dot(X,beta)
    observedY =  trueY+ np.random.multivariate_normal([0.],YNoiseVar,nExamples).T
    observedY= np.hstack(observedY)

   # initial_state_mean = np.random.normal(0,0.1, (inputDim,1))

    kf.initParameters(
        transition_matrix, observation_matrix, transition_covariance,
        observation_covariance, transition_offset, observation_offset,
        initial_state_mean, initial_state_covariance
    )

    t=0

    betaEstimateKalman=[]
    learningRate=[]
    learningAngle=[]
    costFunctionKalman=[]
    costFunctionKalman2=[]
    meanX=np.reshape(kf.meanX,(inputDim,))
    betaEstimateKalman.append(meanX)

    for t in range(nExamples):
        kf.filter(b=X[t],observation=observedY[t])
        meanX=np.reshape(kf.meanX,(inputDim,))
        d=meanX-beta

        costFunctionKalman.append(0.5*np.dot(d.T,np.dot(inputCov,d)))
        costFunctionKalman2.append(0.5*np.dot(d.T,d))
        betaEstimateKalman.append(meanX)

        lr =la.norm(kf.delta)/la.norm(kf.gradient)
        langle= ang(kf.delta,kf.gradient)
        #print kf.delta, kf.gradient
        learningRate.append(lr)
        learningAngle.append(langle)

   
    betaEstimateKalman=np.array(betaEstimateKalman)
    betaEstimateKalmanList[experiment,:,:]= betaEstimateKalman
    
    # now let's try gradient descent 

    epsilon = 0.28
    betaEstimateSGD=[]
    costFunctionSGD=[]
    costFunctionSGD2=[]
    beta2= np.reshape(initial_state_mean,(inputDim,))

    betaEstimateSGD.append(np.reshape(beta2,(inputDim,)))
    for t in range(nExamples):
        y = observedY[t]
        x=np.reshape(X[t],(inputDim,1))
        yHat = np.dot(beta2.T,x)
   

        beta2= beta2+ epsilon*(y-yHat)*np.reshape(x,(inputDim,))
   
        betaEstimateSGD.append(beta2)
        d=beta2-beta
        
        costFunctionSGD.append(0.5*np.dot(d.T,np.dot(inputCov,d)))
        costFunctionSGD2.append(0.5*np.dot(d.T,d))

    betaEstimateSGD=np.array(betaEstimateSGD)
    betaEstimateSGDList[experiment,:,:]= betaEstimateSGD

    learningRate=np.array(learningRate)
    learningAngle=np.array(learningAngle)
    costFunctionKalman=np.array(costFunctionKalman)
    costFunctionKalman2=np.array(costFunctionKalman2)
    costFunctionSGD=np.array(costFunctionSGD)
    costFunctionSGD2=np.array(costFunctionSGD2)

    learningRateList[experiment,:]=learningRate
    learningAngleList[experiment,:]=learningAngle.flatten()
    costFunctionKalmanList[experiment,:]=costFunctionKalman
    costFunctionKalmanList2[experiment,:]=costFunctionKalman2
    costFunctionSGDList[experiment,:]=costFunctionSGD
    costFunctionSGDList2[experiment,:]=costFunctionSGD2



# pl.plot(betaEstimateSGD[:,0],betaEstimateSGD[:,1])
# pl.scatter(betaEstimateSGD[:,0],betaEstimateSGD[:,1],c='g')
pl.figure(1)
pl.plot(betaEstimateKalman)
pl.plot(betaEstimateSGD,'r-')

pl.show()

pl.figure(2)
learningAngle= learningAngle[~np.isnan(learningAngle)]
pl.hist(learningAngle,bins=20)
pl.show()
pl.figure(3)
learningRate= learningRate[~np.isnan(learningRate)]
pl.loglog(learningRate)
pl.show()

# now let's try to plot the cost function and the trajectory followed by gradient descent and the kalman filter

# the cost function is rho= 0.5*E[(y - yhat)^2] = (0.5*beta-betaHat)' E[XX'] (beta - betaHat)


x,y = np.meshgrid( np.arange(-2.,3., 0.02),np.arange(0,5,0.02) )
u = np.zeros(np.shape(x))
v = np.zeros(np.shape(x))
rho = np.zeros((len(x),len(y)))

for i in range(len(x)):
    for j in range(len(y)):
        z = (np.array([x[i,j],y[i,j]]).reshape(2,1)-beta.reshape(2,1))
        rho[i,j]= 0.5*np.dot(z.T,np.dot(inputCov,z))

pl.figure(4)
pl.contour(x, y, rho,450)
#pl.contour(x, y, rho,450,extent=[-2,3,0,5])
pl.plot([beta[0]-0.1,beta[0]+0.1 ],[beta[1],beta[1]])
pl.plot([beta[0],beta[0] ],[beta[1]-0.1,beta[1]+0.1])
# pl.plot(betaEstimateSGD[:,0],betaEstimateSGD[:,1],'b')
# pl.scatter(betaEstimateSGD[0:5,0],betaEstimateSGD[0:5,1],c='b')
# pl.plot(betaEstimateKalman[:,0],betaEstimateKalman[:,1],'r')
# pl.scatter(betaEstimateKalman[0:5,0],betaEstimateKalman[0:5,1],c='r')

u = np.mean(betaEstimateKalmanList,axis=0)
#pl.plot(u[:,0],u[:,1],'r')
pl.plot(u[:,0],u[:,1],'r',marker='o',markerfacecolor='r')
#pl.scatter(u[:,0],u[:,1],c='r')

v=np.mean(betaEstimateSGDList,axis=0)   
pl.plot(v[:,0],v[:,1],'k',marker='o',markerfacecolor='k')
#pl.scatter(v[:,0],v[:,1])
pl.axis('image')
pl.xlim((-2,3))
pl.ylim((0,5))
pl.show()
# use this metric to optimize the learning rate
print np.percentile(costFunctionSGDList2[:,-1],90), np.mean(costFunctionSGDList2[:,-1])