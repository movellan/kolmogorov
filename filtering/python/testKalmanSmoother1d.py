

from kalmanSmoother1d import *
from numpy import *
from  matplotlib.pyplot import *

# the kalman smoother does both smoothing and interpolation. here we test how these two work in practice

# first lets's test smoothing


t = range(5*60*30) # five mins at 30 frames per second
t = array(t)/30.0

qt = t[0:-1:30] #query times are in the observation times. thus. no interpolation



f= 1/60.0 # frequency in hertz
yClean= sin(2.0*pi*f*t +10/30.0)
y=yClean+0.5*random.normal(0,1,len(t)) # observations





# we try three forms of smoothing. 2 of them practically zero and the other one large smoothing. we note that the 2 forms of zero smoothing behave almost identical

s=1e-02 #smoothing constat very small, this pretty much gets the observations at the
f= kalmanSmoother1d(y,t,s)
[ysMean,ysVar] = f.smoothAt(qt)

s=1e-9 #smoothing constat very small, this pretty much gets the observations at the
f= kalmanSmoother1d(y,t,s)
[ys2Mean,ys2Var] = f.smoothAt(qt)


s=1000 # try another smoothing constant
[ys3Mean,ys3Var] = kalmanSmoother1d(y,t,s).smoothAt(qt)
# same smoothing constant but using a kalman filter
[yf3Mean,yf3Var] = kalmanSmoother1d(y,t,s).filterAt(qt)

subplot(311)

plot(qt,ysMean,'b',linewidth=0.5)
plot(qt,ys2Mean,'r--',linewidth=1)

plot(t,yClean,'k',linewidth=1)

plot(qt,ys3Mean,'g--',linewidth=3)
title('Smoothing')

subplot(312)
plot(qt,ys3Mean,'g--',linewidth=1)
plot(qt,yf3Mean,'r--',linewidth=1)
title('Smoothing vs Filtering')



plot(t,yClean,'k',linewidth=1)



# now we try the interpolation properties

qt = linspace(0,5*60,5*60) #query times the only one identical to the observation time is t=0


f= 1/60.0 # frequency in hertz

# we add no noise to study the effects of interpolation

# we notice that s=10^-2 behaves very similar to s=100 and very different from 10^-9

y= (sin(2.0*pi*f*t +10/30.0)+0.0*random.normal(0,1,len(t))) # observations


s=1e-02 #smoothing constat very small, this pretty much gets the observations at the
f= kalmanSmoother1d(y,t,s)
[ysMean,ysVar] = f.smoothAt(qt)

s=1e-9 #smoothing constat very small, this pretty much gets the observations at the
f= kalmanSmoother1d(y,t,s)
[ys2Mean,ys2Var] = f.smoothAt(qt)


s=100 # try another smoothing constant
[ys3Mean,ys3Var] = kalmanSmoother1d(y,t,s).smoothAt(qt)


subplot(313)
plot(qt,ysMean,'b',linewidth=0.5)
plot(qt,ys2Mean,'r--',linewidth=1)
plot(qt,ys3Mean,'g--',linewidth=1)
title('Interpolation')
show()
