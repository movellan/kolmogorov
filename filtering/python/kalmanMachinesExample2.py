
import numpy as np
import scipy.linalg as la
from kalmanMachines  import *
import matplotlib.pyplot as pl

"""
====
Use a Kalman Filter to solve a simple on-line linear regression problem
===
"""
def ang(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'    """
    cosang = np.dot(v1.T, v2)/(la.norm(v1)*la.norm(v2))
    theta = np.arccos(cosang)*360./(2.*np.pi)
    theta = theta*np.sign(cosang)
    #sinang = la.norm(np.cross(v1.flatten(), v2.flatten()))
    return theta.flatten()
    #return np.arctan2(sinang, cosang)




# generate the inputs they are 2-D vectors

inputDim=2
nExamples=200
inputMean=np.zeros(2)

# r for pearson correlation coefficent between the two inputs
r=0.99
inputCov=np.array([[1.,r],[r,1.]])



X=np.random.multivariate_normal(
                inputMean,
                inputCov,nExamples)


# true wegiths
beta = np.array([1,2])
YNoiseVar=0.0001*np.array([[1.]])
trueY=np.dot(X,beta)
observedY =  trueY+ np.random.multivariate_normal([0.],YNoiseVar,nExamples).T
observedY= np.hstack(observedY)

# create Kalman model
transition_matrix = np.eye(inputDim)
transition_offset = np.zeros([inputDim,1])
observation_matrix = np.zeros([1,inputDim])
observation_offset = [0]
transition_covariance = 0.00*np.eye(inputDim)
observation_covariance = YNoiseVar
initial_state_mean = np.zeros([inputDim,1])
initial_state_covariance = 100.*np.eye(inputDim)



kf=KalmanFilter()
kf.initParameters(
    transition_matrix, observation_matrix, transition_covariance,
    observation_covariance, transition_offset, observation_offset,
    initial_state_mean, initial_state_covariance
)

t=0

betaEstimate=[]
learningRate=[]
learningAngle=[]
meanX=np.reshape(kf.meanX,(2,))
betaEstimate.append(meanX)
for t in range(nExamples):
    kf.filter(b=X[t],observation=observedY[t])
    meanX=np.reshape(kf.meanX,(2,))
    betaEstimate.append(meanX)

    lr =la.norm(kf.delta)/la.norm(kf.gradient)
    langle= ang(kf.delta,kf.gradient)
    learningRate.append(lr)
    learningAngle.append(langle)

pl.figure()
#lines_filt = pl.plot(betaEstimate)
betaEstimate=np.array(betaEstimate)
# pl.plot(betaEstimate[:,0],betaEstimate[:,1])
# pl.scatter(betaEstimate[:,0],betaEstimate[:,1])

# now let's try gradient descent 

epsilon = 0.1
betaEstimateGradDescent=[]
beta2= initial_state_mean
betaEstimateGradDescent.append(np.reshape(beta2,(2,)))
for t in range(nExamples):
	y = observedY[t]
	x=np.reshape(X[t],(2,1))
	yHat = np.dot(beta2.T,x)
	beta2= beta2+ epsilon*(y-yHat)*x
	betaEstimateGradDescent.append(np.reshape(beta2,(2,)))

betaEstimateGradDescent=np.array(betaEstimateGradDescent)
learningRate=np.array(learningRate)
learningAngle=np.array(learningAngle)

# pl.plot(betaEstimateGradDescent[:,0],betaEstimateGradDescent[:,1])
# pl.scatter(betaEstimateGradDescent[:,0],betaEstimateGradDescent[:,1],c='g')
pl.figure(1)
pl.plot(betaEstimate)
pl.plot(betaEstimateGradDescent,'r-')

pl.show()

pl.figure(2)
learningAngle= learningAngle[~np.isnan(learningAngle)]
pl.hist(learningAngle,bins=20)
pl.show()
pl.figure(3)
learningRate= learningRate[~np.isnan(learningRate)]
pl.loglog(learningRate)
pl.show()

# now let's try to plot the cost function and the trajectory followed by gradient descent and the kalman filter

# the cost function is rho= 0.5*E[(y - yhat)^2] = (0.5*beta-betaHat)' E[XX'] (beta - betaHat)


x,y = np.meshgrid( np.arange(-1+beta[0],2.1+beta[0],0.02),np.arange(-2+beta[1],1.1+beta[1],0.02) )
u = np.zeros(np.shape(x))
v = np.zeros(np.shape(x))
rho = np.zeros((len(x),len(y)))

for i in range(len(x)):
    for j in range(len(y)):
        z = (np.array([x[i,j],y[i,j]]).reshape(2,1)-beta.reshape(2,1))
        rho[i,j]= 0.5*np.dot(z.T,np.dot(inputCov,z))

pl.figure(4)
pl.contour(x, y, rho,40)
pl.plot([beta[0]-0.1,beta[0]+0.1 ],[beta[1],beta[1]])
pl.plot([beta[0],beta[0] ],[beta[1]-0.1,beta[1]+0.1])
pl.plot(betaEstimateGradDescent[:,0],betaEstimateGradDescent[:,1],'b')
pl.scatter(betaEstimateGradDescent[0:5,0],betaEstimateGradDescent[0:5,1],c='b')
pl.plot(betaEstimate[:,0],betaEstimate[:,1],'r')
pl.scatter(betaEstimate[0:5,0],betaEstimate[0:5,1],c='r')

pl.show()
