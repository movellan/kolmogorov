 
import numpy as np
import scipy.linalg as la
from kalmanMachines  import *
import matplotlib.pyplot as pl
import math
from loadMnist import *

"""
====
Use a Kalman Filter to solve a simple on-line linear regression problem
get aggregate performance statistics in this case we use nd rather than 2d
===
"""

def trainSetCost(w,X,Y):
    cost =0
    for k in range(len(Y)):
        yHat= np.dot(w.T,X[k])
        eHat = observedY[k]-yHat
        cost = cost+ 0.5*(eHat**2.)
    return cost/(len(Y)+0.)

def trainSetCost2(w,X,Y):
    cost =0
    for k in range(len(Y)):
        yHat= np.dot(w.T,X[k])
        if (np.sign(yHat) != np.sign(Y[k])):
            cost = cost+ 1.
    return 100.*cost/(len(Y)+0.)


def cosang(v1, v2): # this only works for inpudDim=2
    """ Returns the cos  angl between vectors 'v1' and 'v2'    """

    ip = np.dot(v1.T,v2).reshape(1,)
    if ip<0:
        print "Found negative inner product"
    n1 = la.norm(v1)
    n2= la.norm(v2)
    if min(n1,n2)< 0.000000000001:
        return 0. 
    else:
        return ip/(n1*n2)
    

# load mnist datase
dataset='mnist.pkl.gz'
datasets = load_data(dataset)

train_set_x, train_set_y = datasets[0]
valid_set_x, valid_set_y = datasets[1]
test_set_x, test_set_y = datasets[2]
xMnist = train_set_x.get_value() # gets the nd numpy arra
yMnist = train_set_y.eval()

x1=xMnist[yMnist==1,:]
#x1=x1[0:1000]
x7=xMnist[yMnist!=5]
#x7=x7[0:1000]
x17=np.vstack((x1,x7))
###############

# generate the inputs they are 2-D vectors

inputDim=784  
#nExamples=50
nExamples=2000
nExperiments=1
inputMean=np.zeros(inputDim)

# r for pearson correlation coefficent between the two inputs
r=0.85
inputCov = np.eye(inputDim)
#inputCov=np.array([[1.,r],[r,1.]])





# true wegiths
# beta = np.array([1,2])
#beta = np.array([-1,4])
beta = np.array(range(inputDim))

YNoiseVar=1.*np.array([[1.]])



# create Kalman model
transition_matrix = np.eye(inputDim)
transition_offset = np.zeros([inputDim,1])
observation_matrix = np.zeros([1,inputDim])
observation_offset = [0]
transition_covariance = 0.00*np.eye(inputDim)
observation_covariance = YNoiseVar
initial_state_mean = np.zeros([inputDim,1])

initial_state_covariance = 1.*np.eye(inputDim)



kf=KalmanFilter()


learningRateList= np.zeros((nExperiments,nExamples))
learningAngleList =np.zeros((nExperiments,nExamples))
costFunctionKalmanList=np.zeros((nExperiments,nExamples))
costFunctionKalmanList2=np.zeros((nExperiments,nExamples))
costFunctionSGDList=np.zeros((nExperiments,nExamples))
costFunctionSGDList2=np.zeros((nExperiments,nExamples))
betaEstimateKalmanList=np.zeros((nExperiments,nExamples+1,inputDim))
betaEstimateSGDList=np.zeros((nExperiments,nExamples+1,inputDim))


for experiment in range(nExperiments):
    print experiment

    X=np.random.multivariate_normal(
                inputMean,
                inputCov,nExamples)
    X=x17
    trueY=np.dot(X,beta)
    trueY[:]=-1
    trueY [0:len(x1)]=1
 
    observedY =  trueY
    observedY= np.hstack(observedY)

   # initial_state_mean = np.random.normal(0,0.1, (inputDim,1))

    kf.initParameters(
        transition_matrix, observation_matrix, transition_covariance,
        observation_covariance, transition_offset, observation_offset,
        initial_state_mean, initial_state_covariance
    )

    t=0

    betaEstimateKalman=[]
    learningRate=[]
    learningAngle=[]
    costFunctionKalman=[]
    costFunctionKalman2=[]
    meanX=np.reshape(kf.meanX,(inputDim,))

    from random import randint
    for t in range(nExamples):
        print "example", t
        rt = randint(0,len(X)-1)
        kf.filter(b=X[rt],observation=observedY[rt])
        meanX=np.reshape(kf.meanX,(inputDim,))
        
        c= trainSetCost(kf.meanX,X,observedY)
        costFunctionKalman.append(c)
        c= trainSetCost2(kf.meanX,X,observedY)
        costFunctionKalman2.append(c)
        
        


        lr =la.norm(kf.delta)/la.norm(kf.gradient)
        langle= cosang(kf.delta,kf.gradient)
        #print kf.delta, kf.gradient
        learningRate.append(lr)
        learningAngle.append(langle)

   

    

   

    learningRate=np.array(learningRate)
    learningAngle=np.array(learningAngle)
    costFunctionKalman=np.array(costFunctionKalman)
    costFunctionKalman2=np.array(costFunctionKalman2)
   

    


pl.figure(1)
learningRate= learningRate[~np.isnan(learningRate)]
pl.loglog(learningRate)
pl.show()

pl.figure(2)
betaHat=kf.meanX
pl.imshow(betaHat.reshape(28,28))
pl.show()

