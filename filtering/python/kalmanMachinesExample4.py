 
import numpy as np
import scipy.linalg as la
from kalmanMachines  import *
import matplotlib.pyplot as pl
import math

"""
====
Use a Kalman Filter to solve a simple on-line linear regression problem
get aggregate performance statistics in this case we use nd rather than 2d
===
"""



def cosang(v1, v2): # this only works for inpudDim=2
    """ Returns the cos  angl between vectors 'v1' and 'v2'    """

    ip = np.dot(v1.T,v2)
    if ip<0:
        print "Found negative inner product"
    n1 = la.norm(v1)
    n2= la.norm(v2)
    if min(n1,n2)< 0.000000000001:
        return(0.)
    else:
        return ip/(n1*n2)
    


# generate the inputs they are 2-D vectors

inputDim=784  
#nExamples=50
nExamples=110
nExperiments=1
inputMean=np.zeros(inputDim)

# r for pearson correlation coefficent between the two inputs
r=0.85
inputCov = np.eye(inputDim)
#inputCov=np.array([[1.,r],[r,1.]])





# true wegiths
# beta = np.array([1,2])
#beta = np.array([-1,4])
beta = np.array(range(inputDim))

YNoiseVar=0.01*np.array([[1.]])



# create Kalman model
transition_matrix = np.eye(inputDim)
transition_offset = np.zeros([inputDim,1])
observation_matrix = np.zeros([1,inputDim])
observation_offset = [0]
transition_covariance = 0.00*np.eye(inputDim)
observation_covariance = YNoiseVar
initial_state_mean = np.zeros([inputDim,1])

initial_state_covariance = 1.*np.eye(inputDim)



kf=KalmanFilter()


learningRateList= np.zeros((nExperiments,nExamples))
learningAngleList =np.zeros((nExperiments,nExamples))
costFunctionKalmanList=np.zeros((nExperiments,nExamples))
costFunctionKalmanList2=np.zeros((nExperiments,nExamples))
costFunctionSGDList=np.zeros((nExperiments,nExamples))
costFunctionSGDList2=np.zeros((nExperiments,nExamples))
betaEstimateKalmanList=np.zeros((nExperiments,nExamples+1,inputDim))
betaEstimateSGDList=np.zeros((nExperiments,nExamples+1,inputDim))


for experiment in range(nExperiments):
    print experiment

    X=np.random.multivariate_normal(
                inputMean,
                inputCov,nExamples)
    trueY=np.dot(X,beta)
    observedY =  trueY+ np.random.multivariate_normal([0.],YNoiseVar,nExamples).T
    observedY= np.hstack(observedY)

   # initial_state_mean = np.random.normal(0,0.1, (inputDim,1))

    kf.initParameters(
        transition_matrix, observation_matrix, transition_covariance,
        observation_covariance, transition_offset, observation_offset,
        initial_state_mean, initial_state_covariance
    )

    t=0

    betaEstimateKalman=[]
    learningRate=[]
    learningAngle=[]
    costFunctionKalman=[]
    costFunctionKalman2=[]
    meanX=np.reshape(kf.meanX,(inputDim,))
    betaEstimateKalman.append(meanX)

    for t in range(nExamples):
        print "example", t
        kf.filter(b=X[t],observation=observedY[t])
        meanX=np.reshape(kf.meanX,(inputDim,))
        d=meanX-beta

        costFunctionKalman.append(0.5*np.dot(d.T,np.dot(inputCov,d)))
        costFunctionKalman2.append(0.5*np.dot(d.T,d))
        betaEstimateKalman.append(meanX)

        lr =la.norm(kf.delta)/la.norm(kf.gradient)
        langle= cosang(kf.delta,kf.gradient)
        #print kf.delta, kf.gradient
        learningRate.append(lr)
        learningAngle.append(langle)

   
    betaEstimateKalman=np.array(betaEstimateKalman)
    betaEstimateKalmanList[experiment,:,:]= betaEstimateKalman
    
    # now let's try gradient descent 

    epsilon = 0.001
    betaEstimateSGD=[]
    costFunctionSGD=[]
    costFunctionSGD2=[]
    beta2= np.reshape(initial_state_mean,(inputDim,))

    betaEstimateSGD.append(np.reshape(beta2,(inputDim,)))
    for t in range(nExamples):
        y = observedY[t]
        x=np.reshape(X[t],(inputDim,1))
        yHat = np.dot(beta2.T,x)
        #epsilon= powerLawLearningRate(t)

        beta2= beta2+ epsilon*(y-yHat)*np.reshape(x,(inputDim,))
   
        betaEstimateSGD.append(beta2)
        d=beta2-beta
        
        costFunctionSGD.append(0.5*np.dot(d.T,np.dot(inputCov,d)))
        costFunctionSGD2.append(0.5*np.dot(d.T,d))

    betaEstimateSGD=np.array(betaEstimateSGD)
    betaEstimateSGDList[experiment,:,:]= betaEstimateSGD

    learningRate=np.array(learningRate)
    learningAngle=np.array(learningAngle)
    costFunctionKalman=np.array(costFunctionKalman)
    costFunctionKalman2=np.array(costFunctionKalman2)
    costFunctionSGD=np.array(costFunctionSGD)
    costFunctionSGD2=np.array(costFunctionSGD2)

    learningRateList[experiment,:]=learningRate
    learningAngleList[experiment,:]=learningAngle.flatten()
    costFunctionKalmanList[experiment,:]=costFunctionKalman
    costFunctionKalmanList2[experiment,:]=costFunctionKalman2
    costFunctionSGDList[experiment,:]=costFunctionSGD
    costFunctionSGDList2[experiment,:]=costFunctionSGD2



# pl.plot(betaEstimateSGD[:,0],betaEstimateSGD[:,1])
# pl.scatter(betaEstimateSGD[:,0],betaEstimateSGD[:,1],c='g')
pl.figure(1)
pl.plot(betaEstimateKalman)
#pl.plot(betaEstimateSGD,'r-')

pl.show()

pl.figure(2)
learningRate= learningRate[~np.isnan(learningRate)]
pl.loglog(learningRate)
pl.show()
