"""
=====================================
Kalman Machines
Goal is to provide an SDK for networks of Extended Kalman Filters
for on-line learning. 
Developer Javier R. Movellan
September 2015
Distribute under MIT style license
=====================================

"""


import numpy as np
import scipy.linalg as la
import warnings





class KalmanFilter(object):
    """

        X_{t+1}   &= a_{t} X_{t} + m_{t} + Z_t \\
        Y_{t}     &= b_{t} X_{t} + c_{t} +  W_t 



    Parameters
    ----------
    a=[dim_state,n_dim_state] array-like
    b=[dim_obs,dim_state]

    covZ  =[dim_state, dim_state] 

    covW = [n_dim_obs, n_dim_obs] 

    m=[n_dim_state] or [n_dim_state] \

    c=[dim_obs] 

    priorMeanX: mu1 = [dim_state] n
    priorCovX: sigma1 [
    """
    def __init__(self):
        print ('created Kalman Filter')

    def initParameters(self, a=None, b=None,
            covZ=None, covW=None,
            m=None, c=None,
            priorMeanX=None, priorCovX=None):
      



        self.a = np.array(a)
        self.b = np.array(b)
        self.covZ = np.array(covZ)
        self.covW = np.array(covW)
        self.m = np.array(m)
        self.c = np.array(c)
        self.priorMeanX = np.array(priorMeanX)
        self.priorCovX = np.array(priorCovX)
        self.meanX=np.array(priorMeanX)
        self.covX=np.array(priorCovX)
        self.meanXNext=np.array(priorMeanX)
        self.covXNext=np.array(priorCovX)
        self.kalman_gain=np.array(b).copy()
        self.gradient=np.zeros(np.shape(self.m))
        self.delta=np.zeros(np.shape(self.m))


        self.checkDimensions()
        self.setDimensions()

    def setDimensions(self):
        self.a=np.reshape(self.a,(self.dim_state,self.dim_state))
        self.b=np.reshape(self.b,(self.dim_obs,self.dim_state))
        self.covZ=np.reshape(self.covZ,(self.dim_state,self.dim_state))
        self.covW=np.reshape(self.covW,(self.dim_obs,self.dim_obs))
        self.m=np.reshape(self.m,(self.dim_state,1))
        self.c=np.reshape(self.c,(self.dim_obs,1))
        self.priorMeanX=np.reshape(self.priorMeanX,(self.dim_state,1))
        self.priorCovX=np.reshape(self.priorCovX,(self.dim_state,self.dim_state))
        self.meanX =np.reshape(self.meanX,(self.dim_state,1))
        self.covX =np.reshape(self.covX,(self.dim_state,self.dim_state))
        self.meanXNext =np.reshape(self.meanXNext,(self.dim_state,1))
        self.covXNext =np.reshape(self.covXNext,(self.dim_state,self.dim_state))


    def checkDimensions(self):
        if len(np.shape(self.b))==1:
            colsB=1
        else:
            colsB = np.shape(self.b)[1]
        stateDims=[np.shape(self.a),
            np.shape(self.m)[0],np.shape(self.covZ),
            np.shape(self.priorCovX),np.shape(self.priorMeanX)[0],
            colsB]  
        stateDims=np.hstack(stateDims)

        if np.var(stateDims)>0:
            print 'state transition offset ', np.shape(self.m)[0]
            print 'state transition covariance ', np.shape(self.covZ)
            print 'state prior covariance ', np.shape(self.priorCovX)
            print 'state prior mean',np.shape(self.priorMeanX)[0]
            print 'cols of observation matrix',colsB
            raise ValueError(
            ('inconsitent dimensions in state parameters')
        )
        obsDims=[np.shape(self.b)[0],np.shape(self.covW),np.shape(self.c)]  
        obsDims=np.hstack(obsDims)
        if np.var(obsDims)>0:
            print 'rows of observation matrix ', np.shape(self.b)[0]
            print 'observation covariance ', np.shape(self.covW)
            print 'observation offset', np.shape(self.c)
            raise ValueError(
            ('inconsitent dimensions in state parameters')
        )

        self.dim_state = stateDims[0]
        self.dim_obs = obsDims[0]

        





    def filter(self,a=None,m=None,
        covZ=None,
        b=None,c=None,
        covW=None,observation=None):

    
        if a is not None:
            self.a=np.array(a)

        if m is not None:
            self.m=np.array(m)
        if covZ is not None:
            self.covZ=covZ
        if b is not None:
            self.b = np.array(b)
        if c is not None:
            self.c = np.array(c)
        if covW is not None:
            self.covW=covW
        if observation is not None:
            observation = np.array(observation)
            observation=np.reshape(observation,(self.dim_obs,1))

        self.setDimensions()
       
        #### correction step
        # expected observation
        #print "Observation Matrix=",self.b, " shape", np.shape(self.b)
        mYNext= (
            np.dot(self.b,
                   self.meanXNext)
            + self.c
        )
        #print "mYNext",mYNext
        # expected observation covariance
        covYNext= (
            np.dot(self.b,
                   np.dot(self.covXNext,
                          self.b.T))
            + self.covW
        )
        
        #print "CovYNext=",covYNext, " shape", np.shape(covYNext)
        covXNextYNext = np.dot(self.covXNext,self.b.T)
        #print "CovXNextYNext=",covXNextYNext, " shape", np.shape(covXNextYNext)
        self.kalman_gain = (
            np.dot(covXNextYNext,la.pinv(covYNext))
         )
      

        #print "kalman_gain=",self.kalman_gain, " shape", np.shape(self.kalman_gain)
        if observation is not None:

            delta = np.dot(self.kalman_gain, observation - mYNext)
            self.meanX = self.meanXNext + delta


            #print "meanX=",self.meanX ," shape", np.shape(self.meanX)
            self.covX = (self.covXNext
                - np.dot(self.kalman_gain,
                     np.dot(self.b,
                            self.covXNext))
            )
            self.gradient = np.dot(self.b.T,observation-mYNext)
            self.delta = delta
 # prediction step
        self.meanXNext = (
        np.dot(self.a, self.meanX)
        + self.m
        )

        
        self.covXNext = (
            np.dot(self.a,
                   np.dot(self.covX,
                          self.a.T))
            + self.covZ
        )


    def sample(self, timesteps, initial_state=None):
        """generate a sequence of states and observations
        """
        
        if initial_state is None:
            initial_state = np.random.multivariate_normal(
                np.reshape(self.priorMeanX,(self.dim_state )),
                self.priorCovX,1
            )
            initial_state=np.hstack(initial_state)
        
        states = np.zeros((timesteps, self.dim_state))
      
        observations = np.zeros((timesteps,self.dim_obs))
        for t in range(timesteps):
            if t == 0:
               
                states[t] = initial_state
            else:
                #print "shape a", np.shape(self.a)
                noise = np.random.multivariate_normal(
                    np.zeros(self.dim_obs),
                    self.covZ,1
                ).T
                # print "shape noise",np.shape(noise)
                # print "shape m",np.shape(self.m)
                # print "shape states_",np.shape(states[t-1])
                previousState=np.reshape(states[t-1],(self.dim_state,1))
                state = (
                    np.dot(self.a, previousState)
                    + self.m
                    + noise
                )
                # print "state=",state
                # print "state shape=", np.shape(state)
                state=np.reshape(state,(self.dim_state,))
                #print "state shape=", np.shape(state)
                states[t]=state
                
            # print "c=",self.c    
            # print "shape c ",np.shape(self.c)
            # print "shape covW",np.shape(self.covW)
            noise = np.random.multivariate_normal(
                    np.zeros(self.dim_obs),
                    self.covW,1
                ).T
            # print "shape noise",np.shape(noise)
            # print "noise=",noise
            state=states[t]
            state=np.reshape(state,(self.dim_state,1))
            # print "state=",state
            # print "shape state=",np.shape(state)
            # print 'b=',self.b
            # print 'shape b',np.shape(self.b)
            observation = (
                np.dot(self.b, state)
                + self.c
                + noise
            )
            observations[t]=np.reshape(observation,(self.dim_obs,))

        return (states, observations)

    def unitTest(self):

        a = [[1.]]
        m = [0]
        b = [[1.]]
        c = [0]
        covZ = [[1.]]
        covW = [[1.]]
        priorMeanX = [1.]
        priorCovX = [[1.]]

        kf2 = KalmanFilter()
        kf2.initParameters(a, b, covZ,
            covW, m, c,
            priorMeanX, priorCovX
        )

        ySequence= [0,3,0,3]
        
        expectedMX=np.array([0.5,2.,0.769230,2.14705])
        expectedCovX=np.array([0.5,0.6,0.615384,0.61764])
        mX=[]
        covX=[]
        for y in ySequence:
            kf2.filter(observation=y)
            mX.append(kf2.meanX)
            covX.append(kf2.covX)
        mX=np.hstack(mX)
        covX=np.hstack(covX)  
        mxError=la.norm(mX-expectedMX)
        if mxError<0.0001:
            print 'Means Unit Test 1 Passed'
        else:
            print 'expected meanX:' , expectedMX
            print 'obtained meanX:' , mX
            raise ValueError(
            ('Means Unit Test 1 Failed')
        )
        covxError=la.norm(covX-expectedCovX)
        if covxError<0.0001:
            print 'Covariances Unit Test 1 Passed'
        else:
            print 'expected covX:' , expectedCovX
            print 'obtained covX:', covX
            raise ValueError(
            ('Covariances Unit Test 1 Failed')
        )


        transition_matrix = [[1., -.5], [.5, 1.]]
        transition_offset = [0., 0.]
        observation_matrix = [[1.,2.]] 
        observation_offset = [0]
        transition_covariance = np.eye(2)
        observation_covariance = 1.*np.eye(1) 
        initial_state_mean = [1., -1.]
        initial_state_covariance = [[1,0.], [0., 1]]

        kf = KalmanFilter()
        #kf.unitTest()
        kf.initParameters(
            transition_matrix, observation_matrix, transition_covariance,
            observation_covariance, transition_offset, observation_offset,
            initial_state_mean, initial_state_covariance
        )

        ySequence=[-2.0000,4.5000,1.7500,7.6250]

        expectedCovX= [ 0.83333333, -0.33333333,  1.62371134, -0.67268041,  2.10091403,
               -0.86480151,  2.3040045 , -0.94466248, -0.33333333,  0.33333333,
               -0.67268041,  0.48582474, -0.86480151,  0.56340011, -0.94466248,
                0.59481207]

        expectedMeanX= [ 0.83333333, -1.33333333,  2.84536082,  0.52835052,  0.82367871,
                0.71092617,  2.50481192,  2.32583434]        
        xEstimate=[]
        xcov=[]
        for y in ySequence:
            kf.filter(observation=y)
            meanX=np.reshape(kf.meanX,(2,))
            xEstimate.append(meanX)
            xcov.append(kf.covX)

        obtainedMeanX=np.hstack(xEstimate)
        obtainedCovX=np.hstack(np.hstack(xcov))
        mxError=la.norm(obtainedMeanX-expectedMeanX)
        if mxError<0.0001:
            print 'Means Unit Test 2 Passed'
        else:
            print 'expected meanX:' , expectedMeanX
            print 'obtained meanX:' , obtainedMeanX
            raise ValueError(
            ('Means Unit Test 2 Failed')
        )
        covxError=la.norm(expectedCovX-obtainedCovX)
        if covxError<0.0001:
            print 'Covariances Unit Test 2 Passed'
        else:
            print 'expected covX:' , expectedCovX
            print 'obtained covX:', obtainedCovX
            raise ValueError(
            ('Covariances Unit Test 2 Failed')
        )



