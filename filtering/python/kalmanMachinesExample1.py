
import numpy as np
from kalmanMachines  import *
import matplotlib.pyplot as pl

"""
=====================================
2D Kalman Filtering example
=====================================

"""


transition_matrix = [[1., -0.1], [0.1, 0.99]]
transition_offset = [-0.1, 0.1]
observation_matrix = np.eye(2) 
observation_offset = [1.0, -1.0]
transition_covariance = 0.0*np.eye(2)
observation_covariance = 10.*np.eye(2) 
initial_state_mean = [5, -5]
initial_state_covariance = [[1, 0.1], [-0.1, 1]]


kf = KalmanFilter()
#kf.unitTest()
kf.initParameters(
    transition_matrix, observation_matrix, transition_covariance,
    observation_covariance, transition_offset, observation_offset,
    initial_state_mean, initial_state_covariance
)

timeSteps=200
xSequence,ySequence=kf.sample(timeSteps)

xEstimate=[]
for y in ySequence:
	kf.filter(observation=y)
	meanX=np.reshape(kf.meanX,(2,))
	xEstimate.append(meanX)


pl.figure()
lines_true = pl.plot(xSequence, 'b')
lines_filt = pl.plot(xEstimate, 'r--')
lines_obs=pl.plot(ySequence,'k--')
pl.legend((lines_true[0], lines_filt[0],lines_obs[0]),
          ('true', 'filter estimates','observations'),
          loc='lower right'
)
pl.show()

