
from kalmanSmoother1d import *
from numpy import *

from matplotlib import pyplot as plt


dt=5  # samplign period in secods
t = arange(0,120,dt)


qt = arange(-10,130,1/30.) #query times at 30 fps


# the observed signal is a sinusoid plus noise
f= 1/60.0 # frequency in hertz
y= (sin(2.0*pi*f*t )+0.1*random.normal(0,1,len(t))) # observations


# we try three forms of smoothing. 2 of them practically zero and the other one large smoothing. we note that the 2 forms of zero smoothing behave almost identical


s=5. #smoothing constant. The larger the more we smooth
f= kalmanSmoother1d(y,t,s)
# mean and variance providdd by the smooother
ym,yv = f.smoothAt(qt)


# plot the mean plus minus 0.1 times the standard deviation
ystd=sqrt(yv)

ymin = ym - 0.1*ystd
ymax=ym+0.1*ystd

plt.plot(qt,ym,'b',linewidth=1)
plt.plot(qt,ymin,linewidth=0.5,c='r')
plt.plot(qt,ymax,linewidth=0.5,c='r')
plt.scatter(t,y,s=80)
plt.show()
