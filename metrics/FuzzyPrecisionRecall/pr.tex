%  Below there is  a latex document. After you read the document I will ask you a question. 



\documentclass{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{geometry}
\usepackage{enumitem}
\usepackage{booktabs}
\usepackage{xcolor}
\usepackage{tcolorbox}

\geometry{margin=1in}

\title{Precision and Recall with Uncertain and Categorical Items\\ An LLM approach}
\author{Tiger Team}
\date{March 2025}

\begin{filecontents}{\jobname.bib}
@article{rosch1975family,
  title={Family resemblances: Studies in the internal structure of categories},
  author={Rosch, Eleanor and Mervis, C. Bob},
  journal={Cognitive Psychology},
  volume={7},
  number={4},
  pages={573--605},
  year={1975},
  publisher={Elsevier}
}
\end{filecontents}
\begin{document}

\maketitle

\begin{abstract}
We  propose an approach to compute precision recall between two lists of items when the items could have a categorical structure and could have different levels of uncertainty. The approach relies on implicit knowledge from LLMs to provide an easy to implement scalable solution. We illustrate the approach on the problem of matching lists of food ingredients but the framework is easy to generalize to other problems. 
\end{abstract}

\section{Introduction}
We address the problem of computing precision recall between two lists of items when the items could have a categorical structure and could have different levels of uncertainty. The approach relies on implicit knowledge from LLMs to provide an easy to implement scalable solution. We illustrate the approach on the problem of matching lists of food ingredients but the framework is easy to generalize to other problems. 
\section{Problem Statement}
The goal is to compute precision and recall between two sets:
\begin{itemize}
  \item \(X\): A ground truth set of ingredients to retrieve.
  \item \(Y\): A predicted set of retrieved ingredients.
\end{itemize}


There are three Issues we aim to address in our framework: categorical structure, visual uncertainty and synonyms. 



\subsection{Categorical Structure}
The ingredients may have a hierarchical category structure some being super or sub categories of others, e.g., meat vs chicken vs chicken breast.  We frame our approach on Rosch's Prototype Theory \cite{rosch1975family}.  Prototype theory proposes that we represent categories based on a prototypical example at the basic (ordinate) level, which offers the most efficient and cognitively accessible way to identify a group. Superordinate categories provide broader, more abstract groupings that encompass various basic categories, while subordinate categories denote more specific instances within these basic groups, resulting in a graded and flexible system of categorization. Basic, ordinate level items can be thought as the most used in our daily interactions. Below are examples of ordinate, superordinate and subordinate versions of some popular food ingredients. 


 We handle this issue using  Rosch's Prototype Theory \cite{rosch1975family}.  Prototype Theory proposes that we represent categories based on a prototypical example at the basic (ordinate) level, which offers the most efficient and cognitively accessible way to identify a group. Superordinate categories provide broader, more abstract groupings that encompass various basic categories, while subordinate categories denote more specific instances within these basic groups, resulting in a graded and flexible system of categorization. Basic, ordinate level items can be thought as the most used in our daily interactions. Below are examples of ordinate, superordinate and subordinate versions of some popular food ingredients. Brands of specific ingredients are consider subordinate of the ingredient. 

\begin{table}[ht]
\centering
\begin{tabular}{lll}
\hline
Superordinate      & Ordinate             & Subordinate                      \\
\hline
Fruits             & Apple                & Organic Gala Apple               \\
Fruits             & Banana                &Dole brand bananas             \\
Pasta            & Spagheti           & Italian Spagheti \\
Dairy.             & Cheese            & Cheddar                        \\
Vegetables         & Carrot               & Baby Carrot                      \\
Herbs              & Basil                & Fresh Basil Leaves               \\
Meat               & Beef                 & Organic Ground Beef              \\
Poultry            & Chicken              & Free-range Chicken Breast        \\
Seafood            & Salmon               & Atlantic Salmon Fillet           \\
Dairy              & Milk                 & Organic Whole Milk               \\
Bakery             & Bread                & Sourdough Bread                  \\
Pasta              & Spaghetti            & Whole Wheat Spaghetti            \\
Rice               & White Rice           & Basmati Rice                     \\
Beans              & Black Beans          & Organic Black Beans              \\
Cereals            & Corn Flakes          & Honey Nut Corn Flakes            \\
Snacks             & Potato Chips         & Sea Salt Potato Chips            \\
Condiments         & Ketchup              & Organic Tomato Ketchup           \\
Sauces             & Marinara Sauce       & Traditional Italian Marinara     \\
Spices             & Cinnamon             & Ground Cinnamon                  \\
Oils               & Olive Oil            & Extra Virgin Olive Oil           \\
Vinegar            & Apple Cider Vinegar  & Organic Apple Cider Vinegar      \\
Legumes            & Lentils              & Red Lentils                      \\
Nuts               & Almonds              & Raw Almonds                      \\
Seeds              & Chia Seeds           & Organic Chia Seeds               \\
Dressings          & Ranch Dressing       & Light Ranch Dressing             \\
Sauces             & Soy Sauce            & Low-Sodium Soy Sauce             \\
Cerials            & Oatmeal              & Steel-Cut Oatmeal                \\
Breakfast Foods    & Cereal Bar           & Granola Bar                      \\
Gluten-Free Foods  & Gluten-Free Bread    & Almond Flour Bread               \\
Desserts           & Ice Cream            & Vanilla Ice Cream                \\
Deli               & Sandwich Meat        & Turkey Breast Slices             \\
Juice              & Orange Juice         & Fresh-Squeezed Orange Juice      \\
\hline
\end{tabular}
\caption{Examples of grocery store superordinate, ordinate, and subordinate food categories.}
\label{tab:grocery}
\end{table}

\subsection{Visual Uncertainty}
We model visual uncertainty of an ingredient $x$  by expanding it into a set of $N=5$  ingredients that are visually similar to it. Visual similarity is defined not by the raw appearance of ingredients, but by their visual characteristics when processed in common dishes. That is, two ingredients are considered visually similar if, after typical culinary processing (e.g., cooking, blending, or plating), their appearance—such as texture, color, and presentation—is comparable. 

\begin{itemize}
\item The list shall have $N=5$ ingredients.
\item The original ingredient $x$ shall be the first element in the list. 
\item The ingredients shall be long to at least two different superordinate categories. 
\end{itemize}
For example 
\begin{align}
&similar tuna = [tuna, swordfish, watermelon, beef, mushroom]\\
&similar meat = [meat, cheese, eggplant, chicken, tofu]\\
&similar brie = [brie, camembert, tofu, egg, potato]
\end{align}

\subsection{Synonyms} 
We use an LLM to detect all the synonyms  of an ingredient and to convert them into a unique name. For example, "coriander" may be converted to "cilantro" and "scallions" to "green onions".


\section{Formal Framework}
We use a  probabilistic document retrieval framework and ground our  computation in a space \((\Omega, F, P)\):
\begin{itemize}
  \item \(\Omega\): The set of  basic (ordinate) food. ingredients one can find at supermarkets and restaurant accross he world. 
  \item \(F\): The power set of \(\Omega\).
  \item \(P(S) = |\Omega|\): for all \( \S \in \Omega.\) Thus the probability of the set $S$ is proportional to the numbe of basic ingredients in $S$.
\end{itemize}

\subsection{Metric Definitions}
Let \(X\) (ground truth) and \(Y\) (prediction) be sets of basic ingredients, i.e. $X, Y\in F$.  We define 
\begin{itemize}
  \item \textbf{Precision:} The fraction of retrieved items in \(Y\) that match \(X\):
  \[
  \text{Precision} = \frac{|X \cap Y|}{|Y|}
  \]
  \item \textbf{Recall:} The fraction of ground truth items in \(X\) retrieved by \(Y\):
  \[
  \text{Recall} = \frac{|X \cap Y|}{|X|}
  \]
  \item \textbf{F1 Score:} The harmonic mean of precision and recall:
  \[
  F1 = \frac{2 \cdot \text{Precision} \cdot \text{Recall}}{\text{Precision} + \text{Recall}}
  \]
\end{itemize}

We can get some intuition about how the approach works using some example.s Suppose $X= \{meat\}$ and   $Y= \{chicken\}$. Under the probabilistic retrieval interpretation the goal is to retrieve all the documents related to meat. Instead the model retrieved all the documents related to chicken. So our precision is goo, i.e., all of our documents are related to meat. However our recall is bad, i.e, we did not retrieve documents related to beef, lamb, etc. 

Similarly consider $X = cheese$, $Y = similar chesse$. In this case we were asked to retrieve all our documents related to chicken. Instead we retrieved all the documents related to chicken and to ingredients that look like cheese, e.g., tofu. So  in this case our recall is good, we retrieved all the documents related to chicken, however our precision is bad, i.e., we also retrieved irrelevant documents related to tofu and to other ingredients that may look like chicken.  

\subsection{Overall approach}
The core of the proposed approach is to convert uncertain, subordinate, and superordinate ingredients into lists of basic ingredients. 
\begin{itemize}
\item{\bf Subordinate ingredients:} are converted into the basic category they belong to. For example, 
\begin{align}
\text{Organic Gala Apple} &\to \text{Apple} \nonumber\\
\text{Baby Carrot} &\to \text{Carrot} \nonumber\\
\text{Fresh Basil Leaves} &\to \text{Basil} \nonumber\\
\text{Ground Beef} &\to \text{Beef} \nonumber\\
\text{Free-range Chicken Breast} &\to \text{Chicken} \nonumber\\
\text{Atlantic Salmon Fillet} &\to \text{Salmon} \nonumber
\end{align}

\item{\bf Superordinate ingredients} are  converted into a list of $N$ basic ingredients, where $N$ is a model parameter. From now on we will use $N=5$

\begin{align}
\text{Fruits} &\rightarrow \text{Apple, Banana, Orange, Grapes, Strawberry}  \nonumber\\
\text{Vegetables} &\rightarrow \text{Carrot, Broccoli, Spinach, Bell Pepper, Cucumber} \nonumber\\
\text{Herbs} &\rightarrow \text{Basil, Thyme, Rosemary, Parsley, Cilantro} \nonumber\\
\text{Meat} &\rightarrow \text{Beef, Pork, Lamb, Venison, Bison}  \nonumber\\
\text{Poultry} &\rightarrow \text{Chicken, Turkey, Duck, Quail, Goose}  \nonumber\\
\text{Seafood} &\rightarrow \text{Salmon, Tuna, Shrimp, Cod, Crab}  \nonumber
\end{align}
\end{itemize}



\section{Step by Step Implementation}

Given a ground truth list $X$ and a retrieved list $Y$. Flatten the lists using the following steps:
\begin{itemize}
\item Step 1: Identify  all the ingredients in $X$, $Y$ that have the uncertainty qualifier  "similar". They shall have the word similar in it. Otherwise they are not considered uncertain ingredients.  Expand each of these uncertain ingredients with the word similar in it  into $N=5$ constituents following the rules above. The result of this expansion is the lists $X_2$, $Y_2$.
    \[
    \text{similar tuna} \to [\text{tuna, swordfish, watermelon, beef, mushroom}]
    \]    
\item Step 2: Identify subordinate ingredients and convert them to  ordinate ingredients. The result of this conversion are the lists $X_3, Y_3$.  For example
\begin{align}
medium pasta shells &\to pasta\\\nonumber
Japanese tuna &\to tuna \nonumber\\
brown sugar &\to sugar\nonumber
\end{align}

\item Step 3: Replace each superordinate ingredient with exactly  $N=5$ ordinate ingredients. The result of this replacement determines the lists $X_4, Y_4$ No superordinate ingredients should remain in this list. 
   \begin{align}
    pasta  &\to [\text{[spaghetti, penne, fusilli, farfalle, rigatoni} \\ \\nonumber
    \text{meat} &\to [\text{beef, pork, lamb, venison, bison}]
   \end{align}

\item Step 4: F ind any synonyms and/or plural forms in the $X_4, Y_4$ lists and convert them to a common form. For example, if coriander and cilantro are present in the list, convert them both to cilantro. This results with no synonyms shall be in  the lists $X_5, Y_5$
    \[
    \text{cilantro, coriander, potatoes, potato} \to [\text{coridander, coriander, potato}]
    \]
\item Error Check 1: $X_4$ and $Y_4$ shall only contain ordinate ingredients. Ordinate ingredients are at the basic, most common category level in daily life conversations and usage as describe in Rosch's prototype theory. 

\item Error Check 2: Make sure you have not inconsistencies in your criteria for what is an ordinate ingredient. For example pasta and spaghuetti cannot both be basic. Tuna and fish cannot both be basic.  Be very careful here. 

\item Error Check 3: $X_5 \cup Y_5$ shall have no synonyms. 

\item Compute set sizes and metrics:
\begin{align}
\text{Precision} &= P(X|Y) = \frac{| X_4 \cap Y_4 |}{| Y_4 |} \\
\text{Recall} &= P(Y|X) = \frac{| X_4 \cap Y_4 |}{| X_4 |} \\
F_1 &= \frac{2 \times \text{Precision} \times \text{Recall}}{\text{Precision} + \text{Recall}}
\end{align}
\end{itemize}

\section{Example}

Let 
\[
X = \{\text{meat, similar tuna, coriander}\}, \quad Y = \{\text{chicken, Japanese tuna, cilantro}\}.
\]

\subsection*{Step 1: Expand Uncertain Ingredients}
\begin{itemize}
    \item Expand the uncertain ingredient \textbf{similar tuna} into 5 visually similar items:
    \[
    \text{similar tuna} \to [\text{tuna, swordfish, watermelon, beef, mushroom}]
    \]
\end{itemize}
Thus,
\[
X_2 = [\text{meat, tuna, swordfish, watermelon, beef, mushroom, coriander}],
\]
\[
Y_2 = [\text{chicken, Japanese tuna, cilantro}].
\]

\subsection*{Step 2: Convert to Ordinate Ingredients}
\begin{itemize}
    \item Expand the superordinate ingredient \textbf{meat}:
    \[
    \text{meat} \to [\text{beef, pork, lamb, venison, bison}]
    \]
    \item Convert the subordinate ingredient \textbf{Japanese tuna} to its basic form:
    \[
    \text{Japanese tuna} \to \text{tuna}
    \]
\end{itemize}
Thus,
\[
X_3 = [\text{beef, pork, lamb, venison, bison, tuna, swordfish, watermelon, beef, mushroom, coriander}],
\]
\[
Y_3 = [\text{chicken, tuna, cilantro}].
\]

\subsection*{Step 3: Synonym Resolution}
Convert \textbf{coriander} to the unified term \textbf{cilantro}:
\[
X_4 = [\text{beef, pork, lamb, venison, bison, tuna, swordfish, watermelon, beef, mushroom, cilantro}],
\]
\[
Y_4 = [\text{chicken, tuna, cilantro}].
\]

\subsection*{Step 4: Compute Metrics}
\begin{itemize}
    \item \textbf{Intersection:} 
    \[
    X_4 \cap Y_4 = \{\text{tuna, cilantro}\} \quad (|X_4 \cap Y_4| = 2)
    \]
    \item \textbf{Cardinalities:} 
    \[
    |X_4| = 10, \quad |Y_4| = 3
    \]
    \item \textbf{Precision:}
    \[
    \text{Precision} = \frac{|X_4 \cap Y_4|}{|Y_4|} = \frac{2}{3} \approx 0.667
    \]
    \item \textbf{Recall:}
    \[
    \text{Recall} = \frac{|X_4 \cap Y_4|}{|X_4|} = \frac{2}{10} = 0.2
    \]
    \item \textbf{F1 Score:}
    \[
    F1 = \frac{2 \cdot \text{Precision} \cdot \text{Recall}}{\text{Precision} + \text{Recall}} \approx 0.308
    \]
\end{itemize}


\bibliographystyle{plain}
\bibliography{\jobname}
\end{document}

