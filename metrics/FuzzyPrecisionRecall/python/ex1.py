#!/usr/bin/env python3

def identity(x):
    """Identity operator: returns the set containing only x."""
    return {x}

def F(x):
    """Fuzzy operator F: returns a set of size 2, containing x and x+1."""
    return {x, x + 1}

def G(x):
    """Operator G: returns a set of size 3, containing x, x+2, and x+3."""
    return {x, x + 2, x + 3}

# Map operator names to functions
operator_map = {
    "Identity": identity,
    "F": F,
    "G": G
}

def compute_precision_recall(triplets):
    """
    Given a list of triplets (integer, op_X, op_Y), compute:
      - Ground truth set X (union of op_X(x) for each triplet)
      - Prediction set Y (union of op_Y(x) for each triplet)
      - Their intersection
      - Precision and Recall
    """
    X_set = set()
    Y_set = set()
    for trip in triplets:
        x, op_x, op_y = trip
        # Apply the corresponding operators
        X_i = operator_map[op_x](x)
        Y_i = operator_map[op_y](x)
        X_set.update(X_i)
        Y_set.update(Y_i)

    intersection = X_set.intersection(Y_set)
    precision = len(intersection) / len(Y_set) if Y_set else 0
    recall = len(intersection) / len(X_set) if X_set else 0
    return X_set, Y_set, intersection, precision, recall

def main():
    # Example input: list of triplets (integer, operator for ground truth, operator for prediction)
    triplets = [
        (1, "F", "Identity"),
        (2, "Identity", "G"),
        (3, "G", "F")
    ]

    X_set, Y_set, inter, prec, rec = compute_precision_recall(triplets)
    print("Ground truth set X:", X_set)
    print("Prediction set Y:", Y_set)
    print("Intersection X ∩ Y:", inter)
    print("Precision: {:.3f}".format(prec))
    print("Recall: {:.3f}".format(rec))

if __name__ == "__main__":
    main()
