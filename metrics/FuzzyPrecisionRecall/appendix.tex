\documentclass[12pt]{article}  % Sets the document type and font size

% Preamble: load any packages you need
\usepackage[utf8]{inputenc}    % For UTF-8 support
\usepackage[T1]{fontenc}       % For improved font encoding
\usepackage{lmodern}           % Modern fonts
\usepackage{amsmath, amssymb}  % For math symbols and environments
\usepackage{graphicx}          % To include images
\usepackage{hyperref}          % For clickable links
\usepackage{array}             % For improved table formatting

\title{Precision Recall with Fuzzy and Uncertain Measures}
\author{DAQ Tiger Team}
\date{\today}  % Automatically inserts today's date

\begin{document}

\maketitle  % Generates the title

\begin{abstract}
This document explores precision and recall measures with a focus on incorporating fuzzy and uncertain aspects into the analysis. It includes foundational explanations, mathematical formulations, and examples.
\end{abstract}

\section{Motivating Example}
In this section, we introduce a motivating example to illustrate how uncertainty affects precision and recall calculations. We begin by defining a probability space \((\Omega, \mathcal{F}, P)\).

First, we define the set of outcomes:
\[
\Omega = \{\text{pork},\ \text{chicken},\ \text{beef},\ \text{lamb},\ \text{cilantro},\ \text{lettuce},\ \text{baguette},\ \text{white\_bread}\}
\]

Next, we define a collection of events. The table below now includes:

\begin{table}[h]
  \centering
  \begin{tabular}{|>{\raggedright\arraybackslash}p{3cm}|>{\raggedright\arraybackslash}p{9cm}|}
    \hline
    \textbf{Event Name} & \textbf{Outcomes} \\
    \hline
    meat & pork, chicken, beef, lamb \\
    \hline
    vegetables & cilantro, lettuce \\
    \hline
    breads & baguette, white\_bread \\
    \hline
    like pork & pork, chicken \\
    \hline
    like chicken & pork, chicken \\
    \hline
    like beef & beef, lamb \\
    \hline
    like lamb & beef, lamb \\
    \hline
    like cilantro & cilantro, lettuce \\
    \hline
    like lettuce & cilantro, lettuce \\
    \hline
    like bahete & baguette, white\_bread \\
    \hline
    like white bread & baguette, white\_bread \\
    \hline
    like meat & pork, chicken, beef, lamb, white\_bread \\
    \hline
    like vegetables & lettuce, cilantro, lamb \\
    \hline
    like bread & baguette, white\_bread, chicken, pork \\
    \hline
  \end{tabular}
  \caption{List of Events and their Outcomes}
  \label{tab:events}
\end{table}

Let \(X\) represent the ground truth (the set of items we want to retrieve) and \(Y\) represent the prediction (the set of items retrieved by the model). Recall that:
\[
P(X \mid Y) = \frac{|X \cap Y|}{|Y|} \quad \text{and} \quad P(Y \mid X) = \frac{|X \cap Y|}{|X|}.
\]

\subsection*{Example 1}
Suppose we have:
\[
X = \{\text{pork},\ \text{lettuce},\ \text{white\_bread}\}
\]
and
\[
Y = \{\text{meat},\ \text{vegetable},\ \text{bread}\}.
\]

Convert each element of \(Y\) into its set of elementary items:
\begin{itemize}
  \item \(\text{meat} \to \{\text{pork}, \text{chicken}, \text{beef}, \text{lamb}\}\)
  \item \(\text{vegetable} \to \{\text{cilantro}, \text{lettuce}\}\)
  \item \(\text{bread} \to \{\text{baguette}, \text{white\_bread}\}\)
\end{itemize}

Taking the union, we obtain:
\[
Y = \{\text{pork}, \text{chicken}, \text{beef}, \text{lamb}, \text{cilantro}, \text{lettuce}, \text{baguette}, \text{white\_bread}\}.
\]
Then,
\[
X \cap Y = \{\text{pork}, \text{lettuce}, \text{white\_bread}\},
\]
so that \(|X \cap Y| = 3\), \(|Y| = 8\), and \(|X| = 3\).

Thus, the precision is:
\[
P(X \mid Y) = \frac{3}{8} \approx 0.375,
\]
and the recall is:
\[
P(Y \mid X) = \frac{3}{3} = 1.
\]

{\bf Interpretation:} In this example, \(X\) is the set of items we want to retrieve and \(Y\) is the set of items retrieved by the model after expansion. The model retrieved all of the relevant items (recall is 1), but it also retrieved many additional items not in \(X\) (precision is 0.375).

\subsection*{Example 2}
Now suppose we have:
\[
X = \{\text{like meat},\ \text{like vegetables},\ \text{breads}\}
\]
and
\[
Y = \{\text{beef},\ \text{cilantro},\ \text{baguette}\}.
\]

Convert each element of \(X\) into its set of elementary items:
\begin{itemize}
  \item \(\text{like meat} \to \{\text{pork},\ \text{chicken},\ \text{beef},\ \text{lamb},\ \text{white\_bread}\}\)
  \item \(\text{like vegetables} \to \{\text{lettuce},\ \text{cilantro},\ \text{lamb}\}\)
  \item \(\text{breads} \to \{\text{baguette},\ \text{white\_bread}\}\)
\end{itemize}

Taking the union, we obtain:
\[
X = \{\text{pork},\ \text{chicken},\ \text{beef},\ \text{lamb},\ \text{white\_bread},\ \text{lettuce},\ \text{cilantro},\ \text{baguette}\}.
\]

Since \(Y\) is already elementary, we have:
\[
Y = \{\text{beef},\ \text{cilantro},\ \text{baguette}\}.
\]
Then, the intersection is:
\[
X \cap Y = \{\text{beef},\ \text{cilantro},\ \text{baguette}\},
\]
so that \(|X \cap Y| = 3\), \(|X| = 8\), and \(|Y| = 3\).

Thus, the precision is:
\[
P(X \mid Y) = \frac{3}{3} = 1,
\]
and the recall is:
\[
P(Y \mid X) = \frac{3}{8} \approx 0.375.
\]

{\bf Interpretation:} In this example, \(X\) represents the expanded ground truth and \(Y\) represents the model's prediction. Here, every item retrieved by the model is relevant (precision is 1), but the model retrieves only a subset of all relevant items (recall is 0.375).

\section{Model Definition}
In our model, we propose two operators on a set \(X\):

\begin{itemize}
  \item The fuzzy operator \(F(X)\) is defined such that the size of the fuzzy set is \(k_g\) times the size of \(X\):
    \[
    |F(X)| = k_g \cdot |X|.
    \]
  \item The uncertainty operator \(U(X)\) is defined such that the size of the uncertain set is \(k_u\) times the size of \(X\):
    \[
    |U(X)| = k_u \cdot |X|.
    \]
\end{itemize}

Here, \(k_g\) and \(k_u\) are constants that control the degree of fuzziness and uncertainty, respectively.

\subsection*{Example 3: Abstract Model Operator Example}
Let
\[
X = \{x_1, x_2, x_3\}
\]
and define
\[
Y = F(x_1) \cup U(x_2).
\]

We make the following assumptions:
\begin{itemize}
  \item Every elementary item has size 1.
  \item For any item \(x\), \(F(x)\) produces a set of size \(|F(x)| = k_g\) that contains \(x\). Assume \(k_g = 2\).
  \item For any item \(x\), \(U(x)\) produces a set of size \(|U(x)| = k_u\) that contains \(x\). Assume \(k_u = 3\).
  \item \(F(x_1)\) and \(U(x_2)\) are disjoint.
\end{itemize}

Then:
\[
|F(x_1)| = 2,\quad |U(x_2)| = 3,\quad \text{and} \quad |Y| = 2 + 3 = 5.
\]
Since \(F(x_1)\) contains \(x_1\) and \(U(x_2)\) contains \(x_2\), we have:
\[
X \cap Y = \{x_1, x_2\},
\]
so that \(|X \cap Y| = 2\) while \(|X| = 3\).

Thus, the precision and recall are:
\[
P(X \mid Y) = \frac{2}{5} = 0.4, \quad P(Y \mid X) = \frac{2}{3} \approx 0.667.
\]

{\bf Interpretation:} In this abstract example, the ground truth \(X\) contains three items, while the model retrieves items by applying the fuzzy operator \(F\) to \(x_1\) and the uncertainty operator \(U\) to \(x_2\). The model correctly retrieves \(x_1\) and \(x_2\) (which appear in both \(X\) and \(Y\)), yielding a precision of 40\% and a recall of about 66.7\%.

\subsection*{Example 5: Composite Operators Example}
Now consider the following:
\[
X = \{F(x_1),\, U(F(x_2))\} \quad \text{and} \quad Y = \{U(x_1),\, x_2\}.
\]

We make the following assumptions:
\begin{itemize}
  \item Every elementary item has size 1.
  \item For any item \(x\), \(F(x)\) produces a set of size \(|F(x)| = k_g\) that contains \(x\); assume \(k_g = 2\). For example, let \(F(x_1) = \{x_1, a\}\).
  \item For any item \(x\), \(U(x)\) produces a set of size \(|U(x)| = k_u\) that contains \(x\); assume \(k_u = 3\). For example, let \(U(x_1) = \{x_1, c, d\}\).
  \item The operators extend to sets by taking the union over the elements.
  \item The additional items produced by the operators are unique and disjoint.
  \item In the composite operator \(U(F(x))\), we first compute \(F(x)\) and then apply \(U\), so that 
    \[
    |U(F(x))| = k_u \cdot |F(x)|.
    \]
\end{itemize}

For our example:
\begin{itemize}
  \item \(F(x_1) = \{x_1, a\}\) (size 2).
  \item \(U(x_1) = \{x_1, c, d\}\) (size 3).
  \item \(F(x_2) = \{x_2, b\}\) (size 2).
  \item \(U(F(x_2))\) then yields a set of size \(3 \times 2 = 6\); for example, let \(U(F(x_2)) = \{x_2, b, e, f, g, h\}\).
\end{itemize}

Thus, the expanded ground truth is:
\[
X = F(x_1) \cup U(F(x_2)) = \{x_1, a\} \cup \{x_2, b, e, f, g, h\},
\]
so that \(|X| = 2 + 6 = 8\).

The prediction is:
\[
Y = U(x_1) \cup \{x_2\} = \{x_1, c, d\} \cup \{x_2\},
\]
so that \(|Y| = 3 + 1 = 4\).

The intersection is:
\[
X \cap Y = \{x_1, x_2\},
\]
so that \(|X \cap Y| = 2\).

Therefore, the precision and recall are:
\[
P(X \mid Y) = \frac{2}{4} = 0.5, \quad P(Y \mid X) = \frac{2}{8} = 0.25.
\]

{\bf Interpretation:} In this composite example, the ground truth \(X\) is built by applying the fuzzy operator \(F\) to \(x_1\) and the uncertainty operator \(U\) to \(F(x_2)\), while the prediction \(Y\) comprises the uncertain version of \(x_1\) and the elementary item \(x_2\). Under our assumptions, only \(x_1\) and \(x_2\) are common between \(X\) and \(Y\), resulting in a precision of 50\% and a recall of 25\%. This illustrates the impact of applying different operators on the ground truth versus the prediction.

\subsection*{Example 6: Challenging Composite Example}
Now consider:
\[
X = \{F(x_1),\, U(F(x_2))\}
\]
and
\[
Y = \{U(x_1),\, x_2\}.
\]

We make the same assumptions as in Example 5. Then:
\begin{itemize}
  \item \(F(x_1) = \{x_1, a\}\) (size 2).
  \item \(U(F(x_2)) = \{x_2, b, e, f, g, h\}\) (size 6).
\end{itemize}

Thus, the ground truth is:
\[
X = \{x_1, a, x_2, b, e, f, g, h\},
\]
with \(|X| = 8\).

For the prediction:
\begin{itemize}
  \item \(U(x_1) = \{x_1, c, d\}\) (size 3).
  \item \(x_2\) is elementary (size 1).
\end{itemize}

Thus:
\[
Y = \{x_1, c, d, x_2\},
\]
with \(|Y| = 4\).

The intersection:
\[
X \cap Y = \{x_1, x_2\},
\]
so that \(|X \cap Y| = 2\).

Hence, the precision and recall are:
\[
P(X \mid Y) = \frac{2}{4} = 0.5, \quad P(Y \mid X) = \frac{2}{8} = 0.25.
\]

{\bf Interpretation:} In this challenging composite example, the ground truth \(X\) is formed by combining the fuzzy operator \(F\) applied to \(x_1\) and the uncertain fuzzy operator \(U(F(x_2))\). The prediction \(Y\) includes the uncertain form of \(x_1\) and the elementary \(x_2\). Under the given assumptions, the overlap between \(X\) and \(Y\) is limited to \(x_1\) and \(x_2\), yielding a precision of 50\% and a recall of 25\%. This highlights the difficulty of aligning fuzzy and uncertain measures when different operators are applied to form the ground truth and prediction.

\end{document}
