% examine the behavior of F1 for the Gaussian case. The sensitivity
% is d'=2.5 we vary the priors and see what happens.

clear
clf
x = -6:0.05:6;
d=2.5;
pis = [1/10  1/100  1/10000];

for k=1:length(pis)
    pi=pis(k);
h = 1- cdf('norm',x,d,1);
f= 1- cdf('norm',x,0,1);
theta = h./f;


p00 = (1-pi)*(1-f);
p01 = (1-pi)*f;
p10 = pi*(1-h);
p11 = pi*h;



p = p11./(p11+p01);


F1 = (p11*2)./(p11*2+p01+p10);

[y,i] = max(F1);


disp(sprintf('pi=%5.6f f=%6.5f:  h=%6.5f p=%6.5f maxF1= %6.5f\n', pi,f(i), h(i), p(i),y))

F1Inv = 0.5*(1./h+ (1-pi)./(pi*theta) + 1);

subplot(2,2,1)
plot(f,h)
xlabel('False Alarm Rate')
ylabel('Hit Rate')
title('ROC')

hold on

subplot(2,2,2)
if pi == 0.1
  plot(p,h,'r')
elseif  pi == 0.01
  plot(p,h,'k')
else 
  plot(p,h,'b')
end

xlabel('Precission')
ylabel('Recall')
title('Precission Recall Curve')
hold on

subplot(2,2,3)
plot(log10(f),theta)
xlabel('log_{10}(false alarms)')
ylabel('hits/(false alarms)')
hold on

subplot(2,2,4)
if pi == 0.1
plot(log10(f),F1,'r')
elseif  pi == 0.01
  plot(log10(f),F1,'k')
else 
   plot(log10(f),F1,'b')
end


hold on
%scatter(log10(f(i)), F1(i),'r')
xlabel('log_{10}(false Alarms)')
ylabel('F_1')

end

subplot(2,2,2)
legend('\pi=1/10','\pi=1/100','\pi=1/10000')
subplot(2,2,4)
legend('\pi=1/10','\pi=1/100','\pi=1/10000','Location','Best')