function F  = computeFScores(rObject,rLabel,alpha) 
% computes Rijsbergen F-score between two sets of rectangular
% areas defined by the matrices rObject and rLabel
% negative values mean F is undefined
% We parameterize the F score by alpha in [0 1] which is the weight given to
%  precision, and 1-alpha the weight given to recall 
%  note the standard parameterization of F is by beta
%  =sqrt((1-alpha)/alpha))
%
% rObject is a 4 x n1 matrix. Each column is a rectangle. rows are the x,
% and y coordinates of the top left corner of the rectngle. and the
% width and height of the rectangle. X axis is horizontal  and grows
% left to right. Y axis  is vertical and grows top to bottom.  rLabel is a
% 4 x n2 matrix of labels, using the same standard as rObject.  rObject
% represent groundtruth (objects). rLabel represents sysem whose
% performance we are calculating.
%
% Labels are assigned to objects using the assignment that maximizes
% the average F score 


n1 = size(rObject,2);
n2 = size(rLabel,2);

% create virtual objects or labels if necessary
if n1>n2
  for k=n2+1:n1
    rLabel(:,k) = [0;0;0;0];
  end
elseif n1<n2
    for k=n1+1:n2
      rObject(:,k) = [0;0;0;0];
    end
end

% by now rObject and rLabel should have equal size
s = size(rObject,2);

% go over all possible assingments of objects in rLabel to objects in
% rObject

per = perms(1:s)';
%cols of J are the different assignemtns
for a=1:size(per,2)
  for k1=1:n1
    F_=computeFScoreOneObjectOneLabel(rObject(:,k1),rLabel(:,per(k1,a)),alpha); 
    Fa(k1,a) = F_;
     
    end
end
% find best assignment
[tmp, k]= max(sum(Fa,1));
% output the best asssignment and the J values for those
% assignments

% grab the F score for the optimal assignment
F= Fa(:,k);


function F= computeFScoreOneObjectOneLabel(rObject,rLabel,alpha) 
% computes Rijsbergen F-score between  between two rectangular
% areas defined by the vectors rObject and rLabel
% negative values mean F is undefined
% We parameterize by alpha in [0 1] which is the weight given to
%  precision, and 1-alpha the weight given to recall 
% rObject, and rLabel are 4dimensional vectors with (x,y,w,h)
% where (x,y) are the x, y coordinates of the top left corner of
% the rectangle, w,h are widhts and heights
% X axis is horizontal and increases left to right
% Y axis is vertical and increases top to bottom 
% Javier R. Movellan
% Copyright Machine Perception Technologies, 2012

xTopLeft1 = rObject(1);
yTopLeft1 = rObject(2);
xTopRight1 = xTopLeft1 +rObject(3);
yTopRight1= yTopLeft1;
xBottomLeft1= xTopLeft1;
yBottomLeft1 = yTopLeft1+rObject(4);
xBottomRight1 = xTopLeft1+rObject(3);
yBottomRight1= yTopLeft1+rObject(4);

xTopLeft2 = rLabel(1);
yTopLeft2 = rLabel(2);
xTopRight2 = xTopLeft2 +rLabel(3);
yTopRight2= yTopLeft2;
xBottomLeft2= xTopLeft2;
yBottomLeft2 = yTopLeft2+rLabel(4);
xBottomRight2 = xTopLeft2+rLabel(3);
yBottomRight2= yTopLeft2+rLabel(4);




yOverlap = max(min(yBottomLeft1, yBottomLeft2) -max(yTopLeft1, ...
						yTopLeft2),0);

xOverlap = max(min(xTopRight1, xTopRight2) -max(xTopLeft1, ...
						  xTopLeft2),0);
sizeIntersection = xOverlap*yOverlap;
size1 = rObject(3)*rObject(4);
size2 = rLabel(3)*rLabel(4);
den = alpha*size2+(1-alpha)*size1;

if(den >0) 
  F= sizeIntersection/den;
else
  F=-1;
end





  
 