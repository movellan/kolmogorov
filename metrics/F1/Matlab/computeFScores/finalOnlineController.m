% mockup of final quality controller for face box labeling tool
% Javier R. Movellan
% Copyright Machine Perception Technologies MPTec.com, 2012
clear

threshold = [ 0.7090 0.7614 0.7846 0.7984 0.8078 0.8148 0.8202 ...
	       0.8246 0.8282 0.8312 0.8339 0.8361 0.8382]; 
nSamples =length(threshold);

for s=1:nSamples
  % get the quality scored for sample s
  F1(s)= rand(1);
  % get the miss rate and junk rate for sample s
  missRate(s) = rand(1) ;
  junkRate(s) = rand(1);
  
  %get the running average for F1,missRate, and junkRate
  averageF1 = mean(F1(1:s));
  averageMissRate = mean(missRate(1:s));
  averageJunkRate = mean(junkRate(1:s));
  
  % provide feedback 
  qualityScore=ceil(100*averageF1);
  disp(sprintf('Your Quality Score is %d  percent',qualityScore))

  if s< nSamples 
    if qualityScore < 100*threshold(s)
      disp('Your Quality Score is too low')
      if averageJunkRate > 0.2 & averageMissRate<0.2
	disp('Your boxes may be too large')
	disp('Try making them a bit tighter');
	disp('Your goal is to minimaly enclose the siluete naked head');
      end
      if averageMissRate > 0.2 & averageJunkRate< 0.2
	disp('Your boxes may be too small')
	disp('Try making them a bit larger');
	disp('Your goal is to enclose enclose the siluete of the  naked  head');
      end
    end
  end
  if s == nSamples
    % by the time we get the last sample we make the final decision, we
    % do not give any further advice
    if qualityScore< 100*threshold(s)
      disp('Your Quality Score is too low')
      disp('We cannot accept your HIT')
      disp('We will erase the data you provided')
      disp('If you think we erroneously rejected your HIT')
      disp('Please contact us through the Amazon MT services')
    end
  end
end


