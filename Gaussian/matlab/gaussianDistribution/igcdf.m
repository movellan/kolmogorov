function z = igcdf(p)
% inverse standard gaussian cumulative distribution function
if p<0 || p>1
  z = nan;
else
  z =  sqrt(2) * erfinv(2*p-1);
end
