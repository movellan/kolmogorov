function p = gcdf(z)
% Gaussian cumulative distribution function 
p = 0.5 * erfc(-z / sqrt(2));

