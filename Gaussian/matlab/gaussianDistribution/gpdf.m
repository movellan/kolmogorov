function d = gpdf(z)
% standard gaussian probability density function

d = exp(-0.5 * z.^2) / sqrt(2 * pi);
