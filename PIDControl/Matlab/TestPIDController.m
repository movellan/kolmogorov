
num=1;
den=[1 10 20];
% run a ltimodelwith transfer function 1/( s^2+ 10s +20)
% note the dc gain is 1/20 so we'll converge to 0.05 
% open loop step response
step(num,den)

figure
% add a proportional controller

Kp=300;


num=[Kp]; den=[1 10 20+Kp]; t=0:0.01:2; step(num,den,t)

figure

% Proportional derivative controller
Kp=300;
Kd= 10;
num= [ Kd Kp];
den = [1 10+Kd 20+Kp];

t=0:0.01:2; step(num,den,t)

figure

% Proportional Integral controller

Kp=30; Ki=70; num=[Kp Ki]; den=[1 10 20+Kp Ki];
t=0:0.01:2; step(num,den,t)
figure

%PID Controller

Kp=350; Ki=300; Kd=50; num=[Kd Kp Ki]; den=[1 10+Kd 20+Kp Ki]; t=0:0.01:2; step(num,den,t)