clear

r=1; % lenght of bar
x(:,1) = [0 r]; % starting position.
y = [0.1 1]';
y = y/norm(y)*r; % desried position
epsilon=0.0000001; % step size

alpha(1) = atan(x(1,1)/x(2,1));
for t=1:200000
  e= y- x(:,t);
  enorm(t) = norm(e);
  gradient_e = [ -sin(alpha(t)) cos(alpha(t))]';

  hessian_e(t) = -  e'*[-cos(alpha(t)) -sin(alpha(t))]';
  hessian_e(t) = hessian_e(t) + x(:,t)'*[ sin(alpha(t)) -cos(alpha(t))]';
  hessian_e(t) = 1/hessian_e(t);
  %hessian_e(t) =0.0187;
  alpha(t+1) = alpha(t) - epsilon*hessian_e(t)*e'*gradient_e;
  x(:,t+1) = [cos(alpha(t+1)) sin(alpha(t+1))];
end
plot(alpha*360/2/pi)