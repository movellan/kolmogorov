\documentclass[12pt]{book}
%\usepackage{Mynips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath,amsthm}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[square,numbers,sort]{natbib} 
%\usepackage[round,sort]{natbib} 
\usepackage[latin1]{inputenc}  
\usepackage{verbatim}
\usepackage[usenames]{color}


 \title{Performance Measures\\}
\author{ 
 Copyright \copyright{}  2011 Javier
R. Movellan. 
}

\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}
\newcommand\given{\medspace|\medspace}        
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\pfrac}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\ppfrac}[3]{\frac{\partial^2 #1}{\partial #2 \partial #3}}
\newcommand\R{\mathcal{R}}




\newtheorem{prop}{Proposition}[section]
\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{lem}{Lemma}[section]
\theoremstyle{definition}
\newtheorem{rem}{Remark}[section]
\theoremstyle{definition}
\newtheorem{ex}{Example}[section]


\begin{document}
\maketitle


    

\section{Evaluating Binary Classifiers}
Let $C$ represent a binary category describing some property of the
world we wish to discover based on sensory information. Let $D$ the
decision made by a binary classifier. For example $C=1$ may represent
that a document is relevant and $D=1$ may represent that our system
decides to retrieve that document. $C=1$ may represent the fact that
there is a fire in the house, and $D=1$ may represent that a fire
detector detects it.  Any measures of performance of the
classifier, must be based on the joint distribution of $D$ and
$C$. This distribution consists of 4 numbers $p(C=0, D=0), p(C=0,
D=1),p(C=1, D=0),p(C=1, D=1)$. These numbers add up to 1. Therefore
the joint distribution is fully specified by 3 numbers.These 4 numbers, organized as a table are typically knows as the ``Confusion Table''
\begin{table}
\begin{center}\begin{tabular}{|c|c|c|}
\hline 
  &  $D=0$ & $D=1$   \\ \hline 
 $C=0$ &  $p(C=0,D=0)$ & $p(C=0,D=1)$   \\ \hline 
 $C=1$ &  $p(C=1,D=0)$ & $p(C=1,D=1)$   \\ \hline 
\end{tabular}
\end{center}
\caption{Confusion Matrix}
\end{table}
The event $\{C=1,D=1\}$ is known as a {\em ``True Positive''}, the
event $\{C=1, D=0\}$ a {\em ``False Negative''}, the event $\{C=0,
D=1\}$ a {\em ``False Positive''} and the event $\{ C=0, D=0\}$ a {\em
  ``True Negative''}



\subsection{Hit Rates, Recall,  False Alarm Rates, Precision}

The conditional probability that $D=1$ given that $C=1$ is known as
the {\em Hit Rate}, or the, {\em True Positive Rate} or the  {\em Recall}
\begin{align}
r = p(D=1\given C=1) = \frac{P(C=1, D=1)}{p(C=1,D=0)+ p(C=1, D=1)}
\end{align}
For example, the hit rate could represent the proportion of fires
detected by a fire detector, or the proportion of relevant sites
correctly found by a Web search engine. High recall in a search engine
means that the system returns most of the relevant results.

The conditional probability that $D=1$ given that $C=0$ is known as the {\em False Alarm Rate}, or {\em False Negative Rate} 

\begin{align}
f = p(D=1\given C=0) = \frac{P(C=0, D=1)}{p(C=0, D=0)+ p(C=0,D=1) }
\end{align}
 The conditional probability that $C=1$ given that $D=1$ is known as the
{\em precision}
\begin{align}
p = p(C=1\given D=1) = \frac{P(C=1, D=1)}{p(C=1, D=1)+ p(C=0, D=1)}
\end{align}
For example, the precision may represent the proportion of times that
there really is a fire when the fire detector beeps, or the proportion
of sites retrieved by a search engine that are relevant. High
precision in a search engine means that most of the results retrieved
by the engine are relevant.

\subsection{ROC and PR curves}
Binary classifiers typically map a sensor signal into a value that
represents the evidence that $C=1$. If the evidence is larger than a
fixed threshold $\theta$ then the classifier decides $D=1$ otherwise,
$D=0$. Thus the threshold $\theta$ will result on changes in the hit
rates, false alarm rates, and precision of the system. There are two
popular ways to represent the performance for different thresholds:
the Response Operator Characteristic (ROC) curve, and the Precision
Recall (PR) curve. The ROC curve can be seen as a scatter plot where
every point corresponds to the hit rate (horizontal axis) and the
false alarm rate (vertical axis) obtained for a given threshold.  For
the PR curve the horizontal axis represents precision and the
vertical axis recall (hit rate).

Note for a given threshold each of these curves plot only 2 out of the
3 independent numbers of the joint distribution of $C,D$ and thus,
neither of them is sufficient to reconstruct the joint distribution. 



\subsection{Relationship between Precision and Hit Rates and False Alarm Rates}

Using Bayes Rule 
\begin{align}
p = p(C=1\given D=1) &= \frac{p(C=1) p(D=1\given C=1)}{
p(C=1) p(D=1\given C=1)+p(C=0) p(D=1\given C=0)}\\
p=  \frac{\pi r}{\pi r + (1-\pi) f}\label{eqn:p}
\end{align}
where 
\begin{align}
\pi = p(C=1)
\end{align}
is the prior probability for $C=1$.  Note that in order to get the
precision we need to know both the hit rate $h$, false alarm rate $f$
and prior probability $\pi$,
\begin{align}
\frac{1-p}{p} = \frac{(1-\pi) f}{\pi r}
\end{align}
Thus, the false alarm rate is also recoverable, provided we know $\pi, p,r$ 
\begin{align}
f = r \frac{1-p}{p}  \frac{\pi} {1-\pi} 
\end{align}
Similarly if $f,p,r$ are known, we can recover the prior probability 
\begin{align}
p(C=1) = p(D=1) p(C=1 \given D=1) + p(C=1) p(D=0\given C=1)
\end{align}
where
\begin{align}
p(D=1) &= p(C=1) p(D=1\given C=1) + p(C=0) p(D=1\given C=0) \nonumber\\= \pi r + (1-\pi) f
\end{align}
Thus
\begin{align}
\pi = (\pi r + (1-\pi) f ) p + \pi (1-r) 
\end{align}
and rearranging terms
\begin{align}
\pi = \frac{pf }{pf + (1-p) r}
\end{align}
In summary. In order to represent the join distribution of $C$ and $D$
we need three out of the 4 numbers: $r,f,p,\pi$
\subsection{The 2AFC score}
The 2AFC score is the probability that the classifier will assign a
larger score to a randomly selected example from Category 1 than to a
randomly selected example of Category 0. The Appendix shows Matlab
code for competing the 2AFC score.  An advantage of this score is that
it is invariant to the prior probability $\pi$. Another advantage is
that it is invariant to monotonic transformations of the 


It can be shown that under some conditions (I know it works for
continuous random variables, not sure for the discrete case)the 2AFC
score corresponds to the area under the ROC curve. Also known as the
AUC. One problem with directly computing the AUC is the interpolation
scheme. For this reason we favor the use of the 2AFC score.

In the case that the two category of interest are Gaussian with equal
variances and with means separated $d'$ standard deviations from each other, it can be shown that 
\begin{align}
d' = \sqrt{2} \Phi^{-1}(q)
\end{align}
where $\Phi^{-1}$ is the inverse Gaussian probability distribution and $q$ is the 2AFC score. 

\subsection{The $F_\beta$ score}
Precision-Recall (PR) curves, often used in Information Retrieval
(Manning \& Schutze, 1999; Raghavan et al., 1989), have been cited as
an alternative to ROC curves for tasks with a large skew in the class
distribution (Bockhorst \& Craven, 2005; Bunescu et al., 2004; Davis et
al., 2005; Goadrich et al., 2004; Kok \& Domingos, 2005; Singla \&
Domingos, 2005). The $F_\beta$ score is defined as follows
\begin{align}
F_\beta = \frac{ P(C=1,D=1) (1 + \beta^2)}{ p(C=1, D=1) (1+ \beta^2) + p(C=0, D=1) \beta^2 + p(C=1, D=0)}
\end{align}
It is sometimes convenient to work with the parameter $\alpha =
1/(1+\beta^2)$. Note 
\begin{align}
F_\beta &= \frac{p(C=1, D=1)}{ p(C=1,D=1) (\alpha +1-\alpha)+  p(C=0, D=1) (1-\alpha) + p(C=1, D=0) \alpha}\nonumber\\
&= \frac{p(C=1, D=1)}{\alpha(  p(C=1,D=1) + p(C=1, D=0) ) + (1-\alpha)(p(C=1, D=1) +   p(C=0, D=1))}\nonumber\\
&= \frac{1}{\alpha / r + (1-\alpha)/ p} 
\end{align}
Thus the inverse of $F_\beta$ is a weighted average of the inverse of the hit rate (or recall) and the inverse of the precision
\begin{align}
F^{-1}_\beta = \alpha \;r^{-1} + (1-\alpha) \;p^{-1}
\end{align}
For $\beta=1$ we get $\alpha = 1-\alpha =0.5$, the precision and the
recall are weighted equally, resulting on the $F_1$ score
\begin{align}
F^{-1}_1 = \frac{ r^{-1} + p^{-1}}{2}
\end{align}
For $\beta=2$ we get $\alpha = 1/5$ and $1-\alpha = 4/5$, Thus the precision gets weighted 4 times as much as the recall
\begin{align}
F^{-1}_2 = \frac{ r^{-1} + 4\; p^{-1}}{5}
\end{align}
For $\beta = 0.5$ we get $\alpha= 4/5$ and $1-\alpha = 1/5$ thus the recall is weighted 4 times as much as the precision
\begin{align}
F^{-1}_{0.5} = \frac{ 4 \;r^{-1} +  p^{-1}}{5}
\end{align}
\paragraph{Relationship between $F_1$ and the ROC:}
Using \eqref{eqn:p} we get
\begin{align}\label{eqn:2f1}
2 F^{-1}_1 &=  r^{-1} +  \frac{ \pi r + (1-\pi) f}{ \pi r}\nonumber \\
&=  r^{-1} +  \frac{1-\pi}{\pi} \theta^{-1} +1
\end{align}
where 
\begin{align}
\theta  =\frac{r}{f}
\end{align}
can be seen as the ``slope'' of the ROC.  Thus to maximize the $F_1$
score we want regions with large hit rate $r$ and a large
``slope''. The smaller the prior probability $\pi$ is the larger the
relative importance of maximizing the slope. In practice, it may be
the case that large slopes tend to happen in regions of the ROC with
small $f$. My interpretation is that $F_1$ thus is equivalent to the
hit rate $r$ for small false alarm rates $f$.

There are hand-wavy arguments that the $F_1$ score (optimized over
thresholds) is a better measure of performance than 2AFC. For example
Fernando de la Torre says ``In our case, the $F_1$ score is a better
performance measure than the more common ROC
metric because the latter is designed for balanced binary
classification rather than detection tasks , and fails to reflect
the effect of the proportion of positive to negative samples on
classification performance.'' There are also
claims that it is a better measure particularly when the prior $\pi$
is very small. Note the $F_1$ score will change depending on the prior
probability $\pi$ and thus it will change depending on how we label
the true categories, i.e, if we switch $\pi$ into $1-\pi$.

Figure~\ref{fig:gaussian}) shows an analysis of the behavior of $F_1$ for the Gaussian case with 2 categories of equal variance and means that are 2.5 standard deviations from each other. Analysis of ROC case for $d'=2.5$ and different prior probability rates. Top Left: The ROC curve is invariant to the prior probability $\pi$. Top Right. The Precision Recall Curve is sensitive to $\pi$. Bottom left: The $r/f$ ratio favors regions of the ROC with low false alarm rates. Bottom right: As a consequence the smaller the prior probability the more $F_1$ favors regions with low false alarm rates. 



\begin{figure}[h] 
\begin{center}
\includegraphics[width=.95\textwidth]{Media/gaussian.pdf}
\end{center}
\caption{\it Analysis of ROC case for $d'=2.5$ and different prior probability rates. Top Left: The ROC curve is invariant to the prior probability $\pi$. Top Right. The Precision Recall Curve is sensitive to $\pi$. Bottom left: The $r/f$ ratio favors regions of the ROC with low false alarm rates. Bottom right: As a consequence the smaller the prior probability the more $F_1$ favors regions with low false alarm rates. }\label{fig:gaussian}
\end{figure}




\section{Baseline}
Suppose we had a classifier with no sensitivity. If we set the
threshold such that we always decide $D=0$ then $p=0$ and $F_1 =
0$. On the other hand if we set it to always decide $D=1$ then
$r=1,f=1, p = \pi$ and
\begin{align}
F_1 = \frac{ 2 \pi}{1+\pi}
\end{align}
Note, in a classifier
with no sensitivity for any threshold $f/r=1$. Using equation
\eqref{eqn:2f1} it follows that the optimal $F_1$ is obtained by
maximizing the hit rate, i.e., deciding $D=1$ with probability 1.


\end{document}

