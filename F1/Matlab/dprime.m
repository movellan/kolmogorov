function dp = dprime(hit, fa)
  % doesn't compute dprime.
  dp = norminv(hit,0,1) - norminv(fa,0,1);

