clear
% read data file
fid = fopen('data.csv');
data = textscan(fid, '%s%f%f%f%f%s%s');
% find all unique image file names
imageFiles= unique(data{1});
labelers = unique(data{6});
nLabelers =length(labelers);
nImages = length(imageFiles);
nlabels = length(data{1});




% go one by one over all images
for k1=1:nImages
  fileName = imageFiles{k1};
  josh{k1}=[];
  javier{k1}=[];
  javierLabelID{k1}=[];
  joshLabelID{k1}=[];
  % grab all labels for that image
  for k2=1:nlabels
    rowName = data{1}(k2);
    labeler = data{6}(k2);
    label=[data{2}(k2);data{3}(k2);data{4}(k2);data{5}(k2)];

    if strcmp(rowName,fileName)

      if strcmp(labelers{1},labeler)
	josh{k1} = [josh{k1} label];
	joshLabelID{k1}=[joshLabelID{k1} data{7}(k2)];
      else
	javier{k1} = [javier{k1} label];
	javierLabelID{k1}=[javierLabelID{k1} data{7}(k2)];
      end
    end
  end
end
javi=javier;
clear javier;
joshi=josh;
clear josh;
javier.labels=javi;
javier.labelIDs=javierLabelID;
josh.labels=joshi;
josh.labelIDs=joshLabelID;

data.javier =javier
data.josh=josh
save('data','data');
%save('josh','josh');



