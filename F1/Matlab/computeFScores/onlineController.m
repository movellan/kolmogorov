% model the dynamics to discriminate good and bad labelers as we
% collect more samples


clear
addpath('../QualityControl')
load F1 % vector with quality score for n labels


% mean of bad labelers
m0 = percentile(F1,10)
% mean of good labelers
m1= percentile(F1,50)
% standard deviation of label quality scores 
sd =std(F1);

wrongAcceptRate = 1/10
wrongRejectRate = 1/100








% use gaussian model to get nummber of Samples and threshold to
% satisfy the desired specs

[threshold,nSamples]=qualityControl(wrongAcceptRate, wrongRejectRate, ...
				    m0,m1,sd);

nSamples = ceil(nSamples);
% find threshold that meets wrongAccept spec at each time step
z=norminv(wrongRejectRate);
for t=1:nSamples
  % standard deviation of the mean
  sdm(t) = sd/sqrt(t);
  th(t) = sdm(t)*z+m1;
end

% now let's check using montecarlo simulation

nExperiments=10000;
acceptableF = F1+m1 - mean(F1);
rejected= zeros(nSamples,1);
nLabels = length(F1);
for k1=1:nExperiments
   sampleID=randi(nLabels,nSamples,1);
  for k2 = 1:nSamples
    m(k1,k2)  = mean(acceptableF(sampleID(1:k2)));
    if m(k1,k2)<th(k2)
      rejected(k2) = rejected(k2)+1;
    end
  end
end

for k2=1:nSamples
  th2(k2) = percentile(m(:,k2), 100*wrongRejectRate);
end
rejected2= zeros(nSamples,1);
for k1=1:nExperiments
  sampleID=randi(nLabels,nSamples,1);
  for k2 = 1:nSamples
    m(k1,k2)  = mean(acceptableF(sampleID(1:k2)));
    if m(k1,k2)<th2(k2)
      rejected2(k2) = rejected2(k2)+1;
    end
  end
end

% the thresold is to tell the labeler he is not doing well
% we then provide advice based on the junkrate and missrate
% javier produced labels consistently larger than josh
% if we use josh as ground trugh and javier as labeler we get
% missrate = 1 percent
% junkrate = 20 percent
%
% here are some rules
% if quality rejected
% if missrate < 5 and junkrate >15
% your boxes are too large. try to make them as close as possible
% to where the naked head siluete would be, without head, or
% artifacts

% if missrate>15 and junkrate < 5
% your boxes are too small. make sure they cover the entire naked
% head siluette, including ears and top of the head



