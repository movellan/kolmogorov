% compute the F scores of Javier with respect to Josh
% on the head  boxing task on 102 reference images
% Javier R. Movellan
% Copyright MPTec, 2012

clear
load data;
nImages = length(data.josh.labels);

alpha= [0 0.5 1];
% alpha =0 gives us recall rate
% alpha =0.5 gives us F1 score
% alpha =1 gives us precision rate

for k2=1:length(alpha) 
  allF{k2}= [];
end

for k=1: nImages; 
    r1 = data.josh.labels{k}; % columns of r1 are boxes for image k
                              % the 4 rows of r1 are the x,y,w,h
                              % parameters of the box
    r2= data.javier.labels{k}; % same for javier boxes. 
    
    % we treat Josh as the ground truth and Javier as the labeler
    for k2=1:length(alpha) 
      F{k,k2} = computeFScores(r1,r2,alpha(k2));
pause
      allF{k2}=  [allF{k2}; F{k,k2}];
    end
    
end

% average only over the non-negative (undefined) F scores
for k2=1:length(alpha)
  overallF(k2) = mean(allF{k2}(allF{k2}>0));
end

overallF

F1= allF{2}(allF{2}>0);
missRate = 1-allF{1}(allF{1}>0);
junkRate = 1 - allF{3}(allF{3}>0); 
save('F1','F1');
save('junkRate','junkRate');
save('missRate','missRae')




