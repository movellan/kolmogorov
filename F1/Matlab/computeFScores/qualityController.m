% compute desired threshold and number of samples to maintain
% desired quality specifications using Gaussian model
% double checks performance of the model using MonteCarlo
% simulation on empirical data
%
% Javier R. Movellan
% Copyright Machine Perception Technologies, 2012


clear
addpath('../QualityControl')
load F1 % vector with quality score for n labels


% set the desired specifications for the quality congroller
% in this case wrong rejects are not to bad but
% wrong rejects are costly. labelers get upset, we have to respond
% to them ...
smallestAcceptableAverage = percentile(F1,10)
largestRejectableAverage= percentile(F1,50)
wrongAcceptRate = 1/10
wrongRejectRate = 1/100




saq = smallestAcceptableAverage;
lrq = largestRejectableAverage;
sd =std(F1);

% use gaussian model to get nummber of Samples and threshold to
% satisfy the desired specs

[threshold,nSamples]=qualityControl(wrongAcceptRate, wrongRejectRate, ...
				    saq,lrq,sd);
nSamples = ceil(nSamples)

nSamples % number of samples recommended by model 
threshold  % threshold recommended by model 
% the controller works as follows: if average quality score over
% the nSamples is less than threshold, reject the entire batch of labels




% below are the parameters provided by the Gaussian model
smallestAcceptableAverage = 0.7987
largestRejectableAverage = 0.8877


% Now we do MonteCarlo simulations using the Bootstrap method to
% make sure the controller works within desired specs
nLabels = length(F1)
nExperiments =40000;
F1mean = mean(F1);

%rejectableF = F1*smallestAcceptableAverage/F1mean;
rejectableF = F1+smallestAcceptableAverage - F1mean;
rejected = 0;
for k=1:nExperiments
  m0(k) = mean(rejectableF(randi(nLabels,nSamples,1)));
  if m0(k) < threshold
    rejected=rejected+1;
  end
end

desiredWrongAcceptRate= wrongAcceptRate
obtainedWrongAcceptRate = 1 -rejected/nExperiments

%acceptableF = F1*largestRejectableAverage/F1mean;
acceptableF = F1+largestRejectableAverage-F1mean;
rejected=0;
for k=1:nExperiments
  m1(k) = mean(acceptableF(randi(nLabels,nSamples,1)));
  if m1(k) < threshold
    rejected=rejected+1;
  end
end
desiredWrongRejectRate=wrongRejectRate
obtainedWrongRejectRate = rejected/nExperiments

