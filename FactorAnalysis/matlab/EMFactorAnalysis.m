% Independent Factor Analysis
% Copyright @ Javier R. Movellan
% Copyright (C) Javier R. Movellan April 2002
% Copyright (C)  Machine Perception Laboratory
% This program is free software; 
% you can redistribute it and/or modify it under the terms 
% of the GNU General Public License 



clear
randn('state',0);
rand('state',0);
n = 2; % observations
d = 2; % sources
truea = rand(n,d); %mixing matrix
truePsi = 1*eye(n); % noise matrix
s = 3000; % number of samples
nt=100; % number EM Cycles

alpha1 = sqrt(100); % The standard deviations are 1 and alpha1

prior = [0.5; 0.5];

sources =  randn(d,s) + sqrt(alpha1^2 - 1)* randn(d,s).* (rand(d,s)< prior(2));



truea = [0.5 1; 1 0 ];
%truea = eye(2);
o = truea*sources  +truePsi* randn(n,s);



%[u,l,v] = svd(cov(o'));

a = rand(n,d);
Psi = truePsi;
L=zeros(nt,1);
for t=1:nt
    t
  lhs = zeros(n,d);
  rhs = zeros(d,d);
  probData= zeros(s,1);
  clear z;
  z = sparse((1:s)',(1:s)',0*(1:s)');

    
    
  for index=0:2^d-1 % go over all possible mixtures
    m = zeros(d,1);
    x= num2str(dec2bin(index));
    for k=1:size(x,2)
      m(k) = str2num(x(k));
    end
    prior_m = (1-prior(2))^(d-sum(m)) * (prior(2))^sum(m);
    sigma_m = (eye(d)-diag(m)) + diag(m)*alpha1^2; % the standard devs for this
                                                 % particular mixture

    estco = a*sigma_m*a'+ Psi;
    z = z+ sparse((1:s)', (1:s)',prior_m*exp( - (n*log(2*pi)+log(det(estco))+ ...
                                       (sum(o.*(inv(estco)*o)))')/2));    
    
  end


  
  

  for index=0:2^d-1 % go over all possible mixtures
    m = zeros(d,1);
    x= num2str(dec2bin(index));
    for k=1:size(x,2)
      m(k) = str2num(x(k));
    end
    
    prior_m = (1-prior(2))^(d-sum(m)) * (prior(2))^sum(m);
    sigma_m = (eye(d)-diag(m)) + diag(m)*alpha1^2; % the standard devs for this
                                                 % particular mixture

    estco = a*sigma_m*a'+ Psi;
    
    
    post_m = sparse((1:s)', (1:s)',prior_m*exp( - (n*log(2*pi)+log(det(estco))+ ...
                                       (sum(o.*(inv(estco)*o)))')/2)); 
    post_m = post_m*inv(z);% normalize to geet posterior of mixtures
                   % diagonal matrix with weights for each observation vector.    
    b = sigma_m*a'*inv(a*sigma_m*a'+Psi); 
    h = b*o;
    lhs= lhs+ (o*post_m)*h'; %left hand side
    tmp = sum(sum(post_m));
    rhs = rhs+ (tmp*sigma_m  - tmp*b*a*sigma_m+ (h*post_m)*h');
 
    
    probData = probData + (exp(-sum(o.*(inv(estco)*o))/2)/(sqrt(det(estco)*(2*pi)^n))*post_m)';
%    L(t)= L(t)  + -( s*n*log(2*pi) + s*log(det(estco)) +   sumsum(o.*(inv(estco)*o))))/2;
  end
    L(t) = sum(log(probData));
     a = lhs*inv(rhs);
%  Psi = diag(diag((o*o' -2 *o*(a*h)' +a* (s*eye(d) - s*b*a+h*h')*a')/s));



%estco = a*a' + Psi;
  %

subplot(1,3,1)
ohat = a*sources  +truePsi* randn(n,s);

plot(ohat(1,:),ohat(2,:),'r.')
subplot(1,3,2)
plot(o(1,:),o(2,:),'.')
 subplot(1,3,3)
plot(L(1:t)); % the log-likelihood of the data throughout training
drawnow

end
