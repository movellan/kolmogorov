% A Stationary Infomax 2 Armed Bandit Problem Gambler can pull left or right
% arm One of the arms is informative with respect to the posterior the other
% arm is not informative The reward is the Renyii negentropy of the
% posterior distribution We get more reward the more certain you are about
% the internal state of the machine, i.e., which arm is which The results
% are quite surprising. In many cases of interest it does not matter which
% action is chosen. Try this problem with a temporal horizon of 2. When the
% prior for one of the states is larger than 0.9 it does not matter which
% action is chosen. This is counterintutive because the informative arm
% provides shannon entropy, however it does not provide Renyii entropy,
% i.e., it does not increase the probability of correctly predicting the
% internal state of the system. 
%
% Copyright (C) Javier R. Movellan April 2002,2009
% Copyright (C)  Machine Perception Laboratory
% This program is free software; 
% you can redistribute it and/or modify it under the terms 
% of the GNU General Public License 



clear
clf




file = 'InfomaxArmedBandit.pdp';



nSamples =0 % 0 means exact solution more than 0 prunes based on nSamples

timeSteps = 2;
useSparse=0;


[w uhat] = InfomaxPOMDP(file,useSparse,nSamples,timeSteps);




%%% This plot assumes 2 states s it wont work with all the problems %%%%%%%%%%
if(size(w{1},1) ==2)

  subplot(3,1,1)
  hold on
  
  % first we create a matrix of belief states 
  q= 0:0.0001:1;
  q= [q; 1-q]; 
  
  % We plot all the linear segments that
  % define the controller
  t=1;
  for i=1:size(w{t},2)
  
    s(:,i) = (w{t}(:,i))'*q;
    if(uhat{t}(i) ==1)
      plot(q(1,:),s(:,i),'b');
    else 
        plot(q(1,:),s(:,i),'r--');
    end
  
  end
  title(' Blue= action 1, Red= action 2 ')
  xlabel('State1 Probability')
  ylabel('Value')
  
  % Now we plot the optimal value function
  subplot(3,1,2)

   
  [u v] = controller(w{1}, uhat{1}, q);
  plot(q(1,:), v,'b');
  xlabel('State1 Probability')
  ylabel('Value')
  
  % And now we plot the optimal action 
  
  subplot(3,1,3)
  plot(q(1,:),u,'LineWidth', 4);
 
  xlabel('State1 Probability')
  ylabel('Optimal Action')
end  
