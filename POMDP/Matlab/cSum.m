% crossum vector of two matrices with equal number of rows
% kron(X,Y) is the Kronecker tensor product of X and Y.
%    The result is a lc1arge matrix formed by taking all possible
%    products between the elements of X and those of Y. For
%    example, if X is 2 by 3, then kron(X,Y) is 
%       [ X(1,1)*Y  X(1,2)*Y  X(1,3)*Y
%         X(2,1)*Y  X(2,2)*Y  X(2,3)*Y ]
%  Note the cross sum is different from the Kronecker sum
function c = cSum(x,y)
  nx =size(x,2); ny = size(y,2);
  c1 = kron(x,ones(1,ny));
  c2 = kron(ones(1,nx), y);
  c0 = kron(y,ones(1,nx));
  disp 'c1=',c1
  disp 'c2=',c2
  disp 'c0=',c0

  c = c1+ c2;
  
 