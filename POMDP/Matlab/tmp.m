%% A few interesting problems

clear
clf



% Uncomment one of these to run the corresponding problem

file = 'labelingProblem.pdp';
%file = 'learner.pdp';
%file = 'tiger.pdp';
%file = '4x3.pdp';
% file = 'thrun1.pdp'


nSamples =100 % 0 means exact solution more than 0 prunes based on nSamples
timeSteps = 10;
useSparse=0;


[w uhat] = InfomaxPOMDP(file,useSparse,nSamples,timeSteps)




%%% This plot assumes 2 states s it wont work with all the problems %%%%%%%%%%
if(size(w{1},1) ==2)

  subplot(3,1,1)
  hold on

  % first we create a matrix of belief states
  q= 0:0.0001:1;
  q= [q; 1-q];

  % We plot all the linear segments that
  % define the controller
  t=1;
  for i=1:size(w{t},2)

    s(:,i) = (w{t}(:,i))'*q;
    if(uhat{t}(i) ==1)
      plot(q(1,:),s(:,i),'b');
    else
        plot(q(1,:),s(:,i),'r--');
    end

  end
  title(' Blue= action 1, Red= action 2 ')
  xlabel('State1 Probability')
  ylabel('Value')

  % Now we plot the optimal value function
  subplot(3,1,2)


  [u v] = controller(w{1}, uhat{1}, q);
  plot(q(1,:), v,'b');
  xlabel('State1 Probability')
  ylabel('Value')

  % And now we plot the optimal action

  subplot(3,1,3)
  plot(q(1,:),u,'LineWidth', 4);

  xlabel('State1 Probability')
  ylabel('Optimal Action')
end
