function [u v] = controller(w, uhat, q) 
% given the w and uhat matrix of a controller and a set
% of belief states q output the value and optimal action of the belief states  
% the w matrix and uhat vector are given by the InfomaxDP program 
% the q matrix contains the belief states we want to probe
% each column of q is a belief state, i.e., a vector containing the
% posterior probability of the states
  
% Copyright (C) Javier R. Movellan April 2002
% This program is free software; 
% you can redistribute it and/or modify it under the terms 
% of the GNU General Public License 

  
  m = size(q,2); % each colum is a posterior probability state
  for i=1:m
    vu = w'*q(:,i);
    [v(i,1), vi ] = max(vu'); 
    u(i,1) = uhat(vi);
   
  end
  print m
  
  
  
