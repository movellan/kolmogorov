function [w uhat] = InfomaxPOMDP(file,useSparse,nSamples,T)
%
% Dynamic programming for Infomax Finite and Infinite Horizon POMDPs
%
% [w uhat] = dp(file,useSparse,nSamples,T)
%
% file: .pdp file with the structure of the pomdp
%
% useSparse: 0 to not use sparse files, 1 to use sparse files
%
% nSamples: number of samples used to do stochastic trimming of policies
%           the more samples, the more precise the obtrained policy is
%           nSamples =0 does not do trimming, i.e., optimal policy
%                       however you may run out of memory if you use
%                       nSamples =0
%
% T: The number of time steps for the finite horizon
%    for infinite horizon problems use a large enough T such that
%    the policy converges. This correesponds to the standard
%    policy iteration algorithm for infinite horizon pomdp problems
%
%  w is a cell array with T cells
%  w{t} is a matrix with the parameters for all the linear segments of the
%  value function at time t
% Each column of w{t} has the weight vector for a linear segment, i.e.,
% each row weights the probability of a state
%
% The matrix uhat{t} keeps track of which response alternative each
% segment belongs to
%
%
% %%%%%%% Explanation of Infomax Control %%%%%%%%%%%%%%
%
% In addition to the standard state and action dependent rewards
% There is an inbuilt reward for accumulation of information
% Instant Reward(q,u)  = (\sum_x q(x) r(x,u))  + exp( - H(q))
%
% where q is the belief vector  (posterior probability of the
% states), u is the action and H(q) is the Renyi entropy
%  of infinite order
%   H(q) = -log max(q)
%
%
% For tutorial see pomdp tutorial at mplab.ucsd.edu
%
% Copyright (C) Javier R. Movellan April 2002,2009
% Copyright (C)  Machine Perception Laboratory
% This program is free software;
% you can redistribute it and/or modify it under the terms
% of the GNU General Public License



p = parsePDPFile(file,useSparse);
p.lambda

L = eye(p.nx)*p.lambda;

w{T+1} = zeros(p.nx,1);
for t = T: -1 :1
  w{t} = zeros(p.nx,1);% stub to get tings started
  uhat{t} =0; % stub to get tings started
  for u =1:p.nu
    wu = zeros(p.nx,1);
    for z =1: p.nz
      wuz = p.gamma*p.k{u,z}*w{t+1};
      wu = cSum(wu, wuz);% cross sums the set of columns
    end
    wu = cSum(p.r{u},wu);
   wu = cSum(L,wu); % Here is where we put the information reward
    wuIndexes = u*ones(1,size(wu,2));
    % here we shoulkd keep track of which action owns a segment
    w{t} = [ w{t} wu];
    uhat{t} = [ uhat{t} wuIndexes];
  end
  w{t}(:,1)=[]; % get rid of the stub
  uhat{t}(1)=[]; % get rid of the stub
t
if (nSamples >0)
  ind  = prune(w{t},nSamples); % get rid of the recommended trims
  w{t}(:,ind) =[];
  uhat{t}(:,ind) =[];
end


end
print 'x'
