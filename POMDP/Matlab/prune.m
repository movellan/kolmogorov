% each column of x is a different linear segment for pomdp value function
% each row is a different state
% nSamples is the number of uniform random samples used to decide whether or
% not to trim a segment
% returns the indexes for the columns of x it recommends to trim
function indexes = prune(x,nSamples)
  nx = size(x,1);
  nSegments = size(x,2);
  % each column of p is a probability distribution
  p = rand(nx,nSamples);
  z = 1./sum(p,1);
  p = p.*kron(z,ones(nx,1));
  r = x'*p;
  % each column of r has nSegments and represents the value of each
  % segment at a particular row of p ( a sample belief)
  
  [m, am ] = max(r);
  freq= zeros(nSegments,1);
  for i=1: nSegments
    freq(i) = freq(i)+sum(am==i);
  end
  indexes = find(freq<1);
  
  