% Goal is to detect bad labelers and send them to train
% Actions:
%  - test: we know ground truth so we can tell if label is right
%  - accept: just accept the label as if it is correct
%  - train:  send labeler to trainingg the good arm

% Javier R. Movellan, 2020
clear
clf





file = 'labelingProblem.pdp';


nSamples = 10000 % 0 means exact solution more than 0 prunes based on nSamples
timeSteps = 20;
useSparse=0;


[w uhat] = InfomaxPOMDP(file,useSparse,nSamples,timeSteps);




%%% This plot assumes 2 states s it wont work with all the problems %%%%%%%%%%
if(size(w{1},1) ==2)

  subplot(3,1,1)
  hold on

  % first we create a matrix of belief states
  q= 0:0.0001:1;
  q= [q; 1-q];

  % We plot all the linear segments that
  % define the controller
  % we use the controller at first time step
  t=1;
  for i=1:size(w{t},2)

    s(:,i) = (w{t}(:,i))'*q;
    if(uhat{t}(i) ==1)
      plot(q(1,:),s(:,i),'b');
    else
        plot(q(1,:),s(:,i),'r--');
    end

  end
  title(' Blue= action 1, Red= action 2 ')
  xlabel('State1 Probability')
  ylabel('Value')


  % Now we plot the optimal value function
  subplot(3,1,2)


  [u v] = controller(w{1}, uhat{1}, q);
  plot(q(1,:), v,'b');
  xlabel('State1 Probability')
  ylabel('Value')

  % And now we plot the optimal action

  subplot(3,1,3)
  plot(q(1,:),u,'LineWidth', 4);

  xlabel('State1 Probability')
  ylabel('Optimal Action')
end
