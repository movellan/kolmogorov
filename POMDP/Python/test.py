
import numpy as np
from pomdp import *



# Option 1. read a parameter filename
# and compute a controller

q3 = pomdp('models/test.pomdp')
q3.computeController()
q3.save('models/test') #becomes 'case3.pkl'

# Option 2. load a pomdp object with
# as a pkl file, with a precomputed controller
p = loadPomdp('models/test.pkl')


p.display_parameters()



dx = 0.1

q = np.arange(0,1+dx,dx)
q = np.vstack((q,1-q))
r = np.transpose(q).dot(p.w)
r = r.transpose()
winners = np.argmax(r,axis=0)

from matplotlib import pyplot as plt


testU = 0
# RED TEST
s=0
for i in range(len(p.u[0])):
    u = p.u[0][i]
    if u == testU:
        plt.plot(q[0,:],r[i,:],'r-', label = p.actionNames[0] if s == 0 else "")
        s =1
testU =1
# BLUE ACCEPT
s=0
for i in range(len(p.u[0])):
    u = p.u[0][i]
    if u == testU:
        plt.plot(q[0,:],r[i,:],'b-', label = p.actionNames[1] if s == 0 else "")
        s=1
testU = 2
# GREEN TRAIN
s=0
for i in range(len(p.u[0])):
    u = p.u[0][i]
    if u == testU:
        plt.plot(q[0,:],r[i,:],'g-', label = p.actionNames[2] if s == 0 else "")
        s=1

plt.ylim([-20,0])
plt.xlabel('Prob Good Labeler')
plt.ylabel('Value')
plt.legend()  # Adds the legend to the plot
plt.show(block=False)
