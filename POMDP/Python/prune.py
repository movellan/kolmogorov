import numpy as np

def prune(w,dp =0.01):
    # for now this onluy works with
    # 2 state prob distributions


    p = np.arange(0,1+dp,dp)
    p = np.append(p[p<1.0],1.0)
    p = np.vstack((p,1-p))
    #each column of v is a sample of the posteior distribution

    r = np.transpose(p).dot(w)
    r= r.transpose()

    #import ipdb; ipdb.set_trace()
    winners = np.argmax(r,axis=0)

    nSegments = w.shape[1]
    deleteColIndexes = list(set(range(nSegments))- set(winners))
    #print winners
    #each cols of r has the action scores for a given sample of the posterior distribution
    return deleteColIndexes


def testPrune():
    p = np.array([[0.1, 0.3,0.5, 0.7,0.9],[0.9,0.7,0.5,0.3,0.1] ])

    w = np.array([
    [1.0/np.sqrt(82.0), 3.0/np.sqrt(58.0), 5.0/np.sqrt(50.0),7.0/np.sqrt(58.0),9.0/np.sqrt(82.0)],
    [9.0/np.sqrt(82.0), 7.0/np.sqrt(58.0), 5.0/np.sqrt(50.0),3.0/np.sqrt(58.0),1.0/np.sqrt(82.0)]
    ])

    r = np.transpose(p).dot(w)
    r = r.transpose()
    winners = np.argmax(r,axis=0)
    if sum(winners == [0,1,2,3,4]) == len(winners):
        print('Prune test passed')
    else:
        print('Prune test failed ')
