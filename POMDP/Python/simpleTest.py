import numpy as np
from pomdpParser import *
from prettytable import PrettyTable

filename_env = 'models/unitTest.pomdp'
m = POMDPEnvironment(filename_env)




cols=['Parameter', 'Value']
table= PrettyTable(cols)
table.add_row(['discount',m.discount])
table.add_row(['values',m.values])
table.add_row(['states',m.states])
table.add_row(['actions',m.actions])
table.add_row(['observations',m.observations])
print((table.get_string()))


nActions = len(m.actions)
nObservations = len(m.observations)
nStates = len(m.states)

print('')

print('----------- State Transition Probabilities -----------')
cols=['Action', 'SourceState','TargetState','TransitionProb']
table= PrettyTable(cols)
for i in range(nActions):
    for j in range(nStates):
        for k in range(nStates):
            table.add_row([m.actions[i],m.states[j],m.states[k],m.T[(i,j,k)]])
print((table.get_string()))



print('')
print('----------- Observation Probabilities -----------')
cols=['Action', 'State','Observation','TransitionProb']
table= PrettyTable(cols)
for i in range(nActions):
    for j in range(nStates):
        for k in range(nObservations):
            table.add_row([m.actions[i],m.states[j],m.observations[k],m.Z[(i,j,k)]])
print((table.get_string()))

print('')
print('----------- Rewards -----------')
cols=['Action', 'sourceState','targetState','Observation','Reward']
table= PrettyTable(cols)
for i in range(nActions):
    for j in range(nStates):
        for k in range(nStates):
            for l in range(nObservations):
                table.add_row([m.actions[i],m.states[j],m.states[k],m.observations[l],m.R[(i,j,k,l)]])
print((table.get_string()))
