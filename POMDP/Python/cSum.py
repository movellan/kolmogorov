import numpy as np

def cSum(x,y):
# cross Sum of two matrices with same rows
# z = [x_{.1}+ y_{.1}, x_{.1}+ y_{.2}, x_{.1}+ y_{.ny}, x_{.2}+y_{.1}, ...,x_{.nx}+y_{.ny}]
# were x_{.i},y_{.j} are the ith colum of x and jth colum of y

    rows1 =  np.shape(x)[0]
    rows2 = np.shape(y)[0]
    if rows1 != rows2:
        raise Exception("Input matrices have different number of rows")
    ny = np.shape(y)[1]
    nx = np.shape(x)[1]
    ny = np.shape(y)[1]
    c1 = np.kron(x,np.ones((1,ny)))
    c2 = np.kron(np.ones((1,nx)),y)
    return(c1+c2)


def test_cSum():
    x = np.array(
     [[0,1,2],
     [3,4,5]])

    y = np.array([[10,11],[12,13]])

    z = np.array( [[10,11,11,12,12,13],
                   [15,16,16,17,17,18]]).astype(int)

    s = cSum(x,y).astype(int)

    r = sum(sum(z==s))
    if r== 12:
        print('cSum test Passed')
    else:
        print('cSum test Failed')
