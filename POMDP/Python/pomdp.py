#  parameters from parser into Format
# similar used by Javier's Matlab POMDP solver
import numpy as np
from pomdpParser import *
from prune import *
from cSum import *
import pickle



"""
Compute receeding horizon POMDP controller
Input is a pomdp parameter file
For format see
   http://cs.brown.edu/research/ai/pomdp/examples/pomdp-file-spec.html
From discount factor (which has to be less than 1) we compute a time horizon T
Then compute a time dependent controller from time 0 to time T
and return the controller at time 0.
The controller consists of a matrix w and a list u.
w has as many rows as hidden states
and columns that corresponds to linear portions of the value function
u has as many elements as w has columns.

Given a posterior probability q over states (q is a belief state)
1- compute v=  q' * w
2- choose the column of v with maximum value. This is the expected value of being
in belief state q
3- choose the column of u corresponding to the column with maximum value in v.
This is the optimal action given belief state q



Notation:
X_t: state at time t
U_t: action at time t
Z_t: observation at time t
R_t: reward at time t
H_t = (Z_{1:t},U_{1:t-1}): observable history up to time t
Generative Model:
(X_t, U_{t-1})-> Z_t
(H_{t-1},Z_t) -> H_t
H_t -> U_t
(X_t, U_t,Z_t)-> R_t
(X_t, U_t)-> X_{t+1}
Returns:
    a: dictionary of state transition matrices
        a[u][x1,x2] = p(X_{t+1}=x2 | X_t = x1, U_t =u)
    b: a dictinary of matrices
        b[u,z][x] = p(Z_{t+1} = z | X_{t+1} =x, U_t =u)
    k: a dictionary of matrices
        k[u,z][x1,x2]= p(X_{t+1}=x2,Z_{t+1}=z | X_t=x1, U_t = u)
    r: a dictionary of vectors
        r[u][x]= E[R_{t+1} | X_{t+1} = x,  U_{t} =u]
"""



class pomdp():
    def __init__(self, paramFile='models/unitTest.pomdp'):
     self.paramFile= paramFile
     self.readParamFile(paramFile)


    """
     reads pomdp env file and output pomdp parameters
    Args:
        filename_env: a pompdp environment file. For format see
        http://cs.brown.edu/research/ai/pomdp/examples/pomdp-file-spec.html
    """

    def readParamFile(self,filename_env):
        m = POMDPEnvironment(filename_env)
        #creates self.a self.b
        self.gamma = m.discount
        self.nx = len(m.states)
        nx = self.nx
        self.stateNames = m.states
        self.nu = len(m.actions)
        nu = self.nu
        self.actionNames = m.actions
        self.nz = len(m.observations)
        nz = self.nz
        self.observationNames = m.observations
        a= {}
        for u in range(nu):
            a[u] = np.zeros((nx,nx))
            for x1 in range(nx):
                for x2 in range(nx):
                    a[u][x1,x2] = m.T[u,x1,x2]
        b={}
        for u in range(nu):
            for z in range(nz):
                b[u,z] = np.zeros(nx)
                for x in range(nx):
                    b[u,z][x] = m.Z[u,x,z]
        k={}
        for u in range(nu):
            for z in range(nz):
                k[u,z] = np.zeros((nx,nx))
                k[u,z]= a[u].dot(np.diag(b[u,z]))

        #import ipdb; ipdb.set_trace()
        r={}
        for u in range(nu):
            r[u] = np.zeros((nx,1))
            for i in range(nx):
                for j in range(nx):
                    for z in range(nz):
                        r[u][i] += a[u][i,j] * b[u,z][i]* m.R[u,i,j,z]


        self.a = a
        self.b = b
        self.k = k
        self.r = r
        return a,b,k,r

    def display_parameters(self):
        print("POMDP Parameters:")
        print(f"  Discount Factor (gamma): {self.gamma}")
        print(f"  States ({self.nx}): {self.stateNames}")
        print(f"  Actions ({self.nu}): {self.actionNames}")
        print(f"  Observations ({self.nz}): {self.observationNames}")
        print("  Transition Probabilities (a):")
        for x1 in range(self.nx):
            for u in range(self.nu):
                for x2 in range(self.nx):
                    print(f"    Probability( X(t+1) =  {self.stateNames[x2]} | X(t) = {self.stateNames[x1]}, U(t+1) = {self.actionNames[u]}) = {self.a[u][x1,x2]} ")
        print("  Observation Probabilities (b):")
        for x in range(self.nx):
            for u in range(self.nu):
                for z in range(self.nz):
                    print(f"    Probability( Z(t+1) =  {self.observationNames[z]} | X(t+1) = {self.stateNames[x]}, U(t+1) = {self.actionNames[u]}) = {self.b[u,z][x]} ")
        print("  Rewards (r):")
        for x in range(self.nx):
            for u in range(self.nu):
                print(f"    E[ R(t+1) | X(t+1) = {self.stateNames[x]}, U(t) = {self.actionNames[u]}] = {self.r[u][x]} ")

    # computes a finite horizon optimal controller and returns
    # the optimal policy for time 0.
    def computeController(self):
        # the time discount determines the time horizon
        # effective time horizon for discount gamma is (1+gamma)/(1-gamma)
        # we add 50 % to be safe
        if self.gamma ==1.0:
            print("Discount Factor needs to be less than 1")
            exit()
        T=  int(np.ceil(1.5*(self.gamma+1.0/(1-self.gamma ))))
        w={}
        w[T+1] = np.zeros((self.nx,1))
        uhat={}

        for t in range(T,-1,-1):
            print(t)
            w[t] = np.zeros((self.nx,1)) # stub to get tings started
            uhat[t] =np.zeros((1,1)) # stub to get tings started
            for u in range(self.nu):
                wu = np.zeros((self.nx,1))
                for z in range(self.nz):
                    wuz = self.gamma*self.k[u,z].dot(w[t+1])
                    wu = cSum(wu, wuz) # cross sums the set of columns
                #import ipdb; ipdb.set_trace()
                wu = cSum(self.r[u],wu)
                wuIndexes = u*np.ones((1,np.shape(wu)[1]))
                w[t] = np.concatenate((w[t],wu),axis=1)
                uhat[t]= np.concatenate((uhat[t],wuIndexes),axis=1)

            w[t] = np.delete(w[t],0,1) # get rid of the stub
            uhat[t] = np.delete(uhat[t],0,1) #get rid of the stub
            doPrune = True
            if doPrune:
                dp =0.01
                dIndexes = prune(w[t],dp)
                w[t] = np.delete(w[t],dIndexes,1)
                uhat[t] =  np.delete(uhat[t],dIndexes,1)
        # we use a receeding horizon controller we only need the initial case
        self.w = w[0]
        self.u = uhat[0]



# save and load as pickle file
    def save(self, filename):
        filename +='.pkl'
        with open(filename, 'wb') as output:
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)

def loadPomdp(filename):
    with open(filename, 'rb') as input:
        return pickle.load(input)



def compareSame(x,y):
    eps= 1e-10
    r = np.sum(np.abs(x-y))
    if r <eps:
        return 1
    else:
        return 0


#p = pompdp('models/unitTest.pomdp')
