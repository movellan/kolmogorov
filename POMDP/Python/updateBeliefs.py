from pomdp import *
"""
Notation:
X_t: state at time t
U_t: action at time t
Z_t: observation at time t
R_t: reward at time t
H_t = (Z_{1:t},U_{1:t-1}): observable history up to time t
Generative Model:
(X_t, U_{t-1})-> Z_t
(H_{t-1},Z_t) -> H_t
H_t -> U_t
(X_t, U_t,Z_t)-> R_t
(X_t, U_t)-> X_{t+1}

p_t = p(X_t | Z_{1:t-1},U_{1:t-1})
Observe Z_t
Update belief
q_t = p(X_t | Z_{1:t},U_{1:t-1})
Generate U_t
U_t = c(q_t)
Update belief
p_{t+1} = p(X_{t+1} | Z_{1:t},U_{1:t})
Observe Z_{t+1}
Update belief
q_{t+1} =p(X_{t+1} | Z_{1:t+1},U_{1:t})
U_{t+1} = c(q_{t+1})


\pi is state prior
c: controller
Initialization:
Choose U_0 with c(\pi),
Sample X_1 from \pi
Sample Z_1 from b(X_1,U_0)
Update belief: p_1 = p(X_1| Z_1,U_0)
Iteration:
Given p_t = p(X_t| Z_{1:t}, U_{1:t-1})
Choose U_t with c(p_t)
Sample X_{t+1} from a(X_t, U_t)
Sample Z_{t+1} from b(X_{t+1},U_t)
Update belief: p_{t+1} = p(X_{t+1}| Z_{1:t+1},U_{1:t    })


"""

p = pomdp()
