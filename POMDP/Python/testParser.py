from pomdpParser import *
from pomdp import *


def testParser():
    p = pomdp()
    #import ipdb; ipdb.set_trace()
    import copy
    ek = copy.deepcopy(p.k)

    ek[0,0]= np.array([[0.76,0.18],[0,0.9]])
    ek[0,1]=np.array([[0.04,0.02],[0,0.1]])
    ek[0,2]= np.array([[0,0],[0,0]])
    ek[1,0]=np.array([[0,0],[0,0]])
    ek[1,1]=np.array([[0,0],[0,0]])
    ek[1,2]=np.array([[0.8,0.2],[0,1]])

    er = copy.deepcopy(p.r)
    er[0]= np.array([[-1],[-1]])
    er[1]= np.array([[1],[-4]])
    er[2]= np.array([[-5],[-5]])


    s = 0
    for i in list(p.k.keys()):
        s+= compareSame(p.k[i],ek[i])
    if s == len(list(p.k.keys())):
        print('Parser a,b,k matrices test passed')
    else:
        print('Parser a,b,k matrices test failed')
    s=0
    for u in list(p.r.keys()):
        s+=compareSame(p.r[u],er[u])
    if s == len(list(p.r.keys())):
        print('Parser r matrices test passed')
    else:
        print('Parser r matrices test failed')
