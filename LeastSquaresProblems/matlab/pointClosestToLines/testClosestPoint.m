clear
% each column of u is a vector that specifies the
% direction of a line

u=[1 0 0;  1 0 0]';
% each column of b is a point on the line

b= [0 1 0; 0 0 0]';

y = closestPoint(u,b)
