clear
% goal is to estimate a 3x3 covariance matrix from observations of
% the 2x2 covariances of different projections of the 3x3 matrix


nP=3; % numver of projections
for i=1:nP % the projection matrices
  p{i} = randn(2,3);
end

% constraint matrix, construct a symmetric 3x3 matrix from 6 parameters
k(1,:)=[1 0 0 0 0 0];
k(2,:)=[0 1 0 0 0 0];
k(3,:)=[0 0 1 0 0 0];
k(4,:)=k(2,:);
k(5,:)=[0 0 0 1 0 0];
k(6,:)=[0 0 0 0 1 0];
k(7,:)=k(3,:);
k(8,:)=k(6,:);
k(9,:)=[0 0 0 0 0 1];

% the covariance matrix we are trying to estimate
sigma = randn(3,3); sigma= sigma*sigma';

% the observed projections of sigma
for j=1:nP
  c{j} = p{j}*sigma*p{j}';
end

% now we solve the least squares problem
% to find sigma
a =zeros(6,6);
b=zeros(6,1);

for j=1:nP
  x = kron(p{j},p{j})*k;
  xx = x'*x;
  a =a +xx;
  y =reshape(c{j}',4,1);
  xy = x'*y;
  b = b+xy;
end

thetaHat = pinv(a)*b;
sigmaHat = k*thetaHat;


sigmaHat = reshape(sigmaHat,3,3)
sigma