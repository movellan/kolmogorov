\documentclass[12pt]{article}
%\usepackage{Mynips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath,amsthm}
\usepackage[pdftex]{graphicx}
%\usepackage{psboxit}
%\usepackage{fancybox}
%\usepackage[square,numbers,sort]{natbib} 
%\usepackage[round,sort]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}

 \title{Primer on the Discrete Fourier Transform\\
{}}
\author{ }
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\pfrac}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\ppfrac}[3]{\frac{\partial^2 #1}{\partial #2 \partial #3}}




\begin{document}
\maketitle

\begin{center} 
 Copyright \copyright{}  2004 Javier
R. Movellan. \end{center} 
 
\newpage







\section{The Discrete Fourier Transform }


Let $h[0], h[1], \cdots h[N-1]$ be a sequence of complex numbers. It can be shown (see Appendix) that the sequence can be expressed
as a weighted sum of complex sinusoids
\begin{align}
&h[n]& = \frac{1}{N} \sum_{k=0}^{N-1} a[k] e^{j 2 \pi  f_n k}\\
&f_n = \frac{n}{N}
\end{align}
where
\begin{align}
&a[k] \bydef  \sum_{n=0}^{N-1} h[n] e^{-j 2 \pi f_k \frac{k}{N} n}\\
&f_k= \frac{k}{N}
\end{align}
The sequence $a_0,\cdots, a_{N-1}$ is called the Discrete Fourier Transform (DCT) of the sequence $h[0],\cdots, h[n]$. The frecuencies $f_0, \cdots, f_{N-1}$ are measured in cycles per sample. So, for example $f_k = k/N$ is $k$ cycles every $N$ samples. 

\section{Discrete Time Sampling Interpretation}


We can think of $h[0],\cdots,h[N-1]$ as samples from a continuous time
function observed at times, $0,\Delta_t, \cdots, (N-1) \Delta_t$. We approximate the underlying continuous time function by the function $h()$ defined as follows:
\begin{align}
h( t )& = \frac{1}{N} \sum_{k=0}^{N-1} a(f_k) e^{j2 \pi f_k t}\\
a(f_k)  & \bydef  
\sum_{n=0}^{N-1} h(t_n) e^{-j 2 \pi f_k t_n }\\
\end{align}
where
\begin{align}
f_k &\bydef \frac{k}{ N \Delta_t}\\
t_n &\bydef n\Delta_t
\end{align}
Note that $a(f_k) = a[k]$ for $k=0,1,\cdots, N$ and  $h(n\Delta_t) =h[n]$, for $n=0,1,\cdots,N-1$. Moreover $h()$
is one an infinite possible set of continuous time signals that may
have generated the sequence $h[]$.

\subsection{Periodicity in the Time Domain}

The approximating continuous time function $h()$ is periodic with
period $T=N \Delta_t$. To see why note
\begin{align}
h(t+T) &=\frac{1}{N} 
\sum_{k=0}^{N-1} a(f_k) e^{j2 \pi f_k (t+T) } \\
&=\frac{1}{N}\sum_{k=0}^{N-1} a(f_k) e^{j2 \pi k  f_k t}\;
e^{j2\pi f_k T} \\
&=\frac{1}{N}\sum_{k=0}^{N-1} a(f_k) e^{j2 \pi k  f_k t}\\
&=h(t)
\end{align} 
where we used the fact that 
\begin{align}
e^{j2\pi f_k T} =e^{j2\pi k}  = \cos( 2 \pi k) + j \sin(-2 \pi k) = 1
\end{align}
Thus, the continuous time  approximating
signal $h()$ is periodic with {\em fundamental frequency} $F =1/T=  1/(N
\Delta_t)$ and with $N-1$ consecutive harmonics of the fundamental
frequency. We note the fundamental frequency, and harmonics, are
measured in cycles per unit of time, where the unit of time is the
same as the one used for the sampling period $\Delta_t$. For example,
if $\Delta_t$ is measured in seconds, then $F$ is measured in cycles
per second (i.e., Hertz). If we choose $\Delta_t$ itself as the unit
of measurement then $\Delta_t=1$ and $F$ is measured in cycles per
sample. So $F=1/N$ would be one cycle every $N$ samples. 

\subsection{Periodicity in the Frequency Domain}
Let $S= 1/\Delta_t$ be the sampling frequency. Then
\begin{align}
a(f - S) &= \sum_{n=0}^{N-1} h(t_n) e^{-j 2 \pi (f-S) t_n} \nonumber\\
&= \sum_{n=0}^{N-1} h(f)  e^{-j 2 \pi f t_n}  e^{j 2\pi 
s t_n}\nonumber \\
&= a(f_k)
\end{align}
where we used the fact that 
\begin{align}
e^{j 2\pi S t_n}  =e^{j2\pi n}  = \cos( 2 \pi n) + j \sin(2 \pi n) = 1
\end{align}
 Thus $f_k$ is equivalent to 
\begin{align}
 f_k = f_k - S 
\end{align}
Note a negative frequency is simply a 90 degree shift change with
respect to a positive frequency
\begin{align}
e^{j 2\pi (-f)t}&= \cos(-2 \pi f  t) + j \sin(-2 \pi f t) \nonumber\\
&= 
\cos(2 \pi f  t -\pi/2) + j \sin(2 \pi f t -\pi/2) \nonumber\\
&= 
e^{j 2\pi ft +\pi/2}
\end{align}

\subsection{Standard Representation}
By convention, if $f_k$ is larger than the Nyquist frequency ( 1/2 the
sampling frequency) we use the corresponding negative frequency, i.e.
\begin{align}
\text{if}\; f_k > \frac{1}{2 \Delta_t}\; \text{then}\; f_k \leftarrow f_k - \frac{1}{ \Delta_t}
\end{align}
 This way the frequencies in the DFT are never larger than the Nyquist limit.   For $N$ odd this is organized as
 follows
\begin{equation}\nonumber
\begin{array}{|c|c|}\hline
\text{Index} &\text{Frequency}\\\hline
0 &  0\\\hline
1 &  F \\\hline
2 &  2 F \\\hline
\cdots&\\\hline
\frac{N-1}{2}  & \frac{N-1}{2} F  \\\hline
\frac{N-1}{2}  +1 & ( 1 - \frac{N+1}{2})  F  \\\hline
\frac{N-1}{2}  +2 & ( 2 - \frac{N+1}{2})  F  \\\hline
\cdots&\\\hline
N-2  &   -2 F  \\\hline
N-1 &  - F  \\\hline
\end{array}
\end{equation}
where $F$ is 1 cycle every $N$ samples. 
For example, it $N=5$ the DFT would be organized  as follows
\begin{equation}\nonumber
\begin{array}{|c|c|}\hline
\text{Index} &\text{Frequency}\\\hline
0 &  0\\\hline
1 &  F\\\hline
2 &  2 F\\\hline
3 & -2 F\\\hline
4 &  F\\\hline
\end{array}
\end{equation}
If $N$ is even the vector is organized as follows
\begin{equation}\nonumber
\begin{array}{|c|c|}\hline
\text{Index} &\text{Frequency}\\\hline
0 &  0\\\hline
1 &  F \\\hline
2 &  2 F \\\hline
\cdots&\\\hline
\frac{N}{2}  & \pm \frac{N}{2} F  \\\hline
\cdots&\\\hline
N-2  &   -2 F  \\\hline
N-1 &   F  \\\hline
\end{array}
\end{equation}
 For example, if $N=6$ the vector would be organized as follows
\begin{equation}\nonumber
\begin{array}{|c|c|}\hline
\text{Index} &\text{Frequency}\\\hline
0 &  0\\\hline
1 &  F\\\hline
2 &  2 F\\\hline
3 & \pm 3 F\\\hline
4 &  -2 F\\\hline
5 &  -1 F\\\hline
\end{array}
\end{equation}

We can combine the indexing for the odd and even cases using the
following algorithm

\begin{verbatim}
F0 = 1/(N*dt);
for n=0:N-1 // We use zero offset vectors. 
  if(n < N/2)
    f(n) = n*F0; % frequecy in cycles per unit of time
  else
    f(n) = (n -N)*F0; % frequency in cycles per unit of time
  end
end




\end{verbatim}



\subsection{Example}
Consider the sequence $h[n]=n$ for $n=0,1,2,3$. We consider this as observations from a continuous time function, with  sampling period $D_t=1$ sec. Figure~\ref{fig:exam1} shows in red, the continuous time function approximation corresponding to the DFT. We note that it is periodic and that it matches the observed samples at the appropriate times.  
 \begin{figure}[bht]\label{fig:energyFilter}
\begin{center}
\includegraphics[width=5in]{Media/example1}
\end{center}
  \caption{Continuous time approximating function (red) found by the DFT of a 4 points observation sequence (blue dots). } 
 \end{figure}

\section{Convolutions}
The circular convolution of two sequences $x[0],\cdots,x[N-1]$, $h[0], \cdots,h[N-1]$ is another sequence $y[0],\cdots,y[N-1]$ defined as follows
\begin{align}
y[n] = \sum_{i=0}^{N-1}h[i]  x[n-i ] 
\end{align}
if $n-i$ is negative, then $x[n-i]$ is defined as $x[N-(n-i)]$. For example, for  for $N=3$ we get
\begin{align}
y[0] = h[0] x[0] + h[1]x[2] + h[2] x[1]\\
y[1] = h[0] x[1] + h[1]x[0] + h[2] x[2]\\
y[2] = h[0] x[2] + h[1]x[1] + h[2] x[0]\\
\end{align}
Note this is like a linear convolution with  $x$ left padded
with $x[-1]=x[2],x[-2]=x[1]$

It can be shown that the DFT of $y$ is the product of the DFT of $x$
and the DFT of $h$.


If we want to peform a linear convolution between a sequence
$x[0],\cdots, x[N-1]$, $h[0],\cdots, h[N-1]$, we right-pad both
sequences with zeros till they both are of length $N+M-1$, perform a
circular convolution and select the first $N$ elements of the
sequence. This is equivalent to a linear convolution with zero padding. 


\section{Relationship to other Fourier Transforms}


The DFT operates on finite, discrete time sequences, thus it is ideal for digital signal processing applications. The Fast Fourier Transform (FFT) is an algorithm for efficient computation of the DFT. The DFT provides the coefficients for the Continuous Time Fourier Transform of the continuous time function used to approximated the observed discrete time samples. The Discrete time Fourier transform DFT works with infinite sequences, and can be seen as the limit of the DFT as $N\to \infty$. 

\newpage
\section{Appendix}
\subsection{The DFT}
A sequence $h[0], \cdots, h[N-1]$ of complex numbers can be represented as a sum of complex sinusoids 
\begin{align}
h[n] =\frac{1}{N}  \sum_{k=0}^{N-1} a[k] e^{j 2 \pi \frac{k}{N} n}
\end{align}
where
\begin{align}
a[k] =  \sum_{n=0}^{N-1} h[n]  e^{-j 2 \pi \frac{n}{N} k}
\end{align}
\paragraph{Informal Proof:}

\begin{align}
&\frac{1}{N} \sum_{k=0}^{N-1} a[k] e^{j 2 \pi \frac{k}{N} u} = \frac{1}{N} \sum_{k=0}^{N-1} 
  \sum_{n=0}^{N-1} h[n]  e^{-j 2 \pi \frac{n}{N} k}
 e^{j 2 \pi \frac{k}{N} u}\\
&= 
\frac{1}{N} \sum_{k=0}^{N-1} 
  \sum_{n=0}^{N-1} h[n]  e^{-j 2 \pi  k\frac{n-u}{N} }
=\frac{1}{N} \sum_{k=0}^{N-1} h[u]
+ 
 \sum_{n\neq u} h[n] \sum_{k=0}^N e^{-j 2 \pi  \frac{n-u}{N}k }\\
& h[u]
+ 
 \sum_{n\neq u} h[n]  \sum_{k=0}^Ne^{-j 2 \pi  \frac{k}{N} } = h[u]
 \end{align}
For the last two steps we used the fact that $e^{j 2\pi } = e^{j 2 \pi
  (u-n)}$.  Moreover note that the complex numbers $e^{j 2\pi 0 } =
e^{j 2 \pi 1/N} \cdots = e^{j 2 \pi (N-1)/N}$ are vectors of length 1,
equidistantly located around the complex unit circle. Therefore their
sum is at the center of the complex unit circle, i.e.,
\begin{align}
\sum_{k=0}^Ne^{-j 2 \pi  \frac{k}{N} } = 0
\end{align}



\section{Using the DFT in Python}

Check the document ``notebook.pdf'' in this directory.
\section{Convolutions in Python}
We want to convolve a sequence $x[0],\cdots,x[N-1]$ with another
sequence $h[0],\cdots,h[M-1]$, with $M\leq N$. The sequence $x$ may
represent a signal and the sequencre $k$ the impulse response of a
system. We let $c$ be the index representing the origin of the
impulse response. If $c>0$ then the filter is not causal.  We want the output sequence to be as follows
\begin{align}
y[n] &= x[n] h[c] + x[n-1] h[c+1] + x[n-M+c+1] h[M-1]\nonumber\\
 &+ x[n+1] h[c-1] + x[n+c]h[0]
\end{align}
If we want for the output sequence to be of the same size as the input
sequence we need to have $y[0]$ and $y[N-1]$. To get $y[0]$ we need
pad $x$ to the left with $M-c-1$  elements
\begin{align}
x[-1],\cdots,x[c+1-M] 
\end{align}
and to the $y[N-1]$ we need to pad $x$ to the right with $c-1$ elements
\begin{align}
x[N],\cdots,x[N-1+c]
\end{align}
This can be achieved in Python as follows

\begin{verbatim}

def filter1D(x,h,c,padMode):
# filter the sequence x using a filter with inpulse response sequence h. 
# c is the index in r that points to the  origin of the impulse response sequence r. 
# c is the 0-offset index for the origin of h  
#padMode can be: 'constant','edge','linear_ramp','mean','median','minimum','symmetric','wrap'

# output is a sequence y of same len as x.
# to this end we use left right padding of x
# Example: x = [1,1,1,1], r=[1,2,-1], c=0
# xpadded=[0,0,1,1,1,1]
# y=[ 1,3,2,2]
# Example: x = [1,1,1,1], r=[1,2,-1], c=1
# xpadded=[0,1,1,1,1,0]
# y=[ 3,2,2,1]
 #Example: x = [1,1,1,1], r=[1,2,-1], c=2
# xpadded=[1,1,1,1,0,0]
# y=[ 2,2,1,-1]

	M = len(h)
	leftPad= M-1-c
	rightPad =c
	x=pad(x,(leftPad,rightPad),'constant')
	y=fftconvolve(x,h,'valid')
	return y

\end{verbatim}



\subsection{Using the Fast Fourier Transform in Matlab} 



\begin{verbatim}
% Illustrates how to get the discrete time FFT of a Gaussian function and
% how to interpret it with respect to the continuous time Fourier Transform
%
% Copyright @ Javier R. Movellan, UCSD, 2009

clear
clf

% (1) Choose  a unit of measurement for the time axis, e.g. seconds.
% (2) Choose the sampling period using the chosen units
dt= 0.1; % time in seconds.If dt =1; the unit of measurement is the time
         % between samples and the frequency is measured in cycles per
         % sample. 
% (3)  Choose the number of samples
N =35;

% (4) The fft assumes the samples  come from a periodic signal starting at
% t=0, dt, 2 dt, ... (N-1) dt. The period of the signal is
% nSamples*dt. Thus implicitly we are defining how the signal would
% behave for negative values of t. We have that s(t  - N dt) = s(t). Thus
% s((N-k) dt ) = s( (N-k -N ) dt ) = s(-kdt)

for n=1:N
  if (n-1< N/2) 
    t(n) = (n-1)* dt; % we start at time 0
  else
    t(n) = ((n-1) - N)*dt;
  end
end

% (5) We now populate our vector with the signal we want to get the fft
% for.

a= 3;
s = exp(-pi*(a*t).^2);

% (6) Get the fft

S = fft(s);

% (6) The k element of the fft vector is the continuous fourier transform of the
% signal evaluated at the frequency f = (k-1)* 1/(N dt), so S[1] = F(0), S[2]
% = F(1/dt), S(3) = F(2/dt).  However the fft is also periodic so F( f) =
% F(f+ 1/dt) = F(1 - 1/dt). Thus S[N-k] = F((N-k+N)/(N dt)) = F (-k /(Ndt))
% . Below we rearrange things so the negative frequencies are to the left
% and the positive frequencies to the right

for n=1: N
  if(n-1 < N/2)
    f(n) = (n-1)/(N*dt); % frequency in cycles per unit of time
  else
    f(n) = ((n-1)-N)/(N*dt); % frequency in cycles per unit of time
  end
end

% Plot and verify the results

subplot(6,1,1)
scatter(t,s)
xlabel('Time in Seconds')
% To verify things we'll plot the half magnitude interval
line(0.4697 *[1/a 1/a] , [0 max(s)])
line(-0.4697 *[1/a 1/a] , [0 max(s)])
line([min(t), max(t)]  ,max(s)* [0.5 0.5])

subplot(6,1,2)
scatter(f,abs(S))
xlabel('Frequency in Hertz')
ylabel('Magnitude')
% To verify things we'll plot the half magnitude interval
line(0.4697 *[a a] , [0 max(S)])
line(-0.4697 *[a a] , [0 max(S)])
line([min(f), max(f)]  ,max(S)* [0.5 0.5])



% (7) If real(S) or imag(S) is very small the phase spectrum runs into
% numerical issues with matlab. Just make very small numbers exactly zero

tiny= 10^(-10);
for n=1:N
  if(abs(real(S(n)) < tiny)) S(n) = j*imag(S(n)); end
  if(abs(imag(S(n)) < tiny)) S(n) = real(S(n)); end
end

subplot(6,1,3)
scatter(f,phase(S))
xlabel('Frequency in Hertz')
ylabel('Phase')




% (8) The matlab functions fftshift and ifftshift  can be used to
% simplify all the index shifting.  Here is how to use them

subplot(6,1,4)

t2 = dt*(- floor(N/2):floor(N/2));
s2 = exp(-pi*(a*t2).^2);
s2= ifftshift(s2);
% we Note that g2 =s 
xlabel('Double Check')
S2 = fft(s2);
S2= ifftshift(S2);
% verify we get same results
subplot(6,1,4)
scatter(1:N, fftshift(s2))
xlabel('Double Check')
subplot(6,1,5)
scatter(1:N, abs(S2))
xlabel('Double Check')
subplot(6,1,6)
for n=1:N
  if(abs(real(S2(n)) < tiny)) S2(n) = j*imag(S2(n)); end
  if(abs(imag(S2(n)) < tiny)) S2(n) = real(S2(n)); end
end
scatter(1:N, phase(S2))
xlabel('Double Check')
\end{verbatim}
\newpage

\subsection{Using the Matlab conv function } 

\begin{verbatim}

% Filters a signal using the conv function
%
% s : The signal
% k : The filter kernel (time flipped impulse response
% c:  Integer with index for origin  of he kernel, points to 
%     the right of the origin are non-causal.
% z:  The output of the filter to each point of s. The outputs for the
%     left and right limits of s are obtained by zero padding s. 
%
%
% Example1; Filter a signal s using a causal ramp filter with 4 time steps
% z = doFilter(s, 1:4, 4)
%
% Example2; Filter a signal s using a ramp filter that looks 3 steps
% into the future
% z = doFilter(s, 1:4, 1)
%
% Copyright Javier R. Movellan,2009, UCSD
function z = doFilter(s,k,c)
  

z=conv(s,fliplr(k)); % note we need to convolve with the impulse response,
                     % which is the time flipped version of the kernel


%Matlab automatically pads s with zeros to the left and right. 
% We now get rid of the response of the filter at times other than the
% times of the original data

z(1:(length(k)-c))=[];
z(end-(c-2):end)=[];

\end{verbatim}



\end{document}
