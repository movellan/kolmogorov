from numpy import *
from scipy.signal import *

def filter1D(x,h,c,padMode):
# filter the sequence x using a filter with inpulse response sequence h. 
# c is the index in r that points to the  origin of the impulse response sequence r. 
# c is the 0-offset index for the origin of h  
#padMode can be: 'constant','edge','linear_ramp','mean','median','minimum','symmetric','wrap','reflect'
# constant padding = zero padding
# 
# output is a sequence y of same len as x.
# to this end we use left right padding of x
# below are some examples using zero padding

# Example: x = [1,1,1,1], h=[1,2,-1], c=0
# xpadded=[0,0,1,1,1,1]
# y=[ 1,3,2,2]
# Example: x = [1,1,1,1], h=[1,2,-1], c=1
# xpadded=[0,1,1,1,1,0]
# y=[ 3,2,2,1]
 #Example: x = [1,1,1,1], h=[1,2,-1], c=2
# xpadded=[1,1,1,1,0,0]
# y=[ 2,2,1,-1]

	M = len(h)
	leftPad= M-1-c
	rightPad =c
	x=pad(x,(leftPad,rightPad),padMode)
	print x
	y=fftconvolve(x,h,'valid')
	return y
