%Illustrates how to get the DFT of a signal (in this case a Gaussian
%centered at zero). Illustrates numerical problems with phase spectrum.

x1 = -0.6:0.001:0.6;
x = ifftshift(x1);
testx = exp(-((50*x).^2));

tftestxinit = fft(testx);
tftestx = fftshift(tftestxinit);

%-- Since phase spectrum is atan(real/imag) we can get numerical problems
%when imag is close to zero but non-zero. Matlab also does weird things
%if real is very small but not zero and imag is very small but not
%zero. To avoid these problems we zero very small numbers

hz = tftestx;

hzr = real(tftestx);
hzi = imag(tftestx);
index = find(abs(hzr) < 1e-10);
hzr(index) = 0;
index = find(abs(hzi) < 1e-10);
hzi(index) = 0;
tftestm = complex(hzr, hzi);



abstftestx = abs(tftestm);
angletftestx = angle(tftestm);

f = 1000*(-600:600)/1201;
figure;plot(f,abstftestx)
title('absolute frequency content of testx')
xlabel('f (1/m)')
ylabel('magnitude of the dft')

figure;plot(f,angletftestx)
title('angular frequency content of testx')
xlabel('f (1/m)')
ylabel('phase of the dft')
