% Illustrates how to get the discrete time FFT of a Gaussian function and
% how to interpret it with respect to the continuous time Fourier Transform
%
% Copyright @ Javier R. Movellan, UCSD, 2009

clear
clf

% (1) Choose  a unit of measurement for the time axis, e.g. seconds.
% (2) Choose the sampling period using the chosen units
dt= 0.1; % time in seconds.If dt =1; the unit of measurement is the time
         % between samples and the frequency is measured in cycles per
         % sample. 
% (3)  Choose the number of samples
N =35;

% (4) The fft assumes the samples  come from a periodic signal starting at
% t=0, dt, 2 dt, ... (N-1) dt. The period of the signal is
% nSamples*dt. Thus implicitly we are defining how the signal would
% behave for negative values of t. We have that s(t  - N dt) = s(t). Thus
% s((N-k) dt ) = s( (N-k -N ) dt ) = s(-kdt)

for n=1:N
  if (n-1< N/2) 
    t(n) = (n-1)* dt; % we start at time 0
  else
    t(n) = ((n-1) - N)*dt;
  end
end

% (5) We now populate our vector with the signal we want to get the fft
% for.

a= 3;
s = exp(-pi*(a*t).^2);

% (6) Get the fft

S = fft(s);

% (6) The k element of the fft vector is the continuous fourier transform of the
% signal evaluated at the frequency f = (k-1)* 1/(N dt), so S[1] = F(0), S[2]
% = F(1/dt), S(3) = F(2/dt).  However the fft is also periodic so F( f) =
% F(f+ 1/dt) = F(1 - 1/dt). Thus S[N-k] = F((N-k+N)/(N dt)) = F (-k /(Ndt))
% . Below we rearrange things so the negative frequencies are to the left
% and the positive frequencies to the right

for n=1: N
  if(n-1 < N/2)
    f(n) = (n-1)/(N*dt); % frequency in cycles per unit of time
  else
    f(n) = ((n-1)-N)/(N*dt); % frequency in cycles per unit of time
  end
end

% Plot and verify the results

subplot(6,1,1)
scatter(t,s)
xlabel('Time in Seconds')
% To verify things we'll plot the half magnitude interval
line(0.4697 *[1/a 1/a] , [0 max(s)])
line(-0.4697 *[1/a 1/a] , [0 max(s)])
line([min(t), max(t)]  ,max(s)* [0.5 0.5])

subplot(6,1,2)
scatter(f,abs(S))
xlabel('Frequency in Hertz')
ylabel('Magnitude')
% To verify things we'll plot the half magnitude interval
line(0.4697 *[a a] , [0 max(S)])
line(-0.4697 *[a a] , [0 max(S)])
line([min(f), max(f)]  ,max(S)* [0.5 0.5])



% (7) If real(S) or imag(S) is very small the phase spectrum runs into
% numerical issues with matlab. Just make very small numbers exactly zero

tiny= 10^(-10);
for n=1:N
  if(abs(real(S(n)) < tiny)) S(n) = j*imag(S(n)); end
  if(abs(imag(S(n)) < tiny)) S(n) = real(S(n)); end
end

subplot(6,1,3)
scatter(f,angle(S))
xlabel('Frequency in Hertz')
ylabel('Phase')




% (8) The matlab functions fftshift and ifftshift  can be used to
% simplify all the index shifting.  Here is how to use them

subplot(6,1,4)

t2 = dt*(- floor(N/2):floor(N/2));
s2 = exp(-pi*(a*t2).^2);
s2= ifftshift(s2);
% we Note that g2 =s 
xlabel('Double Check')
S2 = fft(s2);
S2= ifftshift(S2);
% verify we get same results
subplot(6,1,4)
scatter(1:N, fftshift(s2))
xlabel('Double Check')
subplot(6,1,5)
scatter(1:N, abs(S2))
xlabel('Double Check')
subplot(6,1,6)
for n=1:N
  if(abs(real(S2(n)) < tiny)) S2(n) = j*imag(S2(n)); end
  if(abs(imag(S2(n)) < tiny)) S2(n) = real(S2(n)); end
end
scatter(1:N, angle(S2))
xlabel('Double Check')
