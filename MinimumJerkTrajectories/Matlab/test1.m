% Compute the minimum jerk trajectory between two points.
% Copyright Javier R. Movellan UCSD 2011
clear

%set initial position velocity and acceleration (3 dimension space)
x0 =[0  0 0]';
dx0 =[0  0 0]';
ddx0 =[0 0 0]';
% set the terminal position velocity and acceleration
xT =[ 1 0 0]';
dxT= [0 0 0]';
ddxT=[0 0 0]';
% set the flight time
T=1;
aMinJerk= zeros(3,6); % 3 spatial dimensions 6 coefficients per dimension
% compute the minimum jerk coefficients. 
for k=1:3;
  aMinJerk(k,:)= minimumJerk(x0(k),dx0(k),ddx0(k),xT(k),dxT(k),ddxT(k),T);
end

aMinAcc= zeros(3,4); % 3 spatial dimensions 4 coefficients per dimension
% compute the minimum acceleration coefficients. 
for k=1:3;
  aMinAcc(k,:)= minimumAcceleration(x0(k),dx0(k),xT(k),dxT(k),T);
end



t= linspace(0,T,200);
n = length(t);
xmj = zeros(3,n);
dxmj = zeros(3,n);
xma = zeros(3,n);
dxma = zeros(3,n);
% get the minimum jerk and minimum acc trajectories and velocities 
for( k=1:3)
  xmj(k,:) = aMinJerk(k,1) + aMinJerk(k,2)*t + aMinJerk(k,3)*t.^2+ aMinJerk(k,4)*t.^3 + aMinJerk(k,5)*t.^4+ aMinJerk(k,6)*t.^5;
  dxmj(k,:) =  aMinJerk(k,2) + 2*aMinJerk(k,3)*t+ 3*aMinJerk(k,4)*t.^2 ...
      + 4*aMinJerk(k,5)*t.^3+ 5*aMinJerk(k,6)*t.^4;
  
  xma(k,:) = aMinAcc(k,1) + aMinAcc(k,2)*t + aMinAcc(k,3)*t.^2+aMinAcc(k,4)*t.^3;
  
  dxma(k,:) =  aMinAcc(k,2) + 2*aMinAcc(k,3)*t+ 3*aMinAcc(k,4)*t.^2;
    

  
end
subplot(211)

plot(t,xmj(1,:),t,xma(1,:))

subplot(212)

plot(t,dxmj(1,:),t,dxma(1,:))


