function a =  minimumAcceleration(x0, dx0,xT,dxT,T)
% Compute a point to point minimum accelartion  trajectory 
% x0 dx0 ddx0 are the location and velocity  at the
% start point
% xT dxT  are the target location velocity 
% T is the time required to move from the start point to the target point
%
% The solution is a 4-D vector of coefficients a
% The minimum jerk trajectory takes the form
% x_t = \sum_{k=1}^3 a_k t^(k-1), for 0\leq t \leq T
%
%
% Copyright Javier R. Movellan UCSD 2011



% The minimum acceleration trajectory is a fourth order polinomial. We just
% need to compute the 4 coefficients of this polinomial. 


T2 = T*T; T3 = T2*T; 
a = zeros(4,1);
a(1) = x0;
a(2) = dx0;

b= [T2 T3 ; 2*T 3*T2];
c = [ xT - a(1) - a(2)*T; dxT - a(2) ];
a(3:4,1)=pinv(b)*c;

