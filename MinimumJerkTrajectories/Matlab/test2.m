% Compute the minimum jerk trajectory between two points.
% Copyright Javier R. Movellan UCSD 2011
clear

%set initial position velocity and acceleration (3 dimension space)
x0 =[0  0 0]';
dx0 =[0.25  -0.25 0]';
ddx0 =[0 0 0]';
% set the terminal position velocity and acceleration
xT =[ 2 1 0]';
dxT= [0 0 0]';
ddxT=[0 0 0]';
% set the flight time
T=10;
a= zeros(3,6); % 3 spatial dimensions 6 coefficients per dimension
% compute the minimum jerk coefficients. 
for k=1:3;
  a(k,:)= minimumJerk(x0(k),dx0(k),ddx0(k),xT(k),dxT(k),ddxT(k),T);
end

%The minimum jerk
% trajectory from time t=0 to time t=T is as follows
% x_t = \sum_{k=1}^6 a_k t^{k-1}
t= linspace(0,T,200);

n = length(t);
x = zeros(3,n);
dx = zeros(3,n);
ddx = zeros(3,n);
dddx = zeros(3,n);
ddddx = zeros(3,n);
dddddx = zeros(3,n);

% get the minimum jerk trajectory, velocity, acceleration ...
for( k=1:3)
  x(k,:) = a(k,1) + a(k,2)*t + a(k,3)*t.^2+ a(k,4)*t.^3 + a(k,5)*t.^4+ a(k,6)*t.^5;
  dx(k,:) =  a(k,2) + 2*a(k,3)*t+ 3*a(k,4)*t.^2 + 4*a(k,5)*t.^3+ 5*a(k,6)*t.^4;
  ddx(k,:) =   2*a(k,3)+ 6*a(k,4)*t + 12*a(k,5)*t.^2+ 20*a(k,6)*t.^3;
  dddx(k,:) =    6*a(k,4) + 24*a(k,5)*t+ 60*a(k,6)*t.^2;
  ddddx(k,:) = 24*a(k,5) + 120*a(k,6)*t;
  dddddx(k,:) = 120*a(k,6);
end


plot(x(1,:), x(2,:))
xlim([-2 2]);
ylim([-2 2]);

tstop=0.25*T;

% compute the location velocity and acceleration at tstop
xstop = a(:,1) + a(:,2)*tstop + a(:,3)*tstop.^2+ a(:,4)*tstop.^3 + a(:,5)*tstop.^4+ a(:,6)*tstop.^5;
dxstop =  a(:,2) + 2*a(:,3)*tstop+ 3*a(:,4)*tstop.^2 + 4*a(:,5)*tstop.^3+ 5*a(:,6)*tstop.^4;
ddxstop =   2*a(:,3)+ 6*a(:,4)*tstop + 12*a(:,5)*tstop.^2+ 20*a(:,6)*tstop.^3;

% recompute the minimum jerk trajectory from the current state to
% the target state
for k=1:3
a(k,:)= minimumJerk(xstop(k),dxstop(k),ddxstop(k),xT(k),dxT(k),ddxT(k),T-tstop);
end

% This is the recomputed trajectory 
t2= linspace(0,T-tstop,10);
n2 = length(t2);
x2 = zeros(3,n2);
dx2 = zeros(3,n2);
ddx2 = zeros(3,n2);
dddx2 = zeros(3,n2);
ddddx2 = zeros(3,n2);
dddddx2 = zeros(3,n2);


for k=1:3
x2(k,:) = a(k,1) + a(k,2)*t2 + a(k,3)*t2.^2+ a(k,4)*t2.^3 + a(k,5)*t2.^4+ a(k,6)*t2.^5;
dx2(k,:) =  a(k,2) + 2*a(k,3)*t2+ 3*a(k,4)*t2.^2 + 4*a(k,5)*t2.^3+ 5*a(k,6)*t2.^4;
ddx2(k,:) =   2*a(k,3)+ 6*a(k,4)*t2 + 12*a(k,5)*t2.^2+ 20*a(k,6)*t2.^3;
dddx2(k,:) =    6*a(k,4) + 24*a(k,5)*t2+ 60*a(k,6)*t2.^2;
ddddx2(k,:) = 24*a(k,5) + 120*a(k,6)*t2;
dddddx2(k,:) = 120*a(k,6);
end


