\documentclass{article}
\usepackage{nips00e}
\usepackage{array,amsgen,amssymb,amsopn,amsmath}
\usepackage{amsthm}
\usepackage[pdftex]{graphicx}
\usepackage{psboxit}
\usepackage{fancybox}
\usepackage[round]{natbib} 
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage[usenames]{color}

 \title{Minimum Jerk and Minimum Acceleration Trajectories}






\newcommand{\bydefinition}{\stackrel{\text{\tiny def}}{=}}
\newcommand{\mynote}[1]{\textcolor{red}{\it  #1}}
\newcommand\given{\medspace|\medspace}
\newcommand\st{:}
\newcommand\argmax{\operatornamewithlimits{argmax}}
\newcommand\argmin{\operatornamewithlimits{argmin}}
\newcommand{\E}{\text{E}}
\newcommand{\Ent}{\text{H}}
\newcommand{\Var}{\text{Var}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Na}{\mathcal{N}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\bydef}{\stackrel{\text{\tiny def}}{=}}





\newenvironment{fminipage}%
{\begin{Sbox}\begin{minipage}}%
{\end{minipage}\end{Sbox}\fbox{\TheSbox}}



\newtheorem{thm}{Theorem}
\newtheorem{res}{Result}
\newtheorem{cor}{Corollary}
\newtheorem{lem}{Lemma}
\theoremstyle{definition}
\newtheorem{rem}{Remark}
\theoremstyle{definition}
\newtheorem{ex}{Example}
\theoremstyle{definition}
\newtheorem{defi}[thm]{Definition}




\begin{document}
\maketitle
\Large
\begin{center} 
 Copyright \copyright{}  2011 Javier
R. Movellan. \end{center} 


    
\newpage

Neville Hollan showed that in many situations when we move our hands
from one initial point to a target point, the trajectory minimizes the
total jerk, i.e. ,the integral over the squared third derivative. Here
we show the classical derivation of minimum jerk trajectories. 

Let $x_0, x^{[1]}_0, x^{[2]}_0$ be the initial location, velocity and
acceleration. Let $T$ the terminal time, at which we want to achieve a
target location, velocity and acceleration $x_T, x^{[1]}_T,
x^{[2]}_T$. Our goal is to find a trajectory $x$ that minimizes the
integral of the squared jerk over time

\begin{align}
I(x) = \frac{1}{2} \int_0^T (x^{[3]}_t)^2\; dt
\end{align}
where $x^{[3]}_t$ represents the third derivative of $x_t$ with
respect to time.  For  a fixed trajectory $x$ let's define a
family of functions of the following form
\begin{equation}
h(\epsilon,t) =  x(t) + \epsilon \delta(t)
\end{equation}
where $\delta$ is an arbitrary function with continuous second partial derivatives and  such that $\delta_0 = \delta_T
= 0$, 
$\delta^{[1]}_0 = \delta^{[1]}_T
= 0$, $\delta^{[2]}_0 = \delta^{[2]}_T
= 0$.
 Let
\begin{equation}
F(\epsilon) 
= \frac{1}{2} \int_0^T ( h^{[3]})^2 \; dt
\end{equation}
A necessary condition  for the trajectory $x$ to minimize $I$ is that 
\begin{equation}
\left.\frac{d  F(\epsilon)}{d \epsilon} \right|_{\epsilon=0} = 0
\end{equation}
Note
\begin{equation}
\frac{d  F(\epsilon)}{d \epsilon} 
= \int_0^T (x^{[3]}_t + \epsilon \delta^{[3]}_t) \; \delta^{[3]}_t \; dt 
\end{equation}
and 
\begin{equation}
\frac{d  F(\epsilon)}{d \epsilon} \Big|_{\epsilon =0}
= \int_0^T x^{[3]}_t  \; \delta^{[3]}_t \; dt 
\end{equation}
Using integration by parts
\begin{align}
\int_0^T x^{[3]}_t  \; \delta^{[3]}_t \; dt = x^{[3]}_t \delta^{[2]}_t \Big|_0^T
- \int_0^T x^{[4]}_t  \; \delta^{[2]}_t \; dt 
\end{align}
and since $\delta_0^{[2]} = \delta_T^{[2]} =0$
\begin{align}
\int_0^T x^{[3]}_t  \; \delta^{[3]}_t \; dt =
- \int_0^T x^{[4]}_t  \; \delta^{[2]}_t \; dt 
\end{align}
Using integration by parts again
\begin{align}
\int_0^T x^{[4]}_t  \; \delta^{[2]}_t \; dt =
 x^{[4]}_t \delta^{[1]}_t \Big|_0^T
- \int_0^T x^{[5]}_t  \; \delta^{[1]}_t \; dt
\end{align}
and since $\delta_0^{[1]} = \delta_T^{[1]} =0$
\begin{align}
\int_0^T x^{[4]}_t  \; \delta^{[2]}_t \; dt =
- \int_0^T x^{[5]}_t  \; \delta^{[1]}_t \; dt
\end{align}
Using integration by parts one last time
\begin{align}
\int_0^T x^{[5]}_t  \; \delta^{[1]}_t \; dt = 
- \int_0^T x^{[6]}_t  \; \delta_t \; dt
\end{align}
Thus
\begin{align}
\frac{d  F(\epsilon)}{d \epsilon} \Big|_{\epsilon =0} =0
\end{align}
requires that
\begin{align}
\int_0^T x^{[6]}_t  \; \delta_t \; dt =0
\end{align}
This must be the case for arbitrary functions $\delta$, thus it must be the case that for all $t\in [0,T]$
\begin{align}
x^{[6]}_t =0
\end{align}
Note a function any fifth order polynomial satisfies the constraint that the 6th derivative be zero everywhere, i.e.,
\begin{align}
x_t = \sum_{k=0}^5 a_k t^k
\end{align}
All that is needed now is to determine the six constants $a_0\cdots a_5$
The first 3 constants can be determined from the initial conditions. For $t=0$
\begin{align}
&a_0= x_0 \\
&a_1 = x^{[1]}_0 \\
&a_2 = \frac{1}{2} x^{[2]}_0 
\end{align}
The last 3 constants can be determined from the terminal conditions
\begin{align}
&x_T = a_0 + a_1 T + a_2 T^2 +a_3 T^3  + a_4T^4 + a_5 T^5\\ 
&x^{[1]}_T =  a_1 + 2 a_2 T + 3 a_3 T^2  + 4 a_4T^3 + 5 a_5 T^4\\ 
&x^{[2]}_T =   2 a_2 + 6 a_3 T  + 12 a_4 T^2 + 20 a_5 T^3
\end{align}
In matrix form
\begin{align}
\left[
\begin{array}{l}
x_T-a_0-a_1T  - a_2T^2 \\
x_T^{[1]} - a_1 - 2 a_2T \\
x_T^{[2]} - 2 a_2 \\
\end{array}
\right]
=
\left[
\begin{array}{ccc}
T^3&T^4&T^5\\
3T^2&4T^4&5T^5\\
6T&12T^2&20T^3
\end{array}
\right]
\left[
\begin{array}{c}
a_3\\
a_4\\
a_5
\end{array}
\right]
\end{align}
Then
\begin{align}
\left[
\begin{array}{c}
a_3\\
a_4\\
a_5
\end{array}
\right]
=
\left[
\begin{array}{ccc}
T^3&T^4&T^5\\
3T^2&4T^4&5T^5\\
6T&12T^2&20T^3
\end{array}
\right]^{-1}
\left[
\begin{array}{l}
x_T-a_0-a_1 T- a_2T^2 \\
x_T^{[1]} - a_1 - 2 a_2T \\
x_T^{[2]} - 2 a_2 \\
\end{array}
\right]
\end{align}
Once the $a$ parameters are known, the entire trajectory from start time $0$ to terminal time $T$ is determined
\begin{align}
x_t = \sum_{k=0}^5 a_k t^k
\end{align}
\section{On Line Version}
In the previous section we precomputed a minimum jerk trajectory to
get from an initial state to a final state in a desired time. We can
also implement an on-line version of the minimum jerk algorithm that
allows for the target states, and/or the target time, to change before
the trajectory is completed.  Given a current location, velocity and
acceleration $x_t, x^{[1]}_t, x^{[2]}_t$ a reach time $T$ and a target
location, velocity, acceleration $x_T, x^{[1]}_T, x^{[2]}_T$. The
location, velocity and acceleration of the minimum jerk trajectory at
at time $t+\Delta_t$ can be obtained by getting the $a$ parameters
with starting point $x_t, x^{[1]}_t,x^{[2]}_t$, target point $x_t,
x^{[1]}_t, x^{[2]}_t$ and reach time $T-t$. We can obtain the
location, velocity and acceleration applying the following formulas
\begin{align}
&x_{t+\Delta t} = \sum_{k=0}^5 a_k (\Delta t)^k\\
&x^{[1]}_{t+\Delta t} = \sum_{k=1}^5 k a_k (\Delta t)^{k-1}\\
&x^{[2]}_{t+\Delta t} = \sum_{k=2}^5 k (k-1) a_k (\Delta t)^{k-2}
\end{align}
We can then change the target states and target times, $t+\Delta t$
the new start time, get the minimum jerk $a$ parameters and
iterate.
\section{3D Case (Double Check, This may not be right)}

In this case 
\begin{align}
I(x) = \frac{1}{2} \int_0^T \|x^{[3]}_t\|^2\; dt
\end{align}
where $x_t = (x_{1,t},x_{2,t}, x_3,t)$. Following the same steps as in the 1D deriation we get 

\begin{align}
\sum_{k=1}^3 \int_0^T x^{[6]}_{k,t} \delta_{k,t}\;dt =0
\end{align} 
for all $\delta_k$ functions with the same conditions as in the 3 derivations above. From this it follows that
\begin{align}
x^{[6]}_{k,t} 
\end{align}
for $t\in [0,T]$ and $k=1,2,3$. Thus basically we can solve the
minimum torque problem independently for each dimension. (to me the weird part about this is that you'd think initial speed in one dimension should have an effect on another dimension)
\subsection{Example Matlab Code}
\begin{verbatim}
function a =  minimumJerk(x0, dx0, ddx0,xT,dxT,ddxT,T)
% Compute a point to point minimum jerk trajectory 
% x0 dx0 ddx0 are the location, velocity and acceleration at the
% start point
% xT dxT ddxT are the target location velocity and acceleration
% T is the time required to move from the start point 
% to the target point
%
% The solution is a 6-D vector of coefficients a
% The minimum jerk trajectory takes the form
% x_t = \sum_{k=1}^6 a_k t^(k-1), for 0\leq t \leq T
%
% Copyright Javier R. Movellan UCSD 2011
% BSD stype license
T2 = T*T; T3 = T2*T; 
T4 = T3*T; T5= T4*T;
a = zeros(6,1);
a(1) = x0;
a(2) = dx0;
a(3) = ddx0/2;
b= [T3 T4 T5 ; 3*T2 4*T3 5*T4; 6*T 12* T2 20* T3];
c = [ xT - a(1) - a(2)*T - a(3)*T2; dxT - a(2) - 2*a(3)*T; 
      ddxT - 2*a(3)];
a(4:6,1)=pinv(b)*c;


\end{verbatim}


\section{Minimum Acceleration Trajectories}
The derivation of minimum acceleration trajectories follows the same patter as the derivation of minimum jekr trajectories. 


Let $x_0, x^{[1]}_0$ be the initial location, velocity and
acceleration. Let $T$ the terminal time, at which we want to achieve a
target location, and velocity  $x_T, x^{[1]}_T$. Our goal is to find a trajectory $x$ that minimizes the
integral of the squared acceleration over time

\begin{align}
I(x) = \frac{1}{2} \int_0^T (x^{[2]}_t)^2\; dt
\end{align}
  For a fixed trajectory $x$ let's define a family of functions of the
  following form
\begin{equation}
h(\epsilon,t) =  x(t) + \epsilon \delta(t)
\end{equation}
where $\delta$ is an arbitrary function with continuous second partial derivatives and  such that $\delta_0 = \delta_T
= 0$, 
$\delta^{[1]}_0 = \delta^{[1]}_T
= 0$
 Let
\begin{equation}
F(\epsilon) 
= \frac{1}{2} \int_0^T ( h^{[2]})^2 \; dt
\end{equation}
A necessary condition  for the trajectory $x$ to minimize $I$ is that 
\begin{equation}
\left.\frac{d  F(\epsilon)}{d \epsilon} \right|_{\epsilon=0} = 0
\end{equation}
Note
\begin{equation}
\frac{d  F(\epsilon)}{d \epsilon} 
= \int_0^T (x^{[2]}_t + \epsilon \delta^{[2]}_t) \; \delta^{[2]}_t \; dt 
\end{equation}
and 
\begin{equation}
\frac{d  F(\epsilon)}{d \epsilon} \Big|_{\epsilon =0}
= \int_0^T x^{[2]}_t  \; \delta^{[2]}_t \; dt 
\end{equation}
Using integration by parts
\begin{align}
\int_0^T x^{[2]}_t  \; \delta^{[2]}_t \; dt = x^{[2]}_t \delta^{[1]}_t \Big|_0^T
- \int_0^T x^{[3]}_t  \; \delta^{[1]}_t \; dt 
\end{align}
and since $\delta_0^{[1]} = \delta_T^{[1]} =0$
\begin{align}
\int_0^T x^{[2]}_t  \; \delta^{[1]}_t \; dt =
- \int_0^T x^{[3]}_t  \; \delta^{[1]}_t \; dt 
\end{align}
Using integration by parts again
\begin{align}
-\int_0^T x^{[3]}_t  \; \delta^{[1]}_t \; dt =
 -x^{[3]}_t \delta_t \Big|_0^T
+\int_0^T x^{[4]}_t  \; \delta_t \; dt
\end{align}
and since $\delta_0 = \delta_T =0$
\begin{align}
-\int_0^T x^{[3]}_t  \; \delta^{[1]}_t \; dt =
 \int_0^T x^{[4]}_t  \; \delta_t \; dt
\end{align}
Therefore
\begin{align}
\frac{d  F(\epsilon)}{d \epsilon} \Big|_{\epsilon =0} = \int_0^T x^{[4]}_t  \; \delta_t \; dt
\end{align}
and thus  the condition
\begin{align}
\frac{d  F(\epsilon)}{d \epsilon} \Big|_{\epsilon =0} =0
\end{align}
requires that
\begin{align}
\int_0^T x^{[4]}_t  \; \delta_t \; dt =0
\end{align}
This must be the case for arbitrary functions $\delta$, thus it must be the case that for all $t\in [0,T]$
\begin{align}
x^{[4]}_t =0
\end{align}
Note a function any third order polynomial satisfies the constraint that the 4th derivative be zero everywhere, i.e.,
\begin{align}
x_t = \sum_{k=0}^3 a_k t^k
\end{align}
All that is needed now is to determine the four  constants $a_0\cdots a_3$
The first 2 constants can be determined from the initial conditions. For $t=0$
\begin{align}
&a_0= x_0 \\
&a_1 = x^{[1]}_0 \\
\end{align}
The last 2 constants can be determined from the terminal conditions
\begin{align}
&x_T = a_0 + a_1 T + a_2 T^2 +a_3 T^3  \\ 
&x^{[1]}_T =  a_1 + 2 a_2 T + 3 a_3 T^2 \\ 
&x^{[2]}_T =   2 a_2 + 6 a_3 T 
\end{align}
In matrix form
\begin{align}
\left[
\begin{array}{l}
x_T-a_0-a_1 T \\
x_T^{[1]} - a_1  \\
\end{array}
\right]
=
\left[
\begin{array}{cc}
T^2&T^3\\
2T&3T^2\\
\end{array}
\right]
\left[
\begin{array}{c}
a_2\\
a_3\\
\end{array}
\right]
\end{align}
Then
\begin{align}
\left[
\begin{array}{c}
a_2\\
a_3
\end{array}
\right]
=
\left[
\begin{array}{cc}
T^2&T^3\\
2T&3T^2\\
\end{array}
\right]^{-1}
\left[
\begin{array}{l}
x_T-a_0-a_1T  \\
x_T^{[1]} - a_1  \\
\end{array}
\right]
\end{align}
Once the $a$ parameters are known, the entire trajectory from start time $0$ to terminal time $T$ is determined
\begin{align}
x_t = \sum_{k=0}^3 a_k t^k
\end{align}


\subsection{Example Matlab Code}
\begin{verbatim}
function a =  minimumJerk(x0, dx0, ddx0,xT,dxT,ddxT,T)
% Compute a point to point minimum jerk trajectory 
% x0 dx0 ddx0 are the location, velocity and acceleration at the
% start point
% xT dxT ddxT are the target location velocity and acceleration
% T is the time required to move from the start point 
% to the target point
%
% The solution is a 6-D vector of coefficients a
% The minimum jerk trajectory takes the form
% x_t = \sum_{k=1}^6 a_k t^(k-1), for 0\leq t \leq T
%
% Copyright Javier R. Movellan UCSD 2011
% BSD stype license
T2 = T*T; T3 = T2*T; 
T4 = T3*T; T5= T4*T;
a = zeros(6,1);
a(1) = x0;
a(2) = dx0;
a(3) = ddx0/2;
b= [T3 T4 T5 ; 3*T2 4*T3 5*T4; 6*T 12* T2 20* T3];
c = [ xT - a(1) - a(2)*T - a(3)*T2; dxT - a(2) - 2*a(3)*T; 
      ddxT - 2*a(3)];
a(4:6,1)=pinv(b)*c;


\end{verbatim}


\bibliographystyle{abbrvnat} \bibliography{movellan.stats}


\end{document}



