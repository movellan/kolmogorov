from scipy.stats import norm
import numpy as np
from matplotlib.pyplot import *
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc

# rc('font',size=14)
# rc('font',family='timesz')
# rc('axes',labelsize=14)


def gausspdf(x):
    p1 = np.exp(-x[0]**2/2.)/np.sqrt(2.0*np.pi)
    p2= np.exp(-x[1]**2/2.)/np.sqrt(2.0*np.pi)
    return p1*p2

x,y = np.meshgrid( np.arange(-4.5,4.5,0.01),np.arange(-4.5,4.5,0.01) )
u = np.zeros(np.shape(x))
v = np.zeros(np.shape(x))
p = np.zeros(np.shape(x))
dp = np.zeros(np.shape(x))
dp = np.zeros(np.shape(x))

for i in range(len(x)):
    for j in range(len(y)):
        z = np.array([x[i,j],y[i,j]])
        p[i,j] = gausspdf(z)
        u[i,j]=z[0]*p[i,j]
        v[i,j]=z[1]*p[i,j]
        dp[i,j]= -((1- (z[0]**2.+z[1]**2.)/2.))*p[i,j]
m0 = np.sqrt(x**2+y**2)
m =np.sqrt(u**2.+v**2.)


fig = figure()
ax = fig.gca()

contour(x, y, p,10)
axis('equal')
lw = 4*m0/m0.max()
color = lw

streamplot(x, y, x, y, color='b', linewidth=lw, cmap=cm.coolwarm,arrowstyle='->', arrowsize=1.5,density=2)
axis('equal')
xlim([-4,4])
ylim([-4,4])
xticks([-2,0,2],fontsize=18)
yticks([-2,0,2],fontsize=18)

savefig('fig1.pdf')


fig = figure()
ax = fig.gca()

contour(x, y, p,10)
axis('equal')
lw = 4*m/m.max()
color = lw



streamplot(x, y, u, v, color='b', linewidth=lw, cmap=cm.coolwarm,arrowstyle='->', arrowsize=1.5,density=2)
axis('equal')
xlim([-4,4])
ylim([-4,4])
xticks([-2,0,2],fontsize=18)
yticks([-2,0,2],fontsize=18)

savefig('fig2.pdf')

show()


fig = figure()
ax = gca(projection='3d')

surf = ax.plot_surface(x, y, dp, rstride=25, cstride=25, cmap=cm.coolwarm,
                       linewidth=0.25, antialiased=False)

ax.set_xlim3d([-4,4])
ax.set_ylim3d([-4,4])
xticks([-4,-2,0,2, 4])
yticks([-4,-2,0,2, 4])
ax.set_zticks([0.])

ax.w_xaxis.set_pane_color([0,0,0])
ax.w_yaxis.set_pane_color([0,0,0])
ax.w_zaxis.set_pane_color([0,0,0])

ax.grid(False)
savefig('fig3.pdf')
show()


fig = figure()
ax = gca(projection='3d')
surf = ax.plot_surface(x, y, p, rstride=25, cstride=25, cmap=cm.coolwarm,
                       linewidth=0.25, antialiased=False)


ax.set_xlim3d([-4,4])
ax.set_ylim3d([-4,4])
xticks([-4,-2,0,2, 4])
yticks([-4,-2,0,2, 4])
ax.set_zticks([0.])

ax.w_xaxis.set_pane_color([0,0,0])
ax.w_yaxis.set_pane_color([0,0,0])
ax.w_zaxis.set_pane_color([0,0,0])

ax.grid(False)
savefig('fig4.pdf')
show()
