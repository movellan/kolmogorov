import numpy as np
import matplotlib.pyplot as plt
# simulation of 1D quantom harmonic oscilator
# with x*2 potential function
# using forward euler method
L = 30.0 # x in [-L, L]
x0 = -5.0 # center of initial wave function
sig = 0.5
dx = 0.05
dt = 0.00001
k = 1.0
w=2
K=w**2
a=np.power(K,0.25)
xs = np.arange(-L,L,dx)
nn = len(xs)

mu = k*dt/(dx)**2
dd = 1.0+mu
ee = 1.0-mu
ti = 0.0
tf = 100.0
t = ti
V=np.zeros(len(xs))
u=np.zeros(nn,dtype="complex")
V=K*(xs)**2/2            #harmonic oscillator potential

#u=(np.sqrt(a)/1.33)*np.exp(-(a*(xs - x0))**2)+0j    #initial condition for wave function
sigma = np.sqrt( 0.5)/a
u=1.0/(np.sqrt(2.0*np.pi)*sigma)*np.exp(-0.5*((xs - x0)/sigma)**2) +0j
Z = np.sqrt(np.sum(abs(u)**2.0)*dx)
u = u/Z
u[0]=0.0          #boundary condition
u[-1] = 0.0      #boundary condition

# A = np.zeros((nn-2,nn-2),dtype="complex")     #define A
# for i in range(nn-3):
#     A[i,i] = 1+1j*(mu/2+w*dt*xs[i]**2/4)
#     A[i,i+1] = -1j*mu/4.
#     A[i+1,i] = -1j*mu/4.
# A[nn-3,nn-3] = 1+1j*mu/2+1j*dt*xs[nn-3]**2/4
#
# B = np.zeros((nn-2,nn-2),dtype="complex")    #define A*
# for i in range(nn-3):
#     B[i,i] = 1-1j*mu/2-1j*w*dt*xs[i]**2/4
#     B[i,i+1] = 1j*mu/4.
#     B[i+1,i] = 1j*mu/4.
#     B[nn-3,nn-3] = 1-1j*(mu/2)-1j*dt*xs[nn-3]**2/4

#X = np.linalg.inv(A)    #take inverse of A
plt.ion() # turns on interactive Model

l, = plt.plot(xs,np.abs(u)**2.0,lw=2,color='blue')   #plot initial wave function
#l, = plt.plot(xs,u.real,lw=2,color='blue')   #plot initial wave function

#T=np.matmul(X,B)                                #multiply A inverse with A*

d2=np.zeros(len(u)).astype(complex)
delta=np.zeros(len(u)).astype(complex)
kk=0
while t<tf:
    kk+=1;
    for k in range(1,len(u)-1):
        d2[k] = (u[k+1] - 2*u[k] + u[k-1])/(dx*dx)
    u +=  dt*1j*(0.5*d2- u*V)
    Z = np.sqrt(np.sum(abs(u)**2.0)*dx)
    u = u/Z # normalize so probabilities add up to one
    if kk%500 ==0:
        print kk
        l.set_ydata((abs(u)**2.0))              #update plot with new u
    #l.set_ydata(u.real)
    t += dt
    plt.pause(0.00001)
